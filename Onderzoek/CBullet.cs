using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TDFighter
{
    class CBullet
    {
        Texture2D m_tex;
        Rectangle m_rect;
        Vector2 m_rotOrigin;

        float m_activeTime;
        float m_speed;
        Vector2 m_curVelocity;
        Vector2 m_position;
        CEntity m_entityToFollow;
        float m_angle = 0;

        float m_dt;
        bool m_isActive = false;
        public bool IsActive
        {
            get { return m_isActive; }
        }

        public CBullet(Texture2D Tex, float Speed, float ActiveTime)
        {
            m_tex = Tex;
            m_rect = new Rectangle(0, 0, m_tex.Width, m_tex.Height);
            m_rotOrigin = new Vector2(m_tex.Width / 2, m_tex.Height / 2);
            m_speed = Speed;
            m_activeTime = ActiveTime;
        }

        public void Activate(Vector2 Dir, Vector2 EntityPosition, Vector2 DirToRelativePosition, float AngleOfRelativePosition,
            CEntity EntityToFollow)
        {
            m_entityToFollow = EntityToFollow;
            Dir.Y = Dir.Y * -1.0f; // For the bullet movement
            Dir.Normalize();
            m_curVelocity = Dir * m_speed;
            Dir.Y = Dir.Y * -1.0f; // For the bullet rotation
            m_angle = CToolbox.VectorToAngle(Dir);/*-(float)Math.Atan2(Dir.Y, Dir.X) + (float)MathHelper.PiOver2;*/

            Vector2 test = CToolbox.AngleToVector(AngleOfRelativePosition + m_angle);
            m_position = CToolbox.AngleToVector(AngleOfRelativePosition) * DirToRelativePosition;
            m_position += EntityPosition;

            m_rect.X = (int)m_position.X;
            m_rect.Y = (int)m_position.Y;
            m_dt = 0;
            m_isActive = true;
        }

        public void Update(float DT)
        {
            m_dt += DT;

            if (m_dt > m_activeTime)
            {
                m_isActive = false;
            }

            // If this bullet is supposed to follow and hit an entity
            if (m_entityToFollow != null)
            {
                Vector2 dir = m_entityToFollow.CurPosition - m_position;
                dir.Normalize();
                m_curVelocity = dir * m_speed;
                m_angle = -(float)Math.Atan2(dir.Y, dir.X) + (float)MathHelper.PiOver2;
            }

            m_position += m_curVelocity;

            m_rect.X = (int)m_position.X;
            m_rect.Y = (int)m_position.Y;
        }

        public void Render()
        {
            CRender.Instance.RenderSprite(m_rect, null, Color.White, m_angle, m_rotOrigin, m_tex);
        }
    }
}

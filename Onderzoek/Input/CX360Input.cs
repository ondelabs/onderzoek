﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.GamerServices;

namespace Onderzoek.Input
{
    public class CX360Input : CInput
    {
        // Private varibales
        protected GamePadState m_previousGamePadState;
        protected float m_vibrationAmountL = 0.0f;
        protected float m_vibrationAmountR = 0.0f;
        protected bool m_vibrateOn = false;

        public void CheckControllerStatus(CInputData InputData)
        {
            if (GamePad.GetState(PlayerIndex.One).IsConnected == true)
            {
                InputData.m_cntr1Connect = true;
            }
            else
            {
                InputData.m_cntr1Connect = false;
            }
        }

        // Checks which buttons on the X360 controller has been pushed.
        public override void UpdateInput(CInputData InputData)
        {
#if XBOX360
            if (Guide.IsVisible == true)
            {
                return;
            }
#endif

            // Network status screen
            InputData.m_networkStatus = false;
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            {
                InputData.m_back = true;
                //InputData.m_networkStatus = true;
            }

            // Go back in menu or exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.B == ButtonState.Pressed && m_previousGamePadState.Buttons.B == ButtonState.Released)
            {
                //InputData.m_back = true;
                if (InputData.m_renderMRTs == true)
                {
                    InputData.m_renderMRTs = false;
                }
                else
                {
                    InputData.m_renderMRTs = true;
                }
            }

            // Get the current gamepad state.
            GamePadState current_game_pad_state = GamePad.GetState(PlayerIndex.One);

            // Process input
            InputData.m_cntr1Connect = true;

            if (m_vibrateOn == false)
            {
                m_vibrationAmountL = 0;
                m_vibrationAmountR = 0;
            }

            // Right trigger is pressed.
            if (current_game_pad_state.Triggers.Right > 0)
            {
                //if (m_previousGamePadState.Triggers.Right == 0)
                {
                    InputData.m_weaponFire = true;
                }

                // Left trigger is currently being pressed; add vibration.
                //m_vibrationAmountR = m_currentGamePadState.Triggers.Right;
            }

            // Left thumb stick
            InputData.m_leftStick.X = current_game_pad_state.ThumbSticks.Left.X;
            InputData.m_leftStick.Y = current_game_pad_state.ThumbSticks.Left.Y;

            // Right thumb stick
            InputData.m_rightStick.X = -current_game_pad_state.ThumbSticks.Right.X;
            InputData.m_rightStick.Y = -current_game_pad_state.ThumbSticks.Right.Y;

            // If the X button is pressed
            if ((current_game_pad_state.Buttons.X == ButtonState.Pressed) && (m_previousGamePadState.Buttons.X == ButtonState.Released))
            {
                m_vibrationAmountL = 0.2f;
                m_vibrationAmountR = 0.2f;

                if (InputData.m_showTextInformation == true)
                {
                    InputData.m_showTextInformation = false;
                }
                else
                {
                    InputData.m_showTextInformation = true;
                }
            }

            // If the RightStick button is pressed
            if ((current_game_pad_state.Buttons.RightStick == ButtonState.Pressed) && (m_previousGamePadState.Buttons.RightStick == ButtonState.Released))
            {
                m_vibrationAmountL = 0.2f;
                m_vibrationAmountR = 0.2f;
            }

            // If the Y button is pushed, not held down
            if (current_game_pad_state.Buttons.Y == ButtonState.Pressed && m_previousGamePadState.Buttons.Y == ButtonState.Released)
            {
                if (InputData.m_jump == true)
                {
                    InputData.m_jump = false;
                }
                else
                {
                    InputData.m_jump = true;
                }
                //if (m_vibrateOn == false)
                //{
                //    m_vibrationAmountL = 0.25f;
                //    m_vibrationAmountR = 0.25f;
                //    m_vibrateOn = true;
                //}
                //else
                //{
                //    m_vibrationAmountL = 0.0f;
                //    m_vibrationAmountR = 0.0f;
                //    m_vibrateOn = false;
                //}
            }

            // If the A button is pushed, not held down
            if (current_game_pad_state.Buttons.A == ButtonState.Pressed && m_previousGamePadState.Buttons.A == ButtonState.Released)
            {
                if (InputData.m_renderAll == false)
                {
                    InputData.m_renderAll = true;
                }
                else
                {
                    InputData.m_renderAll = false;
                }
            }

            if ((current_game_pad_state.Buttons.LeftShoulder == ButtonState.Pressed) && (m_previousGamePadState.Buttons.LeftShoulder == ButtonState.Released))
            {
                if (InputData.m_leftClick == true)
                {
                    InputData.m_leftClick = false;
                }
                else
                {
                    InputData.m_leftClick = true;
                }
            }
            if ((current_game_pad_state.Buttons.RightShoulder == ButtonState.Pressed) && (m_previousGamePadState.Buttons.RightShoulder == ButtonState.Released))
            {
                if (InputData.m_rightClick == true)
                {
                    InputData.m_rightClick = false;
                }
                else
                {
                    InputData.m_rightClick = true;
                }
            }

            GamePad.SetVibration(PlayerIndex.One, m_vibrationAmountL, m_vibrationAmountR);

            m_previousGamePadState = current_game_pad_state;
        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Onderzoek.Input
{
    public class CInput
    {
        // Checks which buttons on the X360 controller has been pushed.
        public virtual void UpdateInput(CInputData InputData)
        {
        }

        public virtual void Reset()
        {
        }
    }
}

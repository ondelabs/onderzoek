﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Onderzoek.Input
{
    public class CInputData
    {
        // For debug purpose
        public bool m_cntr1Connect = false;
        public bool m_wasCntr1Connect = true;
        public bool m_cntr2Connect = false;
        public bool m_renderAll = false;
        public bool m_renderMRTs = false;
        public bool m_showTextInformation = false;
        public bool m_networkStatus = false;
        public bool m_printScreen = false;

        // Main Game inputs.
        public Vector2 m_mousePos;
        public Vector2 m_leftStick;
        public Vector2 m_rightStick;
        public bool m_back = false;
        public bool m_jump = false;
        public bool m_weaponFire = false;
        public bool m_leftClick = false;
        public bool m_rightClick = false;
        public bool m_actionKey = false;
        public bool m_onePushActionKey = false;
        public bool m_enter = false;
        public bool m_down = false;
        public bool m_up = false;
        public bool m_left = false;
        public bool m_right = false;
        public bool m_forward = false;
        public bool m_backward = false;
        public bool m_leftStrafe = false;
        public bool m_rightStrafe = false;
        public bool m_lShift = false;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
#if WINDOWS

using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.GamerServices;

using Onderzoek.Visual;

namespace Onderzoek.Input
{
    public class CPCInput : CInput
    {
        // Private varibales
        protected KeyboardState m_previousKeyboardState;
        protected MouseState m_beginningMouseState;
        protected MouseState m_previousMouseState;

        public CPCInput()
        {
        }

        public virtual void Initialise()
        {
            Reset();
        }

        public override void Reset()
        {
            Mouse.SetPosition((int)CVisual.Instance.GetWidthRes() / 2, (int)CVisual.Instance.GetHeightRes() / 2);
            m_beginningMouseState = Mouse.GetState();
        }

        // Checks the status of the keys on the PCs keyboard and the mouse position.
        public override void UpdateInput(CInputData InputData)
        {
#if XBOX360
            if (Guide.IsVisible == true)
            {
                return;
            }
#endif

            // Get the current gamepad state.
            KeyboardState current_key_state = Keyboard.GetState();

            // Get the current keyboard state.
            MouseState current_mouse_state = Mouse.GetState();

            // Network status screen
            if (current_key_state.IsKeyDown(Keys.Tab) == true && m_previousKeyboardState.IsKeyDown(Keys.Tab) == false)
            {
                InputData.m_networkStatus = true;
            }

            // Change particle render method to normal mode.
            if (current_key_state.IsKeyDown(Keys.D1) == true && m_previousKeyboardState.IsKeyDown(Keys.D1) == false)
            {
                CVisual.Instance.SetParticleRendering(0);
            }

            // Change particle render method to shader instance mode.
            if (current_key_state.IsKeyDown(Keys.D2) == true && m_previousKeyboardState.IsKeyDown(Keys.D2) == false)
            {
                CVisual.Instance.SetParticleRendering(1);
            }

            // Change particle render method to hardware instance mode.
            if (current_key_state.IsKeyDown(Keys.D3) == true && m_previousKeyboardState.IsKeyDown(Keys.D3) == false)
            {
                CVisual.Instance.SetParticleRendering(2);
            }

            // Go back in menu or exit
            if (current_key_state.IsKeyDown(Keys.Escape) == true && m_previousKeyboardState.IsKeyDown(Keys.Escape) == false)
            {
                InputData.m_back = true;
            }

            // Action key
            InputData.m_actionKey = false;
            if (current_key_state.IsKeyDown(Keys.E) == true)
            {
                InputData.m_actionKey = true;
            }

            // Action key
            InputData.m_onePushActionKey = false;
            if (current_key_state.IsKeyDown(Keys.E) == true && m_previousKeyboardState.IsKeyDown(Keys.E) == false)
            {
                InputData.m_onePushActionKey = true;
            }

            // Turn Occlusion on or off
            if (current_key_state.IsKeyDown(Keys.O) == true && m_previousKeyboardState.IsKeyDown(Keys.O) == false)
            {
                CVisual.Instance.TurnOnOffSSAO();
            }
            // Turn shadows on or off
            if (current_key_state.IsKeyDown(Keys.I) == true && m_previousKeyboardState.IsKeyDown(Keys.I) == false)
            {
                CVisual.Instance.TurnOnOffShadows();
            }
            // Turn lights on or off
            if (current_key_state.IsKeyDown(Keys.Y) == true && m_previousKeyboardState.IsKeyDown(Keys.Y) == false)
            {
                CVisual.Instance.TurnOnOffLights();
            }

            // Print screen (back buffer)
            if (current_key_state.IsKeyDown(Keys.P) == true && m_previousKeyboardState.IsKeyDown(Keys.P) == false)
            {
                CVisual.Instance.TakeScreenShot();
            }

            // If left control and R are pushed then turn all rendering on/off
            if (current_key_state.IsKeyDown(Keys.LeftControl) == true && current_key_state.IsKeyDown(Keys.R) == true
                && m_previousKeyboardState.IsKeyDown(Keys.R) == false)
            {
                if (InputData.m_renderAll == false)
                {
                    InputData.m_renderAll = true;
                }
                else
                {
                    InputData.m_renderAll = false;
                }
            }

            // F button is pressed.
            InputData.m_weaponFire = false;
            if (current_key_state.IsKeyDown(Keys.Space) == true)
            {
                InputData.m_weaponFire = true;
            }

            // M is pressed
            if ((current_key_state.IsKeyDown(Keys.M) == true) && (m_previousKeyboardState.IsKeyDown(Keys.M) == false))
            {
                if (InputData.m_renderMRTs == true)
                {
                    InputData.m_renderMRTs = false;
                }
                else
                {
                    InputData.m_renderMRTs = true;
                }
            }

            // H is pressed
            if ((current_key_state.IsKeyDown(Keys.H) == true) && (m_previousKeyboardState.IsKeyDown(Keys.H) == false))
            {
                if (InputData.m_showTextInformation == true)
                {
                    InputData.m_showTextInformation = false;
                }
                else
                {
                    InputData.m_showTextInformation = true;
                }
            }

            // Enter is pressed
            if ((current_key_state.IsKeyDown(Keys.Enter) == true) && (m_previousKeyboardState.IsKeyDown(Keys.Enter) == false))
            {
                if (InputData.m_enter == true)
                {
                    InputData.m_enter = false;
                }
                else
                {
                    InputData.m_enter = true;
                }
            }
            else
            {
                if (InputData.m_enter == true)
                {
                    InputData.m_enter = false;
                }
            }

            InputData.m_lShift = false;
            if (current_key_state.IsKeyDown(Keys.LeftShift) == true)
            {
                InputData.m_lShift = true;
            }

            // WASD - forward, backward and strafe maneuvers
            // Reseting values
            InputData.m_leftStick.Y = 0;
            InputData.m_leftStick.X = 0;
            InputData.m_forward = false;
            InputData.m_backward = false;
            InputData.m_leftStrafe = false;
            InputData.m_rightStrafe = false;
            if (current_key_state.IsKeyDown(Keys.W) == true)
            {
                InputData.m_forward = true;
                if (m_previousKeyboardState.IsKeyDown(Keys.W) == false)
                {
                    InputData.m_up = true;
                }
                else
                {
                    InputData.m_up = false;
                }

                // Half the speed if a side key and this key is pushed
                if (current_key_state.IsKeyDown(Keys.A) == true || current_key_state.IsKeyDown(Keys.D) == true)
                {
                    InputData.m_leftStick.Y = 0.5f;
                }
                else
                {
                    InputData.m_leftStick.Y = 1.0f;
                }
            }
            if (current_key_state.IsKeyDown(Keys.S) == true)
            {
                InputData.m_backward = true;
                if (m_previousKeyboardState.IsKeyDown(Keys.S) == false)
                {
                    InputData.m_down = true;
                }
                else
                {
                    InputData.m_down = false;
                }

                // Half the speed if a side key and this key is pushed
                if (current_key_state.IsKeyDown(Keys.A) == true || current_key_state.IsKeyDown(Keys.D) == true)
                {
                    InputData.m_leftStick.Y = -0.5f;
                }
                else
                {
                    InputData.m_leftStick.Y = -1.0f;
                }
            }
            if (current_key_state.IsKeyDown(Keys.A) == true)
            {
                InputData.m_leftStrafe = true;
                if (m_previousKeyboardState.IsKeyDown(Keys.A) == false)
                {
                    InputData.m_left = true;
                }
                else
                {
                    InputData.m_left = false;
                }

                // Half the speed if a forward/backward and this key is pushed
                if (current_key_state.IsKeyDown(Keys.W) == true || current_key_state.IsKeyDown(Keys.S) == true)
                {
                    InputData.m_leftStick.X = -0.5f;
                }
                else
                {
                    InputData.m_leftStick.X = -1.0f;
                }
            }
            if (current_key_state.IsKeyDown(Keys.D) == true)
            {
                InputData.m_rightStrafe = true;
                if (m_previousKeyboardState.IsKeyDown(Keys.D) == false)
                {
                    InputData.m_right = true;
                }
                else
                {
                    InputData.m_right = false;
                }

                // Half the speed if a forward/backward and this key is pushed
                if (current_key_state.IsKeyDown(Keys.W) == true || current_key_state.IsKeyDown(Keys.S) == true)
                {
                    InputData.m_leftStick.X = 0.5f;
                }
                else
                {
                    InputData.m_leftStick.X = 1.0f;
                }
            }

            InputData.m_jump = false;
            if (current_key_state.IsKeyDown(Keys.F) == true && m_previousKeyboardState.IsKeyUp(Keys.F))
            {
                if (InputData.m_jump == true)
                {
                    InputData.m_jump = false;
                }
                else
                {
                    InputData.m_jump = true;
                }
            }

            // Mouse - Camera control
            //if (current_mouse_state.LeftButton == ButtonState.Pressed)
            {
                InputData.m_rightStick.Y = (current_mouse_state.Y - m_beginningMouseState.Y) * 0.05f;
                InputData.m_rightStick.X = (m_beginningMouseState.X - current_mouse_state.X) * 0.05f;

                InputData.m_mousePos.X -= InputData.m_rightStick.X * 5.0f;
                InputData.m_mousePos.Y += InputData.m_rightStick.Y * 5.0f;
                InputData.m_mousePos.X = CToolbox.Clamp(InputData.m_mousePos.X, 0, 100.0f);
                InputData.m_mousePos.Y = CToolbox.Clamp(InputData.m_mousePos.Y, 0, 100.0f);

                Mouse.SetPosition((int)CVisual.Instance.GetWidthRes() / 2, (int)CVisual.Instance.GetHeightRes() / 2);

                // Prints to output debug window
                //System.Diagnostics.Trace.WriteLine(InputData.m_rightStick);
            }

            if ((current_mouse_state.LeftButton == ButtonState.Pressed) && (m_previousMouseState.LeftButton == ButtonState.Released))
            {
                if (InputData.m_leftClick == true)
                {
                    InputData.m_leftClick = false;
                }
                else
                {
                    InputData.m_leftClick = true;
                }
            }
            if ((current_mouse_state.RightButton == ButtonState.Pressed) && (m_previousMouseState.RightButton == ButtonState.Released))
            {
                if (InputData.m_rightClick == true)
                {
                    InputData.m_rightClick = false;
                }
                else
                {
                    InputData.m_rightClick = true;
                }
            }
            // If the middle mouse button is pressed
            if ((current_mouse_state.MiddleButton == ButtonState.Pressed) && (m_previousMouseState.MiddleButton == ButtonState.Released))
            {
            }

            m_previousKeyboardState = current_key_state;
            m_previousMouseState = current_mouse_state;
        }

        public static void GetMsgKeyDown(ref string PrvKey, ref string Msg)
        {
            //We get the state of the keyboard at this moment which will hold keys pressed etc.
            KeyboardState ks = Keyboard.GetState();
            bool left_shift = false;

            // --------------------MSG--------------------- //
            Keys[] temp = ks.GetPressedKeys();
            if (temp.Length == 0)
            {
                PrvKey = "";
            }
            if (temp.Length != 0)
            {
                string value = temp[0].ToString();
                if (string.Compare(PrvKey, value) != 0)
                {
                    if (string.Compare("Back", value) == 0)
                    {
                        if (Msg.Length > 0)
                        {
                            Msg = Msg.Remove(Msg.Length - 1);
                        }
                    }
                    if (temp.Length > 1 && string.Compare("LeftShift", temp[1].ToString()) == 0)
                    {
                        left_shift = true;
                    }
                    else if (temp.Length > 1 && string.Compare("RightShift", temp[1].ToString()) == 0)
                    {
                        left_shift = true;
                    }
                    if (string.Compare("Space", temp[0].ToString()) == 0)
                    {
                        Msg += " ";
                    }
                    else if (value.Length > 1)
                    {
                        if (string.Compare("Oem", 0, temp[0].ToString(), 0, 3) == 0)
                        {
                            if (string.Compare("OemPeriod", value) == 0)
                            {
                                Msg += ".";
                            }
                            else if (string.Compare("OemComma", value) == 0)
                            {
                                Msg += ",";
                            }
                            else if (string.Compare("OemQuestion", value) == 0)
                            {
                                if (ks.IsKeyDown(Keys.LeftShift) == true || ks.IsKeyDown(Keys.RightShift) == true)
                                {
                                    Msg += "?";
                                }
                                else
                                {
                                    Msg += "/";
                                }
                            }
                        }
                        else if (value.Length == 2 && value[0] == 'D')
                        {
                            Msg += value[1];
                        }
                    }
                    else
                    {
                        if (!left_shift)
                        {
                            Msg += temp[0].ToString().ToLower();
                        }
                        else
                        {
                            Msg += temp[0].ToString().ToUpper();
                        }
                    }
                }

                PrvKey = temp[0].ToString();
            }
            // --------------------MSG--------------------- //
        }

        public static void GetMsgKeyDownForIP(ref string PrvKey, ref string Msg)
        {
            //We get the state of the keyboard at this moment which will hold keys pressed ect.
            KeyboardState ks = Keyboard.GetState();

            // --------------------MSG--------------------- //
            Keys[] temp = ks.GetPressedKeys();
            if (temp.Length == 0)
            {
                PrvKey = "";
            }
            if (temp.Length != 0)
            {
                string value = temp[0].ToString();
                if (string.Compare(PrvKey, value) != 0)
                {
                    if (string.Compare("Back", value) == 0)
                    {
                        if (Msg.Length > 0)
                        {
                            Msg = Msg.Remove(Msg.Length - 1);
                        }
                    }
                    else if (value.Length > 1)
                    {
                        if (string.Compare("Oem", 0, temp[0].ToString(), 0, 3) == 0)
                        {
                            if (string.Compare("OemPeriod", value) == 0)
                            {
                                Msg += ".";
                            }
                        }
                        else if (value.Length == 2 && value[0] == 'D')
                        {
                            Msg += value[1];
                        }
                    }
                }

                PrvKey = temp[0].ToString();
            }
            // --------------------MSG--------------------- //
        }
    }
}

#endif
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Onderzoek.Entity;
using Onderzoek.ModelData;
using Onderzoek.Camera;
using Onderzoek.Visual;

using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;

using Microsoft.Xna.Framework.Graphics;

namespace Onderzoek
{
    public class CEntityCreator
    {
        static CEntityCreator m_instance;

        /// <summary>
        /// setting this instance will set the base class instance also.
        /// </summary>
        public static CEntityCreator Instance
        {
            
            get {
                if (m_instance == null)
                    throw new System.Exception("Entity Creator not set");
                return m_instance;
            }

            set 
            {
                m_instance = value;
                
            }
        }

        public virtual CEntity getEntityByID(int id, Vector3 pos, Vector3 rotation, string entName, ContentManager content, Onderzoek.Camera.CCamera camera, Dictionary<string, CModel.SModelData> modelList)
        {
#if ENGINE
            switch (id)
            {
                case (int)EBasicEntityIDs.DotXEntity:
                    {
                        CDotXModel temp_modelr = new CDotXModel();
                        temp_modelr = (CDotXModel)temp_modelr.Initialise(content, "Model/platform", modelList);

                        return new CDotXEntity(pos, rotation, entName, temp_modelr, temp_modelr.GetMaterials(content, "Model/platform"), false, false);
                    }
                case (int)EBasicEntityIDs.AnimatedEntity:
                    {
                        CAnimatedModel animModel = new CAnimatedModel();
                        animModel = (CAnimatedModel)animModel.Initialise(content, "AnimatedModel/pirate", modelList);

                        return new CAnimatedEntity(pos, rotation, entName, animModel, animModel.GetMaterials(content, "AnimatedModel/pirate"), false);
                    }
                case (int)EBasicEntityIDs.DirLightEntity:
                    {

                        CDotXModel temp_modelr = new CDotXModel();
                        temp_modelr = (CDotXModel)temp_modelr.Initialise(content, "Model/platform", modelList);

                        CDirLightEntity dirLight = new CDirLightEntity(Vector3.Up, pos, entName, false, 2, Vector3.One, 1.0f, temp_modelr.GetOBB(), true, 1.0f);

                        return dirLight;
                    }
 
                case (int)EBasicEntityIDs.TerrainEntity:
                    {
                        Texture2D heightmap_tex = content.Load<Texture2D>("Texture/heightmap");

                        Vector2 split = new Vector2(10.0f, 10.0f);
                        Vector2 length = new Vector2(256.0f, 256.0f);
                        Vector2 num_vert = new Vector2(256.0f, 256.0f);

                        CTerrainModel temp_terrain = new CTerrainModel();
                        temp_terrain.LoadAndCreateTerrain(heightmap_tex, CVisual.Instance.GetGraphicsDevice(), length, num_vert, 20, split, modelList, "Terrain0");

                        CTerrainEntity ent = new CTerrainEntity(pos, rotation, entName, temp_terrain, temp_terrain.GetMaterials(content, "NA"));
                        return ent;
                    }

                case (int)EBasicEntityIDs.DotXTerrainEntity:
                    {
                        CDotXModel temp_modelr = new CDotXModel();
                        temp_modelr = (CDotXModel)temp_modelr.Initialise(content, "Model/platform", modelList);

                        return new CDotXTerrainEntity(pos, rotation, entName, temp_modelr, temp_modelr.GetMaterials(content, "Model/platform"));
                    }

                case (int)EBasicEntityIDs.DynamicDotXEntity:
                    {
                        CDotXModel dyndotxModel = new CDotXModel();
                        dyndotxModel = (CDotXModel)dyndotxModel.Initialise(content, "Model/platform", modelList);

                        CDynDotXEntity dynEnt = new CDynDotXEntity(pos, rotation, entName, dyndotxModel, dyndotxModel.GetMaterials(content, "platform"), false);

                        return dynEnt;

                    }

                case (int)EBasicEntityIDs.RoomEntity:
                    {
                        CDotXModel temp_modelport = new CDotXModel();
                        temp_modelport = (CDotXModel)temp_modelport.Initialise(content, "Model/platform", modelList);

                        CRoomEntity roomEnt = new CRoomEntity(pos, rotation, entName, temp_modelport,
                                   temp_modelport.GetMaterials(content, "platform"), false);

                        return roomEnt;
                    }


                case (int)EBasicEntityIDs.PortalEntity:
                    {
                        CWireFrameModel temp_model3 = new CWireFrameModel();
                        temp_model3 = (CWireFrameModel)temp_model3.Initialise(CModel.CreateAPortal(), modelList, "PortalModel0");

                        CPortalEntity roomEnt = new CPortalEntity(Vector3.One, pos, rotation, entName, temp_model3);
                        return roomEnt;
                    }

            }
#endif
            return null;
            
        }

        

    }
}

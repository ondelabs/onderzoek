﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Onderzoek.Camera;

using Onderzoek.Visual;
using Onderzoek.ModelData;

// Assistance from http://www.ziggyware.com/readarticle.php?article_id=122
namespace Onderzoek
{
    public class CSkyBox
    {
        protected TextureCube m_skyTex;
        protected Effect m_skyEffect;
        protected VertexDeclaration m_vertexDecl = null;
        protected VertexPositionTexture[] m_vertices = null;
        protected short[] m_indices = null;

#if EDITOR
        string m_textureCubeName = "";
#endif

        public CSkyBox(string SkyBoxEffect, string SkyBoxTexture, ContentManager Content)
        {
            m_skyEffect = Content.Load<Effect>(SkyBoxEffect);
            m_skyTex = Content.Load<TextureCube>(SkyBoxTexture);

#if EDITOR
            m_textureCubeName = SkyBoxTexture;
#endif

            m_vertexDecl = new VertexDeclaration(CVisual.Instance.GetGraphicsDevice(), VertexPositionTexture.VertexElements);

            Vector2 minus_one = Vector2.One * -1;
            Vector2 one = Vector2.One;

            m_vertices = new VertexPositionTexture[]
                    {
                        new VertexPositionTexture(
                            new Vector3(one.X, minus_one.Y ,0),
                            new Vector2(1,1)),
                        new VertexPositionTexture(
                            new Vector3(minus_one.X, minus_one.Y, 0),
                            new Vector2(0,1)),
                        new VertexPositionTexture(
                            new Vector3(minus_one.X, one.Y, 0),
                            new Vector2(0,0)),
                        new VertexPositionTexture(
                            new Vector3(one.X, one.Y, 0),
                            new Vector2(1,0))
                    };

            m_indices = new short[] { 0, 1, 2, 2, 3, 0 };
        }

        ~CSkyBox()
        {
            Dispose();
        }

#if EDITOR
        public void SetTextureCube(TextureCube SkyBox)
        {
            m_skyTex = SkyBox;
        }

        public string GetTextureCubeName()
        {
            return m_textureCubeName;
        }
#endif

        public void Dispose()
        {
            if (m_vertexDecl != null)
            {
                m_vertexDecl.Dispose();
            }
            m_vertexDecl = null;

            if (m_skyTex != null)
            {
                m_skyTex.Dispose();
            }
            m_skyTex = null;

            if (m_skyEffect != null)
            {
                m_skyEffect.Dispose();
            }
            m_skyEffect = null;

            m_vertices = null;
        }

        public void Render(CCamera Camera)
        {
            m_skyEffect.Parameters["Texture0"].SetValue(m_skyTex);
            m_skyEffect.Parameters["CameraTrans"].SetValue(Camera.GetRotationMatrix());
            m_skyEffect.Parameters["NormalTexture"].SetValue(CVisual.Instance.GetNormalTexture());
            m_skyEffect.Parameters["g_halfPixel"].SetValue(CVisual.Instance.HalfPixel2);

            m_skyEffect.Begin();

            m_skyEffect.Techniques[0].Passes[0].Begin();

            CVisual.Instance.GetGraphicsDevice().VertexDeclaration = m_vertexDecl;

            CVisual.Instance.GetGraphicsDevice().DrawUserIndexedPrimitives<VertexPositionTexture>
                (PrimitiveType.TriangleList, m_vertices, 0, 4, m_indices, 0, 2);

            m_skyEffect.Techniques[0].Passes[0].End();

            m_skyEffect.End();
        }

        public void SimpleRender(CCamera Camera)
        {
            m_skyEffect.Parameters["Texture0"].SetValue(m_skyTex);
            m_skyEffect.Parameters["CameraTrans"].SetValue(Camera.GetRotationMatrix());
            m_skyEffect.Parameters["g_halfPixel"].SetValue(CVisual.Instance.HalfPixel2);

            m_skyEffect.Begin();

            m_skyEffect.Techniques[0].Passes[1].Begin();

            CVisual.Instance.GetGraphicsDevice().VertexDeclaration = m_vertexDecl;

            CVisual.Instance.GetGraphicsDevice().DrawUserIndexedPrimitives<VertexPositionTexture>
                (PrimitiveType.TriangleList, m_vertices, 0, 4, m_indices, 0, 2);

            m_skyEffect.Techniques[0].Passes[1].End();

            m_skyEffect.End();
        }

        public static void LoadSkyBoxData(CEntityDataStore EntityData, Onderzoek.Screens.CGameScreenHelper CurScreen,
            ref Dictionary<string, CModel.SModelData> ModelList, ref ContentManager Content)
        {
            string tex_name = "";

            for (int i = 0; i < EntityData.m_properties.Count; ++i)
            {
                if (EntityData.m_properties[i].propertyName == "Sky_Box_Texture")
                {
                    tex_name = "Texture/" + EntityData.m_properties[i].propertyValue;
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
            }

            CurScreen.m_skyBox = new CSkyBox("fx/SkyBox", tex_name, Content);
        }
    }
}

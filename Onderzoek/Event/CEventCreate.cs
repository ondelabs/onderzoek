﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Onderzoek.Screens;
using Onderzoek.Entity;

namespace Onderzoek.Event
{
    public class CEventCreate : CEvent
    {
        public CEventCreate(CGameScreenHelper CurScreen)
        {
            base.Initialise(CurScreen);
        }

        public override void Dispose()
        {
            base.Dispose();
        }

        public override bool RunEvent(List<string> TriggeredByEntities, List<string> EffectedEntities, List<int> Entities)
        {
            if (TriggeredByEntities != null)
            {
                foreach (string triggered_by in TriggeredByEntities)
                {
                    foreach (int id in Entities)
                    {
                        CEntity entity = m_curScreen.GetEntity(id);
                        if (triggered_by.CompareTo(entity.GetEntityName()) == 0)
                        {
                            TriggerEvent(EffectedEntities);

                            return true;
                        }
                    }
                }
            }
            else
            {
                TriggerEvent(EffectedEntities);

                return true;
            }

            return false;
        }

        private void TriggerEvent(List<string> EffectedEntities)
        {
            foreach (string effected_entity in EffectedEntities)
            {
                m_curScreen.AddEntityToNewList(effected_entity);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Onderzoek.Screens;
using Onderzoek.Entity;

namespace Onderzoek.Event
{
    public class CEventReInit : CEvent
    {
        public CEventReInit(CGameScreenHelper CurScreen)
        {
            base.Initialise(CurScreen);
        }

        public override void Dispose()
        {
            base.Dispose();
        }

        public override bool RunEvent(List<string> TriggeredByEntities, List<string> AffectedEntities, List<int> Entities)
        {
            if (TriggeredByEntities != null)
            {
                foreach (string triggered_by in TriggeredByEntities)
                {
                    foreach (int id in Entities)
                    {
                        if (triggered_by.CompareTo(m_curScreen.GetEntity(id).GetEntityName()) == 0)
                        {
                            TriggerEvent(AffectedEntities);

                            return true;
                        }
                    }
                }
            }
            else
            {
                TriggerEvent(AffectedEntities);

                return true;
            }

            return false;
        }

        protected virtual void TriggerEvent(List<string> EffectedEntities)
        {
            foreach (string effected_entity in EffectedEntities)
            {
                CEntity entity = m_curScreen.GetEntity(effected_entity);
                if (entity != null)
                {
                    entity.Reinitialise();
                }
            }
        }
    }
}

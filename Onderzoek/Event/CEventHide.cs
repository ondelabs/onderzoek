﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Onderzoek.Screens;
using Onderzoek.Entity;

namespace Onderzoek.Event
{
    public class CEventHide : CEvent
    {
        public CEventHide(CGameScreenHelper CurScreen)
        {
            base.Initialise(CurScreen);
        }

        public override void Dispose()
        {
            base.Dispose();
        }

        public override bool RunEvent(List<string> TriggeredByEntities, List<string> EffectedEntities, List<int> Entities)
        {
            if (TriggeredByEntities != null)
            {
                foreach (string triggered_by in TriggeredByEntities)
                {
                    foreach (int id in Entities)
                    {
                        if (triggered_by.CompareTo(m_curScreen.GetEntity(id).GetEntityName()) == 0)
                        {
                            TriggerEvent(EffectedEntities);

                            return true;
                        }
                    }
                }
            }
            else
            {
                TriggerEvent(EffectedEntities);

                return true;
            }

            return false;
        }

        private void TriggerEvent(List<string> EffectedEntities)
        {
            foreach (string effected_entity in EffectedEntities)
            {
                CEntity entity = m_curScreen.GetEntity(effected_entity);
                if (entity != null)
                {
                    entity.Hide();
                }
            }
        }
    }
}

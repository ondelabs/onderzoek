﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Onderzoek.Screens;
using Onderzoek.Entity;

namespace Onderzoek.Event
{
    public abstract class CEvent
    {
        protected CGameScreenHelper m_curScreen;

        public void Initialise(CGameScreenHelper CurScreen)
        {
            m_curScreen = CurScreen;
        }

        public virtual void Dispose()
        {
            m_curScreen = null;
        }

        public virtual bool RunEvent(List<string> TriggeredByEntities, List<string> EffectedEntities, List<int> Entities)
        {
            return false;
        }

        public void AddEntityToDeleteList(CEntity Entity)
        {
            m_curScreen.AddEntityToDeleteList(Entity);
        }

        public void AddEntityToNewList(string Entity)
        {
            m_curScreen.AddEntityToNewList(Entity);
        }
    }
}

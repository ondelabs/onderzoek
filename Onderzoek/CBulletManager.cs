using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace TDFighter
{
    class CBulletManager
    {
        List<CBullet> m_bullets;
        int m_numAliveBullets;

        float m_firingRate = 0;
        float m_dt;

        Vector2 m_dirToRelativePosition;
        float m_angleOfRelativePosition;

        bool m_follow;
        public bool Follow
        {
            get { return m_follow; }
        }

        public CBulletManager(List<CBullet> Bullets, Vector2 RelativePosition, bool Follow, float FiringRate)
        {
            m_dirToRelativePosition = RelativePosition;
            m_angleOfRelativePosition = CToolbox.VectorToAngle(m_dirToRelativePosition);
            m_bullets = Bullets;

            if (m_bullets.Count > 0)
            {
                m_follow = Follow;
            }

            m_firingRate = FiringRate;
        }

        public void FireBullet(Vector2 Dir, Vector2 EntityPosition, CEntity EntityToFollow)
        {
            if (m_numAliveBullets < m_bullets.Count && m_dt >= m_firingRate)
            {
                ++m_numAliveBullets;
                m_bullets[m_numAliveBullets - 1].Activate(Dir, EntityPosition, m_dirToRelativePosition, m_angleOfRelativePosition, EntityToFollow);
                m_dt = 0;
            }
        }

        public void Update(float DT)
        {
            m_dt += DT;

            for (int i = 0; i < m_numAliveBullets; ++i)
            {
                if (m_bullets[i].IsActive == true)
                {
                    m_bullets[i].Update(DT);
                }
                else
                {
                    CBullet temp = m_bullets[i];
                    m_bullets[i] = m_bullets[--m_numAliveBullets];
                    m_bullets[m_numAliveBullets] = temp;
                }
            }
        }

        public void Render()
        {
            for (int i = 0; i < m_numAliveBullets; ++i)
            {
                m_bullets[i].Render();
            }
        }
    }
}

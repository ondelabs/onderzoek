﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace Onderzoek.Audio
{
    class CCue
    {
        public string m_cueName;
        public Cue m_cue;

        public virtual void Reinitalise(Cue NewCue, Matrix CueWorld, Matrix ListnerWorld)
        {
        }

        public virtual void Reinitalise(Cue NewCue)
        {
        }

        public virtual void Update(Matrix CueWorld, Matrix ListnerWorld)
        {
        }
    }
}

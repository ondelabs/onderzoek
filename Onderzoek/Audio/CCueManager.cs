﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Xml;
using System.IO;

using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;

namespace Onderzoek.Audio
{
    public class CCueManager
    {
        // I've removed the e_ because this is used to create a list of strings in which these are defined.
        // The strings in the list are then used to compare against the loaded in cue names. So it makes it
        // easier for the designer/user to not have to add e_ in the name of cues.
        public enum ECurScreen : uint
        {
            MainMenu,
            PauseMenu,
            Game,
            Global
        };

        List<string> m_cues;

        ECurScreen m_curScreen;
        List<ECurScreen> m_prvScreens;

        struct SAEAndSBIDs
        {
            public SAEAndSBIDs(int AEID, int SBID)
            {
                s_aeID = AEID;
                s_sbID = SBID;
            }

            public int s_aeID;
            public int s_sbID;
        }

        public struct SCueData
        {
            public SCueData(int ID, ECurScreen Screen)
            {
                s_id = ID;
                s_screen = Screen;
            }

            public int s_id;
            public ECurScreen s_screen;
        }

        // For singleton
        static CCueManager m_instance;

        public static CCueManager Instance
        {
            get { return m_instance; }
        }

        List<AudioEngine> m_audioEngines;
        List<SoundBank> m_soundBanks;
        Dictionary<string, SAEAndSBIDs> m_cueToAEAndSB;
        Dictionary<int, List<string>> m_sbToWB;
        Dictionary<int, List<int>> m_aeToSB;
        Dictionary<int, List<string>> m_aeToCues;
        Dictionary<ECurScreen, List<int>> m_screenEnumToAE;
        Dictionary<ECurScreen, List<WaveBank>> m_seToWaveBanks;
        Dictionary<ECurScreen, List<CCue>> m_seToBackgroundCues;
        Dictionary<ECurScreen, List<CCue>> m_screenEnumToEntityCues;
        Dictionary<ECurScreen, Dictionary<string, CCue>> m_screenEnumToCueNameTOCue;

        float m_musicVolume;
        float m_sfxVolume;

        public CCueManager()
        {
            m_curScreen = ECurScreen.MainMenu;
            m_prvScreens = new List<ECurScreen>();

            if (m_instance == null)
            {
                m_instance = this;
            }

            m_cues = new List<string>();

            m_audioEngines = new List<AudioEngine>();
            m_soundBanks = new List<SoundBank>();
            m_sbToWB = new Dictionary<int, List<string>>();
            m_cueToAEAndSB = new Dictionary<string, SAEAndSBIDs>();
            m_aeToSB = new Dictionary<int, List<int>>();
            m_aeToCues = new Dictionary<int, List<string>>();

            m_seToWaveBanks = new Dictionary<ECurScreen, List<WaveBank>>();
            m_seToWaveBanks[ECurScreen.Game] = new List<WaveBank>();
            m_seToWaveBanks[ECurScreen.MainMenu] = new List<WaveBank>();
            m_seToWaveBanks[ECurScreen.PauseMenu] = new List<WaveBank>();
            m_seToWaveBanks[ECurScreen.Global] = new List<WaveBank>();

            m_seToBackgroundCues = new Dictionary<ECurScreen, List<CCue>>();
            m_seToBackgroundCues[ECurScreen.Game] = new List<CCue>();
            m_seToBackgroundCues[ECurScreen.MainMenu] = new List<CCue>();
            m_seToBackgroundCues[ECurScreen.PauseMenu] = new List<CCue>();
            m_seToBackgroundCues[ECurScreen.Global] = new List<CCue>();

            m_screenEnumToAE = new Dictionary<ECurScreen, List<int>>();
            m_screenEnumToAE[ECurScreen.Game] = new List<int>();
            m_screenEnumToAE[ECurScreen.MainMenu] = new List<int>();
            m_screenEnumToAE[ECurScreen.PauseMenu] = new List<int>();
            m_screenEnumToAE[ECurScreen.Global] = new List<int>();

            m_screenEnumToEntityCues = new Dictionary<ECurScreen, List<CCue>>();
            m_screenEnumToEntityCues[ECurScreen.Game] = new List<CCue>();
            m_screenEnumToEntityCues[ECurScreen.MainMenu] = new List<CCue>();
            m_screenEnumToEntityCues[ECurScreen.PauseMenu] = new List<CCue>();
            m_screenEnumToEntityCues[ECurScreen.Global] = new List<CCue>();

            m_screenEnumToCueNameTOCue = new Dictionary<ECurScreen, Dictionary<string, CCue>>();
            m_screenEnumToCueNameTOCue[ECurScreen.Game] = new Dictionary<string, CCue>();
            m_screenEnumToCueNameTOCue[ECurScreen.MainMenu] = new Dictionary<string, CCue>();
            m_screenEnumToCueNameTOCue[ECurScreen.PauseMenu] = new Dictionary<string, CCue>();
            m_screenEnumToCueNameTOCue[ECurScreen.Global] = new Dictionary<string, CCue>();

            // Get all the xml file names
            string[] cue_files;
            try
            {
                cue_files = Directory.GetFiles("Content/xml/Audio");
            }
            catch (Exception e)
            {
                return;
            }

            List<string> cue_names = new List<string>();
            List<string> cue_name_Orig = new List<string>();

            for (int i = 0; i < cue_files.Length; ++i)
            {
                Dictionary<string, List<string>> sbs_cues = new Dictionary<string, List<string>>();
                Dictionary<string, List<string>> sbs_wb = new Dictionary<string, List<string>>();

                string ae_name = cue_files[i].Replace("Content/xml/Audio", "Content/Audio");
                ae_name = ae_name.Replace("xml", "xgs");
                // Create a new audio engine.
                m_audioEngines.Add(new AudioEngine(ae_name));

                // Save the audio engine id
                m_aeToSB.Add(m_audioEngines.Count - 1, new List<int>());
                m_aeToCues.Add(m_audioEngines.Count - 1, new List<string>());

                CDataFromXML.GetCuesDataFromXML(cue_files[i], ref sbs_cues, ref sbs_wb);

                foreach (KeyValuePair<string, List<string>> sb_cue in sbs_cues)
                {
                    foreach (string cue_name in sb_cue.Value)
                    {
                        m_cues.Add(cue_name);
                    }
                }

                string screen_name = cue_files[i].Replace("Content/xml/Audio", "");
                screen_name = screen_name.Replace(".xml", "");
                screen_name = screen_name.Replace("\\", "");
                screen_name = screen_name.Replace("/", "");

                foreach (KeyValuePair<string, List<string>> sb_cues in sbs_cues)
                {
                    // Create new sound banks.
                    m_soundBanks.Add(new SoundBank(m_audioEngines[m_audioEngines.Count - 1], "Content/Audio/" + sb_cues.Key + ".xsb"));

                    // save the sound bank id in m_aeToSB.
                    m_aeToSB[m_audioEngines.Count - 1].Add(m_soundBanks.Count-1);

                    // Save the name of the wave banks with respect to the sound banks.
                    List<string> wb;
                    sbs_wb.TryGetValue(sb_cues.Key, out wb);
                    m_sbToWB.Add(m_soundBanks.Count - 1, wb);

                    // Create new cues with respect to the sound banks.
                    foreach (string cues in sb_cues.Value)
                    {
                        if (m_cueToAEAndSB.ContainsKey(cues) == true)
                        {
                            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CCueManger", "Cue with same name has been found.");
                            //m_cueToAEAndSB.Add(screen_name + cues, new SAEAndSBIDs(m_audioEngines.Count - 1, m_soundBanks.Count - 1));
                        }
                        else
                        {
                            m_cueToAEAndSB.Add(cues, new SAEAndSBIDs(m_audioEngines.Count - 1, m_soundBanks.Count - 1));

                            m_aeToCues[m_audioEngines.Count - 1].Add(cues);

                            // We will add the screen name to the cues here so that when the cues are being sorted in SortCuesWithRespectToECueAndECurScreen,
                            // the cues will get sorted with respect to what screen they're associated with.
                            cue_names.Add(screen_name + "." + cues);
                            cue_name_Orig.Add(cues);
                        }
                    }
                }
            }

            SortAudioEnginesWithRespectToScreens(cue_files);
            SortCuesWithRespectToECueAndECurScreen(cue_names, cue_name_Orig);

            // Get the volume data and then set it to all the audio engines.
            m_musicVolume = CDataFromXML.GetMusicVolume();
            m_sfxVolume = CDataFromXML.GetSFXVolume();

            SetVolumeForCategory("Music", m_musicVolume);
            SetVolumeForCategory("Default", m_sfxVolume);
        }

        public float MusicVolume
        {
            get { return m_musicVolume; }
            set
            {
                m_musicVolume = value;

                m_musicVolume = CToolbox.Clamp(m_musicVolume, 0, 1.0f);

                SetVolumeForCategory("Music", m_musicVolume);
            }
        }

        public float SFXVolume
        {
            get { return m_sfxVolume; }
            set
            {
                m_sfxVolume = value;

                m_sfxVolume = CToolbox.Clamp(m_sfxVolume, 0, 1.0f);

                SetVolumeForCategory("Default", m_sfxVolume);
            }
        }

        private void SetVolumeForCategory(string Category, float Volume)
        {
            foreach (AudioEngine ae in m_audioEngines)
            {
                ae.GetCategory(Category).SetVolume(Volume);
            }
        }

        private void SortAudioEnginesWithRespectToScreens(string[] AudioEngineFiles)
        {
            for (int i = 0; i < AudioEngineFiles.Length; ++i)
            {
                string ae_name = AudioEngineFiles[i].Replace("Content/xml/Audio", "");
                ae_name = ae_name.Replace(".xml", "");
                ae_name = ae_name.Replace("\\", "");
                ae_name = ae_name.Replace("/", "");

                if (ae_name.Contains("MainMenu") == true)
                {
                    m_screenEnumToAE[ECurScreen.MainMenu].Add(i);
                }
                else if (ae_name.Contains("PauseMenu") == true)
                {
                    m_screenEnumToAE[ECurScreen.PauseMenu].Add(i);
                }
                else if (ae_name.Contains("Game") == true)
                {
                    m_screenEnumToAE[ECurScreen.Game].Add(i);
                }
                else // This means that the audio engine will always play this.
                {
                    LoadAudioForNewScreen(i);
                }
            }
        }

#if XBOX360
        string GetNameOfECurScreen(int Index)
        {
            switch (Index)
            {
                case 0:
                    return "MainMenu";
                case 1:
                    return "PauseMenu";
                case 2:
                    return "Game";
                case 3:
                    return "Global";
                default:
                    return "Global";
            }
        }

        ECurScreen GetECurScreenEnum(int Index)
        {
            switch (Index)
            {
                case 0:
                    return ECurScreen.MainMenu;
                case 1:
                    return ECurScreen.PauseMenu;
                case 2:
                    return ECurScreen.Game;
                case 3:
                    return ECurScreen.Global;
                default:
                    return ECurScreen.Global;
            }
        }
#endif

        public void SortCuesWithRespectToECueAndECurScreen(List<string> CueNames, List<string> CueNamesOrig)
        {
            // First we need to load all the wavebanks so that we can initialise the cues.
#if WINDOWS
            foreach (ECurScreen screen_enum in Enum.GetValues(typeof(ECurScreen)))
            {
#else
            for( int ics = 0; ics < 4; ics++ )
            {
                ECurScreen screen_enum = GetECurScreenEnum(ics);
#endif
                for (int i = 0; i < m_screenEnumToAE[screen_enum].Count; ++i)
                {
                    int it = m_screenEnumToAE[screen_enum][i];

                    for (int j = 0; j < m_aeToSB[it].Count; ++j)
                    {
                        for (int k = 0; k < m_sbToWB[m_aeToSB[it][j]].Count; ++k)
                        {
                            string wave_bank = "Content/Audio/" + m_sbToWB[m_aeToSB[it][j]][k] + ".xwb";
                            m_seToWaveBanks[screen_enum].Add(new WaveBank(m_audioEngines[it], wave_bank));
                        }
                    }
                }
            }

            // Now to sort the cues out.
            foreach (string ecue_name in m_cues)
            {
                for(int i = 0; i < CueNames.Count; ++i )
                {
                    string cue_name = CueNames[i];
                    string cue_name_orig = CueNamesOrig[i];

                    if (cue_name.Contains(ecue_name) == true)
                    {
#if WINDOWS
                        foreach (ECurScreen screen_enum in Enum.GetValues(typeof(ECurScreen)))
                        {
                            string screen_name = Enum.GetName(typeof(ECurScreen), screen_enum);
#else
                        for( int ics = 0; ics < 4; ics++ )
                        {
                            ECurScreen screen_enum = GetECurScreenEnum(ics);
                            string screen_name = GetNameOfECurScreen(ics);
#endif
                            if (cue_name.Contains(screen_name) == true)
                            {
                                if (cue_name.Contains("3D") == true)
                                {
                                    //m_screenEnumToCueNameTOCue[screen_enum].Add(ecue_name, new C3DCue(m_soundBanks[m_cueToAEAndSB[cue_name_orig].s_sbID].GetCue(cue_name_orig), Matrix.Identity, Matrix.Identity, cue_name_orig, false));
                                }
                                else
                                {
                                    m_screenEnumToCueNameTOCue[screen_enum].Add(ecue_name, new CLocalCue(m_soundBanks[m_cueToAEAndSB[cue_name_orig].s_sbID].GetCue(cue_name_orig), cue_name_orig, false));
                                }

                                CueNames.Remove(cue_name);
                                CueNamesOrig.Remove(cue_name_orig);
                                --i;
                                break;
                            }
                        }
                    }
                }
            }

            // Now we will unload all the wavebanks.
#if WINDOWS
            foreach (ECurScreen screen_enum in Enum.GetValues(typeof(ECurScreen)))
            {
#else
            for( int ics = 0; ics < 4; ics++ )
            {
                ECurScreen screen_enum = GetECurScreenEnum(ics);
#endif
                for (int i = 0; i < m_seToWaveBanks[screen_enum].Count; ++i)
                {
                    m_seToWaveBanks[screen_enum][i].Dispose();
                    m_seToWaveBanks[screen_enum][i] = null;
                }
                m_seToWaveBanks[screen_enum].Clear();
            }
        }

        public void AddOverlappingScreen(ECurScreen ScreenEnum)
        {
            for (int j = 0; j < m_screenEnumToEntityCues[m_curScreen].Count; ++j)
            {
                if (m_screenEnumToEntityCues[m_curScreen][j].m_cue.IsPlaying == true)
                {
                    m_screenEnumToEntityCues[m_curScreen][j].m_cue.Pause();
                }
            }

            for (int i = 0; i < m_seToBackgroundCues[m_curScreen].Count; ++i)
            {
                if (m_seToBackgroundCues[m_curScreen][i].m_cue.IsPlaying == true)
                {
                    m_seToBackgroundCues[m_curScreen][i].m_cue.Pause();
                }
            }

            foreach (KeyValuePair<string, CCue> ecue_cue in m_screenEnumToCueNameTOCue[m_curScreen])
            {
                if (ecue_cue.Value.m_cue.IsPlaying == true)
                {
                    ecue_cue.Value.m_cue.Pause();
                }
            }

            m_prvScreens.Add(m_curScreen);
            m_curScreen = ScreenEnum;

            LoadAudioForNewScreen();
        }

        public void RemoveOverlappingScreen()
        {
            if (m_prvScreens.Count < 0)
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CCueManger", "Cannot remove an overaly screen audio data aas they're doesn't seem to be any underlying screens.");
                return;
            }

            foreach (KeyValuePair<string, CCue> ecue_cue in m_screenEnumToCueNameTOCue[m_curScreen])
            {
                if (ecue_cue.Value.m_cue.IsPlaying == true)
                {
                    ecue_cue.Value.m_cue.Stop(AudioStopOptions.Immediate);
                }
            }

            for (int j = 0; j < m_screenEnumToEntityCues[m_curScreen].Count; ++j)
            {
                if (m_screenEnumToEntityCues[m_curScreen][j].m_cue.IsPlaying == true)
                {
                    m_screenEnumToEntityCues[m_curScreen][j].m_cue.Stop(AudioStopOptions.Immediate);
                }
                m_screenEnumToEntityCues[m_curScreen][j] = null;
            }
            m_screenEnumToEntityCues[m_curScreen].Clear();

            for (int i = 0; i < m_seToBackgroundCues[m_curScreen].Count; ++i)
            {
                if (m_seToBackgroundCues[m_curScreen][i].m_cue.IsStopped != true)
                {
                    m_seToBackgroundCues[m_curScreen][i].m_cue.Stop(AudioStopOptions.Immediate);
                }
                m_seToBackgroundCues[m_curScreen][i] = null;
            }
            m_seToBackgroundCues[m_curScreen].Clear();

            for (int i = 0; i < m_seToWaveBanks[m_curScreen].Count; ++i)
            {
                m_seToWaveBanks[m_curScreen][i].Dispose();
                m_seToWaveBanks[m_curScreen][i] = null;
            }
            m_seToWaveBanks[m_curScreen].Clear();

            m_curScreen = m_prvScreens[m_prvScreens.Count-1];
            m_prvScreens.RemoveAt(m_prvScreens.Count - 1);

            ResumePreviousScreenCues();
        }

        public void ScreenHasChanged(ECurScreen ScreenEnum)
        {
            // Fill the current screen into the previous screen list.
            m_prvScreens.Add(m_curScreen);

            // Now we can remove all the previoius screen in one go.
            foreach (ECurScreen cur_scrn in m_prvScreens)
            {
                foreach (KeyValuePair<string, CCue> ecue_cue in m_screenEnumToCueNameTOCue[cur_scrn])
                {
                    if (ecue_cue.Value.m_cue.IsPlaying == true)
                    {
                        ecue_cue.Value.m_cue.Stop(AudioStopOptions.Immediate);
                    }
                }

                for (int j = 0; j < m_screenEnumToEntityCues[cur_scrn].Count; ++j)
                {
                    if (m_screenEnumToEntityCues[cur_scrn][j].m_cue.IsPlaying == true)
                    {
                        m_screenEnumToEntityCues[cur_scrn][j].m_cue.Stop(AudioStopOptions.Immediate);
                    }
                    m_screenEnumToEntityCues[cur_scrn][j] = null;
                }
                m_screenEnumToEntityCues[cur_scrn].Clear();

                for (int i = 0; i < m_seToBackgroundCues[cur_scrn].Count; ++i)
                {
                    if (m_seToBackgroundCues[cur_scrn][i].m_cue.IsStopped != true)
                    {
                        m_seToBackgroundCues[cur_scrn][i].m_cue.Stop(AudioStopOptions.Immediate);
                    }
                    m_seToBackgroundCues[cur_scrn][i] = null;
                }
                m_seToBackgroundCues[cur_scrn].Clear();

                for (int i = 0; i < m_seToWaveBanks[cur_scrn].Count; ++i)
                {
                    m_seToWaveBanks[cur_scrn][i].Dispose();
                    m_seToWaveBanks[cur_scrn][i] = null;
                }
                m_seToWaveBanks[cur_scrn].Clear();
            }

            // Reset the previous screen list.
            m_prvScreens.Clear();

            // Set the new screen enum.
            m_curScreen = ScreenEnum;

            // Load the corresponding screen's audio data.
            LoadAudioForNewScreen();
        }

        private void ResumePreviousScreenCues()
        {
            foreach (KeyValuePair<string, CCue> ecue_cue in m_screenEnumToCueNameTOCue[m_curScreen])
            {
                if (ecue_cue.Value.m_cue.IsPaused == true)
                {
                    ecue_cue.Value.m_cue.Resume();
                }
            }

            for (int j = 0; j < m_screenEnumToEntityCues[m_curScreen].Count; ++j)
            {
                if (m_screenEnumToEntityCues[m_curScreen][j].m_cue.IsPaused == true)
                {
                    m_screenEnumToEntityCues[m_curScreen][j].m_cue.Resume();
                }
            }

            for (int j = 0; j < m_seToBackgroundCues[m_curScreen].Count; ++j)
            {
                if (m_seToBackgroundCues[m_curScreen][j].m_cue.IsPaused == true)
                {
                    m_seToBackgroundCues[m_curScreen][j].m_cue.Resume();
                }
            }
        }

        private void LoadAudioForNewScreen()
        {
            for (int i = 0; i < m_screenEnumToAE[m_curScreen].Count; ++i)
            {
                int it = m_screenEnumToAE[m_curScreen][i];

                LoadAudioForNewScreen(it);
            }
        }

        private void LoadAudioForNewScreen(int i)
        {
            for (int j = 0; j < m_aeToSB[i].Count; ++j)
            {
                for (int k = 0; k < m_sbToWB[m_aeToSB[i][j]].Count; ++k)
                {
                    string wave_bank = "Content/Audio/" + m_sbToWB[m_aeToSB[i][j]][k] + ".xwb";
                    m_seToWaveBanks[m_curScreen].Add(new WaveBank(m_audioEngines[i], wave_bank));
                }
            }

            for (int j = 0; j < m_aeToCues[i].Count; ++j)
            {
                if (m_aeToCues[i][j].Contains("Background") == true)
                {
                    m_seToBackgroundCues[m_curScreen].Add(new CLocalCue(m_soundBanks[m_cueToAEAndSB[m_aeToCues[i][j]].s_sbID].GetCue(m_aeToCues[i][j]), m_aeToCues[i][j]));
                }
            }
        }

        public void Dispose()
        {
            if (m_instance != null)
            {
                m_instance = null;
            }
        }

        public SCueData InitialiseEntityCue(string CueName, Matrix CueWorld, Matrix ListnerWorld, bool PlayNow)
        {
            if (CreateEntityCCue(CueName, CueWorld, ListnerWorld, PlayNow) == false)
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CCueManager", "Cannot initialise " + CueName);
                return new SCueData(-1, m_curScreen);
            }

            return new SCueData(m_screenEnumToEntityCues[m_curScreen].Count - 1, m_curScreen);
        }

        private bool CreateEntityCCue(string CueName, Matrix CueWorld, Matrix ListnerWorld, bool PlayNow)
        {
            if (m_cueToAEAndSB.ContainsKey(CueName) == false)
            {
                return false;
            }

            if (CueName.Contains("3D") == true)
            {
                m_screenEnumToEntityCues[m_curScreen].Add(new C3DCue(m_soundBanks[m_cueToAEAndSB[CueName].s_sbID].GetCue(CueName), CueWorld, ListnerWorld, CueName, PlayNow));
            }
            else// if (CueName.Contains("Background") == true)
            {
                m_screenEnumToEntityCues[m_curScreen].Add(new CLocalCue(m_soundBanks[m_cueToAEAndSB[CueName].s_sbID].GetCue(CueName), CueName, PlayNow));
            }

            return true;
        }

        public void UpdateEntityCue(Matrix CueWorld, Matrix ListnerWorld, SCueData CueID)
        {
            m_screenEnumToEntityCues[CueID.s_screen][CueID.s_id].Update(CueWorld, ListnerWorld);
        }

        /// <summary>
        /// This will play the specified cue only when it's isn't already playing.
        /// </summary>
        /// <param name="CueName">The name of the cue.</param>
        /// <returns>return true if the cue was found.</returns>
        public bool PlayCue(string CueName)
        {
            CCue cue_to_play;
            if (m_screenEnumToCueNameTOCue[m_curScreen].TryGetValue(CueName, out cue_to_play) == false)
            {
                return false;
            }

            if (cue_to_play.m_cue.IsDisposed == true || cue_to_play.m_cue.IsPlaying == false)
            {
                if (cue_to_play.m_cue.IsStopping == true)
                {
                    cue_to_play.m_cue.Play();
                }
                else
                {
                    cue_to_play.Reinitalise(m_soundBanks[m_cueToAEAndSB[cue_to_play.m_cueName].s_sbID].GetCue(cue_to_play.m_cueName));
                }
            }

            return true;
        }

        /// <summary>
        /// This will play another cue of the same type if one i already playing.
        /// </summary>
        /// <param name="CueName">The name of the cue.</param>
        /// <returns>return true if the cue was found.</returns>
        public bool PlayAnotherCue(string CueName)
        {
            CCue cue_to_play;
            if (m_screenEnumToCueNameTOCue[m_curScreen].TryGetValue(CueName, out cue_to_play) == false)
            {
                return false;
            }

            if (cue_to_play.m_cue.IsDisposed == true || cue_to_play.m_cue.IsPlaying == false)
            {
                if (cue_to_play.m_cue.IsStopping == true)
                {
                    cue_to_play.m_cue.Play();
                }
                else
                {
                    cue_to_play.Reinitalise(m_soundBanks[m_cueToAEAndSB[cue_to_play.m_cueName].s_sbID].GetCue(cue_to_play.m_cueName));
                }
            }
            else if (cue_to_play.m_cue.IsPlaying == true)
            {
                Cue temp = m_soundBanks[m_cueToAEAndSB[cue_to_play.m_cueName].s_sbID].GetCue(cue_to_play.m_cueName);
                temp.Play();
            }

            return true;
        }

        public bool RestartCue(string CueName)
        {
            CCue cue_to_play;
            if (m_screenEnumToCueNameTOCue[m_curScreen].TryGetValue(CueName, out cue_to_play) == false)
            {
                return false;
            }

            if (cue_to_play.m_cue.IsDisposed != true)
            {
                if (cue_to_play.m_cue.IsPlaying == true)
                {
                    cue_to_play.m_cue.Stop(AudioStopOptions.Immediate);
                }
            }

            if (cue_to_play.m_cue.IsStopping == true)
            {
                //cue_to_play.m_cue.Play();
                cue_to_play.Reinitalise(m_soundBanks[m_cueToAEAndSB[cue_to_play.m_cueName].s_sbID].GetCue(cue_to_play.m_cueName));
            }
            else
            {
                cue_to_play.Reinitalise(m_soundBanks[m_cueToAEAndSB[cue_to_play.m_cueName].s_sbID].GetCue(cue_to_play.m_cueName));
            }

            return true;
        }

        public void PlayEntityCue(SCueData CueID)
        {
            if (CueID.s_screen != m_curScreen)
            {
                return;
            }

            if (m_screenEnumToEntityCues[CueID.s_screen][CueID.s_id].m_cue.IsPlaying == false)
            {
                if (m_screenEnumToEntityCues[CueID.s_screen][CueID.s_id].m_cue.IsDisposed == true ||
                    m_screenEnumToEntityCues[CueID.s_screen][CueID.s_id].m_cue.IsStopped == true)
                {
                    m_screenEnumToEntityCues[CueID.s_screen][CueID.s_id].Reinitalise
                        (m_soundBanks[m_cueToAEAndSB[m_screenEnumToEntityCues[CueID.s_screen][CueID.s_id].m_cueName].s_sbID].GetCue
                        (m_screenEnumToEntityCues[CueID.s_screen][CueID.s_id].m_cueName));
                }
                else
                {
                    m_screenEnumToEntityCues[CueID.s_screen][CueID.s_id].m_cue.Play();
                }
            }
            else if (m_screenEnumToEntityCues[CueID.s_screen][CueID.s_id].m_cue.IsPlaying == true)
            {
                m_screenEnumToEntityCues[CueID.s_screen][CueID.s_id].m_cue.Stop(AudioStopOptions.Immediate);

                m_screenEnumToEntityCues[CueID.s_screen][CueID.s_id].Reinitalise
                    (m_soundBanks[m_cueToAEAndSB[m_screenEnumToEntityCues[CueID.s_screen][CueID.s_id].m_cueName].s_sbID].GetCue
                    (m_screenEnumToEntityCues[CueID.s_screen][CueID.s_id].m_cueName));
            }
        }

        public void PauseEntityCue(SCueData CueID)
        {
            if (m_screenEnumToEntityCues[CueID.s_screen][CueID.s_id].m_cue.IsPaused == false)
            {
                if (m_screenEnumToEntityCues[CueID.s_screen][CueID.s_id].m_cue.IsStopped == false)
                {
                    m_screenEnumToEntityCues[CueID.s_screen][CueID.s_id].m_cue.Pause();
                }
            }
        }

        public void ResumeEntityCue(SCueData CueID)
        {
            if (CueID.s_screen != m_curScreen)
            {
                return;
            }

            if (m_screenEnumToEntityCues[CueID.s_screen][CueID.s_id].m_cue.IsPaused == true)
            {
                m_screenEnumToEntityCues[CueID.s_screen][CueID.s_id].m_cue.Resume();
            }
        }

        public void StopEntityCueImmediately(SCueData CueID)
        {
            if (m_screenEnumToEntityCues[CueID.s_screen][CueID.s_id].m_cue.IsStopped == false)
            {
                m_screenEnumToEntityCues[CueID.s_screen][CueID.s_id].m_cue.Stop(AudioStopOptions.Immediate);
            }
        }

        public void StopEntityCueAsAuthord(SCueData CueID)
        {
            if (CueID.s_screen != m_curScreen)
            {
                return;
            }

            if (m_screenEnumToEntityCues[CueID.s_screen][CueID.s_id].m_cue.IsStopped == false && m_screenEnumToEntityCues[CueID.s_screen][CueID.s_id].m_cue.IsStopping == false)
            {
                m_screenEnumToEntityCues[CueID.s_screen][CueID.s_id].m_cue.Stop(AudioStopOptions.AsAuthored);
            }
        }

        public void Update()
        {
            foreach (AudioEngine ae in m_audioEngines)
            {
                ae.Update();
            }
        }
    }
}

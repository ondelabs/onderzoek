﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace Onderzoek.Audio
{
    class C3DCue : CCue
    {
        AudioEmitter m_emit;
        AudioListener m_listen;

        Vector3 m_prvPos;

        public C3DCue(Cue NewCue, Matrix CueWorld, Matrix ListnerWorld, string CueName)
        {
            m_cue = NewCue;
            m_cueName = CueName;

            m_emit = new AudioEmitter();
            m_emit.Position = CueWorld.Translation;
            m_emit.Forward = Vector3.Forward;
            m_emit.Up = Vector3.Up;
            m_listen = new AudioListener();
            m_listen.Position = ListnerWorld.Translation;
            m_listen.Up = ListnerWorld.Up;
            m_listen.Forward = ListnerWorld.Forward;

            m_cue.Apply3D(m_listen, m_emit);
            m_cue.Play();
        }

        public C3DCue(Cue NewCue, Matrix CueWorld, Matrix ListnerWorld, string CueName, bool Play)
        {
            m_cue = NewCue;
            m_cueName = CueName;

            m_emit = new AudioEmitter();
            m_emit.Position = CueWorld.Translation;
            m_emit.Forward = Vector3.Forward;
            m_emit.Up = Vector3.Up;
            m_listen = new AudioListener();
            m_listen.Position = ListnerWorld.Translation;
            m_listen.Up = ListnerWorld.Up;
            m_listen.Forward = ListnerWorld.Forward;

            m_cue.Apply3D(m_listen, m_emit);
            if (Play == true)
            {
                m_cue.Play();
            }
        }

        public override void Reinitalise(Cue NewCue, Matrix CueWorld, Matrix ListnerWorld)
        {
            m_cue = NewCue;

            m_emit = new AudioEmitter();
            m_emit.Position = CueWorld.Translation;
            m_emit.Forward = Vector3.Forward;
            m_emit.Up = Vector3.Up;
            m_listen = new AudioListener();
            m_listen.Position = ListnerWorld.Translation;
            m_listen.Up = ListnerWorld.Up;
            m_listen.Forward = ListnerWorld.Forward;

            m_cue.Apply3D(m_listen, m_emit);
            m_cue.Play();
        }

        public override void Reinitalise(Cue NewCue)
        {
            m_cue = NewCue;

            m_cue.Apply3D(m_listen, m_emit);
            m_cue.Play();
        }

        public override void Update(Matrix CueWorld, Matrix ListnerWorld)
        {
            if (m_prvPos != CueWorld.Translation)
            {
                m_emit.Position = CueWorld.Translation;
                m_emit.Forward = CueWorld.Forward;
                m_emit.Up = CueWorld.Up;

                m_prvPos = CueWorld.Translation;
            }

            m_listen.Position = ListnerWorld.Translation;
            m_listen.Up = ListnerWorld.Up;
            m_listen.Forward = ListnerWorld.Forward;

            if (m_cue.IsPlaying == true)
            {
                m_cue.Apply3D(m_listen, m_emit);
            }
        }
    }
}

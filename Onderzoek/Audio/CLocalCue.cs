﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace Onderzoek.Audio
{
    class CLocalCue : CCue
    {
        public CLocalCue(Cue NewCue, string CueName)
        {
            m_cue = NewCue;
            m_cueName = CueName;

            m_cue.Play();
        }

        public CLocalCue(Cue NewCue, string CueName, bool Play)
        {
            m_cue = NewCue;
            m_cueName = CueName;

            if (Play == true)
            {
                m_cue.Play();
            }
        }

        public override void Reinitalise(Cue NewCue)
        {
            m_cue = NewCue;

            m_cue.Play();
        }
    }
}

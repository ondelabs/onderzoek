﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using Onderzoek.Visual;

namespace Onderzoek.GUI
{
    public abstract class CHUD
    {
        protected Vector2 m_position;
        protected float m_size;
        protected int m_texID;
        protected Rectangle m_rect;
        protected string m_texName;

        public bool m_selectable;
        public bool m_selected;
        public bool m_hover = false;

        public virtual Vector2 Position
        {
            get
            {
                return m_position;
            }
            set
            {
                m_position = value;
            }
        }

        public float Size
        {
            get
            {
                return m_size;
            }
            set
            {
                m_size = value;
            }
        }

        public virtual void Reset()
        {
        }

        protected virtual void FindPosOnly( string Text )
        {
            int width = (int)CVisual.Instance.GetWidthRes();
            int height = (int)CVisual.Instance.GetHeightRes();

            m_rect.X = ((int)m_position.X * width) / 100;
            m_rect.Y = ((int)m_position.Y * height) / 100;

            //// Convert m_size from the percentage value to the multiplier value.
            //Vector2 text_size = CVisual.Instance.GetStringSize(Text);

            //float x_perc = (text_size.X / width) * 100.0f;
            //float x = m_size * (text_size.X / x_perc);

            //m_size = x / text_size.X;
        }

        protected void FindRect()
        {
            int width = (int)CVisual.Instance.GetWidthRes();
            int height = (int)CVisual.Instance.GetHeightRes();

            m_rect.X = (int)((m_position.X * (float)width) / 100.0f);
            m_rect.Y = (int)((m_position.Y * (float)height) / 100.0f);

            // Keep the aspect ratio of the texture
            Vector2 tex_res = CVisual.Instance.GetHudTextureResolution(m_texID);

            m_rect.Height = ((int)m_size * height) / 100;
            m_rect.Width = (int)((float)m_rect.Height * (tex_res.X / tex_res.Y));
        }

        protected void FindRect(ref Rectangle Rect, float Size, int TexID, Vector2 Position)
        {
            int width = (int)CVisual.Instance.GetWidthRes();
            int height = (int)CVisual.Instance.GetHeightRes();

            Rect.X = (int)((m_position.X * (float)width) / 100.0f);
            Rect.Y = (int)((m_position.Y * (float)height) / 100.0f);

            // Keep the aspect ratio of the texture
            Vector2 tex_res = CVisual.Instance.GetHudTextureResolution(TexID);

            Rect.Height = ((int)Size * height) / 100;
            Rect.Width = (int)((float)Rect.Height * (tex_res.X / tex_res.Y));
        }

        protected void FindRect( Vector2 Size )
        {
            int width = (int)CVisual.Instance.GetWidthRes();
            int height = (int)CVisual.Instance.GetHeightRes();

            m_rect.X = (int)((m_position.X * (float)width) / 100.0f);
            m_rect.Y = (int)((m_position.Y * (float)height) / 100.0f);

            // Keep the aspect ratio of the texture
            Vector2 tex_res = CVisual.Instance.GetHudTextureResolution(m_texID);

            m_rect.Height = ((int)Size.Y * height) / 100;
            m_rect.Width = ((int)Size.X * width) / 100;
        }

        public Rectangle Rect
        {
            get { return m_rect; }
        }

        public string GetTexName()
        {
            return m_texName;
        }

        public void SetTexID(int TexID)
        {
            m_texID = TexID;
        }

        public virtual bool Update(GameTime DT, Onderzoek.Input.CInputData InputData)
        {
            return false;
        }

        public virtual void SetNewPosition(Vector2 Position)
        {
        }

        public virtual void Render()
        {
        }

        public virtual void FreeMemory()
        {
            CVisual.Instance.FreeHudMemory(m_texID, m_texName);
        }
    }
}

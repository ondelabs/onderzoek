﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Onderzoek.Visual;

namespace Onderzoek.GUI
{
    public class CHUDButton : CHUD
    {
        public event EventHandler<CPlayerIndexEventArgs> m_event;
        Vector2 m_vecSize;

        public CHUDButton(int TexID, string TexName, Vector2 Position, float Size)
        {
            m_texName = TexName;
            m_selectable = true;
            m_selected = false;
            m_texID = TexID;
            m_position = Position;
            m_size = Size;

            m_rect = new Rectangle();

            FindRect();
        }

        public CHUDButton(int TexID, string TexName, Vector2 Position, Vector2 Size)
        {
            m_texName = TexName;
            m_selectable = true;
            m_selected = false;
            m_texID = TexID;
            m_position = Position;
            m_vecSize = Size;

            m_rect = new Rectangle();

            FindRect( Size );
        }

        public override void Reset()
        {
            if (m_size != 0)
            {
                FindRect();
            }
            else
            {
                FindRect(m_vecSize);
            }
        }

        public override void SetNewPosition(Vector2 Position)
        {
            m_position = Position;

            FindRect();
        }

        public override bool Update(GameTime DT, Onderzoek.Input.CInputData InputData)
        {
            if (m_selected == true)
            {
                if (InputData.m_enter == true)
                {
                    InputData.m_enter = false;
                    if (m_event != null)
                    {
                        m_event(this, new CPlayerIndexEventArgs(PlayerIndex.One));
                        return true;
                    }
                }
            }

            return false;
        }

        public override void Render()
        {
            if (m_hover == true)
            {
                CVisual.Instance.RenderHudElement(m_texID, m_rect, Color.Orange);
            }
            else if (m_selected == true)
            {
                CVisual.Instance.RenderHudElement(m_texID, m_rect, Color.LightGray);
            }
            else
            {
                CVisual.Instance.RenderHudElement(m_texID, m_rect);
            }
        }
    }
}

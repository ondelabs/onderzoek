﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

using Onderzoek.Visual;

namespace Onderzoek.GUI
{
    public class CHUDButtonChar : CHUD
    {
        public event EventHandler<CPlayerIndexEventArgs> m_event;
        protected string m_text;
        protected Color? m_colour;

        public CHUDButtonChar(string Text, Vector2 Position, float Size, Color? Colour)
        {
            m_text = Text;
            m_selectable = true;
            m_selected = false;
            m_position = Position;
            m_size = Size;
            m_colour = Colour;

            m_rect = new Rectangle();

            FindPosOnly( m_text );
        }

        public override void Reset()
        {
            FindPosOnly(m_text);
        }

        protected override void FindPosOnly(string Text)
        {
            int width = (int)CVisual.Instance.GetWidthRes();
            int height = (int)CVisual.Instance.GetHeightRes();

            m_rect.X = ((int)m_position.X * width) / 100;
            m_rect.Y = ((int)m_position.Y * height) / 100;

            m_rect.Width = (int)(m_text.Length * 9.0f * m_size);
            m_rect.Height = (int)(14.0f * m_size);
        }

        public string GetText()
        {
            return m_text;
        }

        public void SetText(string Text)
        {
            m_text = Text;
        }

        public override void SetNewPosition(Vector2 Position)
        {
            m_position = Position;

            FindPosOnly(m_text);
        }

        public override bool Update(GameTime DT, Onderzoek.Input.CInputData InputData)
        {
            if (m_selected == true)
            {
                if (InputData.m_enter == true)
                {
                    InputData.m_enter = false;
                    if (m_event != null)
                    {
                        m_event(this, new CPlayerIndexEventArgs(PlayerIndex.One));
                        return true;
                    }
                }
            }

            return false;
        }

        public override void Render()
        {
            if (m_hover == true)
            {
                CVisual.Instance.RenderHudText(m_text, m_rect, m_size, Color.Orange);
            }
            else if (m_colour == null)
            {
                if (m_hover == true)
                {
                    CVisual.Instance.RenderHudText(m_text, m_rect, m_size, Color.Orange);
                }
                else if (m_selected == true)
                {
                    CVisual.Instance.RenderHudText(m_text, m_rect, m_size, Color.Red);
                }
                else
                {
                    CVisual.Instance.RenderHudText(m_text, m_rect, m_size, Color.Black);
                }
            }
            else
            {
                CVisual.Instance.RenderHudText(m_text, m_rect, m_size, (Color)m_colour);
            }
        }

        public override void FreeMemory()
        {
            m_text = null;
            // Do nothing
        }
    }
}

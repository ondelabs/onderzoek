float2 g_halfPixel;
float2 g_resolution0To1;

texture SSAOTexture;

sampler SSAOSampler : register(s2) = sampler_state
{
	Texture = (SSAOTexture);

    //AddressU = Clamp;
    //AddressV = Clamp;
};


struct AppToVertex
{
    float3 Position : POSITION0;
    float3 TexCoord : TEXCOORD0;
};

struct VertexToPixel
{
    float4 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};


VertexToPixel BlurVertexShader( AppToVertex Input )
{
	VertexToPixel output = (VertexToPixel)0;

    output.Position = float4(Input.Position,1);
    output.TexCoord = Input.TexCoord.xy + g_halfPixel;
    
    return output;
}

float4 BlurPixelShader( VertexToPixel Input ) : COLOR0
{
	float2 res_by_2 = g_resolution0To1 * 2.0f;
	float2 uv_samples[25] = 
	{
		float2(Input.TexCoord.x - g_resolution0To1.x, Input.TexCoord.y - g_resolution0To1.y),
		float2(Input.TexCoord.x, Input.TexCoord.y - g_resolution0To1.y),
		float2(Input.TexCoord.x + g_resolution0To1.x, Input.TexCoord.y - g_resolution0To1.y),
		float2(Input.TexCoord.x - g_resolution0To1.x, Input.TexCoord.y),
		float2(Input.TexCoord.x, Input.TexCoord.y),
		float2(Input.TexCoord.x + g_resolution0To1.x, Input.TexCoord.y),
		float2(Input.TexCoord.x - g_resolution0To1.x, Input.TexCoord.y + g_resolution0To1.y),
		float2(Input.TexCoord.x, Input.TexCoord.y + g_resolution0To1.y),
		float2(Input.TexCoord.x + g_resolution0To1.x, Input.TexCoord.y + g_resolution0To1.y),
		
		float2(Input.TexCoord.x - res_by_2.x, Input.TexCoord.y - res_by_2.y),
		float2(Input.TexCoord.x - g_resolution0To1.x, Input.TexCoord.y - res_by_2.y),
		float2(Input.TexCoord.x, Input.TexCoord.y - res_by_2.y),
		float2(Input.TexCoord.x + g_resolution0To1.x, Input.TexCoord.y - res_by_2.y),
		float2(Input.TexCoord.x + res_by_2.x, Input.TexCoord.y - res_by_2.y),
		
		float2(Input.TexCoord.x - res_by_2.x, Input.TexCoord.y - g_resolution0To1.y),
		float2(Input.TexCoord.x - res_by_2.x, Input.TexCoord.y),
		float2(Input.TexCoord.x - res_by_2.x, Input.TexCoord.y + g_resolution0To1.y),
		
		float2(Input.TexCoord.x + res_by_2.x, Input.TexCoord.y - g_resolution0To1.y),
		float2(Input.TexCoord.x + res_by_2.x, Input.TexCoord.y),
		float2(Input.TexCoord.x + res_by_2.x, Input.TexCoord.y + g_resolution0To1.y),
		
		float2(Input.TexCoord.x - res_by_2.x, Input.TexCoord.y + res_by_2.y),
		float2(Input.TexCoord.x - g_resolution0To1.x, Input.TexCoord.y + res_by_2.y),
		float2(Input.TexCoord.x, Input.TexCoord.y + res_by_2.y),
		float2(Input.TexCoord.x + g_resolution0To1.x, Input.TexCoord.y + res_by_2.y),
		float2(Input.TexCoord.x + res_by_2.x, Input.TexCoord.y + res_by_2.y),
	};
	
	float total_weight = 35;
	float kernal[25] = 
	{
		2, 2, 2, 2, 3, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1
	};
	
	float4 rtn_val = float4(0,0,0,1);
	for( int i = 0; i < 25; ++i )
	{
		rtn_val += tex2D(SSAOSampler, uv_samples[i]) * kernal[i];
	}

	return rtn_val / total_weight;
}


technique Blur
{
    pass Pass0
    {
        VertexShader = compile vs_3_0 BlurVertexShader();
        PixelShader = compile ps_3_0 BlurPixelShader();
    }
}
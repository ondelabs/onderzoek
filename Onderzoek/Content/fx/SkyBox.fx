texture Texture0;
texture NormalTexture;

float4x4  CameraTrans;
float2 g_halfPixel;

sampler EnvironmentSampler = sampler_state
{
    Texture = (Texture0);

};

sampler NormalSampler = sampler_state
{
	Texture = (NormalTexture);

    //AddressU = Clamp;
    //AddressV = Clamp;
};


struct CUBEMAP_VS_OUTPUT
{
    float4 Pos          : POSITION;    // position of the vertex
    float3 AimDirection : TEXCOORD0;    // position of the vertex
    float2 TexCoord		: TEXCOORD1;
};


CUBEMAP_VS_OUTPUT CubeMapVS(float3 InPos  : POSITION, float2 InTex  : TEXCOORD0)
{
    CUBEMAP_VS_OUTPUT Out = (CUBEMAP_VS_OUTPUT)0;

    Out.Pos  = float4(InPos,1);
    Out.AimDirection = mul(InPos + float3(0,0,-2),CameraTrans);
    Out.TexCoord = InTex + g_halfPixel;
    return Out;
}


float4 CubeMapPS1 ( CUBEMAP_VS_OUTPUT In) : COLOR0
{
	clip( tex2D(NormalSampler, In.TexCoord).a - 0.51f );
	
    return texCUBE(EnvironmentSampler, In.AimDirection);
}

float4 CubeMapPS2 ( CUBEMAP_VS_OUTPUT In) : COLOR0
{
    return texCUBE(EnvironmentSampler, In.AimDirection);
}


technique CubeMap
{
    pass P0
    {          
        VertexShader = compile vs_2_0 CubeMapVS();
        PixelShader  = compile ps_2_0 CubeMapPS1();
    }
    
    pass P1
    {          
        VertexShader = compile vs_2_0 CubeMapVS();
        PixelShader  = compile ps_2_0 CubeMapPS2();
    }
}

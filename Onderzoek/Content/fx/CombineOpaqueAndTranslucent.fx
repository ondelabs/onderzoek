float2 g_halfPixel;

texture Texture0;
texture Texture1;
texture Texture2;
texture Texture3;

sampler Sampler0 : register(s0) = sampler_state
{
	Texture = (Texture0);

    //AddressU = Clamp;
    //AddressV = Clamp;
};

sampler Sampler1 : register(s1) = sampler_state
{
	Texture = (Texture1);

    //AddressU = Clamp;
    //AddressV = Clamp;
};

sampler Sampler2 : register(s2) = sampler_state
{
	Texture = (Texture2);

    //AddressU = Clamp;
    //AddressV = Clamp;
};

sampler Sampler3 : register(s3) = sampler_state
{
	Texture = (Texture3);

    //AddressU = Clamp;
    //AddressV = Clamp;
};


struct AppToVertex
{
    float3 Position : POSITION0;
    float3 TexCoord : TEXCOORD0;
};

struct VertexToPixel
{
    float4 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};


VertexToPixel CombineVertexShader( AppToVertex Input )
{
	VertexToPixel output = (VertexToPixel)0;

    output.Position = float4(Input.Position,1);
    output.TexCoord = Input.TexCoord.xy + g_halfPixel;
    
    return output;
}

float4 CombinePixelShader( VertexToPixel Input ) : COLOR0
{
	float depth_opaque = tex2D( Sampler1, Input.TexCoord );
	float depth_trans = tex2D( Sampler3, Input.TexCoord );
	
	if( depth_opaque > depth_trans )
	{
		float4 trans_colour = tex2D( Sampler2, Input.TexCoord );
		return trans_colour * trans_colour.a + tex2D( Sampler0, Input.TexCoord ) * (1.0f - trans_colour.a);
	}
	else
	{
		return tex2D( Sampler0, Input.TexCoord );
	}
}


technique Combine
{
    pass Pass0
    {
        VertexShader = compile vs_2_0 CombineVertexShader();
        PixelShader = compile ps_2_0 CombinePixelShader();
    }
}
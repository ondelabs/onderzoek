//-----------------------------------------------------------------------------
// Distort.fx
//
//-----------------------------------------------------------------------------

float4x4 World;
float4x4 View;
float4x4 Projection;

float2 g_halfPixel;
float3 g_cameraPosition;
float g_distortMultiplier;

bool g_isWindows;
float g_farDistance;

texture BackBufferTexture;
sampler BackBufferSampler : register(s0) = sampler_state
{
	Texture = (BackBufferTexture);

	AddressU = Clamp;
	AddressV = Clamp;
};

texture OpaqueDepthTexture;
sampler OpaqueDepthSampler = sampler_state
{
	Texture = (OpaqueDepthTexture);

	AddressU = Clamp;
	AddressV = Clamp;
};

texture TranslucentDepthTexture;
sampler TranslucentDepthSampler = sampler_state
{
	Texture = (TranslucentDepthTexture);

	AddressU = Clamp;
	AddressV = Clamp;
};

struct VertexToPixel
{
    float4 Position : POSITION;
	float3 TexCoordAndDepth : TEXCOORD0;
    float3 Normal : TEXCOORD1;
    float4 HomogeniousPosition : TEXCOORD2;
};


// Vertex shader input structure.
struct AppToVertex
{
    float4 Position : POSITION0;
    float3 Normal : NORMAL0;
    float2 TexCoord : TEXCOORD0;
};


VertexToPixel DistortVertexShader( AppToVertex Input )
{
    VertexToPixel output = (VertexToPixel)0;

    float4 worldPosition = mul(Input.Position, World);
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Projection);
	output.Normal = mul(Input.Normal, World);
	output.Normal = mul(output.Normal, View);
	output.Normal = mul(output.Normal, Projection);

    output.TexCoordAndDepth.xy = Input.TexCoord;
    output.TexCoordAndDepth.z = -viewPosition.z;
	output.HomogeniousPosition = output.Position;

	return output;
}

float4 DistortPixelShader(VertexToPixel Input) : COLOR
{
	if( g_isWindows == false )
	{
		float2 tex_2d = Input.TexCoordAndDepth.xy + g_halfPixel;
		float depth = tex2D( OpaqueDepthSampler, tex_2d ).r;
		float cur_depth = (Input.TexCoordAndDepth.z / g_farDistance);
		if( depth < cur_depth )
		{
			clip(-1);
		}
		depth = tex2D( TranslucentDepthSampler, tex_2d ).r;
		if( depth < cur_depth )
		{
			clip(-1);
		}
	}

    float2 tex_2d = 0.5 * Input.HomogeniousPosition.xy / Input.HomogeniousPosition.w + float2(0.5f, 0.5f);
    tex_2d.y = 1.0f - tex_2d.y;
    tex_2d += g_halfPixel;
    
    float2 tex_2dn = 0.5 * Input.Normal.xy / Input.HomogeniousPosition.w + float2(0.5f, 0.5f);
    tex_2dn.y = 1.0f - tex_2dn.y;
    tex_2dn += g_halfPixel;

	return tex2D(BackBufferSampler, tex_2d.xy + (tex_2dn.xy * g_distortMultiplier));
}


technique PhongTechnique
{
    pass Pass0
    {
        VertexShader = compile vs_2_0 DistortVertexShader();
        PixelShader = compile ps_2_0 DistortPixelShader();
    }
}
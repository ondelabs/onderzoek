// Still a work in progress

float2 g_halfPixel;
float2 g_resolution0To1;

float3 g_corners[4];

float4x4 g_proj;

#define NUM_SAMPLE_POINTS	32
float3 RandomVector[NUM_SAMPLE_POINTS];
float  SampleRadius[5] = { 1.0f, 0.8f, 0.6f, 0.4f, 0.2f };

texture DepthTexture;
texture NormalTexture;
texture RNTexture;
texture DiffuseTexture;

sampler DepthSampler : register(s0) = sampler_state
{
	Texture = (DepthTexture);

    //AddressU = Clamp;
    //AddressV = Clamp;
};

sampler NormalSampler : register(s1) = sampler_state
{
	Texture = (NormalTexture);

    //AddressU = Clamp;
    //AddressV = Clamp;
};

sampler RNSampler : register(s2) = sampler_state
{
	Texture = (RNTexture);

    //AddressU = Clamp;
    //AddressV = Clamp;
};

sampler DiffuseSampler : register(s3) = sampler_state
{
	Texture = (DiffuseTexture);

    //AddressU = Clamp;
    //AddressV = Clamp;
};

struct AppToVertex
{
    float4 Position : POSITION0;
    float3 TexCoord : TEXCOORD0;
};

struct VertexToPixel
{
    float4 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
    float3 ViewDir : TEXCOORD1;
};


VertexToPixel SSAOVertexShader( AppToVertex Input )
{
	VertexToPixel output = (VertexToPixel)0;

    output.Position = Input.Position;
    output.TexCoord = Input.TexCoord.xy + g_halfPixel;
    output.ViewDir = g_corners[Input.TexCoord.z];
    
    return output;
}


// Can speed this up by using offset tex UVs, instead of projecting rays.
float4 SSAOPixelShader( VertexToPixel Input ) : COLOR0
{
    //reconstruct eye-space position from the depth buffer
    float pixelDepth = tex2D(DepthSampler, Input.TexCoord).r;
    float3 pixelPosEyeSpace = pixelDepth * Input.ViewDir;

    float3 normal = 2.0f * tex2D( NormalSampler, Input.TexCoord ) - 1.0f;
    float3 r_normal = 2.0f * tex2D( RNSampler, Input.TexCoord * 38.0f ) - 1.0f;

	//float dist_atten = 1.0f - (pixelPosEyeSpace.z / 1000.0f);

    //loop through our sample locations, accumulating the occlusion
    float result = 0;
    for (int i = 0; i < NUM_SAMPLE_POINTS; i++)
    {
		float3 reflc_ray = reflect( RandomVector[i], r_normal ) *  ( SampleRadius[i%5] * 0.5f /* dist_atten*/ );

		//determine the eye-space and clip-space locations of our current sample point
		float4 samplePointEyeSpace = float4(pixelPosEyeSpace + reflc_ray, 1.0f);
		float4 samplePointClipSpace = mul(samplePointEyeSpace, g_proj);

		//determine the texture coordinate of our current sample point
		float2 sampleTexCoord = 0.5f * samplePointClipSpace.xy/samplePointClipSpace.w + float2(0.5f, 0.5f);

		//Flip around the y-coordinate and offset by half a pixel
		//This is for Direct3D9 only
		sampleTexCoord.y = 1.0f - sampleTexCoord.y;
		sampleTexCoord += g_halfPixel;

		//read the depth of our sample point from the depth buffer
		float sampleDepth = tex2D(DepthSampler, sampleTexCoord).r;
		float3 sampleNormal = 2.0f * tex2D( NormalSampler, sampleTexCoord ) - 1.0f;

		// FIRST IMPLEMENTATION
		//float dp = dot(sampleNormal, normal);
		//dp = 1 - ( dp / 2 + 0.5f );
		//float dif = 250.0f * max( pixelDepth - sampleDepth, 0 );
		//result += dp * (1.0f / (1.0f + dif * dif));

		// SECOND IMPLEMENTATION - need a better noise map.
		float dp = dot(sampleNormal, normal);
		dp = 1 - ( dp / 2 + 0.5f );

		float dif = max( pixelDepth - sampleDepth, 0 );
		float dist_atten = 1.0f - ( dif / 0.001f );
		if( dist_atten >= 0 )
		{
			result += dp * dist_atten;
		}
	}

	result = 1 - ( result / NUM_SAMPLE_POINTS );
	return float4(result, result, result, 1.0f);
}


technique SSAO
{
    pass Pass0
    {
        VertexShader = compile vs_3_0 SSAOVertexShader();
        PixelShader = compile ps_3_0 SSAOPixelShader();
    }
}
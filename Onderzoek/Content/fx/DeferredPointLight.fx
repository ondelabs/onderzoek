float4x4 World;
float4x4 View;
float4x4 Projection;

float3 g_lightPosition = float3(0,10.0f,0);
float3 g_lightCol = float3(1,0,1);
float g_lightRadius = 20.0f;
float g_lightIntensity = 2.0f;
float g_lightSpec = 1.0f;

float2 g_halfPixel;

// These textures contain all the data to do the lighting
texture DiffuseTexture;
texture NormalTexture;
texture DepthTexture;
texture MaterialTexture;

sampler DiffuseSampler : register(s0) = sampler_state
{
	Texture = (DiffuseTexture);

    //AddressU = Clamp;
    //AddressV = Clamp;
};

sampler NormalSampler : register(s1) = sampler_state
{
	Texture = (NormalTexture);

    //AddressU = Clamp;
    //AddressV = Clamp;
};

sampler DepthSampler : register(s2) = sampler_state
{
	Texture = (DepthTexture);

    //AddressU = Clamp;
    //AddressV = Clamp;
};

sampler MaterialSampler : register(s3) = sampler_state
{
	Texture = (MaterialTexture);

    //AddressU = Clamp;
    //AddressV = Clamp;
};


struct VertexToPixel
{
    float4 Position : POSITION;
    float4 ScreenPosition : TEXCOORD0;
    float3 ViewDir : TEXCOORD1;
};


VertexToPixel FinalVertexShader( float3 InPos : POSITION )
{
    VertexToPixel output = (VertexToPixel)0;
    
    //processing geometry coordinates
    float4 world_pos = mul(float4(InPos,1), World);
    float4 view_pos = mul(world_pos, View);
    output.Position = mul(view_pos, Projection);
    output.ScreenPosition = output.Position;
    
    output.ViewDir = view_pos.xyz;
    
    return output;
}


float4 FinalPixelShader(VertexToPixel Input) : COLOR0
{
    //obtain screen position
    Input.ScreenPosition.xy /= Input.ScreenPosition.w;

    //obtain textureCoordinates corresponding to the current pixel
    //the screen coordinates are in [-1,1]*[1,-1]
    //the texture coordinates need to be in [0,1]*[0,1]
    float2 texCoord = 0.5f * (float2(Input.ScreenPosition.x,-Input.ScreenPosition.y) + 1);
    //allign texels to pixels
    texCoord += float2( -g_halfPixel.x, g_halfPixel.y );

	float4 output = float4( 1, 1, 1, 0 );

	float4 normal = tex2D(NormalSampler,texCoord);
	//if( normal.a != 0 )
	{
		//tranform normal back into [-1,1] range
		float3 unit_normal = 2.0f * normal.xyz - 1.0f;
		
		//read depth
		float depth = tex2D(DepthSampler, texCoord).r;
		
		float3 position = depth * (Input.ViewDir.xyz * (1000.0f/-Input.ViewDir.z));
		
		//surface-to-light vector
		float3 light_vec = mul(float4(g_lightPosition,1.0f), View) - position;

		//compute attenuation based on distance - linear attenuation
		float attenuation = saturate(1.0f - length(light_vec) / g_lightRadius);
		
		//normalize light vector
		light_vec = normalize(light_vec); 

		// Work out phong shade
		float light_vec_norm = dot( light_vec, unit_normal );
		float angle = clamp( light_vec_norm, 0, 1 );

		float3 vertex_intensity = g_lightCol * angle;

		//float4 diffuse = (gb_diffuse * vertex_intensity);
		float3 diffuse = vertex_intensity;
		//diffuse *= tex_colour;
		//diffuse += tex_colour;

		float3 reflection1 = ( unit_normal * light_vec_norm * 2.0f ) - light_vec;
		float3 reflection = normalize( reflection1 );
		
		float3 unit_view_vec = normalize(float3(0,0,0) - position);

		float intensity = dot( reflection, unit_view_vec );
		if( intensity < 0 )
		{
			intensity = 0;
		}

		float spec1 = 0;
		if( g_lightSpec != 0 )
		{
			spec1 = tex2D(DiffuseSampler, texCoord).a * pow( intensity, tex2D(MaterialSampler, texCoord).a * 255.0f );
		}

		// These two lines have moved to the combinefinal shader.
		//float4 spec2 = float4(material.rgb, 1.0f) * spec1;
		//float4 specular = spec2 * g_lightCol;

		output = attenuation * g_lightIntensity * float4(diffuse, spec1 * g_lightSpec);
    }
    
    return output;
}
 
 
 technique FinalDeferred
 {
     pass Pass0
     {
         VertexShader = compile vs_2_0 FinalVertexShader();
         PixelShader = compile ps_2_0 FinalPixelShader();
     }
 }
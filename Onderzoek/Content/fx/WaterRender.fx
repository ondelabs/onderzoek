float4x4 World;
float4x4 View;
float4x4 Projection;

#ifdef REFLECT
float4x4 ReflectView;
#endif

//float g_reflectance;
float g_specularIntensity = 0.8f;
float g_time;
float3 g_windDir;
float3 g_perpDir;
float2 g_halfPixel;
float g_reflecRefracOffset = 100;
float g_speed;
float g_fresnelOffset = 0.3f;

bool g_isWindows;
float g_farDistance;

float3 g_cameraPosition;
float3 g_lightDirection;
float4 g_lightColour;
float g_lightIntensity;
bool g_lightEnabled;

// Material data
float4 g_diffuse;
float4 g_ambientAndEmissive;
float4 g_specularColour;
float g_specularPower;

#ifdef SOFT
	texture DepthMapTexture;
	sampler2D DepthMapSampler = sampler_state
	{
		Texture = <DepthMapTexture>;

	AddressU = Clamp;
	AddressV = Clamp;
	};
#endif

texture BackBufferTexture;
sampler BackBufferSampler = sampler_state
{
	Texture = (BackBufferTexture);

	AddressU = Clamp;
	AddressV = Clamp;
};

texture ProjectedTexture;
sampler2D ProjectedSampler = sampler_state
{
	Texture = <ProjectedTexture>;

	AddressU = Clamp;
	AddressV = Clamp;
};

texture WaterRippleTexture;
sampler2D WaterRippleSampler = sampler_state
{
	Texture = <WaterRippleTexture>;

	AddressU = Wrap;
	AddressV = Wrap;
};

texture OpaqueDepthTexture;
sampler OpaqueDepthSampler = sampler_state
{
	Texture = (OpaqueDepthTexture);

	AddressU = Clamp;
	AddressV = Clamp;
};

texture TranslucentDepthTexture;
sampler TranslucentDepthSampler = sampler_state
{
	Texture = (TranslucentDepthTexture);

	AddressU = Clamp;
	AddressV = Clamp;
};

float3x3 CreateTBNMatrix( const float3 Tangent, const float3 Binormal, const float3 Normal )
{
	float3x3 tbn_mat = { Tangent.x, Tangent.y, Tangent.z,
						 Binormal.x, Binormal.y, Binormal.z,
						 Normal.x, Normal.y, Normal.z };

	return tbn_mat;
}

struct VertexToPixel
{
    float4 Position : POSITION;
	float4 TexCoord0 : TEXCOORD0;
	float3 TexCoord1AndDepth : TEXCOORD1;
    float3 Normal : TEXCOORD2;
    float3 WorldPosition : TEXCOORD3;
    float4 HomogeniousPosition : TEXCOORD4;
    
    float3 Tangent : TEXCOORD5;
	float3 Binormal : TEXCOORD6;

#ifdef REFLECT
	float4 ProjTexCoord : TEXCOORD7;
#endif
};


// Vertex shader input structure.
struct AppToVertex
{
    float4 Position : POSITION0;
    float3 Normal : NORMAL0;
	float2 TexCoord : TEXCOORD0;
	
    float3 Tangent : TANGENT;
    float3 Binormal : BINORMAL;
};

float2 SourceOfRippple = float2(-87, 262);

// Works out the height for the current vertex.
float WorkOutNewHeight( float2 Source, float2 CurPos, float AngFreq, float PhaseConst, float Dist,
						float Amplitude, float WaveLength )
{
	float2 direction = normalize( Source - CurPos );

	float new_amp = pow( 0.5, ( Dist / WaveLength ) ) * Amplitude;

	float2 offset_new_pos = CurPos - Source;

	float k = 2.5f;

	float dot_dir_offset = dot( direction, offset_new_pos );
	float angle = pow( ( sin( dot_dir_offset * AngFreq + g_time * PhaseConst ) + 1 ) / 2, k - 1.0f );
	return k * AngFreq * new_amp * angle * cos( dot_dir_offset * AngFreq + g_time * PhaseConst );
}

struct SWaterTBN
{
	float3 s_binormal;
	float3 s_normal;
	float3 s_tangent;
};

SWaterTBN CalcNewWaterNormal( float WaveHeight )
{
	SWaterTBN rtn_struct = (SWaterTBN)0;

	rtn_struct.s_binormal = float3( 1, WaveHeight, 0 );
	rtn_struct.s_tangent = float3( 0, WaveHeight, 1 );
	rtn_struct.s_normal = cross( rtn_struct.s_tangent, rtn_struct.s_binormal );

	return rtn_struct;
}

float3 NewWavePos( float3 Position )
{
	float wavelength = 2.0f;
	float speed = 100.0f;
	float height = 0;
	float ang_freq = ( 2 * 3.1415926 ) / wavelength;
	float phase_const = speed * ang_freq;

	height = WorkOutNewHeight( SourceOfRippple, Position.xz, ang_freq, phase_const, distance(SourceOfRippple, Position.xz), 0.5f, wavelength );
	SWaterTBN tb_stuff = CalcNewWaterNormal(height);
	float3 normal = tb_stuff.s_normal;

	return normal;
}


VertexToPixel PhongVertexShader( AppToVertex Input )
{
    VertexToPixel output = (VertexToPixel)0;

    float4 worldPosition = mul(Input.Position, World);
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Projection);
	output.Normal = mul(Input.Normal, World);

	output.Tangent = mul( Input.Tangent, World );
	output.Binormal = mul( Input.Binormal, World );

	output.HomogeniousPosition = output.Position;
#ifdef REFLECT
    output.ProjTexCoord = mul(mul(worldPosition, ReflectView), Projection);
#endif
	output.WorldPosition = worldPosition.xyz;

	float dot0 = dot(Input.TexCoord, g_windDir.xz);
	float dot1 = dot(Input.TexCoord, g_perpDir.xz);

	output.TexCoord0.xy = float2(dot0, dot1 + (g_time * g_speed)) * 2.0f;
	output.TexCoord0.zw = float2(dot0 + 2.0f, dot1 + ((g_time * g_speed) * 2.0f) + 2.0f) * 2.0f;
	output.TexCoord1AndDepth.xy = float2(dot0 + 4.0f, dot1 + ((g_time * g_speed) * 4.0f) + 4.0f) * 2.0f;

	output.TexCoord1AndDepth.z = -viewPosition.z;

	return output;
}

float CalcFresnel( float NDotV, float Reflect )
{
	return saturate( Reflect + ( 1.0f - Reflect ) * pow( 1.0f - NDotV, 5 ) );
}

float4 PhongPixelShader(VertexToPixel Input) : COLOR
{
	float4 diffuse = g_diffuse;
	
	if( g_lightEnabled == true )
	{
		float2 tex_2d = 0.5 * Input.HomogeniousPosition.xy / Input.HomogeniousPosition.w + float2(0.5f, 0.5f);
		tex_2d.y = 1.0f - tex_2d.y;

		if( g_isWindows == false )
		{
			tex_2d += g_halfPixel;
			float depth = tex2D( OpaqueDepthSampler, tex_2d ).r;
			float cur_depth = (Input.TexCoord1AndDepth.z / g_farDistance);
			if( depth < cur_depth )
			{
				clip(-1);
			}
			depth = tex2D( TranslucentDepthSampler, tex_2d ).r;
			if( depth < cur_depth )
			{
				clip(-1);
			}
		}

		// Get normal from normal map
		float3x3 tbn_mat = CreateTBNMatrix( Input.Tangent, Input.Binormal, Input.Normal );
		float3 frm_norm_map = 2.0f * tex2D( WaterRippleSampler, Input.TexCoord0.xy ) - 1.0f;
		frm_norm_map += 2.0f * tex2D( WaterRippleSampler, Input.TexCoord0.zw ) - 1.0f;
		frm_norm_map += 2.0f * tex2D( WaterRippleSampler, Input.TexCoord1AndDepth.xy ) - 1.0f;
		float3 normal = mul( frm_norm_map, tbn_mat );
		
		// Ripple
		//normal += NewWavePos(Input.WorldPosition);

		float3 unit_view_vec = normalize( g_cameraPosition - Input.WorldPosition );
		float3 unit_norm = normalize(normal);

		// Work out phong shade
		float angle = dot( g_lightDirection, unit_norm );

		diffuse *= g_lightColour * angle;

		float3 reflection1 = ( unit_norm * angle * 2.0f ) - g_lightDirection;
		float3 reflection = normalize( reflection1 );

		float intensity = dot( reflection, unit_view_vec );
		if( intensity < 0 )
		{
		    intensity = 0;
		}

		float spec1 = pow( intensity, g_specularPower );

		float4 specular = (g_specularColour * spec1) * g_lightColour;

		float2 normal_offset = (unit_norm.xz / g_reflecRefracOffset);

#ifdef REFLECT
		float2 ProjectedTexCoords = float2(Input.ProjTexCoord.x/Input.ProjTexCoord.w/2.0f + 0.5f, -Input.ProjTexCoord.y/Input.ProjTexCoord.w/2.0f + 0.5f);
		float4 proj_col = tex2D(ProjectedSampler, ProjectedTexCoords + g_halfPixel + normal_offset);

		// CalcFresnel
		float n_dot_v = saturate( dot( unit_view_vec, normalize(Input.Normal) ) + g_fresnelOffset );
		//float fresnel = CalcFresnel(n_dot_v, g_reflectance);
#endif

#ifdef SOFT
		float water_reflec_refrac_mix = 0.2f;

		if( g_isWindows == true )
		{
			tex_2d += g_halfPixel;
		}

		float depth = tex2D( DepthMapSampler, tex_2d ).r;
		
		float depthRange = (depth - (Input.TexCoord1AndDepth.z / g_farDistance));
		
		const float shoreFalloff = 1.75f;
		const float shoreScale = 5.0f;

		float alpha = max(pow(depthRange, shoreFalloff) * g_farDistance * shoreScale, 0);
		
		water_reflec_refrac_mix = saturate(alpha);

		tex_2d += normal_offset;
		
 #ifdef REFLECT
		diffuse = specular + lerp( lerp(tex2D(BackBufferSampler, tex_2d), proj_col, (1.0f - n_dot_v) * water_reflec_refrac_mix), diffuse * g_lightIntensity, water_reflec_refrac_mix * 0.2f);
 #else
		diffuse = specular + lerp( tex2D(BackBufferSampler, tex_2d), diffuse * g_lightIntensity, water_reflec_refrac_mix * 0.2f);
 #endif

#else
		tex_2d += g_halfPixel + normal_offset;
		
 #ifdef REFLECT
		//diffuse = specular + lerp( lerp(tex2D(BackBufferSampler, tex_2d), proj_col, fresnel), diffuse * g_lightIntensity, 0.2f);
		
		diffuse = specular + lerp( lerp( proj_col, tex2D(BackBufferSampler, tex_2d), n_dot_v), diffuse * g_lightIntensity, 0.2f);
 #else
		diffuse = specular + lerp( tex2D(BackBufferSampler, tex_2d), diffuse * g_lightIntensity, 0.2f);
 #endif
 
#endif
	}

	float4 output = diffuse + g_ambientAndEmissive;

    return float4(output.r, output.g, output.b, 1.0f);
}


technique WaterRendering
{
    pass Pass0
    {
        VertexShader = compile vs_3_0 PhongVertexShader();
        PixelShader = compile ps_3_0 PhongPixelShader();
    }
}
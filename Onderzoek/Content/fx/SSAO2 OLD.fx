// Still a work in progress

float2 g_halfPixel;
float2 g_resolution0To1;

float3 g_corners[4];

float4x4 g_proj;

#define NUM_SAMPLE_POINTS	32
float3 RandomVector[NUM_SAMPLE_POINTS];
float  SampleRadius[5] = { 1.0f, 0.8f, 0.6f, 0.4f, 0.2f };

texture DepthTexture;
texture NormalTexture;
texture RNTexture;

sampler DepthSampler : register(s0) = sampler_state
{
	Texture = (DepthTexture);
    
    MinFilter = Point;
    MagFilter = Point;
    AddressU = Clamp;
    AddressV = Clamp;
};

sampler NormalSampler : register(s1) = sampler_state
{
	Texture = (NormalTexture);
    
    MinFilter = Point;
    MagFilter = Point;
    AddressU = Clamp;
    AddressV = Clamp;
};

sampler RNSampler : register(s2) = sampler_state
{
	Texture = (RNTexture);
    
    MinFilter = Point;
    MagFilter = Point;
    AddressU = Clamp;
    AddressV = Clamp;
};

struct AppToVertex
{
    float4 Position : POSITION0;
    float3 TexCoord : TEXCOORD0;
};

struct VertexToPixel
{
    float4 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
    float3 ViewDir : TEXCOORD1;
};


VertexToPixel SSAOVertexShader( AppToVertex Input )
{
	VertexToPixel output = (VertexToPixel)0;

    output.Position = Input.Position;
    output.TexCoord = Input.TexCoord.xy - g_halfPixel;
    output.ViewDir = g_corners[Input.TexCoord.z];
    
    return output;
}


#define NUM_SAMPLES 8


//noise producing function to eliminate banding (got it from someone else�s shader):
float rand(float2 co)
{
	return 0.5+(frac(sin(dot(co.xy ,float2(12.9898,78.233))) * 43758.5453))*0.5;
}


float4 SSAOPixelShader( VertexToPixel Input ) : COLOR0
{
	//calculate sampling rates:
	float ratex = (1.0/800.0);
	float ratey = (1.0/600.0);

	//initialize occlusion sum and gi color:
	float sum = 0.0;
	float3 fcolor = float3(0,0,0);

	//far and near clip planes:
	float zFar = 1000.0f;
	float zNear = 1.0f;

	//get depth at current pixel:
	float prof = tex2D(DepthSampler, Input.TexCoord).x;
	//scale sample number with depth:
	int samples = round(NUM_SAMPLES/(0.5+prof));
	prof = zFar * zNear / (prof * (zFar - zNear) - zFar);  //linearize z sample

	//obtain normal and color at current pixel:
	float3 norm = float3(tex2D(NormalSampler,Input.TexCoord).xyz)*2.0-float3(1.0f, 1.0f, 1.0f);
	float3 dcolor1 = tex2D(RNSampler, Input.TexCoord);

	int hf = samples/2;

	//calculate kernel steps:
	float incx = ratex*30;//gi radius
	float incy = ratey*30;

	float incx2 = ratex*8;//ao radius
	float incy2 = ratey*8;

	//do the actual calculations:
	for(int i=-4; i < 4; i++)
	{
		for(int j=-4; j < 4; j++)
		{
			if (i != 0 || j!= 0)
			{
				float2 coords = float2(i*incx,j*incy)/prof;
				float2 coords2 = float2(i*incx2,j*incy2)/prof;

				float prof2 = tex2D(DepthSampler,Input.TexCoord+coords*rand(Input.TexCoord)).x;
				prof2 = zFar * zNear / (prof2 * (zFar - zNear) - zFar);  //linearize z sample

				float prof2g = tex2D(DepthSampler,Input.TexCoord+coords2*rand(Input.TexCoord)).x;
				prof2g = zFar * zNear / (prof2g * (zFar - zNear) - zFar);  //linearize z sample

				float3 norm2g = normalize(float3(tex2D(NormalSampler,Input.TexCoord+coords2*rand(Input.TexCoord)).xyz)*2.0-float3(1.0f, 1.0f, 1.0f)); 

				float3 dcolor2 = tex2D(RNSampler, Input.TexCoord+coords*rand(Input.TexCoord));

				//OCCLUSION:
				//calculate approximate pixel distance:
				float3 dist2 = float3(coords2,prof-prof2g);

				//calculate normal and sampling direction coherence:
				float coherence2 = dot(normalize(-coords2),normalize(float2(norm2g.xy)));

				//if there is coherence, calculate occlusion:
				if (coherence2 > 0)
				{
					float pformfactor2 = 0.5*((1.0-dot(norm,norm2g)))/(3.1416*pow(abs(length(dist2*2)),2.0)+0.5);//el 4: depthscale
					sum += clamp(pformfactor2*0.2,0.0,1.0);//ao intensity; 
				}
				
				//COLOR BLEEDING:
				if (length(dcolor2)>0.3)//color threshold
				{
					float3 norm2 = normalize(float3(tex2D(NormalSampler,Input.TexCoord+coords*rand(Input.TexCoord)).xyz)*2.0-float3(1.0f, 1.0f, 1.0f)); 

					//calculate approximate pixel distance:
					float3 dist = float3(coords,abs(prof-prof2));

					//calculate normal and sampling direction coherence:
					float coherence = dot(normalize(-coords),normalize(float2(norm2.xy)));

					//if there is coherence, calculate bleeding:
					if (coherence > 0)
					{
						float pformfactor = ((1.0-dot(norm,norm2)))/(3.1416*pow(abs(length(dist*2)),2.0)+0.5);//el 4: depthscale
						fcolor += dcolor2*(clamp(pformfactor,0.0,1.0));
					}
				}
			}
		}
	}

	float3 bleeding = (fcolor/samples)*0.5;
	float occlusion = 1.0-(sum/samples);
	//gl_FragColor = vec4(vec3(dcolor1*occlusion+bleeding*0.5),1.0);

	return float4(dcolor1*occlusion+bleeding*0.5,1.0f);
}


technique SSAO
{
    pass Pass0
    {
        VertexShader = compile vs_3_0 SSAOVertexShader();
        PixelShader = compile ps_3_0 SSAOPixelShader();
    }
}
float4x4 g_world;
float4x4 g_view;

float2 g_halfPixel;
float3 g_lightDir;// = float3(-0.3f, 1.0f, -0.3f);
float3 g_lightCol;// = float3(1,1,1);
float g_lightIntensity;// = 0.8f;
float g_lightSpec = 1.0f;

float3 g_corners[4];

// These textures contain all the data to do the lighting
texture DiffuseTexture;
texture NormalTexture;
texture DepthTexture;
texture MaterialTexture;

sampler DiffuseSampler : register(s0) = sampler_state
{
	Texture = (DiffuseTexture);

    //AddressU = Clamp;
    //AddressV = Clamp;
};

sampler NormalSampler : register(s1) = sampler_state
{
	Texture = (NormalTexture);

    //AddressU = Clamp;
    //AddressV = Clamp;
};

sampler DepthSampler : register(s2) = sampler_state
{
	Texture = (DepthTexture);

    //AddressU = Clamp;
    //AddressV = Clamp;
};

sampler MaterialSampler : register(s3) = sampler_state
{
	Texture = (MaterialTexture);

    //AddressU = Clamp;
    //AddressV = Clamp;
};


struct VertexToPixel
{
    float4 Position : POSITION;
    float2 TexCoord : TEXCOORD0;
    float3 ViewDir : TEXCOORD1;
};


VertexToPixel FinalVertexShader( float3 InPos : POSITION, float3 InTexCoord : TEXCOORD0 )
{
    VertexToPixel output = (VertexToPixel)0;
    
    output.Position = float4(InPos,1);
    
    //align texture coordinates
    output.TexCoord = InTexCoord.xy + float2( -g_halfPixel.x, g_halfPixel.y );
    
    output.ViewDir = g_corners[InTexCoord.z];//mul( g_corners[InTexCoord.z], g_world );
    
    return output;
}


float4 FinalPixelShader(VertexToPixel Input) : COLOR
{
	float4 output = float4( 0, 0, 0, 0 );

	float4 normal = tex2D(NormalSampler, Input.TexCoord);
	//if( normal.a != 0 )
	{
		float3 unit_normal = 2.0f * normal.xyz - 1.0f;
		
		//read depth
		float depth = tex2D(DepthSampler, Input.TexCoord).r;
		
		float3 view_pos = depth * Input.ViewDir;

		// camera position in view space is (0,0,0)
		float3 unit_view_vec = normalize(float3(0,0,0) - view_pos);

		float3 unit_light_vec = mul(g_lightDir, g_view);

		// Work out phong shade
		float light_vec_norm = dot( unit_light_vec, unit_normal );
		float angle = clamp( light_vec_norm, 0, 1 );

		float3 vertex_intensity = g_lightCol * angle;

		//float4 diffuse = (gb_diffuse * vertex_intensity);
		float3 diffuse = vertex_intensity;
		//diffuse *= tex_colour;
		//diffuse += tex_colour;

		float3 reflection1 = ( unit_normal * light_vec_norm * 2.0f ) - unit_light_vec;
		float3 reflection = normalize( reflection1 );

		float intensity = dot( reflection, unit_view_vec );
		if( intensity < 0 )
		{
			intensity = 0;
		}
		
		float spec1 = 0;
		if( normal.a - 0.51f < 0 ) // to prevent stupid specular
		{
			spec1 = tex2D(DiffuseSampler, Input.TexCoord).a * pow( intensity, tex2D(MaterialSampler, Input.TexCoord).a * 255.0f );
		}

		// These two lines have moved to the finaldeferred shader.
		//float4 spec2 = float4(material.rgb, 1.0f) * spec1;
		//float4 specular = spec2 * g_lightCol;

		output.rgb = diffuse * g_lightIntensity;
		output.a = spec1 * g_lightSpec;
	}

	return output;
}
 
 
 technique FinalDeferred
 {
     pass Pass0
     {
         VertexShader = compile vs_2_0 FinalVertexShader();
         PixelShader = compile ps_2_0 FinalPixelShader();
     }
 }
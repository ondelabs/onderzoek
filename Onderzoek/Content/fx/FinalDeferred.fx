float2 g_halfPixel;

texture DiffuseTexture;
texture LightTexture;
texture MaterialTexture;
texture ShadowTexture;
texture GITexture;
texture NormalTexture;

bool g_lightsActive;

#ifdef SSAO
texture SSAOTexture;
#endif

sampler DiffuseSampler = sampler_state
{
    Texture = (DiffuseTexture);
    //AddressU = Clamp;
    //AddressV = Clamp;
};
sampler LightSampler = sampler_state
{
    Texture = (LightTexture);
    //AddressU = Clamp;
    //AddressV = Clamp;
};
sampler MaterialSampler = sampler_state
{
    Texture = (MaterialTexture);
    //AddressU = Clamp;
    //AddressV = Clamp;
};
sampler ShadowSampler = sampler_state
{
    Texture = (ShadowTexture);
    //AddressU = Clamp;
    //AddressV = Clamp;
};

#ifdef SSAO
sampler SSAOSampler = sampler_state
{
    Texture = (SSAOTexture);
    //AddressU = Clamp;
    //AddressV = Clamp;
};
#endif

sampler NormalSampler = sampler_state
{
    Texture = (NormalTexture);
    //AddressU = Clamp;
    //AddressV = Clamp;
};


struct VertexShaderInput
{
    float3 Position : POSITION0;
    float3 TexCoord : TEXCOORD0;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};

VertexShaderOutput VertexShaderFunction(VertexShaderInput input)
{
    VertexShaderOutput output;
    output.Position = float4(input.Position,1);
    output.TexCoord = input.TexCoord.xy + g_halfPixel;
    return output;
}

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
    float3 diffuseColor = tex2D(DiffuseSampler,input.TexCoord).rgb;
    float4 light = tex2D(LightSampler,input.TexCoord);
    // the * 1000 is needed because the specular colour is packed
    float4 emissive = tex2D(MaterialSampler,input.TexCoord);
    float3 specularColour = frac( emissive.rgb * 100.0f );
    
    // Without this check we get a purple tint.
    if( tex2D(NormalSampler, input.TexCoord).a - 0.51f >= 0 )
    {
		emissive.rgba = 0;
    }

    float3 diffuseLight = light.rgb;

    if( g_lightsActive == false )
    {
		diffuseLight = float3(1,1,1);
    }

    float specularLight = light.a;
    float3 specular = specularColour * specularLight;

#ifdef SSAO
float2 ssao_offset = float2(g_halfPixel.x, 0);

 #ifdef SHADOW
   return float4((diffuseColor * tex2D(SSAOSampler,input.TexCoord + ssao_offset * -10.0f).r) * diffuseLight + specular,1) * (1.0f - tex2D(ShadowSampler,input.TexCoord).r) + emissive;
 #else
   return float4((diffuseColor * tex2D(SSAOSampler,input.TexCoord + ssao_offset * -10.0f).r) * diffuseLight + specular,1) + emissive;
 #endif
 
#else

 #ifdef SHADOW
   return float4( diffuseColor * diffuseLight + specular,1) * (1.0f - tex2D(ShadowSampler,input.TexCoord).r) + emissive;
 #else
   return float4( diffuseColor * diffuseLight + specular,1) + emissive;
 #endif
	// TEMP
    //return float4((/*diffuseColor */ ( gi ) /*+ specular*/),1) * shadow_term + emissive;
#endif
}

technique Technique1
{
    pass Pass1
    {
        VertexShader = compile vs_2_0 VertexShaderFunction();
        PixelShader = compile ps_2_0 PixelShaderFunction();
    }
}

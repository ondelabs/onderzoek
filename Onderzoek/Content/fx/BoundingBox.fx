float4x4 g_WVP;

float4x4 World;
float4x4 View;
float4x4 Projection;

float g_farDistance;

struct BBVertexToPixel
{
	float4 Position : POSITION;
	float4 Colour : COLOR0;
};

struct BBVertexToPixelDef
{
	float4 Position : POSITION;
	float4 Colour : COLOR0;
	float Depth : TEXCOORD0;
};

struct BBPixelToBuffer
{
	float4 Depth : COLOR0;
	float4 Diffuse : COLOR1;
	float4 Normal : COLOR2;
	float4 Material : COLOR3;
};

BBVertexToPixel BBVertexShader( float4 inPos : POSITION, float4 inColour : COLOR)
{
	BBVertexToPixel Output = (BBVertexToPixel)0;

	Output.Position = mul(inPos, g_WVP);
	Output.Colour = inColour;

	return Output;
}

float4 BBPixelShader(BBVertexToPixel Input) : COLOR0
{
	return Input.Colour;
}

BBVertexToPixelDef BBVertexShaderDef( float4 inPos : POSITION, float4 inColour : COLOR)
{
	BBVertexToPixelDef Output = (BBVertexToPixelDef)0;

    float4 worldPosition = mul(inPos, World);
    float4 viewPosition = mul(worldPosition, View);

    Output.Position = mul(viewPosition, Projection);
    Output.Depth = viewPosition.z;
	Output.Colour = inColour;

	return Output;
}

BBPixelToBuffer BBPixelShaderDef(BBVertexToPixelDef Input)
{
	BBPixelToBuffer rtn_val = (BBPixelToBuffer)0;
	
	rtn_val.Depth = float4( -Input.Depth / g_farDistance, 1.0f, 1.0f, 1.0f);
	rtn_val.Diffuse = Input.Colour;
	rtn_val.Normal = float4(0,1,0,0.5f);
	rtn_val.Material = float4(float3(0.01f,0.01f,0.01f),0.125f);
	
	return rtn_val;
}

 
technique BoundingBox
{
	pass Pass0
	{
		VertexShader = compile vs_2_0 BBVertexShader();
		PixelShader = compile ps_2_0 BBPixelShader();
	}
	
	// For deferred
	pass Pass1
	{
		VertexShader = compile vs_2_0 BBVertexShaderDef();
		PixelShader = compile ps_2_0 BBPixelShaderDef();
	}
}
// Original author for the skinned model shader parts:
//-----------------------------------------------------------------------------
// SkinnedModel.fx
//
// Microsoft Game Technology Group
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------

#ifdef SKINNEDMODEL

// Maximum number of bone matrices we can render using shader 2.0 in a single pass.
// If you change this, update SkinnedModelProcessor.cs to match.
#define MaxBones 59

float4x4 Bones[MaxBones];

#endif

float4x4 World;
float4x4 View;
float4x4 Projection;

float g_specularIntensity = 0.8f;

float3 g_cameraPosition;
float3 g_lightDirection;
float4 g_lightColour;
float g_lightIntensity;
bool g_lightEnabled;
float g_farDistance;

// Material data
float4 g_diffuse;
float4 g_ambient;
float4 g_emissive;
float4 g_specularColour;
float g_specularPower;

texture NoiseVolumeTexture;
sampler3D NoiseVolumeSampler = sampler_state
{
	Texture = <NoiseVolumeTexture>;

	AddressU = Wrap;
	AddressV = Wrap;
};

#ifdef TEXTUREMAP
	texture DiffuseMapTexture;
	sampler2D DiffuseMapSampler = sampler_state
	{
		Texture = <DiffuseMapTexture>;

		AddressU = Wrap;
		AddressV = Wrap;
	};
#endif

#ifdef NORMALMAP
	texture NormalMapTexture;
	sampler2D NormalMapSampler = sampler_state
	{
		Texture = <NormalMapTexture>;

		AddressU = Wrap;
		AddressV = Wrap;
	};
#endif

#ifdef SPECULARMAP
	texture SpecularMapTexture;
	sampler2D SpecularMapSampler = sampler_state
	{
		Texture = <SpecularMapTexture>;

		AddressU = Wrap;
		AddressV = Wrap;
	};
#endif


#ifdef NORMALMAP
	float3x3 CreateTBNMatrix( const float3 Tangent, const float3 Binormal, const float3 Normal )
	{
		float3x3 tbn_mat = { Tangent.x, Tangent.y, Tangent.z,
							 Binormal.x, Binormal.y, Binormal.z,
							 Normal.x, Normal.y, Normal.z };

		return tbn_mat;
	}

	float3 NormalMap( float3x3 TBNMat, const float2 TexCoor )
	{
		float3 rtn_val = 2.0f * tex2D( NormalMapSampler, TexCoor ) - 1.0f;
		return mul( rtn_val, TBNMat );
	}
#endif


struct VertexToPixel
{
    float4 Position : POSITION;
	float4 TexCoordAndDepth : TEXCOORD0;
    float3 Normal : TEXCOORD1;
    float3 WorldPosition : TEXCOORD2;
    float3 TexCoord3 : TEXCOORD3;

#ifdef NORMALMAP
	float3 Tangent : TEXCOORD4;
	float3 Binormal : TEXCOORD5;
#endif
};

struct PixelToBuffer
{
	float4 Diffuse : COLOR0;
	float4 Depth : COLOR1;
};


// Vertex shader input structure.
struct AppToVertex
{
    float4 Position : POSITION0;
    float3 Normal : NORMAL0;

#ifdef TEXTUREMAP
    float2 TexCoord : TEXCOORD0;
#endif
    
#ifdef SKINNEDMODEL
    float4 BoneIndices : BLENDINDICES0;
    float4 BoneWeights : BLENDWEIGHT0;
#endif

#ifdef NORMALMAP
    float3 Tangent : TANGENT;
    float3 Binormal : BINORMAL;
#endif
};


VertexToPixel PhongVertexShader( AppToVertex Input )
{
    VertexToPixel output = (VertexToPixel)0;

#ifdef SKINNEDMODEL
    // Blend between the weighted bone matrices.
    float4x4 skinTransform = 0;

    skinTransform += Bones[Input.BoneIndices.x] * Input.BoneWeights.x;
    skinTransform += Bones[Input.BoneIndices.y] * Input.BoneWeights.y;
    skinTransform += Bones[Input.BoneIndices.z] * Input.BoneWeights.z;
    skinTransform += Bones[Input.BoneIndices.w] * Input.BoneWeights.w;
    
    // Skin the vertex position.
    float4 position = mul(Input.Position, skinTransform);

    float4 worldPosition = mul(position, World);
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Projection);
	output.Normal = mul(mul(Input.Normal, skinTransform), World);

#else
    float4 worldPosition = mul(Input.Position, World);
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Projection);
	output.Normal = mul(Input.Normal, World);
#endif

#ifdef TEXTUREMAP
    output.TexCoordAndDepth.xy = Input.TexCoord;
#endif
	output.WorldPosition = worldPosition.xyz;
	output.TexCoord3 = Input.Position;
	output.TexCoordAndDepth.z = viewPosition.z;

#ifdef NORMALMAP
    output.Tangent = mul( Input.Tangent, World );
	output.Binormal = mul( Input.Binormal, World );
#endif

	return output;
}

PixelToBuffer PhongPixelShader(VertexToPixel Input) : COLOR
{
	PixelToBuffer output = (PixelToBuffer)0;

	float3 view_vec = g_cameraPosition - Input.WorldPosition;

float4 diffuse;
#ifndef TEXTUREMAP
	diffuse = g_diffuse;
#else
	// I guess, if there's a texture then there's no need for the original diffuse colour.
	diffuse = tex2D( DiffuseMapSampler, Input.TexCoordAndDepth.xy ) * g_diffuse;
#endif

	if( g_lightEnabled == true )
	{
		float3 normal;
#ifndef NORMALMAP
		normal = Input.Normal;
#else
		// Get normal from normal map	
		float3x3 tbn_mat = CreateTBNMatrix( Input.Tangent, Input.Binormal, Input.Normal );
		normal = NormalMap( tbn_mat, Input.TexCoordAndDepth.xy );
#endif

		float3 unit_view_vec = normalize( view_vec );
		float3 unit_norm = normalize(normal);

		// Work out phong shade
		float light_vec_norm = dot( g_lightDirection, unit_norm );
		float angle = clamp( light_vec_norm, 0, 1 );

		float4 vertex_intensity = g_lightColour * angle;

		diffuse *= vertex_intensity;

		float3 reflection1 = ( unit_norm * light_vec_norm * 2.0f ) - g_lightDirection;
		float3 reflection = normalize( reflection1 );

		float intensity = dot( reflection, unit_view_vec );
		if( intensity < 0 )
		{
		    intensity = 0;
		}

float spec1;
#ifndef SPECULARMAP
		spec1 = pow( intensity, g_specularPower );
#else
		spec1 = pow( intensity, tex2D( SpecularMapSampler, Input.TexCoordAndDepth.xy ).r );
#endif

		float noisy = tex3D(NoiseVolumeSampler, Input.TexCoord3 * 0.04f).r;
		float3 fp = frac(0.7f * Input.TexCoord3 + 9.0f * noisy + 0.1f * view_vec);
		fp *= (1 - fp);
		float glitter = saturate(1 - 7 * (fp.x + fp.y + fp.z));
		float glittering = glitter * pow(intensity, 1.5);

		float4 specular = (g_specularColour * spec1) * g_lightColour;
		
		diffuse = ( diffuse * g_lightIntensity ) + specular + glittering * 2.0f;
	}

	output.Diffuse = diffuse + g_ambient + g_emissive;
	output.Depth = float4( -Input.TexCoordAndDepth.z / g_farDistance, 1.0f, 1.0f, 1.0f);

    return output;
}


technique PhongTechnique
{
    pass Pass0
    {
        VertexShader = compile vs_2_0 PhongVertexShader();
        PixelShader = compile ps_2_0 PhongPixelShader();
    }
}
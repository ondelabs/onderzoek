float4x4 World;
float4x4 View;
float4x4 Projection;
float g_farDistance;

#define MAX_SHADER_MATRICES 13


float4x4 InstanceTransforms[MAX_SHADER_MATRICES];
float4 InstanceTint[MAX_SHADER_MATRICES];

// Shared material data
float4 g_tint;

#ifdef TEXTUREMAP

 #ifdef ANIMATION
int InstanceFrameNumbers[MAX_SHADER_MATRICES];

float g_widthOfFrame;
int g_frameNumber;
 #endif

	texture DiffuseMapTexture;
	sampler2D DiffuseMapSampler = sampler_state
	{
		Texture = <DiffuseMapTexture>;

		AddressU = Wrap;
		AddressV = Wrap;
	};
#endif

#ifdef SOFT
	texture DepthMapTexture;
	sampler2D DepthMapSampler = sampler_state
	{
		Texture = <DepthMapTexture>;

		//AddressU = Clamp;
		//AddressV = Clamp;
	};

#endif


struct VertexToPixel
{
    float4 Position : POSITION;
	float4 TexCoordAndDepth : TEXCOORD0;
};

struct PixelToBuffer
{
	float4 Diffuse : COLOR0;
	float4 Depth : COLOR1;
};


struct VertexToPixelSI
{
    float4 Position : POSITION;
	float4 TexCoordAndDepth : TEXCOORD0;
	float Index : TEXCOORD1;
};


struct VertexToPixelHI
{
    float4 Position : POSITION;
	float4 TexCoordAndDepth : TEXCOORD0;
	float4 Tint : TEXCOORD1;
#ifdef SOFT
	float Depth : TEXCOORD2;
	float4 ScreenPosition : TEXCOORD3;
#endif
#ifdef ANIMATION
	int FrameNum : TEXCOORD4;
#endif
};


// Vertex shader input structure.
struct AppToVertex
{
    float4 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};


VertexToPixel PRVertexShader( AppToVertex Input )
{
    VertexToPixel output = (VertexToPixel)0;

    float4 worldPosition = mul(Input.Position, World);
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Projection);
    output.TexCoordAndDepth.xy = Input.TexCoord;
    output.TexCoordAndDepth.z = viewPosition.z;

	return output;
}


VertexToPixelSI PRVertexShaderSI( AppToVertex Input, float Index: TEXCOORD1 )
{
    VertexToPixelSI output = (VertexToPixelSI)0;

    float4 worldPosition = mul(Input.Position, InstanceTransforms[Index]);
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Projection);
    output.TexCoordAndDepth.xy = Input.TexCoord;
    output.TexCoordAndDepth.z = viewPosition.z;
    output.Index = Index;

	return output;
}


VertexToPixelHI PRVertexShaderHI( AppToVertex Input, float4x4 WorldMatrix: TEXCOORD1, float4 Tint: TEXCOORD5, int FrameNum: TEXCOORD6 )
{
    VertexToPixelHI output = (VertexToPixelHI)0;

    float4 worldPosition = mul(Input.Position, transpose(WorldMatrix));
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Projection);
    output.TexCoordAndDepth.xy = Input.TexCoord;
    output.TexCoordAndDepth.z = viewPosition.z;
    output.Tint = Tint;
    
#ifdef SOFT
	output.Depth = -viewPosition.z;
	output.ScreenPosition = output.Position;//0.5f * output.Position.xy / output.Position.w + float2(0.5f, 0.5f);
	//output.ScreenPosition.y = 1.0f - output.ScreenPosition.y;
#endif

#ifdef ANIMATION
	output.FrameNum = FrameNum;
#endif

	return output;
}


PixelToBuffer PRPixelShader(VertexToPixel Input) : COLOR
{
	PixelToBuffer output = (PixelToBuffer)0;
	
	output.Depth = float4( -Input.TexCoordAndDepth.z / g_farDistance, 1.0f, 1.0f, 1.0f);

#ifndef TEXTUREMAP
	output.Diffuse = g_tint;
#else
 #ifdef ANIMATION
	float2 new_tex_coord = Input.TexCoordAndDepth.xy;
	new_tex_coord.x = (new_tex_coord.x * g_widthOfFrame) + (g_frameNumber * g_widthOfFrame);
	output.Diffuse = tex2D( DiffuseMapSampler, new_tex_coord ) * g_tint;
 #else
	output.Diffuse = tex2D( DiffuseMapSampler, Input.TexCoordAndDepth.xy ) * g_tint;
 #endif
#endif

	return output;
}


PixelToBuffer PRPixelShaderSI(VertexToPixelSI Input) : COLOR
{
	PixelToBuffer output = (PixelToBuffer)0;
	
	output.Depth = float4( -Input.TexCoordAndDepth.z / g_farDistance, 1.0f, 1.0f, 1.0f);
	
#ifndef TEXTUREMAP
	output.Diffuse = InstanceTint[Input.Index];
#else
 #ifdef ANIMATION
	float2 new_tex_coord = Input.TexCoordAndDepth.xy;
	new_tex_coord.x = (new_tex_coord.x * g_widthOfFrame) + (InstanceFrameNumbers[Input.Index] * g_widthOfFrame);
	output.Diffuse = tex2D( DiffuseMapSampler, new_tex_coord ) * InstanceTint[Input.Index];
 #else
	output.Diffuse = tex2D( DiffuseMapSampler, Input.TexCoordAndDepth.xy ) * InstanceTint[Input.Index];
 #endif
#endif

	return output;
}


PixelToBuffer PRPixelShaderHI(VertexToPixelHI Input) : COLOR
{
	float zFade = 1.0f;
	
	PixelToBuffer output = (PixelToBuffer)0;
	
	output.Depth = float4( -Input.TexCoordAndDepth.z / g_farDistance, 1.0f, 1.0f, 1.0f);

#ifdef SOFT

	float2 texture_coord = 0.5f * Input.ScreenPosition.xy / Input.ScreenPosition.w + float2(0.5f, 0.5f);
	texture_coord.y = 1.0f - texture_coord.y;

	float depth = tex2D( DepthMapSampler, texture_coord ).r * g_farDistance;

	float Output = (depth - Input.Depth) / 5.0f;

    if ((Output < 1) && (Output > 0))
    {
        // Make it fade smoothly from 0 and 1 - thanks nVidia
        zFade = 0.5 * pow(saturate(2 * ((Output > 0.5) ? (1 - Output) : Output)), 1.0f);
        zFade = (Output > 0.5) ? (1 - zFade) : zFade;
    }
    else 
    {
        zFade = saturate(Output);
    }

#endif

#ifndef TEXTUREMAP
	output.Diffuse = Input.Tint * zFade;
#else
 #ifdef ANIMATION
	float2 new_tex_coord = float2((Input.TexCoordAndDepth.x * g_widthOfFrame) + (Input.FrameNum * g_widthOfFrame), Input.TexCoordAndDepth.y);
	output.Diffuse = ( tex2D( DiffuseMapSampler, new_tex_coord ) * Input.Tint ) * zFade;
 #else
	output.Diffuse = ( tex2D( DiffuseMapSampler, Input.TexCoordAndDepth.xy ) * Input.Tint ) * zFade;
 #endif
#endif

	return output;
}


technique ParticleRenderTechnique
{
	// This pass is for rendering particles without instancing.
    pass Pass0
    {
        VertexShader = compile vs_2_0 PRVertexShader();
        PixelShader = compile ps_2_0 PRPixelShader();
    }
    
    // This pass is for rendering particles with shader instancing.
    pass Pass1
    {
        VertexShader = compile vs_2_0 PRVertexShaderSI();
        PixelShader = compile ps_2_0 PRPixelShaderSI();
    }
    
    // This pass is for rendering particles with hardware instancing.
    pass Pass2
    {
        VertexShader = compile vs_3_0 PRVertexShaderHI();
        PixelShader = compile ps_3_0 PRPixelShaderHI();
    }
}
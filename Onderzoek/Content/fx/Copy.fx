float2 g_halfPixel;

texture Texture0;
texture Texture1;
texture Texture2;

sampler Sampler0 : register(s0) = sampler_state
{
	Texture = (Texture0);

    //AddressU = Clamp;
    //AddressV = Clamp;
};

sampler Sampler1 : register(s1) = sampler_state
{
	Texture = (Texture1);

    //AddressU = Clamp;
    //AddressV = Clamp;
};

sampler Sampler2 : register(s2) = sampler_state
{
	Texture = (Texture2);

    //AddressU = Clamp;
    //AddressV = Clamp;
};


struct AppToVertex
{
    float3 Position : POSITION0;
    float3 TexCoord : TEXCOORD0;
};

struct VertexToPixel
{
    float4 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};

struct PixelToBuffer
{
	float4 Colour0 : COLOR0;
	float4 Colour1 : COLOR1;
	float4 Colour2 : COLOR2;
};


VertexToPixel CopyVertexShader( AppToVertex Input )
{
	VertexToPixel output = (VertexToPixel)0;

    output.Position = float4(Input.Position,1);
    output.TexCoord = Input.TexCoord.xy + g_halfPixel;
    
    return output;
}

PixelToBuffer CopyPixelShader( VertexToPixel Input )
{
	PixelToBuffer rtn_val = (PixelToBuffer)0;
	
	rtn_val.Colour0 = tex2D( Sampler0, Input.TexCoord );
	rtn_val.Colour1 = tex2D( Sampler1, Input.TexCoord );
	rtn_val.Colour2 = tex2D( Sampler2, Input.TexCoord );
	
	return rtn_val;
}

float4 CopyPixelShader1( VertexToPixel Input ) : COLOR0
{
	return tex2D( Sampler0, Input.TexCoord );
}


technique Copy
{
    pass Pass0
    {
        VertexShader = compile vs_2_0 CopyVertexShader();
        PixelShader = compile ps_2_0 CopyPixelShader();
    }
    
    pass Pass1
    {
        VertexShader = compile vs_2_0 CopyVertexShader();
        PixelShader = compile ps_2_0 CopyPixelShader1();
    }
}
// Original author for the skinned model shader parts:
//-----------------------------------------------------------------------------
// SkinnedModel.fx
//
// Microsoft Game Technology Group
// Copyright (C) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------

#ifdef SKINNEDMODEL

// Maximum number of bone matrices we can render using shader 2.0 in a single pass.
// If you change this, update SkinnedModelProcessor.cs to match.
#define MaxBones 59

float4x4 Bones[MaxBones];

#endif

float4x4 World;
float4x4 View;
float4x4 Projection;
float4x4 TransInvWorldView;

float g_specularIntensity = 0.8f;

float g_farDistance;

// Material data
float4 g_diffuse;
float4 g_ambient;
float4 g_emissive;
float4 g_specularColour;
float g_specularPower;

#ifdef TEXTUREMAP
	texture DiffuseMapTexture;
	sampler2D DiffuseMapSampler = sampler_state
	{
		Texture = <DiffuseMapTexture>;

		AddressU = Wrap;
		AddressV = Wrap;
	};
#endif

#ifdef NORMALMAP
	texture NormalMapTexture;
	sampler2D NormalMapSampler = sampler_state
	{
		Texture = <NormalMapTexture>;

		AddressU = Wrap;
		AddressV = Wrap;
	};
#endif

#ifdef SPECULARMAP
	texture SpecularMapTexture;
	sampler2D SpecularMapSampler = sampler_state
	{
		Texture = <SpecularMapTexture>;

		AddressU = Wrap;
		AddressV = Wrap;
	};
#endif


#ifdef NORMALMAP
	float3x3 CreateTBNMatrix( const float3 Tangent, const float3 Binormal, const float3 Normal )
	{
		float3x3 tbn_mat = { Tangent.x, Tangent.y, Tangent.z,
							 Binormal.x, Binormal.y, Binormal.z,
							 Normal.x, Normal.y, Normal.z };

		return tbn_mat;
	}

	float3 NormalMap( float3x3 TBNMat, const float2 TexCoor )
	{
		float3 rtn_val = 2.0f * tex2D( NormalMapSampler, TexCoor ) - 1.0f;
		return mul( rtn_val, TBNMat );
	}
#endif


struct VertexToPixel
{
    float4 Position : POSITION;
    
#ifndef NOVERTEXDEPTH
    float2 Depth : TEXCOORD0;
#endif

#ifndef SHADOW
	float2 TexCoord : TEXCOORD1;

    float3 Normal : TEXCOORD2;

  #ifdef NORMALMAP
	float3 Tangent : TEXCOORD4;
	float3 Binormal : TEXCOORD5;
  #endif
#endif
};


struct PixelToBuffer
{
	float4 Depth : COLOR0;

#ifndef SHADOW
	float4 Diffuse : COLOR1;
	float4 Normal : COLOR2;
 
	float4 Material : COLOR3;
#endif
};


// Vertex shader input structure.
struct AppToVertex
{
    float4 Position : POSITION0;
    float3 Normal : NORMAL0;

#ifdef TEXTUREMAP
    float2 TexCoord : TEXCOORD0;
#endif
    
#ifdef SKINNEDMODEL
    float4 BoneIndices : BLENDINDICES0;
    float4 BoneWeights : BLENDWEIGHT0;
#endif

#ifdef NORMALMAP
    float3 Tangent : TANGENT;
    float3 Binormal : BINORMAL;
#endif
};


VertexToPixel PhongVertexShader( AppToVertex Input )
{
    VertexToPixel output = (VertexToPixel)0;

	float3x3 modelViewRotXf;
    modelViewRotXf[0] = TransInvWorldView[0].xyz;
    modelViewRotXf[1] = TransInvWorldView[1].xyz;
    modelViewRotXf[2] = TransInvWorldView[2].xyz;

#ifdef SKINNEDMODEL
    // Blend between the weighted bone matrices.
    float4x4 skinTransform = 0;

    skinTransform += Bones[Input.BoneIndices.x] * Input.BoneWeights.x;
    skinTransform += Bones[Input.BoneIndices.y] * Input.BoneWeights.y;
    skinTransform += Bones[Input.BoneIndices.z] * Input.BoneWeights.z;
    skinTransform += Bones[Input.BoneIndices.w] * Input.BoneWeights.w;
    
    // Skin the vertex position.
    float4 position = mul(Input.Position, skinTransform);

    float4 worldPosition = mul(position, World);
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Projection);

 #ifndef SHADOW
   output.Normal = mul(mul(Input.Normal, skinTransform), modelViewRotXf);//modelViewRotXf
 #endif
#else
    float4 worldPosition = mul(Input.Position, World);
    float4 viewPosition = mul(worldPosition, View);
    output.Position = mul(viewPosition, Projection);
    
 #ifndef SHADOW
   output.Normal = mul(Input.Normal, modelViewRotXf);//modelViewRotXf
 #endif
#endif

#ifndef SHADOW

 #ifdef TEXTUREMAP
    output.TexCoord = Input.TexCoord;
 #endif
    
    output.Depth.x = viewPosition.z;
    //output.Depth.y = output.Position.w;
#else
    output.Depth.x = output.Position.z / output.Position.w;
#endif

 #ifdef NORMALMAP
    output.Tangent = mul( Input.Tangent, World );
	output.Binormal = mul( Input.Binormal, World );
 #endif

	return output;
}


PixelToBuffer PhongPixelShader(VertexToPixel Input)
{
	PixelToBuffer output = (PixelToBuffer)0;

#ifndef SHADOW

    output.Diffuse = g_diffuse;
 #ifdef TEXTUREMAP
    //output.Diffuse += tex2D( DiffuseMapSampler, Input.TexCoord ); // + g_ambient + g_emissive;
    //output.Diffuse *= tex2D( DiffuseMapSampler, Input.TexCoord );

    output.Diffuse = tex2D( DiffuseMapSampler, Input.TexCoord ) * g_diffuse;
 #endif

	//output Depth
	output.Depth = float4( -Input.Depth.x / g_farDistance, 1.0f, 1.0f, 1.0f);//Input.Depth.y;
 
#else

	output.Depth = float4( Input.Depth.x, 1, 1, 1 );//float4( Input.Depth.x / g_farDistance, 1.0f, 1.0f, 1.0f );

#endif

#ifndef SHADOW
	float3 normal = Input.Normal;

	//output SpecularIntensity
	output.Diffuse.a = g_specularIntensity;
 
	// Get normal from normal map	
  #ifdef NORMALMAP
	float3x3 tbn_mat = CreateTBNMatrix( Input.Tangent, Input.Binormal, Input.Normal );
	normal = NormalMap( tbn_mat, Input.TexCoord );
  #endif

	output.Normal.rgb = 0.5f * (normalize(normal) + 1.0f);    //transform normal domain
	output.Normal.a = 0.5f;

	output.Material.rgb = ( g_specularColour / 100.0f ) + (g_emissive + g_ambient);
	output.Material.a = g_specularPower;
  #ifdef SPECULARMAP
	output.Material.a = tex2D( SpecularMapSampler, Input.TexCoord ).r;
  #endif

#endif

    return output;
}


technique PhongTechnique
{
    pass Pass0
    {
        VertexShader = compile vs_2_0 PhongVertexShader();
        PixelShader = compile ps_2_0 PhongPixelShader();
        
		//SrcBlend = one;
		//DestBlend = Zero;
    }
}
/////////////////////////////////////
// Variable declaration
/////////////////////////////////////

// transformations
float4x4 g_lightVPMat;
float4x4 g_invCamera;

float2 g_halfPixel;
float3 g_corners[4];
float2 g_shadowMapSize;

float g_bias;
float g_shadowTerm;

// Light variables
float g_lightRange;

// Textures
texture FrmShadowTexture;
texture DiffuseTexture;
texture ShadowTexture;
texture DepthTexture;

sampler FrmShadowSampler = sampler_state
{
    Texture = (FrmShadowTexture);

};

sampler DiffuseSampler = sampler_state
{
    Texture = (DiffuseTexture);

};

sampler ShadowSampler = sampler_state
{
    Texture = (ShadowTexture);

};

sampler DepthSampler = sampler_state
{
	Texture = (DepthTexture);

};


/////////////////////////////////////
// Structs
/////////////////////////////////////

struct SPSInputShadow
{
	float4 Pos : POSITION;
	float2 TexCoord : TEXCOORD0;
	float3 ViewDir : TEXCOORD1;
};


/////////////////////////////////////
// Functions
/////////////////////////////////////

// Calculates the shadow occlusion using bilinear PCF
float CalcShadowTermPCF(float LightDepth, float2 ShadowTexCoord)
{
	float shadow_term = 0.0f;

	// transform to texel space
	float2 shadow_map_coord = g_shadowMapSize * ShadowTexCoord;

	// Determine the lerp amounts
	float2 vLerps = frac(ShadowTexCoord);

	// Read in the 4 samples, doing a depth check for each
	float samples[4];
	samples[0] = (tex2D(ShadowSampler, ShadowTexCoord).x + g_bias < LightDepth) ? g_shadowTerm: 0.0f;
	samples[1] = (tex2D(ShadowSampler, ShadowTexCoord + float2(1.0/g_shadowMapSize.x, 0)).x + g_bias < LightDepth) ? g_shadowTerm: 0.0f;
	samples[2] = (tex2D(ShadowSampler, ShadowTexCoord + float2(0, 1.0/g_shadowMapSize.y)).x + g_bias < LightDepth) ? g_shadowTerm: 0.0f;
	samples[3] = (tex2D(ShadowSampler, ShadowTexCoord + float2(1.0/g_shadowMapSize.x, 1.0/g_shadowMapSize.y)).x + g_bias < LightDepth) ? g_shadowTerm: 0.0f;

	// lerp between the shadow values to calculate our light amount
	shadow_term = lerp(lerp(samples[0], samples[1], vLerps.x), lerp( samples[2], samples[3], vLerps.x), vLerps.y);

	//shadow_term = 0.0f;
	//if( tex2D(ShadowSampler, ShadowTexCoord).x + g_bias < LightDepth )
	//{
		//shadow_term = g_shadowTerm;
	//}

	return shadow_term;
}


/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////SHADOW SHADER STUFF/////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////
// Vertex Shader
/////////////////////////////////////

SPSInputShadow VSMainShadowProcessor( float3 InPos : POSITION, float3 InTexCoord : TEXCOORD0 )
{
	SPSInputShadow Out = (SPSInputShadow)0;

	Out.Pos = float4(InPos,1);
	Out.TexCoord = InTexCoord.xy;
	Out.ViewDir = g_corners[InTexCoord.z];

	return Out;
}


/////////////////////////////////////
// Pixel Shader
/////////////////////////////////////

float g_near;
float g_far;

float4 PSMainShadowProcessor( SPSInputShadow Input ) : COLOR
{
	// Reconstruct view-space position from the depth buffer
	float pixel_depth = tex2D(DepthSampler, Input.TexCoord).r;
	
	if( pixel_depth == 0 )
	{
		clip(-1);
	}
	
	float4 position_vs = float4(pixel_depth * Input.ViewDir, 1.0f);
	
	// Do not process the fragments that are not within the bounds of the near and far planes.
	float depth = -position_vs.z;
	clip(depth - g_near);
	if( depth > g_far )
	{
		clip(-1);
	}
	
	// Determine the depth of the pixel with respect to the light
	float4x4 matViewToLightViewProj = mul(g_invCamera, g_lightVPMat);
	float4 position_light = mul(position_vs, matViewToLightViewProj);
	
	float light_depth = position_light.z / position_light.w;
	
	// Transform from light space to shadow map texture space.
    float2 shadow_tex_coord = 0.5 * position_light.xy / position_light.w + float2(0.5f, 0.5f);
    shadow_tex_coord.y = 1.0f - shadow_tex_coord.y;
	
	// Get the shadow occlusion factor and output it
	float shadow_term = g_shadowTerm;
	if( shadow_tex_coord.x <= 1.0f && shadow_tex_coord.y <= 1.0f && shadow_tex_coord.x >= 0 && shadow_tex_coord.y >= 0 )
	{
		shadow_term = CalcShadowTermPCF(light_depth, shadow_tex_coord);
	}

	return float4(shadow_term, 0, 0, 1.0f);
}


struct VertexShaderInput
{
    float3 Position : POSITION0;
    float3 TexCoord : TEXCOORD0;
};

struct VertexShaderOutput
{
    float4 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};

VertexShaderOutput VSAddShadow(VertexShaderInput input)
{
    VertexShaderOutput output;
    output.Position = float4(input.Position,1);
    output.TexCoord = input.TexCoord.xy + g_halfPixel;
    return output;
}

float4 PSAddShadow(VertexShaderOutput input) : COLOR0
{
    float3 diffuseColor = tex2D(DiffuseSampler,input.TexCoord).rgb;

    float shadow_term = 1.0f - tex2D(FrmShadowSampler,input.TexCoord).r;
    
    return float4( diffuseColor * shadow_term, 1.0f );
}


/////////////////////////////////////
// Effect techniques
/////////////////////////////////////

technique ShadowProcessor
{
    pass P0
    {
        VertexShader = compile vs_2_0 VSMainShadowProcessor();
        PixelShader = compile ps_2_0 PSMainShadowProcessor();
    }
    pass P1
    {
        VertexShader = compile vs_2_0 VSAddShadow();
        PixelShader = compile ps_2_0 PSAddShadow();
    }
}
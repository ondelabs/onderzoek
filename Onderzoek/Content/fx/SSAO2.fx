// Still a work in progress

float2 g_halfPixel;
float2 g_resolution;

float4x4 g_iProj;

texture DepthTexture;
texture NormalTexture;
texture RNTexture;
texture DiffuseTexture;

sampler DepthSampler : register(s0) = sampler_state
{
	Texture = (DepthTexture);

    //AddressU = Clamp;
    //AddressV = Clamp;
};

sampler NormalSampler : register(s1) = sampler_state
{
	Texture = (NormalTexture);

    //AddressU = Clamp;
    //AddressV = Clamp;
};

sampler RNSampler : register(s2) = sampler_state
{
	Texture = (RNTexture);

    //AddressU = Clamp;
    //AddressV = Clamp;
};

sampler DiffuseSampler : register(s3) = sampler_state
{
	Texture = (DiffuseTexture);

    //AddressU = Clamp;
    //AddressV = Clamp;
};

struct AppToVertex
{
    float4 Position : POSITION0;
    float3 TexCoord : TEXCOORD0;
};

struct VertexToPixel
{
    float4 Position : POSITION0;
    float2 TexCoord : TEXCOORD0;
};


VertexToPixel SSAOVertexShader( AppToVertex Input )
{
	VertexToPixel output = (VertexToPixel)0;

    output.Position = Input.Position;
    output.TexCoord = Input.TexCoord.xy + g_halfPixel;
    
    return output;
}


float3 readNormal(float2 TexCoord)  
{  
     return normalize( tex2D( NormalSampler, TexCoord ).xyz * 2.0f - 1.0f );  
}

float3	PosFromDepth (float2 UV)
{
	float	Sample=tex2D(DepthSampler,UV).r;
	float	Depth=Sample*1000.0f;
	float4	Pos=float4((UV.x-0.5)*2,(0.5-UV.y)*2,1,1);
	float4	Ray=mul(Pos,g_iProj);
	return	Ray.xyz*Depth;
}

//Ambient Occlusion form factor:
float AOFF(float3 ddiff,float3 cnorm,float2 UV)
{
	float3	vv=normalize(ddiff);
	float	rd=length(ddiff);
	return (1-clamp(dot(readNormal(UV),-vv),0,1))*clamp(dot(cnorm,vv),0,1)* (1-1/sqrt(1/(rd*rd)+1));
}


float4 SSAOPixelShader( VertexToPixel Input ) : COLOR0
{
	float2 UV = Input.TexCoord;

	float3	n=readNormal(UV);										
	float3	p=PosFromDepth(UV);										
   
    //randomization texture
    float2 fres = float2(g_resolution.x,g_resolution.y);
    float3 Random = tex2D(RNSampler, Input.TexCoord * fres.xy);
    Random = Random * 2.0 - float3(1.0f, 1.0f, 1.0f);

	float	ao=0;
	float	incx=1.0f/g_resolution.x*10.0f;
	float	incy=1.0f/g_resolution.y*10.0f;
	float	pw=incx;
	float	ph=incy;

	float	Sample=tex2D(DepthSampler,UV).r;								
	float	CDepth=1.0f/(Sample*1000.0f);			

	for (float i=0;i<3;++i)											 
	{																
		float npw = (pw+Random.x)*CDepth;
		float nph = (ph+Random.y)*CDepth;

		float2	UV1=UV+float2(npw,nph);								
		float3	Dif1=PosFromDepth(UV1)-p;							
		ao+=	AOFF(Dif1,n,UV1);									

		float2	UV2=UV+float2(npw,-nph);							
		float3	Dif2=PosFromDepth(UV2)-p;							
		ao+=	AOFF(Dif2,n,UV2);									

		float2	UV3=UV+float2(-npw,nph);							
		float3	Dif3=PosFromDepth(UV3)-p;							
		ao+=	AOFF(Dif3,n,UV3);									

		float2	UV4=UV+float2(-npw,-nph);							
		float3	Dif4=PosFromDepth(UV4)-p;							
		ao+=	AOFF(Dif4,n,UV4);									

		float2	UV5=UV+float2(0,nph);								
		float3	Dif5=PosFromDepth(UV5)-p;							
		ao+=	AOFF(Dif5,n,UV5);									

		float2	UV6=UV+float2(0,-nph);								
		float3	Dif6=PosFromDepth(UV6)-p;							
		ao+=	AOFF(Dif6,n,UV6);									

		float2	UV7=UV+float2(npw,0);								
		float3	Dif7=PosFromDepth(UV7)-p;							
		ao+=	AOFF(Dif7,n,UV7);									

		float2	UV8=UV+float2(-npw,0);								
		float3	Dif8=PosFromDepth(UV8)-p;							
		ao+=	AOFF(Dif8,n,UV8);
	   
   		pw += incx;
		ph += incy;
	}

	ao = 1.0f - (ao/10);
	return	float4(ao, 0, 0, 1.0f);	
}

technique SSAO
{
    pass Pass0
    {
        VertexShader = compile vs_3_0 SSAOVertexShader();
        PixelShader = compile ps_3_0 SSAOPixelShader();
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using Onderzoek.SpatialTree;
using Onderzoek.Entity;
using Onderzoek.Camera;
using Onderzoek.Physics;

namespace Onderzoek.SpatialTree
{
    public class CCompositeSpatialTree : CSpatialTree
    {
        public struct SRoomData
        {
            public Vector3 s_numRoomBranches;
            public Dictionary<uint, List<STreeInput>> s_entityDictionary;
            public List<CMainRoomCompositeSpatialTree> s_roomList;
        }

        protected List<CSpatialTree> m_branchList;
        protected uint m_numBranchesW;
        protected uint m_numBranchesH;
        protected uint m_numBranchesB;
        protected uint m_totalBranches;
        protected Vector3 m_min;
        protected Vector3 m_max;
        protected BoundingSphere m_sphere;
        protected float m_maxLengthOfThisSpace;

        // This is the one to use
        public bool Initialise(Vector3 Min, Vector3 Max, Vector3 NumBraches, Vector3 NumRoomBranches, List<STreeInput> EntityList, out List<CMainRoomCompositeSpatialTree> RoomList, List<CEntity> ActualEntityList)
        {
            SRoomData Room_data = new SRoomData();
            Room_data.s_numRoomBranches = NumRoomBranches;
            Room_data.s_entityDictionary = new Dictionary<uint, List<STreeInput>>();
            Room_data.s_roomList = new List<CMainRoomCompositeSpatialTree>();

            RemoveContainedObjects(EntityList, Room_data.s_entityDictionary);

            bool rtn = Initialise(Min, Max, NumBraches, EntityList, Room_data, null, ActualEntityList);

            RoomList = Room_data.s_roomList;

            return rtn;
        }

        public Vector3 GetMin()
        {
            return m_min;
        }

        public Vector3 GetMax()
        {
            return m_max;
        }

        public void SetMin(Vector3 Min)
        {
            m_min = Min;
            CreateSphereData();
        }

        public void SetMax(Vector3 Max)
        {
            m_max = Max;
            CreateSphereData();
        }

        public float GetMaxLengthOfThisSpace()
        {
            return m_maxLengthOfThisSpace;
        }

        // PLEASE remeber to use WaitMutex and ReleaseMutex in
        // the function that is using this data!
        public List<CSpatialTree> GetBranchList()
        {
            return m_branchList;
        }

        protected void CreateSphereData()
        {
            m_sphere.Radius = Vector3.Distance(m_min, m_max) / 2.0f;
            m_sphere.Center = (m_min + m_max) / 2.0f;
        }

        // Internal initalisation
        private bool Initialise(Vector3 Min, Vector3 Max, Vector3 NumBraches, List<STreeInput> EntityList, SRoomData RoomData, CSpatialTree Parent, List<CEntity> ActualEntityList)
        {
            m_isLeaf = false;
            m_maxLengthOfThisSpace = Vector3.Distance(new Vector3(Max.X, Min.Y, Max.Z), Min);
            m_parent = Parent;
            m_branchList = new List<CSpatialTree>();

            m_min = Min;
            m_max = Max;
            CreateSphereData();
            m_numBranchesW = (uint)NumBraches.X;
            m_numBranchesH = (uint)NumBraches.Y;
            m_numBranchesB = (uint)NumBraches.Z;
            m_totalBranches = m_numBranchesW * m_numBranchesB * m_numBranchesH;

            if (EntityList.Count > m_totalBranches)
            {
                Vector3[] min = new Vector3[m_totalBranches];
                Vector3[] max = new Vector3[m_totalBranches];
                CreateMinMaxForChild(min, max);

                return Sort(EntityList, min, max, RoomData, ActualEntityList);
            }
            else
            {
                return Sort(EntityList, RoomData, ActualEntityList);
            }
        }

        // Internal initalisation
        protected bool Initialise(Vector3 Min, Vector3 Max, Vector3 NumBraches, List<STreeInput> EntityList, CSpatialTree Parent, List<CEntity> ActualEntityList)
        {
            m_isLeaf = false;
            m_maxLengthOfThisSpace = Vector3.Distance(new Vector3(Max.X, Min.Y, Max.Z), Min);
            m_parent = Parent;
            m_branchList = new List<CSpatialTree>();

            m_min = Min;
            m_max = Max;
            CreateSphereData();
            m_numBranchesW = (uint)NumBraches.X;
            m_numBranchesH = (uint)NumBraches.Y;
            m_numBranchesB = (uint)NumBraches.Z;
            m_totalBranches = m_numBranchesW * m_numBranchesB * m_numBranchesH;

            if (EntityList.Count > m_totalBranches)
            {
                Vector3[] min = new Vector3[m_totalBranches];
                Vector3[] max = new Vector3[m_totalBranches];
                CreateMinMaxForChild(min, max);

                return Sort(EntityList, min, max, ActualEntityList);
            }
            else
            {
                return Sort(EntityList, ActualEntityList);
            }
        }

        private void RemoveContainedObjects(List<STreeInput> EntityList, Dictionary<uint, List<STreeInput>> EntityDictionary)
        {
            for (int i = 0; i < EntityList.Count; ++i)
            {
                if (EntityList[i].s_inRoom == true)
                {
                    if (EntityDictionary.ContainsKey(EntityList[i].s_RoomID) == true)
                    {
                        EntityDictionary[EntityList[i].s_RoomID].Add(EntityList[i]);
                    }
                    else
                    {
                        EntityDictionary.Add(EntityList[i].s_RoomID, new List<STreeInput>());
                        EntityDictionary[EntityList[i].s_RoomID].Add(EntityList[i]);
                    }

                    EntityList.Remove(EntityList[i]);
                    --i;
                }
            }
        }

        public override void CheckWithCameraPlanes(CCamera CameraInstance, List<CEntity> EntityList, List<int> Renderable)
        {
            if (CToolbox.ClipWithPlanes(m_min, m_max, CameraInstance.GetClipPlanes()) == true)
            {
                for (int i = 0; i < m_branchList.Count; ++i)
                {
                    m_branchList[i].CheckWithCameraPlanes(CameraInstance, EntityList, Renderable);
                }
            }
        }

        public override void CheckWithCameraPlanes(List<Plane[]> PlaneList, List<CEntity> EntityList, List<int> Renderable)
        {
            List<Plane[]> temp = new List<Plane[]>(); ;
            for (int j = 0; j < PlaneList.Count; ++j)
            {
                if (CToolbox.ClipWithPlanes(m_min, m_max, PlaneList[j]) == true)
                {
                    temp.Add(PlaneList[j]);
                }
            }

            if (temp.Count > 0)
            {
                for (int i = 0; i < m_branchList.Count; ++i)
                {
                    m_branchList[i].CheckWithCameraPlanes(PlaneList, EntityList, Renderable);
                }
            }

            //for (int j = 0; j < PlaneList.Count; ++j)
            //{
            //    if (CToolbox.ClipWithPlanes(m_min, m_max, PlaneList[j]) == true)
            //    {
            //        for (int i = 0; i < m_branchList.Count; ++i)
            //        {
            //            m_branchList[i].CheckWithCameraPlanes(PlaneList[j], EntityList, Renderable);
            //        }
            //    }
            //}
        }

        public override void CheckWithCameraPlanes(Plane[] Planes, List<CEntity> EntityList, List<int> Renderable)
        {
            if (CToolbox.ClipWithPlanes(m_min, m_max, Planes) == true)
            {
                for (int i = 0; i < m_branchList.Count; ++i)
                {
                    m_branchList[i].CheckWithCameraPlanes(Planes, EntityList, Renderable);
                }
            }
        }

        protected override bool IsPointInAABB(Vector3 Position)
        {
            return CToolbox.IsPointInMinMaxOfBoundingBox(m_min, m_max, Position);
        }

        protected override bool SphereVsSphere(BoundingSphere Sphere)
        {
            return CCollision.SphereVsSphere(m_sphere, Sphere);
        }

        protected override bool CheckRayVsAABB(Ray ShotRay)
        {
            return CCollision.RayVsAABB(ShotRay.Direction, ShotRay.Position, m_min, m_max);
        }

        protected override bool CheckRayVsAABB(Ray ShotRay, ref float Dist)
        {
            float dist = 0;
            CCollision.RayVsAABB(ShotRay, m_min, m_max, ref dist);

            if (dist < Dist && dist != 0)
            {
                Dist = dist;
                return true;
            }

            return false;
        }


        protected override bool CheckRayVsChildOOBB(Ray ShotRay, ref float Dist)
        {
            return CheckRayVsAABB(ShotRay, ref Dist);
        }

        protected override bool CheckRayVsSphere(Ray ShotRay)
        {
            return CCollision.RayVsSphere(ShotRay.Direction, ShotRay.Position, m_sphere);
        }

        private void CreateMinMaxForChild(Vector3[] Min, Vector3[] Max)
        {
            float width_step = (m_max.X - m_min.X) / m_numBranchesW;
            float height_step = (m_max.Y - m_min.Y) / m_numBranchesH;
            float bredth_step = (m_max.Z - m_min.Z) / m_numBranchesB;

            uint count = 0;
            for (uint i = 0; i < m_numBranchesH; ++i)
            {
                for (uint j = 0; j < m_numBranchesB; ++j)
                {
                    for (uint k = 0; k < m_numBranchesW; ++k)
                    {
                        Min[count] = new Vector3(m_min.X + ((float)k * width_step), m_min.Y + ((float)i * height_step), m_min.Z + (j * bredth_step));
                        Max[count] = new Vector3(m_min.X + (((float)k + 1.0f) * width_step), m_min.Y + (((float)i + 1.0f) * height_step), m_min.Z + (((float)j + 1.0f) * bredth_step));
                        ++count;
                    }
                }
            }
        }

        private bool Sort(List<STreeInput> EntityList, Vector3[] Min, Vector3[] Max, SRoomData RoomData, List<CEntity> ActualEntityList)
        {
            int original_num = EntityList.Count;

            List<STreeInput>[] new_entity_list = new List<STreeInput>[m_totalBranches];

            uint total_sorted = 0;

            for (int j = 0; j < m_totalBranches; ++j)
            {
                float distance = Vector3.Distance(Min[j], Max[j]);

                new_entity_list[j] = new List<STreeInput>();
                for (int i = 0; i < EntityList.Count; ++i)
                {
                    if (EntityList[i].s_position.X < Min[j].X || EntityList[i].s_position.X > Max[j].X)
                    {
                        continue;
                    }
                    if (EntityList[i].s_position.Y < Min[j].Y || EntityList[i].s_position.Y > Max[j].Y)
                    {
                        continue;
                    }
                    if (EntityList[i].s_position.Z < Min[j].Z || EntityList[i].s_position.Z > Max[j].Z)
                    {
                        continue;
                    }

                    // This check makes sure that the new containes doesn't have a bounding box smaller than the 
                    // current object. If it is smaller, then it assumes that the current Room is big enough
                    // for the current entity. If this check were not to be done then the entity will not render
                    // when it should.
                    if (distance < EntityList[i].s_minToMax)
                    {
                        if (EntityList[i].s_isRoom == true)
                        {
                            if (FillRoom(EntityList[i], RoomData, ActualEntityList) == false)
                            {
                                return false;
                            }
                            m_branchList.Add(new CMainRoomLeaf(EntityList[i].s_entityID, this, RoomData.s_roomList[RoomData.s_roomList.Count - 1]));
                        }
                        else
                        {
                            if (CToolbox.ExpandParentBBToFitChildBB(ref m_min, ref m_max, EntityList[i].s_min, EntityList[i].s_max) == true)
                            {
                                if (m_parent != null)
                                {
                                    CCompositeSpatialTree parent = (CCompositeSpatialTree)m_parent;
                                    parent.ExpandSpatialVolumeSize();
                                }
                            }

                            m_branchList.Add(new CEntityLeaf(EntityList[i].s_entityID, this, ActualEntityList[EntityList[i].s_entityID]));
                        }
                    }
                    else
                    {
                        //else
                        {
                            // Expand the current min and max of the spatial partion if requried. This shouldn't be much of a 
                            // increase in size since the bigger object has been removed from the list in the above if statement.
                            if (CToolbox.ExpandParentBBToFitChildBB(ref Min[j], ref Max[j], EntityList[i].s_min, EntityList[i].s_max) == true)
                            {
                                ExpandSpatialVolumeSize(Min[j], Max[j]);
                            }

                            new_entity_list[j].Add(EntityList[i]);
                        }
                    }
                    EntityList.Remove(EntityList[i]);
                    --i;
                    ++total_sorted;
                }
            }

            if (total_sorted == original_num)
            {
                // This function returns the index of the branch if one of the branches still contains the same amount of entities
                // as the original_num. This is needed otherwise there is a possibility of an infinit loop.
                int branch_id = HasSameNumEntitiesAsBefore(new_entity_list, original_num);
                if (branch_id == -1)
                {
                    for (int i = 0; i < m_totalBranches; ++i)
                    {
                        CCompositeSpatialTree branches = new CCompositeSpatialTree();
                        if (branches.Initialise(Min[i], Max[i], new Vector3((float)m_numBranchesW, (float)m_numBranchesH, (float)m_numBranchesB), new_entity_list[i], RoomData, this, ActualEntityList) == false)
                        {
                            return false;
                        }
                        m_branchList.Add(branches);
                    }
                }
                else
                {
                    for (int i = 0; i < new_entity_list[branch_id].Count; ++i)
                    {
                        if (new_entity_list[branch_id][i].s_isRoom == true)
                        {
                            if (FillRoom(new_entity_list[branch_id][i], RoomData, ActualEntityList) == false)
                            {
                                return false;
                            }
                            m_branchList.Add(new CMainRoomLeaf(new_entity_list[branch_id][i].s_entityID, this, RoomData.s_roomList[RoomData.s_roomList.Count - 1]));
                        }
                        else
                        {
                            m_branchList.Add(new CEntityLeaf(new_entity_list[branch_id][i].s_entityID, this, ActualEntityList[new_entity_list[branch_id][i].s_entityID]));
                        }
                    }
                }
                return true;
            }

            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CCompositeSpatialTree", "Total sorted does not equal to the original number of entities. Total Sorted = " + total_sorted
                + " | Original Num of Entities = " + original_num);

            for (int i = 0; i < EntityList.Count; ++i)
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CCompositeSpatialTree", "This Entity cannot be placed in the SPT: " + ActualEntityList[EntityList[i].s_entityID].GetEntityName());
            }

            return false;
        }

        // This function returns the index of the branch if one of the branches still contains the same amount of entities
        // as the original_num.
        private int HasSameNumEntitiesAsBefore(List<STreeInput>[] NewEntityList, int OriginalNum)
        {
            for (int i = 0; i < m_totalBranches; ++i)
            {
                if (NewEntityList[i].Count == OriginalNum)
                {
                    return i;
                }
            }

            return -1;
        }

        private bool Sort(List<STreeInput> EntityList, SRoomData RoomData, List<CEntity> ActualEntityList)
        {
            int original_num = EntityList.Count;

            List<STreeInput> new_entity_list = new List<STreeInput>();

            uint total_sorted = 0;

            for (int i = 0; i < EntityList.Count; ++i)
            {
                if (EntityList[i].s_position.X < m_min.X || EntityList[i].s_position.X > m_max.X)
                {
                    continue;
                }
                if (EntityList[i].s_position.Y < m_min.Y || EntityList[i].s_position.Y > m_max.Y)
                {
                    continue;
                }
                if (EntityList[i].s_position.Z < m_min.Z || EntityList[i].s_position.Z > m_max.Z)
                {
                    continue;
                }
                new_entity_list.Add(EntityList[i]);
                EntityList.Remove(EntityList[i]);
                --i;
                ++total_sorted;
            }

            if (total_sorted == original_num)
            {
                for (int i = 0; i < total_sorted; ++i)
                {
                    if (new_entity_list[i].s_isRoom == true)
                    {
                        if (FillRoom(new_entity_list[i], RoomData, ActualEntityList) == false)
                        {
                            return false;
                        }
                        m_branchList.Add(new CMainRoomLeaf(new_entity_list[i].s_entityID, this, RoomData.s_roomList[RoomData.s_roomList.Count-1]));
                    }
                    else
                    {
                        if (CToolbox.ExpandParentBBToFitChildBB(ref m_min, ref m_max, new_entity_list[i].s_min, new_entity_list[i].s_max) == true)
                        {
                            if (m_parent != null)
                            {
                                CCompositeSpatialTree parent = (CCompositeSpatialTree)m_parent;
                                parent.ExpandSpatialVolumeSize();
                            }
                        }

                        // The leaf BoxEntity store the ID of the BoxEntity
                        m_branchList.Add(new CEntityLeaf(new_entity_list[i].s_entityID, this, ActualEntityList[new_entity_list[i].s_entityID]));
                    }
                }
                return true;
            }

           CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CCompositeSpatialTree", "Total sorted does not equal to the original number of entities (2). Total Sorted = " + total_sorted
                + " | Original Num of Entities = " + original_num);

            for (int i = 0; i < EntityList.Count; ++i)
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CCompositeSpatialTree", "This Entity cannot be placed in the SPT: " + ActualEntityList[EntityList[i].s_entityID].GetEntityName());
            }

            return false;
        }

        // For sorting objects in Rooms.
        private bool Sort(List<STreeInput> EntityList, Vector3[] Min, Vector3[] Max, List<CEntity> ActualEntityList)
        {
            int original_num = EntityList.Count;

            List<STreeInput>[] new_entity_list = new List<STreeInput>[m_totalBranches];

            uint total_sorted = 0;

            for (int j = 0; j < m_totalBranches; ++j)
            {
                float distance = Vector3.Distance(Min[j], Max[j]);

                new_entity_list[j] = new List<STreeInput>();
                for (int i = 0; i < EntityList.Count; ++i)
                {
                    if (EntityList[i].s_position.X < Min[j].X || EntityList[i].s_position.X > Max[j].X)
                    {
                        continue;
                    }
                    if (EntityList[i].s_position.Y < Min[j].Y || EntityList[i].s_position.Y > Max[j].Y)
                    {
                        continue;
                    }
                    if (EntityList[i].s_position.Z < Min[j].Z || EntityList[i].s_position.Z > Max[j].Z)
                    {
                        continue;
                    }

                    // This check makes sure that the new containes doesn't have a bounding box smaller than the 
                    // current object. If it is smaller, then it assumes that the current Room is big enough
                    // for the current entity. If this check were not to be done then the entity will not render
                    // when it should.
                    if (distance < EntityList[i].s_minToMax)
                    {
                        //SHOULDN'T HAVE TO DO THIS:
                        if (CToolbox.ExpandParentBBToFitChildBB(ref m_min, ref m_max, EntityList[i].s_min, EntityList[i].s_max) == true)
                        {
                            if (m_parent != null)
                            {
                                CCompositeSpatialTree parent = (CCompositeSpatialTree)m_parent;
                                parent.ExpandSpatialVolumeSize();
                            }
                        }

                        m_branchList.Add(new CEntityLeaf(EntityList[i].s_entityID, this, ActualEntityList[EntityList[i].s_entityID]));
                    }
                    else
                    {
                        //SHOULDN'T HAVE TO DO THIS:
                        // Expand the current min and max of the spatial partion if requried. This shouldn't be much of a 
                        // increase in size since the bigger object has been removed from the list in the above if statement.
                        if (CToolbox.ExpandParentBBToFitChildBB(ref Min[j], ref Max[j], EntityList[i].s_min, EntityList[i].s_max) == true)
                        {
                            ExpandSpatialVolumeSize(Min[j], Max[j]);
                        }

                        new_entity_list[j].Add(EntityList[i]);
                    }
                    EntityList.Remove(EntityList[i]);
                    --i;
                    ++total_sorted;
                }
            }

            if (total_sorted == original_num)
            {
                // This function returns the index of the branch if one of the branches still contains the same amount of entities
                // as the original_num. This is needed otherwise there is a possibility of an infinit loop.
                int branch_id = HasSameNumEntitiesAsBefore(new_entity_list, original_num);
                if (branch_id == -1)
                {
                    for (int i = 0; i < m_totalBranches; ++i)
                    {
                        CRoomCompositeSpatialTree branches = new CRoomCompositeSpatialTree();
                        if (branches.Initialise(Min[i], Max[i], new Vector3((float)m_numBranchesW, (float)m_numBranchesH, (float)m_numBranchesB), new_entity_list[i], this, ActualEntityList) == false)
                        {
                            return false;
                        }
                        m_branchList.Add(branches);
                    }
                }
                else
                {
                    for (int i = 0; i < new_entity_list[branch_id].Count; ++i)
                    {
                        m_branchList.Add(new CEntityLeaf(new_entity_list[branch_id][i].s_entityID, this, ActualEntityList[new_entity_list[branch_id][i].s_entityID]));
                    }
                }
                return true;
            }
            return false;
        }

        // For sorting objects in Rooms.
        private bool Sort(List<STreeInput> EntityList, List<CEntity> ActualEntityList)
        {
            int original_num = EntityList.Count;

            List<STreeInput> new_entity_list = new List<STreeInput>();

            uint total_sorted = 0;

            for (int i = 0; i < EntityList.Count; ++i)
            {
                if (EntityList[i].s_position.X < m_min.X || EntityList[i].s_position.X > m_max.X)
                {
                    continue;
                }
                if (EntityList[i].s_position.Y < m_min.Y || EntityList[i].s_position.Y > m_max.Y)
                {
                    continue;
                }
                if (EntityList[i].s_position.Z < m_min.Z || EntityList[i].s_position.Z > m_max.Z)
                {
                    continue;
                }
                new_entity_list.Add(EntityList[i]);
                EntityList.Remove(EntityList[i]);
                --i;
                ++total_sorted;
            }

            if (total_sorted == original_num)
            {
                for (int i = 0; i < total_sorted; ++i)
                {
                    // The leaf BoxEntity store the ID of the BoxEntity
                    m_branchList.Add(new CEntityLeaf(new_entity_list[i].s_entityID, this, ActualEntityList[new_entity_list[i].s_entityID]));
                }
                return true;
            }
            return false;
        }

        private bool FillRoom(STreeInput Entity, SRoomData RoomData, List<CEntity> ActualEntityList)
        {
            bool root_room = false;
            if (Entity.s_RoomID == 0)
            {
                root_room = true;
            }
            if (RoomData.s_entityDictionary.ContainsKey(Entity.s_RoomID) == true)
            {
                CMainRoomCompositeSpatialTree branches = new CMainRoomCompositeSpatialTree();
#if ENGINE
                branches.PreInitialise(Entity.s_position, Entity.s_entityID, RoomData.s_entityDictionary[Entity.s_RoomID], root_room, this);
#else
                branches.PreInitialise(Entity.s_position, Entity.s_entityID, RoomData.s_entityDictionary[Entity.s_RoomID], root_room, this, ActualEntityList[Entity.s_entityID]);
#endif
                if (branches.Initialise(Entity.s_min, Entity.s_max, RoomData.s_numRoomBranches, RoomData.s_entityDictionary[Entity.s_RoomID], this, ActualEntityList) == false)
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CCompositeSpatialTree", "ONDERZOEK ENGINE ERROR: Room could not be filled.");
                    return false;
                }
                RoomData.s_roomList.Add(branches);
            }
            else
            {
                // This will be an empty Room/room
                CMainRoomCompositeSpatialTree branches = new CMainRoomCompositeSpatialTree();
#if ENGINE
                branches.PreInitialise(Entity.s_position, Entity.s_entityID, null, root_room, this);
#else
                branches.PreInitialise(Entity.s_position, Entity.s_entityID, null, root_room, this, ActualEntityList[Entity.s_entityID]);
#endif
                branches.Initialise(Entity.s_min, Entity.s_max, RoomData.s_numRoomBranches, new List<STreeInput>(), this, ActualEntityList);
                RoomData.s_roomList.Add(branches);
            }

            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Onderzoek.SpatialTree;
using Onderzoek.Entity;
using Onderzoek.Physics;

using Microsoft.Xna.Framework;

namespace Onderzoek.SpatialTree
{
    public class CMainRoomCompositeSpatialTree : CCompositeSpatialTree
    {
        public struct SPortalInfo
        {
            public SPortalInfo(STreeInput Input)
            {
                //s_portalPosition = Input.s_min + (( Input.s_max - Input.s_min ) * 0.5f);
                //s_position = Input.s_position;
                s_entityID = Input.s_entityID;
                s_min = Input.s_min;
                s_max = Input.s_max;
                s_portalNormal = Input.s_portalNormal;
                //s_corners = Input.s_vertices;// DON'T NEED THIS.
            }

            //public Vector3 s_position;
            public int s_entityID;
            public Vector3 s_min;
            public Vector3 s_max;
            //public Vector3 s_portalPosition;
            public Vector3 s_portalNormal;
            //public Vector3[] s_corners;
        }

        public enum EReturnValues
        {
            e_portalsFound,
            e_noPortals,
            e_outsideRoom
        }

        public int GetEntityID()
        {
            return m_RoomEntityID;
        }

        private List<SPortalInfo> m_portalEntityID;
        private int m_RoomEntityID;
        private Vector3 m_position;
        private bool m_rootRoom;

        // This check is used to do camera culling through portals.
        public EReturnValues CheckAndGetNewPlanes(ref List<Plane[]> CurPlanes, Vector3 CameraPos, Vector3 CameraDir, List<CEntity> EntityList, List<int> Renderable, ref List<Plane[]> PlaneList)
        {
            CRoomEntity room_entity = (CRoomEntity)EntityList[m_RoomEntityID];

            if (room_entity.IsHidden() == true)
            {
                return EReturnValues.e_outsideRoom;
            }

            if (room_entity.GetRender() == false)
            {
                // Render the Room
                Renderable.Add(m_RoomEntityID);
                room_entity.SetRender(true, false);
            }

            // Determining if the camera is in this Room.
            bool cam_in_room = CToolbox.IsPointInBoundingBox(room_entity.GetRoomBBPlanes(), CameraPos);

            for (int k = 0; k < CurPlanes.Count; ++k)
            {
                if (CToolbox.ClipWithPlanes(m_min, m_max, CurPlanes[k]) == true)
                {
                    // If camera is in the room then use normal camera culling check
                    if (cam_in_room == true)
                    {
                        // When in the room, then use the normal camera planes to
                        // cull the objects in the room.
                        for (int i = 0; i < m_branchList.Count; ++i)
                        {
                            m_branchList[i].CheckWithCameraPlanes(CurPlanes[k], EntityList, Renderable);
                        }

                        bool use_portal = false;
                        foreach (SPortalInfo portal_info in m_portalEntityID)
                        {
                            Vector3 cam_dir = CameraDir;

                            // Old if statement, which is the fallback if the one below doesn't work
                            //float cam_portal_angle = CToolbox.AngleBetweenTwoVectorR(cam_dir, portal_info.s_portalNormal);
                            //if (cam_portal_angle > 2.0f && cam_portal_angle < 3.14f)

                            // This if statement might cause some issues
                            Vector3 min, max;
                            EntityList[portal_info.s_entityID].GetMinMax(out min, out max);
                            if (CToolbox.ClipWithPlanes(min, max, CurPlanes[k]) == false)
                            {
                                continue;
                            }

                            //Vector3 portal_normal = -1 * portal_info.s_portalNormal;

                            //// No need for this, i think
                            //// Determine which side of the Room the camera is on.
                            //if (CToolbox.UseThisInsidePortal(room_entity.GetPlanes(), CameraInstance.m_position, portal_normal) == false)
                            //{
                            //    continue;
                            //}

                            use_portal = true;

                            // Rendering the wire frame model of the portal
                            Renderable.Add(portal_info.s_entityID);

                            Vector3[] portal_pos = EntityList[portal_info.s_entityID].GetWorldOBBIndex0();

                            // + 2 for the near and far clip planes.
                            Plane[] portal_planes = new Plane[portal_pos.Length + 1];
                            portal_planes[0] = CToolbox.CreatePlane(cam_dir, CameraPos);
                            //portal_planes[1] = CToolbox.CreatePlane(cam_dir * -1, CameraInstance.m_position + (cam_dir * -1 * CameraInstance.GetFarClipDistance()));
                            for (int i = 0; i < portal_pos.Length; ++i)
                            {
                                Vector3 a;
                                if (i == 0)
                                {
                                    a = portal_pos[portal_pos.Length - 1] - CameraPos;
                                }
                                else
                                {
                                    a = portal_pos[i - 1] - CameraPos;
                                }
                                Vector3 b = portal_pos[i] - CameraPos;

                                portal_planes[i + 1] = CToolbox.CreatePlane(CToolbox.UnitVector(Vector3.Cross(a, b)), CameraPos);
                            }

                            PlaneList.Add(portal_planes);
                        }

                        // Return true to indicate that new planes have been created
                        if (use_portal == true)
                        {
                            // Make sure that all the other room checks are done with these new planes.
                            CurPlanes = PlaneList;

                            return EReturnValues.e_portalsFound;
                        }

                        return EReturnValues.e_noPortals;
                    }
                    else // Else use the portal to look into the room
                    {
                        foreach (SPortalInfo portal_info in m_portalEntityID)
                        {
                            // This if statement might cause some issues
                            if (CToolbox.ClipWithPlanes(portal_info.s_min, portal_info.s_max, CurPlanes[k]) == false)
                            {
                                continue;
                            }
                            // This is definitly needed!
                            // Determine which side of the Room the camera is on.
                            if (CToolbox.UseThisOutsidePortal(room_entity.GetFirstPlanes(), CameraPos, portal_info.s_portalNormal) == false)
                            {
                                continue;
                            }

                            // Rendering the wire frame model of the portal
                            Renderable.Add(portal_info.s_entityID);

                            Vector3[] portal_pos = EntityList[portal_info.s_entityID].GetWorldOBBIndex0();

                            // + 2 for the near and far clip planes.
                            Plane[] portal_planes = new Plane[portal_pos.Length + 1];
                            Vector3 cam_dir = CameraDir;
                            portal_planes[0] = CToolbox.CreatePlane(cam_dir, CameraPos);
                            //portal_planes[1] = CToolbox.CreatePlane(cam_dir * -1, CameraInstance.m_position + (cam_dir * -1 * CameraInstance.GetFarClipDistance()));
                            for (int i = 0; i < portal_pos.Length; ++i)
                            {
                                Vector3 a;
                                if (i == 0)
                                {
                                    a = portal_pos[portal_pos.Length - 1] - CameraPos;
                                }
                                else
                                {
                                    a = portal_pos[i - 1] - CameraPos;
                                }
                                Vector3 b = portal_pos[i] - CameraPos;

                                portal_planes[i + 1] = CToolbox.CreatePlane(CToolbox.UnitVector(Vector3.Cross(b, a)), CameraPos);
                            }

                            //System.Diagnostics.Trace.WriteLine("Camera can see the portal.");
                            for (int i = 0; i < m_branchList.Count; ++i)
                            {
                                m_branchList[i].CheckWithPortalPlanes(portal_planes, EntityList, Renderable);
                            }
                        }
                    }
                }
            }
            return EReturnValues.e_outsideRoom;
        }

        // This needs to be called first before the normal Initialise function
#if ENGINE
        public void PreInitialise(Vector3 Positon, int RoomEntityID, List<STreeInput> EntityList, bool RootRoom, CSpatialTree Parent)
#else
        public void PreInitialise(Vector3 Positon, int RoomEntityID, List<STreeInput> EntityList, bool RootRoom, CSpatialTree Parent, CEntity ActualEntity)
#endif
        {
            m_rootRoom = RootRoom;
            m_RoomEntityID = RoomEntityID;
            m_position = Positon;
            m_parent = Parent;

            m_portalEntityID = new List<SPortalInfo>();
            if (EntityList != null)
            {
                for (int i = 0; i < EntityList.Count; ++i)
                {
                    if (EntityList[i].s_isPortal == true)
                    {
                        m_portalEntityID.Add(new SPortalInfo(EntityList[i]));
                        EntityList.Remove(EntityList[i]);
                        --i;
                    }
                }
            }

#if EDITOR
            if (ActualEntity.IsDynamicEntity() == false)
            {
                CStaticEntity temp = (CStaticEntity)ActualEntity;
                temp.SetEntityLeafNode(this);
            }
#endif
        }

        protected override bool IsPointInAABB(Vector3 Position)
        {
            return CToolbox.IsPointInMinMaxOfBoundingBox(m_min, m_max, Position);
        }

        protected override bool CheckRayVsAABB(Ray ShotRay, ref float Dist)
        {
            float dist = 0;
            CCollision.RayVsAABB(ShotRay, m_min, m_max, ref dist);

            if (dist < Dist && dist != 0)
            {
                Dist = dist;
                return true;
            }

            return false;
        }

        protected override bool CheckRayVsChildOOBB(Ray ShotRay, ref float Dist)
        {
            return CheckRayVsAABB(ShotRay, ref Dist);
        }

        protected override bool CheckRayVsSphere(Ray ShotRay)
        {
            return CCollision.RayVsSphere(ShotRay.Direction, ShotRay.Position, m_sphere);
        }
    }
}

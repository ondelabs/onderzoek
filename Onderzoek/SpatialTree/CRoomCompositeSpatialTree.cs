﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Onderzoek.SpatialTree;
using Onderzoek.Entity;
using Onderzoek.Camera;
using Onderzoek.Physics;

using Microsoft.Xna.Framework;

namespace Onderzoek.SpatialTree
{
    class CRoomCompositeSpatialTree : CCompositeSpatialTree
    {
        // This check is used to do camera culling.
        public override void CheckWithCameraPlanes(CCamera CameraInstance, List<CEntity> EntityList, List<int> Renderable)
        {
            if (CToolbox.ClipWithPlanes(m_min, m_max, CameraInstance.GetClipPlanes()) == true)
            {
                for (int i = 0; i < m_branchList.Count; ++i)
                {
                    m_branchList[i].CheckWithCameraPlanes(CameraInstance, EntityList, Renderable);
                }
            }
        }

        // This check is used to do camera culling.
        public override void CheckWithCameraPlanes(List<Plane[]> PlaneList, List<CEntity> EntityList, List<int> Renderable)
        {
            List<Plane[]> temp = new List<Plane[]>(); ;
            for (int j = 0; j < PlaneList.Count; ++j)
            {
                if (CToolbox.ClipWithPlanes(m_min, m_max, PlaneList[j]) == true)
                {
                    temp.Add(PlaneList[j]);
                }
            }

            if (temp.Count > 0)
            {
                for (int i = 0; i < m_branchList.Count; ++i)
                {
                    m_branchList[i].CheckWithCameraPlanes(PlaneList, EntityList, Renderable);
                }
            }

            //for (int j = 0; j < PlaneList.Count; ++j)
            //{
            //    if (CToolbox.ClipWithPlanes(m_min, m_max, PlaneList[j]) == true)
            //    {
            //        for (int i = 0; i < m_branchList.Count; ++i)
            //        {
            //            m_branchList[i].CheckWithCameraPlanes(PlaneList, EntityList, Renderable);
            //        }
            //    }
            //}
        }

        public override void CheckWithCameraPlanes(Plane[] Planes, List<CEntity> EntityList, List<int> Renderable)
        {
            if (CToolbox.ClipWithPlanes(m_min, m_max, Planes) == true)
            {
                for (int i = 0; i < m_branchList.Count; ++i)
                {
                    m_branchList[i].CheckWithCameraPlanes(Planes, EntityList, Renderable);
                }
            }
        }

        // This check is only used when using the portal planes
        public override void CheckWithPortalPlanes(Plane[] PortalPlanes, List<CEntity> EntityList, List<int> Renderable)
        {
            if (CToolbox.ClipWithPlanes(m_min, m_max, PortalPlanes) == true)
            {
                for (int i = 0; i < m_branchList.Count; ++i)
                {
                    m_branchList[i].CheckWithPortalPlanes(PortalPlanes, EntityList, Renderable);
                }
            }
        }

        protected override bool IsPointInAABB(Vector3 Position)
        {
            return CToolbox.IsPointInMinMaxOfBoundingBox(m_min, m_max, Position);
        }

        protected override bool CheckRayVsAABB(Ray ShotRay, ref float Dist)
        {
            float dist = 0;
            CCollision.RayVsAABB(ShotRay, m_min, m_max, ref dist);

            if (dist < Dist && dist != 0)
            {
                Dist = dist;
                return true;
            }

            return false;
        }

        protected override bool CheckRayVsChildOOBB(Ray ShotRay, ref float Dist)
        {
            return CheckRayVsAABB(ShotRay, ref Dist);
        }

        protected override bool CheckRayVsSphere(Ray ShotRay)
        {
            return CCollision.RayVsSphere(ShotRay.Direction, ShotRay.Position, m_sphere);
        }
    }
}

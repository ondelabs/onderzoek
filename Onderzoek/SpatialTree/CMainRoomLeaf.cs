﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using Onderzoek.SpatialTree;
using Onderzoek.Entity;

namespace Onderzoek.SpatialTree
{
    class CMainRoomLeaf : CSpatialTree
    {
        public CMainRoomLeaf(int EntityID, CSpatialTree Parent, CMainRoomCompositeSpatialTree MainRoom)
        {
            Initialise(EntityID, Parent, MainRoom);
        }

        public void Initialise(int EntityID, CSpatialTree Parent, CMainRoomCompositeSpatialTree MainRoom)
        {
            m_isLeaf = true;
            m_parent = Parent;
            m_entityID = EntityID;
            m_mainRoom = MainRoom;
        }

        public int GetEntityID()
        {
            return m_entityID;
        }

        public CMainRoomCompositeSpatialTree GetMainRoom()
        {
            return m_mainRoom;
        }

        private int m_entityID;
        private CMainRoomCompositeSpatialTree m_mainRoom;
    }
}

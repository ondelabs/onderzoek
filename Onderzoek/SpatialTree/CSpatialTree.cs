﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;

using Onderzoek.Entity;
using Onderzoek.Camera;

namespace Onderzoek.SpatialTree
{
    // Can have default functions or can be left blank, and faster than interface.
    public abstract class CSpatialTree
    {
        protected CSpatialTree m_parent;
        protected bool m_isLeaf = false;
        protected System.Threading.Mutex m_mutex = new System.Threading.Mutex();

        public virtual void CheckWithCameraPlanes( CCamera CameraInstance, List<CEntity> EntityList, List<int> Renderable )
        {
        }

        public virtual void CheckWithCameraPlanes( List<Plane[]> PlaneList, List<CEntity> EntityList, List<int> Renderable )
        {
        }

        public virtual void CheckWithCameraPlanes(Plane[] Planes, List<CEntity> EntityList, List<int> Renderable)
        {
        }

        protected virtual bool IsPointInAABB(Vector3 Position)
        {
            return false;
        }

        protected virtual bool SphereVsSphere(BoundingSphere Sphere)
        {
            return false;
        }

        protected virtual bool CheckRayVsAABB(Ray ShotRay)
        {
            return false;
        }

        protected virtual bool CheckRayVsAABB(Ray ShotRay, ref float Dist)
        {
            return false;
        }

        protected virtual bool CheckRayVsChildOOBB(Ray ShotRay, ref float Dist)
        {
            return false;
        }

        protected virtual bool CheckRayVsSphere(Ray ShotRay)
        {
            return false;
        }

        public virtual List<int> GetOtherEntityIDsInSameSpace()
        {
            // Try and get the parent's parent to find the leaf entities.
            // Otherwise just use the parent.
            CCompositeSpatialTree temp2 = (CCompositeSpatialTree)m_parent;
            if (temp2.m_parent != null)
            {
                temp2 = (CCompositeSpatialTree)temp2.m_parent;
            }

            List<CSpatialTree> branch_list = temp2.GetBranchList();

            List<int> rtn_list = new List<int>();

            temp2.SearchToGetEntityIDs(ref rtn_list, branch_list, this);

            return rtn_list;
        }

        protected virtual void SearchToGetEntityIDs(ref List<int> RtnList, List<CSpatialTree> BranchList, CSpatialTree OriginalEntity)
        {
            for (int i = 0; i < BranchList.Count; ++i)
            {
                bool isLeaf;
                BranchList[i].IsLeaf(out isLeaf);
                if (isLeaf == false)
                {
                    CCompositeSpatialTree temp2 = (CCompositeSpatialTree)BranchList[i];
                    List<CSpatialTree> branch_list = temp2.GetBranchList();

                    BranchList[i].SearchToGetEntityIDs(ref RtnList, branch_list, OriginalEntity);
                }
                else
                {
                    if (BranchList[i] != OriginalEntity)
                    {
                        Type type_leaf = BranchList[i].GetType();
                        if (type_leaf.Name == "CEntityLeaf")
                        {
                            CEntityLeaf leaf = (CEntityLeaf)BranchList[i];
                            RtnList.Add(leaf.GetEntityID());
                        }
                        else
                        {
                            CMainRoomLeaf leaf = (CMainRoomLeaf)BranchList[i];
                            RtnList.Add(leaf.GetEntityID());
                        }
                    }
                }
            }
        }

        // This is for collecting all the entity IDs that the sphere hits.
        public virtual List<int> CheckSphereVsSphereInTree(BoundingSphere Sphere, int NumSearchLevels)
        {
            List<int> entity_ids = new List<int>();

            int node_lvl = NumSearchLevels;
            CheckSphereTreeUp(Sphere, ref entity_ids, ref node_lvl);

            return entity_ids;
        }

        protected virtual void CheckSphereTreeUp(BoundingSphere Sphere, ref List<int> EntityIDs, ref int NodeLevel)
        {
            if (m_parent != null && NodeLevel > 0)
            {
                --NodeLevel;
                m_parent.CheckSphereTreeUp(Sphere, ref EntityIDs, ref NodeLevel);
            }
            else
            {
                CheckSphereTreeDown(Sphere, ref EntityIDs);
            }
        }

        protected virtual void CheckSphereTreeDown(BoundingSphere Sphere, ref List<int> EntityIDs)
        {
            CCompositeSpatialTree temp = (CCompositeSpatialTree)this;
            List<CSpatialTree> branch_list = temp.GetBranchList();

            for (int i = 0; i < branch_list.Count; ++i)
            {
                bool isLeaf;
                branch_list[i].IsLeaf(out isLeaf);
                if (isLeaf == false)
                {
                    CCompositeSpatialTree temp2 = (CCompositeSpatialTree)branch_list[i];

                    if (temp2.SphereVsSphere(Sphere) == true)
                    {
                        temp2.CheckSphereTreeDown(Sphere, ref EntityIDs);
                    }
                }
                else
                {
                    if (branch_list[i].GetType().Name != "CMainRoomLeaf")
                    {
                        CEntityLeaf temp2 = (CEntityLeaf)branch_list[i];

                        if (temp2.SphereVsSphere(Sphere) == true)
                        {
                            EntityIDs.Add(temp2.GetEntityID());
                        }
                    }
                    else
                    {
                        CMainRoomLeaf temp2 = (CMainRoomLeaf)branch_list[i];
                        CMainRoomCompositeSpatialTree temp3 = temp2.GetMainRoom();

                        if (temp3.SphereVsSphere(Sphere) == true)
                        {
                            EntityIDs.Add(temp3.GetEntityID());
                            temp3.CheckSphereTreeDown(Sphere, ref EntityIDs);
                        }
                    }
                }
            }
        }

        // This is for collecting all the entity IDs that the ray hits.
        public virtual List<int> CheckRayVsSphereInTree(Ray ShotRay)
        {
            List<int> entity_ids = new List<int>();

            CheckRayTreeUp(ShotRay, ref entity_ids);

            return entity_ids;
        }

        protected virtual void CheckRayTreeUp(Ray ShotRay, ref List<int> EntityIDs)
        {
            if (m_parent != null)
            {
                m_parent.CheckRayTreeUp(ShotRay, ref EntityIDs);
            }
            else
            {
                CheckRayTreeDown(ShotRay, ref EntityIDs);
            }
        }

        protected virtual void CheckRayTreeDown(Ray ShotRay, ref List<int> EntityIDs)
        {
            CCompositeSpatialTree temp = (CCompositeSpatialTree)this;
            List<CSpatialTree> branch_list = temp.GetBranchList();

            for (int i = 0; i < branch_list.Count; ++i)
            {
                bool isLeaf;
                branch_list[i].IsLeaf(out isLeaf);
                if (isLeaf == false)
                {
                    CCompositeSpatialTree temp2 = (CCompositeSpatialTree)branch_list[i];

                    if (temp2.IsPointInAABB(ShotRay.Position))
                    {
                        temp2.CheckRayTreeDown(ShotRay, ref EntityIDs);
                    }
                    else
                    {
                        if (temp2.CheckRayVsSphere(ShotRay) == true)
                        {
                            temp2.CheckRayTreeDown(ShotRay, ref EntityIDs);
                        }
                    }
                }
                else
                {
                    if (branch_list[i].GetType().Name != "CMainRoomLeaf")
                    {
                        CEntityLeaf temp2 = (CEntityLeaf)branch_list[i];

                        if (temp2.CheckRayVsSphere(ShotRay) == true)
                        {
                            EntityIDs.Add(temp2.GetEntityID());
                        }
                    }
                    else
                    {
                        CMainRoomLeaf temp2 = (CMainRoomLeaf)branch_list[i];
                        CMainRoomCompositeSpatialTree temp3 = temp2.GetMainRoom();

                        if (temp3.CheckRayVsSphere(ShotRay) == true)
                        {
                            EntityIDs.Add(temp3.GetEntityID());
                            temp3.CheckRayTreeDown(ShotRay, ref EntityIDs);
                        }
                    }
                }
            }
        }

        public virtual void CheckRayVsBBInTree(Ray ShotRay, ref float Dist)
        {
            CheckRayTreeUp(((CEntityLeaf)this).GetEntityID(), ShotRay, ref Dist);
        }

        protected virtual void CheckRayTreeUp(int CurEntityID, Ray ShotRay, ref float Dist)
        {
            if (m_parent != null)
            {
                m_parent.CheckRayTreeUp(CurEntityID, ShotRay, ref Dist);
            }
            else
            {
                CheckRayTreeDown(CurEntityID, ShotRay, ref Dist);
            }
        }

        protected virtual void CheckRayTreeDown(int CurEntityID, Ray ShotRay, ref float Dist)
        {
            CCompositeSpatialTree temp = (CCompositeSpatialTree)this;
            List<CSpatialTree> branch_list = temp.GetBranchList();

            for (int i = 0; i < branch_list.Count; ++i)
            {
                bool isLeaf;
                branch_list[i].IsLeaf(out isLeaf);
                if (isLeaf == false)
                {
                    CCompositeSpatialTree temp2 = (CCompositeSpatialTree)branch_list[i];
                    
                    if (temp2.IsPointInAABB(ShotRay.Position))
                    {
                        temp2.CheckRayTreeDown(CurEntityID, ShotRay, ref Dist);
                    }
                    else
                    {
                        if (temp2.CheckRayVsAABB(ShotRay) == true)
                        {
                            temp2.CheckRayTreeDown(CurEntityID, ShotRay, ref Dist);
                        }
                    }
                }
                else
                {
                    if (branch_list[i].GetType().Name != "CMainRoomLeaf")
                    {
                        CEntityLeaf temp2 = (CEntityLeaf)branch_list[i];

                        if (temp2.GetEntityID() != CurEntityID)
                        {
                            temp2.CheckRayVsChildOOBB(ShotRay, ref Dist);
                        }
                    }
                    else
                    {
                        CMainRoomLeaf temp2 = (CMainRoomLeaf)branch_list[i];
                        CMainRoomCompositeSpatialTree temp3 = temp2.GetMainRoom();

                        if (temp3.CheckRayVsAABB(ShotRay, ref Dist) == true)
                        {
                            temp3.CheckRayTreeDown(CurEntityID, ShotRay, ref Dist);
                        }
                    }
                }
            }
        }

        public virtual void MoveNodeAroundTree(Vector3 Position, float MaxLengthOfEntity)
        {
            // The root node has no parent, so make sure that the parent is not the root node.
            if (m_parent != null)
            {
                // First remove the dynamic node from the parent node's m_branchList.
                CCompositeSpatialTree temp2 = (CCompositeSpatialTree)m_parent;
                List<CSpatialTree> branch_list = temp2.GetBranchList();

                if (branch_list.Remove(this) == false)
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSpatialTree", "ONDERZOEK ENGINE ERROR: An error has occured while trying to move a dynamic object around the SPT.");
                    return;
                }

                CSpatialTree new_parent = MoveNodeAroundTreeUp(Position, MaxLengthOfEntity, this);
                if (new_parent != null)
                {
                    this.SetParent(new_parent);

                    // This might also be needed in the game too.
#if EDITOR
                    new_parent.ExpandSpatialVolumeSize();
#endif
                }
                else
                {
                    branch_list.Add(this);
                }
            }
        }

        protected virtual CSpatialTree MoveNodeAroundTreeUp(Vector3 Position, float MaxLengthOfEntity, CSpatialTree DynamicLeafNode)
        {
            if (m_parent != null)
            {
                CCompositeSpatialTree temp = (CCompositeSpatialTree)m_parent;

                if (CToolbox.IsPointInMinMaxOfBoundingBox(temp.GetMin(), temp.GetMax(), Position) == true)
                {
                    if (MaxLengthOfEntity <= temp.GetMaxLengthOfThisSpace())
                    {
                        return temp.MoveNodeAroundTreeDown(Position, MaxLengthOfEntity, temp, DynamicLeafNode);
                    }
                }
                else
                {
                    return temp.MoveNodeAroundTreeUp(Position, MaxLengthOfEntity, DynamicLeafNode);
                }
            }

            return null;
        }

        protected virtual CSpatialTree MoveNodeAroundTreeDown(Vector3 Position, float MaxLengthOfEntity, CCompositeSpatialTree This, CSpatialTree DynamicLeafNode)
        {
            List<CSpatialTree> branch_list = This.GetBranchList();
            for (int i = 0; i < branch_list.Count; ++i)
            {
                bool isLeaf;
                branch_list[i].IsLeaf(out isLeaf);
                if (isLeaf == false)
                {
                    CCompositeSpatialTree temp2 = (CCompositeSpatialTree)branch_list[i];

                    if (CToolbox.IsPointInMinMaxOfBoundingBox(temp2.GetMin(), temp2.GetMax(), Position) == true)
                    {
                        if (MaxLengthOfEntity <= temp2.GetMaxLengthOfThisSpace())
                        {
                            return temp2.MoveNodeAroundTreeDown(Position, MaxLengthOfEntity, temp2, DynamicLeafNode);
                        }
                    }
                }
                else
                {
                    if (branch_list[i].GetType().Name == "CMainRoomLeaf")
                    {
                        CMainRoomLeaf temp = (CMainRoomLeaf)branch_list[i];
                        CMainRoomCompositeSpatialTree temp2 = temp.GetMainRoom();
                        if( CToolbox.IsPointInMinMaxOfBoundingBox( temp2.GetMin(), temp2.GetMax(), Position ) )
                        {
                            return temp2.MoveNodeAroundTreeDown(Position, MaxLengthOfEntity, temp2, DynamicLeafNode);
                        }
                    }
                }
            }

            // Ok, so we can't go any further down the tree, this means that this is where the dynamic node should be placed in the tree.
            branch_list.Add(DynamicLeafNode);
            return This;
        }

        protected void ExpandSpatialVolumeSize(Vector3 CMin, Vector3 CMax)
        {
            CCompositeSpatialTree temp = (CCompositeSpatialTree)this;
            List<CSpatialTree> branch_list = temp.GetBranchList();

            Vector3 min = temp.GetMin();
            Vector3 max = temp.GetMax();
            if (CToolbox.ExpandParentBBToFitChildBB(ref min, ref max, CMin, CMax) == true)
            {
                temp.SetMin(min);
                temp.SetMax(max);

                if (m_parent != null)
                {
                    CCompositeSpatialTree parent = (CCompositeSpatialTree)m_parent;
                    parent.ExpandSpatialVolumeSize();
                }
            }
        }

        protected void ExpandSpatialVolumeSize()
        {
            CCompositeSpatialTree temp = (CCompositeSpatialTree)this;
            List<CSpatialTree> branch_list = temp.GetBranchList();

            bool has_expanded = false;
            for (int i = 0; i < branch_list.Count; ++i)
            {
                bool isLeaf;
                branch_list[i].IsLeaf(out isLeaf);
                if (isLeaf == false)
                {
                    CCompositeSpatialTree child = (CCompositeSpatialTree)branch_list[i];

                    Vector3 min = temp.GetMin();
                    Vector3 max = temp.GetMax();
                    if (CToolbox.ExpandParentBBToFitChildBB(ref min, ref max, child.GetMin(), child.GetMin()) == true)
                    {
                        temp.SetMin(min);
                        temp.SetMax(max);

                        has_expanded = true;
                    }
                }
            }

            // Now if the current parent's BB has expanded then the parent of the parent's BB needs to be expanded.
            if (has_expanded == true)
            {
                if (m_parent != null)
                {
                    CCompositeSpatialTree parent = (CCompositeSpatialTree)m_parent;
                    parent.ExpandSpatialVolumeSize();
                }
            }
        }

        // This check is only used when using the portal planes
        public virtual void CheckWithPortalPlanes( Plane[] PortalPlanes, List<CEntity> EntityList, List<int> Renderable )
        {
        }

        public void IsLeaf(out bool IsLeaf)
        {
            IsLeaf = m_isLeaf;
        }

        public void SetParent(CSpatialTree Parent)
        {
            m_parent = Parent;
        }

        public struct STreeInput
        {
            public STreeInput(Vector3 Pos, int ID, CEntity.SPortalInfo PortalInfo, Vector3 Min, Vector3 Max)
            {
                s_position = Pos;
                s_entityID = ID;
                s_minToMax = Vector3.Distance(Min, Max);
                s_isRoom = PortalInfo.s_isRoom;
                s_inRoom = false;
                if (PortalInfo.s_roomID > 0 && PortalInfo.s_isRoom == false)
                {
                    s_inRoom = true;
                }
                s_isPortal = PortalInfo.s_isPortal;
                s_RoomID = PortalInfo.s_roomID;
                s_min = Min;
                s_max = Max;
                s_portalNormal = PortalInfo.s_portalNormal;
                s_vertices = new Vector3[1];
            }

            public STreeInput(Vector3 Pos, int ID, CEntity.SPortalInfo PortalInfo, Vector3 Min, Vector3 Max, Vector3[] Vertices)
            {
                s_position = Pos;
                s_entityID = ID;
                s_minToMax = Vector3.Distance(Min, Max); ;
                s_isRoom = PortalInfo.s_isRoom;
                s_inRoom = false;
                if (PortalInfo.s_roomID > 0 && PortalInfo.s_isRoom == false)
                {
                    s_inRoom = true;
                }
                s_isPortal = PortalInfo.s_isPortal;
                s_RoomID = PortalInfo.s_roomID;
                s_min = Min;
                s_max = Max;
                s_portalNormal = PortalInfo.s_portalNormal;
                s_vertices = Vertices;
            }

            public Vector3 s_position;
            public int s_entityID;
            public float s_minToMax;
            public bool s_isRoom;
            public bool s_inRoom;
            public bool s_isPortal;
            public uint s_RoomID;
            public Vector3 s_min;
            public Vector3 s_max;
            public Vector3 s_portalNormal;
            public Vector3[] s_vertices;
        }
    }
}

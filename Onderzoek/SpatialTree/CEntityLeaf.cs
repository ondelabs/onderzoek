﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using Onderzoek.SpatialTree;
using Onderzoek.Entity;
using Onderzoek.Physics;
using Onderzoek.Camera;

namespace Onderzoek.SpatialTree
{
    public class CEntityLeaf : CSpatialTree
    {
        private int m_entityID;

        public int GetEntityID()
        {
            return m_entityID;
        }

        public CEntityLeaf(int EntityID, CSpatialTree Parent, CEntity ActualEntity)
        {
            Initialise(EntityID, Parent, ActualEntity);
        }

        public void Initialise(int EntityID, CSpatialTree Parent, CEntity ActualEntity)
        {
            m_isLeaf = true;
            m_parent = Parent;
            m_entityID = EntityID;

            // This is so that when a dynamic object moves, it can call this node and move it through the tree
            if (ActualEntity.IsDynamicEntity() == true)
            {
                CDynamicEntity temp = (CDynamicEntity)ActualEntity;
                temp.SetEntityLeafNode(this);
            }

#if EDITOR
            if (ActualEntity.IsDynamicEntity() == false)
            {
                CStaticEntity temp = (CStaticEntity)ActualEntity;
                temp.SetEntityLeafNode(this);
            }
#endif
        }

        // This check is used to do camera culling.
        public override void CheckWithCameraPlanes(CCamera CameraInstance, List<CEntity> EntityList, List<int> Renderable)
        {
            // This first check is required so that the renderable entity list doesn't get populated with the same entity IDs,
            // causing it to render the entity more than once.
            if (EntityList[m_entityID].GetRender() == false)
            {
                if (EntityList[m_entityID].CheckAgainstClipPlanes(CameraInstance.GetClipPlanes()) == true)
                {
                    EntityList[m_entityID].SetRender(true, false);
                    Renderable.Add(m_entityID);
                }
            }
        }

        // This check is used to do camera culling.
        public override void CheckWithCameraPlanes(List<Plane[]> PlaneList, List<CEntity> EntityList, List<int> Renderable)
        {
            //for (int j = 0; j < PlaneList.Count; ++j)
            //{
                // This first check is required so that the renderable entity list doesn't get populated with the same entity IDs,
                // causing it to render the entity more than once.
                if (EntityList[m_entityID].GetRender() == false)
                {
                    if (EntityList[m_entityID].CheckAgainstClipPlanes(PlaneList/*[j]*/) == true)
                    {
                        EntityList[m_entityID].SetRender(true, false);
                        Renderable.Add(m_entityID);
                    }
                }
            //}
        }

        public override void CheckWithCameraPlanes(Plane[] Planes, List<CEntity> EntityList, List<int> Renderable)
        {
            if (EntityList[m_entityID].GetRender() == false)
            {
                if (EntityList[m_entityID].CheckAgainstClipPlanes(Planes) == true)
                {
                    EntityList[m_entityID].SetRender(true, false);
                    Renderable.Add(m_entityID);
                }
            }
        }

        // This check is only used when using the portal planes
        public override void CheckWithPortalPlanes(Plane[] PortalPlanes, List<CEntity> EntityList, List<int> Renderable)
        {
            // This first check is required so that the renderable entity list doesn't get populated with the same entity IDs,
            // causing it to render the entity more than once.
            if (EntityList[m_entityID].GetRender() == false)
            {
                if (EntityList[m_entityID].CheckAgainstClipPlanes(PortalPlanes) == true)
                {
                    EntityList[m_entityID].SetRender(true, false);
                    Renderable.Add(m_entityID);
                }
            }
        }

        protected override bool CheckRayVsAABB(Ray ShotRay, ref float Dist)
        {
            float dist = 0;
            
            CCollision.Instance.RayVsEntityBB(ShotRay, ref dist, m_entityID);
            
            if (dist < Dist && dist != 0)
            {
                Dist = dist;
                return true;
            }

            return false;
        }

        protected override bool CheckRayVsChildOOBB(Ray ShotRay, ref float Dist)
        {
            return CCollision.Instance.RayVsEntityChildBB(ShotRay, ref Dist, m_entityID);
        }

        protected override bool SphereVsSphere(BoundingSphere Sphere)
        {
            return CCollision.Instance.SphereVsSphere(Sphere, m_entityID);
        }

        protected override bool CheckRayVsSphere(Ray ShotRay)
        {
            return CCollision.Instance.RayVsEntitySphere(ShotRay, m_entityID);
        }
    }
}

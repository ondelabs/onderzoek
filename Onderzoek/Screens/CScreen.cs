﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

using Onderzoek.GUI;
using Onderzoek.Visual;
using Onderzoek.Entity;
using Onderzoek.ModelData;

namespace Onderzoek.Screens
{
    public abstract class CScreen
    {
        protected ContentManager m_content;

        // Hud List
        protected List<CHUD> m_hudList;

        protected bool m_active;

        private Game1 m_screenManager;
        public Game1 ScreenManager
        {
            get { return m_screenManager; }
            set { m_screenManager = value; }
        }

        public CScreen()
        {
        }

        ~CScreen()
        {
        }

        public void Initialise( GameServiceContainer Services )
        {
            m_content = new ContentManager(Services, "Content");
            m_content.RootDirectory = "Content";

            m_hudList = new List<CHUD>();

            m_active = true;
        }

        public virtual void DeInitialise()
        {
            if (m_hudList != null)
            {
                for (int i = 0; i < m_hudList.Count; ++i)
                {
                    m_hudList[i].FreeMemory();
                }

                m_hudList.Clear();
                m_hudList = null;
            }

            if (m_content != null)
            {
                m_content.Unload();
                m_content = null;
            }

            if (m_screenManager != null)
            {
                m_screenManager = null;
            }
        }

        public virtual void ReInitialise()
        {
            foreach (CHUD hud in m_hudList)
            {
                if (hud.GetTexName() != null)
                {
                    hud.SetTexID(CVisual.Instance.LoadHudTexture(hud.GetTexName(), m_content));
                }
            }
        }

        public virtual void Update(GameTime gameTime)
        {
        }

        public virtual void Draw(GameTime gameTime)
        {
        }
    }
}

﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
//using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
//using Microsoft.Xna.Framework.Media;
//using Microsoft.Xna.Framework.Storage;

//using XSIXNARuntime;

using Onderzoek.ModelData;
using Onderzoek.Entity;
using Onderzoek.SpatialTree;
using Onderzoek.Visual;
using Onderzoek.Input;
using Onderzoek.Physics;
using Onderzoek.GUI;
using Onderzoek.Camera;
using Onderzoek.SceneGraph;
using Onderzoek.Audio;
using XMLManip;

namespace Onderzoek.Screens
{
    public class CGameScreenHelper : CScreen
    {
        protected bool m_initialised = false;

        // SkyBox instance
        public CSkyBox m_skyBox;

//#if ENGINE
        // Model list
        public Dictionary<string, CModel.SModelData> m_modelList;
//#endif

        // Dynamic Entity List Entity
        public List<CDynamicEntity> m_dynamicEntityList;

        // Entity list
        public List<CEntity> m_entityList;
        public Dictionary<string, int> m_entityNameToEntityID;
        private CSceneGraph m_sceneGraph;

        // These lists will be used at the beginning of all updates.
        List<CEntity> m_deadEntites;
        List<string> m_newEntites;

        // Shadow casting Light List
        public List<CDirLightEntity> m_scDirLightList;
        public List<CSpotLightEntity> m_scSpotLightList;
        protected CDirLightEntity m_mainLightEntity;
        private CBBLoader m_bbLoader;

        // For collision stuff
        public CCollision m_collision;

        // Spatial Tree
        public CSpatialTree m_sp;
        public List<int> m_whenRenderAll; // For debug purpose
        public List<int> m_renderable;
        public List<int> m_renderableTran;
        public List<int> m_giRenderable;
        public List<CMainRoomCompositeSpatialTree> m_roomList;

        // Camera data
        public BoundingBox m_giBB;
        public Vector3 m_giMin;
        public Vector3 m_giMax;
        public COrthoCamera m_rsmCamera;
        public float m_giCameraHight;

        //// For post processing and rendering
        //bool m_noPP = true;

         ~CGameScreenHelper()
        {
        }

        public override void DeInitialise()
        {
            if (m_collision != null)
            {
                m_collision.Dispose();
                m_collision = null;
            }

            CVisual.Instance.FreeLevelTextureMemory();

            if (m_initialised == true)
            {
                if (m_skyBox != null)
                {
                    m_skyBox.Dispose();
                    m_skyBox = null;
                }

                //#if ENGINE
                if (m_modelList != null)
                {
                    foreach (KeyValuePair<string, CModel.SModelData> pair in m_modelList)
                    {
                        if (pair.Value.s_model != null)
                        {
                            pair.Value.s_model.Delete();
                        }
                    }
                    m_modelList.Clear();
                    m_modelList = null;
                }
                //#endif

                if (m_dynamicEntityList != null)
                {
                    for (int i = 0; i < m_dynamicEntityList.Count; ++i)
                    {
                        m_dynamicEntityList[i].Dispose();
                    }
                    m_dynamicEntityList = null;
                }

                if (m_entityList != null)
                {
                    for (int i = 0; i < m_entityList.Count; ++i)
                    {
                        m_entityList[i].Dispose();
                    }
                    m_entityList = null;
                }

                if (m_entityNameToEntityID != null)
                {
                    m_entityNameToEntityID.Clear();
                    m_entityNameToEntityID = null;
                }

                m_sp = null;

                m_whenRenderAll = null;

                m_renderable = null;
                m_giRenderable = null;

                if (m_roomList != null)
                {
                    m_roomList.Clear();
                    m_roomList = null;
                }

                m_rsmCamera = null;

                if (m_scDirLightList != null)
                {
                    m_scDirLightList.Clear();
                    m_scDirLightList = null;
                }

                if (m_scSpotLightList != null)
                {
                    m_scSpotLightList.Clear();
                    m_scSpotLightList = null;
                }

                m_mainLightEntity = null;

                if (m_bbLoader != null)
                {
                    m_bbLoader.Dispose();
                    m_bbLoader = null;
                }

                if (m_deadEntites != null)
                {
                    m_deadEntites.Clear();
                    m_deadEntites = null;
                }
                if (m_newEntites != null)
                {
                    m_newEntites.Clear();
                    m_newEntites = null;
                }

                if (m_content != null)
                {
                    m_content.Unload();
                    m_content = null;
                }

                if (m_hudList != null)
                {
                    for (int i = 0; i < m_hudList.Count; ++i)
                    {
                        m_hudList[i].FreeMemory();
                        m_hudList[i] = null;
                    }
                    m_hudList = null;
                }

                if (m_sceneGraph != null)
                {
                    m_sceneGraph.Dispose();
                    m_sceneGraph = null;
                }
            }
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        public virtual void Initialise()
        {
            m_initialised = true;

            CCueManager.Instance.ScreenHasChanged(CCueManager.ECurScreen.Game);

//#if ENGINE
            m_modelList = new Dictionary<string, CModel.SModelData>();
//#endif

            m_bbLoader = new CBBLoader(m_content, m_modelList);

            m_giMin = new Vector3(-26.0f, -16.0f, -26.0f);
            m_giMax = new Vector3(26.0f, 16.0f, 26.0f);
            m_giBB = new BoundingBox(m_giMin, m_giMax);
            //m_rsmCamera = new CFPCamera(CVisual.Instance.GetAspectRatio(), 90.0f, 1.0f, 1000.0f, new Vector3(0, 0, 0), new Vector3(0, -1, 0));
            m_rsmCamera = new COrthoCamera(0,0,0,0,Matrix.Identity, Vector3.Zero);
            m_giCameraHight = 35.0f;

            m_deadEntites = new List<CEntity>();
            m_newEntites = new List<string>();


            m_dynamicEntityList = new List<CDynamicEntity>();

            m_entityList = new List<CEntity>();

            m_entityNameToEntityID = new Dictionary<string, int>();

            m_sp = new CCompositeSpatialTree();

            if (CCollision.Instance == null)
            {
                m_collision = new CCollision();
                m_collision.Initialise(this, m_sceneGraph);
            }
            else
                m_collision = CCollision.Instance;

            m_whenRenderAll = new List<int>();
            m_renderable = new List<int>();
            m_renderableTran = new List<int>();
            m_giRenderable = new List<int>();

            m_scSpotLightList = new List<CSpotLightEntity>();
            m_scDirLightList = new List<CDirLightEntity>();

            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CGameScreenHelper", "Passed CGameScreenHelper Initialiser");
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        public virtual bool LoadContent()
        {
            // Sort the scene out into a spatial partioning tree
            bool rtn_val = SetupSPT();

            return rtn_val;
        }

        public void RefreshSceneGraph()
        {
            // Setting the scene graph.
            if (m_sceneGraph != null)
            {
                m_sceneGraph.Dispose();
            }
            m_sceneGraph = null;
            m_sceneGraph = new CSceneGraph(ref m_entityList, m_dynamicEntityList);
        }

        public void FindMainLight()
        {
            foreach( CDirLightEntity dir in m_scDirLightList )
            {
                if (dir.IsMainLight == true)
                {
                    m_mainLightEntity = dir;
                    break;
                }
            }
        }

#if EDITOR
        public void ChangeMainLight(CDirLightEntity NewMainLight, bool Val)
        {
            if (Val == true)
            {
                foreach (CDirLightEntity dir in m_scDirLightList)
                {
                    if (dir.IsMainLight == true)
                    {
                        dir.IsMainLight = false;
                    }
                }
            }

            NewMainLight.IsMainLight = Val;

            m_mainLightEntity = NewMainLight;
        }
#endif

#if ENGINE
        protected bool SetupSPT()
#else
        public bool SetupSPT()
#endif
        {
            SetUpPortalSystem();

            RefreshSceneGraph();

            FindMainLight();
            
            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CGameSceenHelper", "SceneGraph has been created.");

            // needs to be called to update to the new scene graph.
            m_collision.SetSceneGraph(m_sceneGraph);

            // Clearing the SPT and making a new one.
            m_sp = null;
            m_sp = new CCompositeSpatialTree();
            // Clearing the renderall list (FOR DEBUGGING)
            if (m_whenRenderAll != null)
            {
                m_whenRenderAll.Clear();
                m_whenRenderAll = null;
                m_whenRenderAll = new List<int>();
            }
            // Clearing the roomlist
            if (m_roomList != null)
            {
                m_roomList.Clear();
                m_roomList = null;
                m_roomList = new List<CMainRoomCompositeSpatialTree>();
            }
            if (m_entityNameToEntityID != null)
            {
                m_entityNameToEntityID.Clear();
                m_entityNameToEntityID = null;
                m_entityNameToEntityID = new Dictionary<string, int>();
            }

            List<CSpatialTree.STreeInput> for_spt = new List<CSpatialTree.STreeInput>();
            int count = 0;
            foreach (CEntity entity in m_entityList)
            {
                Vector3 min, max;

                if (entity.GetPortalInfo().s_isRoom == true)
                {
                    ((CRoomEntity)entity).GetRoomMinMax(out min, out max);
                }
                else
                {
                    entity.GetMinMax(out min, out max);
                }

                for_spt.Add(new CSpatialTree.STreeInput(entity.GetWorldPosition(), count, entity.GetPortalInfo(), min, max));

                // Add the name of the entity in the dictioanry along with the entity id.
                if (m_entityNameToEntityID.ContainsKey(entity.GetEntityName()) == false)
                {
                    m_entityNameToEntityID.Add(entity.GetEntityName(), count);
                }

                // DEBUG: When Crtl+R is pressed then all entites wil be rendered and this list will be used for the rendering.
                m_whenRenderAll.Add(for_spt.Count - 1);

                ++count;
            }

            // Sort the scene out into a spatial partioning tree
            CCompositeSpatialTree temp_sp = new CCompositeSpatialTree();
            if (temp_sp.Initialise(new Vector3(-2048, -2048, -2048), new Vector3(2048, 2048, 2048), new Vector3(3, 1, 3), new Vector3(2, 1, 2), for_spt,
                out m_roomList, m_entityList) == false)
            {
                // Prints to output debug window
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CGameSceenHelper", "ONDERZOEK ENGINE ERROR: The spatial partioning tree cannot be Initialised.");
#if ENGINE
                ScreenManager.Exit();
#endif
                return false;
            }
            else
            {
                m_sp = temp_sp;
            }

            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CGameSceenHelper", "SPT has been created.");

            return true;
        }

        private void SetUpPortalSystem()
        {
            // First we need to search through the entity list and find all the portal rooms.
            List<CEntity> portal_rooms = new List<CEntity>();
            uint id = 1;
            foreach (CEntity entity in m_entityList)
            {
                if (entity.IsPortalRoom() == true)
                {
                    entity.SetPortalRoomID(id);
                    portal_rooms.Add(entity);
                    ++id;
                }
                else // Reset the portal ids. They will be set again in the last part of this function.
                {
                    entity.SetPortalRoomID(0);
                }
            }

            // Now check to see if any of the portal rooms are within a portal room
            // If the number is high for the respective room then that means that it is within that many rooms.
            Dictionary<CEntity, int> num_levels = new Dictionary<CEntity, int>();
            foreach (CEntity rentity in portal_rooms)
            {
                num_levels.Add(rentity, 0);
                foreach (CEntity rentity_cmpr in portal_rooms)
                {
                    if (rentity_cmpr == rentity)
                    {
                        continue;
                    }

                    if (CToolbox.IsPointInBoundingBox(rentity_cmpr.GetFirstPlanes(), rentity.GetWorldPosition()) == true)
                    {
                        ++num_levels[rentity];
                    }
                }
            }
            portal_rooms.Clear();

            // Now sort the num_levels dictionary so that the room entity with the least number of levels comes first.
            // This is done so that entities which are in the encapsulated rooms will evetually be palced in the right room.
            // If this wasn't done then there's a chance that the room encapsulating the smaller room will take care of the smaller room's entities,
            // which is wrong and entities will start to disappear.
            while (num_levels.Count != 0)
            {
                int cur_level = 0;
                foreach (KeyValuePair<CEntity, int> rentity in num_levels)
                {
                    if (rentity.Value == cur_level)
                    {
                        portal_rooms.Add(rentity.Key);
                    }
                    if (num_levels.Count == 0)
                    {
                        break;
                    }
                }

                for (int i = 0; i < portal_rooms.Count; ++i)
                {
                    num_levels.Remove(portal_rooms[i]);
                }

                ++cur_level;
            }

            // Now check through all the entities and see if they are in the current room.
            foreach (CEntity rentity in portal_rooms)
            {
                foreach (CEntity entity in m_entityList)
                {
                    if (rentity == entity)
                    {
                        continue;
                    }

                    if (CToolbox.IsPointInBoundingBox(rentity.GetFirstPlanes(), entity.GetWorldPosition()) == true)
                    {
                        //if (entity.GetPortalInfo().s_isPortal == true)
                        //{
                            //entity.SetAsChild(rentity.GetWorldRotation(), rentity.GetWorldMatrix());
                        //}

                        entity.SetPortalRoomID(rentity.GetPortalRoomID());
                    }
                }
            }
        }

        public bool RemoveEntity(CEntity Unrequired)
        {
            if (RemoveEntityNoSPT(Unrequired) == false)
            {
                return false;
            }

            SetupSPT();

            return true;
        }

        // Pass this fucntion the entity that is not required. It will remove the entity from all the lists that it is in.
        public bool RemoveEntityNoSPT(CEntity Unrequired)
        {
            // If entity is a dynamic entity then remove.
            if (Unrequired.IsDynamicEntity() == true)
            {
                if (m_dynamicEntityList.Remove((CDynamicEntity)Unrequired) == false)
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CGameSceenHelper", "ONDERZOEK ENGINE ERROR: RemoveEntity(): Dynamic entity not found");
                    return false;
                }
            }

            // If entity is a terrain entity then remove.
            if (Unrequired.IsTerrain() == true)
            {
                if (m_collision.RemoveTerrain((CTerrainEntity)Unrequired) == false)
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CGameSceenHelper", "ONDERZOEK ENGINE ERROR: RemoveEntity(): Terrain entity not found.");
                    return false;
                }
            }

            // If entity is a directional light which casts a shadow then remove.
            if (string.Compare(Unrequired.ToString(), "Onderzoek.Entity.CDirLightEntity") == 0)
            {
                //if (((CDirLightEntity)Unrequired).IsShadowCaster() == true)
                {
                    if (m_scDirLightList.Remove((CDirLightEntity)Unrequired) == false)
                    {
                        CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CGameSceenHelper", "ONDERZOEK ENGINE ERROR: RemoveEntity(): Dir light not found.");
                        return false;
                    }
                }
            }

            // If entity is a spot light which casts a shadow then remove.
            if (string.Compare(Unrequired.ToString(), "Onderzoek.Entity.CSpotLightEntity") == 0)
            {
                if (((CSpotLightEntity)Unrequired).IsShadowCaster() == true)
                {
                    if (m_scSpotLightList.Remove((CSpotLightEntity)Unrequired) == false)
                    {
                        CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CGameSceenHelper", "ONDERZOEK ENGINE ERROR: RemoveEntity(): Spot light not found.");
                        return false;
                    }
                }
            }

#if ENGINE
            CModel.SModelData num_of_models;
            CModel model = Unrequired.GetModel();
            if (model != null)
            {
                if (m_modelList.TryGetValue(model.GetModelName(), out num_of_models) == true)
                {
                    // Reduce the count of models.
                    --num_of_models.s_numOfInstances;
                    m_modelList[model.GetModelName()] = num_of_models;
                    if (num_of_models.s_numOfInstances < 1)
                    {
                        // Delete model from memory.
                        m_modelList.Remove(model.GetModelName());
                        model.Delete();
                        model = null;
                    }
                }
            }

            // Remove the textures from the texture list.
            List<CMaterial> mat_list = Unrequired.GetMaterialList();
            if (mat_list != null)
            {
                foreach (CMaterial mat in mat_list)
                {
                    if (mat.m_textureID != -1)
                    {
                        CVisual.Instance.RemoveTexture(mat.m_textureID);
                        mat.m_textureID = -1;
                    }
                    if (mat.m_specularID != -1)
                    {
                        CVisual.Instance.RemoveTexture(mat.m_specularID);
                        mat.m_specularID = -1;
                    }
                    if (mat.m_normalID != -1)
                    {
                        CVisual.Instance.RemoveTexture(mat.m_normalID);
                        mat.m_normalID = -1;
                    }
                }
            }
#endif

            // Removing the entity from the main entity list.
            if (m_entityList.Remove(Unrequired) == false)
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CGameSceenHelper", "ONDERZOEK ENGINE ERROR: RemoveEntity(): Entity not found.");
                return false;
            }

            // Now it can completely remove the entity from the face of this planet.
            Unrequired.Dispose();
            Unrequired = null;

            return true;
        }

        // Just create a new entity, such as:
        // CDotXEntity new_entity = new CDotXEntity(pos, Vector3.Zero, m_modelList.Count - 1, m_modelList[m_modelList.Count - 1].GetOBB(),
        //            m_modelList[m_modelList.Count - 1].GetMaterials(m_content, "Model/red"), false, new CEntity.SPortalInfo());
        // Now pass new_entity to this function and all the rest will be taken care of, so don't add new_entity to m_entityList
        // becasue this function does that for you and also for the other lists if necessary.
        public bool AddEntity(CEntity NewEntity)
        {
            if (AddEntityNoSPT(NewEntity) == false)
            {
                return false;
            }

            SetupSPT();

            return true;
        }

        public virtual bool AddEntityNoSPT(CEntity NewEntity)
        {
            // If entity is a dynamic entity then add to dynamic entity list.
            if (NewEntity.IsDynamicEntity() == true)
            {
                m_dynamicEntityList.Add((CDynamicEntity)NewEntity);
            }

            // If entity is a directional light which casts a shadow then add.
            if (string.Compare(NewEntity.ToString(), "Onderzoek.Entity.CDirLightEntity") == 0)
            {
                //if (((CDirLightEntity)NewEntity).IsShadowCaster() == true)
                {
                    m_scDirLightList.Add((CDirLightEntity)NewEntity);
                }
            }

            // If entity is a spot light which casts a shadow then add.
            if (string.Compare(NewEntity.ToString(), "Onderzoek.Entity.CSpotLightEntity") == 0)
            {
                if (((CSpotLightEntity)NewEntity).IsShadowCaster() == true)
                {
                    m_scSpotLightList.Add((CSpotLightEntity)NewEntity);
                }
            }

            // Add the entity to the main entity list.
            m_entityList.Add(NewEntity);

            return true;
        }

#if ENGINE
        /// <summary>
        /// Updates the dynamic entities.
        /// </summary>
        /// <param name="gameTime"></param>
        public void BeginUpdate(GameTime gameTime)
        {
            // Delete any entities which need to be deleted.
            if (m_deadEntites.Count > 0)
            {
                foreach (CEntity entity in m_deadEntites)
                {
                    RemoveEntityNoSPT(entity);
                }

                m_deadEntites.Clear();

                SetupSPT();
            }

            // Create new entities.
            if (m_newEntites.Count > 0)
            {
                foreach (string entity in m_newEntites)
                {
                    CEntity loaded_entity;
                    if (CLevelLoader.loadEntityFromXML("Content/xml/Level/testlevel.olv", entity, out loaded_entity, m_content, m_modelList) == true)
                    {
                        AddEntityNoSPT(loaded_entity);
                    }
                }

                m_newEntites.Clear();

                SetupSPT();
            }

            // Update dynamic entities.
            m_sceneGraph.Update(gameTime);
        }
#else
        /// <summary>
        /// Updates the dynamic entities.
        /// </summary>
        /// <param name="gameTime"></param>
        public void BeginUpdate()
        {
            // Delete any entities which need to be deleted.
            if (m_deadEntites.Count > 0)
            {
                foreach (CEntity entity in m_deadEntites)
                {
                    RemoveEntityNoSPT(entity);
                }

                m_deadEntites.Clear();

                SetupSPT();
            }

            // Update the dynamic entities.
            for (int i = 0; i < m_entityList.Count; ++i)
            {
                m_entityList[i].Update();
            }
        }
#endif

        /// <summary>
        /// This needs to be called after all the updating is finsihed. No more updating should occur in this frame after this function is called.
        /// </summary>
        /// <param name="gameTime"></param>
        /// <param name="Camera">The main camera which will be used to display on screen.</param>
        /// <param name="PlayerPos">The player's position</param>
        public void EndUpdate(GameTime gameTime, CCamera Camera, Vector3 PlayerPos)
        {
            // This is used to freeze the view frustum
            // m_inputData.m_leftClick is being used for DEBUG purposes only.
#if ENGINE
            if (ScreenManager.m_inputData.m_leftClick == false)
#endif
            {
                CVisual.Instance.SetPrvPosRot(Camera.GetPosition(), Camera.GetRotation());
            }

            /////////////////////////////////////////////////////////////For Normal Rendering/////////////////////////////////////////////////////////
            // ALL entities: This is done to help prevent the portal calculations from populating the renderable entity list with the same entities.
            for (int i = 0; i < m_entityList.Count; ++i)
            {
                m_entityList[i].SetRender(false, true);
            }

            // Checking to see if the entites are in the view frustum
#if ENGINE
            if (ScreenManager.m_inputData.m_renderAll == false)
            {
#endif
                CheckAgainstCameraPlanes(Camera, ref m_renderable);
#if ENGINE
            }
            else
            {
                m_renderable = m_whenRenderAll;
            }
#endif
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

            if (CVisual.Instance.AreShadowsActive == true)
            {
                foreach (CSpotLightEntity spot in m_scSpotLightList)
                {
                    if (spot.IsShadowCaster() == true && spot.GetRender() == true)
                    {
                        /////////////////////////////////////////////////////////////For Shadow///////////////////////////////////////////////////////////////////////
                        // ALL entities: This is done to help prevent the portal calculations from populating the renderable entity list with the same entities.
                        // This is for the GI SPT list
                        for (int i = 0; i < m_entityList.Count; ++i)
                        {
                            m_entityList[i].SetRender(false, false);
                        }

                        // Checking to see which entities are visible to m_scSpotLightList
                        CheckAgainstCameraPlanes(spot.GetCamera(), ref spot.m_renderList);
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    }
                }
                // Checking to see which entities are visible to m_scDirLightList
                foreach (CDirLightEntity dir in m_scDirLightList)
                {
                    if (dir.IsShadowCaster() == true && dir.GetRender() == true)
                    {
                        dir.Update(Camera);

                        for (int i = 0; i < dir.m_renderList.Length; ++i)
                        {
                            /////////////////////////////////////////////////////////////For Shadow///////////////////////////////////////////////////////////////////////
                            // ALL entities: This is done to help prevent the portal calculations from populating the renderable entity list with the same entities.
                            // This is for the GI SPT list
                            for (int j = 0; j < m_entityList.Count; ++j)
                            {
                                m_entityList[j].SetRender(false, false);
                            }

                            // Checking to see which entities are visible to m_scDirLightList
                            CheckAgainstCameraPlanes(dir.GetCamera(i), ref dir.m_renderList[i]);
                        }
                        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    }
                }
            }

            // Should try this with Shadow render lists, except no need to seperate translucent entities.
            m_sceneGraph.SortRenderList(ref m_renderable, ref m_renderableTran, m_entityList);

            // Sorting entities front to back order
            CToolbox.SortEntitiesByDistFromCam(ref m_renderableTran, m_entityList, Camera);

#if ENGINE
            for (int i = 0; i < m_hudList.Count; ++i)
            {
                m_hudList[i].Update(gameTime, ScreenManager.m_inputData);
            }
#endif
        }

        private void CheckAgainstCameraPlanes(CCamera Camera, ref List<int> RenderableList)
        {
            RenderableList.Clear();
            bool use_main_camera_planes = true;

            List<Plane[]> plane_list = new List<Plane[]>();
            List<Plane[]> cur_plane = new List<Plane[]>();
            cur_plane.Add(Camera.GetClipPlanes());

            // This quick sort is required here so that the closest room gets checked first.
            // This helps because if this didn't happen then if the camera was in the second room the first room would get rendered.
            CToolbox.SortEntitiesByDistFromCam(ref m_roomList, m_entityList, Camera.GetPosition());

            // Loop through all the rooms. If the camera happens to be in any room then it will add to the plane_list and change the cur_plane as well.
            CMainRoomCompositeSpatialTree.EReturnValues rtn_val = CMainRoomCompositeSpatialTree.EReturnValues.e_outsideRoom;
            for (int i = 0; i < m_roomList.Count; ++i)
            {
                rtn_val = m_roomList[i].CheckAndGetNewPlanes(ref cur_plane, Camera.GetPosition(), Camera.GetDir(), m_entityList, RenderableList, ref plane_list);

                switch (rtn_val)
                {
                    case CMainRoomCompositeSpatialTree.EReturnValues.e_portalsFound:
                        {
                            use_main_camera_planes = false;
                            break;
                        }
                    case CMainRoomCompositeSpatialTree.EReturnValues.e_noPortals:
                        {
                            use_main_camera_planes = false;
                            // We're removing the first seet of planes, which is the min cameras clip planes.
                            // By doing this any other room outside the current room will not be rendered.
                            cur_plane.RemoveAt(0);
                            break;
                        }
                }
            }

            // Add the main camera's clip planes only if not in any rooms, in which case use_main_camera_planes will be set to false,
            // and if the rtn_val is equal to e_outsideRoom.
            if (use_main_camera_planes == true && rtn_val == CMainRoomCompositeSpatialTree.EReturnValues.e_outsideRoom)
            {
                plane_list.Add(Camera.GetClipPlanes());
            }

            // Now check the scene with all the planes.
            m_sp.CheckWithCameraPlanes(plane_list, m_entityList, RenderableList);
        }

        public void BeginDraw(CCamera Camera, GameTime gameTime)
        {
            // Water Rendering
            CVisual.Instance.SetDT(gameTime);

            // NOTE: REMEMBER TO SET THE MODELT PROPERTY TO GENERATE TANGENTS.
            // We're rendering stuff into render targets for the water reflections.
            if (CVisual.Instance.NumberOfWaterEntitiesToRender > 0)
            {
                CVisual.Instance.SetupForWaterRendering();

                for (int i = 0; i < CVisual.Instance.NumberOfWaterEntitiesToRender; ++i)
                {
                    CWCamera reflect_camera;
                    CVisual.Instance.StartWaterRendering(out reflect_camera, Camera, i, m_skyBox);

                    CVisual.Instance.BeginForwardRenderingForWater(m_mainLightEntity, reflect_camera);

                    // Render all the entities
                    for (int j = 0; j < m_renderable.Count; ++j)
                    {
                        m_entityList[m_renderable[j]].ForwardRender(reflect_camera);
                    }

                    //for (int j = 0; j < m_renderableTran.Count; ++j)
                    //{
                    //    m_entityList[m_renderableTran[j]].ForwardTranslucentRender(reflect_camera);
                    //}
                }

                CVisual.Instance.PackUpWaterRendering();
            }

            CVisual.Instance.ClearScreen();

            CVisual.Instance.BeginRender(Camera);

            CVisual.Instance.EnableDepthTest();
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// You can render things after this but it will be directly renderd into the back buffer; no deferred rendering after MainDraw is called.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        /// <param name="Camera">The main camera which will be used to render on to the screen.</param>
        /// <param name="NoPP">When set to true, this will render to the backbuffer. When set to false this will render to m_rtSpareFS1 (a render target)</param>
        public void MainDraw(GameTime gameTime, CCamera Camera/*, bool NoPP*/)
        {
            //m_noPP = NoPP;
#if ENGINE
            CVisual.Instance.UpdateForDebugScreen(gameTime);
#endif

            // To render objects normally
            // This list is required for the translucent objects.
            for (int i = 0; i < m_renderable.Count; ++i)
            {
                m_entityList[m_renderable[i]].Render(Camera);
            }

            // "Render" the main light. It actually adds this light to the list of lights that will be used to light the scene.
            if (m_mainLightEntity != null)
            {
                m_mainLightEntity.Render(Camera);
            }

#if WINDOWS
            // Save the depth stencil buffer for later rendering if needed by the user.
            CVisual.Instance.SaveNormalRenderDSB();
#endif

            if (CVisual.Instance.AreShadowsActive == true)
            {
                // To render objects for Shadows
                CVisual.Instance.BeginRenderForShadow();
                foreach (CSpotLightEntity spot in m_scSpotLightList)
                {
                    if (spot.IsShadowCaster() == true && spot.GetRender() == true)
                    {
                        CVisual.Instance.SetRTForShadow(spot.GetRTIndex(), spot.GetCamera());
                        for (int i = 0; i < spot.m_renderList.Count; ++i)
                        {
                            m_entityList[spot.m_renderList[i]].RenderForShadow(spot.GetCamera());
                        }
                    }
                }
                foreach (CDirLightEntity dir in m_scDirLightList)
                {
                    if (dir.IsShadowCaster() == true && dir.GetRender() == true)
                    {
                        for (int j = 0; j < dir.m_renderList.Length; ++j)
                        {
                            COrthoCamera cur_dir_light_cam = dir.GetCamera(j);
                            CVisual.Instance.SetRTForShadow(dir.GetRTIndex(j), cur_dir_light_cam);
                            for (int i = 0; i < dir.m_renderList[j].Count; ++i)
                            {
                                m_entityList[dir.m_renderList[j][i]].RenderForShadow(cur_dir_light_cam);
                            }
                        }
                    }
                }
                CVisual.Instance.EndRenderForShadow();
            }

            CVisual.Instance.DisableDepthTest();
            CVisual.Instance.EndRender(Camera, m_giBB, m_rsmCamera, m_mainLightEntity/*.GetLightDir(), NoPP*/);

            // Draw the SkyBox
            if (m_skyBox != null)
            {
                m_skyBox.Render(Camera);
            }

#if WINDOWS
            // Restore the depth stencil buffer for later rendering if needed by the user.
            CVisual.Instance.RestoreNormalRenderDSB();
#endif

            CVisual.Instance.EnableDepthTest();
        }

        /// <summary>
        /// Renders the transluscent entities.
        /// </summary>
        /// <param name="Camera">The main camera, same as the one that was passed in MainDraw.</param>
        public void TranslucentDraw(CCamera Camera)
        {
            ////////////////////////////////////////////////Rendering Translucent Entities/////////////////////////////////////////////
            // This needs to be called if you want to have a light, currently only ONE directional light is supported.
            // If you do not wish to add any lights to the objects you are about to render then do not add this line.
            CVisual.Instance.BeginRenderForTransparentEntities(/*m_mainLightEntity, Camera*/);

            // To render translucent objects.
            for (int i = 0; i < m_renderableTran.Count; ++i)
            {
                m_entityList[m_renderableTran[i]].ForwardTranslucentRender(Camera);
            }

            CVisual.Instance.EndRenderForTransparentEntities();
            ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        }

        /// <summary>
        /// Renders the GUI/HUD and debug information.
        /// </summary>
        /// <param name="Camera">The main camera, same as the one that was passed in MainDraw.</param>
        public void EndDraw(CCamera Camera)
        {
#if WINDOWS
            //if (m_noPP == false)
            //{
                CVisual.Instance.DisableDepthTest();
                //  This might need to be called before any post processing that the programmer wishes to use.
                CVisual.Instance.SwapBuffer();
                CVisual.Instance.EnableDepthTest();
            //}
#endif

            // Rendering the water entity.
            CVisual.Instance.RenderWater(Camera);

            // Rendering the explosion spheres/ripple.
            CVisual.Instance.RenderExplosionRipple(Camera);

            // Renders debug lines, which may have been set by the code during the update
            CVisual.Instance.RenderDebugLine(Camera);

#if ENGINE
            for (int i = 0; i < m_hudList.Count; ++i)
            {
                m_hudList[i].Render();
            }

            CVisual.Instance.RenderDebugInfo(Camera.GetPosition(), m_entityList.Count, m_modelList.Count, ScreenManager.m_inputData);

            if (ScreenManager.m_inputData.m_rightClick == true)
            {
                CVisual.Instance.EnableDepthTest();

                // Draw the camera view frustum
                CVisual.Instance.DrawFrustum(Camera);

                for (int i = 0; i < m_renderable.Count; ++i)
                {
                    if (m_entityList[m_renderable[i]].IsHidden() == false)
                    {
                        CEntityOBB[] temp = m_entityList[m_renderable[i]].GetWorldOBB();
                        if (temp != null)
                        {
                            CVisual.Instance.DrawBounds(Camera, temp);
                        }
                    }
                }
                for (int i = 0; i < m_renderableTran.Count; ++i)
                {
                    if (m_entityList[m_renderableTran[i]].IsHidden() == false)
                    {
                        CVisual.Instance.DrawBounds(Camera, m_entityList[m_renderableTran[i]].GetWorldOBB());
                    }
                }
            }
#else
            CVisual.Instance.RenderStoredBounds(Camera);
#endif
        }

        public bool AddEntityToDeleteList(CEntity Entity)
        {
            if (Entity != null)
            {
                m_deadEntites.Add(Entity);
                return true;
            }

            return false;
        }

        public bool AddEntityToNewList(string Entity)
        {
            int val = 0;
            if (m_entityNameToEntityID.TryGetValue(Entity, out val) == false)
            {
                m_newEntites.Add(Entity);
                return true;
            }

            return false;
        }

        public virtual CEntity GetEntity(string Name)
        {
            int entity_id = -1;
            if (m_entityNameToEntityID.TryGetValue(Name, out entity_id) == true)
            {
                return GetEntity(entity_id);
            }
            else
            {
                return null;
            }
        }

        public virtual CEntity GetEntity(int ID)
        {
            return m_entityList[ID];
        }
    }
}

﻿#if EDITOR
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using Microsoft.Xna.Framework;
using Microsoft.Xna;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using Onderzoek.ModelData;
using Onderzoek.Entity;
using Onderzoek.SpatialTree;
using Onderzoek.Visual;
using Onderzoek.Input;
using Onderzoek.Physics;
using Onderzoek.GUI;
using Onderzoek.Camera;



namespace Onderzoek.Screens
{
    public class CEditorScreen : CGameScreenHelper
    {
        //editor handles

        public CDotXScalableEntity xMovementHandle;
        public CDotXScalableEntity yMovementHandle;
        public CDotXScalableEntity zMovementHandle;
        public CDotXScalableEntity xRotationHandle;
        public CDotXScalableEntity yRotationHandle;
        public CDotXScalableEntity zRotationHandle;

        public CDotXEntity editorGrid;

        public CDotXScalableEntity xScaleHandle;
        public CDotXScalableEntity yScaleHandle;
        public CDotXScalableEntity zScaleHandle;

        public bool hideGrid;


        public List<CEntity> forwardRenderList;// a list of entities to be rendered without being updated or collided with.


        // Main camera
        public C3PCamera m_camera;


        public CEditorScreen()
        {
            forwardRenderList = new List<CEntity>();
            
            //base.Initalise(Services);

            //Initialise();
        }


        ~CEditorScreen()
        {
            if (m_initialised == true)
            {
                m_camera = null;

                this.DeInitialise();
            }
        }


        public void setContentManager(ContentManager contentMgr)
        {
            m_content = contentMgr;

        }


        public void Initialise()
        {
            base.Initialise();

            //m_camera = new CCamera(CVisual.Instance.GetAspectRatio(), 90.0f, 1.0f, 1000.0f, new Vector3(22.0f, 17.0f, -22.0f), Vector3.Zero, Vector3.Zero);
            m_camera = new C3PCamera(CVisual.Instance.GetAspectRatio(), 45.0f, 1.0f, 1000.0f, Vector3.Zero,
                new Vector3(-MathHelper.PiOver4 / 2.0f, MathHelper.PiOver4, 0), 140.0f, null);

            //LoadContent();
        }


        public override void DeInitialise()
        {
            if (m_collision != null)
            {
                m_collision.Dispose();
                m_collision = null;
            }

            CVisual.Instance.FreeLevelTextureMemory();

            base.DeInitialise();
        }


        public override bool LoadContent()
        {
            // LOAD THINGS HERE AND MAKE SURE base.LoadContent() IS CALLED AT THE END.

            // Sort the scene out into a spatial partioning tree
            return base.LoadContent();
        }


        public void clearAllEntities()
        {
            for (int i = 0; i < m_entityList.Count; ++i)
            {
                RemoveEntity(m_entityList[i]);
                --i;
            }
        }

        
        public void Update()
        {
            // These lines are for the way points
            CVisual.Instance.RemoveLinesWithThisColour(new Color(5, 255, 5));
            CVisual.Instance.RemoveLinesWithThisColour(new Color(255, 5, 5));

            // These lines are for the dir light
            CVisual.Instance.RemoveLinesWithThisColour(new Color(50, 255, 75));
            CVisual.Instance.RemoveLinesWithThisColour(new Color(255, 50, 50));

            // These lines are for the spot light
            CVisual.Instance.RemoveLinesWithThisColour(new Color(50, 255, 100));
            CVisual.Instance.RemoveLinesWithThisColour(new Color(255, 50, 75));

            Vector3 v = Vector3.One *((m_camera.GetPosition() - xMovementHandle.GetPosition()).Length() / 200);
            xMovementHandle.Scale = v*1.5f;
            yMovementHandle.Scale = v * 1.5f;
            zMovementHandle.Scale = v * 1.5f;

            v = Vector3.One * ((m_camera.GetPosition() - xRotationHandle.GetPosition()).Length() / 200);
            xRotationHandle.Scale = v * 1f;
            yRotationHandle.Scale = v * 1f;
            zRotationHandle.Scale = v * 1f;

            v = Vector3.One * ((m_camera.GetPosition() - xScaleHandle.GetPosition()).Length() / 200);
            xScaleHandle.Scale = v * 0.5f;
            yScaleHandle.Scale = v * 0.5f;
            zScaleHandle.Scale = v * 0.5f;

            xMovementHandle.SetPosition(xMovementHandle.GetPosition());
            yMovementHandle.SetPosition(yMovementHandle.GetPosition());
            zMovementHandle.SetPosition(zMovementHandle.GetPosition());
            

            // NOTE: The editor cannot update dynamic entites, please ask Hank if you would like this to change.

            // YOU CAN UPDATE EDITOR SPECIFIC ENTITIES HERE. MAKE SURE TO CALL base.SHUpdate AND base.UpdateEntities() AT THE END.
            // NOTE: base.SHUpdate and UpdateEntities TAKES CARE OF ALL THE GENERIC UPDATES FOR THE ENTITIES.

            //m_camera.RefreshFPV();
            //m_Camera.GetView() = Matrix.Invert(Matrix.CreateFromQuaternion(m_camera.m_rotation) * Matrix.CreateTranslation(m_camera.m_position));
            m_camera.Refresh();

            base.BeginUpdate();
            base.EndUpdate(null, m_camera, m_camera.GetPosition());                        
        }


        public void updateEditorScreenStuff(List<CEntity> currentEntityList)
        {
            //m_entityList.Clear();
           // m_entityList.AddRange(currentEntityList);
            //m_camera = currentCamera;




        }


        public void Draw()
        {
            base.BeginDraw(m_camera);

            // THE ENGINE WILL TAKE CARE OF RENDERING ALL THE ENTITIES.
            // YOU CAN ADD ANY EDITOR SPECIFIC CODE IN BETWEEN base.BeginDraw() AND base.EndDraw.

            //for (int i = 0; i < m_renderable.Count; ++i)

            base.MainDraw(new GameTime(), m_camera);

            CVisual.Instance.BeginForwardRender(m_camera);
            //render handles
            {
                //CVisual.Instance.DisableDepthTest();

                if (xRotationHandle.GetRender())
                {
                    xRotationHandle.ForwardRender(m_camera);
                    yRotationHandle.ForwardRender(m_camera);
                    zRotationHandle.ForwardRender(m_camera);
                }

                if (xScaleHandle.GetRender())
                {
                    xScaleHandle.ForwardRender(m_camera);
                    yScaleHandle.ForwardRender(m_camera);
                    zScaleHandle.ForwardRender(m_camera);
                }

                for (int i = 0; i < forwardRenderList.Count; i++)
                {
                    forwardRenderList[i].ForwardRender(m_camera);
                }

                if(!hideGrid)
                editorGrid.ForwardTranslucentRender(m_camera);
                //CVisual.Instance.EnableDepthTest();

                if (xMovementHandle.GetRender())
                {
                    CVisual.Instance.DisableDepthCompare();
                    xMovementHandle.ForwardRender(m_camera);
                    yMovementHandle.ForwardRender(m_camera);
                    zMovementHandle.ForwardRender(m_camera);
                    CVisual.Instance.EnableDepthCompare();
                }

            }
            base.TranslucentDraw(m_camera);

            CVisual.Instance.EndForwardRender();

            base.EndDraw(m_camera);

        }


    }
}
#endif
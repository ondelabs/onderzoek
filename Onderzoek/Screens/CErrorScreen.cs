﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Onderzoek.Visual;
using Onderzoek.GUI;

namespace Onderzoek.Screens
{
    public class CErrorScreen : CScreen
    {
        private List<CHUD> m_noneSelectableHudList;

        //int m_curButton;
        //int m_maxButtons;

        public CErrorScreen(GameServiceContainer Services, string ErrorMsg)
        {
            base.Initialise(Services);

            m_noneSelectableHudList = new List<CHUD>();

            CHUDButton temp_hud2 = new CHUDButton(CVisual.Instance.LoadHudTexture("Texture/error_bg", m_content), "Texture/error_bg", new Vector2(0.0f, 46.0f), new Vector2( 100.0f, 20.0f ) );
            m_noneSelectableHudList.Add(temp_hud2);

            CHUDButtonChar temp_hud1 = new CHUDButtonChar(ErrorMsg, new Vector2(0.0f, 50.0f), 2.0f, null);
            m_noneSelectableHudList.Add(temp_hud1);

            //m_maxButtons = m_hudList.Count;
            //m_curButton = 0;
        }

        ~CErrorScreen()
        {
            DeInitialise();
        }

        public override void DeInitialise()
        {
            foreach (CHUD hud in m_noneSelectableHudList)
            {
                hud.FreeMemory();
            }

            m_noneSelectableHudList.Clear();
        }

        public override void Update(GameTime gameTime)
        {
            if (ScreenManager.m_inputData.m_back == true || ScreenManager.m_inputData.m_enter == true)
            {
                ScreenManager.m_inputData.m_enter = false;
                ScreenManager.m_inputData.m_back = false;

                ScreenManager.RemoveMsgFloatingScreen(this);

                return;
            }
        }

        public override void Draw(GameTime gameTime)
        {
            foreach (CHUD hud in m_noneSelectableHudList)
            {
                hud.Render();
            }

            for (int i = 0; i < m_hudList.Count; ++i)
            {
                m_hudList[i].Render();
            }
        }
    }
}

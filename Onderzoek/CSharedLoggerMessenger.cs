﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Onderzoek
{
    public enum ESharedLoggerType
    {
        e_error = 0,
        e_info,
        e_finish,
        e_nothing,
        e_resetInfoLog,
        e_resetErrorLog
    };

    public struct SSharedMessage
    {
        public ESharedLoggerType s_messageType;
        public string s_tag;
        public string s_message;
    }
    
    public class CSharedLoggerMessenger
    {
        private SSharedMessage m_sharedMessage;
        private bool m_hasRead = true;

        static CSharedLoggerMessenger instance;

        public static CSharedLoggerMessenger Instance
        {
            get { return instance; }
        }

        public CSharedLoggerMessenger()
        {
            if (instance != null)
                throw new Exception("Cannot have multiple singletons");

            instance = this;
        }

        public SSharedMessage ReadMessage()
        {
            lock (this)
            {
                if (m_hasRead == true)
                {
                    try
                    {
                        Monitor.Wait(this);
                    }
                    catch (SynchronizationLockException e)
                    {
                        Console.WriteLine(e);
                    }
                    catch (ThreadInterruptedException e)
                    {
                        Console.WriteLine(e);
                    }
                }

                m_hasRead = true;
                Monitor.Pulse(this);
            }

            return m_sharedMessage;
        }

        public void WriteMessage(ESharedLoggerType MessageType, string Tag, string Message)
        {
            lock (this)
            {
                if (m_hasRead == false)
                {
                    try
                    {
                        Monitor.Wait(this);
                    }
                    catch(SynchronizationLockException e)
                    {
                        Console.WriteLine(e);
                    }
                    catch(ThreadInterruptedException e)
                    {
                        Console.WriteLine(e);
                    }
                }

                m_sharedMessage.s_message = Message;
                m_sharedMessage.s_tag = Tag;
                m_sharedMessage.s_messageType = MessageType;
                m_hasRead = false;
                Monitor.Pulse(this);
            }
        }
    }
}

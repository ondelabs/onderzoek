﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using Onderzoek.Visual;
using Onderzoek.ModelData;
using Onderzoek.SpatialTree;
using Onderzoek.Physics;
using Onderzoek.SceneGraph;

namespace Onderzoek.Entity
{
    public class CDynamicEntity : CEntity
    {
        protected CTerrainEntity.SHeightMapData m_entityHMData;
        // This is needed by the scene graph.
        protected int m_sgID = -1;
        protected int m_updateTicket = -1;

        // This is an internal ID, do not alter!
        // Only to be used by the scene graph.
        public virtual int SGID
        {
            get { return m_sgID; }
            set { m_sgID = value; }
        }

        public override int? GetSGID()
        {
            return m_sgID;
        }

        public override void SetUpdateTicket(int UpdateTicket)
        {
            m_updateTicket = UpdateTicket;
        }

        // This is used by the collision engine to set the new height of the entity.
        public override void SetNewHeight(CEntity HitEntity, Vector3 Height)
        {
            m_position.Y = Height.Y;
        }

        public override bool GetHeightMapData(out CTerrainEntity.SHeightMapData HMData)
        {
            HMData = m_entityHMData;
            return true;
        }

        public virtual Vector3 GetDirectionOfMovement()
        {
            return Vector3.Zero;
        }

        public CDynamicEntity()
        {
            m_position = Vector3.Zero;
            m_rotation = Quaternion.Identity;
        }

        public void Initalise()
        {
            m_parWrldMat = Matrix.Identity;
        }

        public void UpdateIfChild()
        {
            if (m_isChild == true)
            {
                m_worldMatrix = m_worldMatrix * m_parWrldMat;
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            m_modelData = null;

            if (m_entityHMData.s_surroundingTileIDs != null)
            {
                m_entityHMData.s_surroundingTileIDs.Clear();
            }
        }

        /// <summary>
        /// This functions sets the parent's local world and rotation to the paramaters which have been sent in.
        /// </summary>
        /// <param name="ParWorldMatrix">The parent's local matrix.</param>
        /// <param name="Rotation">The parent's local rotation</param>
        /// <returns>Returns true if the data has been set.</returns>
        public override bool GetParentDataForChild(ref Matrix ParWorldMatrix, ref Quaternion Rotation)
        {
            ParWorldMatrix = m_worldMatrix;
            Rotation = GetWorldRotation();

            return true;
        }

#if ENGINE
        protected CSpatialTree m_entityLeafNode;

        public virtual void Update(float DT)
        {
        }

        // No need to override this unless you want to use your own position or something
        // This uses a lot of shared memory access in the SPT.
        public virtual void STUpdateEntityNodeInTree()
        {
            m_entityLeafNode.MoveNodeAroundTree(m_position, m_maxLengthOfEntity);
        }

        public void SetEntityLeafNode( CSpatialTree EntityLeafNode )
        {
            m_entityLeafNode = EntityLeafNode;
        }

        public CSpatialTree GetEntityLeafNode()
        {
            return m_entityLeafNode;
        }
#endif
    }
}

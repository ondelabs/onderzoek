﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using Onderzoek.Visual;
using Onderzoek.ModelData;
using Onderzoek.Camera;
using Onderzoek.Physics;


#if EDITOR
using Onderzoek.SpatialTree;
#endif

namespace Onderzoek.Entity
{
    public enum EBasicEntityIDs
    {
        DotXEntity = 1,
        AnimatedEntity = 2,
        DirLightEntity = 3,
        DynamicDotXEntity = 4,
        TerrainEntity = 5,
        SpotLight = 6,
        RoomEntity = 7,
        PortalEntity = 8,
        DotXTerrainEntity = 9,
        PointLight = 10,
        BoxTrigger = 11,
        SphereTrigger = 12,
        KeyFramedEntity = 13,
        KFNode = 14,
        SkyBox = 15,
        e_water = 16
    }

    public abstract class CEntity
    {
        public struct SPortalInfo
        {
            // Portal stuff
            public bool s_isRoom;
            public uint s_roomID;
            public bool s_isPortal;
            public Vector3 s_portalNormal;
        }

        protected string m_entityName;
#if ENGINE
        protected CModel m_modelData; //A reference to the model that this entity is currently using.
#else
        protected CModel m_modelData; //A reference to the model that this entity is currently using.
        public virtual CModel ModelData
        {
            get { return m_modelData; }
            set
            {
                m_modelData = value;

                InitialiseEntityOBBOnly(m_modelData.GetOBB());
            }
        }
#endif
        protected CEntityOBB[] m_wrldOBB;
        protected Matrix m_worldMatrix;
        protected Vector3 m_position;
        protected Quaternion m_rotation = Quaternion.Identity;
        protected List<CMaterial> m_material;
        protected bool m_isTerrain = false;
        protected bool m_isSkinnedAnimation = false;
        protected SPortalInfo m_portalInfo;
        protected int m_associationID = 1;
        protected int m_dissociationID = 0;

#if ENGINE
        protected bool m_isCollidable = false;
        protected bool m_canBeStoodOn = true;
        protected bool m_simpleCollision = false;
#endif
        protected bool m_isDynamic;
        protected float m_maxLengthOfEntity;
        protected BoundingSphere m_sphere;
        protected float m_collisionDist = 1.0f;
        protected BoundingBox m_bb;

        protected bool m_canBeParent = false;
        protected bool m_isChild = false;
        protected bool m_manualRelationship = false;
        protected Matrix m_parWrldMat;
        protected Quaternion m_parRotation;

        protected bool m_render;    // This is used by the SPT, to set the visibility check m_hidden below.
#if ENGINE
        protected bool m_hidden; //is false by default. if set to true this entity is not to be rendered
#endif

        protected bool m_partTranslucent = false;
        protected bool m_wholeTranslucent = false;

        public struct SPartAndMatNum
        {
            public SPartAndMatNum(int PartNum, int PartMaterial)
            {
                s_partNum = PartNum;
                s_partMaterial = PartMaterial;
            }

            public int s_partNum;
            public int s_partMaterial;
        }

        protected Dictionary<int, List<SPartAndMatNum>> m_opaqueParts;
        protected Dictionary<int, List<SPartAndMatNum>> m_translucentParts;




        public virtual void Dispose()
        {
            m_wrldOBB = null;
            m_material = null;
        }

        /// <summary>
        /// matches this entities property list to the passed entites property list. The 2 entieis should be the same type
        /// </summary>
        public virtual void MatchPropertyListTo(CEntity passedPara)
        {
            for (int i = 0; i < m_propertyList.Count; i++)
            {
                for(int j=0; j< passedPara.m_propertyList.Count; j++)
                    if (m_propertyList[i].propertyName.Equals(passedPara.m_propertyList[j].propertyName))
                    {
                        m_propertyList[i].propertyValue = passedPara.m_propertyList[j].propertyValue;
                        break;
                    }

            }


        }
        public virtual void FillPropertyList()
        {
            

            //dont call me!
        }

        public virtual void SetFromListOfProperties(List<CEntityProperty> propList)
        {
            for (int i = 0; i < m_propertyList.Count; i++)
            {
                for(int j=0; j<propList.Count; j++)
                {
                    if (m_propertyList[i].propertyName.Equals(propList[j].propertyName))
                    {
                        m_propertyList[i].propertyValue = propList[j].propertyValue;
                        break;
                    }
                }
            }
            
        }

        public virtual int GetEntityID()
        {
            throw new System.Exception("EntityID not set, " + this.ToString());
        }

        public virtual void writeEntityProperties(XmlWriter writer)
        {
            for (int i = 0; i < m_propertyList.Count; i++)
            {
                writer.WriteElementString(m_propertyList[i].propertyName, m_propertyList[i].propertyValue);
            }
        }

        public virtual void readEntityProperties(XmlReader reader)
        {
            if (m_propertyList.Count == 0)
            {
                return;
            }

            reader.ReadToFollowing("EntityData");
            while (!(reader.Name == "EntityData" && reader.NodeType == XmlNodeType.EndElement))
            {
                reader.Read();
                reader.Read();

                reader.MoveToContent();
                string propName = reader.Name;

                if (reader.Name == "EntityData" && reader.NodeType == XmlNodeType.EndElement)
                    break;
                string propVal;

                if (reader.IsEmptyElement)
                    propVal = "";
                else
                {
                    reader.Read();
                }

                propVal = reader.Value;

                for (int i = 0; i < m_propertyList.Count; i++)
                {
                    if (m_propertyList[i].propertyName.Equals(propName))
                    {
                        m_propertyList[i].propertyValue = propVal;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// This function is called whent he property list is updated, as such it goes through all the properties
        /// and is an editor only function. called to tell teh object to recheck its properties. not called on 
        /// object moved.
        /// </summary>
        public virtual void UpdateFromPropertyList()
        {
        }

        public virtual void UpdateFromPropertyList(ContentManager content, Dictionary<string, CModel.SModelData> modelList)
        {
            UpdateFromPropertyList();
            RecheckModelAndMaterial(content, modelList);
            InitialiseEntityOBB(m_modelData.GetOBB());
        }

        public virtual void RecheckModelAndMaterial(ContentManager content, Dictionary<string, CModel.SModelData> modelList)
        {


        }


        // These are only used by the editor.
        public string m_modelName;
        public string m_materialName; //is usually null till it is changed.

        protected List<CEntityProperty> m_propertyList;
        public virtual List<CEntityProperty> PropertyList
        {
            get { return m_propertyList; }
        }

#if EDITOR
        public bool m_isCollidable = false; // needs to be visible on the editor form.
        public bool m_hidden; //is false by default. if set to true this entity is not to be rendered
        public bool m_canBeStoodOn = true;
        public bool m_simpleCollision = false;
        public bool m_isScalingAllowed = false;
        public bool m_dontSaveMe = false;

        public Vector3 m_editorRotation;
        public Boolean editor_renderMe;//indicates if this entity should be rendered in the editor. its a boolean cos i need it by reference

        public virtual Vector3 Scale
        {
            get { return Vector3.One; }
            set { }
        }

        public virtual void CollateKeyFrameData(ref List<CKeyFrame.SKeyFrame> KeyFrameData, bool GoingForward)
        {
        }

        public virtual bool GetPrvKFNode(ref CEntity PrvKFNode)
        {
            return false;
        }

        public virtual bool SetPrvKFNode(CEntity PrvKFNode)
        {
            return false;
        }

        public virtual bool GetNextKFNode(ref CEntity NextKFNode)
        {
            return false;
        }

        public virtual bool SetNextKFNode(CEntity NextKFNode)
        {
            return false;
        }

        public void setEditorModel(CModel modelToBeSet)
        {
            m_modelData = modelToBeSet;
            //m_material.Clear();
            //m_material.AddRange(modelToBeSet.GetMaterials(

        }

        //tells the editor if it should save this entity when saving the level or not. It is needed to not save
        //dummy entities or controllers.
        public virtual bool saveThisEntity()
        {
            return true; // save all entities by default. 
        }

        // Entity update for the editor.
        // Update is only required by the editor to be able to move the entity around the SPT.
        protected CSpatialTree m_entityLeafNode;

        public virtual void Update()
        {
            m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);

            // Update the obbs
            UpdateEntityOBB(m_modelData.GetOBB());

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            // The following two lines will only needed to be updated if the bounding box size changes or if the dynamic object moves(translation) in the scene.

            // This needs to be updated for the calcualtion that will take place in the below base update.
            UpdateCollisionSphere();

            // this needs to be called so that the spatial partioning tree is updated and the it won't just disappear from the scene.
            if( m_entityLeafNode != null )
            {
                m_entityLeafNode.MoveNodeAroundTree(m_position, m_maxLengthOfEntity);
            }
        }

        public void SetEntityLeafNode(CSpatialTree EntityLeafNode)
        {
            m_entityLeafNode = EntityLeafNode;
        }


        public virtual Vector3 GetEditorRotation()
        {
            //m_editorRotation = CToolbox.QuaternionToEuler(m_rotation);
            return m_editorRotation;
        }

        public virtual void SetEditorRotation2(Vector3 Rotation)//Quaternion Rotation) //added by yorrick for the editor.
        {
            m_editorRotation = Rotation;
            m_rotation = Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z);
            //editor_rotation = Rotation;
            //m_rotation = Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z);
            m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);
        }

        //public virtual void SetEditorRotation(Quaternion Rotation) //added by yorrick for the editor.
        //{
        //    m_rotation = Rotation * m_rotation;
        //    //editor_rotation = Rotation;
        //    //m_rotation = Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z);
        //    m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);
        //}

        //public virtual void SetEditorRotation(Vector3 Rotation) //added by yorrick for the editor.
        //{
        //    //if (Rotation.X >= MathHelper.Pi)
        //    //    Rotation.X = Rotation.X -MathHelper.TwoPi ;

        //    //if (Rotation.Y >= MathHelper.Pi)
        //    //    Rotation.Y = Rotation.Y -  MathHelper.TwoPi ;

        //    //if (Rotation.Z >= MathHelper.Pi)
        //    //    Rotation.Z =  Rotation.Z - MathHelper.TwoPi ;

        //    m_editorRotation = Rotation;
        //    m_rotation = Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z);
        //    //m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);
        //}

        public virtual void setPos(Vector3 newPos)//PLZ DONT USE THI METHOD THANK.S
        {
            m_position = newPos;
        }
#endif

        public int AssociationID
        {
            get { return m_associationID; }
        }

        public int DissociationID
        {
            get { return m_dissociationID; }
        }

        public void AssociateWith(int ExtraAssociateID)
        {
            m_associationID |= ExtraAssociateID;
            m_dissociationID &= ~ExtraAssociateID;
        }

        public void UndoAssociationWith(int UnRequiredAssociateID)
        {
            m_associationID &= ~UnRequiredAssociateID;
        }

        public void UndoDissociationWith(int UnRequiredDissociateID)
        {
            m_dissociationID &= ~UnRequiredDissociateID;
        }

        public void DissociateWith(int UnRequiredAssociateID)
        {
            m_associationID &= ~UnRequiredAssociateID;
            m_dissociationID |= UnRequiredAssociateID;
        }

        public bool IsAssociatedWith(int OtherAssociateID)
        {
            if ((m_associationID & OtherAssociateID) != 0)
            {
                return true;
            }

            return false;
        }

        public void SetEntityName(string EntityName)
        {
            m_entityName = EntityName;
        }

        public virtual void Reinitialise()
        {
            m_hidden = false;
        }

        public virtual void Hide()
        {
            m_hidden = true;
            m_isCollidable = false;
        }

        public virtual bool IsHidden()
        {
            return m_hidden;
        }

        public bool CanBeStoodOn()
        {
            return m_canBeStoodOn;
        }

        public bool SimpleCollision()
        {
            return m_simpleCollision;
        }

        public string GetEntityName()
        {
            return m_entityName;
        }

        public float GetCollisionDistance
        {
            get { return m_collisionDist; }
        }

        public bool ManualRelationship
        {
            get { return m_manualRelationship; }
            set { m_manualRelationship = value; }
        }

        public bool IsChild()
        {
            return m_isChild;
        }

        public BoundingSphere GetBoundingSphere()
        {
            return m_sphere;
        }

        public float GetBoundingSphereRadius()
        {
            return m_sphere.Radius;
        }

        public Matrix ParWrldMat
        {
            set { m_parWrldMat = value; }
        }

        public Quaternion ParRotation
        {
            set { m_parRotation = value; }
        }

        public Vector3 m_min
        {
            get { return m_bb.Min; }
        }

        public Vector3 m_max
        {
            get { return m_bb.Max; }
        }

        public BoundingBox GetBB()
        {
            return m_bb;
        }

        public bool IsPortalRoom()
        {
            return m_portalInfo.s_isRoom;
        }

        public uint GetPortalRoomID()
        {
            return m_portalInfo.s_roomID;
        }

        public void SetPortalRoomID(uint ID)
        {
            m_portalInfo.s_roomID = ID;
        }

        // Needs to be called as soon as the entity becomes a child of another entity.
        public virtual void SetAsChild(Quaternion ParRot, Matrix PWrldMat, CEntity Parent)
        {
            Matrix inv_par_wrld = Matrix.Invert(PWrldMat);
            m_parWrldMat = PWrldMat;
            m_parRotation = ParRot;
            m_position = Vector3.Transform(m_position, inv_par_wrld);
            m_rotation = Quaternion.Concatenate(m_rotation, Quaternion.Inverse(ParRot));

            m_isChild = true;
        }

        // Needs to be called when the entity stops becoming a child of an entity.
        public virtual void SetAsParent(Quaternion ParRot, Matrix PWrldMat)
        {
            m_parWrldMat = Matrix.Identity;
            m_parRotation = Quaternion.Identity;
            m_position = Vector3.Transform(m_position, PWrldMat);
            m_rotation = Quaternion.Concatenate(m_rotation, ParRot);

            m_isChild = false;
        }

        public virtual void SetUpdateTicket(int UpdateTicket)
        {
            // Shouldn't be called here.
            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CEntity", "ONDERZOEK ERROR: Error in CEntity - Cannot set UpdateTicket.");
        }

        public virtual int? GetSGID()
        {
            return null;
        }

        /// <summary>
        /// This is used by the collision engine to get the parent's local and rotation data.
        /// This has been overriden by CDynamicEntity.
        /// </summary>
        /// <param name="ParWorldMatrix">The parent's local matrix.</param>
        /// <param name="Rotation">The parent's local rotation</param>
        /// <returns>Returns true if the data has been set.</returns>
        public virtual bool GetParentDataForChild( ref Matrix ParWorldMatrix, ref Quaternion Rotation )
        {
            return false;
        }

        /// <summary>
        /// This is used by the collision engine to set the new height of the entity.
        /// You may wish to use it for other things but then you will have to find a
        /// way of distinguishing between a height from the collision engine and the height that you set.
        /// </summary>
        /// <param name="HitEntity">The other entity which is under this entity.</param>
        /// <param name="Height">The new height found by the collision engine.</param>
        public virtual void SetNewHeight( CEntity HitEntity, Vector3 Height )
        {
        }

        /// <summary>
        /// This returns the height (map) data.
        /// This has been overriden by CDynamicEntity.
        /// </summary>
        /// <param name="HMData">The entity's height (map) data.</param>
        /// <returns></returns>
        public virtual bool GetHeightMapData( out CTerrainEntity.SHeightMapData HMData )
        {
            HMData = new CTerrainEntity.SHeightMapData();
            return false;
        }

        /// <summary>
        /// You shouldn't need to touch this.
        /// Used internally during portal calculations.
        /// </summary>
        /// <returns>The entity's portal data.</returns>
        public SPortalInfo GetPortalInfo()
        {
            return m_portalInfo;
        }

        /// <summary>
        /// This function will be called when another entity has hit this entity.
        /// This function needs to be overriden if you wish to see a collision response, which you will ahve to code :P.
        /// </summary>
        /// <param name="Collider">The Entity which has hit this entity.</param>
        /// <param name="DistFromCollider">The distance from the collider.</param>
        /// <param name="NormalOfCollision">The normal/direction of the collision</param>
        public virtual void IsHit(CEntity Collider, float DistFromCollider, Vector3 NormalOfCollision)
        {
        }

        /// <summary>
        /// This function will be called when this entity has hit another entity.
        /// This function needs to be overriden if you wish to see a collision response, which you will ahve to code :P.
        /// </summary>
        /// <param name="HitEntity">The entity that this entity has hit.</param>
        /// <param name="DistFromCollider">The distance from the collider.</param>
        /// <param name="NormalOfCollision">The normal/direction of the collision</param>
        public virtual void HasHit(CEntity HitEntity, float DistToHitObject, Vector3 NormalOfCollision)
        {
        }

        public virtual void RenderForShadow(CCamera Camera)
        {
        }

        public virtual void Render(CCamera Camera)
        {
        }

        // This should only be called before or after calling begin render or end render functions, respectively, in CGameScreenHelper.
        public virtual void ForwardRender(CCamera Camera)
        {
        }

        // This should only be called before or after calling begin render or end render functions, respectively, in CGameScreenHelper.
        public virtual void ForwardTranslucentRender(CCamera Camera)
        {
        }

        public virtual Matrix GetWorldMatrix()
        {
            return m_worldMatrix;
        }

        public void SetWorldMatrix(Matrix worldMat)
        {
            m_worldMatrix = worldMat;
        }

        public List<CMaterial> GetMaterialList()
        {
            return m_material;
        }

        public virtual void SetMaterialList(List<CMaterial> MaterialList)
        {
            m_material = MaterialList;

            InitialiseMaterialEffects();

            if (m_modelData != null)
            {
                IsTranslucent(m_modelData.getBaseModel());
            }
        }

        public Matrix recalculateWorldMatrix() // yorricks function.... dont use...
        {
            m_worldMatrix = Matrix.CreateFromQuaternion(this.GetRotation()) * Matrix.CreateTranslation(m_position);

            return m_worldMatrix;
        }

        public void GetMinMax(out Vector3 Min, out Vector3 Max)
        {
            Min = m_min;
            Max = m_max;
        }

        public virtual bool CheckAgainstClipPlanes(Plane[] ClipPlanes)
        {
            return CToolbox.ClipWithPlanes(m_min, m_max, ClipPlanes);
        }

        public virtual bool CheckAgainstClipPlanes(List<Plane[]> ClipPlanes)
        {
            for (int i = 0; i < ClipPlanes.Count; ++i)
            {
                if (CToolbox.ClipWithPlanes(m_min, m_max, ClipPlanes[i]) == true)
                {
                    return true;
                }
            }
            return false;
        }

        public virtual bool CheckAgainstRay(Ray ShotRay, ref float Dist)
        {
            if (m_isCollidable == true)
            {
                return CCollision.RayVsAABB(ShotRay, m_min, m_max, ref Dist);
            }

            return false;
        }

        public virtual bool CheckChildBBAgainstRay(Ray ShotRay, ref float Dist)
        {
            float dist = float.MaxValue;
            bool found = false;

            Matrix rot = m_worldMatrix;
            rot.Translation = Vector3.Zero;

            if (m_isCollidable == true)
            {
                for (int i = 0; i < m_wrldOBB.Length; ++i)
                {
                    if (m_wrldOBB[i].m_child.Length != 0)
                    {
                        if (CheckChildBBAgainstRay(rot, m_wrldOBB[i].m_child, ShotRay, ref Dist) == true)
                        {
                            found = true;
                        }
                    }
                    else
                    {
                        if (CCollision.RayVsOOBB(ShotRay.Direction, ShotRay.Position, m_wrldOBB[i], m_worldMatrix, rot, ref dist) == true)
                        {
                            if (dist < Dist && dist != 0)
                            {
                                Dist = dist;
                            }

                            found = true;
                        }
                    }
                }
            }

            return found;
        }

        private bool CheckChildBBAgainstRay(Matrix WorldRot, CEntityOBB[] CurOBB, Ray ShotRay, ref float Dist)
        {
            float dist = float.MaxValue;
            bool found = false;

            for (int i = 0; i < CurOBB.Length; ++i)
            {
                if (CurOBB[i].m_child.Length != 0)
                {
                    if (CheckChildBBAgainstRay(WorldRot, CurOBB[i].m_child, ShotRay, ref Dist) == true)
                    {
                        found = true;
                    }
                }
                else
                {
                    if (CCollision.RayVsOOBB(ShotRay.Direction, ShotRay.Position, CurOBB[i], m_worldMatrix, WorldRot, ref dist) == true)
                    {
                        if (dist < Dist && dist != 0)
                        {
                            Dist = dist;
                        }

                        found = true;
                    }
                }
            }

            return found;
        }

        public virtual bool CheckRayVsSphere(Ray ShotRay)
        {
            if (m_isCollidable == true)
            {
                return CCollision.RayVsSphere(ShotRay.Direction, ShotRay.Position, m_sphere);
            }

            return false;
        }

        public Vector3 GetWorldPosition()
        {
            return m_worldMatrix.Translation;
        }

        public Vector3 GetPosition()
        {
            return m_position;
        }
		
		public void setPosX(float val)
        {
            m_position.X = val;
        }

        public void setPosY(float val)
        {
            m_position.Y = val;
        }

        public void setPosZ(float val)
        {
            m_position.Z = val;
        }

        public virtual void SetPosition(Vector3 Position)
        {
            m_position = Position;

            m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);
        }

        public Quaternion GetRotation()
        {
            return m_rotation;
        }

        public virtual Quaternion GetWorldRotation()
        {
            if (m_isChild == true)
            {
                return Quaternion.Concatenate(m_rotation, m_parRotation);
            }

            return m_rotation;
        }

        public void SetRotation(Quaternion Rotation)
        {
            m_rotation = Rotation;

#if EDITOR
            m_editorRotation = CToolbox.QuaternionToEuler(m_rotation);
#endif
        }

        public virtual CEntityOBB[] GetWorldOBB()
        {
            return m_wrldOBB;
        }

        public virtual Vector3[] GetWorldOBBIndex0()
        {
            return m_wrldOBB[0].m_wrldOBB;
        }

        public void SetModel(CModel Model)
        {
            m_modelData = Model;
        }

        public CModel GetModel()
        {
            return m_modelData;
        }

        public virtual void SetRender(bool Render, bool AtStartOfFrame)
        {
            m_render = Render;
        }

        public virtual bool GetRender()
        {
            return m_render;
        }

        public bool IsDynamicEntity()
        {
            return m_isDynamic;
        }

        public Plane[] GetFirstPlanes()
        {
            return m_wrldOBB[0].m_planes;
        }

        public bool IsTerrain()
        {
            return m_isTerrain;
        }

        public bool IsSkinnedAnimation()
        {
            return m_isSkinnedAnimation;
        }

        public bool IsCollidable()
        {
            return m_isCollidable;
        }

        public bool CanBeParent
        {
            get { return m_canBeParent; }
            set { m_canBeParent = value; }
        }

        public bool IsWholeTranslucent()
        {
            return m_wholeTranslucent;
        }

        public bool IsPartialTranslucent()
        {
            return m_partTranslucent;
        }

        protected void InitialiseMaterialEffects()
        {
            //Initalising the material data too
            if (m_material != null)
            {
                for (int j = 0; j < m_material.Count; ++j)
                {
                    m_material[j].m_effect = CVisual.Instance.GetShaderID(m_material[j]);
                }
            }
        }

        protected void InitialiseEntityOBBOnly(List<CModelOBB> OBB)
        {
            Matrix temp_wm = Matrix.CreateFromQuaternion(m_rotation);

            m_wrldOBB = new CEntityOBB[OBB.Count];

            for (int i = 0; i < OBB.Count; ++i)
            {
                m_wrldOBB[i] = new CEntityOBB();
                m_wrldOBB[i].InitialiseEntityOBB(OBB[i], m_worldMatrix, temp_wm);

                // If this check fails then that means during collision checks, ray vs AABB will be used.
                if (m_rotation != new Quaternion(0, 0, 0, 0))
                {
                    if (m_rotation != Quaternion.Identity)
                    {
                        m_wrldOBB[i].m_isAABB = false;
                    }
                }
            }
        }

        protected void InitialiseEntityOBB( List<CModelOBB> OBB )
        {
            InitialiseMaterialEffects();

            InitialiseEntityOBBOnly(OBB);
        }

        protected void UpdateEntityOBB(List<CModelOBB> OBB)
        {
            for (int i = 0; i < OBB.Count; ++i)
            {
                m_wrldOBB[i].UpdateEntityOBB(OBB[i], m_worldMatrix, m_rotation);
            }
        }

        protected virtual void UpdateCollisionSphere()
        {
            m_maxLengthOfEntity = Vector3.Distance(new Vector3(m_max.X, m_min.Y, m_max.Z), m_min);

            // Updating the collision sphere.
            m_sphere.Radius = m_maxLengthOfEntity / 2;
            m_sphere.Center = (m_max + m_min) / 2.0f;//GetWorldPosition();

            m_collisionDist = m_sphere.Radius / 3.0f;
        }

        protected void UpdatePlanes(Quaternion WorldNoPos, int OBBI)
        {
            m_wrldOBB[OBBI].UpdatePlanes(WorldNoPos);
        }

        protected void IsTranslucent( Model ModelData )
        {
            m_partTranslucent = false;
            m_wholeTranslucent = false;
            if (m_translucentParts != null)
            {
                m_translucentParts.Clear();
                m_translucentParts = null;
            }
            m_translucentParts = new Dictionary<int, List<SPartAndMatNum>>();

            if (m_opaqueParts != null)
            {
                m_opaqueParts.Clear();
                m_opaqueParts = null;
            }
            m_opaqueParts = new Dictionary<int, List<SPartAndMatNum>>();

            bool opaque = false;
            bool translucent = false;

            int i = 0;
            int count_mesh = 0;
            foreach (ModelMesh mesh in ModelData.Meshes)
            {
                int count_part = 0;
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    if (m_material[i].m_diffuse.W < 1.0f)
                    {
                        if (m_translucentParts.ContainsKey(count_mesh) == false)
                        {
                            m_translucentParts.Add(count_mesh, new List<SPartAndMatNum>());
                        }
                        m_translucentParts[count_mesh].Add(new SPartAndMatNum(count_part, i));
                        translucent = true;
                    }
                    else
                    {
                        if (m_opaqueParts.ContainsKey(count_mesh) == false)
                        {
                            m_opaqueParts.Add(count_mesh, new List<SPartAndMatNum>());
                        }
                        m_opaqueParts[count_mesh].Add(new SPartAndMatNum(count_part, i));
                        opaque = true;
                    }

                    ++count_part;
                    ++i;
                }

                ++count_mesh;
            }

            if (opaque == true && translucent == true)
            {
                m_partTranslucent = true;
            }
            else if (translucent == true)
            {
                m_wholeTranslucent = true;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
#if EDITOR
using Microsoft.Xna.Framework.Graphics;
#endif

using Onderzoek.Visual;
using Onderzoek.ModelData;
using Onderzoek.Camera;

namespace Onderzoek.Entity
{
    public class CSpotLightEntity : CStaticEntity
    {
        protected CVisual.SSpotLight m_spotLight;
        protected bool m_shadowCaster = false;
        public List<int> m_renderList;

        public override void Dispose()
        {
            CVisual.Instance.DeleteShadowRenderTarget(m_spotLight.s_shadowRTIndex);
            m_spotLight.s_model = null;
            m_spotLight.s_lightCamera = null;

            base.Dispose();
        }

#if EDITOR
        Onderzoek.Screens.CGameScreenHelper m_screen;
        int m_numOfSplits = 3;
        Matrix m_scdWorldMatrix;
        List<CModelOBB> m_originalOBB;
        CCamera m_mainCamera;
        Vector3 m_pivotPosition;

        public CSpotLightEntity(Vector3 Position, Vector3 Interest, Vector3 Rotation, string EntityName, float Range, float ConeAngle, bool ShadowCaster,
            Vector3 Colour, float Intensity, List<CModelOBB> OBB, bool IsCollidable, CModel ModelData, List<CMaterial> MaterialList)
        {
#if EDITOR
            m_editorRotation = Rotation;
#endif

            Initialise(Position, Interest, Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z), EntityName, Range, ConeAngle, ShadowCaster, Colour,
                Intensity, OBB, IsCollidable, ModelData, MaterialList);
        }

        public void Initialise(Vector3 Position, Vector3 Interest, Quaternion Rotation, string EntityName, float Range, float ConeAngle, bool ShadowCaster,
            Vector3 Colour, float Intensity, List<CModelOBB> OBB, bool IsCollidable, CModel ModelData, List<CMaterial> MaterialList)
        {
            m_originalOBB = OBB;
            m_scdWorldMatrix = Matrix.Identity;
            m_modelName = ModelData.GetModelName();
            m_isScalingAllowed = true;
            m_material = MaterialList;

            m_modelData = ModelData;

            foreach (CMaterial mat in m_material)
            {
                mat.m_emissive = mat.m_diffuse * 0.25f;
                mat.m_diffuse.W = 0.75f;
            }
            IsTranslucent(m_modelData.getBaseModel());

            m_pivotPosition = Position + new Vector3(0, 20, 0);

            IsTranslucent(m_modelData.getBaseModel());
            InitialiseMaterialEffects();
#else
        public CSpotLightEntity(Vector3 Position, Vector3 Interest, Vector3 Rotation, string EntityName, float Range, float ConeAngle, bool ShadowCaster,
            Vector3 Colour, float Intensity, List<CModelOBB> OBB, bool IsCollidable)
        {
            Initialise(Position, Interest, Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z), EntityName, Range, ConeAngle, ShadowCaster, Colour,
                Intensity, OBB, IsCollidable);
        }

        public void Initialise(Vector3 Position, Vector3 Interest, Quaternion Rotation, string EntityName, float Range, float ConeAngle, bool ShadowCaster,
            Vector3 Colour, float Intensity, List<CModelOBB> OBB, bool IsCollidable)
        {
#endif
            m_portalInfo = new SPortalInfo();
            m_spotLight.s_colour = Colour;
            m_spotLight.s_intensity = Intensity;
            m_spotLight.s_position = Position;
            m_spotLight.s_range = Range;
            m_spotLight.s_coneAngle = ConeAngle;
            m_spotLight.s_interest = Interest;
            m_spotLight.s_specularIntensity = 1.0f;

#if ENGINE
            // Tell CVisual to Initialise a rendertarget for this light
            if (ShadowCaster == true)
            {
                m_shadowCaster = ShadowCaster;

                m_spotLight.s_lightCamera = new CFPCamera(CVisual.Instance.GetAspectRatio(), ConeAngle, 1.0f, Range, Position, Vector3.Zero);
                m_spotLight.s_lightCamera.SetRotationMatrix(CToolbox.ConvertDirIntoRotation(Position, Interest));
                m_spotLight.s_lightCamera.SetRotation( Quaternion.CreateFromRotationMatrix(m_spotLight.s_lightCamera.GetRotationMatrix()) );
                m_spotLight.s_lightCamera.Refresh();
                m_spotLight.s_shadowTerm = 0.3f;

                m_renderList = new List<int>();
                m_spotLight.s_shadowRTIndex = CVisual.Instance.CreateRenderTargetForShadows(true);
            }

            m_material = null;
#endif

            m_entityName = EntityName;

            m_isDynamic = false;
            m_position = Position;
            m_isCollidable = IsCollidable;

            m_worldMatrix = Matrix.CreateFromQuaternion(Rotation) * Matrix.CreateTranslation(m_position);

            // Transform local space OBB corners to world space
            InitialiseEntityOBB(OBB);

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

#if EDITOR
            FillPropertyList();
#endif
        }

        public override void SetPosition(Vector3 Position)
        {
            m_spotLight.s_position = Position;

            base.SetPosition(Position);
        }

        public void SetInterest(Vector3 Interest)
        {
            m_spotLight.s_interest = Interest;
        }

        public override void SetRender(bool Render, bool AtStartOfFrame)
        {
            // This check is to prevent uneccessary tests against clip plane.
            // Only occurs when the SPT list is being calcualted for the GI camera view.
            if (AtStartOfFrame == true)
            {
                m_render = Render;
            }
            else
            {
                m_render = true;
            }
        }

        public int GetRTIndex()
        {
            return m_spotLight.s_shadowRTIndex;
        }

        public CCamera GetCamera()
        {
            return m_spotLight.s_lightCamera;
        }

        public override void Render(CCamera Camera)
        {
#if ENGINE
            CVisual.Instance.AddSpotLightToList(m_spotLight);

            if (m_shadowCaster == true)
            {
                CVisual.Instance.AddSpotLightToShadowList(m_spotLight);
            }
#endif
        }

        public bool IsShadowCaster()
        {
            return m_shadowCaster;
        }

        public override int GetEntityID()
        {
            return (int)EBasicEntityIDs.SpotLight;
            //return base.GetEntityID();
        }

#if EDITOR
        public override List<CEntityProperty> PropertyList
        {
            get
            {
                return base.PropertyList;
            }
        }

        public override void Update()
        {
            Vector3 direction = CToolbox.UnitVector(m_spotLight.s_interest - m_spotLight.s_position) * 5.0f;
            CVisual.Instance.AddToLineList(m_spotLight.s_position, m_spotLight.s_interest - direction, new Color(255, 50, 75));
            CVisual.Instance.AddToLineList(m_spotLight.s_interest - direction, m_spotLight.s_interest, new Color(50, 255, 100));

            m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);

            // Update the obbs
            UpdateEntityOBB(m_originalOBB);

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            // The following two lines will only needed to be updated if the bounding box size changes or if the dynamic object moves(translation) in the scene.

            // This needs to be updated for the calcualtion that will take place in the below base update.
            UpdateCollisionSphere();

            // this needs to be called so that the spatial partioning tree is updated and the it won't just disappear from the scene.
            m_entityLeafNode.MoveNodeAroundTree(m_position, m_maxLengthOfEntity);

            m_scdWorldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_pivotPosition);

            m_spotLight.s_position = m_pivotPosition;
            m_spotLight.s_interest = m_position;

            m_spotLight.s_range = CToolbox.MagnitudeOfVector(m_spotLight.s_position - m_spotLight.s_interest);
            m_propertyList[3].propertyValue = m_spotLight.s_range.ToString();
        }

        public override void ForwardTranslucentRender(CCamera Camera)
        {
            CVisual.Instance.AddSpotLightToList(m_spotLight);

            CVisual.Instance.DrawBounds(Camera, m_wrldOBB);

            if (m_modelData != null)
            {
                CVisual.Instance.ForwardRender(m_modelData.getBaseModel(), m_material, m_scdWorldMatrix, m_translucentParts);
                CVisual.Instance.ForwardRender(m_modelData.getBaseModel(), m_material, m_worldMatrix, m_translucentParts);
            }
        }

        public override Vector3 Scale
        {
            get
            {
                float val = 0;
                for (int i = 0; i < m_originalOBB[0].m_OBB.Length; ++i)
                {
                    if (m_originalOBB[0].m_OBB[i].X > val)
                    {
                        val = m_originalOBB[0].m_OBB[i].X;
                    }
                }

                return new Vector3(val, val, val);
            }
            set
            {
                m_originalOBB = CModel.CreateOBBofUnitSize(value.X);
            }
        }

        public override void writeEntityProperties(XmlWriter writer)
        {
            for (int i = 0; i < m_propertyList.Count; i++)
            {
                writer.WriteElementString(m_propertyList[i].propertyName, m_propertyList[i].propertyValue);
            }

            float val = 0;
            for (int i = 0; i < m_originalOBB[0].m_OBB.Length; ++i)
            {
                if (m_originalOBB[0].m_OBB[i].X > val)
                {
                    val = m_originalOBB[0].m_OBB[i].X;
                }
            }

            writer.WriteElementString("Scale", val.ToString());
        }

        public override void readEntityProperties(XmlReader reader)
        {
            reader.ReadToFollowing("EntityData");
            while (!(reader.Name == "EntityData" && reader.NodeType == XmlNodeType.EndElement))
            {
                reader.Read();
                reader.Read();

                reader.MoveToContent();
                string propName = reader.Name;

                if (reader.Name == "EntityData" && reader.NodeType == XmlNodeType.EndElement)
                    break;
                string propVal;

                if (reader.IsEmptyElement)
                    propVal = "";
                else
                {
                    reader.Read();
                }

                propVal = reader.Value;

                if (propName.Equals("Scale"))
                {
                    float flt = 1.0f;
                    CToolbox.StringToFloat(propVal, ref flt);

                    m_originalOBB = CModel.CreateOBBofUnitSize(flt);
                }
                else
                {
                    for (int i = 0; i < m_propertyList.Count; i++)
                    {
                        if (m_propertyList[i].propertyName.Equals(propName))
                        {
                            if(m_propertyList[i].propertyName.Equals("Pivot_Position"))
                            {
                                CToolbox.StringToVector3(propVal, ref m_pivotPosition);
                                m_spotLight.s_position = m_pivotPosition;
                                m_spotLight.s_interest = m_position;
                            }

                            m_propertyList[i].propertyValue = propVal;
                            break;
                        }
                    }
                }
            }
        }

        public override void FillPropertyList()
        {
            this.m_propertyList = new List<CEntityProperty>();

            string temp = m_modelName.Replace("Model/", "");
            temp = temp.Replace("Content/", "");
            temp = temp.Replace("Content\\\\", "");
            temp = temp.Replace("Content\\", "");
            temp = temp.Replace("Model\\\\", "");
            temp = temp.Replace("Model\\", "");
            temp = temp.Replace("..\\", "");
            m_modelName = temp;

            m_propertyList.Add(new CEntityProperty("Shadow_Caster", m_shadowCaster.ToString(), EPropertyGridInputType.PullDownMultipleChoice));
            m_propertyList[m_propertyList.Count - 1].pullDownOptions = new List<string>();
            m_propertyList[m_propertyList.Count - 1].pullDownOptions.Add("True");
            m_propertyList[m_propertyList.Count - 1].pullDownOptions.Add("False");

            string col = (m_spotLight.s_colour.X * 255.0f).ToString() + " " + (m_spotLight.s_colour.Y * 255.0f).ToString()
                     + " " + (m_spotLight.s_colour.Z * 255.0f).ToString();
            m_propertyList.Add(new CEntityProperty("Light_Colour", col, EPropertyGridInputType.ColourDialogue));
            m_propertyList.Add(new CEntityProperty("Intensity", m_spotLight.s_intensity.ToString(), EPropertyGridInputType.TextBox));

            m_spotLight.s_range = CToolbox.MagnitudeOfVector(m_spotLight.s_position - m_spotLight.s_interest);
            m_propertyList.Add(new CEntityProperty("Range", m_spotLight.s_range.ToString(), EPropertyGridInputType.TextBox));
            m_propertyList.Add(new CEntityProperty("Cone_Angle_in_degrees", m_spotLight.s_coneAngle.ToString(), EPropertyGridInputType.TextBox));

            m_propertyList.Add(new CEntityProperty("Collidable", m_isCollidable.ToString(), EPropertyGridInputType.PullDownMultipleChoice));
            m_propertyList[m_propertyList.Count - 1].pullDownOptions = new List<string>();
            m_propertyList[m_propertyList.Count - 1].pullDownOptions.Add("True");
            m_propertyList[m_propertyList.Count - 1].pullDownOptions.Add("False");

            m_propertyList.Add(new CEntityProperty("Pivot_Position", m_pivotPosition.ToString(), EPropertyGridInputType.TextBox));
        }

        public override void UpdateFromPropertyList()
        {
            bool bl = false;
            if (CToolbox.StringToBool(m_propertyList[0].propertyValue, ref bl) == false)
            {
                m_propertyList[0].propertyValue = m_shadowCaster.ToString();
            }
            else
            {
                m_shadowCaster = bl;
            }

            Color col = Color.White;
            if (CToolbox.StringToColour(m_propertyList[1].propertyValue, ref col) == false)
            {
                m_propertyList[1].propertyValue = (m_spotLight.s_colour.X * 255.0f).ToString() + " " + (m_spotLight.s_colour.Y * 255.0f).ToString()
                     + " " + (m_spotLight.s_colour.Z * 255.0f).ToString();
            }
            else
            {
                m_spotLight.s_colour = new Vector3((float)col.R / 255.0f, (float)col.G / 255.0f, (float)col.B / 255.0f);
            }

            float flt = 1;
            if (CToolbox.StringToFloat(m_propertyList[2].propertyValue, ref flt) == false)
            {
                m_propertyList[2].propertyValue = m_spotLight.s_intensity.ToString();
            }
            else
            {
                if (flt < 0)
                {
                    flt = 0;
                }

                m_spotLight.s_intensity = flt;
            }

            flt = 1;
            if (CToolbox.StringToFloat(m_propertyList[3].propertyValue, ref flt) == false)
            {
                m_propertyList[3].propertyValue = m_spotLight.s_range.ToString();
            }
            else
            {
                if (flt < 0)
                {
                    flt = 0;
                }

                m_spotLight.s_range = flt;

                Vector3 direction = CToolbox.UnitVector(m_spotLight.s_interest - m_spotLight.s_position);
                m_spotLight.s_position = m_pivotPosition;
                m_position = m_pivotPosition + (direction * m_spotLight.s_range);
                m_spotLight.s_interest = m_position;
            }

            flt = 1;
            if (CToolbox.StringToFloat(m_propertyList[4].propertyValue, ref flt) == false)
            {
                m_propertyList[4].propertyValue = m_spotLight.s_coneAngle.ToString();
            }
            else
            {
                m_spotLight.s_coneAngle = flt;
            }

            bl = false;
            if (CToolbox.StringToBool(m_propertyList[5].propertyValue, ref bl) == false)
            {
                m_propertyList[5].propertyValue = m_isCollidable.ToString();
            }
            else
            {
                m_isCollidable = bl;
            }

            Vector3 vec3 = Vector3.Zero;
            if (CToolbox.StringToVector3(m_propertyList[6].propertyValue, ref vec3) == false)
            {
                m_propertyList[6].propertyValue = m_pivotPosition.ToString();
            }
            else
            {
                m_pivotPosition = vec3;
            }
        }
#else
        public static void LoadEntityData(CEntityDataStore EntityData, Onderzoek.Screens.CGameScreenHelper CurScreen,
            ref Dictionary<string, CModel.SModelData> ModelList, ref ContentManager Content)
        {
            bool shadow_caster = false;
            int num_splits = 3;
            Vector3 col = Vector3.One;
            float intensity = 1.0f;
            float range = 10.0f;
            float cone_angle = 45.0f;
            bool collidable = false;
            Vector3 interest = Vector3.Zero;
            float scale = 1.0f;

            for (int i = 0; i < EntityData.m_properties.Count; ++i)
            {
                if (EntityData.m_properties[i].propertyName == "Shadow_Caster")
                {
                    CToolbox.StringToBool(EntityData.m_properties[i].propertyValue, ref shadow_caster);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Num_Splits_for_shadows")
                {
                    CToolbox.StringToInt(EntityData.m_properties[i].propertyValue, ref num_splits);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Light_Colour")
                {
                    Microsoft.Xna.Framework.Graphics.Color colour = Microsoft.Xna.Framework.Graphics.Color.White;
                    CToolbox.StringToColour(EntityData.m_properties[i].propertyValue, ref colour);
                    col = new Vector3((float)colour.R / 255.0f, (float)colour.G / 255.0f, (float)colour.B / 255.0f);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Intensity")
                {
                    CToolbox.StringToFloat(EntityData.m_properties[i].propertyValue, ref intensity);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Range")
                {
                    CToolbox.StringToFloat(EntityData.m_properties[i].propertyValue, ref range);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Cone_Angle_in_degrees")
                {
                    CToolbox.StringToFloat(EntityData.m_properties[i].propertyValue, ref cone_angle);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Collidable")
                {
                    CToolbox.StringToBool(EntityData.m_properties[i].propertyValue, ref collidable);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Pivot_Position")
                {
                    CToolbox.StringToVector3(EntityData.m_properties[i].propertyValue, ref interest);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Scale")
                {
                    CToolbox.StringToFloat(EntityData.m_properties[i].propertyValue, ref scale);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
            }

            // Create the model
            CSpotLightEntity ent = new CSpotLightEntity(interest, EntityData.m_position, EntityData.m_rotation, EntityData.m_entityName, range, cone_angle,
                shadow_caster, col, intensity, CModel.CreateOBBofUnitSize(scale), collidable);
            CurScreen.AddEntityNoSPT(ent);
        }
#endif
    }
}

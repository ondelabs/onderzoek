﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
#if EDITOR
using Microsoft.Xna.Framework.Graphics;
#endif

using Onderzoek.Visual;
using Onderzoek.ModelData;
using Onderzoek.Camera;

namespace Onderzoek.Entity
{
    public class CPointLightEntity : CStaticEntity
    {
#if EDITOR
        List<CModelOBB> m_originalOBB;
#endif

        protected CVisual.SPointLight m_pointLight;

        public override void Dispose()
        {
            base.Dispose();
        }

#if EDITOR
        public CPointLightEntity(Vector3 Position, Vector3 Rotation, string EntityName, float Radius, Vector3 Colour, float Intensity, List<CModelOBB> OBB,
            bool IsCollidable, CModel Model, List<CMaterial> MaterialList)
        {
#if EDITOR
            m_editorRotation = Rotation;
#endif

            Initialise(Position, Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z), EntityName, Radius, Colour, Intensity, OBB,
                IsCollidable, Model, MaterialList);
        }

        public void Initialise(Vector3 Position, Quaternion Rotation, string EntityName, float Radius, Vector3 Colour, float Intensity, List<CModelOBB> OBB,
            bool IsCollidable, CModel Model, List<CMaterial> MaterialList)
        {
            m_originalOBB = OBB;
            m_isScalingAllowed = true;

            m_modelData = Model;
            m_material = MaterialList;

            m_modelName = "point_light_ph";
            foreach (CMaterial mat in m_material)
            {
                mat.m_emissive = mat.m_diffuse * 0.25f;
                mat.m_diffuse.W = 0.75f;
            }
            IsTranslucent(m_modelData.getBaseModel());
#else
        public CPointLightEntity(Vector3 Position, Vector3 Rotation, string EntityName, float Radius, Vector3 Colour, float Intensity, List<CModelOBB> OBB, bool IsCollidable)
        {
            Initialise(Position, Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z), EntityName, Radius, Colour, Intensity, OBB, IsCollidable);
        }

        public void Initialise(Vector3 Position, Quaternion Rotation, string EntityName, float Radius, Vector3 Colour, float Intensity, List<CModelOBB> OBB, bool IsCollidable)
        {
#endif
            m_portalInfo = new SPortalInfo();
            m_pointLight.s_colour = Colour;
            m_pointLight.s_intensity = Intensity;
            m_pointLight.s_position = Position;
            m_pointLight.s_radius = Radius;
            m_pointLight.s_specularIntensity = 1.0f;

#if ENGINE
            m_material = null;
#endif

            m_entityName = EntityName;
            m_isDynamic = false;
            m_position = Position;
            m_isCollidable = IsCollidable;

            m_worldMatrix = Matrix.CreateFromQuaternion(Rotation) * Matrix.CreateTranslation(m_position);

            // Transform local space OBB corners to world space
            InitialiseEntityOBB(OBB);

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

#if EDITOR
            FillPropertyList();
#endif
        }

        public override void SetPosition(Vector3 Position)
        {
            m_pointLight.s_position = Position;

            base.SetPosition(Position);
        }

        public CVisual.SPointLight PointLightData
        {
            get { return m_pointLight; }
            set { m_pointLight = value; }
        }

        public void SetIntensity(float Intensity)
        {
            m_pointLight.s_intensity = Intensity;
        }

        public override void SetRender(bool Render, bool AtStartOfFrame)
        {
            // This check is to prevent uneccessary tests against clip plane.
            // Only occurs when the SPT list is being calcualted for the GI camera view.
            if (AtStartOfFrame == true)
            {
                m_render = Render;
            }
            else
            {
                m_render = true;
            }
        }

        public override void Render(CCamera Camera)
        {
#if ENGINE
            CVisual.Instance.AddPointLightToList(m_pointLight);
#endif
        }

        public override int GetEntityID()
        {
            return (int)EBasicEntityIDs.PointLight;
        }

#if EDITOR
        public override Vector3 Scale
        {
            get
            {
                float val = 0;
                for (int i = 0; i < m_originalOBB[0].m_OBB.Length; ++i)
                {
                    if (m_originalOBB[0].m_OBB[i].X > val)
                    {
                        val = m_originalOBB[0].m_OBB[i].X;
                    }
                }

                return new Vector3(val, val, val);
            }
            set
            {
                m_originalOBB = CModel.CreateOBBofUnitSize(value.X);
            }
        }

        public override void SetMaterialList(List<CMaterial> MaterialList)
        {
            base.SetMaterialList(MaterialList);

            if (m_material != null)
            {
                for (int j = 0; j < m_material.Count; ++j)
                {
                    m_material[j].m_effect = CVisual.Instance.GetShaderID(m_material[j]);
                }
            }

            IsTranslucent(m_modelData.getBaseModel());
        }

        public override void writeEntityProperties(XmlWriter writer)
        {
            for (int i = 0; i < m_propertyList.Count; i++)
            {
                writer.WriteElementString(m_propertyList[i].propertyName, m_propertyList[i].propertyValue);
            }

            float val = 0;
            for (int i = 0; i < m_originalOBB[0].m_OBB.Length; ++i)
            {
                if (m_originalOBB[0].m_OBB[i].X > val)
                {
                    val = m_originalOBB[0].m_OBB[i].X;
                }
            }

            writer.WriteElementString("Scale", val.ToString());
        }

        public override void readEntityProperties(XmlReader reader)
        {
            reader.ReadToFollowing("EntityData");
            while (!(reader.Name == "EntityData" && reader.NodeType == XmlNodeType.EndElement))
            {
                reader.Read();
                reader.Read();

                reader.MoveToContent();
                string propName = reader.Name;

                if (reader.Name == "EntityData" && reader.NodeType == XmlNodeType.EndElement)
                    break;
                string propVal;

                if (reader.IsEmptyElement)
                    propVal = "";
                else
                {
                    reader.Read();
                }

                propVal = reader.Value;

                if (propName.Equals("Scale"))
                {
                    float flt = 1.0f;
                    CToolbox.StringToFloat(propVal, ref flt);

                    m_originalOBB = CModel.CreateOBBofUnitSize(flt);
                }
                else
                {
                    for (int i = 0; i < m_propertyList.Count; i++)
                    {
                        if (m_propertyList[i].propertyName.Equals(propName))
                            m_propertyList[i].propertyValue = propVal;
                    }
                }
            }
        }

        public override void FillPropertyList()
        {
            this.m_propertyList = new List<CEntityProperty>();

            m_propertyList.Add(new CEntityProperty("Radius", m_pointLight.s_radius.ToString(), EPropertyGridInputType.TextBox));

            string col = (m_pointLight.s_colour.X * 255.0f).ToString() + " " + (m_pointLight.s_colour.Y * 255.0f).ToString()
                     + " " + (m_pointLight.s_colour.Z * 255.0f).ToString();
            m_propertyList.Add(new CEntityProperty("Light_Colour", col, EPropertyGridInputType.ColourDialogue));
            m_propertyList.Add(new CEntityProperty("Intensity", m_pointLight.s_intensity.ToString(), EPropertyGridInputType.TextBox));
            m_propertyList.Add(new CEntityProperty("Collidable", m_isCollidable.ToString(), EPropertyGridInputType.PullDownMultipleChoice));
            m_propertyList[m_propertyList.Count - 1].pullDownOptions = new List<string>();
            m_propertyList[m_propertyList.Count - 1].pullDownOptions.Add("True");
            m_propertyList[m_propertyList.Count - 1].pullDownOptions.Add("False");

            //base.FillPropertyList();
        }

        public override void UpdateFromPropertyList()
        {
            float flt = 0;
            if (CToolbox.StringToFloat(m_propertyList[0].propertyValue, ref flt) == false)
            {
                m_propertyList[0].propertyValue = m_pointLight.s_radius.ToString();
            }
            else
            {
                m_pointLight.s_radius = flt;
            }

            Color col = Color.White;
            if (CToolbox.StringToColour(m_propertyList[1].propertyValue, ref col) == false)
            {
                m_propertyList[1].propertyValue = (m_pointLight.s_colour.X * 255.0f).ToString() + " " + (m_pointLight.s_colour.Y * 255.0f).ToString()
                     + " " + (m_pointLight.s_colour.Z * 255.0f).ToString();
            }
            else
            {
                m_pointLight.s_colour = new Vector3((float)col.R / 255.0f, (float)col.G / 255.0f, (float)col.B / 255.0f);
            }

            flt = 0;
            if (CToolbox.StringToFloat(m_propertyList[2].propertyValue, ref flt) == false)
            {
                m_propertyList[2].propertyValue = m_pointLight.s_intensity.ToString();
            }
            else
            {
                m_pointLight.s_intensity = flt;
            }

            bool bl = false;
            if (CToolbox.StringToBool(m_propertyList[3].propertyValue, ref bl) == false)
            {
                m_propertyList[3].propertyValue = m_isCollidable.ToString();
            }
            else
            {
                m_isCollidable = bl;
            }
        }

        public override void Update()
        {
            m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);

            // Update the obbs
            UpdateEntityOBB(m_originalOBB);

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            // The following two lines will only needed to be updated if the bounding box size changes or if the dynamic object moves(translation) in the scene.

            // This needs to be updated for the calcualtion that will take place in the below base update.
            UpdateCollisionSphere();

            // this needs to be called so that the spatial partioning tree is updated and the it won't just disappear from the scene.
            m_entityLeafNode.MoveNodeAroundTree(m_position, m_maxLengthOfEntity);
        }

        public override void ForwardTranslucentRender(Onderzoek.Camera.CCamera Camera)
        {
            CVisual.Instance.AddPointLightToList(m_pointLight);

            CVisual.Instance.DrawBounds(Camera, m_wrldOBB);

            CVisual.Instance.ForwardRender(m_modelData.getBaseModel(), m_material, m_worldMatrix, m_translucentParts);
        }
#else
        public static void LoadEntityData(CEntityDataStore EntityData, Onderzoek.Screens.CGameScreenHelper CurScreen,
            ref Dictionary<string, CModel.SModelData> ModelList, ref ContentManager Content)
        {
            float radius = 10.0f;
            Vector3 col = Vector3.One;
            float intensity = 1.0f;
            bool collidable = false;
            float scale = 1.0f;

            for (int i = 0; i < EntityData.m_properties.Count; ++i)
            {
                if (EntityData.m_properties[i].propertyName == "Radius")
                {
                    CToolbox.StringToFloat(EntityData.m_properties[i].propertyValue, ref radius);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Light_Colour")
                {
                    Microsoft.Xna.Framework.Graphics.Color colour = Microsoft.Xna.Framework.Graphics.Color.White;
                    CToolbox.StringToColour(EntityData.m_properties[i].propertyValue, ref colour);
                    col = new Vector3((float)colour.R / 255.0f, (float)colour.G / 255.0f, (float)colour.B / 255.0f);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Intensity")
                {
                    CToolbox.StringToFloat(EntityData.m_properties[i].propertyValue, ref intensity);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Collidable")
                {
                    CToolbox.StringToBool(EntityData.m_properties[i].propertyValue, ref collidable);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Scale")
                {
                    CToolbox.StringToFloat(EntityData.m_properties[i].propertyValue, ref scale);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
            }

            // Create the model
            CPointLightEntity ent = new CPointLightEntity(EntityData.m_position, EntityData.m_rotation, EntityData.m_entityName, radius,
                col, intensity, CModel.CreateOBBofUnitSize(scale), collidable);
            CurScreen.AddEntityNoSPT(ent);
        }
#endif
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Graphics.PackedVector;
using Microsoft.Xna.Framework.Content;

using Onderzoek.Visual;
using Onderzoek.ModelData;
using Onderzoek.Physics;
using Onderzoek.Camera;

namespace Onderzoek.Entity
{
    public class CNavQuadEntity : CDynamicEntity
    {
        Vector3 m_scale;
        public CDotXEntity m_Corner1, m_Corner2, m_Corner3, m_Corner4;

        public Vector3 m_lastLocation;

        public List<CNavQuadEntity> m_linkedToList;

        CNavQuadModel m_model;

        //public List<CDotXBoxEntity> m_childList; //list of children of this xbox entity in the editor. is #if editor necessary for this? No, only being used by editor so it's ok.

        public override void Dispose()
        {
            //if (m_childList != null)
            //{
            //    foreach (CDotXBoxEntity bb in m_childList)
            //    {
            //        bb.Dispose();
            //    }

            //    m_childList.Clear();
            //    m_childList = null;
            //}

            base.Dispose();
        }

        public CNavQuadEntity(Vector3 Position, Vector3 Rotation, string EntityName, CNavQuadModel Model, List<CMaterial> Material, bool IsCollidable)
        {
#if EDITOR
            m_propertyList = new List<CEntityProperty>();
            m_editorRotation = Rotation;
#endif

            m_model = Model;
            m_lastLocation = Position;
            Initialise(Position, Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z), EntityName, Model, Material, IsCollidable);
        }

        public void SetShaderID()
        {

            if (m_material != null)
            {
                for (int j = 0; j < m_material.Count; ++j)
                {
                    m_material[j].m_effect = CVisual.Instance.GetShaderID(m_material[j]);
                }
            }
        }

        public bool hasPossibleLinkWithThisSector(CNavQuadEntity otherEnt)
        {
            int firstIndex = -1, secondIndex = -1;
            float minDistSoFar = 999999;


            Vector3[] cornerPoints1 = new Vector3[4];
            Vector3[] cornerPoints2 = new Vector3[4];

            cornerPoints1[0] = m_Corner1.GetPosition();
            cornerPoints1[1] = m_Corner2.GetPosition();
            cornerPoints1[2] = m_Corner3.GetPosition();
            cornerPoints1[3] = m_Corner4.GetPosition();

            cornerPoints2[0] = otherEnt.m_Corner1.GetPosition();
            cornerPoints2[1] = otherEnt.m_Corner2.GetPosition();
            cornerPoints2[2] = otherEnt.m_Corner3.GetPosition();
            cornerPoints2[3] = otherEnt.m_Corner4.GetPosition();


            for (int i = 0; i < 4; i++)
                for (int j = 0; j < 4; j++)
                {
                    Vector3 v1 = cornerPoints1[i] - cornerPoints1[(i + 1) % 4];
                    Vector3 v2 = cornerPoints2[j] - cornerPoints2[(j + 1) % 4];
                    v1.Normalize();
                    v2.Normalize();

                    if (Math.Acos(Vector3.Dot(v1, v2)) < MathHelper.PiOver2 / 3 || Math.Acos(Vector3.Dot(v1, v2)) > MathHelper.Pi - MathHelper.PiOver2 / 5) // if the angle between them is less than 30 degrees
                    {
                        if (minDistSoFar > MathHelper.Min((cornerPoints1[i] - cornerPoints2[j]).Length() + (cornerPoints1[(i + 1) % 4] - cornerPoints2[(j + 1) % 4]).Length(), (cornerPoints1[(i + 1) % 4] - cornerPoints2[j]).Length() + (cornerPoints1[i] - cornerPoints2[(j + 1) % 4]).Length()))
                        {
                            minDistSoFar = MathHelper.Min((cornerPoints1[i] - cornerPoints2[j]).Length() + (cornerPoints1[(i + 1) % 4] - cornerPoints2[(j + 1) % 4]).Length(), (cornerPoints1[(i + 1) % 4] - cornerPoints2[j]).Length() + (cornerPoints1[i] - cornerPoints2[(j + 1) % 4]).Length());
                            firstIndex = i;
                            secondIndex = j;
                        }
                    }
                }

            if (minDistSoFar < 5)
            {
                return true;
            }
            else
                return false;


        }



        public void Initialise(Vector3 Position, Quaternion Rotation, string EntityName, CModel Model, List<CMaterial> Material, bool IsCollidable)
        {
            base.Initalise();

            //m_childList = new List<CDotXBoxEntity>(); //added by yorrick.
            m_linkedToList = new List<CNavQuadEntity>();

            m_portalInfo = new SPortalInfo();
            m_material = Material;
            m_canBeParent = false;

            m_entityName = EntityName;
            m_isDynamic = true;
            m_position = Position;
            m_modelData = Model;
            m_isCollidable = IsCollidable;
            m_rotation = Rotation;

            m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);

            m_scale = Vector3.One;

            // Transform local space OBB corners to world space
            InitialiseEntityOBB(m_modelData.GetOBB());

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            
            //IsTranslucent(((CDotXModel)m_modelData).m_model);
        }

#if EDITOR
        public override void Update()
        {

            Vector3 dif = GetPosition() - m_lastLocation;

            //dif = dif / 2;

            

            


            m_worldMatrix = Matrix.CreateScale(m_scale) * Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);


            m_Corner1.SetPosition(dif + m_Corner1.GetPosition());
            m_Corner2.SetPosition(dif + m_Corner2.GetPosition());
            m_Corner3.SetPosition(dif + m_Corner3.GetPosition());
            m_Corner4.SetPosition(dif + m_Corner4.GetPosition());


            m_model.m_vertices[0].Position = m_Corner1.GetPosition();
            m_model.m_vertices[1].Position = m_Corner2.GetPosition();
            m_model.m_vertices[2].Position = m_Corner3.GetPosition();
            m_model.m_vertices[3].Position = m_Corner4.GetPosition();

            m_lastLocation = GetPosition();

            m_model.m_vertexBuffer.SetData(m_model.m_vertices);


            // Update the obbs
            UpdateEntityOBB(m_modelData.GetOBB());

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            // The following two lines will only needed to be updated if the bounding box size changes or if the dynamic object moves(translation) in the scene.

            // This needs to be updated for the calcualtion that will take place in the below base update.
            UpdateCollisionSphere();

            // this needs to be called so that the spatial partioning tree is updated and the it won't just disappear from the scene.
            m_entityLeafNode.MoveNodeAroundTree(m_position, m_maxLengthOfEntity);



        }

        public override Vector3 Scale
        {
            get { return m_scale; }
            set { m_scale = value; }
        }
#else
        public override void Update(float DT)
        {
            m_worldMatrix = Matrix.CreateScale(m_scale) * Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);
            // This will apply the parent matrix on to the entity if it's has a parent.
            // This MUST be called straight after creating the new world matrix!
            UpdateIfChild();

            // Update the obbs
            List<CModelOBB> OBB = m_modelData.GetOBB();
            UpdateEntityOBB(OBB);

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            // The following two lines will only needed to be updated if the bounding box size changes or if the dynamic object moves(translation) in the scene.

            // This needs to be updated for the calculation that will take place in the below base update.
            UpdateCollisionSphere();
        }
#endif

        // Needs to be called to get the OBB coordiantes which will then be saved to xml.
        public Vector3[] GetBoxWorldCoordiantes(CModel ModelData)
        {
            CDotXModel temp = (CDotXModel)ModelData;

            Vector3[] main_obb = temp.GetMainOBB();

            Vector3[] world_coords = new Vector3[8];

            for (int i = 0; i < 8; ++i)
            {
                world_coords[i] = Vector3.Transform(main_obb[i], m_worldMatrix);
            }

            return world_coords;
        }

        public override void Render(CCamera Camera)
        {
            if (m_hidden)
                return;

            CVisual.Instance.Render(((CNavQuadModel)m_modelData), Camera, Matrix.Identity);
            
        }

        public override void ForwardRender(CCamera Camera)
        {
            if (m_hidden)
                return;

            CVisual.Instance.ForwardRender(((CNavQuadModel)m_modelData), Camera, Matrix.Identity);
        }

        public override void ForwardTranslucentRender(CCamera Camera)
        {
            if (m_hidden)
                return;

            CVisual.Instance.ForwardRender(((CNavQuadModel)m_modelData), Camera, Matrix.Identity);
        }
    }
}

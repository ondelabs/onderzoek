﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;



namespace Onderzoek.Entity
{
    public enum EPropertyGridInputType
    {
        TextBox,
        PullDownMultipleChoice,
        ModelSelection,
        MaterialSelection,
        ColourDialogue,
        Slider,
    }

    public class CEntityProperty
    {
        public EPropertyGridInputType inputType;

        public string propertyName;
        public string propertyValue;
        public List<string> pullDownOptions; //applicable only when input type is pulldown mult choice.

        public CEntityProperty(string name, string value, EPropertyGridInputType type)
        {
            propertyName = name;
            propertyValue = value;
            inputType = type;
        }

        /// <summary>
        /// returns the colour if the property is of type colour. returns vector3.zero fiit is not.
        /// </summary>
        /// <returns></returns>
        public Vector3 getColour()
        {
            if(inputType != EPropertyGridInputType.ColourDialogue)
                return Vector3.Zero;

            return new Vector3(float.Parse(propertyValue.Split(' ')[0]), float.Parse(propertyValue.Split(' ')[1]), float.Parse(propertyValue.Split(' ')[2]));
        }
        
    }
}

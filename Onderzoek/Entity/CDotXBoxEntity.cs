﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Graphics.PackedVector;
using Microsoft.Xna.Framework.Content;

using Onderzoek.Visual;
using Onderzoek.ModelData;
using Onderzoek.Physics;
using Onderzoek.Camera;

namespace Onderzoek.Entity
{
    public class CDotXBoxEntity : CDynamicEntity
    {
        protected Vector3 m_scale;

        public List<CDotXBoxEntity> m_childList; //list of children of this xbox entity in the editor. is #if editor necessary for this? No, only being used by editor so it's ok.

        public override void Dispose()
        {
            if (m_childList != null)
            {
                foreach (CDotXBoxEntity bb in m_childList)
                {
                    bb.Dispose();
                }

                m_childList.Clear();
                //m_childList = null;
            }

            base.Dispose();
        }

        public CDotXBoxEntity(Vector3 Position, Vector3 Rotation, string EntityName, CModel Model, List<CMaterial> Material, bool IsCollidable)
        {
            m_childList = new List<CDotXBoxEntity>();
#if EDITOR
            m_editorRotation = Rotation;
#endif

            Initialise(Position, Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z), EntityName, Model, Material, IsCollidable);
        }

        public void Initialise(Vector3 Position, Quaternion Rotation, string EntityName, CModel Model, List<CMaterial> Material, bool IsCollidable)
        {
            base.Initalise();

            m_childList = new List<CDotXBoxEntity>(); //added by yorrick.
#if EDITOR
            m_isScalingAllowed = true;
#endif


            m_portalInfo = new SPortalInfo();
            m_material = Material;
            m_canBeParent = false;

            for (int i = 0; i < m_material.Count; i++)
            {
                m_material[i].m_diffuse.W = 0.5f;
            }

            m_entityName = EntityName;
            m_isDynamic = true;
            m_position = Position;
            m_modelData = Model;
            m_isCollidable = IsCollidable;
            m_rotation = Rotation;

            m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);

            m_scale = Vector3.One;

            // Transform local space OBB corners to world space
            InitialiseEntityOBB(m_modelData.GetOBB());

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            IsTranslucent(((CDotXModel)m_modelData).m_model);
        }

#if EDITOR

        public void removeThisChild(CDotXBoxEntity childToBeRemoved)
        {

            for (int i = 0; i < m_childList.Count; i++)
            {
                if (m_childList[i] == childToBeRemoved)
                    m_childList.RemoveAt(i);
               // else
                    //removeThisChild(childToBeRemoved);

            }

        }

        public override void Update()
        {
            m_worldMatrix = Matrix.CreateScale(m_scale) * Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);

            // Update the obbs
            UpdateEntityOBB(m_modelData.GetOBB());

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            // The following two lines will only needed to be updated if the bounding box size changes or if the dynamic object moves(translation) in the scene.

            // This needs to be updated for the calcualtion that will take place in the below base update.
            UpdateCollisionSphere();
        }

        public override Vector3 Scale
        {
            get { return m_scale; }
            set { m_scale = value; }
        }
#else
        public override void Update(float DT)
        {
            m_worldMatrix = Matrix.CreateScale(m_scale) * Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);
            // This will apply the parent matrix on to the entity if it's has a parent.
            // This MUST be called straight after creating the new world matrix!
            UpdateIfChild();

            // Update the obbs
            List<CModelOBB> OBB = m_modelData.GetOBB();
            UpdateEntityOBB(OBB);

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            // The following two lines will only needed to be updated if the bounding box size changes or if the dynamic object moves(translation) in the scene.

            // This needs to be updated for the calculation that will take place in the below base update.
            UpdateCollisionSphere();
        }
#endif

        // Needs to be called to get the OBB coordiantes which will then be saved to xml.
        public Vector3[] GetBoxWorldCoordiantes(CModel ModelData)
        {
            CDotXModel temp = (CDotXModel)ModelData;

            Vector3[] main_obb = temp.GetMainOBB();

            Vector3[] world_coords = new Vector3[8];

            for (int i = 0; i < 8; ++i)
            {
                world_coords[i] = Vector3.Transform(main_obb[i], m_worldMatrix);
            }

            return world_coords;
        }

        public override void Render(CCamera Camera)
        {
            if (m_hidden)
                return;

            CVisual.Instance.Render(((CDotXModel)m_modelData).m_model, Camera, m_material, m_worldMatrix, m_opaqueParts);
        }

        public override void ForwardRender(CCamera Camera)
        {
            if (m_hidden)
                return;

            CVisual.Instance.ForwardRender(((CDotXModel)m_modelData).m_model, m_material, m_worldMatrix, m_opaqueParts);
        }

        public override void ForwardTranslucentRender(CCamera Camera)
        {
            if (m_hidden)
                return;

            CVisual.Instance.ForwardRender(((CDotXModel)m_modelData).m_model, m_material, m_worldMatrix, m_translucentParts);
        }
    }
}

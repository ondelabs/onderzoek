﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using Onderzoek.Visual;
using Onderzoek.ModelData;

namespace Onderzoek.Entity
{
    public class CStaticEntity : CEntity
    {
        public override void Dispose()
        {
            base.Dispose();
        }
        

#if ENGINE
        // Update for game... ie no update since it's a static object.
        public virtual void Update()
        {
        }
#endif
    }
}

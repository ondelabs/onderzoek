﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
#if EDITOR
using Microsoft.Xna.Framework.Graphics;
#endif

using Onderzoek.Visual;
using Onderzoek.ModelData;
using Onderzoek.Camera;

namespace Onderzoek.Entity
{
    public class CDirLightEntity : CStaticEntity
    {
        protected CVisual.SDirectionLight m_dirLight;
        protected bool m_shadowCaster = false;
        public List<int>[] m_renderList;
        protected bool m_mainLight = false;
        protected float m_lambda = 0.65f;

        public bool IsMainLight
        {
            get { return m_mainLight; }
#if EDITOR
            set
            {
                m_mainLight = value;
                m_propertyList[4].propertyValue = m_mainLight.ToString();
            }
#endif
        }

        public override void Dispose()
        {
            if (m_dirLight.s_shadowRTIndex != null)
            {
                for (int i = 0; i < m_dirLight.s_shadowRTIndex.Length; ++i)
                {
                    CVisual.Instance.DeleteShadowRenderTarget(m_dirLight.s_shadowRTIndex[i]);
                    m_dirLight.s_lightCamera = null;
                    m_dirLight.s_model = null;
                }
            }

            base.Dispose();
        }

#if EDITOR
        Onderzoek.Screens.CGameScreenHelper m_screen;
        int m_numOfSplits = 3;
        Matrix m_scdWorldMatrix;
        List<CModelOBB> m_originalOBB;
        CCamera m_mainCamera;
        Vector3 m_pivotPosition;

        public CDirLightEntity(Vector3 Position, string EntityName, bool ShadowCaster, int NumOfSplits, Vector3 Colour, float Intensity,
            List<CModelOBB> OBB, bool MainLight, CCamera MainCamera, Onderzoek.Screens.CGameScreenHelper Screen, CModel ModelData, List<CMaterial> MaterialList)
        {
            Initialise(Position, EntityName, ShadowCaster, NumOfSplits, Colour, Intensity, OBB, MainLight, MainCamera, Screen, ModelData, MaterialList);
        }

        public void Initialise(Vector3 Position, string EntityName, bool ShadowCaster, int NumOfSplits, Vector3 Colour, float Intensity,
            List<CModelOBB> OBB, bool MainLight, CCamera MainCamera, Onderzoek.Screens.CGameScreenHelper Screen, CModel ModelData, List<CMaterial> MaterialList)
        {
            m_originalOBB = OBB;
            m_scdWorldMatrix = Matrix.Identity;
            m_screen = Screen;
            if (ModelData != null)
            {
                m_modelName = ModelData.GetModelName();
            }
            m_isScalingAllowed = true;
            if (MaterialList != null)
            {
                m_material = MaterialList;
            }
            else
            {
                m_material = new List<CMaterial>();
            }
            m_numOfSplits = NumOfSplits;
            m_modelData = ModelData;
            m_mainCamera = MainCamera;

            foreach (CMaterial mat in m_material)
            {
                mat.m_emissive = mat.m_diffuse * 0.25f;
                mat.m_diffuse.W = 0.75f;
            }
            if (m_modelData != null)
            {
                IsTranslucent(m_modelData.getBaseModel());
            }

            m_position = Position;
            m_pivotPosition = Position + new Vector3(0,20,0);
#else
        public CDirLightEntity(Vector3 Direction, Vector3 Position, string EntityName, bool ShadowCaster, int NumOfSplits, Vector3 Colour, float Intensity,
            List<CModelOBB> OBB, bool MainLight, float Lambda)
        {
            Initialise(Direction, Position, EntityName, ShadowCaster, NumOfSplits, Colour, Intensity, OBB, MainLight, Lambda);
        }

        public void Initialise(Vector3 Direction, Vector3 Position, string EntityName, bool ShadowCaster, int NumOfSplits, Vector3 Colour, float Intensity,
            List<CModelOBB> OBB, bool MainLight, float Lambda)
        {
#endif
            m_lambda = Lambda;

            if (NumOfSplits < 1)
            {
                NumOfSplits = 0;
            }

            m_mainLight = MainLight;
            if (m_mainLight == true)
            {
                m_render = true;
            }

            m_portalInfo = new SPortalInfo();
            m_dirLight.s_colour = Colour;
            m_dirLight.s_intensity = Intensity;
#if ENGINE
            m_dirLight.s_direction = Direction;
#else
            m_dirLight.s_direction = Vector3.Up;
#endif
            m_dirLight.s_specularIntensity = 1.0f;

            if (m_material == null)
                m_material = new List<CMaterial>();

#if ENGINE
            // Tell CVisual to initalise a rendertarget for this light
            if (ShadowCaster == true)
            {
                m_renderList = new List<int>[NumOfSplits];
                m_shadowCaster = ShadowCaster;
                m_dirLight.s_shadowRTIndex = new int[NumOfSplits];
                m_dirLight.s_lightCamera = new COrthoCamera[NumOfSplits];
                m_dirLight.s_shadowTerm = 0.5f;
                // This is a hack to get the dir light to use CalculateSplitDistances
                CFPCamera temp_camera = new CFPCamera(1.0f, 90.0f, 1.0f, 1000.0f, new Vector3(), new Vector3());
                m_dirLight.s_nearFar = temp_camera.CalculateSplitDistances(m_dirLight.s_lightCamera.Length, ref m_lambda, 500.0f);

                for (int i = 0; i < NumOfSplits; ++i)
                {
                    m_renderList[i] = new List<int>();
                    if (i == 0)
                    {
                        m_dirLight.s_shadowRTIndex[i] = CVisual.Instance.CreateRenderTargetForShadows(true);
                    }
                    else
                    {
                        m_dirLight.s_shadowRTIndex[i] = CVisual.Instance.CreateRenderTargetForShadows(false);
                    }
                }
            }

            for (int i = 0; i < m_dirLight.s_lightCamera.Length; ++i)
            {
                // To avoid edge problems, scale the frustum so
                // that it's at least a few pixels larger
                m_dirLight.s_lightCamera[i] = new COrthoCamera(100, 100, 1, 1000, Matrix.Identity, Vector3.One);
            }
#endif

            m_entityName = EntityName;
            m_isDynamic = false;
            m_isCollidable = false;

            m_worldMatrix = Matrix.Identity * Matrix.CreateTranslation(m_position);

            // Transform local space OBB corners to world space
            InitialiseEntityOBB(OBB);

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

#if EDITOR
            if (m_modelName != null)
            {
                FillPropertyList();
            }
#endif
        }

        public override void SetRender(bool Render, bool AtStartOfFrame)
        {
            if (m_mainLight == false)
            {
                // This check is to prevent uneccessary tests against clip plane.
                // Only occurs when the SPT list is being calcualted for the GI camera view.
                if (AtStartOfFrame == true)
                {
                    m_render = Render;
                }
                else
                {
                    m_render = true;
                }
            }
        }


        public override int GetEntityID()
        {
            return (int)EBasicEntityIDs.DirLightEntity;
            //return base.GetEntityID();
        }

        public void Update(CCamera MainCamera)
        {
            // A high Lambda value will decrease the length of all the frustums except the last cascaded frustum.
            m_dirLight.s_nearFar = null;
            //float lambda = 0.65f;
            m_dirLight.s_nearFar = MainCamera.CalculateSplitDistances(m_dirLight.s_lightCamera.Length, ref m_lambda, 500.0f);

            //System.Diagnostics.Trace.WriteLine(m_dirLight.s_nearFar[0] + " " + m_dirLight.s_nearFar[1] + " " + m_dirLight.s_nearFar[2]);

            for (int i = 0; i < m_dirLight.s_lightCamera.Length; ++i)
            {
                // To avoid edge problems, scale the frustum so
                // that it's at least a few pixels larger
                m_dirLight.s_lightCamera[i] = MainCamera.CreateLightViewProjectionMatrix(m_dirLight.s_nearFar[i], m_dirLight.s_nearFar[i + 1], 1.1f, m_dirLight.s_direction);
            }
        }

        public CVisual.SDirectionLight GetLightData()
        {
            return m_dirLight;
        }

        public int GetRTIndex(int Index)
        {
            return m_dirLight.s_shadowRTIndex[Index];
        }

        public COrthoCamera GetCamera(int Index)
        {
            return m_dirLight.s_lightCamera[Index];
        }

        public Vector3 GetLightDir()
        {
            return m_dirLight.s_direction;
        }

        public override void Render(CCamera Camera)
        {
#if ENGINE
            CVisual.Instance.AddDirLightToList(m_dirLight);

            if (m_shadowCaster == true)
            {
                CVisual.Instance.AddDirLightToShadowList(m_dirLight);
            }
#else
            CVisual.Instance.AddDirLightToList(m_dirLight);

            if (m_mainLight == false)
            {
                CVisual.Instance.DrawBounds(Camera, m_wrldOBB);
            }

            if (m_modelData != null)
            {
                CVisual.Instance.Render(m_modelData.getBaseModel(), Camera, m_material, m_scdWorldMatrix, m_translucentParts);
                CVisual.Instance.Render(m_modelData.getBaseModel(), Camera, m_material, m_worldMatrix, m_translucentParts);
            }
#endif
        }

#if EDITOR
        public override void Update()
        {
            CVisual.Instance.AddToLineList(m_pivotPosition, m_position + (m_dirLight.s_direction * 5.0f), new Color(255, 50, 50));
            CVisual.Instance.AddToLineList(m_position + (m_dirLight.s_direction * 5.0f), m_position, new Color(50, 255, 75));

            m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);

            // Update the obbs
            UpdateEntityOBB(m_originalOBB);

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            // The following two lines will only needed to be updated if the bounding box size changes or if the dynamic object moves(translation) in the scene.

            // This needs to be updated for the calcualtion that will take place in the below base update.
            UpdateCollisionSphere();

            // this needs to be called so that the spatial partioning tree is updated and the it won't just disappear from the scene.
            m_entityLeafNode.MoveNodeAroundTree(m_position, m_maxLengthOfEntity);

            m_scdWorldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_pivotPosition);

            m_dirLight.s_direction = CToolbox.UnitVector(m_pivotPosition - m_position);
        }

        public override void ForwardTranslucentRender(CCamera Camera)
        {
            CVisual.Instance.AddDirLightToList(m_dirLight);

            if (m_mainLight == false)
            {
                CVisual.Instance.DrawBounds(Camera, m_wrldOBB);
            }

            if (m_modelData != null)
            {
                CVisual.Instance.ForwardRender(m_modelData.getBaseModel(), m_material, m_scdWorldMatrix, m_translucentParts);
                CVisual.Instance.ForwardRender(m_modelData.getBaseModel(), m_material, m_worldMatrix, m_translucentParts);
            }
        }

        public override Vector3 Scale
        {
            get
            {
                float val = 0;
                for (int i = 0; i < m_originalOBB[0].m_OBB.Length; ++i)
                {
                    if (m_originalOBB[0].m_OBB[i].X > val)
                    {
                        val = m_originalOBB[0].m_OBB[i].X;
                    }
                }

                return new Vector3(val, val, val);
            }
            set
            {
                m_originalOBB = CModel.CreateOBBofUnitSize(value.X);
            }
        }

        public override void writeEntityProperties(XmlWriter writer)
        {
            for (int i = 0; i < m_propertyList.Count; i++)
            {
                writer.WriteElementString(m_propertyList[i].propertyName, m_propertyList[i].propertyValue);
            }

            float val = 0;
            for (int i = 0; i < m_originalOBB[0].m_OBB.Length; ++i)
            {
                if (m_originalOBB[0].m_OBB[i].X > val)
                {
                    val = m_originalOBB[0].m_OBB[i].X;
                }
            }

            writer.WriteElementString("Scale", val.ToString());
        }

        public override void readEntityProperties(XmlReader reader)
        {
            reader.ReadToFollowing("EntityData");
            while (!(reader.Name == "EntityData" && reader.NodeType == XmlNodeType.EndElement))
            {
                reader.Read();
                reader.Read();

                reader.MoveToContent();
                string propName = reader.Name;

                if (reader.Name == "EntityData" && reader.NodeType == XmlNodeType.EndElement)
                    break;
                string propVal;

                if (reader.IsEmptyElement)
                    propVal = "";
                else
                {
                    reader.Read();
                }

                propVal = reader.Value;

                if (propName.Equals("Scale"))
                {
                    float flt = 1.0f;
                    CToolbox.StringToFloat(propVal, ref flt);

                    m_originalOBB = CModel.CreateOBBofUnitSize(flt);
                }
                else
                {
                    for (int i = 0; i < m_propertyList.Count; i++)
                    {
                        if (m_propertyList[i].propertyName.Equals(propName))
                        {
                            m_propertyList[i].propertyValue = propVal;
                            break;
                        }
                    }
                }
            }
        }

        public override void FillPropertyList()
        {
            this.m_propertyList = new List<CEntityProperty>();

            string temp = m_modelName.Replace("Model/", "");
            temp = temp.Replace("Content/", "");
            temp = temp.Replace("Content\\\\", "");
            temp = temp.Replace("Content\\", "");
            temp = temp.Replace("Model\\\\", "");
            temp = temp.Replace("Model\\", "");
            temp = temp.Replace("..\\", "");
            m_modelName = temp;

            m_propertyList.Add(new CEntityProperty("Shadow_Caster", m_shadowCaster.ToString(), EPropertyGridInputType.PullDownMultipleChoice));
            m_propertyList[m_propertyList.Count - 1].pullDownOptions = new List<string>();
            m_propertyList[m_propertyList.Count - 1].pullDownOptions.Add("True");
            m_propertyList[m_propertyList.Count - 1].pullDownOptions.Add("False");

            m_propertyList.Add(new CEntityProperty("Num_Splits_for_shadows", m_numOfSplits.ToString(), EPropertyGridInputType.TextBox));

             string col = (m_dirLight.s_colour.X * 255.0f).ToString() + " " + (m_dirLight.s_colour.Y * 255.0f).ToString()
                     + " " + (m_dirLight.s_colour.Z * 255.0f).ToString();
            m_propertyList.Add(new CEntityProperty("Light_Colour", col, EPropertyGridInputType.ColourDialogue));
            m_propertyList.Add(new CEntityProperty("Intensity", m_dirLight.s_intensity.ToString(), EPropertyGridInputType.TextBox));

            m_propertyList.Add(new CEntityProperty("Main_Light", m_mainLight.ToString(), EPropertyGridInputType.PullDownMultipleChoice));
            m_propertyList[m_propertyList.Count - 1].pullDownOptions = new List<string>();
            m_propertyList[m_propertyList.Count - 1].pullDownOptions.Add("True");
            m_propertyList[m_propertyList.Count - 1].pullDownOptions.Add("False");

            m_propertyList.Add(new CEntityProperty("Pivot_Position", m_pivotPosition.ToString(), EPropertyGridInputType.TextBox));
        }

        public override void UpdateFromPropertyList()
        {
            bool bl = false;
            if (CToolbox.StringToBool(m_propertyList[0].propertyValue, ref bl) == false)
            {
                m_propertyList[0].propertyValue = m_shadowCaster.ToString();
            }
            else
            {
                m_shadowCaster = bl;
            }

            int integer = 1;
            if (CToolbox.StringToInt(m_propertyList[1].propertyValue, ref integer) == false)
            {
                m_propertyList[1].propertyValue = m_numOfSplits.ToString();
            }
            else
            {
                if (integer < 1)
                {
                    integer = 1;
                }

                m_numOfSplits = integer;
            }

            Color col = Color.White;
            if (CToolbox.StringToColour(m_propertyList[2].propertyValue, ref col) == false)
            {
                m_propertyList[2].propertyValue = (m_dirLight.s_colour.X * 255.0f).ToString() + " " + (m_dirLight.s_colour.Y * 255.0f).ToString()
                     + " " + (m_dirLight.s_colour.Z * 255.0f).ToString();
            }
            else
            {
                m_dirLight.s_colour = new Vector3((float)col.R / 255.0f, (float)col.G / 255.0f, (float)col.B / 255.0f);
            }

            float flt = 1;
            if (CToolbox.StringToFloat(m_propertyList[3].propertyValue, ref flt) == false)
            {
                m_propertyList[3].propertyValue = m_dirLight.s_intensity.ToString();
            }
            else
            {
                if (flt < 0)
                {
                    flt = 0;
                }

                m_dirLight.s_intensity = flt;
            }

            bl = false;
            if (CToolbox.StringToBool(m_propertyList[4].propertyValue, ref bl) == false)
            {
                m_propertyList[4].propertyValue = m_mainLight.ToString();
            }
            else
            {
                if (bl == true)
                {
                    m_screen.ChangeMainLight(this, true);
                    m_mainLight = true;
                }
                else
                {
                    m_screen.ChangeMainLight(this, false);
                    m_mainLight = false;
                }
            }

            Vector3 vec3 = Vector3.Zero;
            if (CToolbox.StringToVector3(m_propertyList[5].propertyValue, ref vec3) == false)
            {
                m_propertyList[5].propertyValue = m_pivotPosition.ToString();
            }
            else
            {
                m_pivotPosition = vec3;
            }
        }

        public void setDirection(Vector3 newDir)
        {
            m_dirLight.s_direction = newDir;
        }

        public Vector3 getDirection()
        {
            return m_dirLight.s_direction;
        }

        public void setColour(Vector3 newCol)
        {
            m_dirLight.s_colour = newCol;
        }

        public Vector3 getColour()
        {
            return m_dirLight.s_colour;
        }

        public void setIntensity(float newInten)
        {
            m_dirLight.s_intensity = newInten;
        }

        public float getIntensity()
        {
            return m_dirLight.s_intensity;
        }
#else
        public static void LoadEntityData(CEntityDataStore EntityData, Onderzoek.Screens.CGameScreenHelper CurScreen,
            ref Dictionary<string, CModel.SModelData> ModelList, ref ContentManager Content)
        {
            bool shadow_caster = false;
            int num_splits = 3;
            Vector3 col = Vector3.One;
            float intensity = 1.0f;
            bool main_light = false;
            Vector3 dir = Vector3.Zero;
            float scale = 1.0f;
            float lambda = 0.65f;

            for (int i = 0; i < EntityData.m_properties.Count; ++i)
            {
                if (EntityData.m_properties[i].propertyName == "Shadow_Caster")
                {
                    CToolbox.StringToBool(EntityData.m_properties[i].propertyValue, ref shadow_caster);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Num_Splits_for_shadows")
                {
                    CToolbox.StringToInt(EntityData.m_properties[i].propertyValue, ref num_splits);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Light_Colour")
                {
                    Microsoft.Xna.Framework.Graphics.Color colour = Microsoft.Xna.Framework.Graphics.Color.White;
                    CToolbox.StringToColour(EntityData.m_properties[i].propertyValue, ref colour);
                    col = new Vector3((float)colour.R / 255.0f, (float)colour.G / 255.0f, (float)colour.B / 255.0f);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Intensity")
                {
                    CToolbox.StringToFloat(EntityData.m_properties[i].propertyValue, ref intensity);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Main_Light")
                {
                    CToolbox.StringToBool(EntityData.m_properties[i].propertyValue, ref main_light);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Pivot_Position")
                {
                    Vector3 pivot_pos = Vector3.Zero;
                    CToolbox.StringToVector3(EntityData.m_properties[i].propertyValue, ref pivot_pos);
                    dir = CToolbox.UnitVector(pivot_pos - EntityData.m_position);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Scale")
                {
                    CToolbox.StringToFloat(EntityData.m_properties[i].propertyValue, ref scale);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Lambda")
                {
                    CToolbox.StringToFloat(EntityData.m_properties[i].propertyValue, ref lambda);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
            }

            // Create the model
            CDirLightEntity ent = new CDirLightEntity(dir, EntityData.m_position, EntityData.m_entityName, shadow_caster, num_splits, col, intensity,
                CModel.CreateOBBofUnitSize(scale), main_light, lambda);
            CurScreen.AddEntityNoSPT(ent);
        }
#endif

        public bool IsShadowCaster()
        {
            return m_shadowCaster;
        }
    }
}

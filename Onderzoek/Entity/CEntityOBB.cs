﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Onderzoek.ModelData;

namespace Onderzoek.Entity
{
    public class CEntityOBB
    {
        public Vector3[] m_wrldOBB;
        public BoundingBox m_localOBBNoRot;
        public BoundingBox m_OBB;
        public Plane[] m_planes;
        public Plane[] m_localPlanesNoRot;
        public Vector3 m_position;
        public Vector3 m_rotation;
        public Vector3 m_scale;
        public Matrix m_invRotMatrix;
        public bool m_isAABB = true;
        //public float m_radius;

        public CEntityOBB[] m_child;
        //public CEntityOBB m_parent;

        //public Matrix s_invWrld;
        //public Matrix s_wrld;
        //public Matrix s_invRotWrld;
        //public Matrix s_rotWrld;
        //public Vector3 s_boxCentre;

        public void Dispose()
        {
            m_wrldOBB = null;
            m_planes = null;
            m_child = null;
        }

        public CEntityOBB()
        {
            //m_parent = null;

            m_planes = new Plane[6];
            m_wrldOBB = new Vector3[8];
            //m_radius = 0;
        }

        public void InitialiseEntityOBB( CModelOBB OBB, Matrix WorldMatrix, Matrix Rotation)
        {
            // Transform the local obb to world space
            CToolbox.TransformLocalToWorld(OBB.m_OBB, out m_wrldOBB, WorldMatrix);
            m_OBB = BoundingBox.CreateFromPoints(m_wrldOBB);
            m_invRotMatrix = Matrix.Invert(Matrix.CreateFromYawPitchRoll(OBB.m_rotation.X, OBB.m_rotation.Y, OBB.m_rotation.Z));
            Vector3[] local_oobb = new Vector3[8];
            CToolbox.TransformLocalToWorld(OBB.m_OBB, out local_oobb, m_invRotMatrix);
            m_localOBBNoRot = BoundingBox.CreateFromPoints(local_oobb);

            // Create local planes.
            m_localPlanesNoRot = new Plane[6];
            m_localPlanesNoRot[0] = CToolbox.CreatePlane(new Vector3(-1, 0, 0), m_localOBBNoRot.Min);
            m_localPlanesNoRot[1] = CToolbox.CreatePlane(new Vector3(1, 0, 0), m_localOBBNoRot.Max);
            m_localPlanesNoRot[2] = CToolbox.CreatePlane(new Vector3(0, 0, -1), m_localOBBNoRot.Min);
            m_localPlanesNoRot[3] = CToolbox.CreatePlane(new Vector3(0, 0, 1), m_localOBBNoRot.Max);
            m_localPlanesNoRot[4] = CToolbox.CreatePlane(new Vector3(0, 1, 0), m_localOBBNoRot.Max);
            m_localPlanesNoRot[5] = CToolbox.CreatePlane(new Vector3(0, -1, 0), m_localOBBNoRot.Min);

            // Create local planes and find the local radius
            CreatePlanes(Rotation);
            //FindRadiusOfBB(out m_radius);

            m_position = OBB.m_position;
            m_rotation = OBB.m_rotation;
            m_scale = OBB.m_scale;

            if (OBB.m_rotation != Vector3.Zero)
            {
                m_isAABB = false;
            }

            if (OBB.m_child != null)
            {
                // Create an array of child obbs
                m_child = new CEntityOBB[OBB.m_child.Count];

                // Initalising the children
                for (int i = 0; i < m_child.Length; ++i)
                {
                    m_child[i] = new CEntityOBB();
                    m_child[i].InitialiseEntityOBB(OBB.m_child[i], WorldMatrix, Rotation);
                }
            }
        }

        public void UpdateEntityOBB(CModelOBB OBB, Matrix WorldMatrix, Quaternion Rotation)
        {
            CToolbox.TransformLocalToWorld(OBB.m_OBB, out m_wrldOBB, WorldMatrix);
            m_OBB = BoundingBox.CreateFromPoints(m_wrldOBB);

            // Update the local planes
            UpdatePlanes(Rotation);

            // Update the children
            for (int i = 0; i < m_child.Length; ++i)
            {
                m_child[i].UpdateEntityOBB(OBB.m_child[i], WorldMatrix, Rotation);

                m_child[i].m_isAABB = true;
                if (Rotation != new Quaternion(0, 0, 0, 0))
                {
                    if (Rotation != Quaternion.Identity)
                    {
                        m_child[i].m_isAABB = false;
                    }
                }
            }
        }

        protected void CreatePlanes(Matrix WorldNoPos)
        {
            CreatePlanes(WorldNoPos, m_wrldOBB, ref m_planes);
        }

        public static void CreatePlanes(Matrix WorldNoPos, Vector3[] WorldOBB, ref Plane[] Planes)
        {
            Vector3 min, max;
            CToolbox.GetMinMax(WorldOBB, out min, out max);
            //BoundingBox temp = BoundingBox.CreateFromPoints(m_wrldOBB[OBBID].s_wrldOBB);

            if (Planes == null)
            {
                Planes = new Plane[6];
            }

            Planes[0] = CToolbox.CreatePlane(CToolbox.UnitVector(Vector3.Transform(new Vector3(-1, 0, 0), WorldNoPos)), min);//temp.Min);
            Planes[1] = CToolbox.CreatePlane(CToolbox.UnitVector(Vector3.Transform(new Vector3(1, 0, 0), WorldNoPos)), max);//temp.Max);
            Planes[2] = CToolbox.CreatePlane(CToolbox.UnitVector(Vector3.Transform(new Vector3(0, 0, -1), WorldNoPos)), min);//temp.Min);
            Planes[3] = CToolbox.CreatePlane(CToolbox.UnitVector(Vector3.Transform(new Vector3(0, 0, 1), WorldNoPos)), max);//temp.Max);
            Planes[4] = CToolbox.CreatePlane(CToolbox.UnitVector(Vector3.Transform(new Vector3(0, 1, 0), WorldNoPos)), max);//temp.Max);
            Planes[5] = CToolbox.CreatePlane(CToolbox.UnitVector(Vector3.Transform(new Vector3(0, -1, 0), WorldNoPos)), min);//temp.Min);
        }

        public void UpdatePlanes(Quaternion Rotation)
        {
            ////BoundingBox temp = BoundingBox.CreateFromPoints(m_wrldOBB);

            //m_planes[0].Normal = CToolbox.UnitVector(Vector3.Transform(new Vector3(-1, 0, 0), Rotation));
            //m_planes[1].Normal = CToolbox.UnitVector(Vector3.Transform(new Vector3(1, 0, 0), Rotation));
            //m_planes[2].Normal = CToolbox.UnitVector(Vector3.Transform(new Vector3(0, 0, -1), Rotation));
            //m_planes[3].Normal = CToolbox.UnitVector(Vector3.Transform(new Vector3(0, 0, 1), Rotation));
            //m_planes[4].Normal = CToolbox.UnitVector(Vector3.Transform(new Vector3(0, 1, 0), Rotation));
            //m_planes[5].Normal = CToolbox.UnitVector(Vector3.Transform(new Vector3(0, -1, 0), Rotation));


            ////Vector3 min, max;
            ////CToolbox.GetMinMax(m_wrldOBB[OBBID].s_wrldOBB, out min, out max);

            ////if (m_planes == null)
            ////{
            ////    m_planes = new Plane[6];
            ////}

            m_planes[0] = CToolbox.CreatePlane(CToolbox.UnitVector(Vector3.Transform(new Vector3(-1, 0, 0), Rotation)), m_OBB.Min);//temp.Min);
            m_planes[1] = CToolbox.CreatePlane(CToolbox.UnitVector(Vector3.Transform(new Vector3(1, 0, 0), Rotation)), m_OBB.Max);//temp.Max);
            m_planes[2] = CToolbox.CreatePlane(CToolbox.UnitVector(Vector3.Transform(new Vector3(0, 0, -1), Rotation)), m_OBB.Min);//temp.Min);
            m_planes[3] = CToolbox.CreatePlane(CToolbox.UnitVector(Vector3.Transform(new Vector3(0, 0, 1), Rotation)), m_OBB.Max);//temp.Max);
            m_planes[4] = CToolbox.CreatePlane(CToolbox.UnitVector(Vector3.Transform(new Vector3(0, 1, 0), Rotation)), m_OBB.Max);//temp.Max);
            m_planes[5] = CToolbox.CreatePlane(CToolbox.UnitVector(Vector3.Transform(new Vector3(0, -1, 0), Rotation)), m_OBB.Min);//temp.Min);
        }

        //protected void FindRadiusOfBB(out float Radius)
        //{
        //    BoundingBox temp = BoundingBox.CreateFromPoints(m_wrldOBB);

        //    Radius = Vector3.Distance(temp.Min, temp.Max) * 0.5f;
        //}
    }
}

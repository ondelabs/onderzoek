﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

using System.Xml;

using Microsoft.Xna.Framework;

using Onderzoek.Visual;
using Onderzoek.ModelData;
using Onderzoek.Camera;

namespace Onderzoek.Entity
{
    public class CNavMeshPoint : CStaticEntity
    {
        bool m_initialCollidableValue;

        public override void Dispose()
        {
            base.Dispose();
        }

        public CNavMeshPoint(Vector3 Position, Vector3 Rotation, string EntityName, CModel Model, List<CMaterial> Material, bool IsCollidable)
        {
#if EDITOR
            m_materialName = "";
            m_modelName = "";
#endif
            Initialise(Position, Rotation, EntityName, Model, Material, IsCollidable);
        }


        public void Initialise(Vector3 Position, Vector3 Rotation, string EntityName, CModel Model, List<CMaterial> Material, bool IsCollidable)
        {
#if EDITOR
            this.m_propertyList = new List<CEntityProperty>();
            FillPropertyList();
            m_editorRotation = Rotation;
#endif

            m_portalInfo = new SPortalInfo();
            m_material = Material;

            m_rotation = Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z);

            m_entityName = EntityName;
            m_isDynamic = false;
            m_position = Position;
            m_modelData = Model;
            m_initialCollidableValue = IsCollidable;
            m_isCollidable = IsCollidable;

            m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);

            // Transform local space OBB corners to world space
            InitialiseEntityOBB(Model.GetOBB());

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            UpdateCollisionSphere();

            //CEntityProperty modelList = new CEntityProperty("Model", "Test", EPropertyGridInputType.PullDownMultipleChoice);

            //modelList.pullDownOptions.Add( getPossibleModelsForSelectedType()

            //m_propertyList.Add();

            IsTranslucent(((CDotXModel)m_modelData).m_model);
        }

        public override void Reinitialise()
        {
            m_isCollidable = m_initialCollidableValue;
            m_hidden = false;
        }

        public override void SetMaterialList(List<CMaterial> MaterialList)
        {
            base.SetMaterialList(MaterialList);

            IsTranslucent(((CDotXModel)m_modelData).m_model);
        }

        public override void Render(CCamera Camera)
        {
            if (m_hidden == true)
            {
                return;
            }

            CVisual.Instance.Render(((CDotXModel)m_modelData).m_model, Camera, m_material, m_worldMatrix, m_opaqueParts);
        }

        public override void ForwardRender(CCamera Camera)
        {
            if (m_hidden == true)
            {
                return;
            }

            CVisual.Instance.ForwardRender(((CDotXModel)m_modelData).m_model, m_material, m_worldMatrix, m_opaqueParts);
        }

        public override void ForwardTranslucentRender(CCamera Camera)
        {
            if (m_hidden == true)
            {
                return;
            }

            CVisual.Instance.ForwardRender(((CDotXModel)m_modelData).m_model, m_material, m_worldMatrix, m_translucentParts);
        }

        public override void RenderForShadow(CCamera Camera)
        {
            if (m_hidden == true)
            {
                return;
            }

            CVisual.Instance.RenderForShadow(((CDotXModel)m_modelData).m_model, m_worldMatrix);
        }

        #region Editor stuff
#if EDITOR
        

        public override void FillPropertyList()
        {
            CEntityProperty modelSelectionEntityProperty = new CEntityProperty("Model", "", EPropertyGridInputType.ModelSelection);

            m_propertyList.Add(modelSelectionEntityProperty);

            modelSelectionEntityProperty = new CEntityProperty("Material", "", EPropertyGridInputType.MaterialSelection);

            m_propertyList.Add(modelSelectionEntityProperty);

            CEntityProperty collidable = new CEntityProperty("Collides", "", EPropertyGridInputType.PullDownMultipleChoice);
            string[] strList = { "Yes", "No" };
            collidable.pullDownOptions = new List<string>(strList);
            m_propertyList.Add(collidable);

            //base.FillPropertyList();
        }

        public override int GetEntityID()
        {
            return 1; // z hard coding.
            
            //return base.getEntityNumber();
        }


        public override void UpdateFromPropertyList()
        {
            for (int i = 0; i < m_propertyList.Count; i++)
            {
                if (m_propertyList[i].propertyName.Equals("Model"))
                {
                    m_modelName = m_propertyList[i].propertyValue;
                }


                if (m_propertyList[i].propertyName.Equals("Material"))
                {
                    m_materialName = m_propertyList[i].propertyValue;
                     
                }
            }


            
            //base.UpdateFromPropertyList();
        }

        
#endif
        #endregion
    }
}

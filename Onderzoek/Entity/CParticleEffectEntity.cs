﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Onderzoek.Visual;
using Onderzoek.Entity;
using Onderzoek;
using Onderzoek.Particle_System;
using Onderzoek.Camera;
using Onderzoek.ModelData;
using Onderzoek.Physics;

namespace Onderzoek.Entity
{
    public class CParticleEffectEntity : CDynamicEntity
    {
        public CParticleEffect m_effect;
        public List<CModelOBB> m_OBB;

        protected Camera.CCamera m_cameraReference;

#if ENGINE
        public CParticleEffectEntity(Vector3 Position, Vector3 Rotation, List<CModelOBB> ParticleSystemOBB, string EntityName, bool IsCollidable, CCamera camera)
        {
            Initialise(Position, Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z), ParticleSystemOBB, EntityName, IsCollidable, camera);
        }

        public CParticleEffectEntity(Vector3 Position, Vector3 Rotation, List<CModelOBB> ParticleSystemOBB, string EntityName, bool IsCollidable, CCamera camera,
            CParticleEffectStore Effect)
        {
            Initialise(Position, Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z), ParticleSystemOBB, EntityName, IsCollidable, camera);
            m_effect.m_effectStore = Effect;
        }
#else
        public CParticleEffectEntity(Vector3 Position, Vector3 Rotation, CModel Model, string EntityName, bool IsCollidable, CCamera camera)
        {
#if EDITOR
            m_editorRotation = Rotation;
#endif

            Initialise(Position, Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z), Model, EntityName, IsCollidable, camera);
        }
#endif

#if ENGINE
        public void Initialise(Vector3 Position, Quaternion Rotation, List<CModelOBB> ParticleSystemOBB, string EntityName, bool IsCollidable, Camera.CCamera camera)
#else
        public void Initialise(Vector3 Position, Quaternion Rotation, CModel Model, string EntityName, bool IsCollidable, Camera.CCamera camera)
#endif
        {
            base.Initalise();

            m_cameraReference = camera;

            m_entityName = EntityName;

            m_effect = new CParticleEffect();

            m_portalInfo = new SPortalInfo();
            m_canBeParent = false;

            m_isDynamic = true;
            m_position = Position;
            
            m_isCollidable = IsCollidable;
            m_rotation = Rotation;

#if ENGINE
            if (ParticleSystemOBB != null)
            {
                m_OBB = ParticleSystemOBB;
            }
            else
#else
            m_modelData = Model;
#endif
            {
                // Need to fill in the correct values.
                m_OBB = CPhysics.CreateBoundingBoxFromPhysicsData(Vector3.Zero, Vector3.Zero, Vector3.One, Vector3.One, Vector3.One, Vector3.One, 5, 1);
            }

            m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);

            InitialiseEntityOBB(m_OBB);
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            m_wholeTranslucent = true;
        }

        public override void ForwardTranslucentRender(CCamera Camera)
        {
            
            //CVisual.Instance.RenderParticles(m_wm, m_col, m_material[0].m_textureID);
            m_effect.Render(m_worldMatrix, Camera);
            

            // This is to render without a texture
            //CVisual.Instance.SetUpParticleEffect(0, Camera);
            //CVisual.Instance.RenderParticles(m_wm, m_col);

            
        }

#if EDITOR
        public override void Update()
        {
            m_effect.Update(m_worldMatrix, m_cameraReference);
            
            m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);
            //base.Update();
        }
#else
        public override void Update(float DT)
        {
            // Updating the particle system itself.
            m_effect.Update(m_worldMatrix, m_cameraReference);

            m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);
            // This will apply the parent matrix on to the entity if it's has a parent.
            // This MUST be called straight after creating the new world matrix!
            UpdateIfChild();

            // Update the obbs
            UpdateEntityOBB(m_OBB);

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            // The following two lines will only needed to be updated if the bounding box size changes or if the dynamic object moves(translation) in the scene.

            // This needs to be updated for the calcualtion that will take place in the below base update.
            UpdateCollisionSphere();
        }
#endif
        

        

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Xml;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

using Onderzoek.Visual;
using Onderzoek.ModelData;
using Onderzoek.Camera;
using Onderzoek.Physics;
using Onderzoek.Event;

namespace Onderzoek.Entity
{
    public class CSphereTriggerEntity : CDynamicEntity
    {
        protected CEvent m_event;
        protected List<string> m_triggeredBy;
        protected bool m_triggeredByAll = false;
        protected bool m_infiniteLife = false;
        protected int m_life = -1;

        protected List<string> m_affectedEntities;
        protected List<int> m_hitList;
        protected List<int> m_prvHitList;

        public override void Dispose()
        {
            if (m_triggeredBy != null)
            {
                m_triggeredBy.Clear();
                m_triggeredBy = null;
            }

            if (m_affectedEntities != null)
            {
                m_affectedEntities.Clear();
                m_affectedEntities = null;
            }

            if (m_hitList != null)
            {
                m_hitList.Clear();
                m_hitList = null;
            }

            if (m_prvHitList != null)
            {
                m_prvHitList.Clear();
                m_prvHitList = null;
            }

            if (m_event != null)
            {
                m_event.Dispose();
                m_event = null;
            }

            base.Dispose();
        }

#if EDITOR
        Vector3 m_scale;
        bool m_hideOnLoad = false;

        public CSphereTriggerEntity(Vector3 Position, Vector3 Rotation, string EntityName, CModel ModelData, List<CMaterial> MaterialList,
            List<string> TriggeredBy, int Life, List<string> EffectedEntities, CEvent Event, bool Hidden)
        {
            Initialise(Position, Rotation, EntityName, ModelData, MaterialList, TriggeredBy, Life, EffectedEntities, Event, Hidden);
        }

        public void Initialise(Vector3 Position, Vector3 Rotation, string EntityName, CModel ModelData, List<CMaterial> MaterialList,
            List<string> TriggeredBy, int Life, List<string> EffectedEntities, CEvent Event, bool Hidden)
        {
            m_isScalingAllowed = true;
            m_hideOnLoad = Hidden;

            m_modelData = ModelData;
            m_modelName = m_modelData.GetModelName();
            m_modelName = m_modelName.Replace("Content/", "");
            m_modelName = m_modelName.Replace("Content\\\\", "");
            m_modelName = m_modelName.Replace("Content\\", "");
            m_modelName = m_modelName.Replace("Model\\\\", "");
            m_modelName = m_modelName.Replace("Model\\", "");
            m_modelName = m_modelName.Replace("..\\", "");

            m_material = MaterialList;

            foreach (CMaterial mat in m_material)
            {
                mat.m_diffuse.W = 0.4f;
            }

            IsTranslucent(((CDotXModel)m_modelData).m_model);

            m_materialName = "";

            m_editorRotation = Rotation;
            FillPropertyList();

            m_scale = Vector3.One;

            m_event = Event;
            m_affectedEntities = EffectedEntities;
            m_entityName = EntityName;
            m_prvHitList = new List<int>();
            m_hitList = new List<int>();

            m_life = Life;

            m_triggeredBy = TriggeredBy;

            m_portalInfo = new SPortalInfo();

            m_rotation = Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z);

            m_isDynamic = true;
            m_position = Position;
            m_isCollidable = false;

            m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);

            // Transform local space OBB corners to world space
            InitialiseEntityOBB(m_modelData.GetOBB());

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            UpdateCollisionSphere();

            FillPropertyList();
        }
#else
        public CSphereTriggerEntity(Vector3 Position, Vector3 Rotation, string EntityName, Vector3 Scale, List<string> TriggeredBy, int Life,
            List<string> AffectedEntities, CEvent Event, bool Hidden)
        {
            Initialise(Position, Rotation, EntityName, Scale, TriggeredBy, Life, AffectedEntities, Event, Hidden);
        }

        public void Initialise(Vector3 Position, Vector3 Rotation, string EntityName, Vector3 Scale, List<string> TriggeredBy, int Life,
            List<string> AffectedEntities, CEvent Event, bool Hidden)
        {
            if( Hidden == true )
            {
                Hide();
            }

            m_event = Event;
            m_affectedEntities = AffectedEntities;
            m_entityName = EntityName;
            m_prvHitList = new List<int>();
            m_hitList = new List<int>();
            m_canBeParent = false;
            m_canBeStoodOn = false;

            m_life = Life;
            if (Life < 1)
            {
                m_infiniteLife = true;
            }
            else
            {
                m_infiniteLife = false;
            }

            m_triggeredBy = TriggeredBy;
            if (m_triggeredBy != null && m_triggeredBy.Count > 0)
            {
                m_triggeredByAll = false;
            }
            else
            {
                m_triggeredByAll = true;
            }

            m_portalInfo = new SPortalInfo();

            m_rotation = Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z);

            m_isDynamic = true;
            m_position = Position;
            m_isCollidable = false;

            m_worldMatrix = Matrix.CreateScale(Scale) * Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);

            // Transform local space OBB corners to world space
            InitialiseEntityOBB(CModel.CreateOBBofUnitSize(1.0f));

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            UpdateCollisionSphere();
        }

        public override void Reinitialise()
        {
            m_hidden = false;
            m_isCollidable = true;
        }

        public List<string> GetAffectedEntities()
        {
            return m_affectedEntities;
        }
#endif

        public override int GetEntityID()
        {
            return (int)EBasicEntityIDs.SphereTrigger;
        }

#if ENGINE
        public override void Update(float DT)
        {
            m_hitList = CCollision.Instance.GetNearDynamicEntityIDs(this, m_entityLeafNode, m_sphere, 4);

            List<int> orig_hit_list = m_hitList.ToList<int>();

            if (m_hitList.Count > 0)
            {
                for (int i = 0; i < m_prvHitList.Count; ++i)
                {
                    m_hitList.Remove(m_prvHitList[i]);
                }

                if (m_hitList.Count > 0)
                {
                    // Call event
                    if (m_event.RunEvent(m_triggeredBy, m_affectedEntities, m_hitList) == true)
                    {
                        --m_life;
                    }

                    // Check the life counter.
                    if (m_infiniteLife == false)
                    {
                        // If it's less than one then this trigger gets removed.
                        if (m_life < 1)
                        {
                            //m_event.AddEntityToDeleteList(this);
                            Hide();

                            m_prvHitList = orig_hit_list;

                            return;
                        }
                    }
                }
            }

            m_prvHitList = orig_hit_list;
        }

        public static void LoadEntityData(CEntityDataStore EntityData, Onderzoek.Screens.CGameScreenHelper CurScreen,
            ref Dictionary<string, CModel.SModelData> ModelList, ref ContentManager Content)
        {
            List<string> trig_list = new List<string>();
            int life = 1;
            List<string> affec_list = new List<string>();
            Vector3 scale = Vector3.One;
            bool hide_on_load = false;

            for (int i = 0; i < EntityData.m_properties.Count; ++i)
            {
                if (EntityData.m_properties[i].propertyName == "Triggered_By")
                {
                    trig_list = CToolbox.StringToStringList(EntityData.m_properties[i].propertyValue);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Num_Times_Triggered")
                {
                    CToolbox.StringToInt(EntityData.m_properties[i].propertyValue, ref life);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Affected_Entities")
                {
                    affec_list = CToolbox.StringToStringList(EntityData.m_properties[i].propertyValue);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Scale")
                {
                    CToolbox.StringToVector3(EntityData.m_properties[i].propertyValue, ref scale);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Hidden_on_load")
                {
                    CToolbox.StringToBool(EntityData.m_properties[i].propertyValue, ref hide_on_load);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
            }

            // Create the entity
            CSphereTriggerEntity ent = new CSphereTriggerEntity(EntityData.m_position, EntityData.m_rotation, EntityData.m_entityName, scale, trig_list, life,
                affec_list, new CEventReInit(CurScreen), hide_on_load);
            CurScreen.AddEntityNoSPT(ent);
        }
#else
        public override void Update()
        {
            m_worldMatrix = Matrix.CreateScale(m_scale) * Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);

            // Update the obbs
            UpdateEntityOBB(m_modelData.GetOBB());

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            // The following two lines will only needed to be updated if the bounding box size changes or if the dynamic object moves(translation) in the scene.

            // This needs to be updated for the calcualtion that will take place in the below base update.
            UpdateCollisionSphere();

            // this needs to be called so that the spatial partioning tree is updated and the it won't just disappear from the scene.
            m_entityLeafNode.MoveNodeAroundTree(m_position, m_maxLengthOfEntity);
        }

        public override Vector3 Scale
        {
            get { return m_scale; }
            set
            {
                m_scale.X = value.X;
                m_scale.Y = value.X;
                m_scale.Z = value.X;
            }
        }

        public bool TriggeredByAll
        {
            get { return m_triggeredByAll; }
            set { m_triggeredByAll = value; }
        }

        public List<string> TriggeredBy
        {
            get { return m_triggeredBy; }
            set { m_triggeredBy = value; }
        }

        public List<string> EffectedEntities
        {
            get { return m_affectedEntities; }
            set { m_affectedEntities = value; }
        }

        public int Life
        {
            get { return m_life; }
            set
            {
                m_life = value;
                if (m_life <= 0)
                {
                    m_infiniteLife = true;
                }
                else
                {
                    m_infiniteLife = false;
                }
            }
        }

        public override void writeEntityProperties(XmlWriter writer)
        {
            for (int i = 0; i < m_propertyList.Count; i++)
            {
                writer.WriteElementString(m_propertyList[i].propertyName, m_propertyList[i].propertyValue);
            }

            writer.WriteElementString("Scale", m_scale.ToString());
        }

        public override void readEntityProperties(XmlReader reader)
        {
            reader.ReadToFollowing("EntityData");
            while (!(reader.Name == "EntityData" && reader.NodeType == XmlNodeType.EndElement))
            {
                reader.Read();
                reader.Read();

                reader.MoveToContent();
                string propName = reader.Name;

                if (reader.Name == "EntityData" && reader.NodeType == XmlNodeType.EndElement)
                    break;
                string propVal;

                if (reader.IsEmptyElement)
                    propVal = "";
                else
                {
                    reader.Read();
                }

                propVal = reader.Value;

                if (propName.Equals("Scale"))
                {
                    CToolbox.StringToVector3(propVal, ref m_scale);
                }
                else
                {
                    for (int i = 0; i < m_propertyList.Count; i++)
                    {
                        if (m_propertyList[i].propertyName.Equals(propName))
                            m_propertyList[i].propertyValue = propVal;
                    }
                }
            }
        }

        public override void FillPropertyList()
        {
            this.m_propertyList = new List<CEntityProperty>();

            m_propertyList.Add(new CEntityProperty("Triggered_By", "", EPropertyGridInputType.TextBox));
            m_propertyList.Add(new CEntityProperty("Num_Times_Triggered", m_life.ToString(), EPropertyGridInputType.TextBox));
            m_propertyList.Add(new CEntityProperty("Affected_Entities", "", EPropertyGridInputType.TextBox));

            m_propertyList.Add(new CEntityProperty("Hidden_on_load", m_hideOnLoad.ToString(), EPropertyGridInputType.PullDownMultipleChoice));
            m_propertyList[m_propertyList.Count - 1].pullDownOptions = new List<string>();
            m_propertyList[m_propertyList.Count - 1].pullDownOptions.Add("True");
            m_propertyList[m_propertyList.Count - 1].pullDownOptions.Add("False");
        }

        public override void UpdateFromPropertyList()
        {
            m_triggeredBy = CToolbox.StringToStringList(m_propertyList[0].propertyValue);

            int integer = 0;
            if (CToolbox.StringToInt(m_propertyList[1].propertyValue, ref integer) == false)
            {
                m_propertyList[1].propertyValue = m_life.ToString();
            }
            else
            {
                if (integer < 0)
                {
                    integer = 0;
                }

                m_life = integer;
            }

            m_affectedEntities = CToolbox.StringToStringList(m_propertyList[2].propertyValue);

            bool bl = false;
            if (CToolbox.StringToBool(m_propertyList[3].propertyValue, ref bl) == false)
            {
                m_propertyList[3].propertyValue = m_hideOnLoad.ToString();
            }
            else
            {
                m_hideOnLoad = bl;
            }
        }

        public override void ForwardTranslucentRender(CCamera Camera)
        {
            CVisual.Instance.ForwardRender(m_modelData.getBaseModel(), m_material, m_worldMatrix, m_translucentParts);
        }
#endif
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using Onderzoek.Visual;
using Onderzoek.ModelData;
using Onderzoek.Camera;

namespace Onderzoek.Entity
{
    class CWireFrameEntity : CStaticEntity
    {
        public override void Dispose()
        {
            base.Dispose();
        }

        public CWireFrameEntity(Vector3 Position, Vector3 Rotation, string EntityName, CModel Model, bool IsCollidable)
        {
#if EDITOR
            m_editorRotation = Rotation;
#endif

            Initialise(Position, Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z), EntityName, Model, IsCollidable);
        }

        public void Initialise(Vector3 Position, Quaternion Rotation, string EntityName, CModel Model, bool IsCollidable)
        {
            m_portalInfo = new SPortalInfo();
            m_isDynamic = false;
            m_position = Position;
            m_isCollidable = IsCollidable;

            m_entityName = EntityName;

            m_modelData = Model;

            m_worldMatrix = Matrix.CreateFromQuaternion(Rotation) * Matrix.CreateTranslation(m_position);

            // Transform local space OBB corners to world space
            InitialiseEntityOBB(Model.GetOBB());

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            UpdateCollisionSphere();
        }

        public override void Render(CCamera Camera)
        {
            CVisual.Instance.DrawPortalBounds(Camera, m_wrldOBB, true);
        }

        public override void ForwardRender(CCamera Camera)
        {
            CVisual.Instance.DrawPortalBounds(Camera, m_wrldOBB, false);
        }

        public override void ForwardTranslucentRender(CCamera Camera)
        {
            CVisual.Instance.DrawPortalBounds(Camera, m_wrldOBB, false);
        }

#if EDITOR
        public override void Update()
        {
            base.Update();
        }
#endif
    }
}

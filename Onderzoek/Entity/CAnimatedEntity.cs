﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using Onderzoek.Visual;

using Onderzoek.ModelData;
using Onderzoek.Entity;

using XNAnimation;
using XNAnimation.Controllers;
using XNAnimation.Effects;
using Onderzoek.Camera;

namespace Onderzoek.Entity
{
    public class CAnimatedEntity : CDynamicEntity
    {
        protected SkinnedModel m_skinnedModel;
        protected AnimationController m_animationController;
        protected int m_activeAnimationClip;

        public override void Dispose()
        {
            base.Dispose();

            m_skinnedModel = null;
            m_animationController = null;
        }

        public CAnimatedEntity(Vector3 Position, Vector3 Rotation, string EntityName, CModel AnimatedModel, List<CMaterial> Material,
            bool IsCollidable)
        {
#if EDITOR
            m_editorRotation = Rotation;
#endif

            Initialise(Position, Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z), EntityName, AnimatedModel, Material, IsCollidable);
        }

        public void Initialise(Vector3 Position, Quaternion Rotation, string EntityName, CModel AnimatedModel, List<CMaterial> Material,
            bool IsCollidable)
        {
            base.Initalise();

            m_portalInfo = new SPortalInfo();
            m_material = Material;

            m_isSkinnedAnimation = true;
            m_isDynamic = true;
            m_modelData = AnimatedModel;
            m_isCollidable = IsCollidable;
            m_position = Position;
            m_rotation = Rotation;
            m_entityName = EntityName;

            CAnimatedModel temp_model = (CAnimatedModel)m_modelData;

            m_skinnedModel = temp_model.GetSkinnedModelData();
            m_animationController = temp_model.GetAnimationControllerData();
            m_animationController.StartClip(m_skinnedModel.AnimationClips.Values[m_activeAnimationClip]);
            m_animationController.Speed = 3.0f;
            m_activeAnimationClip = 0;

            m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);

            InitialiseEntityOBB(AnimatedModel.GetOBB());

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            IsTranslucent(m_skinnedModel.Model);

#if EDITOR
            m_modelName = m_modelData.GetModelName();
            FillPropertyList();
#endif
        }

#if EDITOR
        public override CModel ModelData
        {
            get
            {
                return base.ModelData;
            }
            set
            {
                base.ModelData = value;

                CAnimatedModel temp_model = (CAnimatedModel)m_modelData;
                m_skinnedModel = temp_model.GetSkinnedModelData();
                m_animationController = temp_model.GetAnimationControllerData();
                m_activeAnimationClip = 0;
                m_animationController.StartClip(m_skinnedModel.AnimationClips.Values[m_activeAnimationClip]);
                m_animationController.LoopEnabled = false;
                m_animationController.Speed = 1.0f;

                m_animationController.Update(new TimeSpan(), Matrix.Identity);
            }
        }

        public override void Update()
        {
            //TimeSpan time_span = DT.ElapsedGameTime;

            TimeSpan time_span = new TimeSpan(100000);

            //m_animationController.PlaybackMode = PlaybackMode.Forward;

            m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);
            // This will apply the parent matrix on to the entity if it's has a parent.
            // This MUST be called straight after creating the new world matrix!
            UpdateIfChild();

            List<CModelOBB> OBB = m_modelData.GetOBB();

            // Transform local space OBB corners to world space
            for (int i = 0; i < OBB.Count; ++i)
            {
                // Transform local space OBB corners to world space
                CToolbox.TransformLocalToWorld(OBB[0].m_OBB, out m_wrldOBB[0].m_wrldOBB, m_worldMatrix);
                //CToolbox.TransformLocalToWorld(OBB[0].m_OBB, out m_wrldOBB[0].m_child[0].m_wrldOBB, m_worldMatrix);
                //CToolbox.TransformLocalToWorld(OBB[0].m_child[0].m_OBB, out m_wrldOBB[0].m_child[0].m_wrldOBB, m_animationController.SkinnedBoneTransforms[3] * m_worldMatrix);
                //CToolbox.TransformLocalToWorld(OBB[0].m_child[1].m_OBB, out m_wrldOBB[0].m_child[1].m_wrldOBB, m_animationController.SkinnedBoneTransforms[2] * m_worldMatrix);
                //CToolbox.TransformLocalToWorld(OBB[0].m_child[2].m_OBB, out m_wrldOBB[0].m_child[2].m_wrldOBB, m_animationController.SkinnedBoneTransforms[5] * m_worldMatrix);
                //CToolbox.TransformLocalToWorld(OBB[0].m_child[3].m_OBB, out m_wrldOBB[0].m_child[3].m_wrldOBB, m_animationController.SkinnedBoneTransforms[8] * m_worldMatrix);
                //CToolbox.TransformLocalToWorld(OBB[0].m_child[4].m_OBB, out m_wrldOBB[0].m_child[4].m_wrldOBB, m_animationController.SkinnedBoneTransforms[16] * m_worldMatrix);
                //CToolbox.TransformLocalToWorld(OBB[0].m_child[5].m_OBB, out m_wrldOBB[0].m_child[5].m_wrldOBB, m_animationController.SkinnedBoneTransforms[17] * m_worldMatrix);
                //CToolbox.TransformLocalToWorld(OBB[0].m_child[6].m_OBB, out m_wrldOBB[0].m_child[6].m_wrldOBB, m_animationController.SkinnedBoneTransforms[12] * m_worldMatrix);
                //CToolbox.TransformLocalToWorld(OBB[0].m_child[7].m_OBB, out m_wrldOBB[0].m_child[7].m_wrldOBB, m_animationController.SkinnedBoneTransforms[13] * m_worldMatrix);

                // Create the Dynamic Entity's planes - needs to be here so that only the rotation is applied
                UpdatePlanes(m_rotation, i);
            }

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            // The following two lines will only needed to be updated if the bounding box size changes or if the dynamic object moves(translation) in the scene.

            // This needs to be updated for the calcualtion that will take place in the below base update.
            UpdateCollisionSphere();

            m_animationController.Update(time_span, Matrix.Identity);

            base.Update();
        }

        public override void FillPropertyList()
        {
            this.m_propertyList = new List<CEntityProperty>();

            CEntityProperty modelSelectionEntityProperty = new CEntityProperty("Model", m_modelName.ToString(), EPropertyGridInputType.ModelSelection);

            m_propertyList.Add(modelSelectionEntityProperty);

            string temp = m_modelName.Replace("Model/", "");
            temp = temp.Replace("Content/", "");
            temp = temp.Replace("Content\\\\", "");
            temp = temp.Replace("Content\\", "");
            temp = temp.Replace("AnimatedModel\\\\", "");
            temp = temp.Replace("AnimatedModel\\", "");
            temp = temp.Replace("..\\", "");
            m_modelName = temp;
            m_propertyList[m_propertyList.Count - 1].propertyValue = m_modelName;

            m_propertyList.Add(new CEntityProperty("Material", "", EPropertyGridInputType.MaterialSelection));

            m_propertyList.Add(new CEntityProperty("Collidable", m_isCollidable.ToString(), EPropertyGridInputType.PullDownMultipleChoice));
            m_propertyList[m_propertyList.Count - 1].pullDownOptions = new List<string>();
            m_propertyList[m_propertyList.Count - 1].pullDownOptions.Add("True");
            m_propertyList[m_propertyList.Count - 1].pullDownOptions.Add("False");

            base.FillPropertyList();
        }

        public override void UpdateFromPropertyList()
        {
            m_modelName = m_propertyList[0].propertyValue;
            m_materialName = m_propertyList[1].propertyValue;

            bool bl = false;
            if (CToolbox.StringToBool(m_propertyList[2].propertyValue, ref bl) == false)
            {
                m_propertyList[2].propertyValue = m_isCollidable.ToString();
            }
            else
            {
                m_isCollidable = bl;
            }
        }

        public override int GetEntityID()
        {
            return (int)EBasicEntityIDs.AnimatedEntity;
        }
#else
        public override void Update(float DT)
        {
            m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);
            // This will apply the parent matrix on to the entity if it's has a parent.
            // This MUST be called straight after creating the new world matrix!
            UpdateIfChild();

            List<CModelOBB> OBB = m_modelData.GetOBB();
            UpdateEntityOBB(OBB);

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            // The following two lines will only needed to be updated if the bounding box size changes or if the dynamic object moves(translation) in the scene.

            // This needs to be updated for the calcualtion that will take place in the below base update.
            UpdateCollisionSphere();

            m_animationController.Update(new TimeSpan(0,0,0,0,(int)(DT * 1000.0f)), Matrix.Identity);
        }

        public static void LoadEntityData(CEntityDataStore EntityData, Onderzoek.Screens.CGameScreenHelper CurScreen,
            ref Dictionary<string, CModel.SModelData> ModelList, ref ContentManager Content)
        {
            string model_name = "";
            string mat_name = "";
            bool collidable = true;
            bool can_stand = false;

            for (int i = 0; i < EntityData.m_properties.Count; ++i)
            {
                if (EntityData.m_properties[i].propertyName == "Model")
                {
                    model_name = "AnimatedModel\\" + EntityData.m_properties[i].propertyValue;
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Material")
                {
                    mat_name = EntityData.m_properties[i].propertyValue;
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Collidable")
                {
                    CToolbox.StringToBool(EntityData.m_properties[i].propertyValue, ref collidable);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Can_Be_stood_On")
                {
                    CToolbox.StringToBool(EntityData.m_properties[i].propertyValue, ref can_stand);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
            }

            CAnimatedModel anim_mod = new CAnimatedModel();
            anim_mod = (CAnimatedModel)anim_mod.Initialise(Content, model_name, ModelList);
            List<CMaterial> anim_mat = null;
            if (mat_name == "" || mat_name == null)
            {
                anim_mat = anim_mod.GetMaterials(Content, model_name);
            }
            else
            {
                CMaterialManager.loadMaterialXML(anim_mod, mat_name, ref anim_mat, ref Content);
            }


            // Create the model
            CAnimatedEntity ent = new CAnimatedEntity(EntityData.m_position, EntityData.m_rotation, EntityData.m_entityName, anim_mod, anim_mat, collidable);
            CurScreen.AddEntityNoSPT(ent);
        }
#endif

        public override void SetMaterialList(List<CMaterial> MaterialList)
        {
            base.SetMaterialList(MaterialList);

            IsTranslucent(((CAnimatedModel)m_modelData).GetSkinnedModelData().Model);
        }

        public override void Render(CCamera Camera)
        {
            CVisual.Instance.Render(m_skinnedModel.Model, m_animationController, Camera, m_material, m_worldMatrix, m_opaqueParts);
        }

        public override void ForwardRender(CCamera Camera)
        {
            CVisual.Instance.ForwardRender(m_skinnedModel.Model, m_animationController, m_material, m_worldMatrix, m_opaqueParts);
        }

        public override void ForwardTranslucentRender(CCamera Camera)
        {
            CVisual.Instance.ForwardRender(m_skinnedModel.Model, m_animationController, m_material, m_worldMatrix, m_translucentParts);
        }

        public override void RenderForShadow(CCamera Camera)
        {
            CVisual.Instance.RenderForShadow(m_skinnedModel, m_animationController, Camera, m_worldMatrix);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using Onderzoek.Visual;
using Onderzoek.ModelData;
using Onderzoek.Physics;
using Onderzoek.Camera;

namespace Onderzoek.Entity
{
    public class CTerrainEntity : CStaticEntity
    {
        public struct SHeightMapData
        {
            public void Initialise()
            {
                s_surroundingTileIDs = new List<int>();
            }

            public int s_curCollisionTerrainID;
            public List<int> s_surroundingTileIDs;
        }

        protected Vector3[] m_splitTerrainMin;
        protected Vector3[] m_splitTerrainMax;
        protected Vector3[][] m_arrayWrldOBB;
        protected List<int> m_splitTerrainParts;
        protected List<int> m_splitTerrainPartsGI;
        protected bool m_atStartOfFrame = false;

        public override void Dispose()
        {
            // Remove the terrain instance from the collision engine.
            if (CCollision.Instance != null)
            {
                CCollision.Instance.RemoveTerrain(this);
            }

            m_splitTerrainMin = null;
            m_splitTerrainMax = null;
            m_arrayWrldOBB = null;
            if (m_splitTerrainParts != null)
            {
                m_splitTerrainParts.Clear();
                m_splitTerrainParts = null;
            }
            if (m_splitTerrainPartsGI != null)
            {
                m_splitTerrainPartsGI.Clear();
                m_splitTerrainPartsGI = null;
            }

            base.Dispose();
        }

        /// <summary>
        /// Default constructor. Only to be used when you want control over what is initialised.
        /// </summary>
        public CTerrainEntity()
        {
        }

        public CTerrainEntity(Vector3 Position, Vector3 Rotation, string EntityName, CModel Model, List<CMaterial> MaterialList)
        {
#if EDITOR
            m_editorRotation = Rotation;
#endif

            Initialise(Position, Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z), EntityName, Model, MaterialList);
        }

        public void Initialise(Vector3 Position, Quaternion Rotation, string EntityName, CModel Model, List<CMaterial> MaterialList)
        {
            if (CCollision.Instance != null)
            {
                CCollision.Instance.RegisterTerrainIDForCollision(this);
            }

            m_portalInfo = new SPortalInfo();
            m_material = MaterialList;
            m_material[0].m_effect = CVisual.Instance.GetShaderID(m_material[0]);

            m_entityName = EntityName;

            m_rotation = Rotation;


            m_isTerrain = true;
            m_isCollidable = false;
            m_isDynamic = false;
            m_position = Position;
            m_modelData = Model;

            m_worldMatrix = Matrix.CreateFromQuaternion(Rotation) * Matrix.CreateTranslation(m_position);

            // Transform local space OBB corners to world space
            InitialiseEntityOBB(m_modelData.GetOBB());

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            CTerrainModel temp_model = (CTerrainModel)m_modelData;

            BoundingBox[] array_obb = temp_model.GetArrayOfOBB();

            m_splitTerrainMin = new Vector3[array_obb.Length];
            m_splitTerrainMax = new Vector3[array_obb.Length];
            m_arrayWrldOBB = new Vector3[array_obb.Length][];

            for (int i = 0; i < array_obb.Length; ++i)
            {
                Matrix temp_wrld_matrix = Matrix.CreateFromQuaternion(Rotation) * Matrix.CreateTranslation(m_position);

                // Trasform local space ArrayOBB corners to world space
                CToolbox.TransformLocalToWorld(array_obb[i].GetCorners(), out m_arrayWrldOBB[i], temp_wrld_matrix);

                CToolbox.GetMinMax(m_arrayWrldOBB[i], out m_splitTerrainMin[i], out m_splitTerrainMax[i]);
            }

            m_splitTerrainParts = new List<int>();
            m_splitTerrainPartsGI = new List<int>();

            // This is done so that when the height is needed to be found, it doesn't need to multiply by the
            // world matrix then.
            temp_model.SetVerticesForHeightFindToWorld(m_worldMatrix);

#if EDITOR
            m_modelName = Model.GetModelName();
            FillPropertyList();
#endif
        }

        public void RegisterTerrainIDForCollision()
        {
            if (CCollision.Instance != null)
            {
                CCollision.Instance.RegisterTerrainIDForCollision(this);
            }
        }

        public override void SetMaterialList(List<CMaterial> MaterialList)
        {
            base.SetMaterialList(MaterialList);

            if (m_material[0].m_diffuse.W < 1.0f)
            {
                m_wholeTranslucent = true;
            }
        }

        public override void Render(CCamera Camera)
        {
            //// CheckWithCameraPlanes to see if the user has selected to render all objects, regardless of whether it passes/fails the
            //// view frustum test.
            //if (InputData.m_renderAll == true)
            //{
            //    m_splitTerrainParts.RemoveRange(0, m_splitTerrainParts.Count);
            //    for (int i = 0; i < m_arrayWrldOBB.Length; ++i)
            //    {
            //        m_splitTerrainParts.Add(i);
            //    }
            //}

#if EDITOR
            if (m_splitTerrainParts.Count > 0)
            {
                CTerrainModel temp_model = (CTerrainModel)m_modelData;
                temp_model.JoinSplitIndexBuffer(m_splitTerrainParts);
                CVisual.Instance.Render(temp_model, Camera, m_material[0], m_worldMatrix);
            }

#else

            if (m_splitTerrainParts.Count > 0)
            {
                CTerrainModel temp_model = (CTerrainModel)m_modelData;
                temp_model.JoinSplitIndexBuffer(m_splitTerrainParts);
                CVisual.Instance.Render(temp_model, Camera, m_material[0], m_worldMatrix);
            }
#endif

        }

        public override void ForwardRender(CCamera Camera)
        {
            if (m_splitTerrainParts.Count > 0)
            {
                CVisual.Instance.ForwardRender((CTerrainModel)m_modelData, m_material[0], m_worldMatrix);
            }
        }

        public override void ForwardTranslucentRender(CCamera Camera)
        {
            if (m_splitTerrainParts.Count > 0)
            {
                CVisual.Instance.ForwardRender((CTerrainModel)m_modelData, m_material[0], m_worldMatrix);
            }
        }

        public override void RenderForShadow(CCamera Camera)
        {
            if (m_splitTerrainParts.Count > 0)
            {
                CVisual.Instance.RenderForShadow((CTerrainModel)m_modelData, m_worldMatrix);
            }
        }

        public override bool CheckAgainstClipPlanes(Plane[] ClipPlanes)
        {
#if EDITOR
            // The editor will render all of the terrain parts - this simplifies things a bit.
            m_splitTerrainParts.RemoveRange(0, m_splitTerrainParts.Count);
            for (int i = 0; i < m_arrayWrldOBB.Length; ++i)
            {
                m_splitTerrainParts.Add(i);
            }

            return true;
#else
            if (m_atStartOfFrame == false)
            {
                return true;
            }

            m_splitTerrainParts.RemoveRange(0, m_splitTerrainParts.Count);
            if (CToolbox.ClipWithPlanes(m_min, m_max, ClipPlanes) == true)
            {
                for (int i = 0; i < m_arrayWrldOBB.Length; ++i)
                {
                    if (CToolbox.ClipWithPlanes(m_splitTerrainMin[i], m_splitTerrainMax[i], ClipPlanes) == true)
                    {
                        m_splitTerrainParts.Add(i);
                    }
                }
                if (m_splitTerrainParts.Count == 0)
                {
                    return false;
                }

                return true;
            }
            else
            {
                return false;
            }
#endif
        }

        // This is used when there are more than one portals or even when there is one portal.
        // This is called when the camera is inside and looking outwards.
        public override bool CheckAgainstClipPlanes(List<Plane[]> ClipPlanes)
        {
#if EDITOR
            // The editor will render all of the terrain parts - this simplifies things a bit.
            m_splitTerrainParts.RemoveRange(0, m_splitTerrainParts.Count);
            for (int i = 0; i < m_arrayWrldOBB.Length; ++i)
            {
                m_splitTerrainParts.Add(i);
            }

            return true;
#else
            if (m_atStartOfFrame == false)
            {
                return true;
            }

            m_splitTerrainParts.RemoveRange(0, m_splitTerrainParts.Count);
            //List<Plane[]> temp = new List<Plane[]>();
            //for (int j = 0; j < ClipPlanes.Count; ++j)
            //{
            //    if (CToolbox.ClipWithPlanes(m_min, m_max, ClipPlanes[j]) == true)
            //    {
            //        temp.Add(ClipPlanes[j]);
            //    }
            //}

            if (/*temp*/ClipPlanes.Count > 0)
            {
                for (int i = 0; i < m_arrayWrldOBB.Length; ++i)
                {
                    for (int j = 0; j < /*temp*/ClipPlanes.Count; ++j)
                    {
                        if (CToolbox.ClipWithPlanes(m_splitTerrainMin[i], m_splitTerrainMax[i], ClipPlanes[j]) == true)
                        {
                            m_splitTerrainParts.Add(i);
                            j = /*temp*/ClipPlanes.Count;
                        }
                    }
                }
            }
            else
            {
                return false;
            }

            if (m_splitTerrainParts.Count == 0)
            {
                return false;
            }

            return true;
#endif
        }

        //public override Vector3[][] GetWorldOBB()
        //{
        //    return m_arrayWrldOBB;
        //}

        public virtual bool GetHeightOfCurrentPos( Vector3 Position, ref SHeightMapData HMData, Vector3 RayDir, ref float Dist )
        {
            if (CToolbox.IsPointInXZMinMaxOfBoundingBox(m_min, m_max, Position) == false)
            {
                return false;
            }

            return RayVsTerrain(Position, ref HMData, RayDir, ref Dist);
        }

        public virtual bool RayVsTerrain(Vector3 Position, ref SHeightMapData HMData, Vector3 RayDir, ref float Dist)
        {
            // Check through the surronding tiles and the previous tile.
            for (int i = 0; i < HMData.s_surroundingTileIDs.Count; ++i)
            {
                if (CCollision.RayVsAABB(RayDir, Position, m_splitTerrainMin[HMData.s_surroundingTileIDs[i]], m_splitTerrainMax[HMData.s_surroundingTileIDs[i]]) == true)
                {
                    CTerrainModel temp_model = (CTerrainModel)m_modelData;
                    if (temp_model.FindHeightInSplitTerrain(HMData.s_surroundingTileIDs[i], RayDir, Position, ref Dist) == true)
                    {
                        FindSurroundingTiles(temp_model.GetSplitX(), temp_model.GetSplitY(), HMData.s_surroundingTileIDs[i], HMData);
                        return true;
                    }
                }
            }

            // If the above check doesn't work then check through the whole list.
            for (int i = 0; i < m_arrayWrldOBB.Length; ++i)
            {
                if (CCollision.RayVsAABB(RayDir, Position, m_splitTerrainMin[i], m_splitTerrainMax[i]) == true)
                {
                    CTerrainModel temp_model = (CTerrainModel)m_modelData;
                    if (temp_model.FindHeightInSplitTerrain(i, RayDir, Position, ref Dist) == true)
                    {
                        FindSurroundingTiles(temp_model.GetSplitX(), temp_model.GetSplitY(), i, HMData);
                        return true;
                    }
                }
            }

            // Otherwise send the default value of 10. Should only happen when ray is not above the terrain
            // or too far up.
            return false;
        }

        private void FindSurroundingTiles( int SplitX, int SplitY, int CurTileID, SHeightMapData HMData )
        {
            HMData.s_surroundingTileIDs.Clear();
            HMData.s_surroundingTileIDs.Add(CurTileID);

            int y_pos = CurTileID / SplitY;
            int x_pos = CurTileID % SplitY;

            if (x_pos == 0 && y_pos == 0)
            {
                HMData.s_surroundingTileIDs.Add(CurTileID + 1);
                HMData.s_surroundingTileIDs.Add(CurTileID + SplitX);
                HMData.s_surroundingTileIDs.Add(CurTileID + SplitX + 1);
            }
            // Getting a strange error here. Need to relook into this algorithm.
            else if (x_pos == 0 && y_pos == (SplitY - 1))
            {
                HMData.s_surroundingTileIDs.Add(CurTileID + 1);
                HMData.s_surroundingTileIDs.Add(CurTileID + SplitX);
                HMData.s_surroundingTileIDs.Add(CurTileID + SplitX + 1);
                HMData.s_surroundingTileIDs.Add(CurTileID - SplitX);
                HMData.s_surroundingTileIDs.Add(CurTileID - SplitX + 1);

                // A hack to get around the crash.
                int max_tiles = (SplitY * SplitX) - 1;
                for (int i = 0; i < HMData.s_surroundingTileIDs.Count; ++i)
                {
                    if (HMData.s_surroundingTileIDs[i] > max_tiles)
                    {
                        HMData.s_surroundingTileIDs.RemoveAt(i);
                        --i;
                    }
                }
            }
            else if (x_pos == (SplitX - 1) && y_pos == (SplitY - 1))
            {
                HMData.s_surroundingTileIDs.Add(CurTileID - 1);
                HMData.s_surroundingTileIDs.Add(CurTileID - SplitX);
                HMData.s_surroundingTileIDs.Add(CurTileID - SplitX - 1);
            }
            else if (x_pos == 0 && y_pos != 0)
            {
                HMData.s_surroundingTileIDs.Add(CurTileID + 1);
                HMData.s_surroundingTileIDs.Add(CurTileID + SplitX);
                HMData.s_surroundingTileIDs.Add(CurTileID + SplitX + 1);
                HMData.s_surroundingTileIDs.Add(CurTileID - SplitX);
                HMData.s_surroundingTileIDs.Add(CurTileID - SplitX + 1);
            }
            else if (x_pos != 0 && y_pos == 0)
            {
                HMData.s_surroundingTileIDs.Add(CurTileID + 1);
                HMData.s_surroundingTileIDs.Add(CurTileID - 1);
                HMData.s_surroundingTileIDs.Add(CurTileID + SplitX);
                HMData.s_surroundingTileIDs.Add(CurTileID + SplitX + 1);
                HMData.s_surroundingTileIDs.Add(CurTileID + SplitX - 1);
            }
            else if (x_pos == (SplitX - 1) && y_pos != 0)
            {
                HMData.s_surroundingTileIDs.Add(CurTileID - 1);
                HMData.s_surroundingTileIDs.Add(CurTileID + SplitX);
                HMData.s_surroundingTileIDs.Add(CurTileID + SplitX - 1);
                HMData.s_surroundingTileIDs.Add(CurTileID - SplitX);
                HMData.s_surroundingTileIDs.Add(CurTileID - SplitX - 1);
            }
            else if (x_pos != 0 && y_pos == (SplitY - 1))
            {
                HMData.s_surroundingTileIDs.Add(CurTileID + 1);
                HMData.s_surroundingTileIDs.Add(CurTileID - 1);
                HMData.s_surroundingTileIDs.Add(CurTileID - SplitX);
                HMData.s_surroundingTileIDs.Add(CurTileID - SplitX - 1);
                HMData.s_surroundingTileIDs.Add(CurTileID - SplitX + 1);
            }
            else
            {
                HMData.s_surroundingTileIDs.Add(CurTileID + 1);
                HMData.s_surroundingTileIDs.Add(CurTileID - 1);
                HMData.s_surroundingTileIDs.Add(CurTileID - SplitX);
                HMData.s_surroundingTileIDs.Add(CurTileID - SplitX + 1);
                HMData.s_surroundingTileIDs.Add(CurTileID - SplitX - 1);
                HMData.s_surroundingTileIDs.Add(CurTileID + SplitX);
                HMData.s_surroundingTileIDs.Add(CurTileID + SplitX + 1);
                HMData.s_surroundingTileIDs.Add(CurTileID + SplitX - 1);
            }
        }

        public override void SetRender(bool Render, bool AtStartOfFrame)
        {
            m_atStartOfFrame = AtStartOfFrame;
            m_render = Render;
        }

#if EDITOR
        public override void FillPropertyList()
        {
            this.m_propertyList = new List<CEntityProperty>();

            CEntityProperty modelSelectionEntityProperty = new CEntityProperty("Model", "", EPropertyGridInputType.ModelSelection);
            modelSelectionEntityProperty.propertyValue = m_modelName;

            m_propertyList.Add(modelSelectionEntityProperty);

            //base.FillPropertyList();
        }

        public override void UpdateFromPropertyList()
        {
            for (int i = 0; i < m_propertyList.Count; i++)
                if (m_propertyList[i].propertyName.Equals("Model"))
                {
                    m_modelName = m_propertyList[i].propertyValue;
                }

            //base.UpdateFromPropertyList();
        }

        public override int GetEntityID()
        {
            return (int)EBasicEntityIDs.TerrainEntity;

            //return base.GetEntityID();
        }

        public override void Update()
        {
            base.Update();
        }
#else
        public static void LoadEntityData(CEntityDataStore EntityData, Onderzoek.Screens.CGameScreenHelper CurScreen,
            ref Dictionary<string, CModel.SModelData> ModelList, ref ContentManager Content)
        {
            string terrain_fn = "";

            for (int i = 0; i < EntityData.m_properties.Count; ++i)
            {
                if (EntityData.m_properties[i].propertyName == "Model")
                {
                    terrain_fn = "Content/" + EntityData.m_properties[i].propertyValue;
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
            }

            XmlReader terrain_data = new XmlTextReader(terrain_fn);

            terrain_data.Read();
            terrain_data.Read();
            terrain_data.Read();
            terrain_data.Read();

            string tex_name = terrain_data.Value;
            tex_name = tex_name.Replace("Content\\", "");
            tex_name = tex_name.Replace(".xnb", "");
            Texture2D heightmap_tex = Content.Load<Texture2D>(tex_name);

            terrain_data.Read();
            terrain_data.Read();
            terrain_data.Read();
            terrain_data.Read();

            int isplit = 1;
            CToolbox.StringToInt(terrain_data.Value, ref isplit);
            Vector2 split = new Vector2((float)isplit, (float)isplit);

            terrain_data.Read();
            terrain_data.Read();
            terrain_data.Read();
            terrain_data.Read();

            float flength = 100.0f;
            CToolbox.StringToFloat(terrain_data.Value, ref flength);
            Vector2 length = new Vector2(flength, flength);

            terrain_data.Read();
            terrain_data.Read();
            terrain_data.Read();
            terrain_data.Read();

            int inum_verts = 100;
            CToolbox.StringToInt(terrain_data.Value, ref inum_verts);
            Vector2 num_vert = new Vector2((float)inum_verts, (float)inum_verts);

            terrain_data.Read();
            terrain_data.Read();
            terrain_data.Read();
            terrain_data.Read();

            uint uheight = 10;
            CToolbox.StringToUInt(terrain_data.Value, ref uheight);

            terrain_data.Close();
            terrain_data = null;

            CTerrainModel temp_terrain = new CTerrainModel();
            temp_terrain.LoadAndCreateTerrain(heightmap_tex, CVisual.Instance.GetGraphicsDevice(), length, num_vert, uheight, split, ModelList, EntityData.m_entityName);

            heightmap_tex.Dispose();

            // Create a terrain entity - Make a get material for terrain models too!
            CTerrainEntity temp_terrain_ent = new CTerrainEntity(EntityData.m_position, EntityData.m_rotation, EntityData.m_entityName, temp_terrain,
                temp_terrain.GetMaterials(Content, EntityData.m_entityName));
            CurScreen.AddEntityNoSPT(temp_terrain_ent);
        }
#endif
    }
}

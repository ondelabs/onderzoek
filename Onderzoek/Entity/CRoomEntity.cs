﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

using Onderzoek.Visual;
using Onderzoek.ModelData;
using Onderzoek.Camera;

namespace Onderzoek.Entity
{
    public class CRoomEntity : CStaticEntity
    {
        protected bool m_initialCollidableValue;
        protected Plane[] m_roomModelPlanes;
        protected BoundingBox m_roomModelBB;

        public override void Dispose()
        {
            base.Dispose();
        }

        public CRoomEntity(Vector3 Position, Vector3 Rotation, string EntityName, CModel Model, List<CMaterial> Material, bool IsCollidable)
        {
#if EDITOR
            m_editorRotation = Rotation;
#endif

            Initialise(Position, Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z), EntityName, Model, Material, IsCollidable);
        }

        public void Initialise(Vector3 Position, Quaternion Rotation, string EntityName, CModel Model, List<CMaterial> Material, bool IsCollidable)
        {
            m_portalInfo = new SPortalInfo();
            m_portalInfo.s_isRoom = true;
            m_material = Material;

            m_entityName = EntityName;
            m_isDynamic = false;
            m_position = Position;
            m_modelData = Model;
            m_isCollidable = IsCollidable;
            m_initialCollidableValue = IsCollidable;

            m_worldMatrix = Matrix.CreateFromQuaternion(Rotation) * Matrix.CreateTranslation(m_position);

            // Transform local space OBB corners to world space
            InitialiseEntityOBB(m_modelData.GetOBB());

            // Getting the room OBB from the room model.
            CModelOBB room_model_obb = m_modelData.GetRoomOBB();
            Vector3[] room_wrld_obb = new Vector3[8];
            CToolbox.TransformLocalToWorld(room_model_obb.m_OBB, out room_wrld_obb, m_worldMatrix);

            m_roomModelBB = BoundingBox.CreateFromPoints(room_wrld_obb);

            // Creating the planes for the room model, which will be used in the portal system.
            CEntityOBB.CreatePlanes(Matrix.CreateFromQuaternion(Rotation), room_wrld_obb, ref m_roomModelPlanes);

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            UpdateCollisionSphere();

            IsTranslucent(((CDotXModel)m_modelData).m_model);

#if EDITOR
            m_modelName = m_modelData.GetModelName();
            FillPropertyList();
#endif
        }

        public override void Reinitialise()
        {
            m_isCollidable = m_initialCollidableValue;
            m_hidden = false;
        }

        public Plane[] GetRoomBBPlanes()
        {
            return m_roomModelPlanes;
        }

        public void GetRoomMinMax(out Vector3 Min, out Vector3 Max)
        {
            Min = m_roomModelBB.Min;
            Max = m_roomModelBB.Max;
        }

        public override void SetMaterialList(List<CMaterial> MaterialList)
        {
            base.SetMaterialList(MaterialList);

            IsTranslucent(((CDotXModel)m_modelData).m_model);
        }

        public override void Render(CCamera Camera)
        {
            CVisual.Instance.Render(((CDotXModel)m_modelData).m_model, Camera, m_material, m_worldMatrix, m_opaqueParts);
        }

        public override void ForwardRender(CCamera Camera)
        {
            CVisual.Instance.ForwardRender(((CDotXModel)m_modelData).m_model, m_material, m_worldMatrix, m_opaqueParts);
        }

        public override void ForwardTranslucentRender(CCamera Camera)
        {
            CVisual.Instance.ForwardRender(((CDotXModel)m_modelData).m_model, m_material, m_worldMatrix, m_translucentParts);
        }

        public override void RenderForShadow(CCamera Camera)
        {
            CVisual.Instance.RenderForShadow(((CDotXModel)m_modelData).m_model, m_worldMatrix);
        }

        public override int GetEntityID()
        {
            return (int)EBasicEntityIDs.RoomEntity;
            //return base.GetEntityID();
        }

#if EDITOR
        public override void FillPropertyList()
        {
            m_propertyList = new List<CEntityProperty>();

            CEntityProperty modelSelectionEntityProperty = new CEntityProperty("Model", m_modelName.ToString(), EPropertyGridInputType.ModelSelection);

            m_propertyList.Add(modelSelectionEntityProperty);

            string temp = m_modelName.Replace("Model/", "");
            temp = temp.Replace("Content/", "");
            temp = temp.Replace("Content\\\\", "");
            temp = temp.Replace("Content\\", "");
            temp = temp.Replace("Model\\\\", "");
            temp = temp.Replace("Model\\", "");
            temp = temp.Replace("..\\", "");
            m_modelName = temp;
            m_propertyList[m_propertyList.Count - 1].propertyValue = m_modelName;

            m_propertyList.Add(new CEntityProperty("Material", "", EPropertyGridInputType.MaterialSelection));

            m_propertyList.Add(new CEntityProperty("Collidable", m_isCollidable.ToString(), EPropertyGridInputType.PullDownMultipleChoice));
            m_propertyList[m_propertyList.Count - 1].pullDownOptions = new List<string>();
            m_propertyList[m_propertyList.Count - 1].pullDownOptions.Add("True");
            m_propertyList[m_propertyList.Count - 1].pullDownOptions.Add("False");
        }

        public override void UpdateFromPropertyList()
        {
            m_modelName = m_propertyList[0].propertyValue;
            m_materialName = m_propertyList[1].propertyValue;

            bool bl = false;
            if (CToolbox.StringToBool(m_propertyList[2].propertyValue, ref bl) == false)
            {
                m_propertyList[2].propertyValue = m_isCollidable.ToString();
            }
            else
            {
                m_isCollidable = bl;
            }
        }

        public override void Update()
        {
            base.Update();
        }
#else
        public static void LoadEntityData(CEntityDataStore EntityData, Onderzoek.Screens.CGameScreenHelper CurScreen,
            ref Dictionary<string, CModel.SModelData> ModelList, ref ContentManager Content)
        {
            string model_name = "";
            string material = "";
            bool collidable = true;

            for (int i = 0; i < EntityData.m_properties.Count; ++i)
            {
                if (EntityData.m_properties[i].propertyName == "Model")
                {
                    model_name = "Model\\" + EntityData.m_properties[i].propertyValue;
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Material")
                {
                    material = EntityData.m_properties[i].propertyValue;
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Collidable")
                {
                    CToolbox.StringToBool(EntityData.m_properties[i].propertyValue, ref collidable);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
            }

            CDotXModel dotx_mod = new CDotXModel();
            dotx_mod = (CDotXModel)dotx_mod.Initialise(Content, model_name, ModelList);
            List<CMaterial> dotx_mat = null;
            if (material == "" || material == null)
            {
                dotx_mat = dotx_mod.GetMaterials(Content, model_name);
            }
            else
            {
                CMaterialManager.loadMaterialXML(dotx_mod, material, ref dotx_mat, ref Content);
            }

            // Create the model
            CRoomEntity ent = new CRoomEntity(EntityData.m_position, EntityData.m_rotation, EntityData.m_entityName, dotx_mod, dotx_mat, collidable);
            CurScreen.AddEntityNoSPT(ent);
        }
#endif
    }
}

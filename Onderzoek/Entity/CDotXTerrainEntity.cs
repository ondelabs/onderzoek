﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

using Onderzoek.Entity;
using Onderzoek.Visual;
using Onderzoek.ModelData;
using Onderzoek.Physics;
using Onderzoek.Camera;

namespace Onderzoek.Entity
{
    public class CDotXTerrainEntity : CTerrainEntity
    {
        List<short>[] m_indexBuffer;
        List<Vector3>[] m_positionBuffer;

        public CDotXTerrainEntity(Vector3 Position, Vector3 Rotation, string EntityName, CModel Model, List<CMaterial> Material)
            : base()
        {
#if EDITOR
            m_materialName = "";
            m_modelName = Model.GetModelName();
            m_editorRotation = Rotation;
#endif

            if (CCollision.Instance != null)
            {
                CCollision.Instance.RegisterTerrainIDForCollision(this);
            }

            m_portalInfo = new SPortalInfo();
            m_material = Material;

            m_entityName = EntityName;

            m_isTerrain = true;
            m_isCollidable = false;
            m_isDynamic = false;
            m_position = Position;
            m_modelData = Model;
            m_canBeParent = false;

            m_worldMatrix = Matrix.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z) * Matrix.CreateTranslation(m_position);

            // Transform local space OBB corners to world space
            InitialiseEntityOBB(m_modelData.GetOBB());

            // Get the min and max world positions
            Onderzoek.CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            IsTranslucent(((CDotXModel)m_modelData).m_model);


            // We need to retrieve the index and vertex buffer from the model so that we can do height detection calculations later on.
            Microsoft.Xna.Framework.Graphics.Model model_data = m_modelData.getBaseModel();

            //m_indexBuffer = new List<short>[model_data.Meshes.Count];
            List<List<short>> index_buffer = new List<List<short>>();
            //m_positionBuffer = new List<Vector3>[model_data.Meshes.Count];
            List<List<Vector3>> pos_buffer = new List<List<Vector3>>();

            Matrix[] transforms = new Matrix[model_data.Bones.Count];
            model_data.CopyAbsoluteBoneTransformsTo(transforms);

            int i = 0, j = 0;
            foreach (Microsoft.Xna.Framework.Graphics.ModelMesh mesh in model_data.Meshes)
            {
                int prv_vert_num = 0;
                Matrix new_world_matrix = transforms[mesh.ParentBone.Index] * m_worldMatrix;

                // Only if the index data is made up of indices which are two bytes long.
                if (mesh.IndexBuffer.IndexElementSize == Microsoft.Xna.Framework.Graphics.IndexElementSize.SixteenBits)
                {
                    // Loop through all the parts.
                    foreach (Microsoft.Xna.Framework.Graphics.ModelMeshPart part in mesh.MeshParts)
                    {
                        index_buffer.Add(new List<short>());
                        pos_buffer.Add(new List<Vector3>());

                        // Temporary storage for the index data from the mesh.
                        short[] indices = new short[mesh.IndexBuffer.SizeInBytes / 2];

                        // Get the index data from the mesh
                        mesh.IndexBuffer.GetData<short>(indices);

                        int num_indices = part.PrimitiveCount * 3;

                        // Store the indices into the list.
                        index_buffer[i].AddRange(indices);

                        // remove the ecess indices.
                        if (part.StartIndex == 0)
                        {
                            index_buffer[i].RemoveRange(part.StartIndex + num_indices, indices.Length - num_indices);
                        }
                        else
                        {
                            index_buffer[i].RemoveRange(0, part.StartIndex);
                            index_buffer[i].RemoveRange(num_indices, index_buffer[i].Count - num_indices);
                        }


                        Vector3[] vertexs = null;
                        switch (part.VertexStride)
                        {
                            // For a part made up of VertexPositionNormal vertices.
                            case 24:
                                {
                                    // Define a vertex array which will temporarily store the vertex data from the part.
                                    Onderzoek.VertexPositionNormal[] vertices = new Onderzoek.VertexPositionNormal[mesh.VertexBuffer.SizeInBytes / part.VertexStride];

                                    // Retrieve the index and vertex data from the part.
                                    mesh.VertexBuffer.GetData<Onderzoek.VertexPositionNormal>(vertices);

                                    // Now we need to retrieve the positions of this curret part.
                                    vertexs = new Vector3[part.NumVertices];
                                    for (int index = 0; index < index_buffer[i].Count; ++index)
                                    {
                                        vertexs[index_buffer[i][index]] = Vector3.Transform(vertices[index_buffer[i][index] + prv_vert_num].GetPosition(), new_world_matrix);
                                    }

                                    // Store the position data
                                    pos_buffer[j].AddRange(vertexs);
                                    prv_vert_num = part.NumVertices;

                                    break;
                                }
                            // For a part made up of VertexPositionNormalTexture vertices.
                            case 32:
                                {
                                    // Define a vertex array which will temporarily store the vertex data from the part.
                                    Onderzoek.VertexPositionNormalTexture[] vertices = new Onderzoek.VertexPositionNormalTexture[mesh.VertexBuffer.SizeInBytes / part.VertexStride];

                                    // Retrieve the index and vertex data from the part.
                                    mesh.VertexBuffer.GetData<Onderzoek.VertexPositionNormalTexture>(vertices);

                                    // Now we need to retrieve the positions of this current part.
                                    vertexs = new Vector3[part.NumVertices];
                                    for (int index = 0; index < index_buffer[i].Count; ++index)
                                    {
                                        vertexs[index_buffer[i][index]] = Vector3.Transform(vertices[index_buffer[i][index] + prv_vert_num].GetPosition(), new_world_matrix);
                                    }

                                    // Store the position data
                                    pos_buffer[j].AddRange(vertexs);
                                    prv_vert_num = part.NumVertices;

                                    break;
                                }
                            case 56:
                                {
                                    // Define a vertex array which will temporarily store the vertex data from the part.
                                    Onderzoek.VertexPositionNormalTextureTangentBinormal[] vertices =
                                        new Onderzoek.VertexPositionNormalTextureTangentBinormal[mesh.VertexBuffer.SizeInBytes / part.VertexStride];

                                    // Retrieve the index and vertex data from the part.
                                    mesh.VertexBuffer.GetData<Onderzoek.VertexPositionNormalTextureTangentBinormal>(vertices);

                                    // Now we need to retrieve the positions of this curret part.
                                    vertexs = new Vector3[part.NumVertices];
                                    for (int index = 0; index < index_buffer[i].Count; ++index)
                                    {
                                        vertexs[index_buffer[i][index]] = Vector3.Transform(vertices[index_buffer[i][index] + prv_vert_num].GetPosition(), new_world_matrix);
                                    }

                                    // Store the position data
                                    pos_buffer[j].AddRange(vertexs);
                                    prv_vert_num = part.NumVertices;

                                    break;
                                }
                            default:
                                {
                                    Onderzoek.CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CDotXTerrainEntity", "Elemental Game Error: Could not create instance of this entity because the Model's vertex data is in the wrong format");
                                    break;
                                }
                        }

                        ++i;
                        ++j;
                    }
                }
                else
                {
                    Onderzoek.CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CDotXTerrainEntity", "Elemental Game Error: Could not create instance of this entity because the Model's index data is in the wrong format");
                }
            }

            m_positionBuffer = pos_buffer.ToArray();
            m_indexBuffer = index_buffer.ToArray();

#if EDITOR
            FillPropertyList();
#endif
        }

        public override void Dispose()
        {
            if (m_indexBuffer != null)
            {
                foreach (List<short> ib in m_indexBuffer)
                {
                    if (ib != null)
                    {
                        ib.Clear();
                    }
                }
                m_indexBuffer = null;
            }
            if (m_positionBuffer != null)
            {
                foreach (List<Vector3> pb in m_positionBuffer)
                {
                    if (pb != null)
                    {
                        pb.Clear();
                    }
                }
                m_positionBuffer = null;
            }
        }

        public override bool GetHeightOfCurrentPos(Vector3 Position, ref SHeightMapData HMData, Vector3 RayDir, ref float Dist)
        {
            if (CToolbox.IsPointInXZMinMaxAndYMinOfBoundingBox(m_min, m_max, Position) == false)
            {
                return false;
            }

            return RayVsTerrain(Position, ref HMData, RayDir, ref Dist);
        }

        public override bool RayVsTerrain(Vector3 Position, ref SHeightMapData HMData, Vector3 RayDir, ref float Dist)
        {
            for (int i = 0; i < m_indexBuffer.Length; ++i )
            {
                for (int j = 0; j < m_indexBuffer[i].Count; j+=3)
                {
                    CCollision.STriData temp;
                    temp.s_firstPos = m_positionBuffer[i][m_indexBuffer[i][j]];
                    temp.s_secondPos = m_positionBuffer[i][m_indexBuffer[i][j + 1]];
                    temp.s_thirdPos = m_positionBuffer[i][m_indexBuffer[i][j + 2]];

                    if (CCollision.RayVsTriangle(RayDir, Position, temp, out Dist) == true)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public override void SetMaterialList(List<CMaterial> MaterialList)
        {
            base.SetMaterialList(MaterialList);

            IsTranslucent(((CDotXModel)m_modelData).m_model);
        }

        public override bool CheckAgainstClipPlanes(Plane[] ClipPlanes)
        {
            return CToolbox.ClipWithPlanes(m_min, m_max, ClipPlanes);
        }

        public override bool CheckAgainstClipPlanes(List<Plane[]> ClipPlanes)
        {
            for (int i = 0; i < ClipPlanes.Count; ++i)
            {
                if (CToolbox.ClipWithPlanes(m_min, m_max, ClipPlanes[i]) == true)
                {
                    return true;
                }
            }
            return false;
        }

        public override void SetRender(bool Render, bool AtStartOfFrame)
        {
            m_render = Render;
        }

        public override void Render(CCamera Camera)
        {
            if (m_hidden == true)
            {
                return;
            }
            //m_position = Vector3.Zero;

            CVisual.Instance.Render(((CDotXModel)m_modelData).m_model, Camera, m_material, m_worldMatrix, m_opaqueParts);
        }

        public override void ForwardRender(CCamera Camera)
        {
            if (m_hidden == true)
            {
                return;
            }

            CVisual.Instance.ForwardRender(((CDotXModel)m_modelData).m_model, m_material, m_worldMatrix, m_opaqueParts);
        }

        public override void ForwardTranslucentRender(CCamera Camera)
        {
            if (m_hidden == true)
            {
                return;
            }

            CVisual.Instance.ForwardRender(((CDotXModel)m_modelData).m_model, m_material, m_worldMatrix, m_translucentParts);
        }

        public override void RenderForShadow(CCamera Camera)
        {
            if (m_hidden == true)
            {
                return;
            }

            CVisual.Instance.RenderForShadow(((CDotXModel)m_modelData).m_model, m_worldMatrix);
        }

#if EDITOR
        public override void FillPropertyList()
        {
            this.m_propertyList = new List<CEntityProperty>();

            CEntityProperty modelSelectionEntityProperty = new CEntityProperty("Model", "", EPropertyGridInputType.ModelSelection);

            m_propertyList.Add(modelSelectionEntityProperty);

            if (m_modelName != null)
            {
                string temp = m_modelName.Replace("Model/", "");
                temp = temp.Replace("Content/", "");
                temp = temp.Replace("Content\\\\", "");
                temp = temp.Replace("Content\\", "");
                temp = temp.Replace("Model\\\\", "");
                temp = temp.Replace("Model\\", "");
                temp = temp.Replace("..\\", "");
                m_modelName = temp;
                m_propertyList[m_propertyList.Count - 1].propertyValue = temp;
            }

            modelSelectionEntityProperty = new CEntityProperty("Material", "", EPropertyGridInputType.MaterialSelection);
            m_propertyList.Add(modelSelectionEntityProperty);

            //CEntityProperty collidable = new CEntityProperty("Collides", "", EPropertyGridInputType.PullDownMultipleChoice);
            //string[] strList = { "Yes", "No" };
            //collidable.pullDownOptions = new List<string>(strList);
            //m_propertyList.Add(collidable);

            //base.FillPropertyList();
        }

        public override void UpdateFromPropertyList()
        {
            for (int i = 0; i < m_propertyList.Count; i++)
            {
                if (m_propertyList[i].propertyName.Equals("Model"))
                {
                    m_modelName = m_propertyList[i].propertyValue;
                }

                if (m_propertyList[i].propertyName.Equals("Material"))
                {
                    m_materialName = m_propertyList[i].propertyValue;
                }
            }

            //base.UpdateFromPropertyList();
        }

        public override int GetEntityID()
        {
            return (int)EBasicEntityIDs.DotXTerrainEntity;

            //return base.GetEntityID();
        }

        public override void Update()
        {
            base.Update();
        }
#else
        public static void LoadEntityData(CEntityDataStore EntityData, Onderzoek.Screens.CGameScreenHelper CurScreen,
            ref Dictionary<string, CModel.SModelData> ModelList, ref ContentManager Content)
        {
            string mod = "";
            string material = "";

            for (int i = 0; i < EntityData.m_properties.Count; ++i)
            {
                if (EntityData.m_properties[i].propertyName == "Model")
                {
                    mod = "Model/" + EntityData.m_properties[i].propertyValue;
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Material")
                {
                    material = EntityData.m_properties[i].propertyValue;
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
            }

            CDotXModel dotx_mod = new CDotXModel();
            dotx_mod = (CDotXModel)dotx_mod.Initialise(Content, mod, ModelList);
            List<CMaterial> dotx_mat = null;
            if (material == "" || material == null)
            {
                dotx_mat = dotx_mod.GetMaterials(Content, mod);
            }
            else
            {
                CMaterialManager.loadMaterialXML(dotx_mod, material, ref dotx_mat, ref Content);
            }

            // Create the model
            CDotXTerrainEntity dotx_ter = new CDotXTerrainEntity(EntityData.m_position, EntityData.m_rotation, EntityData.m_entityName, dotx_mod, dotx_mat);
            CurScreen.AddEntityNoSPT(dotx_ter);
        }
#endif
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

#if EDITOR
using Microsoft.Xna.Framework.Graphics;
using Onderzoek.Screens;
#endif

using Onderzoek.Visual;
using Onderzoek.ModelData;
using Onderzoek.Physics;
using Onderzoek.Camera;

namespace Onderzoek.Entity
{
    public class CKeyFramedDotXEntity : CDynamicEntity
    {
        protected CKeyFrame.SKFData m_keyFrameData;

        public override void Dispose()
        {
#if EDITOR
            CVisual.Instance.RemoveLinesWithThisColour(new Color(5, 255, 5));
            CVisual.Instance.RemoveLinesWithThisColour(new Color(255, 5, 5));

            if (m_prvNode != null && m_nxtNode != null)
            {
                m_prvNode.SetNextKFNode(m_nxtNode);
                m_nxtNode.SetPrvKFNode(m_prvNode);
            }
            else if (m_prvNode != null)
            {
                m_prvNode.SetNextKFNode(null);
            }
            else if (m_nxtNode != null)
            {
                m_nxtNode.SetPrvKFNode(null);
            }

            m_prvNode = null;
            m_nxtNode = null;
            m_curScreen = null;
#endif

            base.Dispose();

            m_keyFrameData.s_keyFrameAry = null;
        }

#if EDITOR
        protected bool m_loop = false;
        protected CEntity m_prvNode = null;
        protected CEntity m_nxtNode = null;
        protected CGameScreenHelper m_curScreen;
        protected float m_endTime = 0;
        protected List<CKeyFrame.SKeyFrame> m_kfData;
        protected bool m_smoothTransition = false;

        public CKeyFramedDotXEntity(Vector3 Position, Vector3 Rotation, string EntityName, CModel Model, List<CMaterial> Material, bool IsCollidable,
            CKeyFrame.SKFData KeyFrameData, CGameScreenHelper CurScreen)
        {
#if EDITOR
            m_editorRotation = Rotation;
#endif

            Initialise(Position, Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z), EntityName, Model, Material, IsCollidable, KeyFrameData,
                CurScreen);
        }

        public void Initialise(Vector3 Position, Quaternion Rotation, string EntityName, CModel Model, List<CMaterial> Material, bool IsCollidable,
            CKeyFrame.SKFData KeyFrameData, CGameScreenHelper CurScreen)
        {
            m_curScreen = CurScreen;
            m_modelName = Model.GetModelName();
#else
        public CKeyFramedDotXEntity(Vector3 Position, Vector3 Rotation, string EntityName, CModel Model, List<CMaterial> Material, bool IsCollidable,
            CKeyFrame.SKFData KeyFrameData)
        {
            Initialise(Position, Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z), EntityName, Model, Material, IsCollidable, KeyFrameData);
        }

        public void Initialise(Vector3 Position, Quaternion Rotation, string EntityName, CModel Model, List<CMaterial> Material, bool IsCollidable,
            CKeyFrame.SKFData KeyFrameData)
        {
#endif
            base.Initalise();

            m_portalInfo = new SPortalInfo();
            m_material = Material;
            m_entityName = EntityName;

            m_keyFrameData = KeyFrameData;
            m_isDynamic = true;
            m_position = Position;
            m_modelData = Model;
            m_isCollidable = IsCollidable;
            m_rotation = Rotation;
            m_canBeParent = true;

            // The keyframe animation is being activated
            if (m_keyFrameData.s_active == true)
            {
                CKeyFrame.Reset(ref m_keyFrameData, ref m_position, ref m_rotation);
            }

            m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);

            // Transform local space OBB corners to world space
            InitialiseEntityOBB(m_modelData.GetOBB());

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            IsTranslucent(((CDotXModel)m_modelData).m_model);

#if EDITOR
            FillPropertyList();
#endif
        }

        public override int GetEntityID()
        {
            return (int)EBasicEntityIDs.KeyFramedEntity;
        }

#if EDITOR
        public override void SetMaterialList(List<CMaterial> MaterialList)
        {
            m_material = MaterialList;

            InitialiseMaterialEffects();

            IsTranslucent(m_modelData.getBaseModel());
        }

        public override bool GetPrvKFNode(ref CEntity PrvKFNode)
        {
            PrvKFNode = m_prvNode;
            return true;
        }

        public override bool SetPrvKFNode(CEntity PrvKFNode)
        {
            m_prvNode = PrvKFNode;
            if (m_prvNode != null)
            {
                m_propertyList[3].propertyValue = m_prvNode.GetEntityName();
            }
            else
            {
                m_propertyList[3].propertyValue = "";
            }
            return true;
        }

        public override bool GetNextKFNode(ref CEntity NextKFNode)
        {
            NextKFNode = m_nxtNode;
            return true;
        }

        public override bool SetNextKFNode(CEntity NextKFNode)
        {
            m_nxtNode = NextKFNode;
            if (m_nxtNode != null)
            {
                m_propertyList[4].propertyValue = m_nxtNode.GetEntityName();
            }
            else
            {
                m_propertyList[4].propertyValue = "";
            }

            return true;
        }

        public override void Update()
        {
            if (m_hidden == false)
            {
                if (m_prvNode != null)
                {
                    Vector3 prv_pos = m_prvNode.GetWorldPosition();
                    Vector3 fifth_way = prv_pos + ((m_position - prv_pos) / 5.0f) * 4.0f;
                    CVisual.Instance.AddToLineList(prv_pos, fifth_way, new Color(255, 5, 5));
                    CVisual.Instance.AddToLineList(fifth_way, m_position, new Color(5, 255, 5));
                }
            }

            base.Update();
        }

        public override void writeEntityProperties(XmlWriter writer)
        {
            for (int i = 0; i < m_propertyList.Count; i++)
            {
                if (m_propertyList[i].propertyName.Equals("Prv_Node") == true)
                {
                    List<CKeyFrame.SKeyFrame> kf_data = new List<CKeyFrame.SKeyFrame>();
                    StartCollationOfKFData(ref kf_data);

                    for (int j = 0; j < kf_data.Count; ++j)
                    {
                        writer.WriteStartElement("KeyFrame");
                        writer.WriteElementString("Position", kf_data[j].s_position.ToString());
                        writer.WriteElementString("Rotation", kf_data[j].s_rotation.ToString());
                        writer.WriteElementString("StartTime", kf_data[j].s_startTime.ToString());
                        writer.WriteEndElement();
                    }

                    // We want to skip the next two.
                    i += 2;
                }
                else
                {
                    writer.WriteElementString(m_propertyList[i].propertyName, m_propertyList[i].propertyValue);
                }
            }
        }

        public override void readEntityProperties(XmlReader reader)
        {
            m_kfData = new List<CKeyFrame.SKeyFrame>();

            reader.ReadToFollowing("EntityData");
            while (!(reader.Name == "EntityData" && reader.NodeType == XmlNodeType.EndElement))
            {
                reader.Read();
                reader.Read();

                reader.MoveToContent();
                string propName = reader.Name;

                if (reader.Name == "EntityData" && reader.NodeType == XmlNodeType.EndElement)
                    break;

                if (reader.Name == "KeyFrame")
                {
                    while (reader.NodeType == XmlNodeType.Element && reader.Name == "KeyFrame")
                    {
                        reader.Read();
                        reader.Read();
                        reader.Read();

                        Vector3 pos = Vector3.Zero;
                        CToolbox.StringToVector3(reader.Value, ref pos);

                        reader.Read();
                        reader.Read();
                        reader.Read();
                        reader.Read();

                        Vector3 rot = Vector3.Zero;
                        CToolbox.StringToVector3(reader.Value, ref rot);

                        reader.Read();
                        reader.Read();
                        reader.Read();
                        reader.Read();

                        float time = 0;
                        CToolbox.StringToFloat(reader.Value, ref time);

                        reader.Read();
                        reader.Read();
                        reader.Read();
                        reader.Read();
                        reader.Read();

                        m_kfData.Add(new CKeyFrame.SKeyFrame(time, pos, rot));
                    }

                    propName = reader.Name;
                }

                string propVal;

                if (reader.IsEmptyElement)
                    propVal = "";
                else
                {
                    reader.Read();
                }

                propVal = reader.Value;

                for (int i = 0; i < m_propertyList.Count; i++)
                {
                    if (m_propertyList[i].propertyName.Equals(propName))
                    {
                        m_propertyList[i].propertyValue = propVal;
                        break;
                    }
                }
            }

            if (m_kfData.Count > 0)
            {
                m_propertyList[5].propertyValue = m_kfData[m_kfData.Count - 1].s_startTime.ToString();
            }
        }

        public List<CKeyFrame.SKeyFrame> GetKFList()
        {
            return m_kfData;
        }

        void StartCollationOfKFData(ref List<CKeyFrame.SKeyFrame> KeyFrameData)
        {
            if (m_nxtNode != null)
            {
                KeyFrameData.Add(new CKeyFrame.SKeyFrame(0, m_position, GetEditorRotation()));

                m_nxtNode.CollateKeyFrameData(ref KeyFrameData, true);
            }
        }

        public override void CollateKeyFrameData(ref List<CKeyFrame.SKeyFrame> KeyFrameData, bool GoingForward)
        {
            //if (GoingForward == false)
            {
                KeyFrameData.Add(new CKeyFrame.SKeyFrame(m_endTime, m_position, GetEditorRotation()));
            }
        }

        public override void FillPropertyList()
        {
            this.m_propertyList = new List<CEntityProperty>();

            m_propertyList.Add(new CEntityProperty("Model", "", EPropertyGridInputType.ModelSelection));
            if (m_modelName != null)
            {
                string temp = m_modelName.Replace("Model/", "");
                temp = temp.Replace("Content/", "");
                temp = temp.Replace("Content\\\\", "");
                temp = temp.Replace("Content\\", "");
                temp = temp.Replace("Model\\\\", "");
                temp = temp.Replace("Model\\", "");
                temp = temp.Replace("..\\", "");
                m_modelName = temp;
                m_propertyList[m_propertyList.Count - 1].propertyValue = temp;
            }

            m_propertyList.Add(new CEntityProperty("Material", "", EPropertyGridInputType.MaterialSelection));

            m_propertyList.Add(new CEntityProperty("Collidable", m_isCollidable.ToString(), EPropertyGridInputType.PullDownMultipleChoice));
            m_propertyList[m_propertyList.Count - 1].pullDownOptions = new List<string>();
            m_propertyList[m_propertyList.Count - 1].pullDownOptions.Add("True");
            m_propertyList[m_propertyList.Count - 1].pullDownOptions.Add("False");

            m_propertyList.Add(new CEntityProperty("Prv_Node", "", EPropertyGridInputType.TextBox));
            m_propertyList.Add(new CEntityProperty("Next_Node", "", EPropertyGridInputType.TextBox));

            m_propertyList.Add(new CEntityProperty("End_Time", m_endTime.ToString(), EPropertyGridInputType.TextBox));

            m_propertyList.Add(new CEntityProperty("Loop", m_loop.ToString(), EPropertyGridInputType.PullDownMultipleChoice));
            m_propertyList[m_propertyList.Count - 1].pullDownOptions = new List<string>();
            m_propertyList[m_propertyList.Count - 1].pullDownOptions.Add("True");
            m_propertyList[m_propertyList.Count - 1].pullDownOptions.Add("False");

            m_propertyList.Add(new CEntityProperty("Smooth_Transition", m_loop.ToString(), EPropertyGridInputType.PullDownMultipleChoice));
            m_propertyList[m_propertyList.Count - 1].pullDownOptions = new List<string>();
            m_propertyList[m_propertyList.Count - 1].pullDownOptions.Add("True");
            m_propertyList[m_propertyList.Count - 1].pullDownOptions.Add("False");
        }

        public override void UpdateFromPropertyList()
        {
            m_modelName = m_propertyList[0].propertyValue;
            m_materialName = m_propertyList[1].propertyValue;

            bool bl = false;
            if (CToolbox.StringToBool(m_propertyList[2].propertyValue, ref bl) == false)
            {
                m_propertyList[2].propertyValue = m_isCollidable.ToString();
            }
            else
            {
                m_isCollidable = bl;
            }

            if (m_propertyList[3].propertyValue.Equals("") == false)
            {
                CEntity node_ent = m_curScreen.GetEntity(m_propertyList[3].propertyValue);
                if (node_ent == null)
                {
                    if (m_prvNode != null)
                    {
                        m_propertyList[3].propertyValue = m_prvNode.GetEntityName();
                    }
                    else
                    {
                        m_propertyList[3].propertyValue = "";
                    }
                }
                else
                {
                    if (node_ent.GetEntityName().Equals(m_entityName) == false)
                    {
                        if (m_prvNode != null)
                        {
                            m_prvNode.SetNextKFNode(null);
                        }

                        if (node_ent.SetNextKFNode(this) == true)
                        {
                            m_prvNode = node_ent;
                        }
                    }
                    else
                    {
                        if (m_prvNode != null)
                        {
                            m_propertyList[3].propertyValue = m_prvNode.GetEntityName();
                        }
                        else
                        {
                            m_propertyList[3].propertyValue = "";
                        }
                    }
                }
            }
            else
            {
                if (m_prvNode != null)
                {
                    m_prvNode.SetNextKFNode(null);
                }
                m_prvNode = null;
            }

            if (m_propertyList[4].propertyValue.Equals("") == false)
            {
                CEntity node_ent = m_curScreen.GetEntity(m_propertyList[4].propertyValue);
                if (node_ent == null)
                {
                    if (m_nxtNode != null)
                    {
                        m_propertyList[4].propertyValue = m_nxtNode.GetEntityName();
                    }
                    else
                    {
                        m_propertyList[4].propertyValue = "";
                    }
                }
                else
                {
                    if (node_ent.GetEntityName().Equals(m_entityName) == false)
                    {
                        if (m_nxtNode != null)
                        {
                            m_nxtNode.SetPrvKFNode(null);
                        }

                        if (node_ent.SetPrvKFNode(this) == true)
                        {
                            m_nxtNode = node_ent;
                        }
                    }
                    else
                    {
                        if (m_nxtNode != null)
                        {
                            m_propertyList[4].propertyValue = m_nxtNode.GetEntityName();
                        }
                        else
                        {
                            m_propertyList[4].propertyValue = "";
                        }
                    }
                }
            }
            else
            {
                if (m_nxtNode != null)
                {
                    m_nxtNode.SetPrvKFNode(null);
                }
                m_nxtNode = null;
            }

            float flt = 0;
            if (CToolbox.StringToFloat(m_propertyList[5].propertyValue, ref flt) == false)
            {
                m_propertyList[5].propertyValue = m_endTime.ToString();
            }
            else
            {
                if (flt < 0)
                {
                    flt = 0;
                }

                m_endTime = flt;
            }

            bl = false;
            if (CToolbox.StringToBool(m_propertyList[6].propertyValue, ref bl) == false)
            {
                m_propertyList[6].propertyValue = m_loop.ToString();
            }
            else
            {
                m_loop = bl;
            }

            bl = false;
            if (CToolbox.StringToBool(m_propertyList[7].propertyValue, ref bl) == false)
            {
                m_propertyList[7].propertyValue = m_smoothTransition.ToString();
            }
            else
            {
                m_smoothTransition = bl;
            }
        }
#else
        public override void Update(float DT)
        {
            if (m_hidden == true)
            {
                return;
            }

            CKeyFrame.Update(ref m_keyFrameData, DT, ref m_position, ref m_rotation);

            m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);
            // This will apply the parent matrix on to the entity if it's has a parent.
            // This MUST be called straight after creating the new world matrix!
            UpdateIfChild();

            //Matrix wrld_mat_no_tran = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(Vector3.Zero);

            //// Transform local space OBB corners to world space
            //for (int i = 0; i < OBB.Count; ++i)
            //{
            //    //if (i == 0)
            //    {
            //        // Transform local space OBB corners to world space
            //        CToolbox.TransformLocalToWorld(OBB[i], out m_wrldOBB[i].s_wrldOBB, m_worldMatrix);
            //    }
            //    //else
            //    {
            //        //CToolbox.TransformLocalToWorld(OBB[i], out m_wrldOBB[i].s_wrldOBB, wrld_mat_no_tran);
            //    }

            //    // Create the Dynamic Entity's planes - needs to be here so that only the rotation is applied
            //    UpdatePlanes(m_rotation, i);

            //    //m_wrldOBB[i].s_invRotWrld = Matrix.Invert(Matrix.CreateFromQuaternion(Quaternion.CreateFromYawPitchRoll(m_rotation.X, m_rotation.Y, m_rotation.Z)));
            //    //m_wrldOBB[i].s_rotWrld = Matrix.CreateFromQuaternion(Quaternion.CreateFromYawPitchRoll(m_rotation.X, m_rotation.Y, m_rotation.Z));

            //    //m_wrldOBB[i].s_invWrld = Matrix.Invert(Matrix.CreateTranslation(m_position));// m_wrldOBB[i].s_invRotWrld * Matrix.Invert(Matrix.CreateTranslation(m_position));
            //    //m_wrldOBB[i].s_wrld = Matrix.CreateTranslation(m_position);
            //    //m_wrldOBB[i].s_boxCentre = m_position;
            //}

            // Update the obbs - Replaces one of the stuff in the above for loop
            List<CModelOBB> OBB = m_modelData.GetOBB();
            UpdateEntityOBB(OBB);

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            // The following two lines will only need to be updated if the bounding box size changes or if the dynamic object moves(translation) in the scene.

            // This needs to be updated for the calcualtion that will take place in the below base update.
            UpdateCollisionSphere();
        }

        public static void LoadEntityData(CEntityDataStore EntityData, Onderzoek.Screens.CGameScreenHelper CurScreen,
            ref Dictionary<string, CModel.SModelData> ModelList, ref ContentManager Content)
        {
            string model_name = "";
            string material = "";
            bool collidable = true;
            bool loop = false;
            bool smooth_transition = false;
            List<CKeyFrame.SKeyFrame> kf_list = new List<CKeyFrame.SKeyFrame>();

            for (int i = 0; i < EntityData.m_properties.Count; ++i)
            {
                if (EntityData.m_properties[i].propertyName == "Model")
                {
                    model_name = "Model\\" + EntityData.m_properties[i].propertyValue;
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Material")
                {
                    material = EntityData.m_properties[i].propertyValue;
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Collidable")
                {
                    CToolbox.StringToBool(EntityData.m_properties[i].propertyValue, ref collidable);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "KeyFrame")
                {
                    EntityData.m_properties.RemoveAt(i);

                    Vector3 pos = Vector3.One;
                    CToolbox.StringToVector3(EntityData.m_properties[i].propertyValue, ref pos);

                    EntityData.m_properties.RemoveAt(i);

                    Vector3 rot = Vector3.Zero;
                    CToolbox.StringToVector3(EntityData.m_properties[i].propertyValue, ref rot);

                    EntityData.m_properties.RemoveAt(i);

                    float time = 0;
                    CToolbox.StringToFloat(EntityData.m_properties[i].propertyValue, ref time);

                    EntityData.m_properties.RemoveAt(i);
                    --i;

                    if (kf_list.Count == 0)
                    {
                        kf_list.Add(new CKeyFrame.SKeyFrame(time, pos, rot));
                    }
                    else if (time != 0)
                    {
                        kf_list.Add(new CKeyFrame.SKeyFrame(time, pos, rot));
                    }
                }
                else if (EntityData.m_properties[i].propertyName == "Loop")
                {
                    CToolbox.StringToBool(EntityData.m_properties[i].propertyValue, ref loop);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Smooth_Transition")
                {
                    CToolbox.StringToBool(EntityData.m_properties[i].propertyValue, ref smooth_transition);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
            }

            CDotXModel dotx_mod = new CDotXModel();
            dotx_mod = (CDotXModel)dotx_mod.Initialise(Content, model_name, ModelList);
            List<CMaterial> dotx_mat = null;
            if (material == "" || material == null)
            {
                dotx_mat = dotx_mod.GetMaterials(Content, model_name);
            }
            else
            {
                CMaterialManager.loadMaterialXML(dotx_mod, material, ref dotx_mat, ref Content);
            }

            CKeyFrame.SKFData kf_data = new CKeyFrame.SKFData();

            kf_data.s_active = true;
            kf_data.s_loop = loop;
            kf_data.s_smoothTransition = smooth_transition;
            kf_data.s_numKeyFrames = kf_list.Count;
            kf_data.s_keyFrameAry = kf_list.ToArray();

            // Create the model
            CKeyFramedDotXEntity ent = new CKeyFramedDotXEntity(EntityData.m_position, EntityData.m_rotation, EntityData.m_entityName, dotx_mod, dotx_mat,
                collidable, kf_data);
            CurScreen.AddEntityNoSPT(ent);
        }
#endif

        public override void Render(CCamera Camera)
        {
            if (m_hidden == true)
            {
                return;
            }

            CVisual.Instance.Render(((CDotXModel)m_modelData).m_model, Camera, m_material, m_worldMatrix, m_opaqueParts);
        }

        public override void ForwardRender(CCamera Camera)
        {
            if (m_hidden == true)
            {
                return;
            }

            CVisual.Instance.ForwardRender(((CDotXModel)m_modelData).m_model, m_material, m_worldMatrix, m_opaqueParts);
        }

        public override void ForwardTranslucentRender(CCamera Camera)
        {
            if (m_hidden == true)
            {
                return;
            }

            CVisual.Instance.ForwardRender(((CDotXModel)m_modelData).m_model, m_material, m_worldMatrix, m_translucentParts);
        }

        public override void RenderForShadow(CCamera Camera)
        {
            if (m_hidden == true)
            {
                return;
            }

            CVisual.Instance.RenderForShadow(((CDotXModel)m_modelData).m_model, m_worldMatrix);
        }
    }
}

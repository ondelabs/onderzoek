﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using Onderzoek.Visual;
using Onderzoek.ModelData;
using Onderzoek.Physics;
using Onderzoek.Camera;

// THIS IS ONLY USED TO TEST THE PARTICLE RENDERING.

namespace Onderzoek.Entity
{
    public class CTESTParticleEntity : CDynamicEntity
    {
        public struct SParticle
        {
            public CPhysics.SProjectileData s_projData;
            public float s_time;
        }

        SParticle[] m_particleArray;
        int m_lastAliveIndex = 0;
        Matrix[] m_wm;
        Vector4[] m_col;
        List<CModelOBB> m_modelOBB;
        float m_maxLifeTime;

        Vector3 m_maxInitialVelocity;
        Vector3 m_minInitialVelocity;
        Vector3 m_maxAcceleration;
        Vector3 m_minAcceleration;

        int m_numParticles;

        public override void Dispose()
        {
            m_particleArray = null;
            m_wm = null;
            m_col = null;
            m_modelOBB = null;

            base.Dispose();
        }

        public CTESTParticleEntity(Vector3 Position, Vector3 Rotation, string EntityName, List<CMaterial> Material, bool IsCollidable)
        {
            Initialise(Position, Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z), EntityName, Material, IsCollidable);
        }

        public void Initialise(Vector3 Position, Quaternion Rotation, string EntityName, List<CMaterial> Material, bool IsCollidable)
        {
            base.Initalise();

            m_maxInitialVelocity = new Vector3(2, 10, 2);
            m_minInitialVelocity = new Vector3(-1, 10, -1);
            m_maxAcceleration = new Vector3(0, -9.1f, 0);
            m_minAcceleration = new Vector3(0, -9.1f, 0);

            m_numParticles = 4000;
            m_maxLifeTime = 2;
            m_particleArray = new SParticle[m_numParticles];

            // Create the bounding box depending on the physics data.
            m_modelOBB = CPhysics.CreateBoundingBoxFromPhysicsData(new Vector3(0, 0, 30.0f), m_position, m_maxInitialVelocity, m_minInitialVelocity,
                                                                    m_maxAcceleration, m_minAcceleration, m_maxLifeTime, 1);
            m_portalInfo = new SPortalInfo();
            m_material = Material;
            m_canBeParent = false;

            m_entityName = EntityName;
            m_isDynamic = true;
            m_position = Position;
            m_isCollidable = IsCollidable;
            m_rotation = Rotation;

            m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);

            // Transform local space OBB corners to world space
            InitialiseEntityOBB(m_modelOBB);

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            m_wholeTranslucent = true;
        }

#if EDITOR
        public override void Update()
        {
            base.Update();
        }
#else
        public void Update(CCamera Camera, float DT)
        {
            Random ran_num = new Random();
            if (m_lastAliveIndex < m_numParticles)
            {
                for (int i = m_lastAliveIndex; i < m_lastAliveIndex + 1; ++i)
                {
                    m_particleArray[i].s_time = 0;
                    m_particleArray[i].s_projData.s_acceleration = m_maxAcceleration;
                    m_particleArray[i].s_projData.s_curVel = new Vector3(ran_num.Next((int)m_minInitialVelocity.Z, (int)m_maxInitialVelocity.Z),
                        m_maxInitialVelocity.Y, ran_num.Next((int)m_minInitialVelocity.Z, (int)m_maxInitialVelocity.Z));
                    m_particleArray[i].s_projData.s_nxtPos = m_position;
                }
                m_lastAliveIndex += 1;
            }
            else
            {
                m_lastAliveIndex = 0;
            }

            m_wm = new Matrix[m_lastAliveIndex];
            m_col = new Vector4[m_lastAliveIndex];

            Matrix inv_view = Camera.GetInvView();
            for (int i = 0; i < m_lastAliveIndex; ++i)
            {
                if (m_particleArray[i].s_time > m_maxLifeTime)
                {
                    m_particleArray[i].s_projData.s_curVel = Vector3.Zero;
                    m_particleArray[i].s_projData.s_acceleration = Vector3.Zero;
                    m_particleArray[i].s_projData.s_nxtPos = new Vector3(0, -10, 0);
                }
                else
                {
                    m_particleArray[i].s_time += DT;
                    CPhysics.UpdateProjectilePhysics(ref m_particleArray[i].s_projData, DT);
                }

                m_wm[i] = Matrix.CreateTranslation(m_particleArray[i].s_projData.s_nxtPos);
                m_wm[i].M11 = inv_view.M11;
                m_wm[i].M12 = inv_view.M12;
                m_wm[i].M13 = inv_view.M13;
                m_wm[i].M21 = inv_view.M21;
                m_wm[i].M22 = inv_view.M22;
                m_wm[i].M23 = inv_view.M23;
                m_wm[i].M31 = inv_view.M31;
                m_wm[i].M32 = inv_view.M32;
                m_wm[i].M33 = inv_view.M33;
                m_wm[i] = Matrix.CreateScale(new Vector3(3, 3, 3)) * m_wm[i];

                //m_col[i] = new Vector4((float)ran_num.Next(0, 255) / 100.0f, (float)ran_num.Next(0, 255) / 100.0f, (float)ran_num.Next(0, 255) / 100.0f, 0.5f);
                m_col[i] = new Vector4(1.0f, 0.5f, 0.1f, 0.5f);
            }

            m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);
            // This will apply the parent matrix on to the entity if it's has a parent.
            // This MUST be called straight after creating the new world matrix!
            UpdateIfChild();

            // Update the obbs
            UpdateEntityOBB(m_modelOBB);

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            // The following two lines will only needed to be updated if the bounding box size changes or if the dynamic object moves(translation) in the scene.

            // This needs to be updated for the calcualtion that will take place in the below base update.
            UpdateCollisionSphere();
        }

        // DEBUG VARIABLES
        //float tran = -0.5f;

        public override void Update(float DT)
        {
            //m_position.Z += tran;

            //if (m_position.Z > 120.0f)
            //{
            //    tran = -0.5f;
            //}
            //if (m_position.Z < -129.0f)
            //{
            //    tran = 0.5f;
            //}
        }

#endif

        public override void ForwardTranslucentRender(CCamera Camera)
        {
            CVisual.Instance.BeginQuadParticleRender();

            // This ia to render with a texture
            CVisual.Instance.SetUpQuadParticleEffect(0, Camera);
            if (m_wm != null)
            {
                CVisual.Instance.RenderQuadParticles(m_wm, m_col, m_wm.Length);
            }

            CVisual.Instance.EndQuadParticleRender();
        }
    }
}

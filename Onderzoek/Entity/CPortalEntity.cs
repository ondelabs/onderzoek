﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

using Onderzoek.Visual;
using Onderzoek.ModelData;
using Onderzoek.Camera;

namespace Onderzoek.Entity
{
    public class CPortalEntity : CStaticEntity
    {
        protected Vector3 m_scale;

        public override void Dispose()
        {
            base.Dispose();
        }

        public CPortalEntity(Vector3 Scale, Vector3 Position, Vector3 Rotation, string EntityName, CModel Model)
        {
#if EDITOR
            m_editorRotation = Rotation;
#endif

            Initialise(Scale, Position, Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z), EntityName, Model);
        }

        public void Initialise(Vector3 Scale, Vector3 Position, Quaternion Rotation, string EntityName, CModel Model)
        {
            m_rotation = Rotation;
            m_portalInfo = new SPortalInfo();
            m_portalInfo.s_isPortal = true;
            m_portalInfo.s_portalNormal = CToolbox.UnitVector(Vector3.TransformNormal(Vector3.Up, Matrix.CreateFromQuaternion(m_rotation)));
            m_isDynamic = false;
            m_position = Position;
            m_modelData = Model;
            m_isCollidable = false;
            m_scale = Scale;
            m_entityName = EntityName;

#if EDITOR
            m_propertyList = new List<CEntityProperty>();
            m_isScalingAllowed = true;
#endif

            // Forcing it render through the forward render.
            m_material = new List<CMaterial>();
            m_material.Add(new CMaterial());
            m_material[0].m_diffuse.W = 0.99f;
            m_wholeTranslucent = true;

            m_worldMatrix = Matrix.CreateScale(Scale) * Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);

            // Transform local space OBB corners to world space
            InitialiseEntityOBB(m_modelData.GetOBB());

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            UpdateCollisionSphere();

            m_worldMatrix = Matrix.CreateFromQuaternion(Rotation) * Matrix.CreateTranslation(m_position);
        }

#if EDITOR
        public override CModel ModelData
        {
            get
            {
                return base.ModelData;
            }
            set
            {
                //base.ModelData = value;
            }
        }

        public override void SetMaterialList(List<CMaterial> MaterialList)
        {
            //base.SetMaterialList(MaterialList);
        }

        public override void writeEntityProperties(XmlWriter writer)
        {
            writer.WriteElementString("Scale", m_scale.ToString());
        }

        public override void readEntityProperties(XmlReader reader)
        {
            reader.ReadToFollowing("EntityData");
            while (!(reader.Name == "EntityData" && reader.NodeType == XmlNodeType.EndElement))
            {
                reader.Read();
                reader.Read();

                reader.MoveToContent();
                string propName = reader.Name;

                if (reader.Name == "EntityData" && reader.NodeType == XmlNodeType.EndElement)
                    break;
                string propVal;

                if (reader.IsEmptyElement)
                    propVal = "";
                else
                {
                    reader.Read();
                }

                propVal = reader.Value;

                if (propName.Equals("Scale"))
                {
                    CToolbox.StringToVector3(propVal, ref m_scale);
                }
            }
        }

        public override void Update()
        {
            m_worldMatrix = Matrix.CreateScale(m_scale) * Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);

            m_portalInfo.s_portalNormal = CToolbox.UnitVector(Vector3.TransformNormal(Vector3.Up, Matrix.CreateFromQuaternion(m_rotation)));

            // Update the obbs
            UpdateEntityOBB(m_modelData.GetOBB());

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            // The following two lines will only needed to be updated if the bounding box size changes or if the dynamic object moves(translation) in the scene.

            // This needs to be updated for the calcualtion that will take place in the below base update.
            UpdateCollisionSphere();

            // this needs to be called so that the spatial partioning tree is updated and the it won't just disappear from the scene.
            if (m_entityLeafNode != null)
            {
                m_entityLeafNode.MoveNodeAroundTree(m_position, m_maxLengthOfEntity);
            }
        }

        public override Vector3 Scale
        {
            get { return m_scale; }
            set { m_scale = value; }
        }
#else
        public static void LoadEntityData(CEntityDataStore EntityData, Onderzoek.Screens.CGameScreenHelper CurScreen,
            ref Dictionary<string, CModel.SModelData> ModelList, ref ContentManager Content)
        {
            Vector3 scale = Vector3.One;

            for (int i = 0; i < EntityData.m_properties.Count; ++i)
            {
                if (EntityData.m_properties[i].propertyName == "Scale")
                {
                    CToolbox.StringToVector3(EntityData.m_properties[i].propertyValue, ref scale);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
            }

            CWireFrameModel mod = new CWireFrameModel();
            mod = (CWireFrameModel)mod.Initialise(CModel.CreateAPortal(), ModelList, "PortalModel0");

            // Add the portal entity
            CPortalEntity ent = new CPortalEntity(scale, EntityData.m_position, EntityData.m_rotation, EntityData.m_entityName, mod);

            CurScreen.AddEntityNoSPT(ent);
        }
#endif

        public override void ForwardRender(CCamera Camera)
        {
            CVisual.Instance.DrawPortalBounds(Camera, m_wrldOBB, false);
            CVisual.Instance.DrawNormal(Camera, m_portalInfo.s_portalNormal, m_position, false);
        }

        public override void ForwardTranslucentRender(CCamera Camera)
        {
            CVisual.Instance.DrawPortalBounds(Camera, m_wrldOBB, false);
            CVisual.Instance.DrawNormal(Camera, m_portalInfo.s_portalNormal, m_position, false);
        }

        public override int GetEntityID()
        {
            return (int)EBasicEntityIDs.PortalEntity;
            //return base.GetEntityID();
        }
    }
}

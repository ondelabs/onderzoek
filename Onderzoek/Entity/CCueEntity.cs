﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

using System.Xml;

using Microsoft.Xna.Framework;

using Onderzoek.Visual;
using Onderzoek.ModelData;
using Onderzoek.Camera;
using Onderzoek.Audio;

namespace Onderzoek.Entity
{
    public class CCueEntity : CDynamicEntity
    {
        protected List<CModelOBB> m_originalOBB;
        protected Vector3 m_cameraPosition;
        protected Vector3 m_cameraDirection;
        protected Vector3 m_cameraUp;

        protected string m_cueName;
        protected CCueManager.SCueData m_cueID;

        public override void Dispose()
        {
            base.Dispose();
        }

        public CCueEntity(Vector3 Position, Vector3 Rotation, Matrix CameraWorld, string EntityName, List<CModelOBB> OBB, string CueName, bool PlayNow)
        {
#if EDITOR
            m_editorRotation = Rotation;
#endif

            Initialise(Position, Rotation, CameraWorld, EntityName, OBB, CueName, PlayNow);
        }

        public void Initialise(Vector3 Position, Vector3 Rotation, Matrix CameraWorld, string EntityName, List<CModelOBB> OBB, string CueName, bool PlayNow)
        {
            m_cueName = CueName;

#if EDITOR
            //editor_rotation = Rotation;
            this.m_propertyList = new List<CEntityProperty>();
            FillPropertyList();
#endif

            m_originalOBB = OBB;

            m_portalInfo = new SPortalInfo();
            m_material = new List<CMaterial>();

            m_entityName = EntityName;
            m_isDynamic = true;
            m_position = Position;
            m_isCollidable = false;
            m_canBeStoodOn = false;
            m_canBeParent = false;
            m_rotation = Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z);

            m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);

            m_cueID = CCueManager.Instance.InitialiseEntityCue(m_cueName, m_worldMatrix, CameraWorld, PlayNow);

            // Transform local space OBB corners to world space
            InitialiseEntityOBB(OBB);

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            UpdateCollisionSphere();
        }

        public void SetNewCueName(string CueName)
        {
            m_cueName = CueName;
        }

#if EDITOR
        public override void Update()
        {
            base.Update();
        }
#else
        public override void Update(float DT)
        {
            if (m_hidden == true)
            {
                return;
            }

            Matrix new_world = new Matrix();
            new_world.Translation = m_cameraPosition;
            new_world.Forward = m_cameraDirection;
            new_world.Up = m_cameraUp;

            CCueManager.Instance.UpdateEntityCue(m_worldMatrix, new_world, m_cueID);
        }
#endif

        public override void SetPosition(Vector3 Position)
        {
            m_position = Position;

            m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);
            // This will apply the parent matrix on to the entity if it's has a parent.
            // This MUST be called straight after creating the new world matrix!

            // Update the obbs
            UpdateEntityOBB(m_originalOBB);

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            // The following two lines will only needed to be updated if the bounding box size changes or if the dynamic object moves(translation) in the scene.

            // This needs to be updated for the calcualtion that will take place in the below base update.
            UpdateCollisionSphere();
        }

        // Needs to be called as soon as the entity becomes a child of another entity.
        public override void SetAsChild(Quaternion ParRot, Matrix PWrldMat, CEntity Parent)
        {
            // There's no need for a parent child relationship to occur.
        }

        // Needs to be called when the entity stops becoming a child of an entity.
        public override void SetAsParent(Quaternion ParRot, Matrix PWrldMat)
        {
            // There's no need for a parent child relationship to occur.
        }

        public override void Render(CCamera Camera)
        {
            m_cameraPosition = Camera.GetPosition();
            m_cameraDirection = Camera.GetDir();
            m_cameraUp = Camera.GetInvView().Up;
        }

        #region Editor stuff
#if EDITOR
        

        public override void FillPropertyList()
        {
            CEntityProperty modelSelectionEntityProperty = new CEntityProperty("Model", "", EPropertyGridInputType.ModelSelection);

            m_propertyList.Add(modelSelectionEntityProperty);

            CEntityProperty collidable = new CEntityProperty("Collides", "", EPropertyGridInputType.PullDownMultipleChoice);
            string[] strList = { "Yes", "No" };
            collidable.pullDownOptions = new List<string>(strList);
            m_propertyList.Add(collidable);

            CEntityProperty color = new CEntityProperty("Colour", "", EPropertyGridInputType.ColourDialogue);
            //string[] strList = { "Yes", "No" };
            //color= new List<string>(strList);
            m_propertyList.Add(color);



            //base.FillPropertyList();
        }

        public override int GetEntityID()
        {
            return 1; // z hard coding.
            
            //return base.getEntityNumber();
        }


        public override void UpdateFromPropertyList()
        {
            for(int i=0; i< m_propertyList.Count; i++)
                if (m_propertyList[i].propertyName.Equals("Model"))
                {
                    m_modelName = m_propertyList[i].propertyValue;
                }


            
            //base.UpdateFromPropertyList();
        }

        
#endif
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using Onderzoek.Visual;
using Onderzoek.ModelData;
using Onderzoek.Physics;
using Onderzoek.Camera;

namespace Onderzoek.Entity
{
    public class CDynDotXEntity : CDynamicEntity
    {
        public override void Dispose()
        {
            base.Dispose();
        }

        public CDynDotXEntity(Vector3 Position, Vector3 Rotation, string EntityName, CModel Model, List<CMaterial> Material, bool IsCollidable)
        {
#if EDITOR
            m_editorRotation = Rotation;
#endif

            Initialise(Position, Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z), EntityName, Model, Material, IsCollidable);
        }

        public void Initialise(Vector3 Position, Quaternion Rotation, string EntityName, CModel Model, List<CMaterial> Material, bool IsCollidable)
        {
            base.Initalise();

            m_portalInfo = new SPortalInfo();
            m_material = Material;
            //m_canBeParent = true; // FOR DEBUG PURPOSE ONLY

            m_entityName = EntityName;
            m_isDynamic = true;
            m_position = Position;
            m_modelData = Model;
            m_isCollidable = IsCollidable;
            m_rotation = Rotation;

            m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);

            // Transform local space OBB corners to world space
            InitialiseEntityOBB(m_modelData.GetOBB());

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            IsTranslucent(((CDotXModel)m_modelData).m_model);
        }

#if EDITOR
        public override void Update()
        {
            base.Update();
        }
#else
        //// DEBUG VARIABLES
        //float x = 0;
        //float tran = -0.5f;

        public override void Update(float DT)
        {
            if (m_hidden == true)
            {
                return;
            }

            //x += 0.001f;

            //m_position.X += tran;

            //if (m_position.X > 120.0f)
            //{
            //    tran = -0.5f;
            //}
            //if (m_position.X < -129.0f)
            //{
            //    tran = 0.5f;
            //}

            m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);
            // This will apply the parent matrix on to the entity if it's has a parent.
            // This MUST be called straight after creating the new world matrix!
            UpdateIfChild();

            // Update the obbs
            List<CModelOBB> OBB = m_modelData.GetOBB();
            UpdateEntityOBB(OBB);

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            // The following two lines will only needed to be updated if the bounding box size changes or if the dynamic object moves(translation) in the scene.

            // This needs to be updated for the calcualtion that will take place in the below base update.
            UpdateCollisionSphere();
        }
#endif

        public override void SetMaterialList(List<CMaterial> MaterialList)
        {
            base.SetMaterialList(MaterialList);

            IsTranslucent(((CDotXModel)m_modelData).m_model);
        }

        public override int GetEntityID()
        {
            return (int)EBasicEntityIDs.DynamicDotXEntity;
            //return base.GetEntityID();
        }

        public override void Render(CCamera Camera)
        {
            if (m_hidden == true)
            {
                return;
            }

            CVisual.Instance.Render(((CDotXModel)m_modelData).m_model, Camera, m_material, m_worldMatrix, m_opaqueParts);
        }

        public override void ForwardRender(CCamera Camera)
        {
            if (m_hidden == true)
            {
                return;
            }

            CVisual.Instance.ForwardRender(((CDotXModel)m_modelData).m_model, m_material, m_worldMatrix, m_opaqueParts);
        }

        public override void ForwardTranslucentRender(CCamera Camera)
        {
            if (m_hidden == true)
            {
                return;
            }

            CVisual.Instance.ForwardRender(((CDotXModel)m_modelData).m_model, m_material, m_worldMatrix, m_translucentParts);
        }

        public override void RenderForShadow(CCamera Camera)
        {
            if (m_hidden == true)
            {
                return;
            }

            CVisual.Instance.RenderForShadow(((CDotXModel)m_modelData).m_model, m_worldMatrix);
        }
    }
}

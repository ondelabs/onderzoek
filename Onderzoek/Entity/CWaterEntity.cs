﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

using Onderzoek;
using Onderzoek.Entity;
using Onderzoek.ModelData;
using Onderzoek.Visual;
using Onderzoek.Screens;
using Onderzoek.Camera;
using Onderzoek.Particle_System;

namespace Onderzoek.Entity
{
    public class CWaterEntity : CDynDotXEntity
    {
#if EDITOR
        float m_reflcRefrcOffset = 50.0f;
        float m_fresnelOffset = 0.3f;
        float m_waterMovementSpeed = 1.0f;
#endif

        protected int m_waterRTID = 0;
        protected Vector3 m_normalOfWater;
        protected Onderzoek.Camera.CWCamera m_reflectCamera;
        protected float m_dt = 0;
        protected List<int> m_hitSGIDs;

        public CWaterEntity(Vector3 Position, Vector3 Rotation, string EntityName, CModel Model, List<CMaterial> Material, float ReflecRefracOffset,
            float Speed, float FresnelOffset, ContentManager GameContentManager, CGameScreenHelper CurScreen,
            ref Dictionary<string, CModel.SModelData> ModelList)
            : base(Position, Rotation, EntityName, Model, Material, true)
        {
#if EDITOR
            m_modelName = Model.GetModelName();
            m_reflcRefrcOffset = ReflecRefracOffset;
            m_waterMovementSpeed = Speed;
#endif

            m_hitSGIDs = new List<int>();

            m_reflectCamera = new Onderzoek.Camera.CWCamera();

            m_manualRelationship = true;
            m_canBeParent = false;
            m_canBeStoodOn = false;
            m_simpleCollision = true;
            m_canBeParent = false;

#if ENGINE
            Matrix rot = Matrix.CreateFromQuaternion(m_rotation);
            m_normalOfWater = Vector3.TransformNormal(Vector3.Up, rot);

            m_waterRTID = CVisual.Instance.InitialiseForWaterRendering(m_normalOfWater, m_position,
                ReflecRefracOffset, Speed, this, FresnelOffset);
#endif

            // So that both Render and TranslucentRender are called.
            m_partTranslucent = true;
            m_translucentParts = m_opaqueParts;

#if EDITOR
            FillPropertyList();
#endif
        }

        public override void Dispose()
        {
#if ENGINE
            CVisual.Instance.DisposeOfRenderTargetForWater(m_waterRTID);
#endif

            base.Dispose();
        }

        public override int GetEntityID()
        {
            return GetEntityIDStatic();
        }

        public static int GetEntityIDStatic()
        {
            return (int)EBasicEntityIDs.e_water;
        }

#if EDITOR
        public override void SetMaterialList(List<CMaterial> MaterialList)
        {
            base.SetMaterialList(MaterialList);

            IsTranslucent(m_modelData.getBaseModel());

            // So that both Render and TranslucentRender are called.
            m_partTranslucent = true;
            m_translucentParts = m_opaqueParts;
        }

        public override void FillPropertyList()
        {
            base.FillPropertyList();

            m_propertyList = new List<CEntityProperty>();

            m_propertyList.Add(new CEntityProperty("Reflc_Refrc_Offset", m_reflcRefrcOffset.ToString(), EPropertyGridInputType.TextBox));
            m_propertyList.Add(new CEntityProperty("fresnel_Offset", m_fresnelOffset.ToString(), EPropertyGridInputType.TextBox));
            m_propertyList.Add(new CEntityProperty("Water_Movement_Speed", m_waterMovementSpeed.ToString(), EPropertyGridInputType.TextBox));
            m_propertyList.Add(new CEntityProperty("Evaporation_Speed", m_evaporationSpeed.ToString(), EPropertyGridInputType.TextBox));
            m_propertyList.Add(new CEntityProperty("Hide_At_Height", m_hideAtHeight.ToString(), EPropertyGridInputType.TextBox));
            m_propertyList.Add(new CEntityProperty("Hide_When_At_Height", m_hideWhenAtHeight.ToString(), EPropertyGridInputType.PullDownMultipleChoice));
            m_propertyList[m_propertyList.Count - 1].pullDownOptions = new List<string>();
            m_propertyList[m_propertyList.Count - 1].pullDownOptions.Add("True");
            m_propertyList[m_propertyList.Count - 1].pullDownOptions.Add("False");

            m_propertyList.Add(new CEntityProperty("Model", "", EPropertyGridInputType.ModelSelection));
            if (m_modelName != null)
            {
                string temp = m_modelName.Replace("Model/", "");
                temp = temp.Replace("Content/", "");
                temp = temp.Replace("Content\\\\", "");
                temp = temp.Replace("Content\\", "");
                temp = temp.Replace("Model\\\\", "");
                temp = temp.Replace("Model\\", "");
                temp = temp.Replace("..\\", "");
                m_modelName = temp;
                m_propertyList[m_propertyList.Count - 1].propertyValue = temp;
            }

            m_propertyList.Add(new CEntityProperty("Material", "", EPropertyGridInputType.MaterialSelection));
        }

        public override void UpdateFromPropertyList()
        {
            float flt = 0;
            if (CToolbox.StringToFloat(m_propertyList[0].propertyValue, ref flt) == false)
            {
                m_propertyList[0].propertyValue = m_reflcRefrcOffset.ToString();
            }
            else
            {
                m_reflcRefrcOffset = flt;
            }

            flt = 0;
            if (CToolbox.StringToFloat(m_propertyList[1].propertyValue, ref flt) == false)
            {
                m_propertyList[1].propertyValue = m_fresnelOffset.ToString();
            }
            else
            {
                m_fresnelOffset = flt;
            }

            flt = 0;
            if (CToolbox.StringToFloat(m_propertyList[2].propertyValue, ref flt) == false)
            {
                m_propertyList[2].propertyValue = m_waterMovementSpeed.ToString();
            }
            else
            {
                m_waterMovementSpeed = flt;
            }

            flt = 0;
            if (CToolbox.StringToFloat(m_propertyList[3].propertyValue, ref flt) == false)
            {
                m_propertyList[3].propertyValue = m_evaporationSpeed.ToString();
            }
            else
            {
                m_evaporationSpeed = flt;
            }

            flt = 0;
            if (CToolbox.StringToFloat(m_propertyList[4].propertyValue, ref flt) == false)
            {
                m_propertyList[4].propertyValue = m_hideAtHeight.ToString();
            }
            else
            {
                m_hideAtHeight = flt;
            }

            bool bl = false;
            if (CToolbox.StringToBool(m_propertyList[5].propertyValue, ref bl) == false)
            {
                m_propertyList[5].propertyValue = m_hideWhenAtHeight.ToString();
            }
            else
            {
                m_hideWhenAtHeight = bl;
            }

            m_modelName = m_propertyList[6].propertyValue;
            m_materialName = m_propertyList[7].propertyValue;
        }

        public override void Update()
        {
            base.Update();
        }
#else
        public override void Update(float DT)
        {
            if (m_hidden == true)
            {
                return;
            }

            m_dt = DT;

            m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);
            // This will apply the parent matrix on to the entity if it's has a parent.
            // This MUST be called straight after creating the new world matrix!
            UpdateIfChild();

            // Update the obbs
            List<CModelOBB> OBB = m_modelData.GetOBB();
            UpdateEntityOBB(OBB);

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            // The following two lines will only needed to be updated if the bounding box size changes or if the dynamic object moves(translation) in the scene.

            // This needs to be updated for the calcualtion that will take place in the below base update.
            UpdateCollisionSphere();
        }

        public static void LoadEntityData(CEntityDataStore EntityData, Onderzoek.Screens.CGameScreenHelper CurScreen,
            ref Dictionary<string, CModel.SModelData> ModelList, ref ContentManager Content)
        {
            float fresnel_offset = 0.3f;
            float reflc_refrc_offset = 50.0f;
            float water_speed = 1.0f;
            string mod_name = "";
            string mat_name = "";

            for (int i = 0; i < EntityData.m_properties.Count; ++i)
            {
                if (EntityData.m_properties[i].propertyName == "Reflc_Refrc_Offset")
                {
                    CToolbox.StringToFloat(EntityData.m_properties[i].propertyValue, ref reflc_refrc_offset);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "fresnel_offset")
                {
                    CToolbox.StringToFloat(EntityData.m_properties[i].propertyValue, ref fresnel_offset);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Water_Movement_Speed")
                {
                    CToolbox.StringToFloat(EntityData.m_properties[i].propertyValue, ref water_speed);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Model")
                {
                    mod_name = "Model/" + EntityData.m_properties[i].propertyValue;
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Material")
                {
                    mat_name = EntityData.m_properties[i].propertyValue;
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
            }

            CDotXModel water_mod = new CDotXModel();
            water_mod = (CDotXModel)water_mod.Initialise(Content, mod_name, ModelList);
            List<CMaterial> water_mat = null;
            if (mat_name == "" || mat_name == null)
            {
                water_mat = water_mod.GetMaterials(Content, mat_name);
            }
            else
            {
                CMaterialManager.loadMaterialXML(water_mod, mat_name, ref water_mat, ref Content);
            }

            // Create the entity
            CWaterEntity ent = new CWaterEntity(EntityData.m_position, EntityData.m_rotation, EntityData.m_entityName, water_mod, water_mat, reflc_refrc_offset,
                water_speed, fresnel_offset, Content, CurScreen, ref ModelList);
            CurScreen.AddEntityNoSPT(ent);
        }
#endif

        /// <summary>
        /// This will be called by CSBVisual when it's about to render the reflection.
        /// </summary>
        /// <param name="Camera">The main camera</param>
        /// <param name="ReflectCamera">The reflect camera which is used to reflect the world.</param>
        public void SetUpCamera(CCamera Camera, out Onderzoek.Camera.CWCamera ReflectCamera, Plane ClipPlane)
        {
            Vector3 pos = Camera.GetPosition();

            Vector3 reflc_vector = CToolbox.UnitVector(Vector3.Reflect(Camera.GetDir(), ClipPlane.Normal));

            pos.Y = m_position.Y - (pos.Y - m_position.Y);
            m_reflectCamera.SetPosition(pos);
            m_reflectCamera.SetProjection(Camera.GetProjection());

            Vector3 up = CToolbox.CrossProduct(reflc_vector, Camera.GetRotationMatrix().Right);

            Matrix view = Matrix.CreateLookAt(pos, pos + reflc_vector, up);

            m_reflectCamera.SetView(view);

            Matrix rot_mat = Matrix.Invert(view);
            rot_mat.Translation = Vector3.Zero;
            m_reflectCamera.SetRotationMatrix(rot_mat);

            ReflectCamera = m_reflectCamera;
        }

        public override void Render(CCamera Camera)
        {
            if (m_hidden == true)
            {
                return;
            }

#if EDITOR
            CVisual.Instance.Render(m_modelData.getBaseModel(), Camera, m_material, m_worldMatrix, m_opaqueParts);
#else
            float dif = Camera.GetPosition().Y - m_position.Y;

            if( dif > 0 )
            {
                CVisual.Instance.SaveWaterRenderingData(m_waterRTID);
            }
#endif
        }

        public override void ForwardTranslucentRender(CCamera Camera)
        {
            if (m_hidden == true)
            {
                return;
            }

#if EDITOR
            CVisual.Instance.ForwardRender(m_modelData.getBaseModel(), m_material, m_worldMatrix, m_translucentParts);
#else
            CVisual.Instance.RenderWater(m_waterRTID, ((CDotXModel)m_modelData).m_model, m_material, m_worldMatrix, m_translucentParts);
#endif
        }

        public override void ForwardRender(CCamera Camera)
        {
            // We don't want it render anything here as it will render itself again.
        }

        public override void RenderForShadow(CCamera Camera)
        {
        }
    }
}

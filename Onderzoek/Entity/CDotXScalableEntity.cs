﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Xml;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

using Onderzoek.Visual;
using Onderzoek.ModelData;
using Onderzoek.Camera;

namespace Onderzoek.Entity
{
    public class CDotXScalableEntity : CStaticEntity
    {
        protected bool m_initialCollidableValue;
        public Vector3 editorOnlyScale;

        //string m_modelName, m_materialName;

        public override void Dispose()
        {
            base.Dispose();
        }

        public CDotXScalableEntity(Vector3 Position, Vector3 Rotation, string EntityName, CModel Model, List<CMaterial> Material, bool IsCollidable, bool CanBeStoodOn)
        {

            //m_materialName = "";
            //m_modelName = "";

            Initialise(Position, Rotation, EntityName, Model, Material, IsCollidable, CanBeStoodOn);
        }

        //public override Vector3 Scale
        //{
        //    get
        //    {
        //        return base.Scale;
        //    }
        //    set
        //    {
        //        base.Scale = value;
        //    }
        //}


        public void Initialise(Vector3 Position, Vector3 Rotation, string EntityName, CModel Model, List<CMaterial> Material, bool IsCollidable, bool CanBeStoodOn)
        {
#if EDITOR
            m_modelName = Model.GetModelName();
            m_editorRotation = Rotation;
#endif

            m_portalInfo = new SPortalInfo();
            m_material = Material;

            m_rotation = Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z);

            m_entityName = EntityName;
            m_isDynamic = false;
            m_position = Position;
            m_modelData = Model;
            m_initialCollidableValue = IsCollidable;
            m_isCollidable = IsCollidable;
            m_canBeStoodOn = CanBeStoodOn;

            m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);

            // Transform local space OBB corners to world space
            InitialiseEntityOBB(Model.GetOBB());

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            UpdateCollisionSphere();

            //CEntityProperty modelList = new CEntityProperty("Model", "Test", EPropertyGridInputType.PullDownMultipleChoice);

            //modelList.pullDownOptions.Add( getPossibleModelsForSelectedType()

            //m_propertyList.Add();

            IsTranslucent(((CDotXModel)m_modelData).m_model);

#if EDITOR
            FillPropertyList();
#endif
        }

        public override void Reinitialise()
        {
            m_isCollidable = m_initialCollidableValue;
            m_hidden = false;

            // Transform local space OBB corners to world space
            InitialiseEntityOBB(m_modelData.GetOBB());

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            UpdateCollisionSphere();
        }

        public override void SetMaterialList(List<CMaterial> MaterialList)
        {
            base.SetMaterialList(MaterialList);

            IsTranslucent(((CDotXModel)m_modelData).m_model);
        }

        public override void Render(CCamera Camera)
        {
            if (m_hidden == true)
            {
                return;
            }
            //m_position = Vector3.Zero;

            CVisual.Instance.Render(((CDotXModel)m_modelData).m_model, Camera, m_material, m_worldMatrix, m_opaqueParts);
        }

        public override void ForwardTranslucentRender(CCamera Camera)
        {
            if (m_hidden == true)
            {
                return;
            }

            CVisual.Instance.ForwardRender(((CDotXModel)m_modelData).m_model, m_material, m_worldMatrix, m_translucentParts);
        }

        public override void RenderForShadow(CCamera Camera)
        {
            if (m_hidden == true)
            {
                return;
            }

            CVisual.Instance.RenderForShadow(((CDotXModel)m_modelData).m_model, m_worldMatrix);
        }

#if EDITOR
        public override void ForwardRender(CCamera Camera)
        {
            m_worldMatrix = Matrix.CreateScale(editorOnlyScale.X) * Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);
            if (m_hidden == true)
            {
                return;
            }

            CVisual.Instance.ForwardRender(((CDotXModel)m_modelData).m_model, m_material, m_worldMatrix, m_opaqueParts, Scale);
        }

        public override Vector3 Scale
        {
            get
            {
                return editorOnlyScale;
            }
            set
            {
                //base.Scale = value;
                editorOnlyScale = value;
                //Scale = value;
            }
        }

        public override void FillPropertyList()
        {
            this.m_propertyList = new List<CEntityProperty>();

            CEntityProperty modelSelectionEntityProperty = new CEntityProperty("Model", "", EPropertyGridInputType.ModelSelection);

            m_propertyList.Add(modelSelectionEntityProperty);

            string temp = m_modelName.Replace("Model/", "");
            temp = temp.Replace("Content/", "");
            temp = temp.Replace("Content\\\\", "");
            temp = temp.Replace("Content\\", "");
            temp = temp.Replace("Model\\\\", "");
            temp = temp.Replace("Model\\", "");
            temp = temp.Replace("..\\", "");
            m_modelName = temp;
            m_propertyList[m_propertyList.Count - 1].propertyValue = m_modelName;

            modelSelectionEntityProperty = new CEntityProperty("Material", "", EPropertyGridInputType.MaterialSelection);

            m_propertyList.Add(modelSelectionEntityProperty);

            m_propertyList.Add(new CEntityProperty("Collidable", m_isCollidable.ToString(), EPropertyGridInputType.PullDownMultipleChoice));
            m_propertyList[m_propertyList.Count - 1].pullDownOptions = new List<string>();
            m_propertyList[m_propertyList.Count - 1].pullDownOptions.Add("True");
            m_propertyList[m_propertyList.Count - 1].pullDownOptions.Add("False");

            m_propertyList.Add(new Onderzoek.Entity.CEntityProperty("Can_Be_stood_On", m_canBeStoodOn.ToString(), Onderzoek.Entity.EPropertyGridInputType.PullDownMultipleChoice));
            m_propertyList[m_propertyList.Count - 1].pullDownOptions = new List<string>();
            m_propertyList[m_propertyList.Count - 1].pullDownOptions.Add("True");
            m_propertyList[m_propertyList.Count - 1].pullDownOptions.Add("False");
        }

        public override void UpdateFromPropertyList()
        {
            for (int i = 0; i < m_propertyList.Count; i++)
            {
                if (m_propertyList[i].propertyName.Equals("Model"))
                {
                    m_modelName = m_propertyList[i].propertyValue;
                }

                if (m_propertyList[i].propertyName.Equals("Material"))
                {
                    m_materialName = m_propertyList[i].propertyValue;
                }

                if (m_propertyList[i].propertyName.Equals("Collides"))
                {
                    bool bl = false;
                    if (CToolbox.StringToBool(m_propertyList[i].propertyValue, ref bl) == false)
                    {
                        m_propertyList[i].propertyValue = m_isCollidable.ToString();
                    }
                    else
                    {
                        m_isCollidable = bl;
                    }
                }

                if (m_propertyList[i].propertyName.Equals("Can_Be_stood_On"))
                {
                    bool bl = false;
                    if (CToolbox.StringToBool(m_propertyList[i].propertyValue, ref bl) == false)
                    {
                        m_propertyList[i].propertyValue = m_canBeStoodOn.ToString();
                    }
                    else
                    {
                        m_canBeStoodOn = bl;
                    }
                }
            }

        }
#else
        public static void LoadEntityData(CEntityDataStore EntityData, Onderzoek.Screens.CGameScreenHelper CurScreen,
            ref Dictionary<string, CModel.SModelData> ModelList, ref ContentManager Content)
        {
            string model_name = "";
            string material = "";
            bool collidable = true;
            bool can_stand = false;

            for (int i = 0; i < EntityData.m_properties.Count; ++i)
            {
                if (EntityData.m_properties[i].propertyName == "Model")
                {
                    model_name = "Model\\" + EntityData.m_properties[i].propertyValue;
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Material")
                {
                    material = EntityData.m_properties[i].propertyValue;
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Collidable")
                {
                    CToolbox.StringToBool(EntityData.m_properties[i].propertyValue, ref collidable);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
                else if (EntityData.m_properties[i].propertyName == "Can_Be_stood_On")
                {
                    CToolbox.StringToBool(EntityData.m_properties[i].propertyValue, ref can_stand);
                    EntityData.m_properties.RemoveAt(i);
                    --i;
                }
            }

            CDotXModel dotx_mod = new CDotXModel();
            dotx_mod = (CDotXModel)dotx_mod.Initialise(Content, model_name, ModelList);
            List<CMaterial> dotx_mat = dotx_mod.GetMaterials(Content, model_name);

            // Create the model
            CDotXEntity ent = new CDotXEntity(EntityData.m_position, EntityData.m_rotation, EntityData.m_entityName, dotx_mod, dotx_mat, collidable, can_stand);
            CurScreen.AddEntityNoSPT(ent);
        }
#endif

        public override int GetEntityID()
        {
            return (int)EBasicEntityIDs.DotXEntity; // z hard coding.
            
            //return base.getEntityNumber();
        }

        public override void RecheckModelAndMaterial(ContentManager content, Dictionary<string, CModel.SModelData> modelList)
        {
#if ENGINE
            CDotXModel temp_modelr = new CDotXModel();
            temp_modelr = (CDotXModel)temp_modelr.Initialise(content, "Model\\"+m_modelName, modelList);

            SetModel(temp_modelr);
            if (m_materialName != "")
            {
                m_material = CMaterialManager.Instance.getMaterialList(content, m_materialName);

                //do something
            }
            temp_modelr.GetBoundingBoxFromModel(m_modelName);
#endif

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Microsoft.Xna.Framework;

using Onderzoek.Entity;
using Onderzoek.Visual;
using Onderzoek.Physics;
using Onderzoek.Screens;
using Onderzoek.ModelData;

namespace Onderzoek.SceneGraph
{
    public class CSceneGraph
    {
        // Internal use, during the sorting of the entity list, which is just before the setting up of the SPT.
        struct SSort
        {
            public SSort(string Type, CEntity Entity, string ModelID, int EffectID)
            {
                s_type = Type;
                s_entity = Entity;
                s_modelID = ModelID;
                s_effectID = EffectID;
            }

            public string s_type;
            public CEntity s_entity;
            public string s_modelID;
            public int s_effectID;
        }

        int m_notParent = -1;
        CSGNode m_rootNode;
        List<CSGNode> m_parentNodes; // might not need this
        List<CSGNode> m_childNodes; // might not need this
        int m_minTranslucentEntityID;

        // This stores all the dynamic entities. When an entity calls the scene graph then
        // the entity will need to pass it the scene graph id, which is the key in the
        // dictinary, so that it can be identified and parent child relationships can be altered.
        Dictionary<int, CSGNode> m_allDynEntities;

        // This MUST be called before the first time the SPT is setup.
        public CSceneGraph(ref List<CEntity> EntityList, List<CDynamicEntity> DynEntityList)
        {
            SortEntityList(ref EntityList, DynEntityList);
        }

        public void Dispose()
        {
            if (m_parentNodes != null)
            {
                for (int i = 0; i < m_parentNodes.Count; ++i)
                {
                    ResetToParentIfChild(m_parentNodes[i].m_children);
                }
            }

            m_minTranslucentEntityID = -1;
            m_rootNode = null;
            if (m_parentNodes != null)
            {
                m_parentNodes.Clear();
                m_parentNodes = null;
            }
            if (m_childNodes != null)
            {
                m_childNodes.Clear();
                m_childNodes = null;
            }
            if (m_allDynEntities != null)
            {
                m_allDynEntities.Clear();
                m_allDynEntities = null;
            }
        }

        private void ResetToParentIfChild(List<CSGNode> Node)
        {
            for (int i = 0; i < Node.Count; ++i)
            {
                if (Node[i].m_children.Count != 0)
                {
                    ResetToParentIfChild(Node[i].m_children);
                }
                else
                {
                    Node[i].m_entity.SetAsParent(Node[i].m_entity.GetWorldRotation(),
                        Node[i].m_entity.GetWorldMatrix());
                }
            }
        }

        public void SortEntityList(ref List<CEntity> EntityList, List<CDynamicEntity> DynEntityList)
        {
            m_minTranslucentEntityID = -1;
            m_rootNode = new CSGNode();
            m_parentNodes = new List<CSGNode>();
            m_childNodes = new List<CSGNode>();
            m_allDynEntities = new Dictionary<int, CSGNode>();

            List<CEntity> translucent_entities = new List<CEntity>();
            SortOutTranslucentEntities(ref EntityList, ref translucent_entities);

            SortByModel(ref EntityList);

            if( translucent_entities.Count > 0 )
            {
                m_minTranslucentEntityID = EntityList.Count;
            }

            // Last thing is to concatenate the opaque and translucent lists.
            EntityList.AddRange(translucent_entities);

            // Setup the scene for updates
            SortSceneForUpdates(DynEntityList);
        }

        void SortSceneForUpdates(List<CDynamicEntity> EntityList)
        {
            int sgid = 0;
            List<CDynamicEntity> entity_list = new List<CDynamicEntity>();
            entity_list.AddRange(EntityList);

            m_rootNode.m_parent = null;
            m_rootNode.m_entity = null;

            // First find all the parent entities.
            int count = 0;
            List<CDynamicEntity> parent_list = new List<CDynamicEntity>();
            for (int i = 0; i < entity_list.Count; ++i)
            {
                // If the current entity can be a parent then there needs to be a
                // check to see if any entity meets the criteria to be it's child.
                if (entity_list[i].CanBeParent == true)
                {
                    parent_list.Add(entity_list[i]);
                    CSGNode temp = new CSGNode(m_rootNode, entity_list[i], count);
                    m_rootNode.m_children.Add(temp);

                    // This is so that the scene graph data can be accessed by
                    // CCollision to modify the parent/child relationships.
                    m_allDynEntities.Add(sgid++, temp);
                    temp.m_entity.SGID = m_allDynEntities.Count - 1;

                    m_parentNodes.Add(temp); // might not need this

                    entity_list.RemoveAt(i);
                    --i;
                    ++count;
                }
            }

            // Now go through the entity list again and add any children to the parents.
            for (int i = 0; i < entity_list.Count; ++i)
            {
                bool is_child = false;
                for (int j = 0; j < parent_list.Count; ++j)
                {
                    if (i < 0)
                    {
                        ++i;
                    }
                    if (entity_list[i].ManualRelationship == false)
                    {
                        if (parent_list[j].IsCollidable() == false || (entity_list[i].DissociationID & parent_list[j].AssociationID) != 0
                            || parent_list[j].IsHidden() == true)
                        {
                            continue;
                        }

                        if (CCollision.Instance.IsAParentOfB(parent_list[j].GetBB(), entity_list[i].GetBB(), entity_list[i].GetWorldPosition(), parent_list[j]) == true)
                        {
                            CSGNode temp = new CSGNode(m_rootNode.m_children[j], entity_list[i], m_notParent);
                            m_rootNode.m_children[j].m_children.Add(temp);
                            entity_list[i].SetAsChild(parent_list[j].GetWorldRotation(), parent_list[j].GetWorldMatrix(), parent_list[j]);

                            // This is so that the scene graph data can be accessed by
                            // CCollision to modify the parent/child relationships.
                            m_allDynEntities.Add(sgid++, temp);
                            temp.m_entity.SGID = m_allDynEntities.Count - 1;

                            m_childNodes.Add(temp); // might not need this

                            entity_list.RemoveAt(i);
                            --i;
                            is_child = true;

                            break;
                        }
                    }
                }

                // If it isn't a child of any of the parents then just make it a child of the root node.
                if (is_child == false)
                {
                    CSGNode temp = new CSGNode(m_rootNode, entity_list[i], m_notParent);
                    m_rootNode.m_children.Add(temp);

                    // This is so that the scene graph data can be accessed by
                    // CCollision to modify the parent/child relationships.
                    m_allDynEntities.Add(sgid++, temp);
                    temp.m_entity.SGID = m_allDynEntities.Count - 1;

                    m_childNodes.Add(temp); // might not need this

                    entity_list.RemoveAt(i);
                    --i;
                }
            }
        }

        public bool CreateChildToParentRelation(int ChildID, int ParentID)
        {
            if (SetChildToParent(ChildID, ParentID) == true)
            {
                CSGNode child;
                m_allDynEntities.TryGetValue(ChildID, out child);
                child.m_entity.ManualRelationship = true;

                return true;
            }
            else
            {
                return false;
            }
        }

        public bool BreakChildToParentRelation(int ChildID, int ParentID)
        {
            if( NoLongerChildOfParent(ChildID, ParentID) == true )
            {
                CSGNode child;
                m_allDynEntities.TryGetValue(ChildID, out child);
                child.m_entity.ManualRelationship = false;

                return true;
            }
            else
            {
                return false;
            }
        }

        public bool SetChildToParent(int ChildID, int ParentID)
        {
            CSGNode parent;
            if (m_allDynEntities.TryGetValue(ParentID, out parent) == false)
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSceneGraph", "ONDERZOEK ERROR: Error in Scene Graph - Cannot find parent entity object in m_allDynEntities.");
                return false;
            }

            CSGNode child;
            if (m_allDynEntities.TryGetValue(ChildID, out child) == false)
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSceneGraph", "ONDERZOEK ERROR: Error in Scene Graph - Cannot find child entity object in m_allDynEntities.");
                return false;
            }

            if (parent.m_parentID != child.m_parent.m_parentID || parent.m_parentID == -1 && child.m_parent.m_parentID == -1)
            {
                parent.m_children.Add(child);
                if (child.m_parent.m_children.Remove(child) == false)
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSceneGraph", "ONDERZOEK ERROR: Error in Scene Graph - Cannot remove object from parent list.");
                }
                child.m_parent = parent;

                return true;
            }

            return false;
        }

        public CEntity GetParentOfEntity(int ChildID)
        {
            CSGNode child;
            if (m_allDynEntities.TryGetValue(ChildID, out child) == false)
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSceneGraph", "ONDERZOEK ERROR: Error in Scene Graph - Cannot find child entity object in m_allDynEntities.");
                return null;
            }

            if (child.m_parent.m_parentID != m_notParent)
            {
                return child.m_parent.m_entity;
            }

            return null;
        }

        public bool NoLongerChildOfParent(int ChildID, int ParentID)
        {
            CSGNode parent;
            // no need for check here since we're checking before hand
            if (m_allDynEntities.TryGetValue(ParentID, out parent) == false)
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSceneGraph", "ONDERZOEK ERROR: Error in Scene Graph - Cannot find parent entity object in m_allDynEntities.");
                return false;
            }

            CSGNode child;
            // no need for check here since we're checking before hand
            if (m_allDynEntities.TryGetValue(ChildID, out child) == false)
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSceneGraph", "ONDERZOEK ERROR: Error in Scene Graph - Cannot find child entity object in m_allDynEntities.");
                return false;
            }

            m_rootNode.m_children.Add(child);
            if (parent.m_children.Remove(child) == false)
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSceneGraph", "ONDERZOEK ERROR: Error in Scene Graph - Cannot remove object from parent list.");
                return false;
            }
            child.m_parent = m_rootNode;

            return true;
        }

        // This needs to be called every update frame for the update to take place.
        public void Update(GameTime DT)
        {
            float dt = DT.ElapsedGameTime.Milliseconds / 1000.0f;

            // No need to update m_rootNode itself, and so pass the root's children on to the other update function.
            for (int i = 0; i < m_rootNode.m_children.Count; ++i)
            {
                Update(m_rootNode.m_children[i], dt);
            }
        }

        // Internal update which is used to update all the dynamic entities.
        void Update(CSGNode Node, float DT)
        {
            // First update the current entity.
            Node.m_entity.Update(DT);
            // Update the entity's nodes in the tree
            Node.m_entity.STUpdateEntityNodeInTree();

            // Now update the children and pass the current entity's position and rotation.
            for (int i = 0; i < Node.m_children.Count; ++i)
            {
                Node.m_children[i].m_entity.ParWrldMat = Node.m_entity.GetWorldMatrix();
                Node.m_children[i].m_entity.ParRotation = Node.m_entity.GetWorldRotation();
                // Update the entity as normal
                Update(Node.m_children[i], DT);
            }
        }

        void SortOutTranslucentEntities(ref List<CEntity> EntityList, ref List<CEntity> TranslucentEntities)
        {
            // Creating a list of translucent entities
            for(int i = 0; i < EntityList.Count; ++i)
            {
                if (EntityList[i].IsPartialTranslucent() == true || EntityList[i].IsWholeTranslucent() == true)
                {
                    TranslucentEntities.Add(EntityList[i]);
                    EntityList.RemoveAt(i);
                    --i;
                }
            }

            SortByModel(ref TranslucentEntities);
        }

        // The list will be ordered so that the same models are rendered together.
        void SortByModel(ref List<CEntity> EntityList)
        {
            Dictionary<string, List<SSort>> sort_list = new Dictionary<string, List<SSort>>();
            List<string> model_ids = new List<string>();

            foreach (CEntity entity in EntityList)
            {
                CModel temp_model = entity.GetModel();
                string model_id = "Model";
                if( temp_model != null )
                {
                    model_id = temp_model.GetModelName();
                }
                if (sort_list.ContainsKey(model_id) == false)
                {
                    model_ids.Add(model_id);
                    sort_list.Add(model_id, new List<SSort>());
                }

                List<CMaterial> mat_list = entity.GetMaterialList();
                int effect_id = -1;
                if( mat_list != null && mat_list.Count > 0 )
                {
                    effect_id = mat_list[0].m_effect;
                }

                sort_list[model_id].Add(new SSort(entity.ToString(), entity, model_id, effect_id));
            }

            EntityList.Clear();
            foreach (string model_id in model_ids)
            {
                List<SSort> temp = sort_list[model_id];
                SortByEntity(ref temp);
                SortByEffectID(ref temp);
                foreach (SSort entity in temp)
                {
                    EntityList.Add(entity.s_entity);
                }
            }
        }

        void SortByEntity(ref List<SSort> EntityList)
        {
            Dictionary<string, List<SSort>> sort_list = new Dictionary<string, List<SSort>>();
            List<string> entity_types = new List<string>();

            foreach (SSort entity in EntityList)
            {
                if (sort_list.ContainsKey(entity.s_type) == false)
                {
                    entity_types.Add(entity.s_type);
                    sort_list.Add(entity.s_type, new List<SSort>());
                }

                sort_list[entity.s_type].Add(entity);
            }

            EntityList.Clear();
            foreach (string entity_type in entity_types)
            {
                foreach (SSort entity in sort_list[entity_type])
                {
                    EntityList.Add(entity);
                }
            }
        }

        void SortByEffectID(ref List<SSort> EntityList)
        {
            Dictionary<int, List<SSort>> sort_list = new Dictionary<int, List<SSort>>();
            List<int> effect_ids = new List<int>();

            foreach (SSort entity in EntityList)
            {
                if (sort_list.ContainsKey(entity.s_effectID) == false)
                {
                    effect_ids.Add(entity.s_effectID);
                    sort_list.Add(entity.s_effectID, new List<SSort>());
                }

                sort_list[entity.s_effectID].Add(entity);
            }

            EntityList.Clear();
            foreach (int effect_id in effect_ids)
            {
                foreach (SSort entity in sort_list[effect_id])
                {
                    EntityList.Add(entity);
                }
            }
        }

        // Should try this with Shadow render lists, except no need to seperate translucent entities.
        // This will return two list. The translucent entities will be removed from the EntityList and put into the Translucent list.
        public void SortRenderList(ref List<int> EntityIDList, ref List<int> Translucent, List<CEntity> EntityList)
        {
            Translucent.Clear();
            QuickSortEntityList(ref EntityIDList, 0, EntityIDList.Count);

            if (m_minTranslucentEntityID != -1.0f)
            {
                for (int i = EntityIDList.Count - 1; i >= 0; --i)
                {
                    if (EntityIDList[i] >= m_minTranslucentEntityID)
                    {
                        Translucent.Add(EntityIDList[i]);
                        // If the whole entity is translucent then remove from the EntityIDList.
                        // Otherwise leave is since there are parts which will need to be rendered normally since they are 100% opaque.
                        if (EntityList[EntityIDList[i]].IsWholeTranslucent() == true)
                        {
                            EntityIDList.RemoveAt(i);
                        }
                        //--i;
                    }
                    else
                    {
                        // No need to look on since EntityList is in ascending order.
                        break;
                    }
                }
            }
        }


        // A basic quick sort algorithm, which is only used by CToolBox::SortEntitiesByDistFromCam
        private void QuickSortEntityList(ref List<int> EntityList, int BegIndex, int EndIndex)
        {
            int dif = EndIndex - BegIndex;

            if (dif <= 1)
            {
                return;
            }

            List<int> pos = new List<int>();
            List<int> neg = new List<int>();
            int half = BegIndex + (dif / 2);

            for (int i = BegIndex; i < half; ++i)
            {
                if (EntityList[i] > EntityList[half])
                {
                    pos.Add(EntityList[i]);
                }
                else
                {
                    neg.Add(EntityList[i]);
                }
            }
            for (int i = half + 1; i < EndIndex; ++i)
            {
                if (EntityList[i] > EntityList[half])
                {
                    pos.Add(EntityList[i]);
                }
                else
                {
                    neg.Add(EntityList[i]);
                }
            }

            neg.Add(EntityList[half]);

            for (int i = 0; i < neg.Count; ++i)
            {
                EntityList[BegIndex + i] = neg[i];
            }
            for (int i = neg.Count; i < neg.Count + pos.Count; ++i)
            {
                EntityList[BegIndex + i] = pos[i - neg.Count];
            }

            QuickSortEntityList(ref EntityList, BegIndex, BegIndex + neg.Count - 1);
            QuickSortEntityList(ref EntityList, BegIndex + neg.Count, EndIndex);
        }

    }
}

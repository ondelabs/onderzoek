﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Onderzoek.Entity;

namespace Onderzoek.SceneGraph
{
    class CSGNode
    {
        public CSGNode m_parent;
        public CDynamicEntity m_entity;
        public List<CSGNode> m_children;
        public int m_parentID;

        public CSGNode()
        {
            m_parent = null;
            m_entity = null;
            m_children = new List<CSGNode>();
            m_parentID = -1;
        }

        public CSGNode(CSGNode Parent, CDynamicEntity Entity, int ParentID)
        {
            m_parent = Parent;
            m_entity = Entity;
            m_children = new List<CSGNode>();
            m_parentID = ParentID;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Onderzoek.SceneGraph
{
    class CAsynchronousData
    {
        public CSGNode m_sgNode;
        public ManualResetEvent m_manualResetEvent;
        public float m_dt;

        public void Dispose()
        {
            m_manualResetEvent = null;
            m_sgNode = null;
        }

        public CAsynchronousData()
        {
            m_manualResetEvent = new ManualResetEvent(false);
            m_dt = 0;
        }
    }
}

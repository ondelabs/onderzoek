//#define MAXFPS //<- Enable this to max out the fps <- ONLY FOR DEBUGGING!

using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.IO;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
#if XBOX360
using Microsoft.Xna.Framework.GamerServices;
#endif
using Microsoft.Xna.Framework.Net;
//using System.Windows.Forms;

using Onderzoek.Visual;
using Onderzoek.Input;
using Onderzoek.GUI;
using Onderzoek.Screens;


namespace Onderzoek
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        // Visual things
        protected CVisual m_visual;
        protected Onderzoek.Audio.CCueManager m_cueManager;

        // Input
        public CInput m_curInput; // The current input, this will be pointing to either m_x360Input or m_pcInput.
        public CInputData m_inputData;
        protected CX360Input m_x360Input;
#if WINDOWS
        protected CPCInput m_pcInput;
#endif

        // Screens
        protected List<CScreen> m_screens;
        protected List<CScreen> m_updateScreens;

        protected float m_totalSimTime;
        protected float m_garbageCollectTime;

        // Unit Manager and it's threading stuff.
        CLogger m_logger;
        Thread m_loggerThread;
        CSharedLoggerMessenger m_sharedLoggerMessenger;


        /// <summary>
        /// The engines Game1.
        /// </summary>
        /// <param name="ByPassVisualCreation">If you are creating your own derived verion of CVisual then set this to true.</param>
        public Game1(bool ByPassVisualCreation)
        {
#if ENGINE
            // Start the logger
            m_sharedLoggerMessenger = new CSharedLoggerMessenger();
            m_logger = new CLogger();
            m_loggerThread = new Thread(new ThreadStart(m_logger.ThreadRun));
            try
            {
                m_loggerThread.Start();
                //m_unitManagerThread.Join(); <- Can't use here because it freezes the called thread.
            }
            catch (ThreadStateException e)
            {
                Console.WriteLine(e);
            }
            catch (ThreadInterruptedException e)
            {
                Console.WriteLine(e);
            }

            if (ByPassVisualCreation == false)
            {
                m_visual = new CVisual(this);
            }
#endif
            m_cueManager = new Onderzoek.Audio.CCueManager();
#if MAXFPS
            IsFixedTimeStep = false;
            m_visual.GetGraphicsDeviceManager().SynchronizeWithVerticalRetrace = false;
#endif

            Content.RootDirectory = "Content";

#if XBOX360
            this.Components.Add(new GamerServicesComponent(this));

            //// Listen for invite notification events.
            //NetworkSession.InviteAccepted += (sender, e) => CNetworkSessionComponent.InviteAccepted(this, e);

            //// To test the trial mode behavior while developing your game,
            //// uncomment this line:
            //// Guide.SimulateTrialMode = true;

            //this.Components.Add(new GamerServicesComponent(this));
#endif
            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "Game1", "Passed Game1 Constructor");
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // If the current hardware doesn't support sm 2.0 or above then quit.
            if (m_visual.Initialise() == false)
            {
                Exit();
            }
            else
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "Game1", "CVisual initialised");
            }

            if (m_x360Input == null)
            {
                m_x360Input = new CX360Input();
            }
#if WINDOWS
            if (m_pcInput == null)
            {
                m_pcInput = new CPCInput();
            }
            m_pcInput.Initialise();
            m_curInput = m_pcInput;
#else
            m_curInput = m_x360Input;
#endif
            if (m_inputData == null)
            {
                m_inputData = new CInputData();
            }
            m_inputData.m_wasCntr1Connect = false;

            m_totalSimTime = 0;
            m_garbageCollectTime = 0.5f;

            base.Initialize();

//#if DEBUG
//            // Positioning the window to the top left of the screen.
//            Form form = (Form)Form.FromHandle(Window.Handle);
//            form.Location = new System.Drawing.Point(0, 0);
//            //form.FormBorderStyle = FormBorderStyle.None;
//#endif

            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "Game1", "Game1 initialised");
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "Game1", "Entered Game1 content loader.");

            if (m_visual.Load(Content) == false)
            {
                Exit();
            }

            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "Game1", "Passed m_visual.Load(Content);");

            m_screens = new List<CScreen>();
            m_updateScreens = new List<CScreen>();

            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "Onderzoek.Game1", "Game1 content loaded.");
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (IsActive == true)
            {
                m_curInput.UpdateInput(m_inputData);
                m_x360Input.CheckControllerStatus(m_inputData);
            }
            if (m_inputData.m_wasCntr1Connect == true && m_inputData.m_cntr1Connect == false)
            {
                m_inputData.m_wasCntr1Connect = false;
#if WINDOWS
                m_curInput = m_pcInput;
#endif
            }
            //if (m_inputData.m_wasCntr1Connect == false && m_inputData.m_cntr1Connect == true)
            //{
            //    m_inputData.m_wasCntr1Connect = true;
            //    m_curInput = m_x360Input;
            //}

            //foreach (CScreen screen in m_screens)
            for (int i = 0; i < m_updateScreens.Count; ++i)
            {
                m_updateScreens[i].Update(gameTime);
            }

            m_totalSimTime += (float)gameTime.ElapsedGameTime.Milliseconds / 1000.0f;
            if (m_totalSimTime > m_garbageCollectTime)
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();

                m_garbageCollectTime += 0.5f;
            }

            m_cueManager.Update();

            base.Update(gameTime);
        }

        protected override void EndRun()
        {
            // Close all threads
            switch (m_loggerThread.ThreadState)
            {
                case ThreadState.Running:
                case ThreadState.WaitSleepJoin:
                case ThreadState.Unstarted:
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_finish, "CGameScreenHelper:DeInitialise", "Shutting down the logger");
                    break;
            }

            base.EndRun();
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            for( int i = 0; i < m_screens.Count; ++i )
            {
                m_screens[i].Draw(gameTime);
            }

            base.Draw(gameTime);
        }

#if WINDOWS
        public void ChangeToPCInputType()
        {
            m_curInput = m_pcInput;
        }
#endif

        public bool ChangeToX360InputType()
        {
            m_x360Input.CheckControllerStatus(m_inputData);
            if (m_inputData.m_cntr1Connect == true)
            {
                m_curInput = m_x360Input;
                return true;
            }

            return false;
        }

        public CScreen GetCurrentUpdateScreen()
        {
            return m_updateScreens[0];
        }

        public void AddErrorMsgFloatingScreen(CScreen CurrentScreen, string Error)
        {
            RemoveFromUpdateScreen(CurrentScreen);

            // The new screen needs to be added last so that all the deallocations of the textures of the old screen are done.
            AddScreen(new CErrorScreen(Services, Error));
        }

        public void RemoveMsgFloatingScreen(CScreen CurrentScreen)
        {
            RemoveScreen(CurrentScreen);

            // The new screen needs to be added last so that all the deallocations of the textures of the old screen are done.
            AddAllFromScreenToUpdateScreen();
        }

        public void AddScreen(CScreen Screen)
        {
            Screen.ScreenManager = this;

            m_screens.Add(Screen);
            m_updateScreens.Add(Screen);
        }

        public void RemoveScreen(CScreen Screen)
        {
            m_screens.Remove(Screen);
            m_updateScreens.Remove(Screen);

            Screen.DeInitialise();

            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        public void AddToUpdateScreen(CScreen Screen)
        {
            Screen.ReInitialise();
            m_updateScreens.Add(Screen);
        }

        public void RemoveFromUpdateScreen(CScreen Screen)
        {
            m_updateScreens.Remove(Screen);
        }

        public void AddAllFromScreenToUpdateScreen()
        {
            m_updateScreens.Clear();
            foreach (CScreen screen in m_screens)
            {
                screen.ReInitialise();
                m_updateScreens.Add(screen);
            }
        }

        public void RemoveAllScreens()
        {
            foreach (CScreen screen in m_screens)
            {
                screen.DeInitialise();
            }

            m_screens.Clear();

            foreach (CScreen screen in m_updateScreens)
            {
                screen.DeInitialise();
            }

            m_updateScreens.Clear();

            GC.Collect();
            GC.WaitForPendingFinalizers();
        }

        public void RemoveAllScreensButThis( CScreen Screen )
        {
            for (int i = 0; i < m_screens.Count; ++i )
            {
                if (Screen != m_screens[i])
                {
                    m_screens[i].DeInitialise();

                    m_screens.RemoveAt(i);
                    --i;
                }
            }

            //m_screens.Clear();

            for (int i = 0; i < m_updateScreens.Count; ++i)
            {
                if (Screen != m_updateScreens[i])
                {
                    m_updateScreens[i].DeInitialise();

                    m_updateScreens.RemoveAt(i);
                    --i;
                }
            }

            //m_updateScreens.Clear();
        }



    }


       

}

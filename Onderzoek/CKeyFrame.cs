﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Onderzoek
{
    public class CKeyFrame
    {
        public struct SKeyFrame
        {
            public SKeyFrame(float StartTime, Vector3 Position, Vector3 Rotation)
            {
                s_startTime = StartTime;
                s_position = Position;
                s_rotation = Rotation;
            }

            public float s_startTime;
            public Vector3 s_position;
            public Vector3 s_rotation;
        };

        public struct SKFData
        {
            public SKeyFrame[] s_keyFrameAry;
            public int s_numKeyFrames;
            public int s_curKeyNum;
            public bool s_loop;
            public bool s_finished;
            public bool s_active;
            public bool s_smoothTransition;
            public float s_simTime;
            public float s_newTime;
            public float s_oldTime;
            public Vector3 s_oldPos;
            public Vector3 s_newPos;
            public Vector3 s_oldRot;
            public Vector3 s_newRot;
        }

        public static void Update(ref SKFData KeyFrameData, float DT, ref Vector3 Position, ref Quaternion Rotation)
        {
            if (KeyFrameData.s_active == true && KeyFrameData.s_finished == false)
            {
                double v = 0;
                if (KeyFrameData.s_newTime - KeyFrameData.s_oldTime != 0)
                {
                    v = (double)((KeyFrameData.s_simTime - KeyFrameData.s_oldTime) / (KeyFrameData.s_newTime - KeyFrameData.s_oldTime));
                }

                float s = 0;
                if (KeyFrameData.s_smoothTransition == true)
                {
                    s = (float)(3.0 * Math.Pow(v, 2.0) - 2.0 * Math.Pow(v, 3.0));
                }
                else
                {
                    s = (float)v;
                }

                Position = KeyFrameData.s_oldPos + ((KeyFrameData.s_newPos - KeyFrameData.s_oldPos) * s);
                Vector3 rot = KeyFrameData.s_oldRot + ((KeyFrameData.s_newRot - KeyFrameData.s_oldRot) * s);
                Rotation = Quaternion.CreateFromYawPitchRoll(rot.X, rot.Y, rot.Z);

                if (KeyFrameData.s_simTime >= KeyFrameData.s_newTime)
                {
                    if (KeyFrameData.s_curKeyNum == (KeyFrameData.s_numKeyFrames - 1))
                    {
                        if (KeyFrameData.s_loop == false)
                        {
                            KeyFrameData.s_finished = true;
                            return;
                        }

                        KeyFrameData.s_simTime = 0;
                        KeyFrameData.s_curKeyNum = 0;
                        KeyFrameData.s_newPos = KeyFrameData.s_keyFrameAry[KeyFrameData.s_curKeyNum].s_position;
                        KeyFrameData.s_oldPos = KeyFrameData.s_keyFrameAry[KeyFrameData.s_curKeyNum].s_position;
                        KeyFrameData.s_newRot = KeyFrameData.s_keyFrameAry[KeyFrameData.s_curKeyNum].s_rotation;
                        KeyFrameData.s_oldRot = KeyFrameData.s_keyFrameAry[KeyFrameData.s_curKeyNum].s_rotation;
                        Position = KeyFrameData.s_oldPos;
                        Rotation = Quaternion.CreateFromYawPitchRoll(KeyFrameData.s_oldRot.X, KeyFrameData.s_oldRot.Y, KeyFrameData.s_oldRot.Z);
                    }

                    KeyFrameData.s_oldPos = KeyFrameData.s_keyFrameAry[KeyFrameData.s_curKeyNum].s_position;
                    KeyFrameData.s_newPos = KeyFrameData.s_keyFrameAry[KeyFrameData.s_curKeyNum + 1].s_position;
                    KeyFrameData.s_oldRot = KeyFrameData.s_keyFrameAry[KeyFrameData.s_curKeyNum].s_rotation;
                    KeyFrameData.s_newRot = KeyFrameData.s_keyFrameAry[KeyFrameData.s_curKeyNum + 1].s_rotation;
                    KeyFrameData.s_oldTime = KeyFrameData.s_keyFrameAry[KeyFrameData.s_curKeyNum].s_startTime;
                    KeyFrameData.s_newTime = KeyFrameData.s_keyFrameAry[KeyFrameData.s_curKeyNum + 1].s_startTime;

                    ++KeyFrameData.s_curKeyNum;
                }

                KeyFrameData.s_simTime += DT;
            }
        }

        public static void Reset(ref SKFData KeyFrameData, ref Vector3 Position, ref Quaternion Rotation)
        {
            KeyFrameData.s_simTime = 0;
            KeyFrameData.s_curKeyNum = 0;
            KeyFrameData.s_finished = false;
            KeyFrameData.s_active = true;

            KeyFrameData.s_newPos = KeyFrameData.s_keyFrameAry[KeyFrameData.s_curKeyNum].s_position;
            KeyFrameData.s_oldPos = KeyFrameData.s_keyFrameAry[KeyFrameData.s_curKeyNum].s_position;
            KeyFrameData.s_newRot = KeyFrameData.s_keyFrameAry[KeyFrameData.s_curKeyNum].s_rotation;
            KeyFrameData.s_oldRot = KeyFrameData.s_keyFrameAry[KeyFrameData.s_curKeyNum].s_rotation;
            KeyFrameData.s_oldTime = 0;
            KeyFrameData.s_newTime = 0;

            Position = KeyFrameData.s_oldPos;
            Rotation = Quaternion.CreateFromYawPitchRoll(KeyFrameData.s_oldRot.X, KeyFrameData.s_oldRot.Y, KeyFrameData.s_oldRot.Z);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Onderzoek.Entity;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace Onderzoek
{
    public class CEntityDataStore
    {
        public int m_entityID;
        public Vector3 m_position, m_rotation;
        public string m_entityName;

        public List<CEntityProperty> m_properties;

        public CEntityDataStore(int id, Vector3 pos, Vector3 rot, string entityName, List<CEntityProperty> propList)
        {
            m_entityID = id;
            m_position = pos;
            m_rotation = rot;
            m_entityName = entityName;
            m_properties = propList;
        }

    }



    public class CLevelDataStore
    {
        public CLevelDataStore()
        {
            m_entityStoreList = new List<CEntityDataStore>();
        }
        
        public List<CEntityDataStore> m_entityStoreList;

    }
}

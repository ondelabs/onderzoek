﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Xml;

namespace XMLManip
{
    public class XmlNodeStructure
    {
        public string xmlNodeName;//This value shoudl always contain something
        public string xmlNodeValue; //If this value contains nothing "" it means this node has child nodes
        public Dictionary<string, List<XmlNodeStructure>> xmlDictionary; // if thsi value contains nothing it means this node has no child nodes

        public void addNewElement(string elementName, string valueName)//use this to add an element node
        {
            if (xmlDictionary == null)
                xmlDictionary = new Dictionary<string, List<XmlNodeStructure>>();

            if (!xmlDictionary.ContainsKey(elementName))
            {
                xmlDictionary.Add(elementName, new List<XmlNodeStructure>());
                xmlDictionary[elementName].Add(new XmlNodeStructure());

                xmlDictionary[elementName][0].xmlNodeValue = valueName;
                xmlDictionary[elementName][0].xmlNodeName = elementName;
            }
            else
            {
                //xmlDictionary.Add(elementName, new List<XmlNodeStructure>());
                xmlDictionary[elementName].Add(new XmlNodeStructure());
                xmlDictionary[elementName][xmlDictionary[elementName].Count-1].xmlNodeValue = valueName;
                xmlDictionary[elementName][xmlDictionary[elementName].Count - 1].xmlNodeName = elementName;

            }
        }

        // Hank's
        public void AddChildren(string ElementName, XmlNodeStructure ChildNode)
        {
            if (xmlDictionary == null)
                xmlDictionary = new Dictionary<string, List<XmlNodeStructure>>();

            if (!xmlDictionary.ContainsKey(ElementName))
            {
                xmlDictionary.Add(ElementName, new List<XmlNodeStructure>());
                xmlDictionary[ElementName].Add(ChildNode);
            }
            else
            {
                xmlDictionary[ElementName].Add(ChildNode);
            }
        }
    }


    public class CXMLManip
    {   
        public string xmlFileName;
        public XmlNodeStructure xmlNodeStructure;

        public CXMLManip()
        {
            xmlNodeStructure = new XmlNodeStructure();

        }

        public void setXmlFilename(string fileName)
        {
            xmlFileName = fileName;
        }

        //public void CloseFile()
        //{
        //    xmlReader.Close();
        //}

        public void readNext(XmlTextReader xmlReader,ref XmlNodeStructure nodeStruct)
        {
            nodeStruct.xmlNodeName = xmlReader.Name;
            xmlReader.Read();

            while (xmlReader.NodeType != XmlNodeType.EndElement)
            {
                if (xmlReader.HasValue&& xmlReader.NodeType != XmlNodeType.Whitespace)
                {
                    nodeStruct.xmlNodeValue = xmlReader.Value;
                }
                if(xmlReader.NodeType == XmlNodeType.Element)
                {
                    if (nodeStruct.xmlDictionary == null)
                    {
                        nodeStruct.xmlDictionary = new Dictionary<string, List<XmlNodeStructure>>();
                        
                    }

                    XmlNodeStructure tempNodeStruct = new XmlNodeStructure();

                    if(nodeStruct.xmlDictionary.ContainsKey(xmlReader.Name))
                    {
                        readNext(xmlReader, ref tempNodeStruct);
                        nodeStruct.xmlDictionary[xmlReader.Name].Add(tempNodeStruct);
                        //nodeStruct.xmlDictionary[xmlReader.Name][nodeStruct.xmlDictionary[xmlReader.Name].Count - 1]
                    }
                    else
                    {
                        readNext(xmlReader, ref tempNodeStruct);

                        nodeStruct.xmlDictionary.Add(xmlReader.Name, new List<XmlNodeStructure>());
                        nodeStruct.xmlDictionary[xmlReader.Name].Add(tempNodeStruct);
                        //readNext(xmlReader,ref nodeStruct.xmlDictionary[xmlReader.Name][nodeStruct.xmlDictionary[xmlReader.Name].Count-1]);
                    }

                }
                xmlReader.Read();
                
            }
            return;



        }

        public bool loadXmlFile()
        {
            lock (this)
            {
                XmlTextReader xmlReader = null;
                xmlReader = new XmlTextReader(xmlFileName);

                while (!xmlReader.EOF)
                {
                    if (xmlReader.NodeType == XmlNodeType.Element)
                    {

                        xmlNodeStructure.xmlNodeName = xmlReader.Name;
                        XmlNodeStructure tempNodeStruct = new XmlNodeStructure();

                        readNext(xmlReader, ref xmlNodeStructure);
                        //readNext(xmlReader, ref xmlNodeStructure.xmlDictionary[xmlReader.Name][0]);


                        //xmlNodeStructure.xmlDictionary = new Dictionary<string, List<XmlNodeStructure>>();
                        //xmlNodeStructure.xmlDictionary.Add(xmlReader.Name, new List<XmlNodeStructure>());
                        //xmlNodeStructure.xmlDictionary[xmlReader.Name].Add(tempNodeStruct);


                    }
                    try
                    {
                        xmlReader.Read();
                    }
                    catch (XmlException e)
                    {
                        System.Diagnostics.Trace.WriteLine("\n--------------------------------------------------------------\n");
                        System.Diagnostics.Trace.WriteLine("An XML Exception occurred: " + e.Message + "\n");
                        System.Diagnostics.Trace.WriteLine("--------------------------------------------------------------\n");

                        xmlReader.Close();
                        xmlReader = null;

                        return false;
                    }
                    catch (Exception e)
                    {
                        System.Diagnostics.Trace.WriteLine("\n--------------------------------------------------------------\n");
                        System.Diagnostics.Trace.WriteLine("A General Exception occurred: " + e.Message + "\n");
                        System.Diagnostics.Trace.WriteLine("--------------------------------------------------------------\n");

                        xmlReader.Close();
                        xmlReader = null;

                        return false;
                    }


                }
                //XmlNodeStructure tempStruct = xmlNodeStructure.xmlDictionary["note"][0];

                xmlReader.Close();
                xmlReader = null;

                return true;
            }
        }


        //savefilename can be set to "" to save in the original file
        public void saveXmlFile(string saveFileName)
        {
            lock (this)
            {
                XmlTextWriter writer = null;
                try
                {
                    writer = new XmlTextWriter(saveFileName, null);
                }
                catch (Exception e)
                {
                    Console.WriteLine("The process failed: {0}", e.ToString());
                    return;
                }

                writer.Formatting = Formatting.Indented;

                writer.WriteStartDocument();

                writeNext(writer, xmlNodeStructure);
                //writer.WriteStartElement(xmlNodeStructure.xmlNodeName);

                //for (int i = 0; i < xmlNodeStructure.xmlDictionary.Count; i++)
                //{
                //    //xmlNodeStructure.xmlDictionary[xmlNodeStructure.xmlDictionary.Keys[i]]

                writer.Close();

                //}
            }
        }

        public void writeNext(XmlTextWriter xmlWriter, XmlNodeStructure nodeStructure)
        {
            lock (this)
            {
                xmlWriter.WriteStartElement(nodeStructure.xmlNodeName);
                //for (int i = 0; i < xmlNodeStructure.xmlDictionary.Count; i++)
                if (nodeStructure.xmlDictionary == null)
                {

                    xmlWriter.WriteValue(nodeStructure.xmlNodeValue);
                    //xmlWriter.WriteEndElement();
                }
                else
                    foreach (string outerString in nodeStructure.xmlDictionary.Keys)
                    {
                        if (nodeStructure.xmlNodeValue == "" || nodeStructure.xmlNodeValue == null)
                        {
                            //foreach (string str in dict.Keys) 

                            //for (int j = 0; i < nodeStructure.xmlDictionary[nodeStructure.xmlDictionary.Keys[i]].Count; j++)
                            //foreach(string keyString in nodeStructure.xmlDictionary.Keys)
                            for (int j = 0; j < nodeStructure.xmlDictionary[outerString].Count; j++)
                            {
                                writeNext(xmlWriter, nodeStructure.xmlDictionary[outerString][j]);
                            }
                        }
                        else
                        {
                            xmlWriter.WriteValue(nodeStructure.xmlNodeValue);
                        }
                        //xmlWriter.WriteEndElement();
                    }

                xmlWriter.WriteEndElement();
                //xmlWriter.WriteEndElement();
            }
        }

        public void setXmlData( XmlNodeStructure XMLNodeStruct )
        {
            xmlNodeStructure = XMLNodeStruct;
        }

        public void getList(ref List<string> pathList, ref List<XmlNodeStructure> returnList,ref XmlNodeStructure nodeStruct) // returns a list of all xmlManip objects that are at the end of the specified path.
        {
            //int i;
            //XmlNodeStructure manipPtr = xmlNodeStructure;
            //List<XmlNodeStructure> returnList = new List<XmlNodeStructure>();
            //returnList.

            //for (int i = 0; i < pathList.Count; i++)
            //{
            if (pathList.Count > 0)
            {
                if (!nodeStruct.xmlDictionary.ContainsKey(pathList[0]))
                    return;
                //else
                string str = pathList[0];
                pathList.RemoveAt(0);

                for (int i = 0; i < nodeStruct.xmlDictionary[str].Count; i++)
                {
                    XmlNodeStructure tempStruct = nodeStruct.xmlDictionary[str][i];
                    getList(ref pathList, ref returnList,ref tempStruct );
                }
                
            }
            else
            {
                returnList.Add(nodeStruct);
                //if(nodeStruct.xmlDictionary[pathList[0]]
                //manipPtr = nodeStruct.xmlDictionary[pathList[0]][0];

            }

            //return manipPtr;

            
            

        }
    }

    

}

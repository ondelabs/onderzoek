﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml;

using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using XMLManip;

namespace Onderzoek
{
    public class CDataFromXML
    {
        public struct SGraphicsMenuSettings
        {
            public bool s_ssao;
            public bool s_blurSSAO;
            public bool s_shadows;
            public bool s_blurShadows;
            public int s_shadowRes;
            public bool s_soft;
            public bool s_fullScreen;
            public Vector2 s_screenRes;
            public uint s_textureFilter;
            public uint s_maxMipLevel;
            public bool s_waterReflect;
            public int s_waterResolution;
            public bool s_softWater;
        }

        public static bool GetWaterReflect()
        {
            string file = "Content/xml/settings.xml";
            CXMLManip xml_data = new CXMLManip();

            xml_data.setXmlFilename(file);
            bool frm_file = xml_data.loadXmlFile();

            bool rtn_val = false;

            if (frm_file == true)
            {
                List<XmlNodeStructure> data;
                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("WaterReflect", out data) == true)
                {
                    bool value = false;
                    if (CToolbox.StringToBool(data[0].xmlNodeValue.ToString(), ref value) == true)
                    {
                        rtn_val = value;
                    }
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve Water Reflect from settings.xml");
                }
            }
            else
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not find settings.xml");
            }

            return rtn_val;
        }

        public static int GetWaterResolution()
        {
            string file = "Content/xml/settings.xml";
            CXMLManip xml_data = new CXMLManip();

            xml_data.setXmlFilename(file);
            bool frm_file = xml_data.loadXmlFile();

            int rtn_val = 0;

            if (frm_file == true)
            {
                List<XmlNodeStructure> data;
                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("WaterResolution", out data) == true)
                {
                    int value = 0;
                    if (CToolbox.StringToInt(data[0].xmlNodeValue.ToString(), ref value) == true)
                    {
                        rtn_val = value;
                    }
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve Water Resolution from settings.xml");
                }
            }
            else
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not find settings.xml");
            }

            return rtn_val;
        }

        public static bool GetSoftWater()
        {
            string file = "Content/xml/settings.xml";
            CXMLManip xml_data = new CXMLManip();

            xml_data.setXmlFilename(file);
            bool frm_file = xml_data.loadXmlFile();

            bool rtn_val = false;

            if (frm_file == true)
            {
                List<XmlNodeStructure> data;
                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("SoftWater", out data) == true)
                {
                    bool value = false;
                    if (CToolbox.StringToBool(data[0].xmlNodeValue.ToString(), ref value) == true)
                    {
                        rtn_val = value;
                    }
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve Soft Water from settings.xml");
                }
            }
            else
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not find settings.xml");
            }

            return rtn_val;
        }

        public static SGraphicsMenuSettings GetGraphicsMenuSettings()
        {
            string file = "Content/xml/settings.xml";
            CXMLManip xml_data = new CXMLManip();

            xml_data.setXmlFilename(file);
            bool frm_file = xml_data.loadXmlFile();

            SGraphicsMenuSettings rtn_val = new SGraphicsMenuSettings();

            if (frm_file == true)
            {
                List<XmlNodeStructure> data;
                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("SSAO", out data) == true)
                {
                    bool value = false;
                    if( CToolbox.StringToBool(data[0].xmlNodeValue.ToString(), ref value) == true )
                    {
                        rtn_val.s_ssao = value;
                    }
                    else
                    {
                        CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not parse the ShadowRes string value.");
                    }
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve SSAO from settings.xml");
                }

                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("BlurSSAO", out data) == true)
                {
                    bool value = false;
                    if (CToolbox.StringToBool(data[0].xmlNodeValue.ToString(), ref value) == true)
                    {
                        rtn_val.s_blurSSAO = value;
                    }
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve BlurSSAO from settings.xml");
                }

                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("Shadows", out data) == true)
                {
                    bool value = false;
                    if (CToolbox.StringToBool(data[0].xmlNodeValue.ToString(), ref value) == true)
                    {
                        rtn_val.s_shadows = value;
                    }
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve Shadows from settings.xml");
                }

                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("BlurShadows", out data) == true)
                {
                    bool value = false;
                    if (CToolbox.StringToBool(data[0].xmlNodeValue.ToString(), ref value) == true)
                    {
                        rtn_val.s_blurShadows = value;
                    }
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve BlurShadows from settings.xml");
                }

                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("ShadowRes", out data) == true)
                {
                    int value = 0;
                    if (CToolbox.StringToInt(data[0].xmlNodeValue.ToString(), ref value) == true)
                    {
                        if (value % 128 != 0)
                        {
                            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "ShadowRes value is not a power of two value, this may casue slow down or even a crash.");
                        }
                        rtn_val.s_shadowRes = value;
                    }
                    else
                    {
                        CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not parse the ShadowRes string value.");
                    }
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve ShadowRes from settings.xml");
                }

                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("Soft", out data) == true)
                {
                    bool value = false;
                    if (CToolbox.StringToBool(data[0].xmlNodeValue.ToString(), ref value) == true)
                    {
                        rtn_val.s_soft = value;
                    }
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve Soft from settings.xml");
                }

                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("FullScreen", out data) == true)
                {
                    bool value = false;
                    if (CToolbox.StringToBool(data[0].xmlNodeValue.ToString(), ref value) == true)
                    {
                        rtn_val.s_fullScreen = value;
                    }
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve FullScreen from settings.xml");
                }

                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("WaterReflect", out data) == true)
                {
                    bool value = false;
                    if (CToolbox.StringToBool(data[0].xmlNodeValue.ToString(), ref value) == true)
                    {
                        rtn_val.s_waterReflect = value;
                    }
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve Water Reflect from settings.xml");
                }

                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("WaterResolution", out data) == true)
                {
                    int value = 0;
                    if (CToolbox.StringToInt(data[0].xmlNodeValue.ToString(), ref value) == true)
                    {
                        rtn_val.s_waterResolution = value;
                    }
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve Water Resolution from settings.xml");
                }

                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("SoftWater", out data) == true)
                {
                    bool value = false;
                    if (CToolbox.StringToBool(data[0].xmlNodeValue.ToString(), ref value) == true)
                    {
                        rtn_val.s_softWater = value;
                    }
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve Soft Water from settings.xml");
                }

                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("ScreenRes", out data) == true)
                {
                    Vector2 value = Vector2.Zero;
                    if (CToolbox.StringToVector2(data[0].xmlNodeValue, ref value) == true)
                    {
                        rtn_val.s_screenRes = value;
                    }
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve ScreenRes from settings.xml");
                }

                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("TextureFilter", out data) == true)
                {
                    uint value = 0;
                    if (CToolbox.StringToUInt(data[0].xmlNodeValue, ref value) == true)
                    {
                        rtn_val.s_textureFilter = value;
                    }
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve TextureFilter from settings.xml");
                }

                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("MaxMipLevel", out data) == true)
                {
                    uint value = 0;
                    if (CToolbox.StringToUInt(data[0].xmlNodeValue, ref value) == true)
                    {
                        rtn_val.s_maxMipLevel = value;
                    }
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve MaxMipLevel from settings.xml");
                }
            }
            else
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not find settings.xml");
            }

            return rtn_val;
        }

        public static bool SetGraphicsMenuSettings(SGraphicsMenuSettings GS)
        {
            string file = "Content/xml/settings.xml";
            CXMLManip xml_data = new CXMLManip();

            xml_data.setXmlFilename(file);
            bool frm_file = xml_data.loadXmlFile();

            if (frm_file == true)
            {
                List<XmlNodeStructure> data;
                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("SSAO", out data) == true)
                {
                    data[0].xmlNodeValue = GS.s_ssao.ToString();
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve SSAO from settings.xml");
                }

                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("BlurSSAO", out data) == true)
                {
                    data[0].xmlNodeValue = GS.s_blurSSAO.ToString();
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve BlurSSAO from settings.xml");
                }

                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("Shadows", out data) == true)
                {
                    data[0].xmlNodeValue = GS.s_shadows.ToString();
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve Shadows from settings.xml");
                }

                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("BlurShadows", out data) == true)
                {
                    data[0].xmlNodeValue = GS.s_blurShadows.ToString();
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve BlurShadows from settings.xml");
                }

                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("ShadowRes", out data) == true)
                {
                    data[0].xmlNodeValue = GS.s_shadowRes.ToString();
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve ShadowRes from settings.xml");
                }

                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("Soft", out data) == true)
                {
                    data[0].xmlNodeValue = GS.s_soft.ToString();
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve Soft from settings.xml");
                }

                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("FullScreen", out data) == true)
                {
                    data[0].xmlNodeValue = GS.s_fullScreen.ToString();
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve FullScreen from settings.xml");
                }

                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("WaterReflect", out data) == true)
                {
                    data[0].xmlNodeValue = GS.s_waterReflect.ToString();
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve Water Reflect from settings.xml");
                }

                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("WaterResolution", out data) == true)
                {
                    data[0].xmlNodeValue = GS.s_waterResolution.ToString();
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve Water Resolution from settings.xml");
                }

                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("SoftWater", out data) == true)
                {
                    data[0].xmlNodeValue = GS.s_softWater.ToString();
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve Soft Water from settings.xml");
                }

                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("ScreenRes", out data) == true)
                {
                    data[0].xmlNodeValue = GS.s_screenRes.ToString();
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve ScreenRes from settings.xml");
                }

                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("TextureFilter", out data) == true)
                {
                    data[0].xmlNodeValue = GS.s_textureFilter.ToString();
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve TextureFilter from settings.xml");
                }

                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("MaxMipLevel", out data) == true)
                {
                    data[0].xmlNodeValue = GS.s_maxMipLevel.ToString();
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve MaxMipLevel from settings.xml");
                }
            }
            else
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not find settings.xml");
            }

            xml_data.saveXmlFile(file);

            return true;
        }

        public static float GetMusicVolume()
        {
            string file = "Content/xml/settings.xml";
            CXMLManip xml_data = new CXMLManip();

            xml_data.setXmlFilename(file);
            bool frm_file = xml_data.loadXmlFile();

            if (frm_file == true)
            {
                List<XmlNodeStructure> data;
                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("MusicVolume", out data) == true)
                {
                    float value = 0;
                    if (CToolbox.StringToFloat(data[0].xmlNodeValue, ref value) == true)
                    {
                        return value;
                    }
                    else
                    {
                        CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not parse the MusicVolume string value.");
                    }
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve MusicVolume from settings.xml");
                }
            }
            else
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not find settings.xml");
            }

            return 0;
        }

        public static Vector2 GetScreenRes()
        {
            string file = "Content/xml/settings.xml";
            CXMLManip xml_data = new CXMLManip();

            xml_data.setXmlFilename(file);
            bool frm_file = xml_data.loadXmlFile();

            if (frm_file == true)
            {
                List<XmlNodeStructure> data;
                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("ScreenRes", out data) == true)
                {
                    Vector2 value = Vector2.Zero;
                    if (CToolbox.StringToVector2(data[0].xmlNodeValue, ref value) == true)
                    {
                        return value;
                    }
                    else
                    {
                        CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not parse the ScreenRes string value.");
                    }
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve ScreenRes from settings.xml");
                }
            }
            else
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not find settings.xml");
            }

            return Onderzoek.Visual.CVisual.Instance.GetDefaultResolution();
        }

        public static uint GetTextureFilter()
        {
            string file = "Content/xml/settings.xml";
            CXMLManip xml_data = new CXMLManip();

            xml_data.setXmlFilename(file);
            bool frm_file = xml_data.loadXmlFile();

            if (frm_file == true)
            {
                List<XmlNodeStructure> data;
                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("TextureFilter", out data) == true)
                {
                    uint value = 0;
                    if (CToolbox.StringToUInt(data[0].xmlNodeValue, ref value) == true)
                    {
                        return value;
                    }
                    else
                    {
                        CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not parse the TextureFilter string value.");
                    }
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve TextureFilter from settings.xml");
                }
            }
            else
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not find settings.xml");
            }

            return Onderzoek.Visual.CVisual.Instance.GetDefaultTextureFilter();
        }

        public static bool SetTextureFilter(uint TextureFilter)
        {
            string file = "Content/xml/settings.xml";
            CXMLManip xml_data = new CXMLManip();

            xml_data.setXmlFilename(file);
            bool frm_file = xml_data.loadXmlFile();

            if (frm_file == true)
            {
                List<XmlNodeStructure> data;
                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("TextureFilter", out data) == true)
                {
                    data[0].xmlNodeValue = TextureFilter.ToString();
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve TextureFilter from settings.xml");
                    return false;
                }
            }
            else
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not find settings.xml");
                return false;
            }

            xml_data.saveXmlFile(file);

            return true;
        }

        public static uint GetMaxMipLevel()
        {
            string file = "Content/xml/settings.xml";
            CXMLManip xml_data = new CXMLManip();

            xml_data.setXmlFilename(file);
            bool frm_file = xml_data.loadXmlFile();

            if (frm_file == true)
            {
                List<XmlNodeStructure> data;
                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("MaxMipLevel", out data) == true)
                {
                    uint value = 0;
                    if (CToolbox.StringToUInt(data[0].xmlNodeValue, ref value) == true)
                    {
                        return value;
                    }
                    else
                    {
                        CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not parse the MaxMipLevel string value.");
                    }
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve MaxMipLevel from settings.xml");
                }
            }
            else
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not find settings.xml");
            }

            return Onderzoek.Visual.CVisual.Instance.GetDefaultMaxMipLevel();
        }

        public static bool SetMaxMipLevel(uint MaxMipLevel)
        {
            string file = "Content/xml/settings.xml";
            CXMLManip xml_data = new CXMLManip();

            xml_data.setXmlFilename(file);
            bool frm_file = xml_data.loadXmlFile();

            if (frm_file == true)
            {
                List<XmlNodeStructure> data;
                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("MaxMipLevel", out data) == true)
                {
                    data[0].xmlNodeValue = MaxMipLevel.ToString();
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve MaxMipLevel from settings.xml");
                    return false;
                }
            }
            else
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not find settings.xml");
                return false;
            }

            xml_data.saveXmlFile(file);

            return true;
        }

        public static bool SetMusicVolume(float MusicVolume)
        {
            string file = "Content/xml/settings.xml";
            CXMLManip xml_data = new CXMLManip();

            xml_data.setXmlFilename(file);
            bool frm_file = xml_data.loadXmlFile();

            if (frm_file == true)
            {
                List<XmlNodeStructure> data;
                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("MusicVolume", out data) == true)
                {
                    data[0].xmlNodeValue = MusicVolume.ToString();
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve MusicVolume from settings.xml");
                    return false;
                }
            }
            else
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not find settings.xml");
                return false;
            }

            xml_data.saveXmlFile(file);

            return true;
        }

        public static float GetSFXVolume()
        {
            string file = "Content/xml/settings.xml";
            CXMLManip xml_data = new CXMLManip();

            xml_data.setXmlFilename(file);
            bool frm_file = xml_data.loadXmlFile();

            if (frm_file == true)
            {
                List<XmlNodeStructure> data;
                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("SFXVolume", out data) == true)
                {
                    float value = 0;
                    if (CToolbox.StringToFloat(data[0].xmlNodeValue, ref value) == true)
                    {
                        return value;
                    }
                    else
                    {
                        CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not parse the SFXVolume string value.");
                    }
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve SFXVolume from settings.xml");
                }
            }
            else
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not find settings.xml");
            }

            return 0;
        }

        public static bool SetSFXVolume(float SFXVolume)
        {
            string file = "Content/xml/settings.xml";
            CXMLManip xml_data = new CXMLManip();

            xml_data.setXmlFilename(file);
            bool frm_file = xml_data.loadXmlFile();

            if (frm_file == true)
            {
                List<XmlNodeStructure> data;
                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("SFXVolume", out data) == true)
                {
                    data[0].xmlNodeValue = SFXVolume.ToString();
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not retrieve SFXVolume from settings.xml");
                    return false;
                }
            }
            else
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not find settings.xml");
                return false;
            }

            xml_data.saveXmlFile(file);

            return true;
        }

        public static void ResetSettings()
        {
            string file = "Content/xml/settings.xml";
            CXMLManip xml_data = new CXMLManip();

            xml_data.setXmlFilename(file);

            xml_data.xmlNodeStructure.xmlNodeName = "Settings";
            
            xml_data.xmlNodeStructure.xmlDictionary = new Dictionary<string, List<XmlNodeStructure>>();
            
            List<XmlNodeStructure> list_node = new List<XmlNodeStructure>();

            XmlNodeStructure temp = new XmlNodeStructure();
            temp.xmlNodeName = "SSAO";
            temp.xmlNodeValue = "False";
            list_node.Add(temp);

            temp = new XmlNodeStructure();
            temp.xmlNodeName = "BlurSSAO";
            temp.xmlNodeValue = "False";
            list_node.Add(temp);

            temp = new XmlNodeStructure();
            temp.xmlNodeName = "Shadows";
            temp.xmlNodeValue = "True";
            list_node.Add(temp);

            temp = new XmlNodeStructure();
            temp.xmlNodeName = "BlurShadows";
            temp.xmlNodeValue = "False";
            list_node.Add(temp);

            temp = new XmlNodeStructure();
            temp.xmlNodeName = "ShadowRes";
            temp.xmlNodeValue = "512";
            list_node.Add(temp);

            temp = new XmlNodeStructure();
            temp.xmlNodeName = "Soft";
            temp.xmlNodeValue = "False";
            list_node.Add(temp);

            temp = new XmlNodeStructure();
            temp.xmlNodeName = "MusicVolume";
            temp.xmlNodeValue = "0.5";
            list_node.Add(temp);

            temp = new XmlNodeStructure();
            temp.xmlNodeName = "SFXVolume";
            temp.xmlNodeValue = "0.5";
            list_node.Add(temp);

            temp = new XmlNodeStructure();
            temp.xmlNodeName = "FullScreen";
            temp.xmlNodeValue = "False";
            list_node.Add(temp);

            temp = new XmlNodeStructure();
            temp.xmlNodeName = "WaterReflect";
            temp.xmlNodeValue = "True";
            list_node.Add(temp);

            temp = new XmlNodeStructure();
            temp.xmlNodeName = "WaterResolution";
            temp.xmlNodeValue = "1";
            list_node.Add(temp);

            temp = new XmlNodeStructure();
            temp.xmlNodeName = "SoftWater";
            temp.xmlNodeValue = "True";
            list_node.Add(temp);

            temp = new XmlNodeStructure();
            temp.xmlNodeName = "ScreenRes";
            temp.xmlNodeValue = Onderzoek.Visual.CVisual.Instance.GetDefaultResolution().ToString();
            list_node.Add(temp);

            temp = new XmlNodeStructure();
            temp.xmlNodeName = "TextureFilter";
            temp.xmlNodeValue = Onderzoek.Visual.CVisual.Instance.GetDefaultTextureFilter().ToString();
            list_node.Add(temp);

            temp = new XmlNodeStructure();
            temp.xmlNodeName = "MaxMipLevel";
            temp.xmlNodeValue = Onderzoek.Visual.CVisual.Instance.GetDefaultMaxMipLevel().ToString();
            list_node.Add(temp);

            xml_data.xmlNodeStructure.xmlDictionary.Add("Settings", list_node );

            xml_data.saveXmlFile(file);
        }

        public static bool GetCuesDataFromXML(string FileName, ref Dictionary<string, List<string>> SBToCue, ref Dictionary<string, List<string>> SBToWB)
        {
            CXMLManip xml_data = new CXMLManip();

            xml_data.setXmlFilename(FileName);
            bool frm_file = xml_data.loadXmlFile();

            if (frm_file == true)
            {
                List<XmlNodeStructure> data;
                if (xml_data.xmlNodeStructure.xmlDictionary.TryGetValue("SoundBank", out data) == true)
                {
                    for (int i = 0; i < data.Count; ++i)
                    {
                        List<XmlNodeStructure> data_sb;
                        string sb_name = "";
                        if (data[i].xmlDictionary.TryGetValue("Name", out data_sb) == true)
                        {
                            sb_name = data_sb[0].xmlNodeValue;
                            SBToCue.Add(sb_name, new List<string>());
                            SBToWB.Add(sb_name, new List<string>());
                        }
                        else
                        {
                            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not find the sound banks name in " + FileName);
                            return false;
                        }
                        if (data[i].xmlDictionary.TryGetValue("Cue", out data_sb) == true)
                        {
                            for( int j = 0; j < data_sb.Count; ++j )
                            {
                                SBToCue[sb_name].Add(data_sb[j].xmlNodeValue);
                            }
                        }
                        else
                        {
                            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not find the cues in " + FileName);
                            return false;
                        }
                        if (data[i].xmlDictionary.TryGetValue("WaveBank", out data_sb) == true)
                        {
                            for (int j = 0; j < data_sb.Count; ++j)
                            {
                                SBToWB[sb_name].Add(data_sb[j].xmlNodeValue);
                            }
                        }
                        else
                        {
                            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not find the WaveBank in " + FileName);
                            return false;
                        }
                    }
                }
                else
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not find any SoundBank nodes in " + FileName);
                    return false;
                }
            }
            else
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CSettingsXML", "Could not find " + FileName);
                return false;
            }

            return true;
        }
    }
}

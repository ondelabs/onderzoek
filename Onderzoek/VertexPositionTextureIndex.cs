using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Onderzoek
{
    public struct VertexPositionTextureIndex
    {
        private Vector3 m_position;
        private Vector3 m_texture;

        public Vector3 GetPosition()
        {
            return m_position;
        }

        public VertexPositionTextureIndex(Vector3 Position, Vector3 Texture)
        {
            this.m_position = Position;
            this.m_texture = Texture;
        }

        public static VertexElement[] VertexElements =
        {
            new VertexElement(0, 0, VertexElementFormat.Vector3, VertexElementMethod.Default, VertexElementUsage.Position, 0),
            new VertexElement(0, sizeof(float)*3, VertexElementFormat.Vector3, VertexElementMethod.Default, VertexElementUsage.TextureCoordinate, 0)
        };
        public static int SizeInBytes = ( sizeof(float) * 6 );
    }
}

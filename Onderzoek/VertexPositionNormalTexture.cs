using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Onderzoek
{
    public struct VertexPositionNormalTexture
    {
        private Vector3 m_position;
        private Vector3 m_normal;
        private Vector2 m_texture;

        public Vector3 GetPosition()
        {
            return m_position;
        }

        public Vector3 GetNormal()
        {
            return m_normal;
        }

        public void SetNormal(Vector3 Normal)
        {
            m_normal = Normal;
        }

        public void SetUV(Vector2 UV)
        {
            m_texture = UV;
        }

        public VertexPositionNormalTexture(Vector3 Position, Vector3 Normal, Vector2 Texture)
        {
            this.m_position = Position;
            this.m_normal = Normal;
            this.m_texture = Texture;
        }

        public static VertexElement[] VertexElements =
        {
            new VertexElement(0, 0, VertexElementFormat.Vector3, VertexElementMethod.Default, VertexElementUsage.Position, 0),
            new VertexElement(0, sizeof(float)*3, VertexElementFormat.Vector3, VertexElementMethod.Default, VertexElementUsage.Normal, 0),
            new VertexElement(0, sizeof(float)*6, VertexElementFormat.Vector2, VertexElementMethod.Default, VertexElementUsage.TextureCoordinate, 0)
        };
        public static int SizeInBytes = ( sizeof(float) * 6 ) + ( sizeof(float) * 2 );
    }
}

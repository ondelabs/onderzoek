﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Onderzoek.Entity;

namespace Onderzoek.Visual
{
    class CGI
    {
        public static void GetAllGIInjectValues( out Vector3 LightDirInGrid, out float WS, out float prim_count, out VertexBuffer VB, GraphicsDevice Graphics,
            CDirLightEntity DirLight)
        {
            prim_count = 32.0f * 32.0f * 32.0f;

            LightDirInGrid = DirLight.GetCamera().GetDir() * (0.25f / 32.0f);

            float num_cells = 32.0f * 32.0f;
	        WS = num_cells / (32.0f * 32.0f);

            VertexPosition[] vertices = new VertexPosition[(int)prim_count];

            Vector3 v = Vector3.Zero;
            int count = 0;
            for (int i = 0; i < 32; ++i)
	        {
                v.Y = (float)i / 32.0f;
                for (int j = 0; j < 1024; ++j)
		        {
                    v.X = (float)j / 1024.0f;
                    vertices[count] = new VertexPosition( v );

                    ++count;
		        }
	        }

            VB = new VertexBuffer(Graphics, vertices.Length * VertexPositionTexture.SizeInBytes, BufferUsage.WriteOnly);
            VB.SetData(vertices);
        }
    }
}

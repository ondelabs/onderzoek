﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Onderzoek.Camera;

namespace Onderzoek.Visual
{
    public interface CParticleRenderer
    {
        void BeginRender(Model Model);

        void Render(Matrix[] WorldMatrices, Vector4[] TintColour, int MaxParticlesToRender, Effect ShaderEffect);

        void Render(Matrix[] WorldMatrices, Vector4[] TintColour, int[] FrameNumbers, int MaxParticlesToRender, Effect ShaderEffect);

        void SetUpParticleEffect(Effect ShaderEffect);

        void UnDoSetUpParticleEffect(Effect ShaderEffect);
    }
}
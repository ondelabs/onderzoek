﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Onderzoek.Camera;

namespace Onderzoek.Visual
{
    class CPRNormal : CParticleRenderer
    {
        ModelMeshPart m_meshPart;

        void CParticleRenderer.BeginRender(Model Model)
        {
            ModelMesh mesh = Model.Meshes[0];
            m_meshPart = mesh.MeshParts[0];

            CVisual.Instance.GetGraphicsDevice().VertexDeclaration = m_meshPart.VertexDeclaration;
            CVisual.Instance.GetGraphicsDevice().Vertices[0].SetSource(mesh.VertexBuffer, m_meshPart.StreamOffset, m_meshPart.VertexStride);
            CVisual.Instance.GetGraphicsDevice().Indices = mesh.IndexBuffer;
        }

        void CParticleRenderer.SetUpParticleEffect(Effect ShaderEffect)
        {
            ShaderEffect.Begin();
            ShaderEffect.Techniques[0].Passes[0].Begin();
        }

        void CParticleRenderer.UnDoSetUpParticleEffect(Effect ShaderEffect)
        {
            ShaderEffect.Techniques[0].Passes[0].End();
            ShaderEffect.End();
        }

        void CParticleRenderer.Render(Matrix[] WorldMatrices, Vector4[] TintColour, int[] FrameNumbers, int MaxParticlesToRender, Effect ShaderEffect)
        {
            if (WorldMatrices.Length < MaxParticlesToRender)
            {
                return;
            }

            for (int i = 0; i < MaxParticlesToRender; ++i)
            {
                ShaderEffect.Parameters["World"].SetValue(WorldMatrices[i]);
                ShaderEffect.Parameters["g_tint"].SetValue(TintColour[i]);
                ShaderEffect.Parameters["g_frameNumber"].SetValue(FrameNumbers[i]);
                ShaderEffect.CommitChanges();

                CVisual.Instance.GetGraphicsDevice().DrawIndexedPrimitives(PrimitiveType.TriangleList, m_meshPart.BaseVertex, 0, m_meshPart.NumVertices, m_meshPart.StartIndex, m_meshPart.PrimitiveCount);
            }
        }

        void CParticleRenderer.Render(Matrix[] WorldMatrices, Vector4[] TintColour, int MaxParticlesToRender, Effect ShaderEffect)
        {
            if (WorldMatrices.Length < MaxParticlesToRender)
            {
                return;
            }

            for (int i = 0; i < MaxParticlesToRender; ++i)
            {
                ShaderEffect.Parameters["World"].SetValue(WorldMatrices[i]);
                ShaderEffect.Parameters["g_tint"].SetValue(TintColour[i]);
                ShaderEffect.CommitChanges();

                CVisual.Instance.GetGraphicsDevice().DrawIndexedPrimitives(PrimitiveType.TriangleList, m_meshPart.BaseVertex, 0, m_meshPart.NumVertices, m_meshPart.StartIndex, m_meshPart.PrimitiveCount);
            }
        }
    }
}

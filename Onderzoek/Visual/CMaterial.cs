﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Onderzoek.Visual
{
    public class CMaterial
    {
        public CMaterial()
        {
            m_effect = -1;
            m_textureID = -1;
            m_normalID = -1;
            m_specularID = -1;
        }

        public int m_effect;
        public int m_textureID;
        public int m_normalID;
        public int m_specularID;

        public Vector4 m_diffuse;
        public Vector4 m_ambient;
        public Vector4 m_emissive;
        public Vector4 m_specularColour;
        public float m_specularPower;
    }
}

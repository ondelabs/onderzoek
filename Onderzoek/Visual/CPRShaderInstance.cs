﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Onderzoek.Camera;

namespace Onderzoek.Visual
{
    class CPRShaderInstance : CParticleRenderer
    {
        ModelMeshPart m_meshPart;

        int m_vertexStride;
        int m_vertexCount;
        VertexBuffer m_vertexBuffer;
        int m_maxInstances;
        int m_indexCount;
        IndexBuffer m_indexBuffer;
        VertexDeclaration m_particleVD;
        Matrix[] m_tempMatrices;
        Vector4[] m_tempTint;
        int[] m_tempFrameNumbers;

        public CPRShaderInstance(Model ParticleModel)
        {
            m_maxInstances = 13;
            m_tempMatrices = new Matrix[m_maxInstances];
            m_tempTint = new Vector4[m_maxInstances];
            m_tempFrameNumbers = new int[m_maxInstances];

            InitializeShaderInstancing(ParticleModel);
        }

        void InitializeShaderInstancing(Model ParticleModel)
        {
            m_vertexStride = ParticleModel.Meshes[0].MeshParts[0].VertexStride;
            m_vertexCount = ParticleModel.Meshes[0].MeshParts[0].NumVertices;
            m_vertexBuffer = ParticleModel.Meshes[0].VertexBuffer;
            m_indexCount = ParticleModel.Meshes[0].MeshParts[0].PrimitiveCount * 3;
            m_indexBuffer = ParticleModel.Meshes[0].IndexBuffer;

            ReplicateVertexData();
            ReplicateIndexData();

            // When using shader instancing, the instance index is specified for
            // each replicated vertex using a float in texture coordinate channel 1.
            // We must modify our vertex declaration to include that channel.
            int instanceIndexOffset = m_vertexStride - sizeof(float);
            byte usageIndex = 1;
            short stream = 0;

            VertexElement[] extraElements =
            {
                new VertexElement(stream, (short)instanceIndexOffset,
                                  VertexElementFormat.Single,
                                  VertexElementMethod.Default,
                                  VertexElementUsage.TextureCoordinate, usageIndex)
            };

            ExtendVertexDeclaration(extraElements, ParticleModel);
        }

        void ReplicateVertexData()
        {
            // Read the existing vertex data, then destroy the existing vertex buffer.
            byte[] oldVertexData = new byte[m_vertexCount * m_vertexStride];

            m_vertexBuffer.GetData(oldVertexData);
            //m_vertexBuffer.Dispose();

            // Adjust the vertex stride to include our additional index channel.
            int oldVertexStride = m_vertexStride;

            m_vertexStride += sizeof(float);

            // Allocate a temporary array to hold the replicated vertex data.
            byte[] newVertexData = new byte[m_vertexCount * m_vertexStride * m_maxInstances];

            int outputPosition = 0;

            // Replicate one copy of the original vertex buffer for each instance.
            for (int instanceIndex = 0; instanceIndex < m_maxInstances; instanceIndex++)
            {
                int sourcePosition = 0;

                // Convert the instance index from float into an array of raw bits.
                byte[] instanceIndexBits = BitConverter.GetBytes((float)instanceIndex);

                for (int i = 0; i < m_vertexCount; i++)
                {
                    // Copy over the existing data for this vertex.
                    Array.Copy(oldVertexData, sourcePosition,
                               newVertexData, outputPosition, oldVertexStride);

                    outputPosition += oldVertexStride;
                    sourcePosition += oldVertexStride;

                    // Set the value of our new index channel.
                    instanceIndexBits.CopyTo(newVertexData, outputPosition);

                    outputPosition += instanceIndexBits.Length;
                }
            }

            // Create a new vertex buffer, and set the replicated data into it.
            m_vertexBuffer = new VertexBuffer(CVisual.Instance.GetGraphicsDevice(), newVertexData.Length,
                                            BufferUsage.None);

            m_vertexBuffer.SetData(newVertexData);
        }

        void ReplicateIndexData()
        {
            // Read the existing index data, then destroy the existing index buffer.
            ushort[] oldIndices = new ushort[m_indexCount];

            m_indexBuffer.GetData(oldIndices);
            //m_indexBuffer.Dispose();

            // Allocate a temporary array to hold the replicated index data.
            ushort[] newIndices = new ushort[m_indexCount * m_maxInstances];

            int outputPosition = 0;

            // Replicate one copy of the original index buffer for each instance.
            for (int instanceIndex = 0; instanceIndex < m_maxInstances; instanceIndex++)
            {
                int instanceOffset = instanceIndex * m_vertexCount;

                for (int i = 0; i < m_indexCount; i++)
                {
                    newIndices[outputPosition] = (ushort)(oldIndices[i] +
                                                          instanceOffset);

                    outputPosition++;
                }
            }

            // Create a new index buffer, and set the replicated data into it.
            m_indexBuffer = new IndexBuffer(CVisual.Instance.GetGraphicsDevice(),
                                          sizeof(ushort) * newIndices.Length,
                                          BufferUsage.None,
                                          IndexElementSize.SixteenBits);

            m_indexBuffer.SetData(newIndices);
        }

        void ExtendVertexDeclaration(VertexElement[] extraElements, Model ParticleModel)
        {
            // Save the original vertex elements.
            VertexElement[] original = ParticleModel.Meshes[0].MeshParts[0].VertexDeclaration.GetVertexElements();

            // Append the new elements to the original format.
            int length = original.Length + extraElements.Length;

            VertexElement[] elements = new VertexElement[length];

            original.CopyTo(elements, 0);

            extraElements.CopyTo(elements, original.Length);

            // Create a new vertex declaration.
            m_particleVD = new VertexDeclaration(CVisual.Instance.GetGraphicsDevice(), elements);
        }

        void CParticleRenderer.BeginRender(Model Model)
        {
            ModelMesh mesh = Model.Meshes[0];
            m_meshPart = mesh.MeshParts[0];

            CVisual.Instance.GetGraphicsDevice().Vertices[0].SetSource(m_vertexBuffer, 0, m_vertexStride);
            CVisual.Instance.GetGraphicsDevice().Indices = m_indexBuffer;

            CVisual.Instance.GetGraphicsDevice().VertexDeclaration = m_particleVD;
        }

        void CParticleRenderer.SetUpParticleEffect(Effect ShaderEffect)
        {
            ShaderEffect.Begin();
            ShaderEffect.Techniques[0].Passes[1].Begin();
        }

        void CParticleRenderer.UnDoSetUpParticleEffect(Effect ShaderEffect)
        {
            ShaderEffect.Techniques[0].Passes[1].End();
            ShaderEffect.End();
        }

        void CParticleRenderer.Render(Matrix[] WorldMatrices, Vector4[] TintColour, int[] FrameNumbers, int MaxParticlesToRender, Effect ShaderEffect)
        {
            if (WorldMatrices.Length < MaxParticlesToRender)
            {
                return;
            }

            // We can only fit maxInstances into a single call. If asked to draw
            // more than that, we must split them up into several smaller batches.
            for (int i = 0; i < MaxParticlesToRender; i += m_maxInstances)
            {
                // How many instances can we fit into this batch?
                int instanceCount = WorldMatrices.Length - i;

                if (instanceCount > m_maxInstances)
                {
                    instanceCount = m_maxInstances;
                }

                // Upload transform matrices as shader constants.
                Array.Copy(WorldMatrices, i, m_tempMatrices, 0, instanceCount);
                Array.Copy(TintColour, i, m_tempTint, 0, instanceCount);
                Array.Copy(FrameNumbers, i, m_tempFrameNumbers, 0, instanceCount);

                ShaderEffect.Parameters["InstanceTransforms"].SetValue(m_tempMatrices);
                ShaderEffect.Parameters["InstanceTint"].SetValue(m_tempTint);
                ShaderEffect.Parameters["InstanceFrameNumbers"].SetValue(m_tempFrameNumbers);
                ShaderEffect.CommitChanges();

                // Draw maxInstances copies of our geometry in a single batch.
                CVisual.Instance.GetGraphicsDevice().DrawIndexedPrimitives(PrimitiveType.TriangleList,
                                                     0, 0, instanceCount * m_vertexCount,
                                                     0, instanceCount * m_indexCount / 3);
            }
        }

        void CParticleRenderer.Render(Matrix[] WorldMatrices, Vector4[] TintColour, int MaxParticlesToRender, Effect ShaderEffect)
        {
            if (WorldMatrices.Length < MaxParticlesToRender)
            {
                return;
            }

            // We can only fit maxInstances into a single call. If asked to draw
            // more than that, we must split them up into several smaller batches.
            for (int i = 0; i < MaxParticlesToRender; i += m_maxInstances)
            {
                // How many instances can we fit into this batch?
                int instanceCount = WorldMatrices.Length - i;

                if (instanceCount > m_maxInstances)
                {
                    instanceCount = m_maxInstances;
                }

                // Upload transform matrices as shader constants.
                Array.Copy(WorldMatrices, i, m_tempMatrices, 0, instanceCount);
                Array.Copy(TintColour, i, m_tempTint, 0, instanceCount);

                ShaderEffect.Parameters["InstanceTransforms"].SetValue(m_tempMatrices);
                ShaderEffect.Parameters["InstanceTint"].SetValue(m_tempTint);
                ShaderEffect.CommitChanges();

                // Draw maxInstances copies of our geometry in a single batch.
                CVisual.Instance.GetGraphicsDevice().DrawIndexedPrimitives(PrimitiveType.TriangleList,
                                                     0, 0, instanceCount * m_vertexCount,
                                                     0, instanceCount * m_indexCount / 3);
            }
        }
    }
}

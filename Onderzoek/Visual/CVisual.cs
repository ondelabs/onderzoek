﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Graphics.PackedVector;
using Microsoft.Xna.Framework.Content;

using Onderzoek.ModelData;
using Onderzoek.Entity;
using Onderzoek.Camera;

using XNAnimation;
using XNAnimation.Effects;
using XNAnimation.Controllers;

namespace Onderzoek.Visual
{
    public class CWaterRendering
    {
        public RenderTarget2D m_renderTarget;
        public DepthStencilBuffer m_depthBuffer;
        public CWCamera m_reflectCamera;
        public Plane m_clipPlane;
        //public float m_reflectance = 0;
        public CWaterEntity m_waterEntity;
        public Vector3 m_perpWindDirection;
        public float m_reflecRefracOffset;
        public float m_speed;
        public float m_fresnelOffset;

        public void DisposeOfRenderTarget()
        {
            if (m_renderTarget != null)
            {
                m_renderTarget.Dispose();
                m_renderTarget = null;
            }

            if (m_depthBuffer != null)
            {
                m_depthBuffer.Dispose();
                m_depthBuffer = null;
            }
        }

        public void SetCamera(Onderzoek.Camera.CWCamera ReflectCamera)
        {
            m_reflectCamera.SetPosition(ReflectCamera.GetPosition());
            m_reflectCamera.SetProjection(ReflectCamera.GetProjection());
            m_reflectCamera.SetView(ReflectCamera.GetView());
            m_reflectCamera.SetRotationMatrix(ReflectCamera.GetRotationMatrix());
        }

        public void SetNewHeight(Vector3 Position)
        {
            m_clipPlane = CToolbox.CreatePlane(m_clipPlane.Normal, Position);
        }
    }

    public class CVisual
    {
        public struct SPointLight
        {
            public Vector3 s_position;
            public float s_radius;
            public Vector3 s_colour;
            public float s_intensity;
            public Model s_model;
            public float s_specularIntensity;
        }

        public struct SDirectionLight
        {
            public Vector3 s_direction;
            public Vector3 s_colour;
            public float s_intensity;
            public Model s_model;
            public COrthoCamera[] s_lightCamera;
            public int[] s_shadowRTIndex;
            public float[] s_nearFar;
            public float s_shadowTerm;
            public float s_specularIntensity;
        }

        public struct SSpotLight
        {
            public Vector3 s_position;
            public Vector3 s_interest;
            public Vector3 s_colour;
            public float s_intensity;
            public float s_range;
            public float s_coneAngle;
            public Model s_model;
            public CFPCamera s_lightCamera;
            public int s_shadowRTIndex;
            public float s_shadowTerm;
            public float s_specularIntensity;
        }

        // List of lights
        protected List<SPointLight> m_pointLightList;
        protected List<SDirectionLight> m_dirLightList;
        protected List<SSpotLight> m_spotLightList;
        protected List<SDirectionLight> m_dirLightShadowList;
        protected List<SSpotLight> m_spotLightShadowList;

        protected SDirectionLight m_forwardRenderLight;
        protected bool m_lightInForwardRendering = false;

        // Fps counter variables
        protected double m_curFPS = 0;
        protected int m_frameCount = 0;
        protected double m_nxtTime = 0;

        // Font variables
        protected SpriteFont m_fontArial;
        protected Vector2 m_fontArialPos;

        // Previous data for frustum culling
        protected Vector3 m_prvCamPos;
        protected Quaternion m_prvCamRot;

#if ENGINE
        protected GraphicsDeviceManager m_graphicsManager;
#endif
        protected GraphicsDevice m_graphics;
        protected SpriteBatch m_spriteBatch;

        protected Effect[] m_effectPhong;
        protected Effect[] m_effectForwardPhong;
        protected Effect[] m_effectPhongShadow;
        protected Effect m_effectBound;
        protected Effect[] m_effectSkinned;
        protected Effect[] m_effectForwardSkinned;
        protected Effect m_effectDeferredDirLight;
        protected Effect m_effectDeferredPtLight;
        protected Effect m_effectDeferredSpotLight;

        protected Effect m_effectFinalDeferred;
        protected Effect m_effectFinalDeferredSSAO;
        protected Effect m_effectFinalDeferredShadow;
        protected Effect m_effectFinalDeferredSSAOShadow;
        protected Effect m_effectFinalDeferredBasic;

        protected Effect[] m_effectParticleRender;
        protected Effect m_curParticleEffect;
        protected int m_indexOffsetForSoftParticles;
        protected CParticleRenderer m_particleRenderer;
        protected CParticleRenderer m_normParticleRenderer;
        protected Model m_particleModel;

        protected Effect m_ssao;
        protected Effect m_blur;
        protected Effect m_copy;
        protected Effect m_combineOpaqueAndTranslucent;
        protected Effect m_effectShadow;
        protected VertexDeclaration m_boundVD;

        // Glitter rendering stuff
        protected Effect[] m_effectGlitterForwardPhong;
        Texture3D m_noiseVolume;

        // Distort
        Effect m_effectDistort;
        protected GameTime m_dt;
        int m_maxDistortData;
        Model[] m_explosionModelData;
        Matrix[] m_explosionWorldMatrix;
        float[] m_explosionDistortMultiplier;
        int m_maxDistortEffect = 0;

        // Water Rendering stuff
        Effect[] m_effectWaterRenderArray;
        Effect m_effectWaterRender;
        protected List<CWaterRendering> m_waterRenderData;
        protected List<int> m_waterToRender;
        Texture2D m_waterRipple;
        float m_time = 0;
        Vector3 m_windDirection;
        Model[] m_waterModelData;
        Matrix[] m_waterWorldMatrix;
        List<CMaterial>[] m_waterMaterialList;
        Dictionary<int, List<CEntity.SPartAndMatNum>>[] m_meshIDs;
        int[] m_waterID;
        int m_maxWaterEffect = 0;
        bool m_renderReflect = true;
        int m_reflectionResolution = 4;
        bool m_softWater = false;

        public bool RenderReflect
        {
            set
            {
                m_renderReflect = value;

                int offset = 0;
                if (m_softWater == true && m_supportsSM3 == true)
                {
                    offset = 2;
                }

                if (m_renderReflect == true)
                {
                    m_effectWaterRender = m_effectWaterRenderArray[1 + offset];
                }
                else
                {
                    m_effectWaterRender = m_effectWaterRenderArray[0 + offset];
                }
            }

            get { return m_renderReflect; }
        }

        public bool SoftWater
        {
            get { return m_softWater; }
            set
            {
                if (m_supportsSM3 == true && value == true)
                {
                    m_softWater = true;

                    int offset = 0;
                    if (m_softWater == true)
                    {
                        offset = 2;
                    }

                    if (m_renderReflect == true)
                    {
                        m_effectWaterRender = m_effectWaterRenderArray[1 + offset];
                    }
                    else
                    {
                        m_effectWaterRender = m_effectWaterRenderArray[0 + offset];
                    }
                }
                else
                {
                    m_softWater = false;
                }
            }
        }

        protected int m_scCount = 0;
        protected bool m_SSAOActive = false;
        protected bool m_shadowsActive = false;
        protected bool m_lightsActive = true;
        protected bool m_supportsSM3 = false;
        protected int m_numTotalPolygonsRendered = 0; // FOR DEBUG
        protected int m_numShadowPolygonsRendered = 0; // FOR DEBUG
        protected int m_numNormalPolygonsRendered = 0; // FOR DEBUG
        protected List<string> m_debugVariables;
        protected List<CEntityOBB[]> m_listOfBounds;

        // List of all the textures for the level
        protected List<Texture2D> m_levelTextures;
        protected Dictionary<string, int> m_levelTexDictionary;
        protected Dictionary<int, int> m_TexIDToUsageCount;

        // List of all the textures for the HUD
        protected List<Texture2D> m_hudTextures;
        protected Dictionary<string, int> m_hudTexDictionary;

        // For rendering post processing stuff
        protected VertexDeclaration m_vertexDecl = null;
        protected VertexPositionTextureIndex[] m_vertices = null;
        protected short[] m_indices = null;

        // For optimising the point lights
        protected Model m_pointLightSphere;
        protected Model m_spotLightCone;

        protected Vector2 m_halfPixel;
        protected Vector2 m_halfPixel2;

        // Only for debugging
        protected int m_displayIsSD = 0; // For debug purpose

        // MRTs for deferred shading
        protected RenderTarget2D m_rtDepth;
        protected RenderTarget2D m_rtNormal;
        protected RenderTarget2D m_rtDiffuse;
        protected RenderTarget2D m_rtMaterial;
        protected RenderTarget2D m_rtAllLights;

        // For SSAO and SSGI
        protected RenderTarget2D m_rtSSAO;
        protected RenderTarget2D m_rtSpare;
        protected RenderTarget2D m_rtSpare2;
        protected RenderTarget2D m_rtSpareFS1;
        // Random normal texture
        protected Texture2D m_rnTexture;

        protected ResolveTexture2D m_renderTargetTexture;

        // Render target for shadows.
        protected List<int> m_shadowIndex;
        protected List<RenderTarget2D> m_rtShadow;
        protected DepthStencilBuffer m_dsbShadow;
        protected DepthStencilBuffer m_originalDSB;
#if WINDOWS
        protected DepthStencilBuffer m_backupedNormalDSB;
        //protected DepthStencilBuffer m_otherRenderingDSB;
#endif
        protected RenderTarget2D m_frwdRender;
        protected RenderTarget2D m_frwdDepth;
        protected RenderTarget2D m_rtFinalShadow;
        protected int m_shadowRTDimension;
        protected bool m_blurShadows = true;
        protected bool m_blurSSAO = true;
        protected bool m_scrnResChanged = false;
        protected bool m_fullScreenHasChanged = false;

        protected Vector3[] m_randomVectors;

        // For singleton
        protected static CVisual instance;

        public static CVisual Instance
        {
            get { return instance; }
        }

#if EDITOR
        public CVisual(GraphicsDevice device)
        {
            if (instance != null)
                throw new Exception("Cannot have multiple singletons");

            m_listOfBounds = new List<CEntityOBB[]>();

            m_curTexFilter = ETextureFilter.e_linear;

            instance = this;

            m_graphics = device;

            m_lineList = new List<Vector3>();
            m_lineListColour = new List<Color>();


            m_hudTextures = new List<Texture2D>();
            m_hudTexDictionary = new Dictionary<string, int>();

            m_levelTextures = new List<Texture2D>();
            m_levelTexDictionary = new Dictionary<string, int>();
            m_TexIDToUsageCount = new Dictionary<int, int>();

            m_pointLightList = new List<SPointLight>();
            m_dirLightList = new List<SDirectionLight>();
            m_spotLightList = new List<SSpotLight>();
            m_dirLightShadowList = new List<SDirectionLight>();
            m_spotLightShadowList = new List<SSpotLight>();
        }
#else

#if DEBUG
        void GraphicsPreparingDeviceSettings(object sender, PreparingDeviceSettingsEventArgs e)
        {
            int count = 0;
            foreach (GraphicsAdapter curAdapter in GraphicsAdapter.Adapters)
            {
                if (curAdapter.Description.Contains("PerfHUD"))
                {
                    e.GraphicsDeviceInformation.Adapter = curAdapter;
                    e.GraphicsDeviceInformation.DeviceType = DeviceType.Reference;
                    break;
                }

                ++count;
            }
            return;
        }
#endif

        public CVisual(Game ThisGame)
        {
            if (instance != null)
                throw new Exception("Cannot have multiple singletons");

            instance = this;

            m_graphicsManager = new GraphicsDeviceManager(ThisGame);

#if DEBUG
            m_graphicsManager.PreparingDeviceSettings += GraphicsPreparingDeviceSettings;
            m_graphicsManager.SynchronizeWithVerticalRetrace = false;
#endif

            m_lineList = new List<Vector3>();
            m_lineListColour = new List<Color>();

            m_hudTextures = new List<Texture2D>();
            m_hudTexDictionary = new Dictionary<string, int>();

            m_levelTextures = new List<Texture2D>();
            m_levelTexDictionary = new Dictionary<string, int>();
            m_TexIDToUsageCount = new Dictionary<int, int>();

            m_pointLightList = new List<SPointLight>();
            m_dirLightList = new List<SDirectionLight>();
            m_spotLightList = new List<SSpotLight>();
            m_dirLightShadowList = new List<SDirectionLight>();
            m_spotLightShadowList = new List<SSpotLight>();

            m_debugVariables = new List<string>();

            // Water stuff
            m_waterRenderData = new List<CWaterRendering>();
            m_waterToRender = new List<int>();
            m_windDirection = new Vector3(0.5f, 0, 0.5f);

            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CVisual", "Passed CVisual Constructor");
        }
#endif

        public bool Initialise()
        {
            // Works here.
#if ENGINE
            m_graphics = m_graphicsManager.GraphicsDevice;
#endif

            if (m_graphics.GraphicsDeviceCapabilities.PixelShaderVersion.Major < 2)
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CVisual", "SM1 supported");
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CVisual", "SM1 supported, unfortunatly the game is not supported on your hardware.");
                return false;
            }

            // Create a new SpriteBatch, which can be used to draw textures.
            m_spriteBatch = new SpriteBatch(m_graphics);

            m_boundVD = new VertexDeclaration(m_graphics, VertexPositionColor.VertexElements);

#if ENGINE
#if WINDOWS
            m_displayIsSD = 1;
            Vector2 default_res = CDataFromXML.GetScreenRes();
            m_graphicsManager.PreferredBackBufferWidth = (int)default_res.X;
            m_graphicsManager.PreferredBackBufferHeight = (int)default_res.Y;
            m_graphicsManager.ApplyChanges();
#endif
#endif

            m_shadowRTDimension = 1024;

            m_rtShadow = new List<RenderTarget2D>();
            m_shadowIndex = new List<int>();

            if (CreateMRT() == false)
            {
                return false;
            }

            if (m_graphics.GraphicsDeviceCapabilities.PixelShaderVersion.Major >= 3)
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CVisual", "SM3 supported");
                m_supportsSM3 = true;
            }
            else
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CVisual", "SM2 supported");
            }

            // For post processing
            m_vertexDecl = new VertexDeclaration(CVisual.Instance.GetGraphicsDevice(), VertexPositionTextureIndex.VertexElements);

            Vector2 minus_one = Vector2.One * -1;
            Vector2 one = Vector2.One;

            m_vertices = new VertexPositionTextureIndex[]
                    {
                        new VertexPositionTextureIndex(
                            new Vector3(one.X, minus_one.Y ,0),
                            new Vector3(1,1,2)),
                        new VertexPositionTextureIndex(
                            new Vector3(minus_one.X, minus_one.Y, 0),
                            new Vector3(0,1,3)),
                        new VertexPositionTextureIndex(
                            new Vector3(minus_one.X, one.Y, 0),
                            new Vector3(0,0,0)),
                        new VertexPositionTextureIndex(
                            new Vector3(one.X, one.Y, 0),
                            new Vector3(1,0,1))
                    };

            m_indices = new short[] { 0, 1, 2, 2, 3, 0 };

            Random ran_num = new Random();
            m_randomVectors = new Vector3[32];
            for (int i = 0; i < 32; ++i)
            {
                int x = ran_num.Next(-1, 1);
                if (x == 0)
                {
                    x = 1;
                }
                int y = ran_num.Next(-1, 1);
                if (y == 0)
                {
                    y = 1;
                }
                int z = ran_num.Next(-1, 1);
                if (z == 0)
                {
                    z = 1;
                }

                m_randomVectors[i] = CToolbox.UnitVector(new Vector3((float)ran_num.NextDouble() * x, (float)ran_num.NextDouble() * y, (float)ran_num.NextDouble() * z));
            }

#if ENGINE
            SetGraphicsSettings(CDataFromXML.GetGraphicsMenuSettings());
#else
            SetGraphicsSettings();
#endif

            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CVisual", "CVisual Initialised");

            return true;
        }

        public Vector2 HalfPixel
        {
            get { return m_halfPixel; }
        }

        public Vector2 HalfPixel2
        {
            get { return m_halfPixel2; }
        }

        public int CreateRenderTargetForShadows(bool IsFirst)
        {
            PresentationParameters pp = m_graphics.PresentationParameters;

            if (IsFirst == true)
            {
                m_rtShadow.Add(new RenderTarget2D(m_graphics,
                    m_shadowRTDimension, m_shadowRTDimension, 1, SurfaceFormat.Single, pp.MultiSampleType, pp.MultiSampleQuality));
            }
            else
            {
                m_rtShadow.Add(new RenderTarget2D(m_graphics,
                    m_shadowRTDimension / 2, m_shadowRTDimension / 2, 1, SurfaceFormat.Single, pp.MultiSampleType, pp.MultiSampleQuality));
            }

            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CVisual", "Render target for shadow created.");

            return m_rtShadow.Count - 1;
        }

        public void DeleteShadowRenderTarget(int RTIndex)
        {
            if (m_rtShadow[RTIndex] != null)
            {
                m_rtShadow[RTIndex].Dispose();
                m_rtShadow[RTIndex] = null;
            }

            int nulled = 0;
            for (int i = 0; i < m_rtShadow.Count; ++i)
            {
                if (m_rtShadow[i] == null)
                {
                    ++nulled;
                }
            }

            if (nulled == m_rtShadow.Count)
            {
                m_rtShadow = new List<RenderTarget2D>();
            }
        }

        protected bool CreateMRT()
        {
            // Create custom rendertargets.
            PresentationParameters pp = m_graphics.PresentationParameters;

            m_rtDepth = new RenderTarget2D(m_graphics,
                pp.BackBufferWidth, pp.BackBufferHeight, 1,
                SurfaceFormat.Single, pp.MultiSampleType, pp.MultiSampleQuality);

            SurfaceFormat normal_surf = SurfaceFormat.Color;

            if (GraphicsAdapter.DefaultAdapter.CheckDeviceFormat(DeviceType.Hardware,
                GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Format, TextureUsage.None,
                QueryUsages.None, ResourceType.RenderTarget, SurfaceFormat.Bgra1010102) == true)
            {
                if (GraphicsAdapter.DefaultAdapter.VendorId == 4318)
                {
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CVisual", "SurfaceFormat.Bgra1010102 supported");
                    normal_surf = SurfaceFormat.Bgra1010102;
                }
                else
                {
                    // ATI vendor id = 4098 // I dislike ATI and my X1950!
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CVisual", "Nvidia card not detected, there maybe unforeseen issues.");
                    CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CVisual", "SurfaceFormat.Bgra1010102 supported but defaulting to SurfaceFormat.Color");
                }
            }
            else
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CVisual", "SurfaceFormat.Bgra1010102 not supported");
            }

            m_rtNormal = new RenderTarget2D(m_graphics,
                pp.BackBufferWidth, pp.BackBufferHeight, 1,
                normal_surf, pp.MultiSampleType, pp.MultiSampleQuality);

            m_rtDiffuse = new RenderTarget2D(m_graphics,
                pp.BackBufferWidth, pp.BackBufferHeight, 1,
                SurfaceFormat.Color, pp.MultiSampleType, pp.MultiSampleQuality);

            m_rtMaterial = new RenderTarget2D(m_graphics,
                pp.BackBufferWidth, pp.BackBufferHeight, 1,
                SurfaceFormat.Color, pp.MultiSampleType, pp.MultiSampleQuality);

            m_rtAllLights = new RenderTarget2D(m_graphics,
                pp.BackBufferWidth, pp.BackBufferHeight, 1,
                SurfaceFormat.Color);


            // For forward rendering
            m_frwdRender = new RenderTarget2D(m_graphics,
                pp.BackBufferWidth, pp.BackBufferHeight, 1,
                SurfaceFormat.Color, pp.MultiSampleType, pp.MultiSampleQuality);

            m_frwdDepth = new RenderTarget2D(m_graphics,
                pp.BackBufferWidth, pp.BackBufferHeight, 1,
                SurfaceFormat.Single, pp.MultiSampleType, pp.MultiSampleQuality);



            m_rtSSAO = new RenderTarget2D(m_graphics,
                pp.BackBufferWidth / 2, pp.BackBufferHeight / 2, 1,
                SurfaceFormat.Color, pp.MultiSampleType, pp.MultiSampleQuality);

            m_rtSpare = new RenderTarget2D(m_graphics,
                pp.BackBufferWidth / 2, pp.BackBufferHeight / 2, 1,
                SurfaceFormat.Color, pp.MultiSampleType, pp.MultiSampleQuality);
            m_rtSpare2 = new RenderTarget2D(m_graphics,
                pp.BackBufferWidth / 2, pp.BackBufferHeight / 2, 1,
                SurfaceFormat.Color, pp.MultiSampleType, pp.MultiSampleQuality);

            m_rtSpareFS1 = new RenderTarget2D(m_graphics,
                pp.BackBufferWidth, pp.BackBufferHeight, 1,
                SurfaceFormat.Color, pp.MultiSampleType, pp.MultiSampleQuality);

            m_rtFinalShadow = new RenderTarget2D(m_graphics,
                pp.BackBufferWidth, pp.BackBufferHeight, 1, SurfaceFormat.Color, pp.MultiSampleType, pp.MultiSampleQuality);

#if WINDOWS
            DepthFormat perferrred_depth_format = DepthFormat.Depth16;
#else
            DepthFormat perferrred_depth_format = DepthFormat.Depth24;
#endif

            m_dsbShadow = new DepthStencilBuffer(m_graphics,
                m_shadowRTDimension, m_shadowRTDimension, perferrred_depth_format);

            //m_otherRenderingDSB = new DepthStencilBuffer(m_graphics,
            //    pp.BackBufferWidth, pp.BackBufferHeight, pp.AutoDepthStencilFormat);

            try
            {
#if ENGINE
                m_renderTargetTexture = new ResolveTexture2D(m_graphics, m_graphicsManager.PreferredBackBufferWidth,
                                                                                m_graphicsManager.PreferredBackBufferHeight, 1,
                                                                                 SurfaceFormat.Color);
#else
                m_renderTargetTexture = new ResolveTexture2D(m_graphics, pp.BackBufferWidth,
                                                                pp.BackBufferHeight, 1,
                                                                 SurfaceFormat.Color);
#endif
            }
            catch (Exception e)
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CVisual", e.ToString());
                return false;
            }


            // Half pixel
            m_halfPixel.X = -0.5f / (float)pp.BackBufferWidth;
            m_halfPixel.Y = 0.5f / (float)pp.BackBufferHeight;

            m_halfPixel2.X = 0.5f / (float)pp.BackBufferWidth;
            m_halfPixel2.Y = 0.5f / (float)pp.BackBufferHeight;

            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CVisual", "MRTs created.");

            return true;
        }

        public virtual void RenderDeferredMRTs(Onderzoek.Input.CInputData InputData)
        {
            if (InputData.m_renderMRTs == true)
            {
                PresentationParameters pp = m_graphics.PresentationParameters;

                int width = pp.BackBufferWidth / 4;
                int height = pp.BackBufferHeight / 4;

                Rectangle rect0 = new Rectangle(0, 0, width, height);
                Rectangle rect1 = new Rectangle(width * 2, 0, width * 2, height * 2);
                Rectangle rect2 = new Rectangle(width*2, 0, width, height);
                Rectangle rect3 = new Rectangle(width*3, 0, width, height);
                Rectangle rect4 = new Rectangle(0, height, width, height);
                Rectangle rect5 = new Rectangle(width, height, width, height);
                Rectangle rect6 = new Rectangle(width*2, height, width, height);
                Rectangle rect7 = new Rectangle(width*3, height, width, height);
                //Rectangle rect7 = new Rectangle(0, 0, width * 4, height * 4);
                Rectangle rect8 = new Rectangle(0, height * 2, width, height);
                Rectangle rect9 = new Rectangle(0, 0, pp.BackBufferWidth, pp.BackBufferHeight / 16);
                Rectangle rect10 = new Rectangle(0, 0, 128, 128);

                m_spriteBatch.Begin();

                m_spriteBatch.Draw(m_rtDiffuse.GetTexture(), rect0, Color.White);
                m_spriteBatch.Draw(m_rtNormal.GetTexture(), rect2, Color.White);
                m_spriteBatch.Draw(m_rtMaterial.GetTexture(), rect3, Color.White);
                m_spriteBatch.Draw(m_rtAllLights.GetTexture(), rect4, Color.White);
                if (m_shadowsActive == true)
                {
                    m_spriteBatch.Draw(m_rtFinalShadow.GetTexture(), rect8, Color.White);

                    int count = 0;
                    for (int i = 0; i < m_rtShadow.Count; ++i)
                    {
                        if (count >= 4)
                        {
                            break;
                        }

                        Rectangle shadow_rect = new Rectangle(width * i, height * 3, width, height);
                        m_spriteBatch.Draw(m_rtShadow[i].GetTexture(), shadow_rect, Color.White);

                        ++count;
                    }
                }

                //if (m_supportsSM3 == true && m_SSAOActive == true)
                //{
                //    //m_spriteBatch.Draw(m_rtSSAO.GetTexture(), rect7, Color.White);
                //    m_spriteBatch.Draw(m_rtSSAO.GetTexture(), rect7, Color.White);
                //}

                m_spriteBatch.End();
            }
        }

        public void Reset()
        {
            for (int i = 0; i < m_rtShadow.Count; ++i)
            {
                m_rtShadow[i].Dispose();
                m_rtShadow[i] = null;
            }
            m_rtShadow.Clear();

            if (m_dsbShadow != null)
            {
                m_dsbShadow.Dispose();
                m_dsbShadow = null;
                m_originalDSB = null;
            }

#if WINDOWS
            //if (m_otherRenderingDSB != null)
            //{
            //    m_otherRenderingDSB.Dispose();
            //    m_otherRenderingDSB = null;
                m_backupedNormalDSB = null;
            //}
#endif

            if (m_renderTargetTexture != null)
            {
                m_renderTargetTexture.Dispose();
                m_renderTargetTexture = null;
            }

            if (m_rtFinalShadow != null)
            {
                m_rtFinalShadow.Dispose();
                m_rtFinalShadow = null;
            }

            if (m_frwdRender != null)
            {
                m_frwdRender.Dispose();
                m_frwdRender = null;
            }
            if (m_frwdDepth != null)
            {
                m_frwdDepth.Dispose();
                m_frwdDepth = null;
            }

            if (m_rtDepth != null)
            {
                m_rtDepth.Dispose();
                m_rtDepth = null;
            }
            if (m_rtNormal != null)
            {
                m_rtNormal.Dispose();
                m_rtNormal = null;
            }
            if (m_rtDiffuse != null)
            {
                m_rtDiffuse.Dispose();
                m_rtDiffuse = null;
            }
            if (m_rtMaterial != null)
            {
                m_rtMaterial.Dispose();
                m_rtMaterial = null;
            }
            if (m_rtAllLights != null)
            {
                m_rtAllLights.Dispose();
                m_rtAllLights = null;
            }
            if (m_rtSSAO != null)
            {
                m_rtSSAO.Dispose();
                m_rtSSAO = null;
            }
            if (m_rtSpare != null)
            {
                m_rtSpare.Dispose();
                m_rtSpare = null;
            }
            if (m_rtSpare2 != null)
            {
                m_rtSpare2.Dispose();
                m_rtSpare2 = null;
            }
            if (m_rtSpareFS1 != null)
            {
                m_rtSpareFS1.Dispose();
                m_rtSpareFS1 = null;
            }

            CreateMRT();
        }

        public int LoadHudTexture(string TexName, ContentManager Content)
        {
            // If the lsit already sontains the key/image filename then it will also conatina the tex id.
            if (m_hudTexDictionary.ContainsKey(TexName) == true)
            {
                return m_hudTexDictionary[TexName];
            }

            // Otherwise it doesn't and load a new texture.
            Texture2D temp = Content.Load<Texture2D>(TexName);

            m_hudTextures.Add(temp);

            m_hudTexDictionary.Add(TexName, (m_hudTextures.Count - 1));

            return ( m_hudTextures.Count - 1 );
        }

        public Vector2 GetHudTextureResolution( int ID )
        {
            if (m_hudTextures[ID] == null)
            {
                return Vector2.Zero;
            }

            return new Vector2(m_hudTextures[ID].Width, m_hudTextures[ID].Height);
        }

        public void EnableDepthTest()
        {
            // Enabling the depth buffer
            m_graphics.RenderState.DepthBufferEnable = true;
            m_graphics.RenderState.DepthBufferWriteEnable = true;
        }

        public void DisableDepthTest()
        {
            // Enabling the depth buffer
            m_graphics.RenderState.DepthBufferEnable = false;
            m_graphics.RenderState.DepthBufferWriteEnable = false;
        }

        public void EnableDepthCompare()
        {
            // Enabling the depth buffer
            m_graphics.RenderState.DepthBufferEnable = true;
        }

        public void DisableDepthCompare()
        {
            // Enabling the depth buffer
            m_graphics.RenderState.DepthBufferEnable = false;
        }

        public virtual bool Load( ContentManager Content )
        {
#if EDITOR
            CBBLoader.Instance.LoadBB(Content);
#endif

            // Font stuff
            m_fontArial = Content.Load<SpriteFont>("Font/Arial");
            m_fontArialPos = new Vector2(10, 10);

            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CVisual", "Font Loaded.");

            // Loading the test effect file
            m_effectPhong = new Effect[4];
            m_effectPhong[0] = Content.Load<Effect>("fx/PhongSimple");
            m_effectPhong[1] = Content.Load<Effect>("fx/PhongTex");
            m_effectPhong[2] = Content.Load<Effect>("fx/PhongTexNorm");
            m_effectPhong[3] = Content.Load<Effect>("fx/PhongTexNormSpec");

            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CVisual", "Phong Effect Loaded.");

            m_effectForwardPhong = new Effect[4];
            m_effectForwardPhong[0] = Content.Load<Effect>("fx/ForwardPhongSimple");
            m_effectForwardPhong[1] = Content.Load<Effect>("fx/ForwardPhongTex");
            m_effectForwardPhong[2] = Content.Load<Effect>("fx/ForwardPhongTexNorm");
            m_effectForwardPhong[3] = Content.Load<Effect>("fx/ForwardPhongTexNormSpec");

            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CVisual", "ForwardPhong Effect Loaded.");

            m_effectParticleRender = new Effect[6];
            m_effectParticleRender[0] = Content.Load<Effect>("fx/ParticleRenderSimple");
            m_effectParticleRender[1] = Content.Load<Effect>("fx/ParticleRenderTex");
            m_effectParticleRender[2] = Content.Load<Effect>("fx/ParticleRenderAnimTex");
            if (m_supportsSM3 == true)
            {
                m_effectParticleRender[3] = Content.Load<Effect>("fx/SoftParticleRenderSimple");
                m_effectParticleRender[4] = Content.Load<Effect>("fx/SoftParticleRenderTex");
                m_effectParticleRender[5] = Content.Load<Effect>("fx/SoftParticleRenderAnimTex");
            }

            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CVisual", "ParticleRender Effect Loaded.");

            m_effectBound = Content.Load<Effect>("fx/BoundingBox");

            m_effectSkinned = new Effect[4];
            m_effectSkinned[0] = Content.Load<Effect>("fx/PhongSkin");
            m_effectSkinned[1] = Content.Load<Effect>("fx/PhongSkinTex");
            m_effectSkinned[2] = Content.Load<Effect>("fx/PhongSkinTexNorm");
            m_effectSkinned[3] = Content.Load<Effect>("fx/PhongSkinTexNormSpec");

            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CVisual", "Phong Skin Effect Loaded.");

            m_effectForwardSkinned = new Effect[4];
            m_effectForwardSkinned[0] = Content.Load<Effect>("fx/ForwardPhongSkin");
            m_effectForwardSkinned[1] = Content.Load<Effect>("fx/ForwardPhongSkinTex");
            m_effectForwardSkinned[2] = Content.Load<Effect>("fx/ForwardPhongSkinTexNorm");
            m_effectForwardSkinned[3] = Content.Load<Effect>("fx/ForwardPhongSkinTexNormSpec");

            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CVisual", "ForwardPhong Skin Effect Loaded.");

            m_effectPhongShadow = new Effect[2];
            m_effectPhongShadow[0] = Content.Load<Effect>("fx/PhongShadowSimple");
            m_effectPhongShadow[1] = Content.Load<Effect>("fx/PhongShadowSkin");

            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CVisual", "Phong Shadow Effect Loaded.");

            m_effectShadow = Content.Load<Effect>("fx/Shadow");

            m_effectDeferredDirLight = Content.Load<Effect>("fx/DeferredDirLight");

            m_effectDeferredPtLight = Content.Load<Effect>("fx/DeferredPointLight");

            m_effectDeferredSpotLight = Content.Load<Effect>("fx/DeferredSpotLight");

            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CVisual", "Lighting Effects Loaded.");

            m_effectFinalDeferredShadow = Content.Load<Effect>("fx/FinalDeferredShadow");
            m_effectFinalDeferredSSAOShadow = Content.Load<Effect>("fx/FinalDeferredSSAOShadow");
            m_effectFinalDeferredSSAO = Content.Load<Effect>("fx/FinalDeferredSSAO");
            m_effectFinalDeferredBasic = Content.Load<Effect>("fx/FinalDeferredBasic");

            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CVisual", "Deferred Shader Effects Loaded.");

            SelectFinalDeferredShader();

            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CVisual", "Suitable Deferred Shader Effect chosen.");

            m_ssao = Content.Load<Effect>("fx/SSAO2");

            m_blur = Content.Load<Effect>("fx/Blur");

            m_copy = Content.Load<Effect>("fx/Copy");

            // Glitter stuff
            m_effectGlitterForwardPhong = new Effect[4];
            m_effectGlitterForwardPhong[0] = Content.Load<Effect>("fx/GlitterForwardPhongSimple");
            m_effectGlitterForwardPhong[1] = Content.Load<Effect>("fx/GlitterForwardPhongTex");
            m_effectGlitterForwardPhong[2] = Content.Load<Effect>("fx/GlitterForwardPhongTexNorm");
            m_effectGlitterForwardPhong[3] = Content.Load<Effect>("fx/GlitterForwardPhongTexNormSpec");
            m_noiseVolume = Content.Load<Texture3D>("Texture/NoiseVolume");

            // Water stuff
            m_effectWaterRenderArray = new Effect[4];
            m_effectWaterRenderArray[0] = Content.Load<Effect>("fx/WaterRender");
            m_effectWaterRenderArray[1] = Content.Load<Effect>("fx/WaterRenderReflect");
            if (m_graphics.GraphicsDeviceCapabilities.PixelShaderVersion.Major >= 3)
            {
                m_effectWaterRenderArray[0] = Content.Load<Effect>("fx/WaterRenderSoft");
                m_effectWaterRenderArray[1] = Content.Load<Effect>("fx/WaterRenderReflectSoft");
            }

            RenderReflect = CDataFromXML.GetWaterReflect();
            m_reflectionResolution = CDataFromXML.GetWaterResolution();
            SoftWater = CDataFromXML.GetSoftWater();
            m_waterRipple = Content.Load<Texture2D>("Texture/waves");

            // Explosion distortion stuff
            m_effectDistort = Content.Load<Effect>("fx/Distort");
            m_maxDistortData = 5;
            m_explosionModelData = new Model[m_maxDistortData];
            m_explosionWorldMatrix = new Matrix[m_maxDistortData];
            m_explosionDistortMultiplier = new float[m_maxDistortData];

            if (m_supportsSM3 == true)
            {
                m_effectWaterRenderArray[2] = Content.Load<Effect>("fx/WaterRenderSoft");
                m_effectWaterRenderArray[3] = Content.Load<Effect>("fx/WaterRenderReflectSoft");
            }

            if (m_renderReflect == true)
            {
                m_effectWaterRender = m_effectWaterRenderArray[1];
            }
            else
            {
                m_effectWaterRender = m_effectWaterRenderArray[0];
            }

            m_meshIDs = new Dictionary<int, List<CEntity.SPartAndMatNum>>[10];
            m_waterModelData = new Model[10];
            m_waterWorldMatrix = new Matrix[10];
            m_waterMaterialList = new List<CMaterial>[10];
            m_waterID = new int[10];
            m_waterRenderData = new List<CWaterRendering>();
            m_waterToRender = new List<int>();


            m_combineOpaqueAndTranslucent = Content.Load<Effect>("fx/CombineOpaqueAndTranslucent");

            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CVisual", "SSAO, Blur and Copy loaded.");

            try
            {
                m_particleModel = Content.Load<Model>("Model/Particle");
            }
            catch (Exception e)
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CVisual", e.ToString());
                return false;
            }
            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CVisual", "Model/Particle loaded");

            m_normParticleRenderer = new CPRNormal();

            m_pointLightSphere = Content.Load<Model>("Model/sphere");
            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CVisual", "Model/sphere loaded");
#if WINDOWS
            if (m_supportsSM3 == true)
            {
                m_particleRenderer = new CPRHardwareInstance(m_particleModel);
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CVisual", "CPRHardwareInstance will be used for rendering particles");
            }
            else
#endif
            {
                m_particleRenderer = new CPRShaderInstance(m_particleModel);
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CVisual", "CPRShaderInstance will be used for rendering particles");
            }

            m_spotLightCone = Content.Load<Model>("Model/cone");
            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CVisual", "Model/cone loaded");

            m_rnTexture = Content.Load<Texture2D>("Texture/random");

            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CVisual", "Visual Content Loaded.");

            return true;
        }

        public int GetShaderID( CMaterial Material )
        {
            if (Material.m_textureID != -1)
            {
                if (Material.m_normalID != -1)
                {
                    if (Material.m_specularID != -1)
                    {
                        return 3;
                    }
                    else
                    {
                        return 2;
                    }
                }
                else
                {
                    return 1;
                }
            }
            else
            {
                return 0;
            }
        }

        public void ClearScreen()
        {
            m_graphics.Clear(Color.TransparentBlack);

            m_graphics.SetRenderTarget(0, m_rtDepth);
            m_graphics.SetRenderTarget(1, m_rtDiffuse);
            m_graphics.SetRenderTarget(2, m_rtNormal);
            m_graphics.SetRenderTarget(3, m_rtMaterial);

            m_graphics.Clear(Color.TransparentBlack);

            m_graphics.SetRenderTarget(0, null);
            m_graphics.SetRenderTarget(1, null);
            m_graphics.SetRenderTarget(2, null);
            m_graphics.SetRenderTarget(3, null);
        }

        public void ClearScreen( Color ClearColor )
        {
            m_graphics.Clear(ClearColor);
        }

        public bool ChangeSoftOnOff()
        {
            if (m_supportsSM3 == true)
            {
                int offset = 0;
                if (m_softWater == true)
                {
                    m_softWater = false;
                }
                else
                {
                    offset = 2;
                    m_softWater = true;
                }

                if (m_renderReflect == true)
                {
                    m_effectWaterRender = m_effectWaterRenderArray[1 + offset];
                }
                else
                {
                    m_effectWaterRender = m_effectWaterRenderArray[0 + offset];
                }

                return true;
            }

            return false;
        }

        public void ChangeReflectOnOff()
        {
            if (m_renderReflect == true)
            {
                m_renderReflect = false;
            }
            else
            {
                m_renderReflect = true;
            }
        }

        public void ChangeReflectionResolution()
        {
            switch (m_reflectionResolution)
            {
                case 1:
                    {
                        m_reflectionResolution = 4;
                        break;
                    }
                case 2:
                    {
                        m_reflectionResolution = 1;
                        break;
                    }
                case 4:
                    {
                        m_reflectionResolution = 2;
                        break;
                    }
            }
        }

        public int ReflectionResolution
        {
            set { m_reflectionResolution = value; }
            get { return m_reflectionResolution; }
        }

        /// <summary>
        /// This gets called by the water entities.
        /// Each water entity has it's own reflection render target.
        /// </summary>
        /// <returns>Returns the render target ID which needs to be used during the rendering of the water.</returns>
        public int InitialiseForWaterRendering(Vector3 Normal, Vector3 Position, /*float Reflectance,*/ float ReflecRefracOffset, float Speed,
            CWaterEntity WaterEntity, float FresnelOffset)
        {
            CWaterRendering temp = new CWaterRendering();

            if (m_renderReflect == true)
            {
                PresentationParameters pp = m_graphics.PresentationParameters;

                temp.m_renderTarget = new RenderTarget2D(m_graphics,
                    pp.BackBufferWidth / m_reflectionResolution, pp.BackBufferHeight / m_reflectionResolution, 1,
                    SurfaceFormat.Color, pp.MultiSampleType, pp.MultiSampleQuality);

                m_graphics.SetRenderTarget(0, temp.m_renderTarget);
                m_graphics.SetRenderTarget(0, null);

#if WINDOWS
                temp.m_depthBuffer = new DepthStencilBuffer(m_graphics,
                    pp.BackBufferWidth / m_reflectionResolution, pp.BackBufferHeight / m_reflectionResolution, DepthFormat.Depth16);
#else
                temp.m_depthBuffer = new DepthStencilBuffer(m_graphics,
                    pp.BackBufferWidth / m_reflectionResolution, pp.BackBufferHeight / m_reflectionResolution, DepthFormat.Depth24);
#endif

                temp.m_reflectCamera = new CWCamera();
                temp.m_clipPlane = CToolbox.CreatePlane(Normal, Position);
                //temp.m_reflectance = Reflectance;
            }

            temp.m_waterEntity = WaterEntity;
            temp.m_reflecRefracOffset = ReflecRefracOffset;
            temp.m_fresnelOffset = FresnelOffset;
            temp.m_perpWindDirection = CToolbox.CrossProduct(m_windDirection, Normal);
            temp.m_speed = Speed;

            if (m_waterRenderData == null)
            {
                m_waterRenderData = new List<CWaterRendering>();
            }
            m_waterRenderData.Add(temp);

            return m_waterRenderData.Count - 1;
        }

        /// <summary>
        /// This needs to be called to dispose of the water render target.
        /// </summary>
        /// <param name="WaterRTID">The render target ID which corresponds to the water render target.</param>
        public void DisposeOfRenderTargetForWater(int WaterRTID)
        {
            if (m_waterRenderData != null)
            {
                if (WaterRTID < m_waterRenderData.Count && WaterRTID > -1)
                {
                    if (m_waterRenderData[WaterRTID] != null)
                    {
                        m_waterRenderData[WaterRTID].DisposeOfRenderTarget();
                        m_waterRenderData[WaterRTID].m_reflectCamera = null;
                        m_waterRenderData[WaterRTID].m_waterEntity = null;
                    }
                }

                // Look through the list of render targets.
                // If they're all null then clear the list and reset the List count to 0.
                bool all_null = true;
                for (int i = 0; i < m_waterRenderData.Count; ++i)
                {
                    if (m_waterRenderData[i].m_renderTarget != null)
                    {
                        all_null = false;
                        break;
                    }
                }

                if (all_null == true)
                {
                    m_waterRenderData.Clear();
                    m_waterRenderData = null;
                    m_waterToRender.Clear();

                    //m_waterRipple.Dispose();
                    //m_waterRipple = null;
                    //m_waterModelData = null;
                    //m_waterWorldMatrix = null;
                    //m_waterMaterialList = null;
                    //m_meshIDs = null;
                    //m_waterID = null;
                }
            }
        }

        /// <summary>
        /// This gets called when the water entity is needed to be rendered.
        /// </summary>
        /// <param name="WaterRTID">The water id which corresponds to the water rendering data.</param>
        /// <param name="ViewMatrix">The view matrix of the reflection camera.</param>
        /// <param name="Height">The new height of the water entity.</param>
        public void SaveWaterRenderingData(int WaterRTID)
        {
            if (m_renderReflect == true)
            {
                m_waterToRender.Add(WaterRTID);
            }
        }

        public void SetDT(GameTime DT)
        {
            m_time += (float)DT.ElapsedGameTime.Milliseconds / 100000.0f;
        }

        public void SetupForWaterRendering()
        {
            CVisual.Instance.EnableDepthTest();

            m_originalDSB = m_graphics.DepthStencilBuffer;
            m_graphics.ClipPlanes[0].IsEnabled = true;
        }

        public void PackUpWaterRendering()
        {
            m_graphics.DepthStencilBuffer = m_originalDSB;
            m_graphics.ClipPlanes[0].IsEnabled = false;
            m_graphics.SetRenderTarget(0, null);

            m_waterToRender.Clear();

            m_lightInForwardRendering = false;
        }

        public int NumberOfWaterEntitiesToRender
        {
            get { return m_waterToRender.Count; }
        }

        public void StartWaterRendering(out CWCamera ReflectCamera, CCamera Camera, int Index, CSkyBox SkyBox)
        {
            // Create the new plane.
            m_waterRenderData[m_waterToRender[Index]].SetNewHeight(m_waterRenderData[m_waterToRender[Index]].m_waterEntity.GetWorldPosition());

            // First get the reflect camera from the water entity.
            CWCamera reflect_camera;
            m_waterRenderData[m_waterToRender[Index]].m_waterEntity.SetUpCamera(Camera, out reflect_camera,
                m_waterRenderData[m_waterToRender[Index]].m_clipPlane);

            // Set the out paramter.
            ReflectCamera = reflect_camera;

            // Set the data into the local data list.
            m_waterRenderData[m_waterToRender[Index]].SetCamera(reflect_camera);

            m_graphics.DepthStencilBuffer = m_waterRenderData[m_waterToRender[Index]].m_depthBuffer;
            m_graphics.SetRenderTarget(0, m_waterRenderData[m_waterToRender[Index]].m_renderTarget);
            m_graphics.Clear(Color.TransparentBlack);

            // Render the skybox
            if (SkyBox != null)
            {
                m_graphics.ClipPlanes[0].IsEnabled = false;
                DisableDepthTest();
                SkyBox.SimpleRender(ReflectCamera);
                EnableDepthTest();
                m_graphics.ClipPlanes[0].IsEnabled = true;
            }

            Matrix inv = Matrix.Invert(ReflectCamera.GetView() * ReflectCamera.GetProjection());
            inv = Matrix.Transpose(inv);

            m_graphics.ClipPlanes[0].Plane = new Plane(Vector4.Transform(new Vector4(m_waterRenderData[m_waterToRender[Index]].m_clipPlane.Normal,
                m_waterRenderData[m_waterToRender[Index]].m_clipPlane.D), inv));
        }

        /// <summary>
        /// This is for rendering water only.
        /// </summary>
        /// <param name="WaterRTID"></param>
        /// <param name="ModelData"></param>
        /// <param name="Camera"></param>
        /// <param name="Material"></param>
        /// <param name="WorldMatrix"></param>
        /// <param name="MeshIDs"></param>
        public void RenderWater(int WaterRTID, Model ModelData, List<CMaterial> Material, Matrix WorldMatrix,
            Dictionary<int, List<CEntity.SPartAndMatNum>> MeshIDs)
        {
            if (m_maxWaterEffect < 10)
            {
                m_waterID[m_maxWaterEffect] = WaterRTID;
                m_waterModelData[m_maxWaterEffect] = ModelData;
                m_waterWorldMatrix[m_maxWaterEffect] = WorldMatrix;
                m_waterMaterialList[m_maxWaterEffect] = Material;
                m_meshIDs[m_maxWaterEffect] = MeshIDs;

                ++m_maxWaterEffect;
            }
        }

        public void RenderWater(CCamera Camera)
        {
            if (m_maxWaterEffect != 0)
            {
                m_lightInForwardRendering = true;

                m_effectWaterRender.Parameters["View"].SetValue(Camera.GetView());
                m_effectWaterRender.Parameters["Projection"].SetValue(Camera.GetProjection());
                m_effectWaterRender.Parameters["g_cameraPosition"].SetValue(Camera.GetPosition());
                m_effectWaterRender.Parameters["WaterRippleTexture"].SetValue(m_waterRipple);
                m_effectWaterRender.Parameters["g_time"].SetValue(m_time);
                m_effectWaterRender.Parameters["g_windDir"].SetValue(m_windDirection);
                m_effectWaterRender.Parameters["g_halfPixel"].SetValue(m_halfPixel2);

#if XBOX360
                m_effectWaterRender.Parameters["OpaqueDepthTexture"].SetValue(m_rtDepth.GetTexture());
                m_effectWaterRender.Parameters["TranslucentDepthTexture"].SetValue(m_frwdDepth.GetTexture());
                m_effectWaterRender.Parameters["g_isWindows"].SetValue(false);
                m_effectWaterRender.Parameters["g_farDistance"].SetValue(Camera.GetFarClipDistance());
                m_effectWaterRender.Parameters["BackBufferTexture"].SetValue(/*m_rtSpareFS1*/m_rtDiffuse.GetTexture());
#else
                m_effectWaterRender.Parameters["g_isWindows"].SetValue(true);
                m_effectWaterRender.Parameters["BackBufferTexture"].SetValue(m_rtSpareFS1.GetTexture());
#endif

                if (m_softWater == true)
                {
                    m_effectWaterRender.Parameters["DepthMapTexture"].SetValue(m_rtDepth.GetTexture());
                    m_effectWaterRender.Parameters["g_farDistance"].SetValue(Camera.GetFarClipDistance());
                }

                if (m_lightInForwardRendering == true)
                {
                    m_effectWaterRender.Parameters["g_lightDirection"].SetValue(m_forwardRenderLight.s_direction);
                    m_effectWaterRender.Parameters["g_lightColour"].SetValue(new Vector4(m_forwardRenderLight.s_colour * m_forwardRenderLight.s_intensity, 1.0f));
                    m_effectWaterRender.Parameters["g_lightIntensity"].SetValue(m_forwardRenderLight.s_intensity);
                    m_effectWaterRender.Parameters["g_lightEnabled"].SetValue(true);
                }
                else
                {
                    m_effectWaterRender.Parameters["g_lightEnabled"].SetValue(false);
                }
            }

            for (int i = 0; i < m_maxWaterEffect; ++i)
            {
                if (m_renderReflect == true)
                {
                    m_effectWaterRender.Parameters["ReflectView"].SetValue(m_waterRenderData[m_waterID[i]].m_reflectCamera.GetView());
                    //m_effectWaterRender.Parameters["g_reflectance"].SetValue(m_waterRenderData[m_waterID[i]].m_reflectance);
                    m_effectWaterRender.Parameters["ProjectedTexture"].SetValue(m_waterRenderData[m_waterID[i]].m_renderTarget.GetTexture());
                }

                m_effectWaterRender.Parameters["g_perpDir"].SetValue(m_waterRenderData[m_waterID[i]].m_perpWindDirection);
                m_effectWaterRender.Parameters["g_reflecRefracOffset"].SetValue(m_waterRenderData[m_waterID[i]].m_reflecRefracOffset);
                m_effectWaterRender.Parameters["g_speed"].SetValue(m_waterRenderData[m_waterID[i]].m_speed);
                m_effectWaterRender.Parameters["g_fresnelOffset"].SetValue(m_waterRenderData[m_waterID[i]].m_fresnelOffset);

                // Might need removing for optimisation, that will mean that all models need their transforms frozen.
                Matrix[] transforms = new Matrix[m_waterModelData[i].Bones.Count];

                m_waterModelData[i].CopyAbsoluteBoneTransformsTo(transforms);

                int cur_mat_id = 0;
                foreach (KeyValuePair<int, List<CEntity.SPartAndMatNum>> mesh_id in m_meshIDs[i])
                {
                    ModelMesh mesh = m_waterModelData[i].Meshes[mesh_id.Key];

                    Matrix new_world_matrix = transforms[mesh.ParentBone.Index] * m_waterWorldMatrix[i];

                    for (int j = 0; j < mesh_id.Value.Count; ++j)
                    {
                        cur_mat_id = mesh_id.Value[j].s_partMaterial;
                        ModelMeshPart part = mesh.MeshParts[mesh_id.Value[j].s_partNum];

                        m_effectWaterRender.CurrentTechnique = m_effectWaterRender.Techniques["WaterRendering"];

                        // Setting the vertex shader specific global variables
                        m_effectWaterRender.Parameters["World"].SetValue(new_world_matrix);

                        // Setting the global material variables
                        m_effectWaterRender.Parameters["g_diffuse"].SetValue(m_waterMaterialList[i][cur_mat_id].m_diffuse);
                        m_effectWaterRender.Parameters["g_ambientAndEmissive"].SetValue(m_waterMaterialList[i][cur_mat_id].m_ambient +
                            m_waterMaterialList[i][cur_mat_id].m_emissive);
                        m_effectWaterRender.Parameters["g_specularColour"].SetValue(m_waterMaterialList[i][cur_mat_id].m_specularColour);
                        m_effectWaterRender.Parameters["g_specularPower"].SetValue(m_waterMaterialList[i][cur_mat_id].m_specularPower);

                        m_effectWaterRender.Begin();

                        m_effectWaterRender.CurrentTechnique.Passes[0].Begin();
                        m_graphics.VertexDeclaration = part.VertexDeclaration;
                        m_graphics.Indices = mesh.IndexBuffer;
                        m_graphics.Vertices[0].SetSource(mesh.VertexBuffer, part.StreamOffset, part.VertexStride);
                        m_effectWaterRender.CurrentTechnique.Passes[0].End();

                        m_effectWaterRender.End();

                        m_graphics.DrawIndexedPrimitives(PrimitiveType.TriangleList, part.BaseVertex, 0, part.NumVertices, part.StartIndex, part.PrimitiveCount);

                        m_numNormalPolygonsRendered += part.PrimitiveCount;
                    }
                }
            }

            m_maxWaterEffect = 0;
        }

        // Render content pipeline models (.x and .fbx)
        public void StoreExplosionRippleDataForRendering(Model ModelData, CCamera Camera, Matrix WorldMatrix, float DistortMultiplier)
        {
            if (m_maxDistortEffect < m_maxDistortData)
            {
                m_explosionModelData[m_maxDistortEffect] = ModelData;
                m_explosionWorldMatrix[m_maxDistortEffect] = WorldMatrix;
                m_explosionDistortMultiplier[m_maxDistortEffect] = DistortMultiplier;

                ++m_maxDistortEffect;
            }
        }

        // Render content pipeline models (.x and .fbx)
        public void RenderExplosionRipple(CCamera Camera)
        {
            if (m_maxDistortEffect != 0)
            {
#if XBOX360
                m_effectDistort.Parameters["OpaqueDepthTexture"].SetValue(m_rtDepth.GetTexture());
                m_effectDistort.Parameters["TranslucentDepthTexture"].SetValue(m_frwdDepth.GetTexture());
                m_effectDistort.Parameters["g_isWindows"].SetValue(false);
                m_effectDistort.Parameters["g_farDistance"].SetValue(Camera.GetFarClipDistance());
#else
                m_effectDistort.Parameters["g_isWindows"].SetValue(true);
#endif

                m_effectDistort.Parameters["View"].SetValue(Camera.GetView());
                m_effectDistort.Parameters["Projection"].SetValue(Camera.GetProjection());
                m_effectDistort.Parameters["g_cameraPosition"].SetValue(Camera.GetPosition());
                m_effectDistort.Parameters["g_halfPixel"].SetValue(m_halfPixel);
            }

            for (int i = m_maxDistortEffect - 1; i >= 0; --i)
            {
                m_effectDistort.Parameters["World"].SetValue(m_explosionWorldMatrix[i]);
                m_effectDistort.Parameters["g_distortMultiplier"].SetValue(m_explosionDistortMultiplier[i]);

#if WINDOWS
                m_effectDistort.Parameters["BackBufferTexture"].SetValue(m_rtSpareFS1.GetTexture());
#else
                m_effectDistort.Parameters["BackBufferTexture"].SetValue(/*m_rtSpareFS1*/m_rtDiffuse.GetTexture());
#endif

                Matrix[] transforms = new Matrix[m_explosionModelData[i].Bones.Count];
                m_explosionModelData[i].CopyAbsoluteBoneTransformsTo(transforms);

                foreach (ModelMesh mesh in m_explosionModelData[i].Meshes)
                {
                    foreach (ModelMeshPart part in mesh.MeshParts)
                    {
                        m_effectDistort.Begin();
                        m_effectDistort.CurrentTechnique.Passes[0].Begin();

                        m_graphics.VertexDeclaration = part.VertexDeclaration;
                        m_graphics.Indices = mesh.IndexBuffer;
                        m_graphics.Vertices[0].SetSource(mesh.VertexBuffer, part.StreamOffset, part.VertexStride);

                        m_effectDistort.CurrentTechnique.Passes[0].End();
                        m_effectDistort.End();

                        m_graphics.DrawIndexedPrimitives(PrimitiveType.TriangleList, part.BaseVertex, 0, part.NumVertices, part.StartIndex, part.PrimitiveCount);
                    }
                }

                //m_modelData[m_maxDistortEffect] .RemoveAt(i);
                //m_worldMatrix.RemoveAt(i);
                //m_distortMultiplier.RemoveAt(i);

                //--m_maxDistortEffect;
            }

            m_maxDistortEffect = 0;
        }

        public void UpdateForDebugScreen(GameTime PassedGameTime)
        {
            // FPS counter update code
            ++m_frameCount;

            if (PassedGameTime.TotalGameTime.TotalSeconds > m_nxtTime + 1.0)
            {
                m_nxtTime = PassedGameTime.TotalGameTime.TotalSeconds;
                m_curFPS = m_frameCount;
                m_frameCount = 0;
            }
        }

        public void AddDebugInfoToRender(string NewDebugInfo)
        {
            m_debugVariables.Add(NewDebugInfo);
        }

        public void RenderDebugInfo( Vector3 MainCameraPos, int EntityCount, int ModelCount, Onderzoek.Input.CInputData InputData )
        {
            RenderDeferredMRTs(InputData);

            if (InputData.m_showTextInformation == false)
            {
                // Need to be here otherwise all the entities will be rendered with alpha transparency on.
                m_graphics.RenderState.AlphaBlendEnable = false;

                m_numTotalPolygonsRendered = 0;
                m_numShadowPolygonsRendered = 0;
                m_numNormalPolygonsRendered = 0;

                return;
            }

            m_spriteBatch.Begin();

            // Draw The fps counter
            string output = "FPS: " + m_curFPS;

            int x_pos = -60 * m_displayIsSD;

            // Draw the string
            m_spriteBatch.DrawString(m_fontArial, "Onderzoek", m_fontArialPos, Color.AntiqueWhite, 0, new Vector2(x_pos, 0), 1.0f, SpriteEffects.None, 0.5f);
            m_spriteBatch.DrawString(m_fontArial, output, m_fontArialPos, Color.AntiqueWhite, 0, new Vector2(x_pos, -30), 1.0f, SpriteEffects.None, 0.5f);

            if (InputData.m_cntr1Connect == false)
            {
                m_spriteBatch.DrawString(m_fontArial, "Controller one is disconnected, use the PC keyboard and mouse.", m_fontArialPos, Color.AntiqueWhite, 0, new Vector2(x_pos, -50), 1.0f, SpriteEffects.None, 0.5f);
            }
            else
            {
                m_spriteBatch.DrawString(m_fontArial, "Controller one is connected.", m_fontArialPos, Color.AntiqueWhite, 0, new Vector2(x_pos, -50), 1.0f, SpriteEffects.None, 0.5f);
            }

            if (InputData.m_renderAll == true)
            {
                m_spriteBatch.DrawString(m_fontArial, "Rendering All", m_fontArialPos, Color.AntiqueWhite, 0, new Vector2(x_pos, -70), 1.0f, SpriteEffects.None, 0.5f);
            }
            else
            {
                m_spriteBatch.DrawString(m_fontArial, "Frustum Culling", m_fontArialPos, Color.AntiqueWhite, 0, new Vector2(x_pos, -70), 1.0f, SpriteEffects.None, 0.5f);
            }

            m_spriteBatch.DrawString(m_fontArial, "Camera Pos: " + MainCameraPos, m_fontArialPos, Color.AntiqueWhite, 0, new Vector2(x_pos, -90), 1.0f, SpriteEffects.None, 0.5f);

            m_spriteBatch.DrawString(m_fontArial, "Model Count: " + ModelCount, m_fontArialPos, Color.AntiqueWhite, 0, new Vector2(x_pos, -110), 1.0f, SpriteEffects.None, 0.5f);

            m_spriteBatch.DrawString(m_fontArial, "Entity Count: " + EntityCount, m_fontArialPos, Color.AntiqueWhite, 0, new Vector2(x_pos, -130), 1.0f, SpriteEffects.None, 0.5f);

            m_numShadowPolygonsRendered /= 2;
            m_numNormalPolygonsRendered /= 2;
            m_numTotalPolygonsRendered = m_numShadowPolygonsRendered + m_numNormalPolygonsRendered;

            m_spriteBatch.DrawString(m_fontArial, "Polygons rendered for shadows: " + m_numShadowPolygonsRendered, m_fontArialPos, Color.AntiqueWhite, 0, new Vector2(x_pos, -150), 1.0f, SpriteEffects.None, 0.5f);
            m_spriteBatch.DrawString(m_fontArial, "Polygons rendered for Main Screen: " + m_numNormalPolygonsRendered, m_fontArialPos, Color.AntiqueWhite, 0, new Vector2(x_pos, -190), 1.0f, SpriteEffects.None, 0.5f);
            m_spriteBatch.DrawString(m_fontArial, "Total Polygons rendered: " + m_numTotalPolygonsRendered, m_fontArialPos, Color.AntiqueWhite, 0, new Vector2(x_pos, -210), 1.0f, SpriteEffects.None, 0.5f);
            m_numTotalPolygonsRendered = 0;
            m_numShadowPolygonsRendered = 0;
            m_numNormalPolygonsRendered = 0;

            if (m_supportsSM3 == false)
            {
                m_spriteBatch.DrawString(m_fontArial, "Current Hardware does not support SSAO", m_fontArialPos, Color.AntiqueWhite, 0, new Vector2(x_pos, -270), 1.0f, SpriteEffects.None, 0.5f);
            }

            for (int i = 0; i < m_debugVariables.Count; ++i)
            {
                m_spriteBatch.DrawString(m_fontArial, m_debugVariables[i], m_fontArialPos, Color.AntiqueWhite, 0, new Vector2(x_pos, -600 - (i * 20)), 0.5f, SpriteEffects.None, 0.5f);
            }
            m_debugVariables.Clear();

            m_spriteBatch.End();

            // Need to be here otherwise all the entities will be rendered with alpha transparency on.
            m_graphics.RenderState.AlphaBlendEnable = false;
        }

        public Vector2 GetStringSize(string Text)
        {
            return m_fontArial.MeasureString(Text);
        }

        public void RenderHudText(string Text, Rectangle Position, float Scale, Color TextColour)
        {
            m_spriteBatch.Begin();

            m_spriteBatch.DrawString(m_fontArial, Text, new Vector2(Position.X, Position.Y), TextColour, 0, Vector2.Zero, Scale, SpriteEffects.None, 1.0f);

            m_spriteBatch.End();
        }

        public void RenderHudText(string Text, Vector2 Position, float Scale, Color TextColour)
        {
            m_spriteBatch.Begin();

            m_spriteBatch.DrawString(m_fontArial, Text, Position, TextColour, 0, Vector2.Zero, Scale, SpriteEffects.None, 1.0f);

            m_spriteBatch.End();
        }

        public void RenderHudElement(Rectangle Rect)
        {
            //m_spriteBatch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.Deferred, SaveStateMode.SaveState);
            m_spriteBatch.Begin();

            // Change the colour and see what happens
            m_spriteBatch.Draw(null, Rect, Color.White);

            m_spriteBatch.End();
        }

        public void RenderHudElement(int TexID, Rectangle Rect)
        {
            //m_spriteBatch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.Deferred, SaveStateMode.SaveState);
            m_spriteBatch.Begin();

            // Change the colour and see what happens
            m_spriteBatch.Draw(m_hudTextures[TexID], Rect, Color.White);

            m_spriteBatch.End();
        }

        public void RenderHudElement(int TexID, Rectangle Rect, Color Tint)
        {
            //m_spriteBatch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.Deferred, SaveStateMode.SaveState);
            m_spriteBatch.Begin();

            // Change the colour and see what happens
            m_spriteBatch.Draw(m_hudTextures[TexID], Rect, Tint);

            m_spriteBatch.End();
        }

        public void RenderHudElement(int TexID, Rectangle Rect, Rectangle Source)
        {
            //m_spriteBatch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.Deferred, SaveStateMode.SaveState);
            m_spriteBatch.Begin();

            // Change the colour and see what happens
            m_spriteBatch.Draw(m_hudTextures[TexID], Rect, Source, Color.White);

            m_spriteBatch.End();
        }

        public void RenderHudElement(int TexID, Rectangle Rect, Rectangle Source, Color Tint)
        {
            //m_spriteBatch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.Deferred, SaveStateMode.SaveState);
            m_spriteBatch.Begin();

            // Change the colour and see what happens
            m_spriteBatch.Draw(m_hudTextures[TexID], Rect, Source, Tint);

            m_spriteBatch.End();
        }

        public void RenderHudElement( int TexID, Rectangle Rect, float Rotation, Vector2 Origin )
        {
            //m_spriteBatch.Begin(SpriteBlendMode.AlphaBlend, SpriteSortMode.Deferred, SaveStateMode.SaveState);
            m_spriteBatch.Begin();

            // Change the colour and see what happens
            m_spriteBatch.Draw(m_hudTextures[TexID], Rect, null, Color.White, Rotation, Origin, SpriteEffects.None, 0);

            m_spriteBatch.End();
        }

        public void FreeHudMemory(int TexID, string TexName)
        {
            if (m_hudTextures.Count - 1 < TexID)
            {
                return;
            }

            if (m_hudTextures[TexID] == null)
            {
                return;
            }

            m_hudTextures[TexID].Dispose();
            m_hudTextures[TexID] = null;
            m_hudTexDictionary.Remove(TexName);

            // If the dictionary is empty then make sure that the hud texture list is also empty.
            if (m_hudTexDictionary.Count == 0)
            {
                m_hudTextures.Clear();
            }
            else
            {
                // If the end of hud texture list is full of null values then trim them off.
                if (m_hudTextures[m_hudTextures.Count - 1] == null)
                {
                    for (int i = m_hudTextures.Count - 1; i >= 0; --i)
                    {
                        if (m_hudTextures[i] == null)
                        {
                            m_hudTextures.RemoveAt(i);
                        }
                        else
                        {
                            // don't look any more otherwise the TexID system will break
                            i = -1;
                        }
                    }
                }
            }

            int nulled = 0;
            for (int i = 0; i < m_hudTextures.Count; ++i)
            {
                if (m_hudTextures[i] == null)
                {
                    ++nulled;
                }
            }

            if (nulled == m_hudTextures.Count)
            {
                m_hudTextures = new List<Texture2D>();
                m_hudTexDictionary = new Dictionary<string, int>();
            }
        }

        public void RemoveTexture(int TextureID)
        {
            int usage_count = 0;
            if (m_TexIDToUsageCount.TryGetValue(TextureID, out usage_count) == true)
            {
                --m_TexIDToUsageCount[TextureID];
                if (m_TexIDToUsageCount[TextureID] < 1)
                {
                    m_levelTextures[TextureID].Dispose();
                    m_levelTextures[TextureID] = null;
                }
            }
        }

        public int LoadLevelTexture(string TexName, ContentManager Content)
        {
            // If the lsit already sontains the key/image filename then it will also conatina the tex id.
            if (m_levelTexDictionary.ContainsKey(TexName) == true)
            {
                if (m_levelTextures[m_levelTexDictionary[TexName]] == null)
                {
                    m_levelTextures[m_levelTexDictionary[TexName]] = Content.Load<Texture2D>(TexName);
                }

                ++m_TexIDToUsageCount[m_levelTexDictionary[TexName]];
                return m_levelTexDictionary[TexName];
            }

            // Otherwise it doesn't, so load a new texture.
            Texture2D temp = Content.Load<Texture2D>(TexName);

            m_levelTextures.Add(temp);

            m_levelTexDictionary.Add(TexName, (m_levelTextures.Count - 1));
            m_TexIDToUsageCount.Add((m_levelTextures.Count - 1), 1);

            return (m_levelTextures.Count - 1);
        }

        public Texture2D GetLevelTextureFromId(int id)
        {
            if (id > m_levelTextures.Count - 1)
                throw new System.Exception("somethings not right. a texture that is not loaded previously si being called by id");

            return m_levelTextures[id];


        }

        public int LoadLevelTexture(string TexName, ContentManager Content, Texture2D Texture)
        {
            // If the lsit already sontains the key/image filename then it will also conatina the tex id.
            if (m_levelTexDictionary.ContainsKey(TexName) == true)
            {
                if (m_levelTextures[m_levelTexDictionary[TexName]] == null)
                {
                    m_levelTextures[m_levelTexDictionary[TexName]] = Content.Load<Texture2D>(TexName);
                }

                ++m_TexIDToUsageCount[m_levelTexDictionary[TexName]];
                return m_levelTexDictionary[TexName];
            }

            // Otherwise it doesn't and add the passed in texture.
            m_levelTextures.Add(Texture);

            m_levelTexDictionary.Add(TexName, (m_levelTextures.Count - 1));
            m_TexIDToUsageCount.Add((m_levelTextures.Count - 1), 1);

            return (m_levelTextures.Count - 1);
        }


        // Free the level textures
        public void FreeLevelTextureMemory()
        {
            m_levelTextures.Clear();
            m_levelTexDictionary.Clear();
            m_TexIDToUsageCount.Clear();

            m_forwardRenderLight = new SDirectionLight();
        }

        // Bounds should be drawn with a push of the button
        public void DrawBounds(CCamera Camera, CEntityOBB[] OBB)
        {
            for (int j = 0; j < OBB.Length; ++j)
            {
                if (OBB[j].m_wrldOBB.Length != 8)
                {
                    continue;
                }
                VertexPositionColor[] temp_vertices = new VertexPositionColor[8];

                temp_vertices[0] = new VertexPositionColor(OBB[j].m_wrldOBB[1], Color.Red);
                temp_vertices[1] = new VertexPositionColor(OBB[j].m_wrldOBB[0], Color.Red);
                temp_vertices[2] = new VertexPositionColor(OBB[j].m_wrldOBB[2], Color.Red);
                temp_vertices[3] = new VertexPositionColor(OBB[j].m_wrldOBB[3], Color.Red);
                temp_vertices[4] = new VertexPositionColor(OBB[j].m_wrldOBB[5], Color.Red);
                temp_vertices[5] = new VertexPositionColor(OBB[j].m_wrldOBB[4], Color.Red);
                temp_vertices[6] = new VertexPositionColor(OBB[j].m_wrldOBB[6], Color.Red);
                temp_vertices[7] = new VertexPositionColor(OBB[j].m_wrldOBB[7], Color.Red);

                int[] index = {
			    0, 1, 0, 2, 1, 3, 2, 3,
			    4, 5, 4, 6, 5, 7, 6, 7,
			    0, 4, 1, 5, 2, 6, 3, 7
		        };

                m_graphics.VertexDeclaration = m_boundVD;

                m_effectBound.CurrentTechnique = m_effectBound.Techniques["BoundingBox"];
                m_effectBound.Begin(SaveStateMode.SaveState);

                m_effectBound.Parameters["g_WVP"].SetValue(Camera.GetView() * Camera.GetProjection());

                // Render the OBB
                m_effectBound.CurrentTechnique.Passes[0].Begin();
                m_graphics.DrawUserIndexedPrimitives<VertexPositionColor>(PrimitiveType.LineList, temp_vertices, 0, 8, index, 0, 12);
                m_effectBound.CurrentTechnique.Passes[0].End();

                m_effectBound.End();

                DrawBounds(Camera, OBB[j].m_child);
            }
        }

        public void StoreBounds(CEntityOBB[] Bounds)
        {
            m_listOfBounds.Add(Bounds);
        }

        public void RenderStoredBounds(CCamera Camera)
        {
            for (int i = 0; i < m_listOfBounds.Count; ++i)
            {
                DrawBounds(Camera, m_listOfBounds[i]);
            }

            m_listOfBounds.Clear();
        }

        List<Vector3> m_lineList;
        List<Color> m_lineListColour;

        public void ClearLineList()
        {
            m_lineList.Clear();
            m_lineListColour.Clear();
        }

        public void AddToLineList(Vector3 A, Vector3 B, Color LineColour)
        {
            m_lineList.Add(A);
            m_lineList.Add(B);
            
            m_lineListColour.Add(LineColour);
        }

        public void RemoveLinesWithThisColour(Color LineColour)
        {
            for (int i = 0; i < m_lineListColour.Count; ++i)
            {
                if (m_lineListColour[i] == LineColour)
                {
                    m_lineList.RemoveAt(i * 2);
                    m_lineList.RemoveAt(i * 2);
                    m_lineListColour.RemoveAt(i);

                    --i;
                }
            }
        }

        // Bounds should be drawn with a push of the button
        public void RenderDebugLine(CCamera Camera)
        {
            int count = 0;
            for( int j = 0; j < m_lineList.Count; j+=2 )
            {
                VertexPositionColor[] temp_vertices = new VertexPositionColor[2];

                temp_vertices[0] = new VertexPositionColor(m_lineList[j], m_lineListColour[count]);
                temp_vertices[1] = new VertexPositionColor(m_lineList[j + 1], m_lineListColour[count]);

                int[] index = {
			        0, 1
		            };

                m_graphics.VertexDeclaration = m_boundVD;

                m_effectBound.CurrentTechnique = m_effectBound.Techniques["BoundingBox"];
                m_effectBound.Begin(SaveStateMode.SaveState);

                m_effectBound.Parameters["g_WVP"].SetValue(Camera.GetView() * Camera.GetProjection());

                // Render the OBB
                m_effectBound.CurrentTechnique.Passes[0].Begin();
                m_graphics.DrawUserIndexedPrimitives<VertexPositionColor>(PrimitiveType.LineList, temp_vertices, 0, 2, index, 0, 1);
                m_effectBound.CurrentTechnique.Passes[0].End();

                m_effectBound.End();

                ++count;
            }
        }


        // Bounds should be drawn with a push of the button
        public void DrawPortalBounds(CCamera Camera, CEntityOBB[] Vertices, bool Deferred)
        {
            for (int j = 0; j < Vertices.Length; ++j)
            {
                VertexPositionColor[] temp_vertices = new VertexPositionColor[Vertices[j].m_wrldOBB.Length];

                for (int i = 0; i < Vertices[j].m_wrldOBB.Length; ++i)
                {
                    temp_vertices[i] = new VertexPositionColor(Vertices[j].m_wrldOBB[i], Color.Red);
                }

                int[] index = new int[Vertices[j].m_wrldOBB.Length * 2];
                index[0] = 0;
                for (int i = 0; i < index.Length - 1; ++i)
                {
                    if ((i + 1) == (index.Length - 1))
                    {
                        index[i + 1] = 0;
                    }
                    else if ((i + 1) % 2 == 0)
                    {
                        index[i + 1] = index[i];
                    }
                    else
                    {
                        if (i == 0)
                        {
                            index[i + 1] = 1;
                        }
                        else
                        {
                            index[i + 1] = index[i] + 1;
                        }
                    }
                }

                m_graphics.VertexDeclaration = m_boundVD;

                m_effectBound.CurrentTechnique = m_effectBound.Techniques["BoundingBox"];
                m_effectBound.Begin(SaveStateMode.SaveState);

                m_effectBound.Parameters["g_WVP"].SetValue(Camera.GetView() * Camera.GetProjection());

                int k = 0;
                if (Deferred == true)
                {
                    k = 1;
                }

                // Render the OBB
                m_effectBound.CurrentTechnique.Passes[k].Begin();
                m_graphics.DrawUserIndexedPrimitives<VertexPositionColor>(PrimitiveType.LineList, temp_vertices, 0, temp_vertices.Length, index, 0, temp_vertices.Length);
                m_effectBound.CurrentTechnique.Passes[k].End();

                m_effectBound.End();
            }
        }

        // Bounds should be drawn with a push of the button
        public void DrawNormal(CCamera Camera, Vector3 Normal, Vector3 Position, bool Deferred)
        {
            VertexPositionColor[] temp_vertices = new VertexPositionColor[2];

            temp_vertices[0] = new VertexPositionColor(Position, Color.Red);
            temp_vertices[1] = new VertexPositionColor(Position + Normal, Color.Red);

            int[] index = new int[2];
            index[0] = 0;
            index[1] = 1;

            m_graphics.VertexDeclaration = m_boundVD;

            m_effectBound.CurrentTechnique = m_effectBound.Techniques["BoundingBox"];
            m_effectBound.Begin(SaveStateMode.SaveState);

            m_effectBound.Parameters["g_WVP"].SetValue(Camera.GetView() * Camera.GetProjection());

            int k = 0;
            if (Deferred == true)
            {
                k = 1;
            }

            // Render the OBB
            m_effectBound.CurrentTechnique.Passes[k].Begin();
            m_graphics.DrawUserIndexedPrimitives<VertexPositionColor>(PrimitiveType.LineList, temp_vertices, 0, temp_vertices.Length, index, 0, 1);
            m_effectBound.CurrentTechnique.Passes[k].End();

            m_effectBound.End();
        }

        public void SetPrvPosRot(Vector3 PrvPos, Quaternion PrvRot)
        {
            m_prvCamPos = PrvPos;
            m_prvCamRot = PrvRot;
        }

        public void DrawFrustum(CCamera Camera)
        {
            Vector3[] m_view_frustum_corners;
            Camera.GetViewFrustumCorners(out m_view_frustum_corners);

            VertexPositionColor[] try1 = new VertexPositionColor[5];

            try1[0] = new VertexPositionColor(m_view_frustum_corners[0], Color.Red);
            try1[1] = new VertexPositionColor(m_view_frustum_corners[1], Color.Red);
            try1[2] = new VertexPositionColor(m_view_frustum_corners[2], Color.Red);
            try1[3] = new VertexPositionColor(m_view_frustum_corners[3], Color.Red);
            try1[4] = new VertexPositionColor(m_view_frustum_corners[4], Color.Red);

            int[] index1 = {
                0, 1, 0, 2, 1, 2,
                0, 3, 0, 4, 3, 4,
                3, 1, 4, 2
                };

            m_graphics.VertexDeclaration = m_boundVD;

            m_effectBound.CurrentTechnique = m_effectBound.Techniques["BoundingBox"];
            m_effectBound.Begin(SaveStateMode.SaveState);

            //// Render the view frustum
            //Vector3 dir = new Vector3(0, 0, 1);
            //dir = Vector3.Transform(dir, m_prvCamRot);
            //Vector3 up = new Vector3(0, 1, 0);

            //Matrix world_matrix = Matrix.CreateWorld(m_prvCamPos, dir, up);
            m_effectBound.Parameters["g_WVP"].SetValue( Camera.GetView() * Camera.GetProjection());

            m_effectBound.CurrentTechnique.Passes[0].Begin();
            m_graphics.DrawUserIndexedPrimitives<VertexPositionColor>(PrimitiveType.LineList, try1, 0, 5, index1, 0, 8);
            m_effectBound.CurrentTechnique.Passes[0].End();

            m_effectBound.End();
        }

        //// Render content pipeline models (.x and .fbx)
        //public void RenderWithDefaultShader(Model ModelData, CCamera Camera, CMaterial Mat, Matrix WorldMatrix)
        //{
        //    Matrix[] transforms = new Matrix[ModelData.Bones.Count];
        //    ModelData.CopyAbsoluteBoneTransformsTo(transforms);

        //    foreach (ModelMesh mesh in ModelData.Meshes)
        //    {
        //        foreach (BasicEffect effect in mesh.Effects)
        //        {
        //            effect.DirectionalLight0.Enabled = true;
        //            effect.DirectionalLight0.Direction = new Vector3(-0.3f, -0.3f, -0.3f);
        //            effect.DirectionalLight0.DiffuseColor = new Vector3(1, 1, 1);
        //            effect.DirectionalLight0.SpecularColor = new Vector3(1, 1, 1);
        //            effect.LightingEnabled = true;

        //            effect.View = Camera.GetView();
        //            effect.Projection = Camera.GetProjection();
        //            effect.World = transforms[mesh.ParentBone.Index] * WorldMatrix;
        //        }
        //        mesh.Draw();
        //    }
        //}

        //// Render content pipeline models (.x and .fbx)
        //public void Render(Model ModelData, CCamera Camera, CMaterial Mat, Matrix[] WorldMatrix)
        //{
        //    Matrix[] transforms = new Matrix[ModelData.Bones.Count];
        //    ModelData.CopyAbsoluteBoneTransformsTo(transforms);
        //    int count = 0;

        //    foreach (ModelMesh mesh in ModelData.Meshes)
        //    {
        //        foreach (BasicEffect effect in mesh.Effects)
        //        {
        //            effect.DirectionalLight0.Enabled = true;
        //            effect.DirectionalLight0.Direction = new Vector3(-0.3f, -0.3f, -0.3f);
        //            effect.DirectionalLight0.DiffuseColor = new Vector3(1, 1, 1);
        //            effect.DirectionalLight0.SpecularColor = new Vector3(1, 1, 1);
        //            effect.LightingEnabled = true;

        //            effect.View = Camera.GetView();
        //            effect.Projection = Camera.GetProjection();
        //            effect.World = transforms[mesh.ParentBone.Index] * WorldMatrix[count];
        //        }
        //        mesh.Draw();
        //        ++count;
        //    }
        //}
        public void AddDirLightToShadowList(SDirectionLight DirectionLight)
        {
            m_dirLightShadowList.Add(DirectionLight);
        }

        public void AddSpotLightToShadowList(SSpotLight SpotLight)
        {
            m_spotLightShadowList.Add(SpotLight);
        }

        public void AddPointLightToList(SPointLight PointLight)
        {
            m_pointLightList.Add(PointLight);
        }

        public void AddDirLightToList( SDirectionLight DirectionLight )
        {
            m_dirLightList.Add(DirectionLight);
        }

        public void AddSpotLightToList(SSpotLight SpotLight)
        {
            m_spotLightList.Add(SpotLight);
        }

        public Texture2D GetNormalTexture()
        {
            return m_rtNormal.GetTexture();
        }

        public Texture2D GetTranFinalTexture()
        {
            return m_rtMaterial.GetTexture();
        }

        public void BeginRender(CCamera Camera)
        {
            SetTextureFilter();

            foreach (Effect effect in m_effectPhong)
            {
                effect.Parameters["View"].SetValue(Camera.GetView());
                effect.Parameters["Projection"].SetValue(Camera.GetProjection());
                effect.Parameters["g_farDistance"].SetValue(Camera.GetFarClipDistance());
            }

            foreach (Effect effect in m_effectSkinned)
            {
                effect.Parameters["View"].SetValue(Camera.GetView());
                effect.Parameters["Projection"].SetValue(Camera.GetProjection());
                effect.Parameters["g_farDistance"].SetValue(Camera.GetFarClipDistance());
            }

            m_graphics.SetRenderTarget(0, m_rtDepth);
            m_graphics.SetRenderTarget(1, m_rtDiffuse);
            m_graphics.SetRenderTarget(2, m_rtNormal);
            m_graphics.SetRenderTarget(3, m_rtMaterial);
        }

        public void BeginRenderForTransparentEntities(/*CDirLightEntity DirLight, CCamera Camera*/)
        {
            //if (DirLight != null)
            //{
            //    BeginForwardRender(DirLight, Camera);
            //}
            //else
            //{
            //    BeginForwardRender(Camera);
            //}

            m_graphics.RenderState.AlphaBlendEnable = true;
            m_graphics.RenderState.AlphaBlendOperation = BlendFunction.Add;
            m_graphics.RenderState.SourceBlend = Blend.SourceAlpha;
            m_graphics.RenderState.DestinationBlend = Blend.InverseSourceAlpha;
            //use the same operation on the alpha channel
            m_graphics.RenderState.SeparateAlphaBlendEnabled = false;
            m_graphics.RenderState.AlphaFunction = CompareFunction.Always;
            m_graphics.RenderState.DepthBufferWriteEnable = false;
            m_graphics.RenderState.DepthBufferEnable = true;
        }

        public void EndRenderForTransparentEntities()
        {
            m_graphics.RenderState.AlphaBlendEnable = false;
            m_graphics.RenderState.DepthBufferWriteEnable = true;

            //EndForwardRender();
        }

        public void BeginForwardRenderingForWater(CDirLightEntity DirLight, CCamera Camera)
        {
            m_forwardRenderLight = DirLight.GetLightData();
            m_lightInForwardRendering = true;

            foreach (Effect effect in m_effectForwardPhong)
            {
                effect.Parameters["View"].SetValue(Camera.GetView());
                effect.Parameters["Projection"].SetValue(Camera.GetProjection());
                effect.Parameters["g_cameraPosition"].SetValue(Camera.GetPosition());
                effect.Parameters["g_lightDirection"].SetValue(m_forwardRenderLight.s_direction);
                effect.Parameters["g_lightColour"].SetValue(new Vector4(m_forwardRenderLight.s_colour, 1.0f));
                effect.Parameters["g_lightIntensity"].SetValue(m_forwardRenderLight.s_intensity);
                effect.Parameters["g_lightEnabled"].SetValue(true);
                effect.Parameters["g_farDistance"].SetValue(Camera.GetFarClipDistance());
                effect.CommitChanges();
            }

            foreach (Effect effect in m_effectForwardSkinned)
            {
                effect.Parameters["View"].SetValue(Camera.GetView());
                effect.Parameters["Projection"].SetValue(Camera.GetProjection());
                effect.Parameters["g_cameraPosition"].SetValue(Camera.GetPosition());
                effect.Parameters["g_lightDirection"].SetValue(m_forwardRenderLight.s_direction);
                effect.Parameters["g_lightColour"].SetValue(new Vector4(m_forwardRenderLight.s_colour, 1.0f));
                effect.Parameters["g_lightIntensity"].SetValue(m_forwardRenderLight.s_intensity);
                effect.Parameters["g_lightEnabled"].SetValue(true);
                effect.Parameters["g_farDistance"].SetValue(Camera.GetFarClipDistance());
                effect.CommitChanges();
            }
        }

        public void BeginForwardRender( CDirLightEntity DirLight, CCamera Camera )
        {
#if XBOX360
            m_graphics.SetRenderTarget(0, m_frwdRender);
            m_graphics.SetRenderTarget(1, m_frwdDepth);
#endif

            m_forwardRenderLight = DirLight.GetLightData();
            m_lightInForwardRendering = true;

            foreach (Effect effect in m_effectForwardPhong)
            {
                effect.Parameters["View"].SetValue(Camera.GetView());
                effect.Parameters["Projection"].SetValue(Camera.GetProjection());
                effect.Parameters["g_cameraPosition"].SetValue(Camera.GetPosition());
                effect.Parameters["g_lightDirection"].SetValue(m_forwardRenderLight.s_direction);
                effect.Parameters["g_lightColour"].SetValue(new Vector4(m_forwardRenderLight.s_colour, 1.0f));
                effect.Parameters["g_lightIntensity"].SetValue(m_forwardRenderLight.s_intensity);
                effect.Parameters["g_lightEnabled"].SetValue(true);
                effect.Parameters["g_farDistance"].SetValue(Camera.GetFarClipDistance());
                effect.CommitChanges();
            }

            foreach (Effect effect in m_effectForwardSkinned)
            {
                effect.Parameters["View"].SetValue(Camera.GetView());
                effect.Parameters["Projection"].SetValue(Camera.GetProjection());
                effect.Parameters["g_cameraPosition"].SetValue(Camera.GetPosition());
                effect.Parameters["g_lightDirection"].SetValue(m_forwardRenderLight.s_direction);
                effect.Parameters["g_lightColour"].SetValue(new Vector4(m_forwardRenderLight.s_colour, 1.0f));
                effect.Parameters["g_lightIntensity"].SetValue(m_forwardRenderLight.s_intensity);
                effect.Parameters["g_lightEnabled"].SetValue(true);
                effect.Parameters["g_farDistance"].SetValue(Camera.GetFarClipDistance());
                effect.CommitChanges();
            }
        }

        public void BeginForwardRender(CCamera Camera)
        {
#if XBOX360
            m_graphics.SetRenderTarget(0, m_frwdRender);
            m_graphics.SetRenderTarget(1, m_frwdDepth);
#endif

            m_lightInForwardRendering = false;

            foreach (Effect effect in m_effectForwardPhong)
            {
                effect.Parameters["View"].SetValue(Camera.GetView());
                effect.Parameters["Projection"].SetValue(Camera.GetProjection());
                effect.Parameters["g_cameraPosition"].SetValue(Camera.GetPosition());
                effect.Parameters["g_lightEnabled"].SetValue(false);
                effect.Parameters["g_farDistance"].SetValue(Camera.GetFarClipDistance());
            }

            foreach (Effect effect in m_effectForwardSkinned)
            {
                effect.Parameters["View"].SetValue(Camera.GetView());
                effect.Parameters["Projection"].SetValue(Camera.GetProjection());
                effect.Parameters["g_cameraPosition"].SetValue(Camera.GetPosition());
                effect.Parameters["g_lightEnabled"].SetValue(false);
                effect.Parameters["g_farDistance"].SetValue(Camera.GetFarClipDistance());
            }
        }

        public void EndForwardRender(GameTime DT)
        {
            m_dt = DT;

#if XBOX360
            m_graphics.SetRenderTarget(0, m_rtDiffuse);
            m_graphics.SetRenderTarget(1, null);
            DisableDepthTest();


            m_combineOpaqueAndTranslucent.Parameters["Texture0"].SetValue(m_rtSpareFS1.GetTexture());
            m_combineOpaqueAndTranslucent.Parameters["Texture1"].SetValue(m_rtDepth.GetTexture());
            m_combineOpaqueAndTranslucent.Parameters["Texture2"].SetValue(m_frwdRender.GetTexture());
            m_combineOpaqueAndTranslucent.Parameters["Texture3"].SetValue(m_frwdDepth.GetTexture());
            m_combineOpaqueAndTranslucent.Parameters["g_halfPixel"].SetValue(m_halfPixel2);

            m_combineOpaqueAndTranslucent.Begin();
            m_combineOpaqueAndTranslucent.Techniques[0].Passes[0].Begin();

            m_graphics.VertexDeclaration = m_vertexDecl;

            m_graphics.DrawUserIndexedPrimitives<VertexPositionTextureIndex>
                (PrimitiveType.TriangleList, m_vertices, 0, 4, m_indices, 0, 2);

            m_combineOpaqueAndTranslucent.Techniques[0].Passes[0].End();
            m_combineOpaqueAndTranslucent.End();


            // Copy
            m_graphics.SetRenderTarget(0, null);

            m_copy.Parameters["Texture0"].SetValue(m_rtDiffuse.GetTexture());
            m_copy.Parameters["g_halfPixel"].SetValue(m_halfPixel2);

            m_copy.Begin();
            m_copy.Techniques[0].Passes[1].Begin();

            m_graphics.VertexDeclaration = m_vertexDecl;

            m_graphics.DrawUserIndexedPrimitives<VertexPositionTextureIndex>
                (PrimitiveType.TriangleList, m_vertices, 0, 4, m_indices, 0, 2);

            m_copy.Techniques[0].Passes[1].End();
            m_copy.End();


            EnableDepthTest();
#endif
            m_lightInForwardRendering = false;
        }

        public void TurnAlphaBlendingOff()
        {
            m_graphics.RenderState.AlphaBlendEnable = false;
        }

#if WINDOWS
        public void SaveNormalRenderDSB()
        {
            m_backupedNormalDSB = m_graphics.DepthStencilBuffer;
            m_graphics.DepthStencilBuffer = null;
        }

        public void RestoreNormalRenderDSB()
        {
            m_graphics.DepthStencilBuffer = m_backupedNormalDSB;
            m_backupedNormalDSB = null;
        }
#endif

        public void BeginRenderForShadow()
        {
#if EDITOR
            if (m_shadowsActive == true)
#endif
            {
                m_originalDSB = m_graphics.DepthStencilBuffer;
                m_graphics.DepthStencilBuffer = m_dsbShadow;
            }
        }

        public void SetRTForShadow(int RTIndex, CCamera Camera)
        {
#if EDITOR
            if (m_shadowsActive == false)
            {
                return;
            }
#endif
            foreach (Effect effect in m_effectPhongShadow)
            {
                effect.Parameters["View"].SetValue(Camera.GetView());
                effect.Parameters["Projection"].SetValue(Camera.GetProjection());
                effect.Parameters["g_farDistance"].SetValue(Camera.GetFarClipDistance());
            }

            m_graphics.SetRenderTarget(0, m_rtShadow[RTIndex]);
            m_graphics.SetRenderTarget(1, null);
            m_graphics.SetRenderTarget(2, null);
            m_graphics.SetRenderTarget(3, null);

            m_graphics.Clear(Color.TransparentBlack);
        }

        // Setting the buffer back to what it was before shadow calculations
        public void EndRenderForShadow()
        {
#if EDITOR
            if (m_shadowsActive == true)
#endif
            {
                m_graphics.DepthStencilBuffer = m_originalDSB;
            }
        }

        public void EndRender(CCamera Camera, BoundingBox GIBB, CCamera RSMCamera, CDirLightEntity LightDir/*Vector3 LightDir, bool NoPP*/)
        {
            // Resolve the render targets (GBuffer)
            m_graphics.SetRenderTarget(0, null);
            m_graphics.SetRenderTarget(1, null);
            m_graphics.SetRenderTarget(2, null);
            m_graphics.SetRenderTarget(3, null);

            if (m_SSAOActive == true)
            {
                // Screen space ambient occlusion
                SSAO(Camera);
            }

            //use additive blending, and make sure the blending factors are as we need them
            m_graphics.RenderState.AlphaBlendOperation = BlendFunction.Add;
            m_graphics.RenderState.SourceBlend = Blend.One;
            m_graphics.RenderState.DestinationBlend = Blend.One;
            //use the same operation on the alpha channel
            m_graphics.RenderState.SeparateAlphaBlendEnabled = false;
            m_graphics.RenderState.AlphaFunction = CompareFunction.Always;
            //m_graphics.RenderState.DepthBufferEnable = false;

            // Enable alpha
            m_graphics.RenderState.AlphaBlendEnable = true;

            // Render shadow
            if (m_shadowsActive == true)
            {
                RenderShadow(Camera);
            }

            // Set render target for final light pass - Opaque objects only
            m_graphics.SetRenderTarget(0, m_rtAllLights);
            m_graphics.Clear(Color.TransparentBlack);
            // Lighting the scene and rendering into m_rtAllLights - Opaque objects only
            LightTheScene(Camera, m_rtDiffuse.GetTexture(), m_rtNormal.GetTexture(), m_rtDepth.GetTexture(), m_rtMaterial.GetTexture());

            // Disable alpha
            m_graphics.RenderState.AlphaBlendEnable = false;

            //if (NoPP == false)
            //{
                m_graphics.SetRenderTarget(0, m_rtSpareFS1);
            //}
            //else
            //{
            //    m_graphics.SetRenderTarget(0, null);
            //}

            FinalDeferred(m_rtDiffuse.GetTexture(), m_rtMaterial.GetTexture(), m_rtAllLights.GetTexture(), m_rtNormal.GetTexture());

            // Reseting the list of lights
            m_spotLightShadowList.Clear();
            m_dirLightShadowList.Clear();

            m_pointLightList.Clear();
            m_dirLightList.Clear();
            m_spotLightList.Clear();
        }

        protected void FinalDeferred( Texture2D Diffuse, Texture2D Material, Texture2D Light, Texture2D Normal )
        {
            //set the effect parameters
            m_effectFinalDeferred.Parameters["DiffuseTexture"].SetValue(Diffuse);
            m_effectFinalDeferred.Parameters["MaterialTexture"].SetValue(Material);
            m_effectFinalDeferred.Parameters["LightTexture"].SetValue(Light);
            m_effectFinalDeferred.Parameters["NormalTexture"].SetValue(Normal);

            m_effectFinalDeferred.Parameters["g_lightsActive"].SetValue(m_lightsActive);

            if (m_shadowsActive == true)
            {
                m_effectFinalDeferred.Parameters["ShadowTexture"].SetValue(m_rtFinalShadow.GetTexture());
            }
            if (m_SSAOActive == true)
            {
                m_effectFinalDeferred.Parameters["SSAOTexture"].SetValue(m_rtSSAO.GetTexture());
            }

            m_effectFinalDeferred.Parameters["g_halfPixel"].SetValue(m_halfPixel2);

            m_effectFinalDeferred.Begin();
            m_effectFinalDeferred.Techniques[0].Passes[0].Begin();

            m_graphics.VertexDeclaration = m_vertexDecl;

            m_graphics.DrawUserIndexedPrimitives<VertexPositionTextureIndex>
                (PrimitiveType.TriangleList, m_vertices, 0, 4, m_indices, 0, 2);

            m_effectFinalDeferred.Techniques[0].Passes[0].End();
            m_effectFinalDeferred.End();
        }

        /// <summary>
        /// This is only needed if NoPP was set to false. <- This is causing an issue on the X360 so the engine will always assume that NoPP is set to off
        /// </summary>
        public virtual void SwapBuffer()
        {
            m_graphics.SetRenderTarget(0, null);

            m_copy.Parameters["Texture0"].SetValue(m_rtSpareFS1.GetTexture());
            m_copy.Parameters["g_halfPixel"].SetValue(m_halfPixel2);

            m_copy.Begin();
            m_copy.Techniques[0].Passes[1].Begin();

            m_graphics.VertexDeclaration = m_vertexDecl;

            m_graphics.DrawUserIndexedPrimitives<VertexPositionTextureIndex>
                (PrimitiveType.TriangleList, m_vertices, 0, 4, m_indices, 0, 2);

            m_copy.Techniques[0].Passes[1].End();
            m_copy.End();
        }

        protected void RenderShadow(CCamera Camera)
        {
            m_graphics.SetRenderTarget(0, m_rtFinalShadow);
            ClearScreen(Color.TransparentBlack);

            foreach (SSpotLight spot in m_spotLightShadowList)
            {
                FinalShadowPass(Camera, spot.s_shadowRTIndex, spot.s_lightCamera.GetViewProjection(), spot.s_lightCamera.GetFarClipDistance(), m_rtDepth,
                    0.001f, spot.s_shadowTerm, spot.s_lightCamera.GetNearClipDistance(), 
                    spot.s_lightCamera.GetFarClipDistance());
            }
            foreach (SDirectionLight dir in m_dirLightShadowList)
            {
                // The directional light amy have more than one texture for cascaded shadow mapping.
                for (int i = 0; i < dir.s_shadowRTIndex.Length; ++i)
                {
                    FinalShadowPass(Camera, dir.s_shadowRTIndex[i], dir.s_lightCamera[i].GetViewProjection(), dir.s_lightCamera[i].GetFarClipDistance(), m_rtDepth,
                        0.005f, dir.s_shadowTerm, dir.s_nearFar[i], dir.s_nearFar[i + 1]);
                }
            }

            if (m_blurShadows == true)
            {
                // Nearly double the FPS when this is turned off!
                Blur(ref m_rtFinalShadow);
            }
        }

        protected void FinalShadowPass(CCamera Camera, int RTIndex, Matrix LightVP, float LightRange, RenderTarget2D Depth,
            float Bias, float ShadowTerm, float Near, float Far)
        {
            Texture2D shadow = m_rtShadow[RTIndex].GetTexture();
            m_effectShadow.Parameters["ShadowTexture"].SetValue(shadow);
            m_effectShadow.Parameters["DepthTexture"].SetValue(Depth.GetTexture());

            m_effectShadow.Parameters["g_invCamera"].SetValue(Camera.GetInvView());
            m_effectShadow.Parameters["g_halfPixel"].SetValue(new Vector2(0.5f / Depth.Width, 0.5f / Depth.Height));
            m_effectShadow.Parameters["g_shadowMapSize"].SetValue(new Vector2(shadow.Width, shadow.Height));
            m_effectShadow.Parameters["g_lightVPMat"].SetValue(LightVP);
            m_effectShadow.Parameters["g_lightRange"].SetValue(LightRange);
            m_effectShadow.Parameters["g_bias"].SetValue(Bias);
            m_effectShadow.Parameters["g_shadowTerm"].SetValue(ShadowTerm);
            m_effectShadow.Parameters["g_near"].SetValue(Near);
            m_effectShadow.Parameters["g_far"].SetValue(Far);

            // Creating the frustum corners
            Vector3[] farFrustumCornersVS;
            Camera.GetFarFrustumCornersVS(out farFrustumCornersVS);
            m_effectShadow.Parameters["g_corners"].SetValue(farFrustumCornersVS);

            m_effectShadow.Begin();
            m_effectShadow.Techniques[0].Passes[0].Begin();

            m_graphics.VertexDeclaration = m_vertexDecl;

            m_graphics.DrawUserIndexedPrimitives<VertexPositionTextureIndex>
                (PrimitiveType.TriangleList, m_vertices, 0, 4, m_indices, 0, 2);

            m_effectShadow.Techniques[0].Passes[0].End();
            m_effectShadow.End();
        }

        protected void AddShadowIntoDiffuse(RenderTarget2D FrmShadow, RenderTarget2D Diffuse)
        {
            m_effectShadow.Parameters["FrmShadowTexture"].SetValue(FrmShadow.GetTexture());
            m_effectShadow.Parameters["DiffuseTexture"].SetValue(Diffuse.GetTexture());

            m_effectShadow.Parameters["g_halfPixel"].SetValue(new Vector2(0.5f / Diffuse.Width, 0.5f / Diffuse.Height));

            m_effectShadow.Begin();
            m_effectShadow.Techniques[0].Passes[1].Begin();

            m_graphics.VertexDeclaration = m_vertexDecl;

            m_graphics.DrawUserIndexedPrimitives<VertexPositionTextureIndex>
                (PrimitiveType.TriangleList, m_vertices, 0, 4, m_indices, 0, 2);

            m_effectShadow.Techniques[0].Passes[1].End();
            m_effectShadow.End();
        }

        protected void SSAO( CCamera Camera )
        {
            m_graphics.SetRenderTarget(0, m_rtSSAO);
            //ClearScreen(Color.TransparentBlack);

            //set the effect parameters - SSAO2.fx
            m_ssao.Parameters["g_iProj"].SetValue(Camera.GetInvProjection());
            m_ssao.Parameters["g_halfPixel"].SetValue(m_halfPixel);
            m_ssao.Parameters["g_resolution"].SetValue(new Vector2(m_rtSpare.Width, m_rtSpare.Height));

            // Creating the frustum corners
            //Vector3[] farFrustumCornersVS;
            //Camera.GetFarFrustumCornersVS(out farFrustumCornersVS);
            //m_ssao.Parameters["g_corners"].SetValue(farFrustumCornersVS);

            //m_ssao.Parameters["RandomVector"].SetValue(m_randomVectors);

            m_ssao.Parameters["DepthTexture"].SetValue(m_rtDepth.GetTexture());
            m_ssao.Parameters["NormalTexture"].SetValue(m_rtNormal.GetTexture());
            m_ssao.Parameters["RNTexture"].SetValue(m_rnTexture);
            m_ssao.Parameters["DiffuseTexture"].SetValue(m_rtDiffuse.GetTexture());

            m_ssao.Begin();
            m_ssao.Techniques[0].Passes[0].Begin();

            m_graphics.VertexDeclaration = m_vertexDecl;

            m_graphics.DrawUserIndexedPrimitives<VertexPositionTextureIndex>
                (PrimitiveType.TriangleList, m_vertices, 0, 4, m_indices, 0, 2);

            m_ssao.Techniques[0].Passes[0].End();
            m_ssao.End();

            if (m_blurSSAO == true)
            {
                // Better FPS without this blur.
                Blur(ref m_rtSSAO);
            }
        }

        protected void Blur( ref RenderTarget2D CopyFrm )
        {
            // Blur 1
            m_graphics.SetRenderTarget(0, m_rtSpare);
            ClearScreen(Color.TransparentBlack);

            //set the effect parameters
            m_blur.Parameters["SSAOTexture"].SetValue(CopyFrm.GetTexture());
            m_blur.Parameters["g_resolution0To1"].SetValue(new Vector2(1.0f / (m_graphics.PresentationParameters.BackBufferWidth / 2), 1.0f / (m_graphics.PresentationParameters.BackBufferHeight / 2)));
            m_blur.Parameters["g_halfPixel"].SetValue(m_halfPixel);

            m_blur.Begin();
            m_blur.Techniques[0].Passes[0].Begin();

            m_graphics.VertexDeclaration = m_vertexDecl;

            m_graphics.DrawUserIndexedPrimitives<VertexPositionTextureIndex>
                (PrimitiveType.TriangleList, m_vertices, 0, 4, m_indices, 0, 2);

            m_blur.Techniques[0].Passes[0].End();
            m_blur.End();

            //m_graphics.SetRenderTarget(0, null);

            // Blur 2
            m_graphics.SetRenderTarget(0, CopyFrm);
            ClearScreen(Color.TransparentBlack);

            //set the effect parameters
            m_blur.Parameters["SSAOTexture"].SetValue(m_rtSpare.GetTexture());
            m_blur.Parameters["g_resolution0To1"].SetValue(new Vector2(1.0f / (m_graphics.PresentationParameters.BackBufferWidth / 2), 1.0f / (m_graphics.PresentationParameters.BackBufferHeight / 2)));
            m_blur.Parameters["g_halfPixel"].SetValue(m_halfPixel);

            m_blur.Begin();
            m_blur.Techniques[0].Passes[0].Begin();

            m_graphics.VertexDeclaration = m_vertexDecl;

            m_graphics.DrawUserIndexedPrimitives<VertexPositionTextureIndex>
                (PrimitiveType.TriangleList, m_vertices, 0, 4, m_indices, 0, 2);

            m_blur.Techniques[0].Passes[0].End();
            m_blur.End();

            m_graphics.SetRenderTarget(0, null);
        }

        protected void LightTheScene(CCamera Camera,
            Texture2D Diffuse, Texture2D Normal, Texture2D Depth, Texture2D Material)
        {
            // CODE TO RENDER THE LIGHTS
            // Setting up the shared directional light rendering parameters.
            m_effectDeferredDirLight.Begin();
            m_effectDeferredDirLight.Techniques[0].Passes[0].Begin();
            {
                // Creating the frustum corners
                Vector3[] farFrustumCornersVS;
                Camera.GetFarFrustumCornersVS(out farFrustumCornersVS);
                m_effectDeferredDirLight.Parameters["g_corners"].SetValue(farFrustumCornersVS);

                m_effectDeferredDirLight.Parameters["DiffuseTexture"].SetValue(Diffuse);
                m_effectDeferredDirLight.Parameters["NormalTexture"].SetValue(Normal);
                m_effectDeferredDirLight.Parameters["DepthTexture"].SetValue(Depth);
                m_effectDeferredDirLight.Parameters["MaterialTexture"].SetValue(Material);
                m_effectDeferredDirLight.Parameters["g_halfPixel"].SetValue(m_halfPixel);
                m_effectDeferredDirLight.Parameters["g_view"].SetValue(Camera.GetView());
                m_effectDeferredDirLight.Parameters["g_world"].SetValue(Camera.GetRotationMatrix() * Matrix.CreateTranslation(Camera.GetPosition()));

                m_graphics.VertexDeclaration = m_vertexDecl;

                foreach (SDirectionLight dir in m_dirLightList)
                {
                    DrawDirLight(dir);
                }
            }
            m_effectDeferredDirLight.Techniques[0].Passes[0].End();
            m_effectDeferredDirLight.End();


            // Setting up the shared point light rendering parameters.
            m_effectDeferredPtLight.Begin();
            m_effectDeferredPtLight.Techniques[0].Passes[0].Begin();
            {
                m_effectDeferredPtLight.Parameters["DiffuseTexture"].SetValue(Diffuse);
                m_effectDeferredPtLight.Parameters["NormalTexture"].SetValue(Normal);
                m_effectDeferredPtLight.Parameters["DepthTexture"].SetValue(Depth);
                m_effectDeferredPtLight.Parameters["MaterialTexture"].SetValue(Material);
                m_effectDeferredPtLight.Parameters["g_halfPixel"].SetValue(m_halfPixel);
                m_effectDeferredPtLight.Parameters["View"].SetValue(Camera.GetView());
                m_effectDeferredPtLight.Parameters["Projection"].SetValue(Camera.GetProjection());

                ModelMesh mesh = m_pointLightSphere.Meshes[0];
                ModelMeshPart mesh_part = mesh.MeshParts[0];

                m_graphics.Vertices[0].SetSource(mesh.VertexBuffer, mesh_part.StreamOffset, mesh_part.VertexStride);
                m_graphics.Indices = mesh.IndexBuffer;
                m_graphics.VertexDeclaration = mesh_part.VertexDeclaration;

                DetermineCCWOrCWRenderingForPointLights(Camera);
            }
            m_effectDeferredPtLight.Techniques[0].Passes[0].End();
            m_effectDeferredPtLight.End();


            // Setting up the shared spot light rendering parameters.
            m_effectDeferredSpotLight.Begin();
            m_effectDeferredSpotLight.Techniques[0].Passes[0].Begin();
            {
                m_effectDeferredSpotLight.Parameters["DiffuseTexture"].SetValue(Diffuse);
                m_effectDeferredSpotLight.Parameters["NormalTexture"].SetValue(Normal);
                m_effectDeferredSpotLight.Parameters["DepthTexture"].SetValue(Depth);
                m_effectDeferredSpotLight.Parameters["MaterialTexture"].SetValue(Material);
                m_effectDeferredSpotLight.Parameters["g_halfPixel"].SetValue(m_halfPixel);
                m_effectDeferredSpotLight.Parameters["View"].SetValue(Camera.GetView());
                m_effectDeferredSpotLight.Parameters["Projection"].SetValue(Camera.GetProjection());

                ModelMesh mesh = m_spotLightCone.Meshes[0];
                ModelMeshPart mesh_part = m_spotLightCone.Meshes[0].MeshParts[0];
                m_graphics.VertexDeclaration = mesh_part.VertexDeclaration;
                m_graphics.Vertices[0].SetSource(mesh.VertexBuffer, mesh_part.StreamOffset, mesh_part.VertexStride);
                m_graphics.Indices = mesh.IndexBuffer;

                DetermineCCWOrCWRenderingForSpotLights(Camera);
            }
            m_effectDeferredSpotLight.Techniques[0].Passes[0].End();
            m_effectDeferredSpotLight.End();
        }

        protected void DetermineCCWOrCWRenderingForPointLights(CCamera Camera)
        {
            List<SPointLight> new_list_inside = new List<SPointLight>();
            List<SPointLight> new_list_outside = new List<SPointLight>();
            foreach (SPointLight point in m_pointLightList)
            {
                //calculate the distance between the camera and light center
                float cam_to_center = Vector3.DistanceSquared(Camera.GetPosition(), point.s_position);

                //if we are inside the light volume, draw the sphere's inside face
                float radius = point.s_radius * 1.5f;
                if (cam_to_center < (radius * radius))// This 1.2 is required to prevent any flickering occuring when the cull mode is changed.
                {
                    new_list_inside.Add(point);
                }
                else
                {
                    new_list_outside.Add(point);
                }
            }

            if (new_list_inside.Count > 0)
            {
                // Set to CW cullmode and then render the point lights
                m_graphics.RenderState.CullMode = CullMode.CullClockwiseFace;
                foreach (SPointLight point in new_list_inside)
                {
                    DrawPointLight(Camera, point);
                }

                // Set back to CCW cullmode.
                m_graphics.RenderState.CullMode = CullMode.CullCounterClockwiseFace;
            }

            foreach (SPointLight point in new_list_outside)
            {
                DrawPointLight(Camera, point);
            }
        }

        protected void DetermineCCWOrCWRenderingForSpotLights(CCamera Camera)
        {
            List<SSpotLight> new_list_inside = new List<SSpotLight>();
            List<SSpotLight> new_list_outside = new List<SSpotLight>();
            foreach (SSpotLight spot in m_spotLightList)
            {
                //calculate the distance between the camera and light center
                float cam_to_center = Vector3.DistanceSquared(Camera.GetPosition(), spot.s_position);

                Vector3 unit_light_dir = CToolbox.UnitVector(spot.s_position - spot.s_interest);

                float angle_dir_lv = CToolbox.DotProduct(unit_light_dir, CToolbox.UnitVector(spot.s_position - (Camera.GetPosition() - unit_light_dir)));
                float cone_angle = (float)Math.Cos(CToolbox.FindRadian(spot.s_coneAngle * 1.3f)); // This 1.3 is required to prevent any flickering occuring when the cull mode is changed.

                float side1 = CToolbox.FindSide1InTriangleWithSide2AngleA(spot.s_range, spot.s_coneAngle / 2);

                //if we are inside the light volume, draw the sphere's inside face
                float range = spot.s_range + side1;
                if (angle_dir_lv > cone_angle && cam_to_center < (range * range))
                {
                    new_list_inside.Add(spot);
                }
                else
                {
                    new_list_outside.Add(spot);
                }
            }

            if (new_list_inside.Count > 0)
            {
                // Set to CW cullmode and then render the point lights
                m_graphics.RenderState.CullMode = CullMode.CullClockwiseFace;
                foreach (SSpotLight point in new_list_inside)
                {
                    DrawSpotLight(Camera, point);
                }

                // Set back to CCW cullmode.
                m_graphics.RenderState.CullMode = CullMode.CullCounterClockwiseFace;
            }

            foreach (SSpotLight point in new_list_outside)
            {
                DrawSpotLight(Camera, point);
            }
        }

        protected void DrawDirLight(SDirectionLight DirLight)
        {
            m_effectDeferredDirLight.Parameters["g_lightDir"].SetValue(CToolbox.UnitVector(DirLight.s_direction));
            m_effectDeferredDirLight.Parameters["g_lightCol"].SetValue(DirLight.s_colour);
            m_effectDeferredDirLight.Parameters["g_lightIntensity"].SetValue(DirLight.s_intensity);
            m_effectDeferredDirLight.Parameters["g_lightSpec"].SetValue(DirLight.s_specularIntensity);

            m_effectDeferredDirLight.CommitChanges();

            m_graphics.DrawUserIndexedPrimitives<VertexPositionTextureIndex>
                (PrimitiveType.TriangleList, m_vertices, 0, 4, m_indices, 0, 2);
        }

        protected void DrawPointLight(CCamera Camera, SPointLight PointLight)
        {
            m_effectDeferredPtLight.Parameters["g_lightPosition"].SetValue(PointLight.s_position);
            m_effectDeferredPtLight.Parameters["g_lightCol"].SetValue(PointLight.s_colour);
            m_effectDeferredPtLight.Parameters["g_lightRadius"].SetValue(PointLight.s_radius);
            m_effectDeferredPtLight.Parameters["g_lightIntensity"].SetValue(PointLight.s_intensity);
            m_effectDeferredPtLight.Parameters["g_lightSpec"].SetValue(PointLight.s_specularIntensity);

            Matrix sphereWorldMatrix = Matrix.CreateScale(PointLight.s_radius) * Matrix.CreateTranslation(PointLight.s_position);
            m_effectDeferredPtLight.Parameters["World"].SetValue(sphereWorldMatrix);

            m_effectDeferredPtLight.CommitChanges();

            ModelMeshPart mesh_part = m_pointLightSphere.Meshes[0].MeshParts[0];
            m_graphics.DrawIndexedPrimitives(PrimitiveType.TriangleList, mesh_part.BaseVertex, 0, mesh_part.NumVertices, mesh_part.StartIndex, mesh_part.PrimitiveCount);
        }

        protected void DrawSpotLight(CCamera Camera, SSpotLight SpotLight)
        {
            m_effectDeferredSpotLight.Parameters["g_lightPosition"].SetValue(SpotLight.s_position);
            m_effectDeferredSpotLight.Parameters["g_lightCol"].SetValue(SpotLight.s_colour);
            m_effectDeferredSpotLight.Parameters["g_lightConeAngle"].SetValue(SpotLight.s_coneAngle);
            m_effectDeferredSpotLight.Parameters["g_lightIntensity"].SetValue(SpotLight.s_intensity);
            m_effectDeferredSpotLight.Parameters["g_lightRange"].SetValue(SpotLight.s_range);
            Vector3 unit_light_dir = CToolbox.UnitVector(SpotLight.s_position - SpotLight.s_interest);
            m_effectDeferredSpotLight.Parameters["g_lightDirection"].SetValue(unit_light_dir);
            m_effectDeferredSpotLight.Parameters["g_lightSpec"].SetValue(SpotLight.s_specularIntensity);

            // Calculate the roation for the cone model
            Vector3 x = new Vector3(SpotLight.s_interest.X, SpotLight.s_interest.Y, SpotLight.s_position.Z);
            Vector3 down = new Vector3(SpotLight.s_position.X, SpotLight.s_interest.Y, SpotLight.s_position.Z);
            float x_angle = CToolbox.AngleBetweenTwoVectorR(down - SpotLight.s_position, x - SpotLight.s_position);
            if (SpotLight.s_interest.X < SpotLight.s_position.X)
            {
                x_angle *= -1.0f;
            }

            Vector3 z = new Vector3(SpotLight.s_position.X, SpotLight.s_interest.Y, SpotLight.s_interest.Z);
            float z_angle = CToolbox.AngleBetweenTwoVectorR(down - SpotLight.s_position, z - SpotLight.s_position);
            if (SpotLight.s_interest.Z > SpotLight.s_position.Z)
            {
                z_angle *= -1.0f;
            }

            float side1 = CToolbox.FindSide1InTriangleWithSide2AngleA(SpotLight.s_range, SpotLight.s_coneAngle / 2);

            Matrix sphereWorldMatrix = Matrix.CreateScale(side1 * 2.5f, SpotLight.s_range + side1, side1 * 2.5f) * Matrix.CreateFromYawPitchRoll(0, z_angle, x_angle) * Matrix.CreateTranslation(SpotLight.s_position);
            m_effectDeferredSpotLight.Parameters["World"].SetValue(sphereWorldMatrix);

            m_effectDeferredSpotLight.CommitChanges();

            ModelMeshPart mesh_part = m_spotLightCone.Meshes[0].MeshParts[0];
            m_graphics.DrawIndexedPrimitives(PrimitiveType.TriangleList, mesh_part.BaseVertex, 0, mesh_part.NumVertices, mesh_part.StartIndex, mesh_part.PrimitiveCount);
        }

        /// <summary>
        /// Sets the render state to render both faces of a mesh.
        /// </summary>
        public void RenderBothFaces()
        {
            m_graphics.RenderState.CullMode = CullMode.None;
        }

        /// <summary>
        /// Sets the render state to render the clockwise face of the mesh.
        /// </summary>
        public void RenderOneFace()
        {
            m_graphics.RenderState.CullMode = CullMode.CullCounterClockwiseFace;
        }

        /// <summary>
        /// Needs to be called right before any particle rendering is done.
        /// Only for rendering quads.
        /// </summary>
        public void BeginQuadParticleRender()
        {
            m_particleRenderer.BeginRender(m_particleModel);
        }

        /// <summary>
        /// Needs to be called right before any particle rendering is done.
        /// This is for rendering .x (non-quad) models.
        /// </summary>
        /// <param name="ModelMesh">The Model you wish to render.</param>
        public void BeginDotXParticleRender(Model ModelMesh)
        {
            m_normParticleRenderer.BeginRender(ModelMesh);
        }

        /// <summary>
        /// Needs to be called only when switching between particles which need animated textures and which don't.
        /// This is for rendering particles which are quads and have animated textures.
        /// </summary>
        /// <param name="Camera">The Current Camera</param>
        /// <param name="WidthOfOneFrame">The width of one frame in the texture. Must be a value betwee 0 and 1 (real frame width / real texture width).</param>
        public void SetUpQuadParticleEffect(CCamera Camera, float WidthOfOneFrame)
        {
            m_curParticleEffect = m_effectParticleRender[m_indexOffsetForSoftParticles + 2];

            m_curParticleEffect.Parameters["View"].SetValue(Camera.GetView());
            m_curParticleEffect.Parameters["Projection"].SetValue(Camera.GetProjection());
            m_curParticleEffect.Parameters["g_widthOfFrame"].SetValue(WidthOfOneFrame);
            m_curParticleEffect.Parameters["g_farDistance"].SetValue(Camera.GetFarClipDistance());
            m_curParticleEffect.CommitChanges();

            if (m_indexOffsetForSoftParticles == 3)
            {
                m_curParticleEffect.Parameters["DepthMapTexture"].SetValue(m_rtDepth.GetTexture());
            }

            m_particleRenderer.SetUpParticleEffect(m_curParticleEffect);
        }

        /// <summary>
        /// Needs to be called only when switching between particles which need textures and which don't.
        /// This is for rendering particles which are quads.
        /// </summary>
        /// <param name="EffectID">Effect Id. 0: for untextured particles 1: for textured particles</param>
        /// <param name="Camera">The Current Camera</param>
        public void SetUpQuadParticleEffect(int EffectID, CCamera Camera)
        {
            SetUpOtherParticleEffectCommon(EffectID, Camera);

            m_particleRenderer.SetUpParticleEffect(m_curParticleEffect);
        }

        /// <summary>
        /// Needs to be called only when switching between particles which need animated textures and which don't.
        /// This is for rendering particles which are NOT quads.
        /// </summary>
        /// <param name="Camera">The Current Camera</param>
        /// <param name="WidthOfOneFrame">The width of one frame in the texture. Must be a value betwee 0 and 1 (real frame width / real texture width).</param>
        public void SetUpDotXParticleEffect(CCamera Camera, float WidthOfOneFrame)
        {
            m_curParticleEffect = m_effectParticleRender[m_indexOffsetForSoftParticles + 2];

            m_curParticleEffect.Parameters["View"].SetValue(Camera.GetView());
            m_curParticleEffect.Parameters["Projection"].SetValue(Camera.GetProjection());
            m_curParticleEffect.Parameters["g_widthOfFrame"].SetValue(WidthOfOneFrame);
            m_curParticleEffect.Parameters["g_farDistance"].SetValue(Camera.GetFarClipDistance());
            m_curParticleEffect.CommitChanges();

            if (m_indexOffsetForSoftParticles == 3)
            {
                m_curParticleEffect.Parameters["DepthMapTexture"].SetValue(m_rtDepth.GetTexture());
            }

            m_particleRenderer.SetUpParticleEffect(m_curParticleEffect);
        }

        /// <summary>
        /// Needs to be called only when switching between particles which need textures and which don't.
        /// This is for rendering particles which are NOT quads.
        /// </summary>
        /// <param name="EffectID">Effect Id. 0: for untextured particles 1: for textured particles</param>
        /// <param name="Camera">The Current Camera</param>
        public void SetUpDotXParticleEffect(int EffectID, CCamera Camera)
        {
            SetUpOtherParticleEffectCommon(EffectID, Camera);

            m_normParticleRenderer.SetUpParticleEffect(m_curParticleEffect);
        }

        protected void SetUpOtherParticleEffectCommon(int EffectID, CCamera Camera)
        {
            m_curParticleEffect = m_effectParticleRender[m_indexOffsetForSoftParticles + EffectID];

            m_curParticleEffect.Parameters["View"].SetValue(Camera.GetView());
            m_curParticleEffect.Parameters["Projection"].SetValue(Camera.GetProjection());
            m_curParticleEffect.Parameters["g_farDistance"].SetValue(Camera.GetFarClipDistance());
            m_curParticleEffect.CommitChanges();

            if (m_indexOffsetForSoftParticles == 3)
            {
                m_curParticleEffect.Parameters["DepthMapTexture"].SetValue(m_rtDepth.GetTexture());
            }
        }

        /// <summary>
        /// Needs to be called only when switching between Quad particles which need textures,
        /// which don't need textures and ending the last particle render.
        /// </summary>
        public void EndQuadParticleRender()
        {
            m_particleRenderer.UnDoSetUpParticleEffect(m_curParticleEffect);
        }

        /// <summary>
        /// Needs to be called only when switching between DotX particles which need textures,
        /// which don't need textures and ending the last particle render.
        /// </summary>
        public void EndDotXParticleRender()
        {
            m_normParticleRenderer.UnDoSetUpParticleEffect(m_curParticleEffect);
        }

        /// <summary>
        /// Render Quad Particles without a texture.
        /// </summary>
        /// <param name="WorldMatrices">Array of World Matrices</param>
        /// <param name="TintColour">Array of Tints</param>
        /// <param name="MaxParticlesToRender">Maximum particles to render from the arrays</param>
        public void RenderQuadParticles(Matrix[] WorldMatrices, Vector4[] TintColour, int MaxParticlesToRender)
        {
            m_particleRenderer.Render(WorldMatrices, TintColour, MaxParticlesToRender, m_curParticleEffect);
        }

        /// <summary>
        /// Render Quad Particles with a texture.
        /// </summary>
        /// <param name="WorldMatrices">Array of World Matrices corresponding to each particle</param>
        /// <param name="TintColour">Array of Tints corresponding to each particle</param>
        /// <param name="TextureID">The ID of the texture to be rendered ontop of the Quad</param>
        /// <param name="MaxParticlesToRender">Maximum particles to render from the arrays</param>
        public void RenderQuadParticles(Matrix[] WorldMatrices, Vector4[] TintColour, int TextureID, int MaxParticlesToRender)
        {
            m_curParticleEffect.Parameters["DiffuseMapTexture"].SetValue(m_levelTextures[TextureID]);
            m_curParticleEffect.CommitChanges();
            m_particleRenderer.Render(WorldMatrices, TintColour, MaxParticlesToRender, m_curParticleEffect);
        }

        /// <summary>
        /// Render Quad Particles with an animated texture.
        /// </summary>
        /// <param name="WorldMatrices">Array of World Matrices corresponding to each particle</param>
        /// <param name="TintColour">Array of Tints corresponding to each particle</param>
        /// <param name="FrameNumbers">Array of frame numbers corresponding to each particle</param>
        /// <param name="TextureID">The ID of the texture to be rendered ontop of the Quad</param>
        /// <param name="MaxParticlesToRender">Maximum particles to render from the arrays</param>
        public void RenderQuadParticles(Matrix[] WorldMatrices, Vector4[] TintColour, int[] FrameNumbers, int TextureID, int MaxParticlesToRender)
        {
            m_curParticleEffect.Parameters["DiffuseMapTexture"].SetValue(m_levelTextures[TextureID]);
            m_curParticleEffect.CommitChanges();
            m_particleRenderer.Render(WorldMatrices, TintColour, FrameNumbers, MaxParticlesToRender, m_curParticleEffect);
        }

        /// <summary>
        /// // Render DotX Particles without a texture.
        /// </summary>
        /// <param name="WorldMatrices">Array of World Matrices corresponding to each particle</param>
        /// <param name="TintColour">Array of Tints corresponding to each particle</param>
        /// <param name="MaxParticlesToRender">Maximum particles to render from the arrays</param>
        public void RenderDotXParticles(Matrix[] WorldMatrices, Vector4[] TintColour, int MaxParticlesToRender)
        {
            m_normParticleRenderer.Render(WorldMatrices, TintColour, MaxParticlesToRender, m_curParticleEffect);
        }

        /// <summary>
        /// // Render DotX Particles with a texture.
        /// </summary>
        /// <param name="WorldMatrices">Array of World Matrices corresponding to each particle</param>
        /// <param name="TintColour">Array of Tints corresponding to each particle</param>
        /// <param name="TextureID">The ID of the texture to be rendered ontop of the Quad</param>
        /// <param name="MaxParticlesToRender">Maximum particles to render from the arrays</param>
        public void RenderDotXParticles(Matrix[] WorldMatrices, Vector4[] TintColour, int TextureID, int MaxParticlesToRender)
        {
            m_curParticleEffect.Parameters["DiffuseMapTexture"].SetValue(m_levelTextures[TextureID]);
            m_curParticleEffect.CommitChanges();
            m_normParticleRenderer.Render(WorldMatrices, TintColour, MaxParticlesToRender, m_curParticleEffect);
        }

        /// <summary>
        /// // Render DotX Particles with a texture.
        /// </summary>
        /// <param name="WorldMatrices">Array of World Matrices corresponding to each particle</param>
        /// <param name="TintColour">Array of Tints corresponding to each particle</param>
        /// <param name="FrameNumbers">Array of frame numbers corresponding to each particle</param>
        /// <param name="TextureID">The ID of the texture to be rendered ontop of the Quad</param>
        /// <param name="MaxParticlesToRender">Maximum particles to render from the arrays</param>
        public void RenderDotXParticles(Matrix[] WorldMatrices, Vector4[] TintColour, int[] FrameNumbers, int TextureID, int MaxParticlesToRender)
        {
            m_curParticleEffect.Parameters["DiffuseMapTexture"].SetValue(m_levelTextures[TextureID]);
            m_curParticleEffect.CommitChanges();
            m_normParticleRenderer.Render(WorldMatrices, TintColour, FrameNumbers, MaxParticlesToRender, m_curParticleEffect);
        }

        // Render content pipeline models (.x and .fbx)
        public void Render(Model ModelData, CCamera Camera, List<CMaterial> Material, Matrix WorldMatrix, Dictionary<int, List<CEntity.SPartAndMatNum>> MeshIDs)
        {
            // TODO: Instead of m_effectPhong[Material[count].m_effect], which means that it has to search for that particular effect every time i use this
            // line of code, define a new effect variable at the beginning of Render and then use that.
            // TODO: http://blogs.msdn.com/shawnhar/archive/2006/11/20/models-meshes-parts-and-bones.aspx <- Rendering optimisation.

            // Might need removing for optimisation, that will mean that all models need their transforms frozen.
            Matrix[] transforms = new Matrix[ModelData.Bones.Count];
            ModelData.CopyAbsoluteBoneTransformsTo(transforms);

            int cur_mat_id = 0;
            foreach (KeyValuePair<int, List<CEntity.SPartAndMatNum>> mesh_id in MeshIDs)
            {
                ModelMesh mesh = ModelData.Meshes[mesh_id.Key];

                Matrix new_world_matrix = transforms[mesh.ParentBone.Index] * WorldMatrix;

                for (int j = 0; j < mesh_id.Value.Count; ++j)
                {
                    cur_mat_id = mesh_id.Value[j].s_partMaterial;
                    ModelMeshPart part = mesh.MeshParts[mesh_id.Value[j].s_partNum];
                    
                    Effect cur_effect = m_effectPhong[Material[cur_mat_id].m_effect];

                    cur_effect.CurrentTechnique = cur_effect.Techniques["PhongTechnique"];

                    // Setting the vertex shader specific global variables
                    cur_effect.Parameters["World"].SetValue(new_world_matrix);
                    cur_effect.Parameters["TransInvWorldView"].SetValue(Matrix.Transpose(Matrix.Invert(new_world_matrix * Camera.GetView())));

                    // Setting the global material variables
                    cur_effect.Parameters["g_diffuse"].SetValue(Material[cur_mat_id].m_diffuse);
                    cur_effect.Parameters["g_ambient"].SetValue(Material[cur_mat_id].m_ambient);
                    cur_effect.Parameters["g_emissive"].SetValue(Material[cur_mat_id].m_emissive);
                    cur_effect.Parameters["g_specularColour"].SetValue(Material[cur_mat_id].m_specularColour);
                    cur_effect.Parameters["g_specularPower"].SetValue(Material[cur_mat_id].m_specularPower / 255.0f);

                    if (Material[cur_mat_id].m_textureID != -1)
                    {
#if EDITOR
                        if (m_levelTextures.Count - 1 >= Material[cur_mat_id].m_textureID)
                        {
                            cur_effect.Parameters["DiffuseMapTexture"].SetValue(m_levelTextures[Material[cur_mat_id].m_textureID]);
                        }
#else
                        cur_effect.Parameters["DiffuseMapTexture"].SetValue(m_levelTextures[Material[cur_mat_id].m_textureID]);
#endif
                        if (Material[cur_mat_id].m_normalID != -1)
                        {
                            cur_effect.Parameters["NormalMapTexture"].SetValue(m_levelTextures[Material[cur_mat_id].m_normalID]);
                        }
                        if (Material[cur_mat_id].m_specularID != -1)
                        {
                            cur_effect.Parameters["SpecularMapTexture"].SetValue(m_levelTextures[Material[cur_mat_id].m_specularID]);
                        }
                    }

                    cur_effect.Begin();

                    cur_effect.CurrentTechnique.Passes[0].Begin();
                    m_graphics.VertexDeclaration = part.VertexDeclaration;
                    m_graphics.Indices = mesh.IndexBuffer;
                    m_graphics.Vertices[0].SetSource(mesh.VertexBuffer, part.StreamOffset, part.VertexStride);
                    cur_effect.CurrentTechnique.Passes[0].End();

                    cur_effect.End();

                    m_graphics.DrawIndexedPrimitives(PrimitiveType.TriangleList, part.BaseVertex, 0, part.NumVertices, part.StartIndex, part.PrimitiveCount);

                    m_numNormalPolygonsRendered += part.PrimitiveCount;
                }
            }
        }

        // Render content pipeline models (.x and .fbx) but with scale
        public void Render(Model ModelData, CCamera Camera, List<CMaterial> Material, Matrix WorldMatrix, Dictionary<int, List<CEntity.SPartAndMatNum>> MeshIDs, Vector3 scale)
        {
            // TODO: Instead of m_effectPhong[Material[count].m_effect], which means that it has to search for that particular effect every time i use this
            // line of code, define a new effect variable at the beginning of Render and then use that.
            // TODO: http://blogs.msdn.com/shawnhar/archive/2006/11/20/models-meshes-parts-and-bones.aspx <- Rendering optimisation.

            // Might need removing for optimisation, that will mean that all models need their transforms frozen.
            Matrix[] transforms = new Matrix[ModelData.Bones.Count];
            ModelData.CopyAbsoluteBoneTransformsTo(transforms);

            int cur_mat_id = 0;
            foreach (KeyValuePair<int, List<CEntity.SPartAndMatNum>> mesh_id in MeshIDs)
            {
                ModelMesh mesh = ModelData.Meshes[mesh_id.Key];

                Matrix new_world_matrix = transforms[mesh.ParentBone.Index] * WorldMatrix;
                new_world_matrix.M14 = scale.X;
                new_world_matrix.M24 = scale.Y;
                new_world_matrix.M34 = scale.Z;


                for (int j = 0; j < mesh_id.Value.Count; ++j)
                {
                    cur_mat_id = mesh_id.Value[j].s_partMaterial;
                    ModelMeshPart part = mesh.MeshParts[mesh_id.Value[j].s_partNum];

                    Effect cur_effect = m_effectPhong[Material[cur_mat_id].m_effect];

                    cur_effect.CurrentTechnique = cur_effect.Techniques["PhongTechnique"];

                    // Setting the vertex shader specific global variables
                    cur_effect.Parameters["World"].SetValue(new_world_matrix);
                    cur_effect.Parameters["TransInvWorldView"].SetValue(Matrix.Transpose(Matrix.Invert(new_world_matrix * Camera.GetView())));

                    // Setting the global material variables
                    cur_effect.Parameters["g_diffuse"].SetValue(Material[cur_mat_id].m_diffuse);
                    cur_effect.Parameters["g_ambient"].SetValue(Material[cur_mat_id].m_ambient);
                    cur_effect.Parameters["g_emissive"].SetValue(Material[cur_mat_id].m_emissive);
                    cur_effect.Parameters["g_specularColour"].SetValue(Material[cur_mat_id].m_specularColour);
                    cur_effect.Parameters["g_specularPower"].SetValue(Material[cur_mat_id].m_specularPower / 255.0f);

                    if (Material[cur_mat_id].m_textureID != -1)
                    {
#if EDITOR
                        if (m_levelTextures.Count - 1 >= Material[cur_mat_id].m_textureID)
                        {
                            cur_effect.Parameters["DiffuseMapTexture"].SetValue(m_levelTextures[Material[cur_mat_id].m_textureID]);
                        }
#else
                        cur_effect.Parameters["DiffuseMapTexture"].SetValue(m_levelTextures[Material[cur_mat_id].m_textureID]);
#endif
                    }
                    if (Material[cur_mat_id].m_normalID != -1)
                    {
                        cur_effect.Parameters["NormalMapTexture"].SetValue(m_levelTextures[Material[cur_mat_id].m_normalID]);
                    }
                    if (Material[cur_mat_id].m_specularID != -1)
                    {
                        cur_effect.Parameters["SpecularMapTexture"].SetValue(m_levelTextures[Material[cur_mat_id].m_specularID]);
                    }

                    cur_effect.Begin();

                    cur_effect.CurrentTechnique.Passes[0].Begin();
                    m_graphics.VertexDeclaration = part.VertexDeclaration;
                    m_graphics.Indices = mesh.IndexBuffer;
                    m_graphics.Vertices[0].SetSource(mesh.VertexBuffer, part.StreamOffset, part.VertexStride);
                    cur_effect.CurrentTechnique.Passes[0].End();

                    cur_effect.End();

                    m_graphics.DrawIndexedPrimitives(PrimitiveType.TriangleList, part.BaseVertex, 0, part.NumVertices, part.StartIndex, part.PrimitiveCount);

                    m_numNormalPolygonsRendered += part.PrimitiveCount;
                }
            }
        }

        // Render content pipeline models (.x and .fbx)
        public void ForwardRender(Model ModelData, List<CMaterial> Material, Matrix WorldMatrix, Dictionary<int, List<CEntity.SPartAndMatNum>> MeshIDs)
        {
            // TODO: Instead of m_effectForwardPhong[Material[count].m_effect], which means that it has to search for that particular effect every time i use this
            // line of code, define a new effect variable at the beginning of Render and then use that.
            // TODO: http://blogs.msdn.com/shawnhar/archive/2006/11/20/models-meshes-parts-and-bones.aspx <- Rendering optimisation.

            // Might need removing for optimisation, that will mean that all models need their transforms frozen.
            Matrix[] transforms = new Matrix[ModelData.Bones.Count];
            
            ModelData.CopyAbsoluteBoneTransformsTo(transforms);

            int cur_mat_id = 0;
            foreach (KeyValuePair<int, List<CEntity.SPartAndMatNum>> mesh_id in MeshIDs)
            {
                ModelMesh mesh = ModelData.Meshes[mesh_id.Key];

                Matrix new_world_matrix = transforms[mesh.ParentBone.Index] * WorldMatrix;

                for (int j = 0; j < mesh_id.Value.Count; ++j)
                {
                    cur_mat_id = mesh_id.Value[j].s_partMaterial;
                    ModelMeshPart part = mesh.MeshParts[mesh_id.Value[j].s_partNum];
                    Effect cur_effect = m_effectForwardPhong[Material[cur_mat_id].m_effect];

                    cur_effect.CurrentTechnique = cur_effect.Techniques["PhongTechnique"];

                    // Setting the vertex shader specific global variables
                    cur_effect.Parameters["World"].SetValue(new_world_matrix);

                    // Setting the global material variables
                    cur_effect.Parameters["g_diffuse"].SetValue(Material[cur_mat_id].m_diffuse);
                    cur_effect.Parameters["g_ambient"].SetValue(Material[cur_mat_id].m_ambient);
                    cur_effect.Parameters["g_emissive"].SetValue(Material[cur_mat_id].m_emissive);
                    cur_effect.Parameters["g_specularColour"].SetValue(Material[cur_mat_id].m_specularColour);
                    cur_effect.Parameters["g_specularPower"].SetValue(Material[cur_mat_id].m_specularPower);

                    if (Material[cur_mat_id].m_textureID != -1)
                    {
#if EDITOR
                        if (m_levelTextures.Count - 1 >= Material[cur_mat_id].m_textureID)
                        {
                            cur_effect.Parameters["DiffuseMapTexture"].SetValue(m_levelTextures[Material[cur_mat_id].m_textureID]);
                        }
#else
                        cur_effect.Parameters["DiffuseMapTexture"].SetValue(m_levelTextures[Material[cur_mat_id].m_textureID]);
#endif
                        if (Material[cur_mat_id].m_normalID != -1)
                        {
                            cur_effect.Parameters["NormalMapTexture"].SetValue(m_levelTextures[Material[cur_mat_id].m_normalID]);
                        }
                        if (Material[cur_mat_id].m_specularID != -1)
                        {
                            cur_effect.Parameters["SpecularMapTexture"].SetValue(m_levelTextures[Material[cur_mat_id].m_specularID]);
                        }
                    }

                    cur_effect.Begin();

                    cur_effect.CurrentTechnique.Passes[0].Begin();
                    m_graphics.VertexDeclaration = part.VertexDeclaration;
                    m_graphics.Indices = mesh.IndexBuffer;
                    m_graphics.Vertices[0].SetSource(mesh.VertexBuffer, part.StreamOffset, part.VertexStride);
                    cur_effect.CurrentTechnique.Passes[0].End();

                    cur_effect.End();

                    m_graphics.DrawIndexedPrimitives(PrimitiveType.TriangleList, part.BaseVertex, 0, part.NumVertices, part.StartIndex, part.PrimitiveCount);

                    m_numNormalPolygonsRendered += part.PrimitiveCount;
                }
            }
        }

        // Forward Render content pipeline models (.x and .fbx) but with scale.
        public void ForwardRender(Model ModelData, List<CMaterial> Material, Matrix WorldMatrix, Dictionary<int, List<CEntity.SPartAndMatNum>> MeshIDs, Vector3 scale)
        {
            // TODO: Instead of m_effectForwardPhong[Material[count].m_effect], which means that it has to search for that particular effect every time i use this
            // line of code, define a new effect variable at the beginning of Render and then use that.
            // TODO: http://blogs.msdn.com/shawnhar/archive/2006/11/20/models-meshes-parts-and-bones.aspx <- Rendering optimisation.

            // Might need removing for optimisation, that will mean that all models need their transforms frozen.
            Matrix[] transforms = new Matrix[ModelData.Bones.Count];

            ModelData.CopyAbsoluteBoneTransformsTo(transforms);

            int cur_mat_id = 0;
            foreach (KeyValuePair<int, List<CEntity.SPartAndMatNum>> mesh_id in MeshIDs)
            {
                ModelMesh mesh = ModelData.Meshes[mesh_id.Key];

                Matrix new_world_matrix = transforms[mesh.ParentBone.Index] * WorldMatrix;
                //new_world_matrix.M14 = scale.X;
                //new_world_matrix.M24 = scale.Y;
                //new_world_matrix.M34 = scale.Z;
                //new_world_matrix = Matrix.CreateScale(scale.X) * Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);
                    //new_world_matrix*Matrix.CreateScale(scale.X);
               
                

                for (int j = 0; j < mesh_id.Value.Count; ++j)
                {
                    cur_mat_id = mesh_id.Value[j].s_partMaterial;
                    ModelMeshPart part = mesh.MeshParts[mesh_id.Value[j].s_partNum];
                    Effect cur_effect = m_effectForwardPhong[Material[cur_mat_id].m_effect];

                    cur_effect.CurrentTechnique = cur_effect.Techniques["PhongTechnique"];

                    // Setting the vertex shader specific global variables
                    cur_effect.Parameters["World"].SetValue(new_world_matrix);

                    // Setting the global material variables
                    cur_effect.Parameters["g_diffuse"].SetValue(Material[cur_mat_id].m_diffuse);
                    cur_effect.Parameters["g_ambient"].SetValue(Material[cur_mat_id].m_ambient);
                    cur_effect.Parameters["g_emissive"].SetValue(Material[cur_mat_id].m_emissive);
                    cur_effect.Parameters["g_specularColour"].SetValue(Material[cur_mat_id].m_specularColour);
                    cur_effect.Parameters["g_specularPower"].SetValue(Material[cur_mat_id].m_specularPower);

                    if (Material[cur_mat_id].m_textureID != -1)
                    {
#if EDITOR
                        if (m_levelTextures.Count - 1 >= Material[cur_mat_id].m_textureID)
                        {
                            cur_effect.Parameters["DiffuseMapTexture"].SetValue(m_levelTextures[Material[cur_mat_id].m_textureID]);
                        }
#else
                        cur_effect.Parameters["DiffuseMapTexture"].SetValue(m_levelTextures[Material[cur_mat_id].m_textureID]);
#endif
                    }
                    if (Material[cur_mat_id].m_normalID != -1)
                    {
                        cur_effect.Parameters["NormalMapTexture"].SetValue(m_levelTextures[Material[cur_mat_id].m_normalID]);
                    }
                    if (Material[cur_mat_id].m_specularID != -1)
                    {
                        cur_effect.Parameters["SpecularMapTexture"].SetValue(m_levelTextures[Material[cur_mat_id].m_specularID]);
                    }

                    cur_effect.Begin();

                    cur_effect.CurrentTechnique.Passes[0].Begin();
                    m_graphics.VertexDeclaration = part.VertexDeclaration;
                    m_graphics.Indices = mesh.IndexBuffer;
                    m_graphics.Vertices[0].SetSource(mesh.VertexBuffer, part.StreamOffset, part.VertexStride);
                    cur_effect.CurrentTechnique.Passes[0].End();

                    cur_effect.End();

                    m_graphics.DrawIndexedPrimitives(PrimitiveType.TriangleList, part.BaseVertex, 0, part.NumVertices, part.StartIndex, part.PrimitiveCount);

                    m_numNormalPolygonsRendered += part.PrimitiveCount;
                }
            }
        }

        public void BeginRenderForGlitterTransparentEntities(CCamera Camera)
        {
            if (m_lightInForwardRendering == true)
            {
                foreach (Effect effect in m_effectGlitterForwardPhong)
                {
                    effect.Parameters["View"].SetValue(Camera.GetView());
                    effect.Parameters["Projection"].SetValue(Camera.GetProjection());
                    effect.Parameters["g_cameraPosition"].SetValue(Camera.GetPosition());
                    effect.Parameters["g_lightDirection"].SetValue(m_forwardRenderLight.s_direction);
                    effect.Parameters["g_lightColour"].SetValue(new Vector4(m_forwardRenderLight.s_colour, 1.0f));
                    effect.Parameters["g_lightIntensity"].SetValue(m_forwardRenderLight.s_intensity);
                    effect.Parameters["g_lightEnabled"].SetValue(true);
                    effect.Parameters["NoiseVolumeTexture"].SetValue(m_noiseVolume);
                    effect.Parameters["g_farDistance"].SetValue(Camera.GetFarClipDistance());
                    effect.CommitChanges();
                }
            }
            else
            {
                foreach (Effect effect in m_effectGlitterForwardPhong)
                {
                    effect.Parameters["View"].SetValue(Camera.GetView());
                    effect.Parameters["Projection"].SetValue(Camera.GetProjection());
                    effect.Parameters["g_cameraPosition"].SetValue(Camera.GetPosition());
                    effect.Parameters["g_lightEnabled"].SetValue(false);
                    effect.Parameters["NoiseVolumeTexture"].SetValue(m_noiseVolume);
                    effect.Parameters["g_farDistance"].SetValue(Camera.GetFarClipDistance());
                    effect.CommitChanges();
                }
            }
        }

        // Render content pipeline models (.x and .fbx)
        public void GlitterForwardRender(Model ModelData, List<CMaterial> Material, Matrix WorldMatrix, Dictionary<int, List<CEntity.SPartAndMatNum>> MeshIDs)
        {
            // TODO: Instead of m_effectGlitterForwardPhong[Material[count].m_effect], which means that it has to search for that particular effect every time i use this
            // line of code, define a new effect variable at the beginning of Render and then use that.
            // TODO: http://blogs.msdn.com/shawnhar/archive/2006/11/20/models-meshes-parts-and-bones.aspx <- Rendering optimisation.

            // Might need removing for optimisation, that will mean that all models need their transforms frozen.
            Matrix[] transforms = new Matrix[ModelData.Bones.Count];

            ModelData.CopyAbsoluteBoneTransformsTo(transforms);

            int cur_mat_id = 0;
            foreach (KeyValuePair<int, List<CEntity.SPartAndMatNum>> mesh_id in MeshIDs)
            {
                ModelMesh mesh = ModelData.Meshes[mesh_id.Key];

                Matrix new_world_matrix = transforms[mesh.ParentBone.Index] * WorldMatrix;

                for (int j = 0; j < mesh_id.Value.Count; ++j)
                {
                    cur_mat_id = mesh_id.Value[j].s_partMaterial;
                    ModelMeshPart part = mesh.MeshParts[mesh_id.Value[j].s_partNum];
                    Effect cur_effect = m_effectGlitterForwardPhong[Material[cur_mat_id].m_effect];

                    cur_effect.CurrentTechnique = cur_effect.Techniques["PhongTechnique"];

                    // Setting the vertex shader specific global variables
                    cur_effect.Parameters["World"].SetValue(new_world_matrix);

                    // Setting the global material variables
                    cur_effect.Parameters["g_diffuse"].SetValue(Material[cur_mat_id].m_diffuse);
                    cur_effect.Parameters["g_ambient"].SetValue(Material[cur_mat_id].m_ambient);
                    cur_effect.Parameters["g_emissive"].SetValue(Material[cur_mat_id].m_emissive);
                    cur_effect.Parameters["g_specularColour"].SetValue(Material[cur_mat_id].m_specularColour);
                    cur_effect.Parameters["g_specularPower"].SetValue(Material[cur_mat_id].m_specularPower);

                    if (Material[cur_mat_id].m_textureID != -1)
                    {
                        cur_effect.Parameters["DiffuseMapTexture"].SetValue(m_levelTextures[Material[cur_mat_id].m_textureID]);
                    }
                    if (Material[cur_mat_id].m_normalID != -1)
                    {
                        cur_effect.Parameters["NormalMapTexture"].SetValue(m_levelTextures[Material[cur_mat_id].m_normalID]);
                    }
                    if (Material[cur_mat_id].m_specularID != -1)
                    {
                        cur_effect.Parameters["SpecularMapTexture"].SetValue(m_levelTextures[Material[cur_mat_id].m_specularID]);
                    }

                    cur_effect.Begin();

                    cur_effect.CurrentTechnique.Passes[0].Begin();
                    m_graphics.VertexDeclaration = part.VertexDeclaration;
                    m_graphics.Indices = mesh.IndexBuffer;
                    m_graphics.Vertices[0].SetSource(mesh.VertexBuffer, part.StreamOffset, part.VertexStride);
                    cur_effect.CurrentTechnique.Passes[0].End();

                    cur_effect.End();

                    m_graphics.DrawIndexedPrimitives(PrimitiveType.TriangleList, part.BaseVertex, 0, part.NumVertices, part.StartIndex, part.PrimitiveCount);

                    m_numNormalPolygonsRendered += part.PrimitiveCount;
                }
            }
        }

        // Render content pipeline models (.x and .fbx) for Shadow
        public void RenderForShadow(Model ModelData, Matrix WorldMatrix)
        {
            if (m_shadowsActive == false)
            {
                return;
            }

            Matrix[] transforms = new Matrix[ModelData.Bones.Count];
            ModelData.CopyAbsoluteBoneTransformsTo(transforms);

            int count = 0;
            foreach (ModelMesh mesh in ModelData.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    m_effectPhongShadow[0].CurrentTechnique = m_effectPhongShadow[0].Techniques["PhongTechnique"];

                    // Setting the vertex shader specific global variables
                    m_effectPhongShadow[0].Parameters["World"].SetValue(transforms[mesh.ParentBone.Index] * WorldMatrix);

                    m_effectPhongShadow[0].Begin();

                    m_effectPhongShadow[0].CurrentTechnique.Passes[0].Begin();
                    m_graphics.VertexDeclaration = part.VertexDeclaration;
                    m_graphics.Indices = mesh.IndexBuffer;
                    m_graphics.Vertices[0].SetSource(mesh.VertexBuffer, part.StreamOffset, part.VertexStride);
                    m_effectPhongShadow[0].CurrentTechnique.Passes[0].End();

                    m_effectPhongShadow[0].End();

                    m_graphics.DrawIndexedPrimitives(PrimitiveType.TriangleList, part.BaseVertex, 0, part.NumVertices, part.StartIndex, part.PrimitiveCount);

                    m_numShadowPolygonsRendered += part.PrimitiveCount;

                    ++count;
                }
            }
        }

        public void Render(CNavQuadModel ModelData, CCamera Camera, Matrix WorldMatrix)
        {
            //m_graphics.RenderState.FillMode = FillMode.WireFrame;

            m_graphics.VertexDeclaration = m_boundVD;

            m_effectBound.CurrentTechnique = m_effectBound.Techniques["BoundingBox"];
            m_effectBound.Begin(SaveStateMode.SaveState);

            m_effectBound.Parameters["World"].SetValue(WorldMatrix);
            m_effectBound.Parameters["View"].SetValue(Camera.GetView());
            m_effectBound.Parameters["Projection"].SetValue(Camera.GetProjection());
            m_effectBound.Parameters["g_farDistance"].SetValue(Camera.GetFarClipDistance());

            // Render the nav quad
            m_effectBound.CurrentTechnique.Passes[1].Begin();
            m_graphics.DrawUserIndexedPrimitives<VertexPositionColor>(PrimitiveType.TriangleList, ModelData.m_vertices, 0, 4, ModelData.m_indices, 0, 2);
            m_effectBound.CurrentTechnique.Passes[1].End();

            m_effectBound.End();
        }

        public void ForwardRender(CNavQuadModel ModelData, CCamera Camera, Matrix WorldMatrix)
        {
            //m_graphics.RenderState.FillMode = FillMode.WireFrame;

            m_graphics.VertexDeclaration = m_boundVD;

            m_effectBound.CurrentTechnique = m_effectBound.Techniques["BoundingBox"];
            m_effectBound.Begin(SaveStateMode.SaveState);

            m_effectBound.Parameters["g_WVP"].SetValue(WorldMatrix * Camera.GetView() * Camera.GetProjection());

            // Render the OBB
            m_effectBound.CurrentTechnique.Passes[0].Begin();
            m_graphics.DrawUserIndexedPrimitives<VertexPositionColor>(PrimitiveType.TriangleList, ModelData.m_vertices, 0, 4, ModelData.m_indices, 0, 2);
            m_effectBound.CurrentTechnique.Passes[0].End();

            m_effectBound.End();
        }

        // Render indexed primitives
        public void Render(CCustomModel ModelData, CCamera Camera, CMaterial Material, Matrix WorldMatrix)
        {
            //m_graphics.RenderState.FillMode = FillMode.WireFrame;

            m_effectPhong[Material.m_effect].CurrentTechnique = m_effectPhong[Material.m_effect].Techniques["PhongTechnique"];

            // Setting the vertex shader specific global variables
            m_effectPhong[Material.m_effect].Parameters["World"].SetValue(WorldMatrix);
            m_effectPhong[Material.m_effect].Parameters["TransInvWorldView"].SetValue(Matrix.Transpose(Matrix.Invert(WorldMatrix * Camera.GetView())));

            // Setting the global material variables
            m_effectPhong[Material.m_effect].Parameters["g_diffuse"].SetValue(Material.m_diffuse);
            m_effectPhong[Material.m_effect].Parameters["g_ambient"].SetValue(Material.m_ambient);
            m_effectPhong[Material.m_effect].Parameters["g_emissive"].SetValue(Material.m_emissive);
            m_effectPhong[Material.m_effect].Parameters["g_specularColour"].SetValue(Material.m_specularColour);
            m_effectPhong[Material.m_effect].Parameters["g_specularPower"].SetValue(Material.m_specularPower / 255.0f);
            if (Material.m_textureID != -1)
            {
                m_effectPhong[Material.m_effect].Parameters["DiffuseMapTexture"].SetValue(m_levelTextures[Material.m_textureID]);
            }

            m_effectPhong[Material.m_effect].Begin();

            m_effectPhong[Material.m_effect].CurrentTechnique.Passes[0].Begin();
            m_graphics.VertexDeclaration = ModelData.m_vertexDeclaration;
            m_graphics.Indices = ModelData.m_indexBuffer;
            m_graphics.Vertices[0].SetSource(ModelData.m_vertexBuffer, 0, VertexPositionNormalTexture.SizeInBytes);
            m_effectPhong[Material.m_effect].CurrentTechnique.Passes[0].End();

            m_effectPhong[Material.m_effect].End();

            m_graphics.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, ModelData.m_numVerts, 0, ModelData.m_numTriangles);

            m_numNormalPolygonsRendered += ModelData.m_numTriangles;
        }

        // Render indexed primitives
        public void ForwardRender(CCustomModel ModelData, CMaterial Material, Matrix WorldMatrix)
        {
            //m_graphics.RenderState.FillMode = FillMode.WireFrame;

            m_effectForwardPhong[Material.m_effect].CurrentTechnique = m_effectForwardPhong[Material.m_effect].Techniques["PhongTechnique"];

            // Setting the vertex shader specific global variables
            m_effectForwardPhong[Material.m_effect].Parameters["World"].SetValue(WorldMatrix);

            // Setting the global material variables
            m_effectForwardPhong[Material.m_effect].Parameters["g_diffuse"].SetValue(Material.m_diffuse);
            m_effectForwardPhong[Material.m_effect].Parameters["g_ambient"].SetValue(Material.m_ambient);
            m_effectForwardPhong[Material.m_effect].Parameters["g_emissive"].SetValue(Material.m_emissive);
            m_effectForwardPhong[Material.m_effect].Parameters["g_specularColour"].SetValue(Material.m_specularColour);
            m_effectForwardPhong[Material.m_effect].Parameters["g_specularPower"].SetValue(Material.m_specularPower);
            if (Material.m_textureID != -1)
            {
                m_effectForwardPhong[Material.m_effect].Parameters["DiffuseMapTexture"].SetValue(m_levelTextures[Material.m_textureID]);
            }

            m_effectForwardPhong[Material.m_effect].Begin();

            m_effectForwardPhong[Material.m_effect].CurrentTechnique.Passes[0].Begin();
            m_graphics.VertexDeclaration = ModelData.m_vertexDeclaration;
            m_graphics.Indices = ModelData.m_indexBuffer;
            m_graphics.Vertices[0].SetSource(ModelData.m_vertexBuffer, 0, VertexPositionNormalTexture.SizeInBytes);
            m_effectForwardPhong[Material.m_effect].CurrentTechnique.Passes[0].End();

            m_effectForwardPhong[Material.m_effect].End();

            m_graphics.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, ModelData.m_numVerts, 0, ModelData.m_numTriangles);

            m_numNormalPolygonsRendered += ModelData.m_numTriangles;
        }

        // Render indexed primitives for Shadow
        public void RenderForShadow(CCustomModel ModelData, Matrix WorldMatrix)
        {
            if (m_shadowsActive == false)
            {
                return;
            }

            //m_graphics.RenderState.FillMode = FillMode.WireFrame;

            m_effectPhongShadow[0].CurrentTechnique = m_effectPhongShadow[0].Techniques["PhongTechnique"];

            // Setting the vertex shader specific global variables
            m_effectPhongShadow[0].Parameters["World"].SetValue(WorldMatrix);

            m_effectPhongShadow[0].Begin();

            m_effectPhongShadow[0].CurrentTechnique.Passes[0].Begin();
            m_graphics.VertexDeclaration = ModelData.m_vertexDeclaration;
            m_graphics.Indices = ModelData.m_indexBuffer;
            m_graphics.Vertices[0].SetSource(ModelData.m_vertexBuffer, 0, VertexPositionNormalTexture.SizeInBytes);
            m_effectPhongShadow[0].CurrentTechnique.Passes[0].End();

            m_effectPhongShadow[0].End();

            m_graphics.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, ModelData.m_numVerts, 0, ModelData.m_numTriangles);

            m_numShadowPolygonsRendered += ModelData.m_numTriangles;
        }

        // Render SkinnedModel models (.x and .fbx)
        public void Render(Model ModelData, AnimationController AnimController, CCamera Camera, List<CMaterial> Material, Matrix WorldMatrix, Dictionary<int, List<CEntity.SPartAndMatNum>> MeshIDs)
        {
            Matrix[] transforms = new Matrix[ModelData.Bones.Count];
            ModelData.CopyAbsoluteBoneTransformsTo(transforms);

            int cur_mat_id = 0;
            foreach( KeyValuePair<int, List<CEntity.SPartAndMatNum>> mesh_id in MeshIDs )
            {
                ModelMesh modelMesh = ModelData.Meshes[mesh_id.Key];

                Matrix new_world_matrix = transforms[modelMesh.ParentBone.Index] * WorldMatrix;

                for (int j = 0; j < mesh_id.Value.Count; ++j)
                {
                    cur_mat_id = mesh_id.Value[j].s_partMaterial;
                    ModelMeshPart part = modelMesh.MeshParts[mesh_id.Value[j].s_partNum];
                    Effect cur_effect = m_effectSkinned[Material[cur_mat_id].m_effect];

                    cur_effect.CurrentTechnique = cur_effect.Techniques["PhongTechnique"];

                    // Setting the global material variables
                    cur_effect.Parameters["World"].SetValue(new_world_matrix);
                    cur_effect.Parameters["TransInvWorldView"].SetValue(Matrix.Transpose(Matrix.Invert(new_world_matrix * Camera.GetView())));

                    cur_effect.Parameters["Bones"].SetValue(AnimController.SkinnedBoneTransforms);

                    // Setting the global material variables
                    cur_effect.Parameters["g_diffuse"].SetValue(Material[cur_mat_id].m_diffuse);
                    cur_effect.Parameters["g_ambient"].SetValue(Material[cur_mat_id].m_ambient);
                    cur_effect.Parameters["g_emissive"].SetValue(Material[cur_mat_id].m_emissive);
                    cur_effect.Parameters["g_specularColour"].SetValue(Material[cur_mat_id].m_specularColour);
                    cur_effect.Parameters["g_specularPower"].SetValue(Material[cur_mat_id].m_specularPower / 255.0f);

                    if (Material[cur_mat_id].m_textureID != -1)
                    {
#if EDITOR
                        if (m_levelTextures.Count - 1 >= Material[cur_mat_id].m_textureID)
                        {
                            cur_effect.Parameters["DiffuseMapTexture"].SetValue(m_levelTextures[Material[cur_mat_id].m_textureID]);
                        }
#else
                        cur_effect.Parameters["DiffuseMapTexture"].SetValue(m_levelTextures[Material[cur_mat_id].m_textureID]);
#endif
                        if (Material[cur_mat_id].m_normalID != -1)
                        {
                            cur_effect.Parameters["NormalMapTexture"].SetValue(m_levelTextures[Material[cur_mat_id].m_normalID]);
                        }
                        if (Material[cur_mat_id].m_specularID != -1)
                        {
                            cur_effect.Parameters["SpecularMapTexture"].SetValue(m_levelTextures[Material[cur_mat_id].m_specularID]);
                        }
                    }

                    cur_effect.Begin();

                    cur_effect.CurrentTechnique.Passes[0].Begin();
                    m_graphics.VertexDeclaration = part.VertexDeclaration;
                    m_graphics.Indices = modelMesh.IndexBuffer;
                    m_graphics.Vertices[0].SetSource(modelMesh.VertexBuffer, part.StreamOffset, part.VertexStride);
                    cur_effect.CurrentTechnique.Passes[0].End();

                    cur_effect.End();

                    m_graphics.DrawIndexedPrimitives(PrimitiveType.TriangleList, part.BaseVertex, 0, part.NumVertices, part.StartIndex, part.PrimitiveCount);

                    m_numNormalPolygonsRendered += part.PrimitiveCount;
                }
            }
        }

        // Render SkinnedModel models (.x and .fbx)
        public void ForwardRender(Model ModelData, AnimationController AnimController, List<CMaterial> Material, Matrix WorldMatrix,
            Dictionary<int, List<CEntity.SPartAndMatNum>> MeshIDs)
        {
            Matrix[] transforms = new Matrix[ModelData.Bones.Count];
            ModelData.CopyAbsoluteBoneTransformsTo(transforms);

            int cur_mat_id = 0;
            foreach (KeyValuePair<int, List<CEntity.SPartAndMatNum>> mesh_id in MeshIDs)
            {
                ModelMesh modelMesh = ModelData.Meshes[mesh_id.Key];

                Matrix new_world_matrix = transforms[modelMesh.ParentBone.Index] * WorldMatrix;

                for (int j = 0; j < mesh_id.Value.Count; ++j)
                {
                    cur_mat_id = mesh_id.Value[j].s_partMaterial;
                    ModelMeshPart part = modelMesh.MeshParts[mesh_id.Value[j].s_partNum];
                    Effect cur_effect = m_effectForwardSkinned[Material[cur_mat_id].m_effect];

                    cur_effect.CurrentTechnique = cur_effect.Techniques["PhongTechnique"];

                    // Setting the global material variables
                    cur_effect.Parameters["World"].SetValue(new_world_matrix);

                    cur_effect.Parameters["Bones"].SetValue(AnimController.SkinnedBoneTransforms);

                    // Setting the global material variables
                    cur_effect.Parameters["g_diffuse"].SetValue(Material[cur_mat_id].m_diffuse);
                    cur_effect.Parameters["g_ambient"].SetValue(Material[cur_mat_id].m_ambient);
                    cur_effect.Parameters["g_emissive"].SetValue(Material[cur_mat_id].m_emissive);
                    cur_effect.Parameters["g_specularColour"].SetValue(Material[cur_mat_id].m_specularColour);
                    cur_effect.Parameters["g_specularPower"].SetValue(Material[cur_mat_id].m_specularPower);

                    if (Material[cur_mat_id].m_textureID != -1)
                    {
                        cur_effect.Parameters["DiffuseMapTexture"].SetValue(m_levelTextures[Material[cur_mat_id].m_textureID]);

                        if (Material[cur_mat_id].m_normalID != -1)
                        {
                            cur_effect.Parameters["NormalMapTexture"].SetValue(m_levelTextures[Material[cur_mat_id].m_normalID]);
                        }
                        if (Material[cur_mat_id].m_specularID != -1)
                        {
                            cur_effect.Parameters["SpecularMapTexture"].SetValue(m_levelTextures[Material[cur_mat_id].m_specularID]);
                        }
                    }

                    cur_effect.Begin();

                    cur_effect.CurrentTechnique.Passes[0].Begin();
                    m_graphics.VertexDeclaration = part.VertexDeclaration;
                    m_graphics.Indices = modelMesh.IndexBuffer;
                    m_graphics.Vertices[0].SetSource(modelMesh.VertexBuffer, part.StreamOffset, part.VertexStride);
                    cur_effect.CurrentTechnique.Passes[0].End();

                    cur_effect.End();

                    m_graphics.DrawIndexedPrimitives(PrimitiveType.TriangleList, part.BaseVertex, 0, part.NumVertices, part.StartIndex, part.PrimitiveCount);

                    m_numNormalPolygonsRendered += part.PrimitiveCount;
                }
            }
        }

        // Render SkinnedModel models (.x and .fbx) for Shadows
        public void RenderForShadow(SkinnedModel SkinModel, AnimationController AnimController, CCamera Camera, Matrix WorldMatrix)
        {
            if (m_shadowsActive == false)
            {
                return;
            }

            Matrix[] transforms = new Matrix[SkinModel.Model.Bones.Count];
            SkinModel.Model.CopyAbsoluteBoneTransformsTo(transforms);

            int count = 0;
            foreach (ModelMesh modelMesh in SkinModel.Model.Meshes)
            {
                Matrix new_world_matrix = transforms[modelMesh.ParentBone.Index] * WorldMatrix;

                foreach (ModelMeshPart part in modelMesh.MeshParts)
                {
                    m_effectPhongShadow[1].CurrentTechnique = m_effectPhongShadow[1].Techniques["PhongTechnique"];

                    // Setting the global material variables
                    m_effectPhongShadow[1].Parameters["World"].SetValue(new_world_matrix);
                    m_effectPhongShadow[1].Parameters["TransInvWorldView"].SetValue(Matrix.Transpose(Matrix.Invert(new_world_matrix * Camera.GetView())));

                    m_effectPhongShadow[1].Parameters["Bones"].SetValue(AnimController.SkinnedBoneTransforms);

                    m_effectPhongShadow[1].Begin();

                    m_effectPhongShadow[1].CurrentTechnique.Passes[0].Begin();
                    m_graphics.VertexDeclaration = part.VertexDeclaration;
                    m_graphics.Indices = modelMesh.IndexBuffer;
                    m_graphics.Vertices[0].SetSource(modelMesh.VertexBuffer, part.StreamOffset, part.VertexStride);
                    m_effectPhongShadow[1].CurrentTechnique.Passes[0].End();

                    m_effectPhongShadow[1].End();

                    m_graphics.DrawIndexedPrimitives(PrimitiveType.TriangleList, part.BaseVertex, 0, part.NumVertices, part.StartIndex, part.PrimitiveCount);

                    m_numShadowPolygonsRendered += part.PrimitiveCount;

                    ++count;
                }
            }
        }

        public void RenderRawVertices(VertexBuffer buffer, CCamera Camera, CMaterial Mat, Matrix WorldMatrix, bool tintModelBadly)
        {
            if (tintModelBadly == true)
            { }

            BasicEffect effect = new BasicEffect(GetGraphicsDevice(), null);

            //effect.CurrentTechnique = effect.Techniques["Simplest"];
            effect.Parameters["View"].SetValue(Camera.GetView());
            //effect.Parameters["CameraPos"].SetValue(Camera.m_position);

            effect.Projection = Camera.GetProjection();
            effect.Parameters["World"].SetValue(WorldMatrix);
            //effect.Parameters["tintBlue"].SetValue(tintModelBadly);
            //effect.DiffuseColor
            m_graphics.Vertices[0].SetSource(buffer, 0, VertexPositionColor.SizeInBytes);
            effect.Begin();
            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Begin();
                m_graphics.VertexDeclaration = new VertexDeclaration(m_graphics, VertexPositionColor.VertexElements);

                
                //m_graphics.Indices = ModelData.m_indexBuffer;
                

                 m_graphics.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 4, 0, 12);
                pass.End();

                m_numNormalPolygonsRendered += 12;
            }
           
            effect.End();
        }

        public float GetAspectRatio()
        {
            return (float)m_graphics.Viewport.Width / (float)m_graphics.Viewport.Height;
        }

        public uint GetWidthRes()
        {
            return (uint)m_graphics.Viewport.Width;
        }

        public uint GetHeightRes()
        {
            return (uint)m_graphics.Viewport.Height;
        }

        public GraphicsDevice GetGraphicsDevice()
        {
            return m_graphics;
        }

#if WINDOWS
        public void TakeScreenShot()
        {
            ++m_scCount;

            m_graphics.ResolveBackBuffer(m_renderTargetTexture, 0);

            string file_name = "sc" + m_scCount + "_" +
                System.DateTime.Now.Date.Day + "-" + System.DateTime.Now.Date.Month + "-" + System.DateTime.Now.Date.Year
                + "_" + "h" + System.DateTime.Now.Hour.ToString() + "m" + System.DateTime.Now.Minute.ToString() + "s" + System.DateTime.Now.Second.ToString()
                + "ms" + System.DateTime.Now.Millisecond.ToString()
                + ".png";

            m_renderTargetTexture.Save(file_name, ImageFileFormat.Png);
        }
#endif

        protected void SelectFinalDeferredShader()
        {
            if( m_SSAOActive == true )
            {
                if( m_shadowsActive == true )
                {
                    m_effectFinalDeferred = m_effectFinalDeferredSSAOShadow;
                }
                else
                {
                    m_effectFinalDeferred = m_effectFinalDeferredSSAO;
                }
            }
            else
            {
                if (m_shadowsActive == true)
                {
                    m_effectFinalDeferred = m_effectFinalDeferredShadow;
                }
                else
                {
                    m_effectFinalDeferred = m_effectFinalDeferredBasic;
                }
            }
        }

        public bool TurnOnOffSSAO()
        {
            if (m_supportsSM3 == true)
            {
                if (m_SSAOActive == false)
                {
                    m_SSAOActive = true;
                }
                else
                {
                    m_SSAOActive = false;
                }

                SelectFinalDeferredShader();

                return true;
            }

            return false;
        }

        public bool IsSSAOActive
        {
            get { return m_SSAOActive; }
        }

        public void TurnOnOffShadows()
        {
            if (m_shadowsActive == false)
            {
                m_shadowsActive = true;
            }
            else
            {
                m_shadowsActive = false;
            }

            SelectFinalDeferredShader();
        }

        public bool AreShadowsActive
        {
            get { return m_shadowsActive; }
        }

        public void TurnOnOffLights()
        {
            if (m_lightsActive == false)
            {
                m_lightsActive = true;
            }
            else
            {
                m_lightsActive = false;
            }
        }

        public void SetShadowRTDimension()
        {
            switch (m_shadowRTDimension)
            {
                case 512:
                    {
                        m_shadowRTDimension = 1 << 10;

                        break;
                    }
                case 1024:
                    {
                        m_shadowRTDimension = 1 << 11;
                        break;
                    }
                case 2048:
                    {
                        m_shadowRTDimension = 1 << 9;
                        break;
                    }
            }

            m_dsbShadow.Dispose();
            m_dsbShadow = null;

#if WINDOWS
            m_dsbShadow = new DepthStencilBuffer(m_graphics, m_shadowRTDimension, m_shadowRTDimension, DepthFormat.Depth16);
#else
            m_dsbShadow = new DepthStencilBuffer(m_graphics, m_shadowRTDimension, m_shadowRTDimension, DepthFormat.Depth24);
#endif
        }

        public int ShadowRTDimension
        {
            get { return m_shadowRTDimension; }
        }

        public void TurnOnOffBlurShadows()
        {
            if (m_blurShadows == false)
            {
                m_blurShadows = true;
            }
            else
            {
                m_blurShadows = false;
            }
        }

        public bool IsBlurShadowsActive
        {
            get { return m_blurShadows; }
        }

        public void TurnOnOffBlurSSAO()
        {
            if (m_blurSSAO == false)
            {
                m_blurSSAO = true;
            }
            else
            {
                m_blurSSAO = false;
            }
        }

        public bool IsBlurSSAOActive
        {
            get { return m_blurSSAO; }
        }

        public Vector2 GetDefaultResolution()
        {
#if ENGINE
            DisplayModeCollection disp_mode = GraphicsAdapter.DefaultAdapter.SupportedDisplayModes;
            foreach (DisplayMode dm in disp_mode)
            {
                return new Vector2(dm.Width, dm.Height);
            }

            return Vector2.Zero;
#else
            return Vector2.Zero;
#endif
        }

        public enum ETextureFilter : uint
        {
            e_none = 0,
            e_point,
            e_linear,
            GaussianQuad,
            PyramidalQuad,
            e_2xanisotropy,
            e_4xanisotropy,
            e_8xanisotropy,
            e_16xanisotropy
        }

        ETextureFilter m_curTexFilter = ETextureFilter.e_point;

        public uint GetDefaultTextureFilter()
        {
            return (uint)ETextureFilter.e_point;
        }

        public uint GetCurrentTextureFilter()
        {
            return (uint)m_curTexFilter;
        }

        public void SetTextureFilter()
        {
            switch ((ETextureFilter)m_curTexFilter)
            {
                case ETextureFilter.e_none:
                    {
                        m_graphics.SamplerStates[0].MagFilter = TextureFilter.None;
                        m_graphics.SamplerStates[0].MinFilter = TextureFilter.None;
                        m_graphics.SamplerStates[0].MipFilter = TextureFilter.None;
                        break;
                    }
                case ETextureFilter.e_point:
                    {
                        m_graphics.SamplerStates[0].MagFilter = TextureFilter.Point;
                        m_graphics.SamplerStates[0].MinFilter = TextureFilter.Point;
                        m_graphics.SamplerStates[0].MipFilter = TextureFilter.Point;
                        break;
                    }
                case ETextureFilter.e_linear:
                    {
                        m_graphics.SamplerStates[0].MagFilter = TextureFilter.Linear;
                        m_graphics.SamplerStates[0].MinFilter = TextureFilter.Linear;
                        m_graphics.SamplerStates[0].MipFilter = TextureFilter.Linear;
                        break;
                    }
                case ETextureFilter.GaussianQuad:
                    {
                        m_graphics.SamplerStates[0].MagFilter = TextureFilter.GaussianQuad;
                        m_graphics.SamplerStates[0].MinFilter = TextureFilter.GaussianQuad;
                        m_graphics.SamplerStates[0].MipFilter = TextureFilter.GaussianQuad;
                        break;
                    }
                case ETextureFilter.PyramidalQuad:
                    {
                        m_graphics.SamplerStates[0].MagFilter = TextureFilter.PyramidalQuad;
                        m_graphics.SamplerStates[0].MinFilter = TextureFilter.PyramidalQuad;
                        m_graphics.SamplerStates[0].MipFilter = TextureFilter.PyramidalQuad;
                        break;
                    }
                case ETextureFilter.e_2xanisotropy:
                    {
                        m_graphics.SamplerStates[0].MagFilter = TextureFilter.Anisotropic;
                        m_graphics.SamplerStates[0].MinFilter = TextureFilter.Anisotropic;
                        m_graphics.SamplerStates[0].MipFilter = TextureFilter.Anisotropic;
                        m_graphics.SamplerStates[0].MaxAnisotropy = 2;
                        break;
                    }
                case ETextureFilter.e_4xanisotropy:
                    {
                        m_graphics.SamplerStates[0].MagFilter = TextureFilter.Anisotropic;
                        m_graphics.SamplerStates[0].MinFilter = TextureFilter.Anisotropic;
                        m_graphics.SamplerStates[0].MipFilter = TextureFilter.Anisotropic;
                        m_graphics.SamplerStates[0].MaxAnisotropy = 4;
                        break;
                    }
                case ETextureFilter.e_8xanisotropy:
                    {
                        m_graphics.SamplerStates[0].MagFilter = TextureFilter.Anisotropic;
                        m_graphics.SamplerStates[0].MinFilter = TextureFilter.Anisotropic;
                        m_graphics.SamplerStates[0].MipFilter = TextureFilter.Anisotropic;
                        m_graphics.SamplerStates[0].MaxAnisotropy = 8;
                        break;
                    }
                case ETextureFilter.e_16xanisotropy:
                    {
                        m_graphics.SamplerStates[0].MagFilter = TextureFilter.Anisotropic;
                        m_graphics.SamplerStates[0].MinFilter = TextureFilter.Anisotropic;
                        m_graphics.SamplerStates[0].MipFilter = TextureFilter.Anisotropic;
                        m_graphics.SamplerStates[0].MaxAnisotropy = 16;
                        break;
                    }
            }
        }

        public void IncrementTextureFilter()
        {
            ++m_curTexFilter;

            if (m_curTexFilter > ETextureFilter.e_16xanisotropy)
            {
                m_curTexFilter = ETextureFilter.e_none;
            }

            SetTextureFilter();
        }

        public uint GetDefaultMaxMipLevel()
        {
            return (uint)m_graphics.SamplerStates[0].MaxMipLevel;
        }

        public void SetMaxMipLevel(uint MaxMipLevel)
        {
            m_graphics.SamplerStates[0].MaxMipLevel = (int)MaxMipLevel;
        }

        public bool IsFullScreen
        {
#if ENGINE
            get { return m_graphicsManager.IsFullScreen; }
#else
           
            get
            {
                return false;
            }
#endif
        }

#if ENGINE
        public void TurnOnOffFullScreen()
        {
            if (m_graphicsManager.IsFullScreen == false)
            {
                m_graphicsManager.IsFullScreen = true;
            }
            else
            {
                m_graphicsManager.IsFullScreen = false;
            }

            m_fullScreenHasChanged = true;
        }

        public List<Vector2> GetSupportedResolutions()
        {
            List<Vector2> res = new List<Vector2>();
            Vector2 first_res = Vector2.Zero;

            DisplayModeCollection disp_mode = GraphicsAdapter.DefaultAdapter.SupportedDisplayModes;
            foreach (DisplayMode dm in disp_mode)
            {
                bool foundsame = false;

                if (dm.Width >= 640)
                {
                    foreach (Vector2 stored_res in res)
                    {
                        if (stored_res.X == dm.Width && stored_res.Y == dm.Height)
                        {
                            foundsame = true;
                            break;
                        }
                    }

                    if (foundsame == false)
                    {
                        res.Add(new Vector2(dm.Width, dm.Height));
                    }
                }
            }

            return res;
        }

        public void SetScreenResolution(Vector2 ScreenResolution)
        {
#if WINDOWS
            if (m_graphicsManager.PreferredBackBufferWidth != (int)ScreenResolution.X ||
                m_graphicsManager.PreferredBackBufferHeight != (int)ScreenResolution.Y)
            {
                m_scrnResChanged = true;
            }

            m_graphicsManager.PreferredBackBufferWidth = (int)ScreenResolution.X;
            m_graphicsManager.PreferredBackBufferHeight = (int)ScreenResolution.Y;
#endif
        }

        public bool SetGraphicsSettings(CDataFromXML.SGraphicsMenuSettings GS)
        {
            m_blurShadows = GS.s_blurShadows;
            m_shadowRTDimension = GS.s_shadowRes;
            m_shadowsActive = GS.s_shadows;
            m_SSAOActive = GS.s_ssao;
            m_blurSSAO = GS.s_blurSSAO;

            if (m_supportsSM3 == true)
            {
                if (GS.s_soft == true)
                {
                    m_indexOffsetForSoftParticles = 3;
                }
                else
                {
                    m_indexOffsetForSoftParticles = 0;
                }
            }
            else
            {
                m_indexOffsetForSoftParticles = 0;
            }

#if WINDOWS
            if (m_graphicsManager.PreferredBackBufferWidth != (int)GS.s_screenRes.X ||
                m_graphicsManager.PreferredBackBufferHeight != (int)GS.s_screenRes.Y)
            {
                m_scrnResChanged = true;
            }
#endif

            if (m_graphicsManager.IsFullScreen != GS.s_fullScreen)
            {
                m_fullScreenHasChanged = true;
            }

            m_graphicsManager.IsFullScreen = GS.s_fullScreen;

            SelectFinalDeferredShader();

            if (m_scrnResChanged == true || m_fullScreenHasChanged == true)
            {
                m_graphicsManager.PreferredBackBufferWidth = (int)GS.s_screenRes.X;
                m_graphicsManager.PreferredBackBufferHeight = (int)GS.s_screenRes.Y;
                m_graphicsManager.ApplyChanges();

                m_scrnResChanged = false;
                m_fullScreenHasChanged = false;
                return true;
            }

            m_curTexFilter = (ETextureFilter)GS.s_textureFilter;
            SetTextureFilter();

            return false;
        }
#else
        public bool SetGraphicsSettings()
        {
            m_blurShadows = true;
            m_giActive = false;
            m_shadowRTDimension = 512;
            m_shadowsActive = false;
            m_SSAOActive = false;
            m_blurSSAO = true;

            SelectFinalDeferredShader();

            return false;
        }
#endif

        public CDataFromXML.SGraphicsMenuSettings GetGraphicsSettings()
        {
            CDataFromXML.SGraphicsMenuSettings rtn_val = new CDataFromXML.SGraphicsMenuSettings();

            rtn_val.s_blurShadows = m_blurShadows;

            if (m_indexOffsetForSoftParticles == 3)
            {
                rtn_val.s_soft = true;
            }
            else
            {
                rtn_val.s_soft = false;
            }

            rtn_val.s_shadowRes = m_shadowRTDimension;
            rtn_val.s_shadows = m_shadowsActive;
            rtn_val.s_ssao = m_SSAOActive;
            rtn_val.s_blurSSAO = m_blurSSAO;
#if ENGINE
            rtn_val.s_fullScreen = m_graphicsManager.IsFullScreen;
            rtn_val.s_screenRes = new Vector2(m_graphicsManager.PreferredBackBufferWidth, m_graphicsManager.PreferredBackBufferHeight);
#endif
            rtn_val.s_textureFilter = (uint)m_curTexFilter;
            rtn_val.s_maxMipLevel = (uint)m_graphics.SamplerStates[0].MaxMipLevel;

            return rtn_val;
        }

        public void SetSoftParticle()
        {
            if (m_supportsSM3 == true)
            {
                if (m_indexOffsetForSoftParticles == 0)
                {
                    m_indexOffsetForSoftParticles = 3;
                }
                else
                {
                    m_indexOffsetForSoftParticles = 0;
                }
            }
        }

        public bool IsSoftParticle()
        {
            if (m_indexOffsetForSoftParticles == 3)
            {
                return true;
            }

            return false;
        }

        public void SetParticleRendering(int It)
        {
            switch (It)
            {
                case 0:
                    {
                        m_particleRenderer = new CPRNormal();
                        break;
                    }
                case 1:
                    {
                        m_particleRenderer = new CPRShaderInstance(m_particleModel);
                        break;
                    }
#if WINDOWS
                case 2:
                    {
                        m_particleRenderer = new CPRHardwareInstance(m_particleModel);
                        break;
                    }
#endif
            }
        }
    }
}

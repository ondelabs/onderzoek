﻿#if WINDOWS

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Onderzoek.Camera;

namespace Onderzoek.Visual
{
    class CPRHardwareInstance : CParticleRenderer
    {
        ModelMeshPart m_meshPart;
        VertexDeclaration m_particleVD;
        DynamicVertexBuffer m_instanceDataStreamWM;
        DynamicVertexBuffer m_instanceDataStreamTint;
        DynamicVertexBuffer m_instanceDataStreamFrameNum;
        // This empty array is used when the particle effect being
        // rendered does NOT have a animated texture.
        int[] m_emptyArray;

        public CPRHardwareInstance(Model ParticleModel)
        {
            InitializeHardwareInstancing(ParticleModel);
        }

        void InitializeHardwareInstancing(Model ParticleModel)
        {
            m_emptyArray = new int[1000000];

            // When using hardware instancing, the instance transform matrix is
            // specified using a second vertex stream that provides 4x4 matrices
            // in texture coordinate channels 1 to 4. We must modify our vertex
            // declaration to include these channels.
            // I've added some more variables to this.
            VertexElement[] extraElements = new VertexElement[6];

            short offset = 0;
            byte usageIndex = 1;
            short size_of_vec4 = sizeof(float) * 4;

            for (int i = 0; i < 6; i++)
            {
                if (i < 4)
                {
                    // The stream needs to be set differently to the original data.
                    extraElements[i] = new VertexElement(1, offset,
                                                    VertexElementFormat.Vector4,
                                                    VertexElementMethod.Default,
                                                    VertexElementUsage.TextureCoordinate,
                                                    usageIndex);
                }
                else if (i == 4)
                {
                    // The stream needs to be set differently to the original data and the matrix data above.
                    extraElements[i] = new VertexElement(2, 0,
                                VertexElementFormat.Vector4,
                                VertexElementMethod.Default,
                                VertexElementUsage.TextureCoordinate,
                                usageIndex);
                }
                else
                {
                    // The stream needs to be set differently to the original data and the matrix data above.
                    // Since an int is made up of 4 bytes each of the int's bytes are put into the four components of the vector
                    extraElements[i] = new VertexElement(3, 0,
                                VertexElementFormat.Byte4,
                                VertexElementMethod.Default,
                                VertexElementUsage.TextureCoordinate,
                                usageIndex);
                }

                offset += size_of_vec4;
                ++usageIndex;
            }

            ExtendVertexDeclaration(extraElements, ParticleModel);
        }

        /// <summary>
        /// Windows only. Modifies our vertex declaration to include additional
        /// vertex input channels. This is necessary when switching between the
        /// ShaderInstancing and HardwareInstancing techniques.
        /// </summary>
        void ExtendVertexDeclaration(VertexElement[] extraElements, Model ParticleModel)
        {
            // Save the original vertex elements.
            VertexElement[] original = ParticleModel.Meshes[0].MeshParts[0].VertexDeclaration.GetVertexElements();

            // Append the new elements to the original format.
            int length = original.Length + extraElements.Length;

            VertexElement[] elements = new VertexElement[length];

            original.CopyTo(elements, 0);

            extraElements.CopyTo(elements, original.Length);

            // Create a new vertex declaration.
            m_particleVD = new VertexDeclaration(CVisual.Instance.GetGraphicsDevice(), elements);
        }

        void CParticleRenderer.BeginRender(Model Model)
        {
            ModelMesh mesh = Model.Meshes[0];
            m_meshPart = mesh.MeshParts[0];

            CVisual.Instance.GetGraphicsDevice().Vertices[0].SetSource(mesh.VertexBuffer, m_meshPart.StreamOffset, m_meshPart.VertexStride);
            CVisual.Instance.GetGraphicsDevice().Indices = mesh.IndexBuffer;

            CVisual.Instance.GetGraphicsDevice().VertexDeclaration = m_particleVD;
        }

        void CParticleRenderer.SetUpParticleEffect(Effect ShaderEffect)
        {
            ShaderEffect.Begin();
            ShaderEffect.Techniques[0].Passes[2].Begin();
        }

        void CParticleRenderer.UnDoSetUpParticleEffect(Effect ShaderEffect)
        {
            ShaderEffect.Techniques[0].Passes[2].End();
            ShaderEffect.End();
        }

        void CParticleRenderer.Render(Matrix[] WorldMatrices, Vector4[] TintColour, int[] FrameNumbers, int MaxParticlesToRender, Effect ShaderEffect)
        {
            if (WorldMatrices.Length == 0)
            {
                return;
            }
            if (WorldMatrices.Length < MaxParticlesToRender)
            {
                return;
            }

            // Make sure our instance data vertex buffer is big enough.
            int instanceDataSize = (sizeof(float) * 16) * MaxParticlesToRender;
            if ((m_instanceDataStreamWM == null) || (m_instanceDataStreamWM.SizeInBytes < instanceDataSize))
            {
                if (m_instanceDataStreamWM != null)
                {
                    m_instanceDataStreamWM.Dispose();
                }

                m_instanceDataStreamWM = new DynamicVertexBuffer(CVisual.Instance.GetGraphicsDevice(), instanceDataSize, BufferUsage.WriteOnly);
            }
            // Upload transform matrices to the instance data vertex buffer.
            m_instanceDataStreamWM.SetData(WorldMatrices, 0, MaxParticlesToRender, SetDataOptions.Discard);

            instanceDataSize = (sizeof(float) * 4) * MaxParticlesToRender;
            if ((m_instanceDataStreamTint == null) || (m_instanceDataStreamTint.SizeInBytes < instanceDataSize))
            {
                if (m_instanceDataStreamTint != null)
                {
                    m_instanceDataStreamTint.Dispose();
                }

                m_instanceDataStreamTint = new DynamicVertexBuffer(CVisual.Instance.GetGraphicsDevice(), instanceDataSize, BufferUsage.WriteOnly);
            }
            // Upload Light Positions to the instance data vertex buffer.
            m_instanceDataStreamTint.SetData(TintColour, 0, MaxParticlesToRender, SetDataOptions.Discard);

            instanceDataSize = sizeof(float) * MaxParticlesToRender;
            if ((m_instanceDataStreamFrameNum == null) || (m_instanceDataStreamFrameNum.SizeInBytes < instanceDataSize))
            {
                if (m_instanceDataStreamFrameNum != null)
                {
                    m_instanceDataStreamFrameNum.Dispose();
                }

                m_instanceDataStreamFrameNum = new DynamicVertexBuffer(CVisual.Instance.GetGraphicsDevice(), instanceDataSize, BufferUsage.WriteOnly);
            }
            // Upload Light Positions to the instance data vertex buffer.
            // Converting from int to Byte4 (a vector of 4 bytes). Since an int is made up of 4 bytes
            // each of the int's bytes are put into the four components of the vector. In the shader we convert back to an int.
            m_instanceDataStreamFrameNum.SetData(FrameNumbers, 0, MaxParticlesToRender, SetDataOptions.Discard);

            // Set up five vertex streams for instanced rendering.
            // The first stream provides the actual vertex data,
            // the second provides per-instance transform matrices,
            // the third provides per-instance tint.
            // the Fourth provides per-instance frame number.
            VertexStreamCollection vertices = CVisual.Instance.GetGraphicsDevice().Vertices;

            vertices[0].SetFrequencyOfIndexData(MaxParticlesToRender);

            vertices[1].SetSource(m_instanceDataStreamWM, 0, sizeof(float) * 16);
            vertices[1].SetFrequencyOfInstanceData(1);

            vertices[2].SetSource(m_instanceDataStreamTint, 0, sizeof(float) * 4);
            vertices[2].SetFrequencyOfInstanceData(1);

            vertices[3].SetSource(m_instanceDataStreamFrameNum, 0, sizeof(float));
            vertices[3].SetFrequencyOfInstanceData(1);

            // Draw all the instances in a single batch.
            CVisual.Instance.GetGraphicsDevice().DrawIndexedPrimitives(PrimitiveType.TriangleList, m_meshPart.BaseVertex, 0, m_meshPart.NumVertices, m_meshPart.StartIndex, m_meshPart.PrimitiveCount);

            // Reset the instancing streams.
            vertices[0].SetSource(null, 0, 0);
            vertices[1].SetSource(null, 0, 0);
            vertices[2].SetSource(null, 0, 0);
            vertices[3].SetSource(null, 0, 0);
        }

        void CParticleRenderer.Render(Matrix[] WorldMatrices, Vector4[] TintColour, int MaxParticlesToRender, Effect ShaderEffect)
        {
            if (WorldMatrices.Length == 0)
            {
                return;
            }
            if (WorldMatrices.Length < MaxParticlesToRender)
            {
                return;
            }

            // Make sure our instance data vertex buffer is big enough.
            int instanceDataSize = (sizeof(float) * 16) * MaxParticlesToRender;
            if ((m_instanceDataStreamWM == null) || (m_instanceDataStreamWM.SizeInBytes < instanceDataSize))
            {
                if (m_instanceDataStreamWM != null)
                {
                    m_instanceDataStreamWM.Dispose();
                }

                m_instanceDataStreamWM = new DynamicVertexBuffer(CVisual.Instance.GetGraphicsDevice(), instanceDataSize, BufferUsage.WriteOnly);
            }
            // Upload transform matrices to the instance data vertex buffer.
            m_instanceDataStreamWM.SetData(WorldMatrices, 0, MaxParticlesToRender, SetDataOptions.Discard);

            instanceDataSize = (sizeof(float) * 4) * MaxParticlesToRender;
            if ((m_instanceDataStreamTint == null) || (m_instanceDataStreamTint.SizeInBytes < instanceDataSize))
            {
                if (m_instanceDataStreamTint != null)
                {
                    m_instanceDataStreamTint.Dispose();
                }

                m_instanceDataStreamTint = new DynamicVertexBuffer(CVisual.Instance.GetGraphicsDevice(), instanceDataSize, BufferUsage.WriteOnly);
            }
            // Upload Light Positions to the instance data vertex buffer.
            m_instanceDataStreamTint.SetData(TintColour, 0, MaxParticlesToRender, SetDataOptions.Discard);


            instanceDataSize = sizeof(float) * MaxParticlesToRender;
            if ((m_instanceDataStreamFrameNum == null) || (m_instanceDataStreamFrameNum.SizeInBytes < instanceDataSize))
            {
                if (m_instanceDataStreamFrameNum != null)
                {
                    m_instanceDataStreamFrameNum.Dispose();
                }

                m_instanceDataStreamFrameNum = new DynamicVertexBuffer(CVisual.Instance.GetGraphicsDevice(), instanceDataSize, BufferUsage.WriteOnly);
            }
            // Upload Light Positions to the instance data vertex buffer.
            // Converting from int to Byte4 (a vector of 4 bytes). Since an int is made up of 4 bytes
            // each of the int's bytes are put into the four components of the vector. In the shader we convert back to an int.
            m_instanceDataStreamFrameNum.SetData(m_emptyArray, 0, MaxParticlesToRender, SetDataOptions.Discard);


            // Set up five vertex streams for instanced rendering.
            // The first stream provides the actual vertex data,
            // the second provides per-instance transform matrices,
            // the third provides per-instance tint.
            // the Fourth provides per-instance frame number. This is an empty array since we're nota actully wanting to use the values stored in them.
            VertexStreamCollection vertices = CVisual.Instance.GetGraphicsDevice().Vertices;

            vertices[0].SetFrequencyOfIndexData(MaxParticlesToRender);

            vertices[1].SetSource(m_instanceDataStreamWM, 0, sizeof(float) * 16);
            vertices[1].SetFrequencyOfInstanceData(1);

            vertices[2].SetSource(m_instanceDataStreamTint, 0, sizeof(float) * 4);
            vertices[2].SetFrequencyOfInstanceData(1);

            vertices[3].SetSource(m_instanceDataStreamFrameNum, 0, sizeof(float));
            vertices[3].SetFrequencyOfInstanceData(1);

            // Draw all the instances in a single batch.
            CVisual.Instance.GetGraphicsDevice().DrawIndexedPrimitives(PrimitiveType.TriangleList, m_meshPart.BaseVertex, 0, m_meshPart.NumVertices, m_meshPart.StartIndex, m_meshPart.PrimitiveCount);

            // Reset the instancing streams.
            vertices[0].SetSource(null, 0, 0);
            vertices[1].SetSource(null, 0, 0);
            vertices[2].SetSource(null, 0, 0);
            vertices[3].SetSource(null, 0, 0);
        }
    }
}

#endif
﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

using XMLManip;

namespace Onderzoek
{
    public class CLogger
    {
        static CLogger instance;

        public static CLogger Instance
        {
            get { return instance; }
        }

        public CLogger()
        {
            if (instance != null)
                throw new Exception("Cannot have multiple singletons");

            instance = this;
        }

        ~CLogger()
        {
            instance = null;
        }

        string GetTime()
        {
            return System.DateTime.Now.Hour.ToString() + System.DateTime.Now.Minute.ToString() + System.DateTime.Now.Second.ToString() + System.DateTime.Now.Millisecond.ToString();
        }

        public void ResetLog()
        {
#if ENGINE && WINDOWS
            string file = "log.xml";

            CXMLManip xml_data = new CXMLManip();

            XMLManip.XmlNodeStructure xml_node = new XMLManip.XmlNodeStructure();
            xml_node.xmlNodeName = "Logger";

            xml_node.addNewElement("First_vb0.25", "Entered Program at: " + System.DateTime.Now.Day + "/" + System.DateTime.Now.Month + "/" + System.DateTime.Now.Year + " - " + System.DateTime.Now.Hour + ":" + System.DateTime.Now.Minute + ":" + System.DateTime.Now.Second);

            xml_data.setXmlData(xml_node);
            xml_data.saveXmlFile(file);
            xml_data = null;
#endif
        }

        public void WriteToLog(string Tag, string Message)
        {
#if ENGINE && WINDOWS
            string file = "log.xml";

            CXMLManip xml_data = new CXMLManip();

            xml_data.setXmlFilename(file);
            bool frm_file = xml_data.loadXmlFile();

            if (frm_file == true)
            {
                xml_data.xmlNodeStructure.addNewElement("T" + GetTime(), Tag + " : " + Message);

                xml_data.setXmlData(xml_data.xmlNodeStructure);
                xml_data.saveXmlFile(file);
            }
            else
            {
                XMLManip.XmlNodeStructure xml_node = new XMLManip.XmlNodeStructure();
                xml_node.xmlNodeName = "Logger";

                xml_node.addNewElement(Tag, Message);

                xml_data.setXmlData(xml_node);
                xml_data.saveXmlFile(file);
                xml_data = null;
            }
#endif
        }

        public void ResetErrorLog()
        {
#if ENGINE && WINDOWS
            string file = "error_log.xml";

            CXMLManip xml_data = new CXMLManip();

            XMLManip.XmlNodeStructure xml_node = new XMLManip.XmlNodeStructure();
            xml_node.xmlNodeName = "Logger";

            xml_node.addNewElement("First", "Entered Program at: " + System.DateTime.Now.Day + "/" + System.DateTime.Now.Month + "/" + System.DateTime.Now.Year + " - " + System.DateTime.Now.Hour + ":" + System.DateTime.Now.Minute + ":" + System.DateTime.Now.Second);

            xml_data.setXmlData(xml_node);
            xml_data.saveXmlFile(file);
            xml_data = null;
#endif
        }

        public void WriteToErrorLog(string Tag, string Message)
        {
#if ENGINE && WINDOWS
            System.Console.WriteLine("Error: " + Tag + " = " + Message);

            string file = "error_log.xml";

            CXMLManip xml_data = new CXMLManip();

            xml_data.setXmlFilename(file);
            bool frm_file = xml_data.loadXmlFile();

            if (frm_file == true)
            {
                xml_data.xmlNodeStructure.addNewElement("T" + GetTime(), Tag + " : " + Message);

                xml_data.setXmlData(xml_data.xmlNodeStructure);
                xml_data.saveXmlFile(file);
            }
            else
            {
                XMLManip.XmlNodeStructure xml_node = new XMLManip.XmlNodeStructure();
                xml_node.xmlNodeName = "Logger";

                xml_node.addNewElement(Tag, Message);

                xml_data.setXmlData(xml_node);
                xml_data.saveXmlFile(file);
                xml_data = null;
            }
#endif
        }

        public void ThreadRun()
        {
            System.Console.WriteLine("The CLogger has started.");

            ESharedLoggerType finish = ESharedLoggerType.e_nothing;

            do
            {
                SSharedMessage message = CSharedLoggerMessenger.Instance.ReadMessage();
                finish = message.s_messageType;
                switch (finish)
                {
                    case ESharedLoggerType.e_error:
                        WriteToErrorLog(message.s_tag, message.s_message);
                        break;

                    case ESharedLoggerType.e_finish:
                    case ESharedLoggerType.e_info:
                        WriteToLog(message.s_tag, message.s_message);
                        break;

                    case ESharedLoggerType.e_resetInfoLog:
                        ResetLog();
                        break;

                    case ESharedLoggerType.e_resetErrorLog:
                        ResetErrorLog();
                        break;

                    case ESharedLoggerType.e_nothing:
                        ResetLog();
                        ResetErrorLog();
                        break;

                    default:
                        // nothing
                        break;
                }
            } while (finish != ESharedLoggerType.e_finish);

            System.Console.WriteLine("The CLogger has finished and is exiting.");
        }
    }
}

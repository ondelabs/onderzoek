﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Onderzoek.Navigation
{
    public class CNavigationManager
    {

        public class CPathFindingNode
        {
            public CNavigationSector m_sector;
            public CPathFindingNode m_parentSector;
            public float m_costToThisSector;


            public CPathFindingNode(CNavigationSector thisSector, CPathFindingNode parentSector, float cost)
            {
                m_sector = thisSector;
                m_parentSector = parentSector;
                m_costToThisSector = cost;

            }

        }


        public class CNavigationSector
        {
            public Vector3[] m_cornerPoints;
            public string m_sectorName;

            Vector3 m_sectorCenterPoint;

            //the links to the other sectors.
            public List<CNavigationSectorLink> m_sectorLinks;

            public CNavigationSectorLink getLinkWithThisSector(CNavigationSector sec)
            {
                for(int i=0; i< m_sectorLinks.Count; i++)
                {
                    if (m_sectorLinks[i].m_sectorLinkedTo == sec)
                        return m_sectorLinks[i];
                }

                return null;


            }

            public CNavigationSector(Vector3 v1, Vector3 v2, Vector3 v3, Vector3 v4)
            {
                m_cornerPoints = new Vector3[4];
                m_cornerPoints[0] = v1;
                m_cornerPoints[1] = v2;
                m_cornerPoints[2] = v3;
                m_cornerPoints[3] = v4;

                m_sectorLinks = new List<CNavigationSectorLink>();
            }

            public Vector3 GetCenterPoint()
            {
                if (m_sectorCenterPoint == null || m_sectorCenterPoint == Vector3.Zero)
                {
                    m_sectorCenterPoint = (m_cornerPoints[0] + m_cornerPoints[1] + m_cornerPoints[2] + m_cornerPoints[3]) / 4;

                }

                return m_sectorCenterPoint;


            }


            public void CalculateCommonSegment(CNavigationSector otherSector, out Vector3 p1, out Vector3 p2)
            {
                p1 = Vector3.Zero;
                p2 = Vector3.Zero;

                int firstIndex = -1, secondIndex = -1;
                float minDistSoFar = 999999;

                for (int i = 0; i < 4; i++)
                    for (int j = 0; j < 4; j++)
                    {
                        Vector3 v1 = m_cornerPoints[i] - m_cornerPoints[(i + 1) % 4];
                        Vector3 v2 = otherSector.m_cornerPoints[j] - otherSector.m_cornerPoints[(j + 1) % 4];
                        v1.Normalize();
                        v2.Normalize();

                        if (Math.Acos(Vector3.Dot(v1, v2)) < MathHelper.PiOver2 / 3 || Math.Acos(Vector3.Dot(v1, v2)) > MathHelper.Pi - MathHelper.PiOver2 / 3) // if the angle between them is less than 30 degrees
                        {
                            if (minDistSoFar > MathHelper.Min((m_cornerPoints[i] - otherSector.m_cornerPoints[j]).Length() + (m_cornerPoints[(i + 1) % 4] - otherSector.m_cornerPoints[(j + 1) % 4]).Length(), (m_cornerPoints[(i + 1) % 4] - otherSector.m_cornerPoints[j]).Length() + (m_cornerPoints[i] - otherSector.m_cornerPoints[(j + 1) % 4]).Length()))
                            {
                                minDistSoFar = MathHelper.Min((m_cornerPoints[i] - otherSector.m_cornerPoints[j]).Length() + (m_cornerPoints[(i + 1) % 4] - otherSector.m_cornerPoints[(j + 1) % 4]).Length(), (m_cornerPoints[(i + 1) % 4] - otherSector.m_cornerPoints[j]).Length() + (m_cornerPoints[i] - otherSector.m_cornerPoints[(j + 1) % 4]).Length());
                                firstIndex = i;
                                secondIndex = j;
                            }
                        }

                    }

                if(firstIndex == -1)
                {
                    throw new System.Exception("You have connected 2 nav quads that dont seem to have any segments with a <30 degree angle between them");
                }

                float selfDistance1, selfDistance2;

                selfDistance1 = (m_cornerPoints[firstIndex] - m_cornerPoints[(firstIndex+1)%4]).Length() ;
                selfDistance2 = (otherSector.m_cornerPoints[secondIndex] - otherSector.m_cornerPoints[(secondIndex + 1) % 4]).Length() ;

                //List<int> outsidePoints = new List<int>(); 
                

                bool isF1Bad = false, isF2Bad = false, isS1Bad = false, isS2Bad = false;

                if (MathHelper.Max((m_cornerPoints[firstIndex] - otherSector.m_cornerPoints[secondIndex]).Length(), (m_cornerPoints[firstIndex] - otherSector.m_cornerPoints[(secondIndex + 1) % 4]).Length()) > selfDistance2)
                    isF1Bad = true;

                if (MathHelper.Max((m_cornerPoints[(firstIndex + 1) % 4] - otherSector.m_cornerPoints[secondIndex]).Length(), (m_cornerPoints[(firstIndex + 1) % 4] - otherSector.m_cornerPoints[(secondIndex + 1) % 4]).Length()) > selfDistance2)
                    isF2Bad = true;

                if (MathHelper.Max((otherSector.m_cornerPoints[secondIndex] - m_cornerPoints[firstIndex]).Length(), (otherSector.m_cornerPoints[secondIndex] - m_cornerPoints[(firstIndex + 1) % 4]).Length()) > selfDistance1)
                    isS1Bad = true;

                if (MathHelper.Max((otherSector.m_cornerPoints[(secondIndex + 1) % 4] - m_cornerPoints[firstIndex]).Length(), (otherSector.m_cornerPoints[(secondIndex + 1) % 4] - m_cornerPoints[(firstIndex + 1) % 4]).Length()) > selfDistance1)
                    isS2Bad = true;

                

                if(isF1Bad && isF2Bad)
                {
                    p1 = otherSector.m_cornerPoints[secondIndex];
                    p2 = otherSector.m_cornerPoints[(secondIndex + 1) % 4];
                }

                if(isS1Bad && isS2Bad)
                {
                    p1 = m_cornerPoints[firstIndex];
                    p2 = m_cornerPoints[(firstIndex+1)%4];
                }

                if(isF1Bad && isS2Bad)
                {
                    p1 = otherSector.m_cornerPoints[secondIndex];
                    p2 = m_cornerPoints[(firstIndex+1)%4];
                }

                if(isS1Bad && isF2Bad)
                {
                    p1 = m_cornerPoints[firstIndex];
                    p2 = otherSector.m_cornerPoints[(secondIndex + 1) % 4];
                }

                if (isF1Bad && isS1Bad)
                {
                    p1 = m_cornerPoints[(firstIndex+ 1) % 4];
                    p2 = otherSector.m_cornerPoints[(secondIndex + 1) % 4];
                }

                if (isF2Bad && isS2Bad)
                {
                    p1 = m_cornerPoints[firstIndex ];
                    p2 = otherSector.m_cornerPoints[secondIndex];
                }



            }


            //any additional data about this sector can eb kept here.
        }

        /// <summary>
        /// a class that defines the link between 2 sectors
        /// </summary>
        public class CNavigationSectorLink
        {
            public CNavigationSector m_sectorLinkedTo;

            public Vector3 m_connectingPoint1, m_connectingPoint2; //these 2 points are shared between both sectors and as such you can pas between them anywehre between these 2 points.
            public Vector3 m_linkMidPoint;

            public CNavigationSectorLink(CNavigationSector link, Vector3 sharedPt1, Vector3 sharedPt2)
            {
                m_sectorLinkedTo = link;
                m_connectingPoint1 = sharedPt1;
                m_connectingPoint2 = sharedPt2;
            }

            public Vector3 getLinkMidPoint()
            {
                if (m_linkMidPoint == null || m_linkMidPoint == Vector3.Zero)
                {
                    m_linkMidPoint = (m_connectingPoint1 + m_connectingPoint2) / 2;


                }

                return m_linkMidPoint;
            }

        }


        public CNavigationManager()
        {
            m_sectorList = new List<CNavigationSector>();
        }


        
        public List<CNavigationSector> m_sectorList;

        static CNavigationManager m_instance;


        public void clearManager()
        {
            m_sectorList.Clear();
        }


        public static CNavigationManager Instance
        {
            get 
            {
                if (m_instance != null)
                    return m_instance;

                m_instance = new CNavigationManager();
                return m_instance;
            
            }
        }


        public CNavigationSector FindClosestSector(Vector3 vec)
        {
            float minDistance = 999999, secondaryMinDist = 99999;
            int minIndex = -1, minCornerIndex;

            float breakOutThreshold = 5;

            for (int i = 0; i < m_sectorList.Count; i++)
            {
                float localMaxSoFar = 99990, localSecondSoFar = 999999;

                for (int j = 0; j < 4; j++)
                {
                    float localSecDist1 = 0, localSecDist2 = 99999;

                    float curDist = CToolbox.DistanceBetweenLineSegToPoint(m_sectorList[i].m_cornerPoints[j], m_sectorList[i].m_cornerPoints[(j + 1) % 4], vec);

                    if (curDist < localMaxSoFar)
                    {
                        localSecondSoFar = localMaxSoFar;
                        localMaxSoFar = curDist;
                    }
                    else
                    {
                        if (curDist < localSecondSoFar)
                            localSecondSoFar = curDist;
                    }
                }

            
                if (localMaxSoFar < minDistance + 1) //-1 is a small buffer for badly lapping sectors. maybe we should clear it up in preprocessing?
                {
                    if (localMaxSoFar > minDistance - 1)
                    {
                        //buffer check. check secondaries.
                        if (localSecondSoFar < secondaryMinDist)
                        {
                            minDistance = localMaxSoFar;
                            secondaryMinDist = localSecondSoFar;
                            minIndex = i;
                        }
                    }
                    else
                    {
                        minDistance = localMaxSoFar;
                        secondaryMinDist = localSecondSoFar;
                        minIndex = i;
                        
                    }
                }
            }

            return m_sectorList[minIndex];

        }


        

        public float getCost(CNavigationSector sector1, CNavigationSector sector2)
        {
            int index = -1;
            int i;

            if (sector1 == sector2)
                return 0;

            for (i = 0; i < sector1.m_sectorLinks.Count; i++)
            {
                if (sector1.m_sectorLinks[i].m_sectorLinkedTo == sector2)
                {
                    index = i;

                }
            }

            if (index == -1)
            {
                return (sector1.GetCenterPoint() - sector2.GetCenterPoint()).Length();


            }



            Vector3 midPt = (sector1.m_sectorLinks[index].m_connectingPoint1 + sector1.m_sectorLinks[index].m_connectingPoint2) / 2;
            //need to store the midpoint of a link and the center of the sector for quick evaluation.

            return (sector1.GetCenterPoint() - midPt).Length() + (sector2.GetCenterPoint() - midPt).Length();

        }

        public List<CNavigationSector> EvaluateThisNode(CPathFindingNode currentNode, List<CPathFindingNode> openList, List<CPathFindingNode> closedList, CNavigationSector targetSector)
        {


            if (currentNode.m_sector == targetSector)
            {
                List<CNavigationSector> finalList = new List<CNavigationSector>();
                finalList.Add(currentNode.m_sector);

                while (currentNode.m_parentSector != currentNode && currentNode.m_parentSector != null)
                {
                    finalList.Add(currentNode.m_parentSector.m_sector);
                    currentNode = currentNode.m_parentSector;
                }

                return finalList;
            }


            for (int i = 0; i < currentNode.m_sector.m_sectorLinks.Count; i++)
            {
                int j;
                for (j = 0; j < closedList.Count; j++)
                {
                    if (currentNode.m_sector.m_sectorLinks[i].m_sectorLinkedTo == closedList[j].m_sector)
                    {
                        break;
                    }
                }

                if (j == closedList.Count) //wasnt in the closed list.
                {
                    openList.Add(new CPathFindingNode(currentNode.m_sector.m_sectorLinks[i].m_sectorLinkedTo, currentNode, currentNode.m_costToThisSector + getCost(currentNode.m_sector, currentNode.m_sector.m_sectorLinks[i].m_sectorLinkedTo)));
                }
            }


            if (openList.Count == 0)
            {
                throw new System.Exception("wtf?");
            }

            float minSoFar = 9999;
            int minIndex = -1;
            for (int i = 0; i < openList.Count; i++)
            {
                float dist= openList[i].m_costToThisSector + getCost( openList[i].m_sector, targetSector);
                if (dist < minSoFar)
                {
                    minSoFar = dist;
                    minIndex = i;
                }
            }


            CPathFindingNode newCurrent = openList[minIndex];
            openList.RemoveAt(minIndex);
            closedList.Add(newCurrent);

            

            return EvaluateThisNode(newCurrent, openList, closedList, targetSector);




        }

        public List<CNavigationSector> CalculateSectorList(CNavigationSector sector1, CNavigationSector sector2)
        {
            List<CPathFindingNode> openList = new List<CPathFindingNode>();
            List<CPathFindingNode> closedList = new List<CPathFindingNode>();

            //openList.Add(new CPathFindingNode(sector1, sector1, 0);

            return EvaluateThisNode(new CPathFindingNode(sector1, null, 0), openList, closedList, sector2);




        }

        public bool CalculatePathBetweenPoints(Vector3 point1, Vector3 point2, out List<Vector3> moveList)
        {
            CNavigationSector startSect = FindClosestSector(point1);
            CNavigationSector endSect = FindClosestSector(point2);

            moveList = new List<Vector3>();

            List<CNavigationSector> list = CalculateSectorList(startSect, endSect);

            moveList.Add(point2);

            for (int i = 0; i < list.Count-1; i++)
            {
                float d1, d2;
                if (i != list.Count - 2)
                {
                    d1 = (moveList[i] - list[i].getLinkWithThisSector(list[i + 1]).m_connectingPoint1).Length() + (list[i + 1].getLinkWithThisSector(list[i + 2]).getLinkMidPoint() - list[i].getLinkWithThisSector(list[i + 1]).m_connectingPoint1).Length();
                    d2 = (moveList[i] - list[i].getLinkWithThisSector(list[i + 1]).m_connectingPoint2).Length() + (list[i + 1].getLinkWithThisSector(list[i + 2]).getLinkMidPoint() - list[i].getLinkWithThisSector(list[i + 1]).m_connectingPoint2).Length();
                }
                else
                {
                    d1 = (moveList[i] - list[i].getLinkWithThisSector(list[i + 1]).m_connectingPoint1).Length() + (point1 - list[i].getLinkWithThisSector(list[i + 1]).m_connectingPoint1).Length();
                    d2 = (moveList[i] - list[i].getLinkWithThisSector(list[i + 1]).m_connectingPoint2).Length() + (point1 - list[i].getLinkWithThisSector(list[i + 1]).m_connectingPoint2).Length();

                }

                if (d1 > d2)
                {
                    moveList.Add(list[i].getLinkWithThisSector(list[i+1]).m_connectingPoint2);
                }
                else
                    moveList.Add(list[i].getLinkWithThisSector(list[i+1]).m_connectingPoint1);

            }

            moveList.Add(point1);


            //post process
            int removedSoFar = 0;

            for (int i = 0; i < moveList.Count - 2 ; i++)
            {
                CNavigationSectorLink currentLink = list[i - removedSoFar].getLinkWithThisSector(list[i + 1 - removedSoFar]);
                //if (i == moveList.Count - 2)
                //{
                //    float distToMidPt = CToolbox.DistanceBetweenLineSegToPoint(moveList[i], moveList[i + 2], (currentLink.m_connectingPoint1 + currentLink.m_connectingPoint2) / 2);
                //    if (distToMidPt < (currentLink.m_connectingPoint1 - currentLink.m_connectingPoint2).Length() / 2)
                //    {
                //        //this piont is redundent. yay. (i+1)
                //        moveList.RemoveAt(i + 1);
                //        removedSoFar++;
                //    }



                //}
                //else
                {

                    float distToMidPt = CToolbox.DistanceBetweenLineSegToPoint(moveList[i - removedSoFar], moveList[i + 2 - removedSoFar], (currentLink.m_connectingPoint1 + currentLink.m_connectingPoint2) / 2);
                    if (distToMidPt < (currentLink.m_connectingPoint1 - currentLink.m_connectingPoint2).Length() / 2)
                    {
                        //this piont is redundent. yay. (i+1)
                        moveList.RemoveAt(i + 1);
                        //list.RemoveAt(i + 1);
                        
                        removedSoFar++;
                    }
                }



            }
            //for (int i = 0; i < list.Count; i++)
            //{

            //    moveList.Add(list[i].GetCenterPoint());
            //}

            return true;




            




        }



    }
}

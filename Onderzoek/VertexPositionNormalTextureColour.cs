using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Onderzoek
{
    public struct VertexPositionNormalTextureColour
    {
        private Vector3 m_position;
        private Vector3 m_normal;
        private Vector2 m_texture;
        private Color m_colour;

        public Vector3 GetPosition()
        {
            return m_position;
        }

        public Vector3 GetNormal()
        {
            return m_normal;
        }

        public void SetNormal(Vector3 Normal)
        {
            m_normal = Normal;
        }

        public void SetUV(Vector2 UV)
        {
            m_texture = UV;
        }

        public VertexPositionNormalTextureColour(Vector3 Position, Vector3 Normal, Vector2 Texture, Color Colour)
        {
            this.m_position = Position;
            this.m_normal = Normal;
            this.m_texture = Texture;
            this.m_colour = Colour;
        }

        public static VertexElement[] VertexElements =
        {
            new VertexElement(0, 0, VertexElementFormat.Vector3, VertexElementMethod.Default, VertexElementUsage.Position, 0),
            new VertexElement(0, sizeof(float)*3, VertexElementFormat.Vector3, VertexElementMethod.Default, VertexElementUsage.Normal, 0),
            new VertexElement(0, sizeof(float)*6, VertexElementFormat.Vector2, VertexElementMethod.Default, VertexElementUsage.TextureCoordinate, 0),
            new VertexElement(0, sizeof(float)*8, VertexElementFormat.Vector4, VertexElementMethod.Default, VertexElementUsage.Color, 0)
        };
        public static int SizeInBytes = (sizeof(float) * 6) + (sizeof(float) * 2) + (sizeof(float) * 4);
    }
}

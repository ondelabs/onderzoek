﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Onderzoek.ModelData
{
    public class CNavQuadModel : CModel
    {
        public override void Delete()
        {
            m_indexBuffer.Dispose();
            m_vertexBuffer.Dispose();
            m_vertexDeclaration.Dispose();
            m_vertices = null;
        }

        public VertexPositionColor[] m_vertices;
        public int[] m_indices;

        public DynamicVertexBuffer m_vertexBuffer;
        public IndexBuffer m_indexBuffer;
        public VertexDeclaration m_vertexDeclaration;
        public int m_numVerts;
        public int m_numIndices;
        public int m_numTriangles;




        public void InitializeForQuad(Vector2 size, GraphicsDevice device)
        {

            m_vertexDeclaration = new VertexDeclaration(device, VertexPositionColor.VertexElements);
            m_numTriangles = 2;
            m_numVerts = 4;
            m_numIndices = 6;

            m_bb = new List<CModelOBB>();
            CModelOBB obb = new CModelOBB();
            obb.m_position = Vector3.Zero;
            obb.m_rotation = Vector3.Zero;
            obb.m_scale = Vector3.One;
            m_bb.Add(obb);


            m_vertices = new VertexPositionColor[m_numVerts];


            m_vertexBuffer = new DynamicVertexBuffer(device, m_numVerts * VertexPositionColor.SizeInBytes, BufferUsage.WriteOnly);


            m_vertices[0].Position = new Vector3(size.X / 2, 0, size.Y / 2);
            m_vertices[0].Color = Color.Green;

            m_vertices[1].Position = new Vector3(size.X / 2, 0, -size.Y / 2);
            m_vertices[1].Color = Color.Green;

            m_vertices[2].Position = new Vector3(-size.X / 2, 0, -size.Y / 2);
            m_vertices[2].Color = Color.Green;

            m_vertices[3].Position = new Vector3(-size.X / 2, 0, size.Y / 2);
            m_vertices[3].Color = Color.Green;

            m_vertexBuffer.SetData(m_vertices);


            m_indices = new int[6];

            m_indices[0] = 3;
            m_indices[1] = 1;
            m_indices[2] = 0;
            m_indices[3] = 3;
            m_indices[4] = 2;
            m_indices[5] = 1;


            m_indexBuffer = new IndexBuffer(device, typeof(int), m_numIndices, BufferUsage.WriteOnly);
            m_indexBuffer.SetData(m_indices, 0, m_indices.Length);


        }



    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using Onderzoek.ModelData;
using Onderzoek.Entity;
using Onderzoek.Visual;

using XMLManip;

using XNAnimation;
using XNAnimation.Controllers;
using XNAnimation.Effects;

namespace Onderzoek.ModelData
{
    public class CAnimatedModel : CModel
    {
        private SkinnedModel m_skinnedModel;
        private AnimationController m_animationController;

        public SkinnedModel GetSkinnedModelData()
        {
            return m_skinnedModel;
        }

        public AnimationController GetAnimationControllerData()
        {
            return m_animationController;
        }

#if ENGINE
        public virtual CModel Initialise(ContentManager Content, string ModelName, Dictionary<string, CModel.SModelData> ModelList)
        {
            CModel instance = base.Initialise(ModelList, ModelName);
            //if (instance != null)
            //{
            //    return instance;
            //}
#else
        public virtual void Initialise(ContentManager Content, string ModelName)
        {
#endif
            m_modelName = ModelName;

            m_bb = new List<CModelOBB>();

            m_skinnedModel = Content.Load<SkinnedModel>(ModelName);
            m_animationController = new AnimationController(m_skinnedModel.SkeletonBones);
            
            GetBoundingBoxFromModel(ModelName);

#if ENGINE
            return this;
//#else
//            return true;
#endif
        }

        public virtual void GetBoundingBoxFromModel(string ModelName)
        {
            string file = "Content/" + ModelName.Replace("AnimatedModel", "xml/AnimatedModel") + ".xml";

            //CXMLManip xml_data = new CXMLManip();

            //xml_data.setXmlFilename(file;)
            //bool frm_file = xml_data.loadXmlFile();

            if (CBBLoader.Instance != null && CBBLoader.Instance.loadBBFromXML(file, ref m_bb) == false)
            {
                BoundingBox boundingBox = new BoundingBox();

                Matrix[] transforms = new Matrix[m_skinnedModel.Model.Bones.Count];
                m_skinnedModel.Model.CopyAbsoluteBoneTransformsTo(transforms);

                foreach (ModelMesh mesh in m_skinnedModel.Model.Meshes)
                {
                    VertexPositionNormalTexture[] vertices = new VertexPositionNormalTexture[mesh.VertexBuffer.SizeInBytes / VertexPositionNormalTexture.SizeInBytes];

                    if (mesh.IndexBuffer.IndexElementSize == IndexElementSize.SixteenBits)
                    {
                        short[] indices = new short[mesh.IndexBuffer.SizeInBytes / 2];

                        mesh.VertexBuffer.GetData<VertexPositionNormalTexture>(vertices);
                        mesh.IndexBuffer.GetData<short>(indices);

                        Vector3[] vertexs = new Vector3[vertices.Length];

                        for (int index = 0; index < indices.Length; ++index)
                        {
                            vertexs[indices[index]] = Vector3.Transform(vertices[indices[index]].GetPosition(), transforms[mesh.ParentBone.Index]);
                        }

                        boundingBox = BoundingBox.CreateFromPoints(vertexs);

                        CModelOBB temp = new CModelOBB();
                        temp.m_OBB = boundingBox.GetCorners();
                        m_bb.Add(temp);
                    }
                    else
                    {
                        CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CAnimatedModel", "ONDERZOEK ENGINE ERROR: SizeInBytes is not equal to 16bits. Fix please.");
                    }
                }
            }

            //Vector3 amin = new Vector3(-5.92367f, -6.037843f, -6.037843f);
            //Vector3 amax = new Vector3(5.92367f, 6.037843f, 6.037843f);
            //Matrix trans = Matrix.CreateTranslation(new Vector3(0, 0.8866f, 0));

            //amin = Vector3.Transform(amin, trans);
            //amax = Vector3.Transform(amax, trans);

            //BoundingBox temp = new BoundingBox(amin, amax);
            //Vector3[] corners = temp.GetCorners();

            //string file1 = "Content/" + ModelName.Replace("Model", "xml") + "_01" + ".xml";

            //XmlNodeStructure test = new XmlNodeStructure();
            //test.xmlNodeName = "PhysicsBB";
            ////for (int i = 0; i < m_OBB.Count; ++i)
            //{
            //    for (int j = 0; j < corners.Length; ++j)
            //    {
            //        test.addNewElement("BB" + 0, corners[j].ToString());
            //    }
            //}

            //CXMLManip write_test = new CXMLManip();
            //write_test.setXmlData(test);
            //write_test.saveXmlFile(file1);
        }

        public override List<CMaterial> GetMaterials(ContentManager Content, string ModelName)
        {
            List<CMaterial> materials = new List<CMaterial>();

            int count = 0;
            foreach (ModelMesh mesh in m_skinnedModel.Model.Meshes)
            {
                foreach (SkinnedModelBasicEffect effect in mesh.Effects)
                {
                    materials.Add(new CMaterial());

                    materials[count].m_diffuse = new Vector4(effect.Material.DiffuseColor, 1.0f);
                    materials[count].m_ambient = new Vector4(effect.AmbientLightColor, 1.0f);
                    materials[count].m_emissive = new Vector4(effect.Material.EmissiveColor, 1.0f);
                    materials[count].m_specularColour = new Vector4(effect.Material.SpecularColor, 1.0f);
                    materials[count].m_specularPower = effect.Material.SpecularPower;

                    if (effect.DiffuseMapEnabled == true)
                    {
                        materials[count].m_textureID = CVisual.Instance.LoadLevelTexture(ModelName + count, Content, effect.DiffuseMap);
                    }
                    if (effect.NormalMapEnabled == true)
                    {
                        materials[count].m_normalID = CVisual.Instance.LoadLevelTexture(ModelName + count, Content, effect.NormalMap);
                    }
                    if (effect.SpecularMapEnabled == true)
                    {
                        materials[count].m_specularID = CVisual.Instance.LoadLevelTexture(ModelName + count, Content, effect.SpecularMap);
                    }

                    ++count;
                }
            }

            return materials;
        }

        public override Effect GetMaterialAtIndex(int Index)
        {
            int count = 0;
            foreach (ModelMesh mesh in m_skinnedModel.Model.Meshes)
            {
                foreach (SkinnedModelBasicEffect effect in mesh.Effects)
                {
                    if (count == Index)
                    {
                        return effect;
                    }

                    ++count;
                }
            }

            return null;
        }

        public override Model getBaseModel()
        {
            return m_skinnedModel.Model;
            //return base.getBaseModel();
        }
    }
    
}

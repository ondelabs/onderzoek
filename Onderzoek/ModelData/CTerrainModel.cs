﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using Onderzoek.Physics;

using System.IO;

namespace Onderzoek.ModelData
{
    public class CTerrainModel : CCustomModel
    {
        public CTerrainModel()
        {
            m_bb = new List<CModelOBB>();

            m_numXVerts = 0;
            m_numYVerts = 0;
            m_numVerts = 0;
            m_numIndices = 0;
            m_numTriangles = 0;
            m_lengthX = 0;
            m_lengthY = 0;
            m_splitX = 0;
            m_splitY = 0;

            m_arrayIndices = new List<int[]>();
        }

        float GetBilinearFilteredHeight(Color[] FromTex, float CurX, float CurY, float StepX, float StepY, int MaxX, int MaxY, uint MaxHeight)
        {
            if ((CurX - StepX) > 0 && (CurX + StepX) < MaxX &&
                (CurY - StepY) > 0 && (CurY + StepY) < MaxY)
            {
                // Centre
                int centre_position = (int)((CurY * MaxX) + CurX);
                // up_Left
                int up_left_position = (int)(((CurY - StepY) * MaxX) + (CurX - StepX));
                // Left
                int left_position = (int)((CurY * MaxX) + (CurX - StepX));
                // up_right
                int up_right_position = (int)(((CurY - StepY) * MaxX) + (CurX + StepX));
                // down_Left
                int down_left_position = (int)(((CurY + StepY) * MaxX) + (CurX - StepX));
                // Right
                int right_position = (int)((CurY * MaxX) + (CurX + StepX));
                // down_right
                int down_right_position = (int)(((CurY + StepY) * MaxX) + (CurX + StepX));
                // Up
                int up_position = (int)(((CurY - StepY) * MaxX) + CurX);
                // Down
                int down_position = (int)(((CurY + StepY) * MaxX) + StepX);


                return ((FromTex[centre_position].R + FromTex[up_left_position].R + FromTex[left_position].R +
                    FromTex[up_right_position].R + FromTex[right_position].R + FromTex[down_left_position].R +
                    FromTex[down_right_position].R + FromTex[up_position].R + FromTex[down_position].R)
                    / (255.0f * 9.0f)) * MaxHeight;
            }
            else
            {
                return (FromTex[(int)((CurY * MaxX) + CurX)].R / 255.0f) * MaxHeight;
            }
        }

#if ENGINE
        public bool LoadAndCreateTerrain(Texture2D HeightMap, GraphicsDevice Device, Vector2 VertLength, Vector2 NumVerts, uint MaxHeight, Vector2 Split,
            Dictionary<string, CModel.SModelData> ModelList, string ModelName)
        {
            if (ModelList.ContainsKey(ModelName) == false)
            {
                ModelList.Add(ModelName, new CModel.SModelData(this));
            }
#else
        public bool LoadAndCreateTerrain(Texture2D HeightMap, GraphicsDevice Device, Vector2 VertLength, Vector2 NumVerts, uint MaxHeight, Vector2 Split)
        {
#endif
            m_vertexDeclaration = new VertexDeclaration(Device, VertexPositionNormalTexture.VertexElements);

            int tex_width = HeightMap.Width;
            int tex_height = HeightMap.Height;

            // Extracts the colour data from the heightmap texture
            Color[] frm_tex = new Color[HeightMap.Height * HeightMap.Width];
            HeightMap.GetData<Color>(frm_tex);

            m_lengthX = (int)VertLength.X;
            m_lengthY = (int)VertLength.Y;
            m_numXVerts = (int)NumVerts.X;
            m_numYVerts = (int)NumVerts.Y;
            m_numVerts = m_numYVerts * m_numXVerts;
            m_numTriangles = ((m_numXVerts - 1) * 2) * (m_numYVerts - 1);
            m_numIndices = m_numTriangles * 3;
            m_splitX = (int)Split.X;
            m_splitY = (int)Split.Y;

            VertexPositionNormalTexture[] vertices = new VertexPositionNormalTexture[m_numVerts];
            m_heightVertices = new Vector3[m_numVerts];
            Vector3[] obb_vertices = new Vector3[m_numVerts];

            int vert_count = 0;
            float z = (float)m_lengthY / -2;
            float x_step = (float)m_lengthX / (float)m_numXVerts;
            float y_step = (float)m_lengthY / (float)m_numYVerts;

            // These variables are used to get the index value from the texture array.
            float index_x_step = (float)tex_width / (float)m_numXVerts;
            float index_y_step = (float)tex_height / (float)m_numYVerts;
            float index_z = 0;

            for (int i = 0; i < m_numYVerts; ++i)
            {
                float x = (float)m_lengthX / -2.0f; // For the vertex x position
                float index_x = 0;// For the x position to get the index
                for (int j = 0; j < m_numXVerts; ++j)
                {
                    float height = GetBilinearFilteredHeight(frm_tex, index_x, index_z, index_x_step, index_y_step,
                        tex_width, tex_height, MaxHeight);

                    //int position = ((int)index_z * tex_width) + (int)index_x;
                    index_x += index_x_step;

                    //float height = ((frm_tex[position].R + frm_tex[position].G + frm_tex[position].B) / (255.0f * 3)) * MaxHeight;

                    // Storing the necessary values in the vertex buffer
                    vertices[vert_count] = new VertexPositionNormalTexture(new Vector3(x, height, z), new Vector3(0, 0, 0),
                        new Vector2((float)j / (float)m_numXVerts, (float)i / (float)m_numYVerts));
                    m_heightVertices[vert_count] = new Vector3(x, height, z);
                    // Storing all the values in a seperate array to help create an OBB
                    obb_vertices[vert_count] = new Vector3(x, height, z);

                    ++vert_count;
                    x += x_step;
                }
                z += y_step; // For the vertex z position
                index_z += index_y_step; // For the z position to get the index
            }

            // Create the AABB/OBB for this terrain instance
            BoundingBox bb = new BoundingBox(); ;
            bb = BoundingBox.CreateMerged(bb, BoundingBox.CreateFromPoints(obb_vertices));

            CModelOBB temp = new CModelOBB();
            temp.m_OBB = bb.GetCorners();
            m_bb.Add( temp );

            // Creating the index buffer
            m_indices = new int[m_numIndices];

            // Filling the index buffer
            int index_value = 0;
            int index_count = 0;
            int num_y_vert = m_numYVerts;
            int num_x_vert = m_numXVerts;

            for (int y = 0; y < m_numYVerts - 1; ++y)
            {
                for (int x = 0; x < m_numXVerts - 1; ++x)
                {
                    m_indices[index_count] = index_value;
                    m_indices[index_count += 1] = (index_value + (num_x_vert + 1));
                    m_indices[index_count += 1] = (index_value + num_x_vert);
                    m_indices[index_count += 1] = index_value;
                    m_indices[index_count += 1] = (index_value + 1);
                    m_indices[index_count += 1] = (index_value + (num_x_vert + 1));

                    ++index_count;
                    ++index_value;
                }
                ++index_value;
            }

            // Creating the right normals
            for (index_count = 0; index_count < m_numIndices; index_count += 3)
            {
                Vector3 vert0 = vertices[m_indices[index_count]].GetPosition();
                Vector3 vert1 = vertices[m_indices[index_count + 1]].GetPosition();
                Vector3 vert2 = vertices[m_indices[index_count + 2]].GetPosition();

                Vector3 dir_a = vert0 - vert1;
                Vector3 dir_b = vert0 - vert2;
                Vector3 face_normal = CToolbox.CrossProduct(dir_b, dir_a);
                //face_normal = CToolbox.UnitVector(face_normal);

                Vector3 normal0 = vertices[m_indices[index_count]].GetNormal();
                Vector3 normal1 = vertices[m_indices[index_count + 1]].GetNormal();
                Vector3 normal2 = vertices[m_indices[index_count + 2]].GetNormal();

                vertices[m_indices[index_count]].SetNormal(normal0 + face_normal);
                vertices[m_indices[index_count + 1]].SetNormal(normal1 + face_normal);
                vertices[m_indices[index_count + 2]].SetNormal(normal2 + face_normal);
            }

            // Normalise the normals of all the vertices
            for (int vertex_count = 0; vertex_count < m_numVerts; ++vertex_count)
            {
                Vector3 new_normal = vertices[vertex_count].GetNormal();

                vertices[vertex_count].SetNormal(CToolbox.UnitVector(new_normal));
            }

            // Make sure there aren't the same or more amounts of splits and vertices; in their repective axis.
            if (Split.X >= NumVerts.X)
            {
                --Split.X;
            }
            if (Split.Y >= NumVerts.Y)
            {
                --Split.Y;
            }

            // Find the split vertex count in the x and y. make sure that it isn't 1. If it is 1 then change to 2 and
            // recalculate the new split values.
            int num_vert_split_x = (int)((NumVerts.X + (Split.X - 1.0f)) / Split.X);
            if (num_vert_split_x <= 1)
            {
                num_vert_split_x = 2;
                int new_split_x = (int)((NumVerts.X + 1.0f) / num_vert_split_x);
                Split.X = new_split_x;
            }
            int num_vert_split_y = (int)((NumVerts.Y + (Split.Y - 1.0f)) / Split.Y);
            if (num_vert_split_y <= 1)
            {
                num_vert_split_y = 2;
                int new_split_y = (int)((NumVerts.Y + 1.0f) / num_vert_split_y);
                Split.Y = new_split_y;
            }

            // There maybe a row and/or column of vertices which hasn't been accounted for. This next two lines finds out
            // if there are any unaccounted for vertices.
            int temp_split_x = (int)Split.X;
            int left_over_x = GetLeftOver((int)NumVerts.X, num_vert_split_x, ref temp_split_x);
            Split.X = (float)temp_split_x;

            int temp_split_y = (int)Split.Y;
            int left_over_y = GetLeftOver((int)NumVerts.Y, num_vert_split_y, ref temp_split_y);
            Split.Y = (float)temp_split_y;

            int saved_left_over_y = left_over_y;

            int left_over_tri = 0;
            int left_over_ind = 0;
            int left_over_tri_x = 0;
            int left_over_ind_x = 0;
            int left_over_tri_y = 0;
            int left_over_ind_y = 0;

            if (left_over_x > 0 || left_over_y > 0)
            {
                left_over_y = num_vert_split_y;
                left_over_tri = ((left_over_x - 1) * 2) * (left_over_y - 1);
                left_over_ind = left_over_tri * 3;
                left_over_tri_x = ((left_over_x - 1) * 2);
                left_over_ind_x = left_over_tri_x * 3;
                left_over_tri_y = ((left_over_y - 1) * 2);
                left_over_ind_y = left_over_tri_y * 3;
            }

            int num_tri_split = ((num_vert_split_x - 1) * 2) * (num_vert_split_y - 1);
            int num_ind_split = num_tri_split * 3;
            int num_tri_split_x = ((num_vert_split_x - 1) * 2);
            int num_ind_split_x = num_tri_split_x * 3;
            int num_tri_split_y = ((num_vert_split_y - 1) * 2);
            int num_ind_split_y = num_tri_split_y * 3;

            int total_split_ind_x = (((int)NumVerts.X - 1) * 2);
            total_split_ind_x *= 3;

            int total_split_ind_y = (((int)NumVerts.Y - 1) * 2);
            total_split_ind_y *= 3;

            int total_split = 0;
            int total_split_x = 0;
            {
                total_split = (int)Split.X * (int)Split.Y;
                total_split_x = (int)Split.X;
            }

            m_numTri = new int[total_split];

            int ind_count = 0;
            int i_counter_again = 0;
            bool using_left = false;
            int num_y_split_done = 0;
            int nxt_ind_count = 0;
            int x_amount = 0;
            int y_amount = 0;
            int start_last_column = (int)((Split.Y - 1.0f) * Split.X);
            for (int i = 0; i < total_split; ++i)
            {
                int num_split = 0;
                int num_ind_x = 0;
                if (i == start_last_column)
                {
                    num_tri_split += ((num_vert_split_x - 1) * 2) * (saved_left_over_y - 1);
                    num_ind_split += ((((num_vert_split_x - 1) * 2) * (saved_left_over_y - 1))) * 3;
                    num_vert_split_y += saved_left_over_y;

                    left_over_tri += ((left_over_x - 1) * 2) * (saved_left_over_y - 1);
                    left_over_ind += ((((left_over_x - 1) * 2) * (saved_left_over_y - 1))) * 3;
                }
                if (i_counter_again == Split.X - 1.0f)
                {
                    i_counter_again = -1;
                    ++num_y_split_done;
                    num_split = num_ind_split + left_over_ind;
                    num_ind_x = num_ind_split_x + left_over_ind_x;
                    using_left = true;
                    m_numTri[i] = num_tri_split + left_over_tri;
                }
                else
                {
                    num_split = num_ind_split;
                    num_ind_x = num_ind_split_x;
                    using_left = false;
                    m_numTri[i] = num_tri_split;
                }
                int[] temp_ind = new int[num_split];
                int x_count = 1;
                for (int k = 0; k < num_split; ++k)
                {
                    temp_ind[k] = m_indices[ind_count];
                    ++ind_count;

                    if (k + 1 == num_ind_x * x_count)
                    {
                        if (using_left == true)
                        {
                            ind_count += total_split_ind_x - (num_ind_split_x + left_over_ind_x);
                        }
                        else
                        {
                            ind_count += total_split_ind_x - num_ind_split_x;
                        }
                        ++x_count;
                    }
                }

                ++x_amount;
                int by_y = 0;
                int by_x = 0;
                if (x_amount == total_split_x)
                {
                    x_amount = 0;
                    ++y_amount;
                    by_y = ((y_amount * (num_vert_split_y - 1)) * total_split_ind_x);
                    nxt_ind_count = 0;
                }
                else
                {
                    by_x = num_ind_x;
                }

                nxt_ind_count += by_x + by_y;
                ind_count = nxt_ind_count;

                m_arrayIndices.Add(temp_ind);
                ++i_counter_again;
            }

            m_arrayOBB = new BoundingBox[total_split];

            for (int i = 0; i < total_split; ++i)
            {
                m_arrayOBB[i].Min = vertices[m_arrayIndices[i][0]].GetPosition();
                m_arrayOBB[i].Max = vertices[m_arrayIndices[i][0]].GetPosition();

                List<Vector3> temp_vector = new List<Vector3>();
                for (int j = 0; j < m_arrayIndices[i].Length; ++j)
                {
                    Vector3 pos = vertices[m_arrayIndices[i][j]].GetPosition();
                    if (pos.X < m_arrayOBB[i].Min.X)
                    {
                        m_arrayOBB[i].Min.X = pos.X;
                    }
                    else if (pos.X > m_arrayOBB[i].Max.X)
                    {
                        m_arrayOBB[i].Max.X = pos.X;
                    }
                    if (pos.Y < m_arrayOBB[i].Min.Y)
                    {
                        m_arrayOBB[i].Min.Y = pos.Y;
                    }
                    else if (pos.Y > m_arrayOBB[i].Max.Y)
                    {
                        m_arrayOBB[i].Max.Y = pos.Y;
                    }
                    if (pos.Z < m_arrayOBB[i].Min.Z)
                    {
                        m_arrayOBB[i].Min.Z = pos.Z;
                    }
                    else if (pos.Z > m_arrayOBB[i].Max.Z)
                    {
                        m_arrayOBB[i].Max.Z = pos.Z;
                    }
                }
            }

            //int count = 0;
            //for (int i = 0; i < m_numYVerts; ++i)
            //{
            //    for (int j = 0; j < m_numXVerts; ++j)
            //    {
            //        vertices[count].SetUV(new Vector2((float)j / (float)m_numXVerts, (float)i / (float)m_numYVerts));
            //    }
            //}

            // Finalising the creation of the vertex buffer
            m_vertexBuffer = new VertexBuffer(Device, vertices.Length * VertexPositionNormalTexture.SizeInBytes, BufferUsage.WriteOnly);
            m_vertexBuffer.SetData(vertices);

            // Finalising the creation of the index buffer
            m_indexBuffer = new DynamicIndexBuffer(Device, typeof(int), m_indices.Length, BufferUsage.WriteOnly);
            m_indexBuffer.SetData(m_indices, 0, m_indices.Length, SetDataOptions.None);

            //HeightMap.Dispose();

            return true;
        }

#if EDITOR
        public void loadTerrainFromXML(string fileName, ContentManager content, GraphicsDevice graphicsDevice)
#else
        public void loadTerrainFromXML(string fileName, ContentManager content, GraphicsDevice graphicsDevice, Dictionary<string, CModel.SModelData> ModelList)
#endif
        {
            string s = System.IO.Directory.GetCurrentDirectory();

            XmlReader reader = new XmlTextReader(s+ "\\"+fileName);
            
            reader.ReadToFollowing("TextureName");
            reader.Read();
            string texString = reader.Value;

            reader.ReadToFollowing("Split");
            reader.Read();
            int split = int.Parse(reader.Value);


            reader.ReadToFollowing("Length");
            reader.Read();
            int length = int.Parse(reader.Value);


            reader.ReadToFollowing("NumVertices");
            reader.Read();
            int numVerts = int.Parse(reader.Value);

            reader.ReadToFollowing("Height");
            reader.Read();
            int height = int.Parse(reader.Value);

            //texString = texString.Replace("Content", "");
            Texture2D currentTex = content.Load<Texture2D>("..\\"+texString.Replace(".xnb", ""));

#if EDITOR
            LoadAndCreateTerrain(currentTex, graphicsDevice, new Vector2(length), new Vector2(numVerts), (uint)height, new Vector2(split));
#else
            LoadAndCreateTerrain(currentTex, graphicsDevice, new Vector2(length), new Vector2(numVerts), (uint)height, new Vector2(split),
            ModelList, fileName);
#endif

            reader.Close();


        }

        private int GetLeftOver( int NumVert, int NumVertSplit, ref int Split )
        {
            int left_over = NumVert - (((NumVertSplit - 2) * Split) + (Split + 1));
            if (left_over == 1)
            {
                ++left_over;
            }
            if (left_over > NumVertSplit)
            {
                Split += left_over / NumVertSplit;
                left_over = GetLeftOver(NumVert, NumVertSplit, ref Split);
            }

            return left_over;
        }

        public BoundingBox[] GetArrayOfOBB()
        {
            return m_arrayOBB;
        }

        public void JoinSplitIndexBuffer( List<int> SplitTerrainIndex )
        {
            int num_indices = 0;
            m_numTriangles = 0;
            for (int i = 0; i < SplitTerrainIndex.Count; ++i)
            {
                num_indices += m_arrayIndices[SplitTerrainIndex[i]].Length;
                m_numTriangles += m_numTri[SplitTerrainIndex[i]];
            }

            // Print to output screen
            //System.Diagnostics.Trace.WriteLine(m_numVerts);

            int[] add = new int[num_indices];
            
            int count = 0;
            for (int i = 0; i < SplitTerrainIndex.Count; ++i)
            {
                m_arrayIndices[SplitTerrainIndex[i]].CopyTo(add, count);
                count += m_arrayIndices[SplitTerrainIndex[i]].Length;
            }

            m_indexBuffer.SetData(add, 0, num_indices, SetDataOptions.Discard);
        }

        public void SetVerticesForHeightFindToWorld( Matrix WrldMat )
        {
            for (int i = 0; i < m_heightVertices.Length; ++i)
            {
                m_heightVertices[i] = Vector3.Transform(m_heightVertices[i], WrldMat);
            }
        }

        public bool FindHeightInSplitTerrain(int SplitTerrainIndex, Vector3 RayDir, Vector3 RayOrigin, ref float Dist)
        {
            for (int i = 0; i < m_arrayIndices[SplitTerrainIndex].Length; i+=3)
            {
                CCollision.STriData temp;
                temp.s_firstPos = m_heightVertices[m_arrayIndices[SplitTerrainIndex][i]];
                temp.s_secondPos = m_heightVertices[m_arrayIndices[SplitTerrainIndex][i + 1]];
                temp.s_thirdPos = m_heightVertices[m_arrayIndices[SplitTerrainIndex][i + 2]];

                if (CCollision.RayVsTriangle(RayDir, RayOrigin, temp, out Dist) == true)
                {
                    return true;
                }
            }

            return false;
        }

        public int GetSplitX()
        {
            return m_splitX;
        }

        public int GetSplitY()
        {
            return m_splitY;
        }

        public override bool FindIntersectionPoint(Vector3 RayDir, Vector3 RayOrigin, Matrix WrldMat, ref Vector3 InterPoint)
        {
            return false;
        }

        public override List<Visual.CMaterial> GetMaterials(ContentManager Content, string ModelName)
        {
            m_modelName = ModelName;

            List<Visual.CMaterial> materials = new List<Visual.CMaterial>();

            materials.Add(new Visual.CMaterial());

            materials[0].m_diffuse = new Vector4(0.2f, 0.8f, 0.2f, 1.0f);
            materials[0].m_ambient = new Vector4(0.001f, 0.004f, 0.001f, 1.0f);
            materials[0].m_emissive = new Vector4(0, 0, 0, 0);
            materials[0].m_specularColour = Vector4.Zero;//new Vector4(1.0f, 1.0f, 1.0f, 1.0f);
            materials[0].m_specularPower = 0;// 128.0f;

            return materials;
        }

        private int m_numXVerts;
        private int m_numYVerts;
        private int m_lengthX;
        private int m_lengthY;
        private int m_splitX;
        private int m_splitY;
        private int[] m_indices;
        private List<int[]> m_arrayIndices;
        private BoundingBox[] m_arrayOBB;
        public int[] m_numTri;
        Vector3[] m_heightVertices;
    }
}

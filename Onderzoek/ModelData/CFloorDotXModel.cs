﻿////NOTE: THIS IS NOT NEEDED BUT IS BEING KEPT JUST INCASE IT IS NEEDED IN THE FUTURE.
////WARNING: THIS MAY CAUSE ERRORS IF THIS IS USED!
////CONTACT: Ankur Agarwal (akahank@gmail.com) if you want ot use this file.

//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;

//using Microsoft.Xna.Framework;
//using Microsoft.Xna.Framework.Graphics;

//using XMLManip;

//namespace Onderzoek.ModelData
//{
//    class CFloorDotXModel : CDotXModel
//    {
//        public override bool FindIntersectionPoint(Vector3 RayDir, Vector3 RayOrigin, Matrix WrldMat, ref Vector3 InterPoint)
//        {
//            float dist = 10000.0f;
//            Vector3 inter_point = new Vector3();
//            foreach (ModelMesh mesh in m_model.Meshes)
//            {
//                VertexPositionNormalTexture[] vertices = new VertexPositionNormalTexture[mesh.VertexBuffer.SizeInBytes / VertexPositionNormalTexture.SizeInBytes];

//                short[] indices = new short[mesh.IndexBuffer.SizeInBytes / 2];

//                mesh.VertexBuffer.GetData<VertexPositionNormalTexture>(vertices);
//                mesh.IndexBuffer.GetData<short>(indices);

//                Vector3[] vertexs = new Vector3[vertices.Length];

//                for (int index = 0; index < indices.Length; index+=3)
//                {
//                    vertexs[indices[index]] = vertices[indices[index]].Position;

//                    CToolbox.TriData temp;
//                    temp.s_firstPos = Vector3.Transform(vertices[indices[index]].Position, WrldMat);
//                    temp.s_secondPos = Vector3.Transform(vertices[indices[index+1]].Position, WrldMat);
//                    temp.s_thirdPos = Vector3.Transform(vertices[indices[index+2]].Position, WrldMat);

//                    if (CToolbox.RayVsTriangle(RayDir, RayOrigin, temp, ref InterPoint) == true)
//                    {
//                        float new_dist = RayOrigin.Y - InterPoint.Y;
//                        if (new_dist < dist)
//                        {
//                            inter_point = InterPoint;
//                            dist = new_dist;
//                        }
//                    }
//                }
//            }

//            if (dist != 10000.0f)
//            {
//                InterPoint = inter_point;
//                return true;
//            }
//            return false;
//        }

//        //public Model m_model;
//    }
//}

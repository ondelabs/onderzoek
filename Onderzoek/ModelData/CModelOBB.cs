﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Onderzoek.ModelData
{
    public class CModelOBB
    {
        public CModelOBB()
        {
            //m_parent = null;
            m_child = new List<CModelOBB>();
            m_OBB = new Vector3[8];
        }

        public bool ReturnOBBWithName(string Name, ref CModelOBB RtnVal)
        {
            if (m_name == Name)
            {
                RtnVal = this;
                return true;
            }

            for (int i = 0; i < m_child.Count; ++i)
            {
                if( m_child[i].ReturnOBBWithName(Name, ref RtnVal) == true )
                {
                    return true;
                }
            }

            return false;
        }

        //public CModelOBB m_parent;
        public List<CModelOBB> m_child;
        public Vector3[] m_OBB;
        public Vector3 m_position;
        public Vector3 m_rotation;
        public Vector3 m_scale;
        public string m_name;
        public bool m_encompass = false;
    }
}

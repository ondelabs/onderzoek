﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

using Onderzoek.Visual;
using XMLManip;

namespace Onderzoek.ModelData
{
    public class CDotXModel : CModel
    {
        public Model m_model;

#if ENGINE
        public virtual CModel Initialise(ContentManager Content, string ModelName, Dictionary<string, CModel.SModelData> ModelList)
        {
            CModel instance = base.Initialise(ModelList, ModelName);
            if (instance != null)
            {
                return instance;
            }
#else
        public virtual bool Initialise(ContentManager Content, string ModelName)
        {
#endif

            m_modelName = ModelName;

            m_bb = new List<CModelOBB>();

            try
            {
                m_model = Content.Load<Model>(ModelName);
            }
            catch (Exception e)
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CDotXModel", e.ToString());

#if ENGINE
                return null;
#else
                return false;
#endif
            }

            GetBoundingBoxFromModel( ModelName );

#if ENGINE
            return this;
#else
            return true;
#endif
        }

        public virtual void GetBoundingBoxFromModel(string ModelName)
        {
            string file;
            if (ModelName.Contains("Model"))
                file = "Content/" + ModelName.Replace("Model", "xml/Model") + ".xml";
            else // this most likely means its just the editor and doesnt have a model/ in it
            {
                file = "Content\\xml\\Model\\" + ModelName + ".xml";
                //System.
            }

            GetOBBFromModel(file, ref m_bb);

            //string file1 = "Content/" + ModelName.Replace("Model", "xml") + "_01" + ".xml";

            //XmlNodeStructure test = new XmlNodeStructure();
            //test.xmlNodeName = "PhysicsBB";
            //for (int i = 0; i < m_bb.Count; ++i)
            //{
            //    for (int j = 0; j < m_bb[i].m_OBB.Length; ++j)
            //    {
            //        test.addNewElement("BB" + i, m_bb[i].m_OBB[j].ToString());
            //    }
            //}

            //CXMLManip write_test = new CXMLManip();
            //write_test.setXmlData(test);
            //write_test.saveXmlFile(file1);
        }

        private void GetOBBFromModel( string FileName, ref List<CModelOBB> OBBList )
        {
            if (CBBLoader.Instance != null && CBBLoader.Instance.loadBBFromXML(FileName, ref OBBList) == false)
            {
                //BoundingBox boundingBox = new BoundingBox();
                Matrix[] transforms = new Matrix[m_model.Bones.Count];
                m_model.CopyAbsoluteBoneTransformsTo(transforms);

                foreach (ModelMesh mesh in m_model.Meshes)
                {
                    if (mesh.IndexBuffer.IndexElementSize == IndexElementSize.SixteenBits)
                    {
                        foreach (ModelMeshPart part in mesh.MeshParts)
                        {
                            // This shows what each of the vertices are made up of.
                            //VertexElement[] vertexElements = part.VertexDeclaration.GetVertexElements();

                            Vector3[] vertexs = null;
                            switch (part.VertexStride)
                            {
                                case 24:
                                    {
                                        VertexPositionNormal[] vertices = new VertexPositionNormal[mesh.VertexBuffer.SizeInBytes / part.VertexStride];

                                        short[] indices = new short[mesh.IndexBuffer.SizeInBytes / 2];

                                        mesh.VertexBuffer.GetData<VertexPositionNormal>(vertices);
                                        mesh.IndexBuffer.GetData<short>(indices);

                                        vertexs = new Vector3[vertices.Length];

                                        for (int index = part.StartIndex; index < part.PrimitiveCount * 3; ++index)
                                        {
                                            vertexs[indices[index]] = Vector3.Transform(vertices[indices[index]].GetPosition(), transforms[mesh.ParentBone.Index]);
                                        }

                                        break;
                                    }
                                case 32:
                                    {
                                        VertexPositionNormalTexture[] vertices = new VertexPositionNormalTexture[mesh.VertexBuffer.SizeInBytes / part.VertexStride];
                                        
                                        short[] indices = new short[mesh.IndexBuffer.SizeInBytes / 2];

                                        mesh.VertexBuffer.GetData<VertexPositionNormalTexture>(vertices);
                                        mesh.IndexBuffer.GetData<short>(indices);

                                        vertexs = new Vector3[vertices.Length];

                                        for (int index = part.StartIndex; index < part.PrimitiveCount * 3; ++index)
                                        {
                                            vertexs[indices[index]] = Vector3.Transform(vertices[indices[index]].GetPosition(), transforms[mesh.ParentBone.Index]);
                                        }

                                        break;
                                    }
                                case 36:
                                    {
                                        VertexPositionNormalTextureColour[] vertices = new VertexPositionNormalTextureColour[mesh.VertexBuffer.SizeInBytes / part.VertexStride];

                                        short[] indices = new short[mesh.IndexBuffer.SizeInBytes / 2];

                                        mesh.VertexBuffer.GetData<VertexPositionNormalTextureColour>(vertices);
                                        mesh.IndexBuffer.GetData<short>(indices);

                                        vertexs = new Vector3[vertices.Length];

                                        for (int index = part.StartIndex; index < part.PrimitiveCount * 3; ++index)
                                        {
                                            vertexs[indices[index]] = Vector3.Transform(vertices[indices[index]].GetPosition(), transforms[mesh.ParentBone.Index]);
                                        }

                                        break;
                                    }
                                case 56:
                                    {
                                        VertexPositionNormalTextureTangentBinormal[] vertices =
                                            new VertexPositionNormalTextureTangentBinormal[mesh.VertexBuffer.SizeInBytes / part.VertexStride];

                                        short[] indices = new short[mesh.IndexBuffer.SizeInBytes / 2];

                                        mesh.VertexBuffer.GetData<VertexPositionNormalTextureTangentBinormal>(vertices);
                                        mesh.IndexBuffer.GetData<short>(indices);

                                        vertexs = new Vector3[vertices.Length];

                                        for (int index = part.StartIndex; index < part.PrimitiveCount * 3; ++index)
                                        {
                                            vertexs[indices[index]] = Vector3.Transform(vertices[indices[index]].GetPosition(), transforms[mesh.ParentBone.Index]);
                                        }

                                        break;
                                    }
                                default:
                                    {
                                        CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CDotXModel", "ONDERZOEK ENGINE ERROR: Model BB could not be created due to error in VertexStride.");
                                        throw new Exception("ONDERZOEK ENGINE ERROR: Model BB could not be created due to error in VertexStride.");
                                    }
                            }

                            Vector3 min, max;
                            CToolbox.GetMinMax(vertexs, out min, out max);

                            //boundingBox = BoundingBox.CreateMerged(boundingBox, BoundingBox.CreateFromPoints(vertexs).GetCorners());

                            CModelOBB temp = new CModelOBB();
                            temp.m_OBB = BoundingBox.CreateFromPoints(vertexs).GetCorners();
                            OBBList.Add(temp);
                        }
                    }
                    else
                    {
                        CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CDotXModel", "ONDERZOEK ENGINE ERROR: SizeInBytes is not equal to 16bits. Fix please.");
                    }
                }
            }
        }

        public override CModelOBB GetRoomOBB()
        {
            string file;
            if (m_modelName.Contains("Model"))
                file = "Content/" + m_modelName.Replace("Model", "xml/Model") + ".xml";
            else // this most likely means its just the editor and doesnt have a model/ in it
            {
                file = "Content/xml/Model/" + m_modelName + ".xml";
                //System.
            }

            string new_filename = file.Replace(".xml", "_ForRoom.xml");

            List<CModelOBB> bb_list = new List<CModelOBB>();
            GetOBBFromModel(new_filename, ref bb_list);

            return bb_list[0];
        }

        public override List<CMaterial> GetMaterials(ContentManager Content, string ModelName)
        {
            List<CMaterial> materials = new List<CMaterial>();

            int count = 0;
            foreach (ModelMesh mesh in m_model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    materials.Add(new CMaterial());

                    materials[count].m_diffuse = new Vector4(effect.DiffuseColor, 1.0f);
                    materials[count].m_ambient = new Vector4(effect.AmbientLightColor, 1.0f);
                    materials[count].m_emissive = new Vector4(effect.EmissiveColor, 1.0f);
                    materials[count].m_specularColour = new Vector4(effect.SpecularColor, 1.0f);
                    materials[count].m_specularPower = effect.SpecularPower;

                    if (effect.TextureEnabled == true)
                    {
                        materials[count].m_textureID = CVisual.Instance.LoadLevelTexture(ModelName + count, Content, effect.Texture);
                    }

                    ++count;
                }
            }
            
            return materials;
        }

        public override Effect GetMaterialAtIndex(int Index)
        {
            int count = 0;
            foreach (ModelMesh mesh in m_model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    if (count == Index)
                    {
                        return effect;
                    }

                    ++count;
                }
            }

            return null;
        }

        public override Model getBaseModel()
        {
            return m_model;
            //return base.getBaseModel();
        }
    }
}

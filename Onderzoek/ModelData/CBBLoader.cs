﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml;

using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;

namespace Onderzoek.ModelData
{
    public class CBBLoader
    {
        // For singleton
        static CBBLoader m_instance;
        CDotXModel m_obbModel;

        public static CBBLoader Instance
        {
            get { return m_instance; }
        }

        public CBBLoader(ContentManager Content, Dictionary<string, CModel.SModelData> ModelList)
        {

            if (m_instance == null)
            {
                m_instance = this;
            }
            else
            {
                // Release any garbage before creating a new instance, just incase.
                GC.Collect();
                GC.WaitForPendingFinalizers();

                m_instance = this;
            }

#if ENGINE
            m_obbModel = new CDotXModel();
            m_obbModel = (CDotXModel)m_obbModel.Initialise(Content, "Model/XBox", ModelList);
#endif
        }

#if EDITOR
        public void LoadBB(ContentManager Content)
        {
            m_obbModel = new CDotXModel();
            m_obbModel.Initialise(Content, "Model/XBox");
        }
#endif

        public void Dispose()
        {
            if (m_obbModel != null)
            {
                m_obbModel.Delete();
                m_obbModel = null;
            }

            m_instance = null;
        }

        public Vector3[] GetBaseOOBB()
        {
            return m_obbModel.GetMainOBB();
        }

        private CModelOBB readBBEntity(XmlReader reader)
        {
            Vector3 position, rotation, scale;

            reader.ReadToDescendant("Position");
            reader.Read();

            position = new Vector3(float.Parse(reader.Value.Split(' ')[0]), float.Parse(reader.Value.Split(' ')[1]), float.Parse(reader.Value.Split(' ')[2]));

            reader.ReadToFollowing("Rotation");

            reader.Read();

            rotation = new Vector3(float.Parse(reader.Value.Split(' ')[0]), float.Parse(reader.Value.Split(' ')[1]), float.Parse(reader.Value.Split(' ')[2]));

            reader.ReadToFollowing("Scale");

            reader.Read();

            scale = new Vector3(float.Parse(reader.Value.Split(' ')[0]), float.Parse(reader.Value.Split(' ')[1]), float.Parse(reader.Value.Split(' ')[2]));

            reader.Read();


            reader.ReadToFollowing("Children");
            //reader.Read();

            CModelOBB model_obb = new CModelOBB();
            model_obb.m_position = position;
            model_obb.m_rotation = rotation;
            model_obb.m_scale = scale;

            Matrix wrld_mat = Matrix.CreateScale(scale) * Matrix.CreateFromYawPitchRoll(rotation.X, rotation.Y, rotation.Z) * Matrix.CreateTranslation(position);

            Vector3[] main_bb = m_obbModel.GetMainOBB();
            for (int i = 0; i < 8; ++i)
            {
                model_obb.m_OBB[i] = Vector3.Transform(main_bb[i], wrld_mat);
            }
            //model_obb.m_OBB[0] = Vector3.Transform(new Vector3(-1, 1, 1), wrld_mat);
            //model_obb.m_OBB[1] = Vector3.Transform(new Vector3(1, 1, 1), wrld_mat);
            //model_obb.m_OBB[2] = Vector3.Transform(new Vector3(1, -1, 1), wrld_mat);
            //model_obb.m_OBB[3] = Vector3.Transform(new Vector3(-1, -1, 1), wrld_mat);
            //model_obb.m_OBB[4] = Vector3.Transform(new Vector3(-1, 1, -1), wrld_mat);
            //model_obb.m_OBB[5] = Vector3.Transform(new Vector3(1, 1, -1), wrld_mat);
            //model_obb.m_OBB[6] = Vector3.Transform(new Vector3(1, -1, -1), wrld_mat);
            //model_obb.m_OBB[7] = Vector3.Transform(new Vector3(-1, -1, -1), wrld_mat);

            //reader.Read();

            if (!reader.IsEmptyElement)
            {
                //while ((reader.NodeType != XmlNodeType.EndElement || !reader.Name.Equals("Children")))
                while (true)
                {
                    if ((reader.NodeType == XmlNodeType.EndElement && reader.Name.Equals("Children")))
                        break;

                    if (reader.NodeType == XmlNodeType.Element && reader.Name.Equals("BoundingBox"))
                        model_obb.m_child.Add(readBBEntity(reader));

                    reader.Read();
                }
            }

            reader.Read();

            return model_obb;
        }


        
        public bool loadBBFromXML(string fileName, ref List<CModelOBB> OBB)
        {
            XmlTextReader xmlReader = new XmlTextReader(fileName);

            while (!xmlReader.EOF)
            {
                if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "PhysicsBB")
                {
                    xmlReader.ReadToFollowing("BoundingBox");
                    OBB.Add(readBBEntity(xmlReader));
                }
                try
                {
                    xmlReader.Read();
                }
                catch (XmlException e)
                {
                    System.Diagnostics.Trace.WriteLine("\n--------------------------------------------------------------\n");
                    System.Diagnostics.Trace.WriteLine("An XML Exception occurred: " + e.Message + "\n");
                    System.Diagnostics.Trace.WriteLine("--------------------------------------------------------------\n");

                    xmlReader.Close();

                    return false;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Trace.WriteLine("\n--------------------------------------------------------------\n");
                    System.Diagnostics.Trace.WriteLine("A General Exception occurred: " + e.Message + "\n");
                    System.Diagnostics.Trace.WriteLine("--------------------------------------------------------------\n");

                    xmlReader.Close();

                    return false;
                }
                //xmlReader.Read();
            }

            xmlReader.Close();
            return true;
        }
    }
}

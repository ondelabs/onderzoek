﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Onderzoek.ModelData
{
    public class CWireFrameModel : CModel
    {
        // Need to Initialise the corners in a clockwise fashion.
#if ENGINE
        public CModel Initialise(Vector3[] Corners, Dictionary<string, CModel.SModelData> ModelList, string ModelName)
        {
            CModel instance = base.Initialise(ModelList, ModelName);
            if (instance != null)
            {
                return instance;
            }
#else
        public bool Initialise(Vector3[] Corners, string ModelName)
        {
#endif
            m_modelName = ModelName;

            m_bb = new List<CModelOBB>();

            CModelOBB temp = new CModelOBB();
            temp.m_OBB = Corners;
            m_bb.Add( temp );

#if ENGINE
            return this;
#else
            return true;
#endif
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Onderzoek.ModelData
{
    public class CCustomModel : CModel
    {
        public override void Delete()
        {
            m_indexBuffer.Dispose();
            m_vertexBuffer.Dispose();
            m_vertexDeclaration.Dispose();
        }

        public VertexBuffer m_vertexBuffer;
        public DynamicIndexBuffer  m_indexBuffer;
        public VertexDeclaration m_vertexDeclaration;
        public int m_numVerts;
        public int m_numIndices;
        public int m_numTriangles;
    }
}

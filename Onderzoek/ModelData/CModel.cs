﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

using Onderzoek.Visual;
using XMLManip;

namespace Onderzoek.ModelData
{
    public abstract class CModel
    {
        public struct SModelData
        {
            public SModelData(CModel Model)
            {
                s_model = Model;
                s_numOfInstances = 1;
            }

            public CModel s_model;
            public int s_numOfInstances;
        }

        protected struct STexLoadData
        {
            public STexLoadData( int TexID, int NormID, int SpecID )
            {
                s_texID = TexID;
                s_normID = NormID;
                s_specID = SpecID;
            }

            public int s_texID;
            public int s_normID;
            public int s_specID;
        }

        protected CModel Initialise(Dictionary<string, SModelData> ModelList, string ModelName)
        {
            SModelData num_of_models;
            if (ModelList.TryGetValue(ModelName, out num_of_models) == true)
            {
                ++num_of_models.s_numOfInstances;
                ModelList[ModelName] = num_of_models;
                return num_of_models.s_model;
            }
            else
            {
                ModelList.Add(ModelName, new SModelData(this));
                return null;
            }
        }

        protected List<CModelOBB> m_bb;
        protected bool m_multipleBB = false;
        protected string m_modelName = "Model";

        public string GetModelName()
        {
            return m_modelName;
        }

        public Vector3[] GetMainOBB()
        {
            return m_bb[0].m_OBB;
        }

        public List<CModelOBB> GetOBB()
        {
            return m_bb;
        }

        public void SetOBB(List<CModelOBB> BBData)
        {
            m_bb = BBData;
        }

        public virtual CModelOBB GetRoomOBB()
        {
            return new CModelOBB();
        }

        public bool HasMultipleBB()
        {
            return m_multipleBB;
        }

        public virtual void Delete()
        {
            m_bb.Clear();
        }

        public virtual List<CMaterial> GetMaterials(ContentManager Content, string ModelName)
        {
            return null;
        }

        public virtual Microsoft.Xna.Framework.Graphics.Effect GetMaterialAtIndex(int Index)
        {
            return null;
        }

        protected void GetMapData(XmlNodeStructure XMLData, ContentManager Content, List<STexLoadData> TexData)
        {
            for (int i = 0; i < XMLData.xmlDictionary.Count; ++i)
            {
                KeyValuePair<string, List<XmlNodeStructure>> temp = XMLData.xmlDictionary.ElementAt(i);
                GetMapDataFromKeyValue(temp, Content, TexData);
            }
        }

        protected void GetMapDataFromKeyValue(KeyValuePair<string, List<XmlNodeStructure>> KeyValue, ContentManager Content, List<STexLoadData> TexData)
        {
            for (int i = 0; i < KeyValue.Value.Count; ++i)
            {
                XmlNodeStructure temp = KeyValue.Value.ElementAt(i);
                if (temp.xmlDictionary != null)
                {
                    TexData.Add(new STexLoadData(-1,-1,-1));

                    GetMapData(temp, Content, TexData);
                }
                else
                {
                    if (string.Compare("diffusemap", temp.xmlNodeName) == 0)
                    {
                        STexLoadData tex_data = TexData[TexData.Count - 1];
                        tex_data.s_texID = CVisual.Instance.LoadLevelTexture(temp.xmlNodeValue, Content);
                        TexData[TexData.Count - 1] = tex_data;
                    }
                    else if (string.Compare("normalmap", temp.xmlNodeName) == 0)
                    {
                        STexLoadData tex_data = TexData[TexData.Count - 1];
                        tex_data.s_normID = CVisual.Instance.LoadLevelTexture(temp.xmlNodeValue, Content);
                        TexData[TexData.Count - 1] = tex_data;
                    }
                    else if (string.Compare("specularmap", temp.xmlNodeName) == 0)
                    {
                        STexLoadData tex_data = TexData[TexData.Count - 1];
                        tex_data.s_specID = CVisual.Instance.LoadLevelTexture(temp.xmlNodeValue, Content);
                        TexData[TexData.Count - 1] = tex_data;
                    }
                    else
                    {
                        System.Diagnostics.Trace.WriteLine("\n--------------------------------------------------------------\n");
                        System.Diagnostics.Trace.WriteLine(temp.xmlNodeName + " is not a valid xml tag for maps in the material xml.\n");
                        System.Diagnostics.Trace.WriteLine("--------------------------------------------------------------\n");
                    }
                }
            }
        }

#region OLD_LOADER
        //protected void GetOBBFromFile(Dictionary<string, List<XmlNodeStructure>> XMLData, ref List<CModelOBB> ModelOBB)
        //{
        //    foreach (KeyValuePair<string, List<XmlNodeStructure>> data in XMLData)
        //    {
        //        for (int i = 0; i < data.Value.Count; ++i)
        //        {
        //            OBBDataFromFile(data.Value[i].xmlDictionary, ref ModelOBB);
        //        }
        //    }
        //}

        //private void OBBDataFromFile(Dictionary<string, List<XmlNodeStructure>> XMLData, ref List<CModelOBB> ModelOBB)
        //{
        //    foreach (KeyValuePair<string, List<XmlNodeStructure>> data in XMLData)
        //    {
        //        GetOBBDataFromFile(data.Value, ref ModelOBB);
        //    }
        //}

        //private void GetOBBDataFromFile(List<XmlNodeStructure> XMLData, ref List<CModelOBB> ModelOBB)
        //{
        //    for (int i = 0; i < XMLData.Count; ++i)
        //    {
        //        if (XMLData[i].xmlDictionary != null)
        //        {
        //            if (string.Compare("BB", XMLData[i].xmlNodeName) == 0)
        //            {
        //                OBBDataFromFile(XMLData[i].xmlDictionary, ref ModelOBB[ModelOBB.Count - 1].m_child);
        //            }
        //            else
        //            {
        //                OBBDataFromFile(XMLData[i].xmlDictionary, ref ModelOBB);
        //            }
        //        }
        //        else
        //        {
        //            SetData(XMLData[i].xmlNodeName, XMLData[i].xmlNodeValue, ref ModelOBB);
        //        }
        //    }
        //}

        //private void SetData(string NodeName, string NodeValue, ref List<CModelOBB> ModelOBB)
        //{
        //    if (string.Compare("Position", NodeName) == 0)
        //    {
        //        CModelOBB temp = new CModelOBB();
        //        CToolbox.StringToVector3(NodeValue, ref temp.m_position);
        //        ModelOBB.Add(temp);
        //    }
        //    else if (string.Compare("Rotation", NodeName) == 0)
        //    {
        //        CToolbox.StringToVector3(NodeValue, ref ModelOBB[ModelOBB.Count - 1].m_rotation);
        //    }
        //    else if (string.Compare("Scale", NodeName) == 0)
        //    {
        //        CModelOBB temp = ModelOBB[ModelOBB.Count - 1];

        //        CToolbox.StringToVector3(NodeValue, ref temp.m_scale);

        //        Matrix wrld_mat = Matrix.CreateScale(temp.m_scale) * Matrix.CreateFromYawPitchRoll(temp.m_rotation.X, temp.m_rotation.Y, temp.m_rotation.Z) *
        //            Matrix.CreateTranslation(temp.m_position);

        //        temp.m_OBB = new Vector3[8];
        //        temp.m_OBB[0] = Vector3.Transform(new Vector3(-1, 1, 1), wrld_mat);
        //        temp.m_OBB[1] = Vector3.Transform(new Vector3(1, 1, 1), wrld_mat);
        //        temp.m_OBB[2] = Vector3.Transform(new Vector3(1, -1, 1), wrld_mat);
        //        temp.m_OBB[3] = Vector3.Transform(new Vector3(-1, -1, 1), wrld_mat);
        //        temp.m_OBB[4] = Vector3.Transform(new Vector3(-1, 1, -1), wrld_mat);
        //        temp.m_OBB[5] = Vector3.Transform(new Vector3(1, 1, -1), wrld_mat);
        //        temp.m_OBB[6] = Vector3.Transform(new Vector3(1, -1, -1), wrld_mat);
        //        temp.m_OBB[7] = Vector3.Transform(new Vector3(-1, -1, -1), wrld_mat);

        //        ModelOBB[ModelOBB.Count - 1] = temp;
        //    }
        //    else
        //    {
        //        CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CDotXModel", "Unknown node in bb xml file.");
        //    }
        //}
#endregion OLD_LOADER

        public bool FindOBBWithName(string Name, ref CModelOBB RtnVal)
        {
            for (int i = 0; i < m_bb.Count; ++i)
            {
                if (m_bb[i].ReturnOBBWithName(Name, ref RtnVal) == true)
                {
                    return true;
                }
            }

            return false;
        }

        static public List<CModelOBB> CreateOBBofUnitSize(float HalfSize)
        {
            Vector3 min = new Vector3(-HalfSize, -HalfSize, -HalfSize);
            Vector3 max = new Vector3(HalfSize, HalfSize, HalfSize);

            BoundingBox bb = new BoundingBox(min, max);

            CModelOBB temp = new CModelOBB();

            temp.m_child = new List<CModelOBB>();
            temp.m_encompass = false;
            temp.m_name = "BB0";
            temp.m_OBB = bb.GetCorners();

            List<CModelOBB> list_bb = new List<CModelOBB>();

            list_bb.Add(temp);

            return list_bb;
        }

        static public Vector3[] CreateAPortal()
        {
            Vector3[] rtn_val = new Vector3[4];
            rtn_val[0] = new Vector3(-1, 0, -1);
            rtn_val[1] = new Vector3(-1, 0, 1);
            rtn_val[2] = new Vector3(1, 0, 1);
            rtn_val[3] = new Vector3(1, 0, -1);

            return rtn_val;
        }

        public virtual bool FindIntersectionPoint(Vector3 RayDir, Vector3 RayOrigin, Matrix WrldMat, ref Vector3 InterPoint)
        {
            return false;
        }

        public virtual Microsoft.Xna.Framework.Graphics.Model getBaseModel()
        {
            return null;
        }
    }
}

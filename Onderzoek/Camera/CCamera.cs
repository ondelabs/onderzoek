using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;

namespace Onderzoek.Camera
{
    public class CCamera
    {
        // Members which are used by all three camera types.
        protected Matrix m_projection;
        protected Matrix m_view;
        protected Matrix m_invProjection;
        protected Matrix m_invView;
        protected Matrix m_vp;
        protected Vector3 m_dir;
        protected float m_near = 0;
        protected float m_far = 0;
        protected Plane[] m_clipPlanes;
        protected float m_pitch = 0;
        protected float m_yaw = 0;

        // For debug only
        protected Matrix m_oldVP;
        protected bool m_savePrvData = false;
        protected Vector3 m_prvDir;
        protected Vector3 m_prvPosition;
        protected Matrix m_prvRot;

        #region unused
        //private void Translate()
        //{
        //    Vector3 v = new Vector3(CSharedData.Instance.m_leftStick.X * 0.4f, 0, -CSharedData.Instance.m_leftStick.Y * 0.4f);
        //    m_position += Vector3.Transform(v, m_rotation);
        //}

        //private Vector3 TranslateAUnitDistForward()
        //{
        //    return (m_position + Vector3.Transform(new Vector3(0, 0, -1.0f), m_rotation));
        //}

        //public bool ClipXNA(Vector3 Min, Vector3 Max)
        //{
        //    Matrix vp;
        //    if (Onderzoek.CSharedData.Instance.m_leftClick == true)
        //    {
        //        vp = m_oldVP;
        //    }
        //    else
        //    {
        //        vp = Matrix.Multiply(m_view, m_projection);
        //        m_oldVP = vp;
        //    }

        //    BoundingBox bb = new BoundingBox(Min, Max);
        //    BoundingFrustum vf = new BoundingFrustum(vp);
        //    return vf.Intersects(bb);
        //}

        //public Vector3 GetTopRightFarPlanePosition()
        //{
        //    float h_far = (2.0f * (float)(Math.Tan(m_fov / 2.0f)) * m_far) * 0.5f;
        //    float w_far = ((h_far * 2.0f) * m_aspectRatio) * 0.5f;

        //    Vector3 far_pos = m_position + (m_dir * m_far);

        //    Vector3 rtn_val = far_pos + (m_matRot.Up * h_far) + (m_matRot.Right * w_far);
        //    return rtn_val;
        //}

        //public void GetFarPlanePositionsInViewSpace( Vector3[] Corners )
        //{
        //    float h_far = (2.0f * (float)(Math.Tan(m_fov / 2.0f)) * m_far) * 0.5f;
        //    float w_far = ((h_far * 2.0f) * m_aspectRatio) * 0.5f;

        //    Vector3 far_pos = m_position + (m_dir * m_far);

        //    Corners[0] = Vector3.Transform(far_pos + (m_matRot.Up * h_far) - (m_matRot.Right * w_far), m_view);
        //    Corners[1] = Vector3.Transform(far_pos + (m_matRot.Up * h_far) + (m_matRot.Right * w_far), m_view);
        //    Corners[2] = Vector3.Transform(far_pos - (m_matRot.Up * h_far) + (m_matRot.Right * w_far), m_view);
        //    Corners[3] = Vector3.Transform(far_pos - (m_matRot.Up * h_far) - (m_matRot.Right * w_far), m_view);
        //}

        //public COrthoCamera CreateLightViewProjectionMatrix(Vector3 LightDir)
        //{
        //    // Matrix with that will rotate in points the direction of the light
        //    Matrix lightRotation = Matrix.CreateLookAt(Vector3.Zero,
        //                                               -LightDir,
        //                                               Vector3.Up);

        //    // Get the corners of the frustum
        //    //BoundingFrustum cameraFrustum = new BoundingFrustum(m_vp);
        //    //Vector3[] frustumCorners = cameraFrustum.GetCorners();
        //    Vector3[] frustumCorners;
        //    GetViewFrustumCorners(out frustumCorners);

        //    // Transform the positions of the corners into the direction of the light
        //    for (int i = 0; i < frustumCorners.Length; i++)
        //    {
        //        frustumCorners[i] = Vector3.Transform(frustumCorners[i], lightRotation);
        //    }

        //    // Find the smallest box around the points
        //    BoundingBox lightBox = BoundingBox.CreateFromPoints(frustumCorners);

        //    Vector3 boxSize = lightBox.Max - lightBox.Min;
        //    Vector3 halfBoxSize = boxSize * 0.5f;

        //    // The position of the light should be in the center of the back
        //    // pannel of the box. 
        //    Vector3 lightPosition = lightBox.Min + halfBoxSize;
        //    lightPosition.Z = lightBox.Min.Z;

        //    // We need the position back in world coordinates so we transform 
        //    // the light position by the inverse (more efficient if tranpose - which is the same as ionvert when it comes to rotations)
        //    // of the lights rotation
        //    lightPosition = Vector3.Transform(lightPosition,
        //                                      Matrix.Transpose(lightRotation));

        //    // Create the view matrix for the light
        //    Matrix lightView = Matrix.CreateLookAt(lightPosition,
        //                                           lightPosition - LightDir,
        //                                           Vector3.Up);

        //    COrthoCamera newCamera = new COrthoCamera(boxSize.X, boxSize.Y, -boxSize.Z, boxSize.Z, lightView, LightDir);

        //    return newCamera;
        //}
        #endregion
        public virtual void SetPosition(Vector3 Position)
        {
            throw new Exception("You need to override this funtion to use it.");
        }

        public virtual Vector3 GetPosition()
        {
            //throw new Exception("You need to override this funtion to use it.");
            return Vector3.Zero;
        }

        public virtual Quaternion GetRotation()
        {
            throw new Exception("You need to override this funtion to use it.");
        }
        public virtual void SetRotation(Quaternion Rotation)
        {
            throw new Exception("You need to override this funtion to use it.");
        }

        public virtual Matrix GetRotationMatrix()
        {
            throw new Exception("You need to override this funtion to use it.");
        }

        public virtual void SetRotationMatrix(Matrix RotationMatrix)
        {
            throw new Exception("You need to override this funtion to use it.");
        }

        public void SetView(Matrix View)
        {
            m_view = View;
        }

        public Matrix GetView()
        {
            return m_view;
        }

        public void SetProjection(Matrix Projection)
        {
            m_projection = Projection;
        }

        public Matrix GetProjection()
        {
            return m_projection;
        }

        public Matrix GetInvView()
        {
            return m_invView;
        }

        public Matrix GetInvProjection()
        {
            return m_invProjection;
        }

        public Matrix GetViewProjection()
        {
            return m_vp;
        }

        public Vector3 GetDir()
        {
            return m_dir;
        }

        public Plane[] GetClipPlanes()
        {
            return m_clipPlanes;
        }

        public float GetNearClipDistance()
        {
            return m_near;
        }

        public float GetFarClipDistance()
        {
            return m_far;
        }

        public virtual void Update(Vector3 ChangeInPosSinceLastCameraUpdate, Vector2 ChangeInRotSinceLastCameraUpdate)
        {
        }

        public virtual void Refresh()
        {
        }

        public virtual void UpdateProjectionMatrix(float FOV, float AspectRatio, float Near, float Far)
        {
        }

        public void CreateViewFrustumClipPlanes( Plane[] Planes, bool Normalise )
        {
            //Matrix pv = Matrix.Multiply(m_view, m_projection);

            // Left clipping plane
            Planes[0].Normal.X = m_vp.M14 + m_vp.M11;
            Planes[0].Normal.Y = m_vp.M24 + m_vp.M21;
            Planes[0].Normal.Z = m_vp.M34 + m_vp.M31;
            Planes[0].D = m_vp.M44 + m_vp.M41;

            // Right clipping plane
            Planes[1].Normal.X = m_vp.M14 - m_vp.M11;
            Planes[1].Normal.Y = m_vp.M24 - m_vp.M21;
            Planes[1].Normal.Z = m_vp.M34 - m_vp.M31;
            Planes[1].D = m_vp.M44 - m_vp.M41;
            
            // Top clipping plane
            Planes[2].Normal.X = m_vp.M14 - m_vp.M12;
            Planes[2].Normal.Y = m_vp.M24 - m_vp.M22;
            Planes[2].Normal.Z = m_vp.M34 - m_vp.M32;
            Planes[2].D = m_vp.M44 - m_vp.M42;

            // Bottom clipping plane
            Planes[3].Normal.X = m_vp.M14 + m_vp.M12;
            Planes[3].Normal.Y = m_vp.M24 + m_vp.M22;
            Planes[3].Normal.Z = m_vp.M34 + m_vp.M32;
            Planes[3].D = m_vp.M44 + m_vp.M42;

            // Near clipping plane
            Planes[4].Normal.X = m_vp.M13;
            Planes[4].Normal.Y = m_vp.M23;
            Planes[4].Normal.Z = m_vp.M33;
            Planes[4].D = m_vp.M43;

            // Far clipping plane
            Planes[5].Normal.X = m_vp.M14 - m_vp.M13;
            Planes[5].Normal.Y = m_vp.M24 - m_vp.M23;
            Planes[5].Normal.Z = m_vp.M34 - m_vp.M33;
            Planes[5].D = m_vp.M44 - m_vp.M43;
            
            // Normalize the plane equations, if requested
            if (Normalise == true)
            {
                Planes[0] = Plane.Normalize(Planes[0]);
                Planes[1] = Plane.Normalize(Planes[1]);
                Planes[2] = Plane.Normalize(Planes[2]);
                Planes[3] = Plane.Normalize(Planes[3]);
                Planes[4] = Plane.Normalize(Planes[4]);
                Planes[5] = Plane.Normalize(Planes[5]);
            }
        }

        public virtual void GetViewFrustumCorners(out Vector3[] Corners)
        {
            Corners = new Vector3[1];
        }
        protected void GetViewFrustumCorners(out Vector3[] Corners, Vector3 Position, Matrix MatRot, float FarHeight, float FarWidth)
        {
            if (m_savePrvData == false)
            {
                m_savePrvData = true;
                m_prvPosition = Position;
                m_prvDir = m_dir;
                m_prvRot = MatRot;
            }

            Vector3 far_pos = m_prvPosition + (m_prvDir * m_far);

            Corners = new Vector3[5];

            Corners[0] = m_prvPosition;
            Corners[4] = far_pos - (m_prvRot.Up * FarHeight) + (m_prvRot.Right * FarWidth);
            Corners[3] = far_pos + (m_prvRot.Up * FarHeight) + (m_prvRot.Right * FarWidth);
            Corners[2] = far_pos - (m_prvRot.Up * FarHeight) - (m_prvRot.Right * FarWidth);
            Corners[1] = far_pos + (m_prvRot.Up * FarHeight) - (m_prvRot.Right * FarWidth);
        }

        public void GetFarFrustumCornersVS(out Vector3[] FrutumCorners)
        {
            Vector4[] frustum_corners = new Vector4[4];

            //frustum_corners[0] = new Vector4(-1.0f, 1.0f, 0.0f, 1.0f);
            //frustum_corners[1] = new Vector4(1.0f, 1.0f, 0.0f, 1.0f);
            //frustum_corners[2] = new Vector4(1.0f, -1.0f, 0.0f, 1.0f);
            //frustum_corners[3] = new Vector4(-1.0f, -1.0f, 0.0f, 1.0f);
            frustum_corners[0] = new Vector4(-1.0f, 1.0f, 1.0f, 1.0f);
            frustum_corners[1] = new Vector4(1.0f, 1.0f, 1.0f, 1.0f);
            frustum_corners[2] = new Vector4(1.0f, -1.0f, 1.0f, 1.0f);
            frustum_corners[3] = new Vector4(-1.0f, -1.0f, 1.0f, 1.0f);

            FrutumCorners = new Vector3[4];

            for (int i = 0; i < 4; i++)
            {
                frustum_corners[i] = Vector4.Transform(frustum_corners[i], m_invProjection);

                FrutumCorners[i].X = frustum_corners[i].X / frustum_corners[i].W;
                FrutumCorners[i].Y = frustum_corners[i].Y / frustum_corners[i].W;
                FrutumCorners[i].Z = frustum_corners[i].Z / frustum_corners[i].W;

                //FrutumCorners[i] = Vector4.Transform(FrutumCorners[i], m_view);
            }
        }

        // This needs to be palced somewhere else.
        public static COrthoCamera CreateLightViewProjectionMatrix(Vector3[] WorldBBCoordinates, Vector3 LightDir)
        {
            // Matrix with that will rotate in points the direction of the light
            Matrix lightRotation = Matrix.CreateLookAt(Vector3.Zero,
                                                       -LightDir,
                                                       Vector3.Up);

            // Transform the positions of the corners into the direction of the light
            for (int i = 0; i < WorldBBCoordinates.Length; i++)
            {
                WorldBBCoordinates[i] = Vector3.Transform(WorldBBCoordinates[i], lightRotation);
            }

            // Find the smallest box around the points
            BoundingBox lightBox = BoundingBox.CreateFromPoints(WorldBBCoordinates);

            Vector3 boxSize = lightBox.Max - lightBox.Min;
            Vector3 halfBoxSize = boxSize * 0.5f;

            // The position of the light should be in the center of the back
            // pannel of the box. 
            Vector3 lightPosition = lightBox.Min + halfBoxSize;
            lightPosition.Z = lightBox.Min.Z;

            // We need the position back in world coordinates so we transform 
            // the light position by the inverse of the lights rotation
            // Using transpose instead of invert as it's the same thing when it comes to rotation matrices.
            lightPosition = Vector3.Transform(lightPosition,
                                              Matrix.Transpose(lightRotation));

            // Create the view matrix for the light
            Matrix lightView = Matrix.CreateLookAt(lightPosition,
                                                   lightPosition - LightDir,
                                                   Vector3.Up);

            COrthoCamera newCamera = new COrthoCamera(boxSize.X, boxSize.Y, -boxSize.Z, boxSize.Z, lightView, LightDir);

            return newCamera;
        }

        // This needs to be placed somewhere else.
        // Creating the view and projection matrices for shadow casting - only for directional lights.
        public virtual COrthoCamera CreateLightViewProjectionMatrix(float Near, float Far, float Scale, Vector3 LightDir)
        {
            return null;
        }
        protected COrthoCamera CreateLightViewProjectionMatrix(float Near, float Far, float Scale, Vector3 LightDir, float FOV, float AspectRatio, Vector3 Position)
        {
            // Matrix with that will rotate in points the direction of the light
            Matrix lightRotation = Matrix.CreateLookAt(Vector3.Zero,
                                                       -LightDir,
                                                       Vector3.Up);

            Vector3[] WorldBBCoordinates = CalculateFrustumCorners(Near, Far, Scale, FOV, AspectRatio, Position);

            // Transform the positions of the corners into the direction of the light
            for (int i = 0; i < WorldBBCoordinates.Length; i++)
            {
                WorldBBCoordinates[i] = Vector3.Transform(WorldBBCoordinates[i], lightRotation);
            }

            // Find the smallest box around the points
            BoundingBox lightBox = BoundingBox.CreateFromPoints(WorldBBCoordinates);

            Vector3 boxSize = lightBox.Max - lightBox.Min;
            Vector3 halfBoxSize = boxSize * 0.5f;

            // The position of the light should be in the center of the back
            // pannel of the box. 
            Vector3 lightPosition = lightBox.Min + halfBoxSize;
            lightPosition.Z = lightBox.Min.Z;

            // We need the position back in world coordinates so we transform 
            // the light position by the inverse of the lights rotation
            // Using transpose instead of invert as it's the same thing when it comes to rotation matrices.
            lightPosition = Vector3.Transform(lightPosition,
                                              Matrix.Transpose(lightRotation));

            // This hack fixes disappearing shadows and wrong shadows.
            float fix_shadow_val = 50.0f;

            // Create the view matrix for the light
            // Increase the 20.0f to stop the shadows to disappear.
            Matrix lightView = Matrix.CreateLookAt(lightPosition + (LightDir * fix_shadow_val),
                                                   lightPosition - LightDir,
                                                   Vector3.Up);

            float fScaleX = 2.0f / (lightBox.Max.X - lightBox.Min.X);
            float fScaleY = 2.0f / (lightBox.Min.Y - lightBox.Min.Y);

            float fOffsetX = -0.5f * (lightBox.Max.X - lightBox.Min.X) * fScaleX;
            float fOffsetY = -0.5f * (lightBox.Min.Y - lightBox.Min.Y) * fScaleY;

            Matrix mCropView = new Matrix(fScaleX, 0.0f, 0.0f, 0.0f,
                                            0.0f, fScaleY, 0.0f, 0.0f,
                                            0.0f, 0.0f, 1.0f, 0.0f,
                                            fOffsetX, fOffsetY, 0.0f, 1.0f);

            COrthoCamera newCamera = new COrthoCamera(boxSize.X, boxSize.Y, -boxSize.Z, boxSize.Z + fix_shadow_val, lightView, LightDir);

            return newCamera;
        }

        private Vector3[] CalculateFrustumCorners(float Near, float Far, float Scale, float FOV, float AspectRatio, Vector3 Position)
        {
            Vector3 vZ = m_dir;

            Vector3 vX;
            vX = CToolbox.CrossProduct(Vector3.Up,vZ);
            vX = CToolbox.UnitVector(vX);

            Vector3 vY;
            vY = CToolbox.CrossProduct(vZ,vX);


            float fNearPlaneHeight = (float)Math.Tan(FOV * 0.5f) * Near;
            float fNearPlaneWidth = fNearPlaneHeight * AspectRatio;

            float fFarPlaneHeight = (float)Math.Tan(FOV * 0.5f) * Far;
            float fFarPlaneWidth = fFarPlaneHeight * AspectRatio;

            Vector3 vNearPlaneCenter = Position + vZ * Near;
            Vector3 vFarPlaneCenter = Position + vZ * Far;

            Vector3[] points = new Vector3[8];

            points[0] = vNearPlaneCenter - vX * fNearPlaneWidth - vY * fNearPlaneHeight;
            points[1] = vNearPlaneCenter - vX * fNearPlaneWidth + vY * fNearPlaneHeight;
            points[2] = vNearPlaneCenter + vX * fNearPlaneWidth + vY * fNearPlaneHeight;
            points[3] = vNearPlaneCenter + vX * fNearPlaneWidth - vY * fNearPlaneHeight;

            points[4] = vFarPlaneCenter - vX * fFarPlaneWidth - vY * fFarPlaneHeight;
            points[5] = vFarPlaneCenter - vX * fFarPlaneWidth + vY * fFarPlaneHeight;
            points[6] = vFarPlaneCenter + vX * fFarPlaneWidth + vY * fFarPlaneHeight;
            points[7] = vFarPlaneCenter + vX * fFarPlaneWidth - vY * fFarPlaneHeight;

            // calculate center of points
            Vector3 vCenter = new Vector3(0,0,0);
            for (int i = 0; i < 8; i++)
            {
                vCenter += points[i];
            }

            vCenter/=8;

            // for each point
            for(int i=0;i<8;i++)
            {
                // scale by adding offset from center
                points[i] += (points[i] - vCenter) * (Scale - 1);
            }

            return points;
        }

        public float[] CalculateSplitDistances(int NumSplits, ref float Lambda, float NewFar)
        {
            MathHelper.Clamp(Lambda, 0.0f, 1.0f);

            float[] near_far_array = new float[NumSplits + 1];

            for (int i = 1; i < NumSplits; i++)
            {
                float pow = i / (float)NumSplits;
                float log = m_near * (float)Math.Pow((NewFar / m_near), pow);
                float uniform = m_near + (NewFar - m_near) * pow;
                near_far_array[i] = Lambda * log + (1 - Lambda) * uniform;
            }

            // make sure border values are right
            near_far_array[0] = m_near;
            near_far_array[NumSplits] = NewFar;

            return near_far_array;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////
    }
}

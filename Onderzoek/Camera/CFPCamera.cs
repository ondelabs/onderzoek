﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Onderzoek.Camera
{
    public class CFPCamera : CCamera
    {
        protected Quaternion m_rotation;
        protected Matrix m_matRot;
        protected Vector3 m_position;
        protected float m_aspectRatio = 0;
        protected float m_fov = 0;
        protected float m_farHeight = 0;
        protected float m_farWidth = 0;

        Onderzoek.Input.CInputData m_inputData;


        public override void SetPosition(Vector3 Position)
        {
            m_position = Position;
        }


        public override void UpdateProjectionMatrix(float FOV, float AspectRatio, float Near, float Far)
        {
            m_fov = FOV;
            AspectRatio = m_aspectRatio;
            m_near = Near;
            m_far = Far;

            m_projection = Matrix.CreatePerspectiveFieldOfView(m_fov, AspectRatio, m_near, m_far);
            m_invProjection = Matrix.Invert(m_projection);
        }

        public override Vector3 GetPosition()
        {
            return m_position;
        }

        

        public override Quaternion GetRotation()
        {
            return m_rotation;
        }
        public override void SetRotation(Quaternion Rotation)
        {
            m_rotation = Rotation;
        }

        
        public override Matrix GetRotationMatrix()
        {
            return m_matRot;
        }
        public override void SetRotationMatrix(Matrix RotationMatrix)
        {
            m_matRot = RotationMatrix;
        }

        /// <summary>
        /// Create a new instance of a camera. This should be used when you want the input to directly control the camera.
        /// </summary>
        /// <param name="AspectRatio">Aspect ratio, which is width divided by height</param>
        /// <param name="FOV">Field of View, angle in degrees.</param>
        /// <param name="Near"></param>
        /// <param name="Far"></param>
        /// <param name="Position"></param>
        /// <param name="Rotation"></param>
        /// <param name="InputData">The input data instance.</param>
        public CFPCamera(float AspectRatio, float FOV, float Near, float Far, Vector3 Position, Vector3 Rotation, Onderzoek.Input.CInputData InputData)
        {
            m_inputData = InputData;
            m_aspectRatio = AspectRatio;

            m_fov = MathHelper.ToRadians(FOV) / 2.0f;
            m_near = Near;
            m_far = Far;
            m_position = Position;
            m_rotation = Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z);
            m_projection = Matrix.CreatePerspectiveFieldOfView(m_fov, AspectRatio, m_near, m_far);
            m_invProjection = Matrix.Invert(m_projection);
            m_view = Matrix.CreateLookAt(m_position, Vector3.Zero, Vector3.Up);
            m_dir = Vector3.Zero;// TranslateAUnitDistForward() - m_position;
            m_clipPlanes = new Plane[6];

            m_farHeight = (2.0f * (float)(Math.Tan(m_fov / 2.0f)) * m_far) * 0.5f;
            m_farWidth = ((m_farHeight * 2.0f) * m_aspectRatio) * 0.5f;
        }

        public CFPCamera(float AspectRatio, float FOV, float Near, float Far, Vector3 Position, Vector3 Rotation)
        {
            m_aspectRatio = AspectRatio;

            m_fov = MathHelper.ToRadians(FOV) / 2.0f;
            m_near = Near;
            m_far = Far;
            m_position = Position;
            m_rotation = Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z);
            m_projection = Matrix.CreatePerspectiveFieldOfView(m_fov, AspectRatio, m_near, m_far);
            m_invProjection = Matrix.Invert(m_projection);
            m_view = Matrix.CreateLookAt(m_position, Vector3.Zero, Vector3.Up);
            m_dir = Vector3.Zero;// TranslateAUnitDistForward() - m_position;
            m_clipPlanes = new Plane[6];

            m_farHeight = (2.0f * (float)(Math.Tan(m_fov / 2.0f)) * m_far) * 0.5f;
            m_farWidth = ((m_farHeight * 2.0f) * m_aspectRatio) * 0.5f;
        }

        ~CFPCamera()
        {
            m_inputData = null;
        }

        public virtual void Update(Vector3 Position, Vector3 Direction, Matrix Rotation, Matrix PlayerWorld)
        {
            if (m_inputData != null && m_inputData.m_rightClick == false)
            {
                m_savePrvData = false;
            }

            m_position = Position;
            m_dir = Direction;
            m_matRot = Rotation;
            m_invView = PlayerWorld;
            m_view = Matrix.Invert(PlayerWorld);

            m_vp = m_view * m_projection;

            // Not needed unless the Aspect ratio or any of the other arguments change.
            //m_projection = Matrix.CreatePerspectiveFieldOfView(m_fov, AspectRatio, m_near, m_far);

            // m_inputData.m_leftClick is being used for DEBUG purposes only.
            if (m_inputData != null && m_inputData.m_leftClick == false)
            {
                CreateViewFrustumClipPlanes(m_clipPlanes, true);
            }
        }

        public virtual void Update(Vector3 Position, Matrix Rotation)
        {
            if (m_inputData != null && m_inputData.m_rightClick == false)
            {
                m_savePrvData = false;
            }

            //System.Diagnostics.Trace.WriteLine(Position + "\n");
            m_position = Position;
            m_matRot = Rotation;
            Matrix translation = Matrix.CreateTranslation(m_position);
            m_invView = m_matRot * translation;
            // Remember, the objects in the world need to move into view space, which is where the camera is at the origin.
            // Instead of inverting with an invert function (too expensive) just do this to invert invView matrix.
            m_view = Matrix.Transpose(m_matRot) * (-translation);
            m_dir = m_matRot.Forward;

            m_vp = m_view * m_projection;

            // Not needed unless the Aspect ratio or any of the other arguments change.
            //m_projection = Matrix.CreatePerspectiveFieldOfView(m_fov, AspectRatio, m_near, m_far);

            // m_inputData.m_leftClick is being used for DEBUG purposes only.
            if (m_inputData != null && m_inputData.m_leftClick == false)
            {
                CreateViewFrustumClipPlanes(m_clipPlanes, true);
            }
        }

        public virtual void Update()
        {
            if (m_inputData == null)
            {
                return;
            }

            /////////////////////////////Look at maths//////////////////////////////
            m_pitch -= m_inputData.m_rightStick.Y * 0.1f;

            //constrain pitch to vertical to avoid confusion
            m_pitch = MathHelper.Clamp(m_pitch, -(MathHelper.PiOver2) + .0001f,
                (MathHelper.PiOver2) - .0001f);

            m_yaw -= m_inputData.m_rightStick.X * 0.1f;

            m_yaw = MathHelper.WrapAngle(m_yaw);

            //float mod yaw to avoid eventual precision errors
            //as we move away from 0
            m_yaw = m_yaw % MathHelper.TwoPi;

            //create a new aspect based on pitch and yaw
            m_rotation = Quaternion.CreateFromAxisAngle(Vector3.Up, -m_yaw) *
                Quaternion.CreateFromAxisAngle(Vector3.Right, m_pitch);

            //normalize to reduce errors
            Quaternion.Normalize(ref m_rotation, out m_rotation);
            ////////////////////////////////////////////////////////////////////////

            Vector3 v = new Vector3(m_inputData.m_leftStick.X * 0.4f, 0, -m_inputData.m_leftStick.Y * 0.4f);
            m_position += Vector3.Transform(v, m_rotation);

            m_matRot = Matrix.CreateFromQuaternion(m_rotation);
            Matrix translation = Matrix.CreateTranslation(m_position);
            m_invView = m_matRot * translation;
            // Remember, the objects in the world need to move into view space, which is where the camera is at the origin.
            // Instead of inverting with an invert function (too expensive) just do this to invert invView matrix.
            m_view = Matrix.Transpose(m_matRot) * (-translation);
            m_dir = m_matRot.Forward;

            m_vp = m_view * m_projection;

            // Not needed unless the Aspect ratio or any of the other arguments change.
            //m_projection = Matrix.CreatePerspectiveFieldOfView(m_fov, AspectRatio, m_near, m_far);

            // m_inputData.m_leftClick is being used for DEBUG purposes only.
            if (m_inputData.m_leftClick == false)
            {
                CreateViewFrustumClipPlanes(m_clipPlanes, true);
            }
        }

        public override void Update(Vector3 ChangeInPosSinceLastCameraUpdate, Vector2 ChangeInRotSinceLastCameraUpdate)
        {
            /////////////////////////////Look at maths//////////////////////////////
            m_pitch -= ChangeInRotSinceLastCameraUpdate.Y;

            //constrain pitch to vertical to avoid confusion
            m_pitch = MathHelper.WrapAngle(m_pitch);

            m_yaw -= ChangeInRotSinceLastCameraUpdate.X;

            m_yaw = MathHelper.WrapAngle(m_yaw);

            //float mod yaw to avoid eventual precision errors
            //as we move away from 0
            m_yaw = m_yaw % MathHelper.TwoPi;

            //create a new aspect based on pitch and yaw
            m_rotation = Quaternion.CreateFromAxisAngle(Vector3.Up, -m_yaw) *
                Quaternion.CreateFromAxisAngle(Vector3.Right, m_pitch);

            //normalize to reduce errors
            Quaternion.Normalize(ref m_rotation, out m_rotation);
            ////////////////////////////////////////////////////////////////////////

            m_position += Vector3.Transform(ChangeInPosSinceLastCameraUpdate, m_rotation);

            m_matRot = Matrix.CreateFromQuaternion(m_rotation);
            Matrix translation = Matrix.CreateTranslation(m_position);
            m_invView = m_matRot * translation;
            // Remember, the objects in the world need to move into view space, which is where the camera is at the origin.
            // Instead of inverting with an invert function (too expensive) just do this to invert invView matrix.
            m_view = Matrix.Transpose(m_matRot) * (-translation);
            m_dir = m_matRot.Forward;

            m_vp = m_view * m_projection;

            // Not needed unless the Aspect ratio or any of the other arguments change.
            //m_projection = Matrix.CreatePerspectiveFieldOfView(m_fov, AspectRatio, m_near, m_far);
        }

        public override void Refresh()
        {
            // not good, needs to be fixed.
            m_matRot = Matrix.CreateFromQuaternion(m_rotation);
            Matrix translation = Matrix.CreateTranslation(m_position);
            m_invView = m_matRot * translation;
            // Remember, the objects in the world need to move into view space, which is where the camera is at the origin.
            // Instead of inverting with an invert function (too expensive) just do this to invert invView matrix.
            m_view = Matrix.Transpose(m_matRot) * (-translation);
            m_vp = m_view * m_projection;
            m_dir = m_matRot.Forward;
        }

        public override void GetViewFrustumCorners(out Vector3[] Corners)
        {
            GetViewFrustumCorners(out Corners, m_position, m_matRot, m_farHeight, m_farWidth);
        }

        public override COrthoCamera CreateLightViewProjectionMatrix(float Near, float Far, float Scale, Vector3 LightDir)
        {
            return CreateLightViewProjectionMatrix(Near, Far, Scale, LightDir, m_fov, m_aspectRatio, m_position);
        }
    }
}

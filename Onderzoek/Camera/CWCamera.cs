﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using Onderzoek;
using Onderzoek.Camera;
using Onderzoek.SpatialTree;
using Onderzoek.Visual;
using Onderzoek.Screens;
using Onderzoek.Entity;
using Onderzoek.Physics;

namespace Onderzoek.Camera
{
    public class CWCamera : C3PCamera
    {
        public CWCamera()
        {
        }

        public override void SetPosition(Vector3 Position)
        {
            m_position = Position;
        }

        public override void SetRotationMatrix(Matrix RotationMatrix)
        {
            m_matRot = RotationMatrix;
        }
    }
}

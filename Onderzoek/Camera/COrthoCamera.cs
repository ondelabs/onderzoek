﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Onderzoek.Camera
{
    public class COrthoCamera : CCamera
    {
        //Matrix m_view;
        //Matrix m_projection;
        //Matrix m_vp;
        //float m_near;
        //float m_far;

        public COrthoCamera(float Width, float Height, float Near, float Far, Matrix View, Vector3 LightDir, Matrix Scale)
        {
            m_clipPlanes = new Plane[6];

            m_near = Near;
            m_far = Far;

            m_view = View;
            m_projection = Matrix.CreateOrthographic(Width, Height, m_near, m_far) * Scale;

            m_vp = m_view * m_projection;
            m_dir = LightDir;

            CreateViewFrustumClipPlanes(m_clipPlanes, true);
        }

        public COrthoCamera(float Width, float Height, float Near, float Far, Matrix View, Vector3 LightDir)
        {
            m_clipPlanes = new Plane[6];

            m_near = Near;
            m_far = Far;

            m_view = View;
            m_projection = Matrix.CreateOrthographic(Width, Height, m_near, m_far);

            m_vp = m_view * m_projection;
            m_dir = LightDir;

            CreateViewFrustumClipPlanes(m_clipPlanes, true);
        }

        public COrthoCamera(float Left, float Right, float Bottom, float Top, float Near, float Far, Matrix View, Vector3 LightDir)
        {
            m_clipPlanes = new Plane[6];

            m_near = Near;
            m_far = Far;

            m_view = View;
            m_projection = Matrix.CreateOrthographicOffCenter(Left, Right, Bottom, Top, m_near, m_far);

            m_vp = m_view * m_projection;
            m_dir = LightDir;

            CreateViewFrustumClipPlanes(m_clipPlanes, true);
        }

        public Matrix ViewMatrix
        {
            get { return m_view; }
            set {
                    m_view = value;
                    m_vp = m_view * m_projection;
                }
        }

        public Matrix ProjectionMatrix
        {
            get { return m_projection; }
        }

        public Matrix ViewProjectionMatrix
        {
            get { return m_vp; }
        }

        public float NearClip
        {
            get { return m_near; }
        }

        public float FarClip
        {
            get { return m_far; }
        }

        public override void UpdateProjectionMatrix(float FOV, float AspectRatio, float Near, float Far)
        {
        }
    }
}

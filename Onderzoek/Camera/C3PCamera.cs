﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

#if EDITOR
using Onderzoek.Visual;
#endif

namespace Onderzoek.Camera
{
    public class C3PCamera : CCamera
    {
        protected Quaternion m_rotation;
        protected Quaternion m_origRotation;
        protected Matrix m_matRot;
        protected Vector3 m_position;
        protected float m_aspectRatio = 0;
        protected float m_fov = 0;
        protected float m_farHeight = 0;
        protected float m_farWidth = 0;
        protected float m_distFrmInterest;
        protected Onderzoek.Input.CInputData m_inputData;

#if EDITOR
        Vector3 m_interest;
        bool m_fpMode = false;
        float m_originalDist = 0;

        public override void SetPosition(Vector3 Position)
        {
            m_interest = Position;

            m_position = m_interest - (m_matRot.Forward * m_distFrmInterest);
        }

        public void ToggleFPMode()
        {
            if (m_fpMode == true)
            {
                m_distFrmInterest = m_originalDist;
                m_fpMode = false;

                m_interest = m_position + (m_matRot.Forward * m_distFrmInterest);
            }
            else
            {
                m_originalDist = m_distFrmInterest;
                m_fpMode = true;
                m_interest = m_position;
                m_distFrmInterest = 0;
            }
        }
#endif

        public float DistFrmInterest
        {
            get{ return m_distFrmInterest; }
            set
            {
#if ENGINE
                if (value < 0)
                {
                    m_distFrmInterest = 0;
                }
                else
                {
                    m_distFrmInterest = value;
                }
#else
                if (m_fpMode == true)
                {
                    m_interest = m_position + (m_matRot.Forward * -value);
                    m_position = m_interest;
                }
                else
                {
                    if (value < 0)
                    {
                        m_distFrmInterest = 0;
                    }
                    else
                    {
                        m_distFrmInterest = value;
                    }
                }
#endif
            }
        }

        public override void UpdateProjectionMatrix(float FOV, float AspectRatio, float Near, float Far)
        {
            m_fov = FOV;
            AspectRatio = m_aspectRatio;
            m_near = Near;
            m_far = Far;

            m_projection = Matrix.CreatePerspectiveFieldOfView(m_fov, AspectRatio, m_near, m_far);
            m_invProjection = Matrix.Invert(m_projection);

            m_farHeight = (2.0f * (float)(Math.Tan(m_fov / 2.0f)) * m_far) * 0.5f;
            m_farWidth = ((m_farHeight * 2.0f) * m_aspectRatio) * 0.5f;
        }

        public override Vector3 GetPosition()
        {
            return m_position;
        }

        public override Quaternion GetRotation()
        {
            return m_rotation;
        }
        public override void SetRotation(Quaternion Rotation)
        {
            m_rotation = Rotation;
        }

        public override Matrix GetRotationMatrix()
        {
            return m_matRot;
        }
        public override void SetRotationMatrix(Matrix RotationMatrix)
        {
            m_matRot = RotationMatrix;
        }

        public C3PCamera()
        {
        }

        public C3PCamera(float AspectRatio, float FOV, float Near, float Far, Vector3 Position, Vector3 Rotation, float DistFrmInterest,
            Onderzoek.Input.CInputData InputData)
        {
            m_inputData = InputData;

            m_aspectRatio = AspectRatio;

            m_fov = MathHelper.ToRadians(FOV) / 2.0f;
            m_near = Near;
            m_far = Far;
            m_position = Position;
            m_rotation = Quaternion.CreateFromYawPitchRoll(Rotation.X, Rotation.Y, Rotation.Z);
            m_origRotation = m_rotation;

            m_yaw = Rotation.Y;
            m_pitch = Rotation.X;

            m_projection = Matrix.CreatePerspectiveFieldOfView(m_fov, AspectRatio, m_near, m_far);
            m_invProjection = Matrix.Invert(m_projection);
            m_view = Matrix.CreateLookAt(m_position, Vector3.Zero, Vector3.Up);
            m_dir = Vector3.Zero;// TranslateAUnitDistForward() - m_position;
            m_clipPlanes = new Plane[6];
            m_distFrmInterest = DistFrmInterest;

            m_farHeight = (2.0f * (float)(Math.Tan(m_fov / 2.0f)) * m_far) * 0.5f;
            m_farWidth = ((m_farHeight * 2.0f) * m_aspectRatio) * 0.5f;
        }

        ~C3PCamera()
        {
            m_inputData = null;
        }

        /// <summary>
        /// Use this when you wish the camera to rotate along with another entity.
        /// </summary>
        /// <param name="PlayerPosition">InterestPosition is where you would like the camera to look at.</param>
        /// <param name="PlayerRotation">InterestRotation is the rotation you want the camera to use.</param>
        public virtual void Update(Vector3 InterestPosition, Quaternion InterestRotation)
        {
            // For rotation of camera
            m_rotation = InterestRotation;
            m_matRot = Matrix.CreateFromQuaternion(m_rotation);

            m_position = InterestPosition - (m_matRot.Forward * m_distFrmInterest);

            Matrix translation = Matrix.CreateTranslation(m_position);
            m_invView = m_matRot * translation;
            // Remember, the objects in the world need to move into view space, which is where the camera is at the origin.
            // Instead of inverting with an invert function (too expensive) just do this to invert invView matrix.
            m_view = Matrix.Transpose(m_matRot) * (-translation);
            m_dir = m_matRot.Forward;

            m_vp = m_view * m_projection;

            // m_inputData.m_leftClick is being used for DEBUG purposes only.
            if (m_inputData.m_leftClick == false)
            {
                CreateViewFrustumClipPlanes(m_clipPlanes, true);
            }
        }

        /// <summary>
        /// Use this when you want the input to directly control the camera's direction.
        /// </summary>
        /// <param name="InterestPosition">InterestPosition is where you would like the camera to look at.</param>
        public virtual void Update(Vector3 InterestPosition)
        {
            if (m_inputData.m_rightClick == false)
            {
                m_savePrvData = false;
            }

            // For rotation of camera
            /////////////////////////////Look at maths//////////////////////////////
            m_pitch -= m_inputData.m_rightStick.Y * 0.1f;

            //constrain pitch to vertical to avoid confusion
            m_pitch = MathHelper.Clamp(m_pitch, -(MathHelper.PiOver2) + .0001f,
                (MathHelper.PiOver2) - .0001f);

            m_yaw -= m_inputData.m_rightStick.X * 0.1f;

            m_yaw = MathHelper.WrapAngle(m_yaw);

            //float mod yaw to avoid eventual precision errors
            //as we move away from 0
            m_yaw = m_yaw % MathHelper.TwoPi;

            m_rotation = Quaternion.CreateFromAxisAngle(Vector3.Up, -m_yaw) *
                Quaternion.CreateFromAxisAngle(Vector3.Right, m_pitch);

            //normalize to reduce errors
            Quaternion.Normalize(ref m_rotation, out m_rotation);
            ////////////////////////////////////////////////////////////////////////

            m_matRot = Matrix.CreateFromQuaternion(m_rotation);

            m_position = InterestPosition - (m_matRot.Forward * m_distFrmInterest);

            Matrix translation = Matrix.CreateTranslation(m_position);
            m_invView = m_matRot * translation;
            // Remember, the objects in the world need to move into view space, which is where the camera is at the origin.
            // Instead of inverting with an invert function (too expensive) just do this to invert invView matrix.
            m_view = Matrix.Transpose(m_matRot) * (-translation);
            m_dir = m_matRot.Forward;

            m_vp = m_view * m_projection;

            // m_inputData.m_leftClick is being used for DEBUG purposes only.
            if (m_inputData.m_leftClick == false)
            {
                CreateViewFrustumClipPlanes(m_clipPlanes, true);
            }
        }

        public override void GetViewFrustumCorners(out Vector3[] Corners)
        {
            GetViewFrustumCorners(out Corners, m_position, m_matRot, m_farHeight, m_farWidth);
        }

        public override COrthoCamera CreateLightViewProjectionMatrix(float Near, float Far, float Scale, Vector3 LightDir)
        {
            return CreateLightViewProjectionMatrix(Near, Far, Scale, LightDir, m_fov, m_aspectRatio, m_position);
        }

#if EDITOR
        public override void Update(Vector3 ChangeInPosSinceLastCameraUpdate, Vector2 ChangeInRotSinceLastCameraUpdate)
        {
            // For rotation of camera
            /////////////////////////////Look at maths//////////////////////////////
            m_pitch += ChangeInRotSinceLastCameraUpdate.Y;

            //constrain pitch to vertical to avoid confusion
            m_pitch = MathHelper.Clamp(m_pitch, -(MathHelper.PiOver2) + .0001f,
                (MathHelper.PiOver2) - .0001f);

            m_yaw -= ChangeInRotSinceLastCameraUpdate.X;

            m_yaw = MathHelper.WrapAngle(m_yaw);

            //float mod yaw to avoid eventual precision errors
            //as we move away from 0
            m_yaw = m_yaw % MathHelper.TwoPi;

            m_rotation = Quaternion.CreateFromAxisAngle(Vector3.Up, -m_yaw) *
                Quaternion.CreateFromAxisAngle(Vector3.Right, m_pitch);

            //normalize to reduce errors
            Quaternion.Normalize(ref m_rotation, out m_rotation);
            ////////////////////////////////////////////////////////////////////////

            m_matRot = Matrix.CreateFromQuaternion(m_rotation);

            m_interest += Vector3.Transform(ChangeInPosSinceLastCameraUpdate, m_rotation);

            m_position = m_interest - (m_matRot.Forward * m_distFrmInterest);

            Matrix translation = Matrix.CreateTranslation(m_position);
            m_invView = m_matRot * translation;
            // Remember, the objects in the world need to move into view space, which is where the camera is at the origin.
            // Instead of inverting with an invert function (too expensive) just do this to invert invView matrix.
            m_view = Matrix.Transpose(m_matRot) * (-translation);
            m_dir = m_matRot.Forward;

            m_vp = m_view * m_projection;

            CVisual.Instance.RemoveLinesWithThisColour(new Microsoft.Xna.Framework.Graphics.Color(250, 0, 0));
            CVisual.Instance.AddToLineList(m_interest, m_interest + Vector3.Right, new Microsoft.Xna.Framework.Graphics.Color(250, 0, 0));

            CVisual.Instance.RemoveLinesWithThisColour(new Microsoft.Xna.Framework.Graphics.Color(0, 250, 0));
            CVisual.Instance.AddToLineList(m_interest, m_interest + Vector3.Backward, new Microsoft.Xna.Framework.Graphics.Color(0, 250, 0));

            CVisual.Instance.RemoveLinesWithThisColour(new Microsoft.Xna.Framework.Graphics.Color(0, 0, 250));
            CVisual.Instance.AddToLineList(m_interest, m_interest + Vector3.Up, new Microsoft.Xna.Framework.Graphics.Color(0, 0, 250));
        }

        public override void Refresh()
        {
            m_matRot = Matrix.CreateFromQuaternion(m_rotation);
            Matrix translation = Matrix.CreateTranslation(m_position);
            m_invView = m_matRot * translation;
            // Remember, the objects in the world need to move into view space, which is where the camera is at the origin.
            // Instead of inverting with an invert function (too expensive) just do this to invert invView matrix.
            m_view = Matrix.Transpose(m_matRot) * (-translation);
            m_vp = m_view * m_projection;
            m_dir = m_matRot.Forward;
        }
#endif
    }
}

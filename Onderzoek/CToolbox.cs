using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Onderzoek.Entity;
using Onderzoek.Camera;
using Onderzoek.SpatialTree;

namespace Onderzoek
{
    static public class CToolbox
    {
        #region Trig
        /// <summary>
        /// Trigonometry triangle layout for following functions:
        /// 
        ///                - |
        ///       3     - B  |
        ///          -       | 1
        ///       -          |
        ///    - A           |
        /// -________________|
        ///         2
        /// </summary>
        public static float FindSide1WithSide2Side3(float Side2, float Side3)
        {
            return (float)Math.Sqrt((Side3 * Side3) - (Side2 * Side2));
        }

        public static float FindSide2WithSide1Side3(float Side1, float Side3)
        {
            return (float)Math.Sqrt((Side3 * Side3) - (Side1 * Side1));
        }

        public static float FindSide3WithSide1Side2(float Side1, float Side2)
        {
            return (float)Math.Sqrt((Side1 * Side1) + (Side2 * Side2));
        }

        public static float FindSide1InTriangleWithSide2AngleB(float Side2, float AngleB)
        {
            return Side2 / (float)Math.Tan(CToolbox.FindRadian(AngleB));
        }

        public static float FindSide1InTriangleWithSide2AngleA(float Side2, float AngleA)
        {
            return (float)Math.Tan(CToolbox.FindRadian(AngleA)) * Side2;
        }

        public static float FindSide1InTriangleWithSide3AngleB(float Side3, float AngleB)
        {
            return (float)Math.Cos(CToolbox.FindRadian(AngleB)) * Side3;
        }

        public static float FindSide1InTriangleWithSide3AngleA(float Side3, float AngleA)
        {
            return (float)Math.Sin(CToolbox.FindRadian(AngleA)) * Side3;
        }

        public static float FindSide2InTriangleWithSide1AngleB(float Side1, float AngleB)
        {
            return (float)Math.Tan(CToolbox.FindRadian(AngleB)) * Side1;
        }

        public static float FindSide2InTriangleWithSide1AngleA(float Side1, float AngleA)
        {
            return Side1 / (float)Math.Tan(CToolbox.FindRadian(AngleA));
        }

        public static float FindSide2InTriangleWithSide3AngleB(float Side3, float AngleB)
        {
            return (float)Math.Sin(CToolbox.FindRadian(AngleB)) * Side3;
        }

        public static float FindSide2InTriangleWithSide3AngleA(float Side3, float AngleA)
        {
            return (float)Math.Cos(CToolbox.FindRadian(AngleA)) * Side3;
        }

        public static float FindSide3InTriangleWithSide1AngleB(float Side1, float AngleB)
        {
            return Side1 / (float)Math.Cos(CToolbox.FindRadian(AngleB));
        }

        public static float FindSide3InTriangleWithSide1AngleA(float Side1, float AngleA)
        {
            return Side1 / (float)Math.Sin(CToolbox.FindRadian(AngleA));
        }

        public static float FindSide3InTriangleWithSide2AngleB(float Side2, float AngleB)
        {
            return Side2 / (float)Math.Sin(CToolbox.FindRadian(AngleB));
        }

        public static float FindSide3InTriangleWithSide2AngleA(float Side2, float AngleA)
        {
            return Side2 / (float)Math.Cos(CToolbox.FindRadian(AngleA));
        }

        public static float FindAngleBInTriangleWithSide1Side2(float Side1, float Side2)
        {
            return FindDegree((float)Math.Atan(Side2 / Side1));
        }

        public static float FindAngleBInTriangleWithSide1Side3(float Side1, float Side3)
        {
            return FindDegree((float)Math.Acos(Side1 / Side3));
        }

        public static float FindAngleBInTriangleWithSide2Side3(float Side2, float Side3)
        {
            return FindDegree((float)Math.Asin(Side2 / Side3));
        }

        public static float FindAngleAInTriangleWithSide1Side2(float Side1, float Side2)
        {
            return FindDegree((float)Math.Atan(Side1 / Side2));
        }

        public static float FindAngleAInTriangleWithSide1Side3(float Side1, float Side3)
        {
            return FindDegree((float)Math.Asin(Side1 / Side3));
        }

        public static float FindAngleAInTriangleWithSide2Side3(float Side2, float Side3)
        {
            return FindDegree((float)Math.Acos(Side2 / Side3));
        }
        #endregion

        // Expand the parent bounding box to fit the child bounding box.
        public static bool ExpandParentBBToFitChildBB(ref Vector3 PMin, ref Vector3 PMax, Vector3 CMin, Vector3 CMax)
        {
            bool rtn = false;

            if (PMin.X > CMin.X)
            {
                PMin.X = CMin.X;
                rtn = true;
            }
            if (PMin.Y > CMin.Y)
            {
                PMin.Y = CMin.Y;
                rtn = true;
            }
            if (PMin.Z > CMin.Z)
            {
                PMin.Z = CMin.Z;
                rtn = true;
            }

            if (PMax.X < CMax.X)
            {
                PMax.X = CMax.X;
                rtn = true;
            }
            if (PMax.Y < CMax.Y)
            {
                PMax.Y = CMax.Y;
                rtn = true;
            }
            if (PMax.Z < CMax.Z)
            {
                PMax.Z = CMax.Z;
                rtn = true;
            }

            return rtn;
        }

        // From http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToEuler/index.htm 
        public static Vector3 QuaternionToEuler(Quaternion q)
        {
            Vector3 v = Vector3.Zero;

            v.X = (float)Math.Atan2
            (
                2 * q.Y * q.W - 2 * q.X * q.Z,
                   1 - 2 * Math.Pow(q.Y, 2) - 2 * Math.Pow(q.Z, 2)
            );

            v.Z = (float)Math.Asin
            (
                CToolbox.Clamp(2 * q.X * q.Y + 2 * q.Z * q.W, 0, 1.0f)
            );

            v.Y = (float)Math.Atan2
            (
                2 * q.X * q.W - 2 * q.Y * q.Z,
                1 - 2 * Math.Pow(q.X, 2) - 2 * Math.Pow(q.Z, 2)
            );

            if (q.X * q.Y + q.Z * q.W == 0.5)
            {
                v.X = (float)(2 * Math.Atan2(q.X, q.W));
                v.Y = 0;
            }

            else if (q.X * q.Y + q.Z * q.W == -0.5)
            {
                v.X = (float)(-2 * Math.Atan2(q.X, q.W));
                v.Y = 0;
            }

            return v;
        }

        // Calculates the minimum and maximum vectors for an AABB.
        public static void GetMinMax(Vector3[] Corners, out BoundingBox BB)
        {
            GetMinMax(Corners, out BB.Min, out BB.Max);
        }

        public static void GetMinMax(Vector3[] Corners, out Vector3 Min, out Vector3 Max)
        {
            Min = Corners[0];
            Max = Corners[0];

            for (int i = 1; i < Corners.Length; ++i)
            {
                if (Corners[i].X < Min.X)
                {
                    Min.X = Corners[i].X;
                }
                if (Corners[i].Y < Min.Y)
                {
                    Min.Y = Corners[i].Y;
                }
                if (Corners[i].Z < Min.Z)
                {
                    Min.Z = Corners[i].Z;
                }

                if (Corners[i].X > Max.X)
                {
                    Max.X = Corners[i].X;
                }
                if (Corners[i].Y > Max.Y)
                {
                    Max.Y = Corners[i].Y;
                }
                if (Corners[i].Z > Max.Z)
                {
                    Max.Z = Corners[i].Z;
                }
            }
        }

        public static void TransformLocalToWorld(Vector3[] Local, out Vector3[] World, Matrix WorldMatrix)
        {
            World = new Vector3[Local.Length];
            for (int i = 0; i < Local.Length; ++i)
            {
                World[i] = Vector3.Transform(Local[i], WorldMatrix);
            }
        }

        public static void TransformLocalToWorld(List<Vector3> Local, out List<Vector3> World, Matrix WorldMatrix)
        {
            World = new List<Vector3>();
            for (int i = 0; i < Local.Count; ++i)
            {
                World.Add( Vector3.Transform(Local[i], WorldMatrix) );
            }
        }

        public static int Clamp(int Value, int Min, int Max)
        {
            if (Value < Min)
            {
                Value = Min;
            }
            if (Value > Max)
            {
                Value = Max;
            }

            return Value;
        }

        public static float Clamp( float Value, float Min, float Max )
        {
            if (Value < Min)
            {
                Value = Min;
            }
            if (Value > Max)
            {
                Value = Max;
            }

            return Value;
        }

        public static void Clamp(ref float Value, float Min, float Max)
        {
            if (Value < Min)
            {
                Value = Min;
            }
            if (Value > Max)
            {
                Value = Max;
            }
        }

        public static float Repeat( float Value, float Min, float Max )
        {
            if (Value < Min)
            {
                Value = Max - (Min - Value);
            }
            if (Value > Max)
            {
                Value = Value - Max;
            }

            return Value;
        }

        public static void Repeat(ref float Value, float Min, float Max)
        {
            if (Value < Min)
            {
                Value = Max - (Min - Value);
            }
            if (Value > Max)
            {
                Value = Value - Max;
            }
        }

        //Works out the cross product of the two vectors
        public static Vector3 CrossProduct( Vector3 Vector0, Vector3 Vector1 )
        {
	        Vector3 crossproductvector = Vector3.Zero;

            crossproductvector.X = (Vector0.Y * Vector1.Z) - (Vector1.Y * Vector0.Z);
            crossproductvector.Y = -((Vector0.X * Vector1.Z) - (Vector1.X * Vector0.Z));
            crossproductvector.Z = (Vector0.X * Vector1.Y) - (Vector1.X * Vector0.Y);
            
	        return crossproductvector;
        }

        //Works out the dot product
        public static float DotProduct(Vector3 Vec1, Vector3 Vec2)
        {
            return ((Vec1.X * Vec2.X) + (Vec1.Y * Vec2.Y) + (Vec1.Z * Vec2.Z));
        }

        //Works out the perp dot product
        public static float PerpDotProduct(Vector2 Vec1, Vector2 Vec2)
        {
            return (Vec1.X * Vec2.Y - Vec1.Y * Vec2.X);
        }

        /// <summary>
        /// Works out the perp dot product. This will ignore the (up) vector and use x and z.
        /// </summary>
        /// <param name="Vec1"></param>
        /// <param name="Vec2"></param>
        /// <returns>magnitude of the resulting vector.</returns>
        public static float PerpDotProduct(Vector3 Vec1, Vector3 Vec2)
        {
            return (Vec1.X * Vec2.Z - Vec1.Z * Vec2.X);
        }

        //Works out the dot product
        public static float DotProduct(Plane Vec1, Vector3 Vec2)
        {
            return ((Vec1.Normal.X * Vec2.X) + (Vec1.Normal.Y * Vec2.Y) + (Vec1.Normal.Z * Vec2.Z));
        }

        //Works out the unit vector
        public static Vector3 UnitVector(Vector3 Vec1)
        {
            float modulus = (float)Math.Sqrt((Vec1.X * Vec1.X) + (Vec1.Y * Vec1.Y) + (Vec1.Z * Vec1.Z));

            return new Vector3(Vec1.X / modulus, Vec1.Y / modulus, Vec1.Z / modulus);
        }

        /// <summary>
        /// Works out the Magnitude.
        /// </summary>
        /// <param name="Vec1">The vector which needs converting into it's magnitude.</param>
        /// <returns>The magnitude of the vector (input paramater).</returns>
        public static float MagnitudeOfVector(Vector3 Vec1)
        {
            float modulus;

            modulus = (float)Math.Sqrt((Vec1.X * Vec1.X) + (Vec1.Y * Vec1.Y) + (Vec1.Z * Vec1.Z));

            return modulus;
        }

        /// <summary>
        /// Works out the Magnitude and leaves it as a squared function.
        /// </summary>
        /// <param name="Vec1">The vector which needs converting into it's magnitude.</param>
        /// <returns>The magnitude of the vector (input paramater).</returns>
        public static float SquaredMagnitudeOfVector(Vector3 Vec1)
        {
            float modulus;

            modulus = (Vec1.X * Vec1.X) + (Vec1.Y * Vec1.Y) + (Vec1.Z * Vec1.Z);

            return modulus;
        }

        public static float FindRadian( float Degree )
        {
	        float temp = ( 3.14159265f / 180.0f ) * Degree;
	        return temp;
        }

        public static float FindDegree( float Radian )
        {
	        float temp = ( 180.0f / 3.14159265f ) * Radian;
	        return temp;
        }

        public static float AngleBetweenTwoVectorR( Vector3 Vec1, Vector3 Vec2 )
        {
            return (float)Math.Acos(DotProduct(Vec1, Vec2) / (MagnitudeOfVector(Vec1) * MagnitudeOfVector(Vec2)));
        }

        public static float AngleBetweenTwoVectorD( Vector3 Vec1, Vector3 Vec2 )
        {
            return FindDegree((float)Math.Acos(DotProduct(Vec1, Vec2) / (MagnitudeOfVector(Vec1) * MagnitudeOfVector(Vec2))));
        }

        public static Plane CreatePlane(Vector3 Normal, Vector3 PointOnPlane)
        {
            return new Plane(Normal.X, Normal.Y, Normal.Z, -DotProduct(Normal, PointOnPlane));
        }

        public static float DistanceBetweenLineSegToPoint(Vector3 LineA, Vector3 LineB, Vector3 Point)
        {
            Vector3 p = LineB - LineA;
        	
	        float square = p.X * p.X + p.Y * p.Y + p.Z * p.Z;
        	
	        float t = ((Point.X - LineA.X) * p.X + (Point.Y - LineA.Y) * p.Y + (Point.Z - LineA.Z) * p.Z) / square;
        	
	        if (t > 1)
            {
		        t = 1;
            }
	        else if (t < 0 )
            {
                t = 0;
            }
        	
	        Vector3 intersection_pt = new Vector3( LineA.X + t * p.X, LineA.Y + t * p.Y, LineA.Z + t * p.Z );

            return Vector3.Distance(intersection_pt, Point);
        }

        public static Vector3 PerpendicularIntersectionLineSegToPoint(Vector3 LineA, Vector3 LineB, Vector3 Point)
        {
            Vector3 p = LineB - LineA;

            float square = p.X * p.X + p.Y * p.Y + p.Z * p.Z;

            float t = ((Point.X - LineA.X) * p.X + (Point.Y - LineA.Y) * p.Y + (Point.Z - LineA.Z) * p.Z) / square;

            if (t > 1)
            {
                t = 1;
            }
            else if (t < 0)
            {
                t = 0;
            }

            return new Vector3(LineA.X + t * p.X, LineA.Y + t * p.Y, LineA.Z + t * p.Z);
        }

        public static float PerpendicularIntersectionLineSegToT(Vector3 LineA, Vector3 LineB, Vector3 Point)
        {
            Vector3 p = LineB - LineA;

            float square = p.X * p.X + p.Y * p.Y + p.Z * p.Z;

            return ((Point.X - LineA.X) * p.X + (Point.Y - LineA.Y) * p.Y + (Point.Z - LineA.Z) * p.Z) / square;
        }

        public static float DistanceBetweenLineSegToPointSquared(Vector3 LineA, Vector3 LineB, Vector3 Point)
        {
            Vector3 p = LineB - LineA;

            float square = p.X * p.X + p.Y * p.Y + p.Z * p.Z;

            float t = ((Point.X - LineA.X) * p.X + (Point.Y - LineA.Y) * p.Y + (Point.Z - LineA.Z) * p.Z) / square;

            if (t > 1)
            {
                t = 1;
            }
            else if (t < 0)
            {
                t = 0;
            }

            Vector3 intersection_pt = new Vector3(LineA.X + t * p.X, LineA.Y + t * p.Y, LineA.Z + t * p.Z);

            return Vector3.DistanceSquared(intersection_pt, Point);
        }

        public static bool IsPointInBoundingBox(Plane[] Planes, Vector3 CamPos)
        {
            if ((DotProduct(Planes[0].Normal, CamPos) + Planes[0].D) <= 0)
            {
                if ((DotProduct(Planes[1].Normal, CamPos) + Planes[1].D) <= 0)
                {
                    if ((DotProduct(Planes[4].Normal, CamPos) + Planes[4].D) <= 0)
                    {
                        if ((DotProduct(Planes[5].Normal, CamPos) + Planes[5].D) <= 0)
                        {
                            if ((DotProduct(Planes[2].Normal, CamPos) + Planes[2].D) <= 0)
                            {
                                if ((DotProduct(Planes[3].Normal, CamPos) + Planes[3].D) <= 0)
                                {
                                    //System.Diagnostics.Trace.WriteLine("yeah!");
                                    return true;
                                }
                            }
                        }
                    }
                }
            }

            //System.Diagnostics.Trace.WriteLine("no!");
            return false;
        }

        public static bool WhichSideOfPlane(Plane[] Planes, Vector3 PointPosition, ref Vector3 PlaneNormal)
        {
            bool found_plane = false;
            for (int i = 0; i < Planes.Length; ++i)
            {
                if ((DotProduct(Planes[i].Normal, PointPosition) + Planes[i].D) > 0)
                {
                    PlaneNormal = Planes[i].Normal;
                    found_plane = true;
                }
            }

            return found_plane;
        }

        public static float GetDeterminantOfMat(Matrix Mat)
        {
            return ((Mat.M11 * Mat.M22 * Mat.M33 * Mat.M44) + (Mat.M12 * Mat.M23 * Mat.M34 * Mat.M41) + (Mat.M13 * Mat.M24 * Mat.M31 * Mat.M42) + (Mat.M14 * Mat.M21 * Mat.M32 * Mat.M43)
                - (Mat.M14 * Mat.M23 * Mat.M32 * Mat.M41) - (Mat.M13 * Mat.M22 * Mat.M31 * Mat.M44) - (Mat.M12 * Mat.M21 * Mat.M34 * Mat.M43) - (Mat.M11 * Mat.M24 * Mat.M33 * Mat.M42));
        }

        public static Matrix Transpose(Matrix Mat)
        {
            Matrix rtn_val = new Matrix();

            rtn_val.M11 = Mat.M11;
            rtn_val.M12 = Mat.M21;
            rtn_val.M13 = Mat.M31;
            rtn_val.M14 = Mat.M41;
            
            rtn_val.M21 = Mat.M12;
            rtn_val.M22 = Mat.M22;
            rtn_val.M23 = Mat.M32;
            rtn_val.M24 = Mat.M42;

            rtn_val.M31 = Mat.M13;
            rtn_val.M32 = Mat.M23;
            rtn_val.M33 = Mat.M33;
            rtn_val.M34 = Mat.M43;

            rtn_val.M41 = Mat.M14;
            rtn_val.M42 = Mat.M24;
            rtn_val.M43 = Mat.M34;
            rtn_val.M44 = Mat.M44;

            return rtn_val;
        }

        public static Matrix GetInverse(Matrix Mat)
        {
            Matrix cofactor = new Matrix();

            float determinant = GetDeterminantOfMat(Mat);

            if (determinant != 0)
            {
                cofactor.M11 = (Mat.M22 * Mat.M33 * Mat.M44) + (Mat.M23 * Mat.M34 * Mat.M42) + (Mat.M24 * Mat.M32 * Mat.M43) - (Mat.M24 * Mat.M33 * Mat.M42) - (Mat.M23 * Mat.M32 * Mat.M44) - (Mat.M22 * Mat.M34 * Mat.M43);
                cofactor.M12 = (Mat.M21 * Mat.M33 * Mat.M44) + (Mat.M23 * Mat.M34 * Mat.M41) + (Mat.M24 * Mat.M31 * Mat.M43) - (Mat.M24 * Mat.M33 * Mat.M41) - (Mat.M23 * Mat.M31 * Mat.M44) - (Mat.M21 * Mat.M34 * Mat.M43);
                cofactor.M13 = (Mat.M21 * Mat.M32 * Mat.M44) + (Mat.M22 * Mat.M34 * Mat.M41) + (Mat.M24 * Mat.M31 * Mat.M42) - (Mat.M24 * Mat.M32 * Mat.M41) - (Mat.M22 * Mat.M31 * Mat.M44) - (Mat.M21 * Mat.M34 * Mat.M42);
                cofactor.M14 = (Mat.M21 * Mat.M32 * Mat.M43) + (Mat.M22 * Mat.M33 * Mat.M41) + (Mat.M23 * Mat.M31 * Mat.M42) - (Mat.M23 * Mat.M32 * Mat.M41) - (Mat.M22 * Mat.M31 * Mat.M43) - (Mat.M21 * Mat.M33 * Mat.M42);

                cofactor.M21 = (Mat.M12 * Mat.M33 * Mat.M44) + (Mat.M13 * Mat.M34 * Mat.M42) + (Mat.M14 * Mat.M32 * Mat.M43) - (Mat.M14 * Mat.M33 * Mat.M42) - (Mat.M13 * Mat.M32 * Mat.M44) - (Mat.M12 * Mat.M34 * Mat.M43);
                cofactor.M22 = (Mat.M11 * Mat.M33 * Mat.M44) + (Mat.M13 * Mat.M34 * Mat.M41) + (Mat.M14 * Mat.M31 * Mat.M43) - (Mat.M14 * Mat.M33 * Mat.M41) - (Mat.M13 * Mat.M31 * Mat.M44) - (Mat.M11 * Mat.M34 * Mat.M43);
                cofactor.M23 = (Mat.M11 * Mat.M32 * Mat.M44) + (Mat.M12 * Mat.M34 * Mat.M41) + (Mat.M14 * Mat.M31 * Mat.M42) - (Mat.M14 * Mat.M32 * Mat.M41) - (Mat.M12 * Mat.M31 * Mat.M44) - (Mat.M11 * Mat.M34 * Mat.M42);
                cofactor.M24 = (Mat.M11 * Mat.M32 * Mat.M43) + (Mat.M12 * Mat.M33 * Mat.M41) + (Mat.M13 * Mat.M31 * Mat.M42) - (Mat.M13 * Mat.M32 * Mat.M41) - (Mat.M12 * Mat.M31 * Mat.M43) - (Mat.M11 * Mat.M33 * Mat.M42);

                cofactor.M31 = (Mat.M12 * Mat.M23 * Mat.M44) + (Mat.M13 * Mat.M24 * Mat.M42) + (Mat.M14 * Mat.M22 * Mat.M43) - (Mat.M14 * Mat.M23 * Mat.M42) - (Mat.M13 * Mat.M22 * Mat.M44) - (Mat.M12 * Mat.M24 * Mat.M43);
                cofactor.M32 = (Mat.M11 * Mat.M23 * Mat.M44) + (Mat.M13 * Mat.M24 * Mat.M41) + (Mat.M14 * Mat.M21 * Mat.M43) - (Mat.M14 * Mat.M23 * Mat.M41) - (Mat.M13 * Mat.M21 * Mat.M44) - (Mat.M11 * Mat.M24 * Mat.M43);
                cofactor.M33 = (Mat.M11 * Mat.M22 * Mat.M44) + (Mat.M12 * Mat.M24 * Mat.M41) + (Mat.M14 * Mat.M21 * Mat.M42) - (Mat.M14 * Mat.M22 * Mat.M41) - (Mat.M12 * Mat.M21 * Mat.M44) - (Mat.M11 * Mat.M24 * Mat.M42);
                cofactor.M34 = (Mat.M11 * Mat.M22 * Mat.M43) + (Mat.M12 * Mat.M23 * Mat.M41) + (Mat.M13 * Mat.M21 * Mat.M42) - (Mat.M13 * Mat.M22 * Mat.M41) - (Mat.M12 * Mat.M21 * Mat.M43) - (Mat.M11 * Mat.M23 * Mat.M42);

                cofactor.M41 = (Mat.M12 * Mat.M23 * Mat.M34) + (Mat.M13 * Mat.M24 * Mat.M32) + (Mat.M14 * Mat.M22 * Mat.M33) - (Mat.M14 * Mat.M23 * Mat.M32) - (Mat.M13 * Mat.M22 * Mat.M34) - (Mat.M12 * Mat.M24 * Mat.M33);
                cofactor.M42 = (Mat.M11 * Mat.M23 * Mat.M34) + (Mat.M13 * Mat.M24 * Mat.M31) + (Mat.M14 * Mat.M21 * Mat.M33) - (Mat.M14 * Mat.M23 * Mat.M31) - (Mat.M13 * Mat.M21 * Mat.M34) - (Mat.M11 * Mat.M24 * Mat.M33);
                cofactor.M43 = (Mat.M11 * Mat.M22 * Mat.M34) + (Mat.M12 * Mat.M24 * Mat.M31) + (Mat.M14 * Mat.M21 * Mat.M32) - (Mat.M14 * Mat.M22 * Mat.M31) - (Mat.M12 * Mat.M21 * Mat.M34) - (Mat.M11 * Mat.M24 * Mat.M32);
                cofactor.M44 = (Mat.M11 * Mat.M22 * Mat.M33) + (Mat.M12 * Mat.M23 * Mat.M31) + (Mat.M13 * Mat.M21 * Mat.M32) - (Mat.M13 * Mat.M22 * Mat.M31) - (Mat.M12 * Mat.M21 * Mat.M33) - (Mat.M11 * Mat.M23 * Mat.M32);

                cofactor = Transpose(cofactor);

                cofactor /= determinant;
            }

            return cofactor;
        }

        public static Matrix ConvertDirIntoRotation( Vector3 Position, Vector3 Interest )
        {
            Matrix rotation = new Matrix();

            Vector3 zaxis = CToolbox.UnitVector(Position - Interest);
            Vector3 xaxis = CToolbox.UnitVector(CToolbox.CrossProduct(Vector3.UnitY, zaxis));
            Vector3 yaxis = CToolbox.CrossProduct(zaxis, xaxis);

            rotation.M11 = xaxis.X;
            rotation.M12 = xaxis.Y;
            rotation.M13 = xaxis.Z;
            rotation.M14 = 0;

            rotation.M21 = yaxis.X;
            rotation.M22 = yaxis.Y;
            rotation.M23 = yaxis.Z;
            rotation.M24 = 0;

            rotation.M31 = zaxis.X;
            rotation.M32 = zaxis.Y;
            rotation.M33 = zaxis.Z;
            rotation.M34 = 0;

            rotation.M41 = 0;
            rotation.M42 = 0;
            rotation.M43 = 0;
            rotation.M44 = 1;

            return rotation;
        }

        public static Matrix ConvertDirIntoRotation(Vector3 Direction)
        {
            Matrix rotation = new Matrix();

            Vector3 zaxis = Direction;
            Vector3 xaxis = CToolbox.UnitVector(CToolbox.CrossProduct(Vector3.UnitY, zaxis));
            Vector3 yaxis = CToolbox.CrossProduct(zaxis, xaxis);

            rotation.M11 = xaxis.X;
            rotation.M12 = xaxis.Y;
            rotation.M13 = xaxis.Z;
            rotation.M14 = 0;

            rotation.M21 = yaxis.X;
            rotation.M22 = yaxis.Y;
            rotation.M23 = yaxis.Z;
            rotation.M24 = 0;

            rotation.M31 = zaxis.X;
            rotation.M32 = zaxis.Y;
            rotation.M33 = zaxis.Z;
            rotation.M34 = 0;

            rotation.M41 = 0;
            rotation.M42 = 0;
            rotation.M43 = 0;
            rotation.M44 = 1;

            return rotation;
        }

        public static bool UseThisInsidePortal(Plane[] Planes, Vector3 CamPos, Vector3 PortalNormal)
        {
            float dot_prod = DotProduct(Planes[0].Normal, PortalNormal);
            if (dot_prod > 0.9f && dot_prod < 1.1f)
            {
                if ((DotProduct(Planes[0].Normal, CamPos) + Planes[0].D) < 0)
                {
                    return true;
                }
            }

            dot_prod = DotProduct(Planes[1].Normal, PortalNormal);
            if (dot_prod > 0.9f && dot_prod < 1.1f)
            {
                if ((DotProduct(Planes[1].Normal, CamPos) + Planes[1].D) < 0)
                {
                    return true;
                }
            }

            dot_prod = DotProduct(Planes[2].Normal, PortalNormal);
            if (dot_prod > 0.9f && dot_prod < 1.1f)
            {
                if ((DotProduct(Planes[2].Normal, CamPos) + Planes[2].D) < 0)
                {
                    return true;
                }
            }

            dot_prod = DotProduct(Planes[3].Normal, PortalNormal);
            if (dot_prod > 0.9f && dot_prod < 1.1f)
            {
                if ((DotProduct(Planes[3].Normal, CamPos) + Planes[3].D) < 0)
                {
                    return true;
                }
            }

            dot_prod = DotProduct(Planes[4].Normal, PortalNormal);
            if (dot_prod > 0.9f && dot_prod < 1.1f)
            {
                if ((DotProduct(Planes[4].Normal, CamPos) + Planes[4].D) < 0)
                {
                    return true;
                }
            }

            dot_prod = DotProduct(Planes[5].Normal, PortalNormal);
            if (dot_prod > 0.9f && dot_prod < 1.1f)
            {
                if ((DotProduct(Planes[5].Normal, CamPos) + Planes[5].D) < 0)
                {
                    return true;
                }
            }

            return false;
        }


        public static bool UseThisOutsidePortal(Plane[] Planes, Vector3 CamPos, Vector3 PortalNormal)
        {
            float dot_prod = DotProduct(Planes[0].Normal, PortalNormal);
            if (dot_prod > 0.9f && dot_prod < 1.1f)
            {
                //float test = (DotProduct(Planes.s_negX, CamPos) + Planes.s_negX.W);
                if ((DotProduct(Planes[0].Normal, CamPos) + Planes[0].D) > 0)
                {
                    return true;
                }
            }

            dot_prod = DotProduct(Planes[1].Normal, PortalNormal);
            if (dot_prod > 0.9f && dot_prod < 1.1f)
            {
                //float test = (DotProduct(Planes.s_negX, CamPos) + Planes.s_negX.W);
                if ((DotProduct(Planes[1].Normal, CamPos) + Planes[1].D) > 0)
                {
                    return true;
                }
            }

            dot_prod = DotProduct(Planes[2].Normal, PortalNormal);
            if (dot_prod > 0.9f && dot_prod < 1.1f)
            {
                //float test = (DotProduct(Planes.s_negX, CamPos) + Planes.s_negX.W);
                if ((DotProduct(Planes[2].Normal, CamPos) + Planes[2].D) > 0)
                {
                    return true;
                }
            }

            dot_prod = DotProduct(Planes[3].Normal, PortalNormal);
            if (dot_prod > 0.9f && dot_prod < 1.1f)
            {
                //float test = (DotProduct(Planes.s_negX, CamPos) + Planes.s_negX.W);
                if ((DotProduct(Planes[3].Normal, CamPos) + Planes[3].D) > 0)
                {
                    return true;
                }
            }

            dot_prod = DotProduct(Planes[4].Normal, PortalNormal);
            if (dot_prod > 0.9f && dot_prod < 1.1f)
            {
                //float test = (DotProduct(Planes.s_negX, CamPos) + Planes.s_negX.W);
                if ((DotProduct(Planes[4].Normal, CamPos) + Planes[4].D) > 0)
                {
                    return true;
                }
            }

            dot_prod = DotProduct(Planes[5].Normal, PortalNormal);
            if (dot_prod > 0.9f && dot_prod < 1.1f)
            {
                //float test = (DotProduct(Planes.s_negX, CamPos) + Planes.s_negX.W);
                if ((DotProduct(Planes[5].Normal, CamPos) + Planes[5].D) > 0)
                {
                    return true;
                }
            }

            return false;
        }

        public static bool ClipWithPlanes(Vector3 Min, Vector3 Max, Plane[] ClipPlanes)
        {
            for (int i = 0; i < ClipPlanes.Length; ++i)
            {
                Vector3 p = Min, n = Max;

                if (ClipPlanes[i].Normal.X > 0)
                {
                    p.X = Max.X;
                    n.X = Min.X;
                }
                if (ClipPlanes[i].Normal.Y > 0)
                {
                    p.Y = Max.Y;
                    n.Y = Min.Y;
                }
                if (ClipPlanes[i].Normal.Z > 0)
                {
                    p.Z = Max.Z;
                    n.Z = Min.Z;
                }

                float dist1 = ClipPlanes[i].Normal.X * p.X + ClipPlanes[i].Normal.Y * p.Y + ClipPlanes[i].Normal.Z * p.Z + ClipPlanes[i].D;
                if (dist1 < 0)
                {
                    float dist2 = ClipPlanes[i].Normal.X * n.X + ClipPlanes[i].Normal.Y * n.Y + ClipPlanes[i].Normal.Z * n.Z + ClipPlanes[i].D;
                    if (dist2 < 0)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        //public static bool ClipWithPlanes2(Vector3 Min, Vector3 Max, Plane[] ClipPlanes)
        //{
        //    // It's only objects which are completely inside the planes that will work atm.
        //    for (int i = 0; i < ClipPlanes.Length; ++i)
        //    {
        //        float dist1 = DotProduct(ClipPlanes[i], Min);
        //        float dist2 = DotProduct(ClipPlanes[i], Max);

        //        if (dist1 < 0 && dist2 < 0)
        //        {
        //            return false;
        //        }
        //    }

        //    return true;
        //}

        public static bool IsPointInMinMaxOfBoundingBox(Vector3 Min, Vector3 Max, Vector3 Point)
        {
            if (Point.X >= Min.X && Point.X <= Max.X)
            {
                if (Point.Y >= Min.Y && Point.Y <= Max.Y)
                {
                    if (Point.Z >= Min.Z && Point.Z <= Max.Z)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        // Ignores the y element
        public static bool IsPointInXZMinMaxOfBoundingBox(Vector3 Min, Vector3 Max, Vector3 Point)
        {
            if (Point.X >= Min.X && Point.X <= Max.X)
            {
                if (Point.Z >= Min.Z && Point.Z <= Max.Z)
                {
                    return true;
                }
            }

            return false;
        }

        // Ignores the max y element
        public static bool IsPointInXZMinMaxAndYMinOfBoundingBox(Vector3 Min, Vector3 Max, Vector3 Point)
        {
            if (Point.X >= Min.X && Point.X <= Max.X)
            {
                if (Point.Y >= Min.Y)
                {
                    if (Point.Z >= Min.Z && Point.Z <= Max.Z)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public static bool StringToVector3(string strVector, ref Vector3 RtnVal)
        {
            int X = strVector.IndexOf("X:");
            if (X == -1)
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CToolBox", "ONDERZOEK ENGINE ERROR: X not found when trying to convert string to Vector3.");
                return false;
            }
            int Y = strVector.IndexOf("Y:");
            if (Y == -1)
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CToolBox", "ONDERZOEK ENGINE ERROR: Y not found when trying to convert string to Vector3.");
                return false;
            }
            int Z = strVector.IndexOf("Z:");
            if (Z == -1)
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CToolBox", "ONDERZOEK ENGINE ERROR: Z not found when trying to convert string to Vector3.");
                return false;
            }

            float valueOfX = float.Parse(strVector.Substring(X + 2, Y - X - 2));
            float valueOfY = float.Parse(strVector.Substring(Y + 2, Z - Y - 2));
            float valueOfZ = float.Parse(strVector.Substring(Z + 2, strVector.Length - 1 - Z - 2));

            RtnVal = new Vector3(valueOfX, valueOfY, valueOfZ);

            return true;
        }

        public static bool StringToVector3List(string strVector, ref List<Vector3> RtnVal)
        {
            if (strVector == null)
            {
                return false;
            }

            strVector = strVector.Remove(strVector.Length - 1, 1);
            try
            {
                string[] num_array = strVector.Split(' ');
                for (int i = 0; i < num_array.Length; i+=3)
                {
                    Vector3 vertex = Vector3.Zero;
                    vertex.X = float.Parse(num_array[i].Split(':')[1]);
                    vertex.Y = float.Parse(num_array[i+1].Split(':')[1]);
                    vertex.Z = float.Parse(num_array[i + 2].Split(':')[1].Split('}')[0]);

                    RtnVal.Add(vertex);
                }
            }
            catch (Exception e)
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CToolBox", "ONDERZOEK ENGINE ERROR: Vector3 list cannot be created: " + e);
                return false;
            }

            return true;
        }

        public static bool StringToIntList(string strVector, ref List<int> RtnVal)
        {
            if (strVector == null)
            {
                return false;
            }

            strVector = strVector.Remove(strVector.Length - 1, 1);
            try
            {
                string[] num_array = strVector.Split(' ');
                for (int i = 0; i < num_array.Length; ++i)
                {
                    RtnVal.Add(int.Parse(num_array[i]));
                }
            }
            catch (Exception e)
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CToolBox", "ONDERZOEK ENGINE ERROR: int list cannot be created: " + e);
                return false;
            }

            return true;
        }

        // Sorts the entity list so that the firthest objects [to the camera] are placed at the beginning of the list
        // and the closest objects are placed at the end of the list.
        // This fucntion is currently only being used before rendering the translucent entities.
        public static void SortEntitiesByDistFromCam(ref List<int> EntityID, List<CEntity> EntityList, CCamera Camera)
        {
            Vector2[] dist_frm_cam = new Vector2[EntityID.Count];

            for (int i = 0; i < dist_frm_cam.Length; ++i)
            {
                dist_frm_cam[i].X = Vector3.DistanceSquared(EntityList[EntityID[i]].GetWorldPosition(), Camera.GetPosition());
                dist_frm_cam[i].Y = EntityID[i];
            }

            QuickSort(dist_frm_cam, 0, dist_frm_cam.Length);

            // I have reveresed the order since this function is solely being used for the transparent entities.
            int count = 0;
            for (int i = dist_frm_cam.Length - 1; i >= 0; --i)
            {
                EntityID[count] = (int)dist_frm_cam[i].Y;

                ++count;
            }
        }

        // Sorts the RoomEntity list so that the closest rooms [to the camera] are placed at the beginning of the list
        // and the furthest objects are placed at the end of the list.
        public static void SortEntitiesByDistFromCam(ref List<CMainRoomCompositeSpatialTree> RoomList, List<CEntity> EntityList, Vector3 AvatarPosition)
        {
            Vector2[] dist_frm_cam = new Vector2[RoomList.Count];

            for (int i = 0; i < dist_frm_cam.Length; ++i)
            {
                dist_frm_cam[i].X = Vector3.DistanceSquared(EntityList[RoomList[i].GetEntityID()].GetPosition(), AvatarPosition);
                dist_frm_cam[i].Y = i;
            }

            QuickSort(dist_frm_cam, 0, dist_frm_cam.Length);

            List<CMainRoomCompositeSpatialTree> new_room_list = new List<CMainRoomCompositeSpatialTree>();
            for (int i = 0; i < dist_frm_cam.Length; ++i)
            {
                new_room_list.Add(RoomList[(int)dist_frm_cam[i].Y]);
            }

            RoomList = new_room_list;
        }

        // A basic quick sort algorithm, which is only used by CToolBox::SortEntitiesByDistFromCam
        private static void QuickSort(Vector2[] DistFrmCam, int BegIndex, int EndIndex)
        {
            int dif = EndIndex - BegIndex;

            if (dif <= 1)
            {
                return;
            }

            List<Vector2> dist_frm_cam_pos = new List<Vector2>();
            List<Vector2> dist_frm_cam_neg = new List<Vector2>();
            int half = BegIndex + (dif / 2);

            for (int i = BegIndex; i < half; ++i)
            {
                if (DistFrmCam[i].X > DistFrmCam[half].X)
                {
                    dist_frm_cam_pos.Add(DistFrmCam[i]);
                }
                else
                {
                    dist_frm_cam_neg.Add(DistFrmCam[i]);
                }
            }
            for (int i = half + 1; i < EndIndex; ++i)
            {
                if (DistFrmCam[i].X > DistFrmCam[half].X)
                {
                    dist_frm_cam_pos.Add(DistFrmCam[i]);
                }
                else
                {
                    dist_frm_cam_neg.Add(DistFrmCam[i]);
                }
            }

            dist_frm_cam_neg.Add(DistFrmCam[half]);

            for (int i = 0; i < dist_frm_cam_neg.Count; ++i)
            {
                DistFrmCam[BegIndex + i] = dist_frm_cam_neg[i];
            }
            for (int i = dist_frm_cam_neg.Count; i < dist_frm_cam_neg.Count + dist_frm_cam_pos.Count; ++i)
            {
                DistFrmCam[BegIndex + i] = dist_frm_cam_pos[i - dist_frm_cam_neg.Count];
            }

            QuickSort(DistFrmCam, BegIndex, BegIndex + dist_frm_cam_neg.Count - 1);
            QuickSort(DistFrmCam, BegIndex + dist_frm_cam_neg.Count, EndIndex);
        }

        //Function that converts  "{X:1 Y:1 Z:1 W:0.5}" into Vector4(1, 1, 1, 0.5)
        public static Vector4 StringToVector4(string str)
        {
            str = str.Remove(str.Length - 1, 1);
            try
            {
                string teststr = str.Split(' ')[3];
                string test2str = teststr.Split(':')[1];
                float t = float.Parse(test2str);

                return new Vector4(float.Parse(str.Split(' ')[0].Split(':')[1]),
                                    float.Parse(str.Split(' ')[1].Split(':')[1]),
                                    float.Parse(str.Split(' ')[2].Split(':')[1]),
                                    float.Parse(str.Split(' ')[3].Split(':')[1]));

            }
            catch (Exception e)
            {
                throw new Exception("The string passed to StringToVector4 is not in correct format. it should be the format creeated by vector4.toString(): " + e);
            }
        }

        //Function that converts  "{X:1 Y:1 Z:1 W:0.5}" into Vector4(1, 1, 1, 0.5)
        public static bool StringToVector4(string str, ref Vector4 Vec4)
        {
            str = str.Remove(str.Length - 1, 1);
            try
            {
                string teststr = str.Split(' ')[3];
                string test2str = teststr.Split(':')[1];
                float t = float.Parse(test2str);

                Vec4 = new Vector4(float.Parse(str.Split(' ')[0].Split(':')[1]),
                                    float.Parse(str.Split(' ')[1].Split(':')[1]),
                                    float.Parse(str.Split(' ')[2].Split(':')[1]),
                                    float.Parse(str.Split(' ')[3].Split(':')[1]));

            }
            catch (Exception e)
            {
                //throw new Exception("The string passed to StringToVector4 is not in correct format. it should be the format creeated by vector4.toString(): " + e);
                return false;
            }

            return true;
        }

        public static bool StringToColour(string strVector, ref Color Col)
        {
            string[] array = strVector.Split(' ');

            if (array.Length > 3 || array.Length < 3)
            {
                return false;
            }

            try
            {
                Col.R = byte.Parse(array[0]);
                Col.G = byte.Parse(array[1]);
                Col.B = byte.Parse(array[2]);
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

        public static bool StringToVector2(string strVector, ref Vector2 RtnVal)
        {
            int X = strVector.IndexOf("X:");
            if (X == -1)
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CToolBox", "ONDERZOEK ENGINE ERROR: X not found when trying to convert string to Vector3.");
                return false;
            }
            int Y = strVector.IndexOf("Y:");
            if (Y == -1)
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CToolBox", "ONDERZOEK ENGINE ERROR: Y not found when trying to convert string to Vector3.");
                return false;
            }

            float valueOfX = float.Parse(strVector.Substring(X + 2, Y - X - 2));
            float valueOfY = float.Parse(strVector.Substring(Y + 2, strVector.Length - 1 - Y - 2));

            RtnVal = new Vector2(valueOfX, valueOfY);

            return true;
        }

        public static List<string> StringToStringList(string WholeString)
        {
            if (WholeString != "" && WholeString != "\r\n        ")
            {
                int index = WholeString.IndexOf(" ");

                char[] delim = new char[3];
                delim[0] = ' ';
                delim[1] = ';';
                delim[2] = ',';

                string[] array_of_strings = WholeString.Split(delim);

                return array_of_strings.ToList();
            }
            else
            {
                return new List<string>();
            }
        }

        public static bool StringToBool(string Value, ref bool BValue)
        {
            if (Value.CompareTo("True") == 0)
            {
                BValue = true;
            }
            else if (Value.CompareTo("False") == 0)
            {
                BValue = false;
            }
            else if (Value.CompareTo("true") == 0)
            {
                BValue = true;
            }
            else if (Value.CompareTo("false") == 0)
            {
                BValue = false;
            }
            else if (Value.CompareTo("1") == 0)
            {
                BValue = true;
            }
            else if (Value.CompareTo("0") == 0)
            {
                BValue = false;
            }
            else
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CToolbox", "GetBoolFromString - correct string value was not sent to this function.");
                return false;
            }

            return true;
        }

        public static bool StringToInt(string Value, ref int IValue)
        {
#if WINDOWS
            return int.TryParse(Value, out IValue);
#else
            IValue = int.Parse(Value);
            return true;
#endif
        }

        public static bool StringToUInt(string Value, ref uint IValue)
        {
#if WINDOWS
            return uint.TryParse(Value, out IValue);
#else
            IValue = uint.Parse(Value);
            return true;
#endif
        }

        public static bool StringToFloat(string Value, ref float FValue)
        {
#if WINDOWS
            return float.TryParse(Value, out FValue);
#else
            FValue = float.Parse(Value);
            return true;
#endif
        }
    }
}

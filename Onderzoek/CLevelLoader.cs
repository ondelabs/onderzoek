﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml;

using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;

using Onderzoek.Entity;
using Onderzoek.ModelData;
using Onderzoek.Navigation;

#if ENGINE

namespace Onderzoek
{
    public class CLevelLoader
    {
        private static CEntity readEntity(XmlReader Reader, ContentManager Content, string EntityName, Dictionary<string, CModel.SModelData> ModelList)
        {
            string name;
            Vector3 position, rotation;
            int entity_type = 0;

            name = EntityName;

            Reader.ReadToFollowing("Position");
            Reader.Read();
            position = new Vector3(float.Parse(Reader.Value.Split(' ')[0]), float.Parse(Reader.Value.Split(' ')[1]), float.Parse(Reader.Value.Split(' ')[2]));

            Reader.ReadToFollowing("Orientation");
            Reader.Read();
            rotation = new Vector3(float.Parse(Reader.Value.Split(' ')[0]), float.Parse(Reader.Value.Split(' ')[1]), float.Parse(Reader.Value.Split(' ')[2]));

            Reader.ReadToFollowing("EntityType");
            Reader.Read();
            entity_type = int.Parse(Reader.Value);

            Reader.Read();

            string model_name = "";
            bool collision = false;

            Reader.ReadToDescendant("EntityData");

            Reader.ReadToFollowing("ModelName");
            Reader.Read();
            model_name = Reader.Value;

            Reader.ReadToFollowing("Collision");
            Reader.Read();
            if (CToolbox.StringToBool(Reader.Value, ref collision) == false)
            {
                collision = false;
            }

            CDotXModel model = new CDotXModel();
#if ENGINE
            model.Initialise(Content, model_name, ModelList);
#else
            model.Initialise(Content, model_name);
#endif

            return new CDotXEntity(position, rotation, name, model, model.GetMaterials(Content, model_name), collision, false);
        }

        public static bool loadEntityFromXML(string fileName, string EntityNameToLoad, out CEntity Entity, ContentManager Content,
            Dictionary<string, CModel.SModelData> ModelList)
        {
            XmlTextReader xmlReader = null;
            xmlReader = new XmlTextReader(fileName);
            CEntity entity = null;

            while (!xmlReader.EOF)
            {
                if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "Entity")
                {
                    xmlReader.ReadToDescendant("Name");
                    xmlReader.Read();

                    if (EntityNameToLoad.CompareTo(xmlReader.Value) == 0)
                    {
                        entity = readEntity(xmlReader, Content, xmlReader.Value, ModelList);
                        Entity = entity;
                        return true;
                    }
                }
                try
                {
                    xmlReader.ReadToFollowing("Entity");
                }
                catch (XmlException e)
                {
                    System.Diagnostics.Trace.WriteLine("\n--------------------------------------------------------------\n");
                    System.Diagnostics.Trace.WriteLine("An XML Exception occurred: " + e.Message + "\n");
                    System.Diagnostics.Trace.WriteLine("--------------------------------------------------------------\n");

                    xmlReader.Close();

                    Entity = null;

                    return false;
                }
                catch (Exception e)
                {
                    System.Diagnostics.Trace.WriteLine("\n--------------------------------------------------------------\n");
                    System.Diagnostics.Trace.WriteLine("A General Exception occurred: " + e.Message + "\n");
                    System.Diagnostics.Trace.WriteLine("--------------------------------------------------------------\n");

                    xmlReader.Close();

                    Entity = null;

                    return false;
                }
            }

            Entity = entity;
            xmlReader.Close();
            return false;
        }


        public static void LoadLevelFromXML(XmlReader xmlReader, ContentManager content, Camera.CCamera camera,Dictionary<string,Onderzoek.ModelData.CModel.SModelData> modelList)
        {
            CNavigationManager.Instance.clearManager();

            #region NavMesh
            xmlReader.ReadToDescendant("NavMesh");
            if (!xmlReader.IsEmptyElement)
            {
                xmlReader.Read();

                while (!xmlReader.EOF && (xmlReader.NodeType != XmlNodeType.EndElement || !xmlReader.Name.Equals("NavMesh")))
                {
                    if (xmlReader.Name.Equals("NavSector") && xmlReader.NodeType == XmlNodeType.Element)
                    {
                        xmlReader.Read();
                        xmlReader.ReadToFollowing("SectorName");
                        xmlReader.Read();
                        string sectorName = xmlReader.Value;

                        xmlReader.ReadToFollowing("Point1");
                        xmlReader.Read();
                        Vector3 p1 = Vector3.Zero;
                        CToolbox.StringToVector3(xmlReader.Value.ToString(), ref p1);

                        xmlReader.ReadToFollowing("Point2");
                        xmlReader.Read();
                        Vector3 p2 = Vector3.Zero;
                        CToolbox.StringToVector3(xmlReader.Value.ToString(), ref p2);

                        xmlReader.ReadToFollowing("Point3");
                        xmlReader.Read();
                        Vector3 p3 = Vector3.Zero;
                        CToolbox.StringToVector3(xmlReader.Value.ToString(), ref p3);

                        xmlReader.ReadToFollowing("Point4");
                        xmlReader.Read();
                        Vector3 p4 = Vector3.Zero;
                        CToolbox.StringToVector3(xmlReader.Value.ToString(), ref p4);

                        CNavigationManager.Instance.m_sectorList.Add(new CNavigationManager.CNavigationSector(p1, p2, p3, p4));
                        CNavigationManager.Instance.m_sectorList[CNavigationManager.Instance.m_sectorList.Count - 1].m_sectorName = sectorName;



                        xmlReader.ReadToFollowing("NavSectorLinks");
                        if (!xmlReader.IsEmptyElement)
                        {
                            while (!xmlReader.EOF)
                            {
                                if (xmlReader.Name.Equals("NavSectorLink") && xmlReader.NodeType == XmlNodeType.Element)
                                {
                                    xmlReader.Read();
                                    xmlReader.ReadToFollowing("SectorIndex");
                                    xmlReader.Read();
                                    int index = int.Parse(xmlReader.Value);
                                    xmlReader.ReadToFollowing("CommonPoint1");

                                    xmlReader.Read();

                                    Vector3 conPoint = Vector3.Zero;
                                    CToolbox.StringToVector3(xmlReader.Value.ToString(), ref conPoint);
                                    xmlReader.ReadToFollowing("CommonPoint2");

                                    xmlReader.Read();

                                    Vector3 conPoint2 = Vector3.Zero;
                                    CToolbox.StringToVector3(xmlReader.Value.ToString(), ref conPoint2);

                                    if (index < CNavigationManager.Instance.m_sectorList.Count)
                                    {
                                        CNavigationManager.Instance.m_sectorList[CNavigationManager.Instance.m_sectorList.Count - 1].m_sectorLinks.Add(new CNavigationManager.CNavigationSectorLink(
                                            CNavigationManager.Instance.m_sectorList[index], conPoint, conPoint2));

                                        CNavigationManager.Instance.m_sectorList[index].m_sectorLinks.Add(new CNavigationManager.CNavigationSectorLink(
                                            CNavigationManager.Instance.m_sectorList[CNavigationManager.Instance.m_sectorList.Count - 1], conPoint, conPoint2));
                                    }
                                    break;

                                }

                                if (xmlReader.Name.Equals("NavSectorLinks") && xmlReader.NodeType == XmlNodeType.EndElement)
                                    break;

                                xmlReader.Read();

                            }
                        }

                    }

                    if (xmlReader.Name.Equals("NavMesh") && xmlReader.NodeType == XmlNodeType.EndElement)
                    {
                        break;
                    }
                    xmlReader.Read();
                }
            }
            #endregion


            while (!xmlReader.EOF)
            {
                if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "EntityList")
                {
                    xmlReader.ReadToDescendant("Entity");



                    while (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "Entity")
                    {

                        xmlReader.ReadToDescendant("EntityName");
                        xmlReader.Read();
                        string entityNameString = xmlReader.Value;

                        if (entityNameString == "")
                        {
                            throw new System.Exception("An entities name cannot be empty");
                        }


                        xmlReader.ReadToFollowing("Position");

                        Vector3 curPos = new Vector3();
                        Vector3 curRot = new Vector3();

                        if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "Position")
                        {
                            xmlReader.Read();
                            curPos.X = float.Parse(xmlReader.Value.Split(' ')[0]);
                            curPos.Y = float.Parse(xmlReader.Value.Split(' ')[1]);
                            curPos.Z = float.Parse(xmlReader.Value.Split(' ')[2]);
                        }
                        //xmlReader.MoveToContent();
                        xmlReader.ReadToFollowing("Orientation");

                        if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "Orientation")
                        {
                            xmlReader.Read();
                            curRot.X = float.Parse(xmlReader.Value.Split(' ')[0]);
                            curRot.Y = float.Parse(xmlReader.Value.Split(' ')[1]);
                            curRot.Z = float.Parse(xmlReader.Value.Split(' ')[2]);

                        }

                        //xmlReader.MoveToContent();
                        xmlReader.Read();

                        xmlReader.ReadToFollowing("EntityType");

                        int entityType = 0;
                        if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "EntityType")
                        {
                            xmlReader.Read();
                            xmlReader.MoveToContent();
                            entityType = int.Parse(xmlReader.Value);
                        }

                        CEntity tempEnt = CEntityCreator.Instance.getEntityByID(entityType, curPos, curRot, entityNameString, content, camera, modelList);

                        xmlReader.ReadToFollowing("EntityData");

                        while ((xmlReader.NodeType != XmlNodeType.Element || xmlReader.Name != "Entity") && (xmlReader.NodeType != XmlNodeType.EndElement || xmlReader.Name != "EntityList"))
                        {
                            if (xmlReader.EOF)
                                break;
                            xmlReader.Read();
                        }

                        tempEnt.readEntityProperties(xmlReader);

                        tempEnt.UpdateFromPropertyList();
                    }
                }
                xmlReader.Read();

            }


        }

        public static CLevelDataStore LoadLevelToDataStore(XmlReader xmlReader)
        {
            CNavigationManager.Instance.clearManager();

            CLevelDataStore levelDataStore = new CLevelDataStore();

            #region NavMesh
            xmlReader.ReadToDescendant("NavMesh");
            if (!xmlReader.IsEmptyElement)
            {
                xmlReader.Read();

                while (!xmlReader.EOF && (xmlReader.NodeType != XmlNodeType.EndElement || !xmlReader.Name.Equals("NavMesh")))
                {
                    if (xmlReader.Name.Equals("NavSector") && xmlReader.NodeType == XmlNodeType.Element)
                    {
                        xmlReader.Read();
                        xmlReader.ReadToFollowing("SectorName");
                        xmlReader.Read();
                        string sectorName = xmlReader.Value;

                        xmlReader.ReadToFollowing("Point1");
                        xmlReader.Read();
                        Vector3 p1 = Vector3.Zero;
                        CToolbox.StringToVector3(xmlReader.Value.ToString(), ref p1);

                        xmlReader.ReadToFollowing("Point2");
                        xmlReader.Read();
                        Vector3 p2 = Vector3.Zero;
                        CToolbox.StringToVector3(xmlReader.Value.ToString(), ref p2);

                        xmlReader.ReadToFollowing("Point3");
                        xmlReader.Read();
                        Vector3 p3 = Vector3.Zero;
                        CToolbox.StringToVector3(xmlReader.Value.ToString(), ref p3);

                        xmlReader.ReadToFollowing("Point4");
                        xmlReader.Read();
                        Vector3 p4 = Vector3.Zero;
                        CToolbox.StringToVector3(xmlReader.Value.ToString(), ref p4);

                        CNavigationManager.Instance.m_sectorList.Add(new CNavigationManager.CNavigationSector(p1, p2, p3, p4));
                        CNavigationManager.Instance.m_sectorList[CNavigationManager.Instance.m_sectorList.Count - 1].m_sectorName = sectorName;



                        xmlReader.ReadToFollowing("NavSectorLinks");
                        if (!xmlReader.IsEmptyElement)
                        {
                            while (!xmlReader.EOF)
                            {
                                if (xmlReader.Name.Equals("NavSectorLink") && xmlReader.NodeType == XmlNodeType.Element)
                                {
                                    xmlReader.Read();
                                    xmlReader.ReadToFollowing("SectorIndex");
                                    xmlReader.Read();
                                    int index = int.Parse(xmlReader.Value);
                                    xmlReader.ReadToFollowing("CommonPoint1");

                                    xmlReader.Read();

                                    Vector3 conPoint = Vector3.Zero;
                                    CToolbox.StringToVector3(xmlReader.Value.ToString(), ref conPoint);
                                    xmlReader.ReadToFollowing("CommonPoint2");

                                    xmlReader.Read();

                                    Vector3 conPoint2 = Vector3.Zero;
                                    CToolbox.StringToVector3(xmlReader.Value.ToString(), ref conPoint2);

                                    if (index < CNavigationManager.Instance.m_sectorList.Count)
                                    {
                                        CNavigationManager.Instance.m_sectorList[CNavigationManager.Instance.m_sectorList.Count - 1].m_sectorLinks.Add(new CNavigationManager.CNavigationSectorLink(
                                            CNavigationManager.Instance.m_sectorList[index], conPoint, conPoint2));

                                        CNavigationManager.Instance.m_sectorList[index].m_sectorLinks.Add(new CNavigationManager.CNavigationSectorLink(
                                            CNavigationManager.Instance.m_sectorList[CNavigationManager.Instance.m_sectorList.Count - 1], conPoint, conPoint2));
                                    }
                                    break;

                                }

                                if (xmlReader.Name.Equals("NavSectorLinks") && xmlReader.NodeType == XmlNodeType.EndElement)
                                    break;

                                xmlReader.Read();

                            }
                        }

                    }

                    if (xmlReader.Name.Equals("NavMesh") && xmlReader.NodeType == XmlNodeType.EndElement)
                    {
                        break;
                    }
                    xmlReader.Read();
                }
            }
            #endregion


            while (!xmlReader.EOF)
            {
                if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "EntityList")
                {
                    xmlReader.ReadToDescendant("Entity");



                    while (!xmlReader.EOF && !(xmlReader.NodeType == XmlNodeType.EndElement && xmlReader.Name == "EntityList"))
                    {

                        xmlReader.ReadToDescendant("EntityName");
                        xmlReader.Read();
                        string entityNameString = xmlReader.Value;

                        if (entityNameString == "")
                        {
                            throw new System.Exception("An entities name cannot be empty");
                        }


                        xmlReader.ReadToFollowing("Position");

                        Vector3 curPos = new Vector3();
                        Vector3 curRot = new Vector3();

                        if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "Position")
                        {
                            xmlReader.Read();
                            CToolbox.StringToVector3(xmlReader.Value, ref curPos);
                        }
                        //xmlReader.MoveToContent();
                        xmlReader.ReadToFollowing("Orientation");

                        if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "Orientation")
                        {
                            xmlReader.Read();
                            CToolbox.StringToVector3(xmlReader.Value, ref curRot);
                        }

                        //xmlReader.MoveToContent();
                        xmlReader.Read();

                        xmlReader.ReadToFollowing("EntityType");

                        int entityType = 0;
                        if (xmlReader.NodeType == XmlNodeType.Element && xmlReader.Name == "EntityType")
                        {
                            xmlReader.Read();
                            xmlReader.MoveToContent();
                            entityType = int.Parse(xmlReader.Value);
                        }

                        //CEntity tempEnt = CEntityCreator.Instance.getEntityByID(entityType, content, camera, modelList);

                        //xmlReader.ReadToFollowing("EntityData");

                        levelDataStore.m_entityStoreList.Add(new CEntityDataStore(entityType, curPos, curRot, entityNameString, readProperties(xmlReader)));

                        while ((xmlReader.NodeType != XmlNodeType.Element || xmlReader.Name != "Entity") && (xmlReader.NodeType != XmlNodeType.EndElement || xmlReader.Name != "EntityList"))
                        {
                            if (xmlReader.EOF)
                                break;
                            xmlReader.ReadToFollowing("Entity"); ;
                        }

                        


                        

                        //tempEnt.readEntityProperties(xmlReader);

                        //tempEnt.UpdateFromPropertyList();
                    }
                }
                xmlReader.Read();

            }

            return levelDataStore;


        }

        public static List<CEntityProperty> readProperties(XmlReader reader)
        {
            List<CEntityProperty> propList = new List<CEntityProperty>();
            reader.ReadToFollowing("EntityData");
            while (!(reader.Name == "EntityData" && reader.NodeType == XmlNodeType.EndElement))
            {
                reader.Read();
                reader.Read();

                if (reader.Name == "Entity")
                {
                    break;
                }

                reader.MoveToContent();

                if (reader.Name == "EntityData" && reader.NodeType == XmlNodeType.EndElement)
                    break;
                string propName = reader.Name;

                if (propName.Contains("KeyFrame") == true)
                {
                    LoadKeyFrameData(reader, ref propList);
                    propName = reader.Name;
                }

                if (reader.Name == "EntityData" && reader.NodeType == XmlNodeType.EndElement)
                    break;
                string propVal;

                if (reader.IsEmptyElement)
                    propVal = "";
                else
                {
                    reader.Read();
                }

                propVal = reader.Value;

                propList.Add(new CEntityProperty(propName, propVal, EPropertyGridInputType.TextBox));
            }

            return propList;

        }

        public static void LoadKeyFrameData(XmlReader Reader, ref List<CEntityProperty> propList)
        {
            while (Reader.NodeType == XmlNodeType.Element && Reader.Name == "KeyFrame")
            {
                propList.Add(new CEntityProperty("KeyFrame", "", EPropertyGridInputType.TextBox));

                Reader.Read();
                Reader.Read();
                Reader.Read();

                propList.Add(new CEntityProperty("Position", Reader.Value, EPropertyGridInputType.TextBox));

                Reader.Read();
                Reader.Read();
                Reader.Read();
                Reader.Read();

                propList.Add(new CEntityProperty("Rotation", Reader.Value, EPropertyGridInputType.TextBox));

                Reader.Read();
                Reader.Read();
                Reader.Read();
                Reader.Read();

                propList.Add(new CEntityProperty("StartTime", Reader.Value, EPropertyGridInputType.TextBox));

                Reader.Read();
                Reader.Read();
                Reader.Read();
                Reader.Read();
                Reader.Read();
            }
        }
    }
}
#endif
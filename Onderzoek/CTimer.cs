﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Onderzoek
{
    class CTimer
    {
        DateTime m_start;
        DateTime m_end;
        long m_totalTime = 0;
        float m_avgTime = 0;
        int m_count = 0;
        float m_dif = 0;

        public void BeginTimer()
        {
            m_start = DateTime.Now;
        }

        public void EndTimer()
        {
            m_end = DateTime.Now;

            ++m_count;
            m_dif = (float)(m_end.Ticks - m_start.Ticks) / 10000.0f;
            m_totalTime += m_end.Ticks - m_start.Ticks;
            m_avgTime = ((float)m_totalTime / (float)m_count) / 10000.0f;
        }
    }
}

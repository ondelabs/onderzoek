﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Onderzoek
{
    public struct VertexPositionNormal
    {
        private Vector3 m_position;
        private Vector3 m_normal;

        public Vector3 GetPosition()
        {
            return m_position;
        }

        public Vector3 GetNormal()
        {
            return m_normal;
        }

        public void SetNormal(Vector3 Normal)
        {
            m_normal = Normal;
        }

        public VertexPositionNormal(Vector3 Position, Vector3 Normal)
        {
            this.m_position = Position;
            this.m_normal = Normal;
        }

        public static VertexElement[] VertexElements =
        {
            new VertexElement(0, 0, VertexElementFormat.Vector3, VertexElementMethod.Default, VertexElementUsage.Position, 0),
            new VertexElement(0, sizeof(float)*3, VertexElementFormat.Vector3, VertexElementMethod.Default, VertexElementUsage.Normal, 0),
        };

        public static int SizeInBytes = (sizeof(float) * 6);
    }
}

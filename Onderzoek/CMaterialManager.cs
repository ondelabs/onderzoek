﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

using Onderzoek.Visual;
using Onderzoek.ModelData;

using System.Xml;

namespace Onderzoek
{
    public class MaterialDataStruct
    {
        public Vector4 diffuse, ambient, emissive, specColour;
        public float specPower;

        public Texture2D diffuseTex, normalTex, specularTex;
        public string diffuseTexPath, normalTexPath, specularTexPath;
        public int diffuseTextureId, normalTextureId, specularTexId;

        public MaterialDataStruct()
        {
            diffuseTextureId = -1;
            normalTextureId = -1;
            specularTexId = -1;
            diffuse = new Vector4();
            ambient = new Vector4();
            emissive = new Vector4();
        }

        public void setFromMaterial(CMaterial material, ContentManager content)
        {
            diffuse = material.m_diffuse;
            ambient = material.m_ambient;
            emissive = material.m_emissive;

            if (material.m_textureID != -1)
            {
                diffuseTex = CVisual.Instance.GetLevelTextureFromId(material.m_textureID);
                diffuseTextureId = material.m_textureID;

            }

            if (material.m_normalID != -1)
            {
                normalTex = CVisual.Instance.GetLevelTextureFromId(material.m_normalID);
                normalTextureId = material.m_normalID;
            }

            if (material.m_specularID != -1)
            {
                specularTex = CVisual.Instance.GetLevelTextureFromId(material.m_specularID);
                specularTexId = material.m_specularID;
            }

            this.specColour = material.m_specularColour;
            specPower = material.m_specularPower;

        }

    }


    public class CMaterialManager
    {
        static CMaterialManager m_instance;

        Dictionary<string, List<CMaterial>> m_materialDictionary;


        public static CMaterialManager Instance
        {
            get
            {
                if (m_instance != null)
                    return m_instance;
                m_instance = new CMaterialManager();
                return m_instance;
            }
        }

        public List<CMaterial> getMaterialList(ContentManager content, string materialName)
        {
            if(m_materialDictionary.ContainsKey(materialName))
                return m_materialDictionary[materialName];

            XmlTextReader reader = new XmlTextReader("Materials"+ "\\" + materialName);

            List<MaterialDataStruct> matDataStructList = new List<MaterialDataStruct>();
            loadMaterialXML(reader, matDataStructList);

            List<CMaterial> mlist=  createMaterialFromMaterialData(matDataStructList, content);

            m_materialDictionary.Add(materialName, mlist);

            return mlist;

        }


        public List<CMaterial> createMaterialFromMaterialData(List<MaterialDataStruct> materialDataStructList, ContentManager content)
        {

            List<CMaterial> matList = new List<CMaterial>();

            for (int i = 0; i < materialDataStructList.Count; i++)
            {
                matList.Add(new CMaterial());
                matList[matList.Count - 1].m_diffuse = materialDataStructList[i].diffuse;
                matList[matList.Count - 1].m_ambient = materialDataStructList[i].ambient;
                matList[matList.Count - 1].m_emissive = materialDataStructList[i].emissive;

                //matList[matList.Count - 1].m_textureID = materialDataStructList[i].diffuse;

                //if (materialDataStructList[i].diffuseTex != null)
                if (materialDataStructList[i].diffuseTexPath != null)
                    matList[matList.Count - 1].m_textureID = CVisual.Instance.LoadLevelTexture(materialDataStructList[i].diffuseTexPath, content);
                else
                    matList[matList.Count - 1].m_textureID = materialDataStructList[i].diffuseTextureId;



                if (materialDataStructList[i].normalTexPath != null)
                    matList[matList.Count - 1].m_normalID = CVisual.Instance.LoadLevelTexture(materialDataStructList[i].normalTexPath, content);
                else
                    matList[matList.Count - 1].m_normalID = materialDataStructList[i].normalTextureId;


                if (materialDataStructList[i].specularTexPath != null)
                    matList[matList.Count - 1].m_specularID = CVisual.Instance.LoadLevelTexture(materialDataStructList[i].specularTexPath, content);
                else
                    matList[matList.Count - 1].m_specularID = materialDataStructList[i].specularTexId;

                matList[matList.Count - 1].m_specularColour = materialDataStructList[i].specColour;
                matList[matList.Count - 1].m_specularPower = materialDataStructList[i].specPower;

                matList[matList.Count - 1].m_effect = CVisual.Instance.GetShaderID(matList[matList.Count - 1]);

            }


            return matList;

            //materialEditorControl1.level.editorScreen.m_entityList[0].SetMaterialList(matList);

            //materialEditorControl1.level.editorScreen.RefreshSceneGraph();

        }

        public List<string> loadMaterialXML(XmlTextReader reader, List<MaterialDataStruct> dataStructList)
        {
            List<string> modelName = new List<string>();

            reader.ReadToFollowing("ModelNameList");
            reader.Read();
            while (reader.NodeType != XmlNodeType.EndElement && reader.Name != "ModelNameList")
            {
                if (reader.NodeType == XmlNodeType.Element && reader.Name == "ModelName")
                {
                    reader.Read();

                    modelName.Add(reader.Value);

                }
                reader.Read();

            }


            reader.ReadToFollowing("SkinData");
            reader.ReadToDescendant("Material");

            //reader.Read();

            dataStructList.Clear();


            for (int i = 0; !reader.EOF; i++)
            {
                dataStructList.Add(new MaterialDataStruct());

                reader.ReadToDescendant("diffuse");
                reader.Read();
                dataStructList[i].diffuse = CToolbox.StringToVector4(reader.Value);

                reader.ReadToFollowing("ambient");
                reader.Read();
                dataStructList[i].ambient = CToolbox.StringToVector4(reader.Value);

                reader.ReadToFollowing("emissive");
                reader.Read();
                dataStructList[i].emissive = CToolbox.StringToVector4(reader.Value);


                reader.ReadToFollowing("diffuseTex");


                if (!reader.IsEmptyElement)
                {
                    reader.Read();
                    dataStructList[i].diffuseTexPath = (reader.Value);
                }

                reader.ReadToFollowing("normalTex");

                if (!reader.IsEmptyElement)
                {
                    reader.Read();
                    dataStructList[i].normalTexPath = (reader.Value);
                }

                reader.ReadToFollowing("specularTex");

                if (!reader.IsEmptyElement)
                {
                    reader.Read();
                    dataStructList[i].specularTexPath = (reader.Value);
                }


                reader.ReadToFollowing("specularColour");
                reader.Read();
                dataStructList[i].specColour = CToolbox.StringToVector4((reader.Value));

                reader.ReadToFollowing("specularPower");
                reader.Read();
                dataStructList[i].specPower = float.Parse(reader.Value);

                reader.ReadToFollowing("Material");

            }

            return modelName;




        }

        public static bool loadMaterialXML(CModel Model, string MaterialFilePath, ref List<CMaterial> dataStructList, ref ContentManager Content)
        {
            XmlTextReader reader = null;
            try
            {
                reader = new XmlTextReader(MaterialFilePath);
            }
            catch (Exception e)
            {
                return false;
            }


            reader.ReadToFollowing("SkinData");
            reader.ReadToDescendant("Material");

            //reader.Read();

            if (dataStructList != null)
            {
                dataStructList.Clear();
            }
            else
            {
                dataStructList = new List<CMaterial>();
            }


            for (int i = 0; !reader.EOF; i++)
            {
                dataStructList.Add(new CMaterial());

                reader.ReadToDescendant("diffuse");
                reader.Read();
                dataStructList[i].m_diffuse = CToolbox.StringToVector4(reader.Value);

                reader.ReadToFollowing("ambient");
                reader.Read();
                dataStructList[i].m_ambient = CToolbox.StringToVector4(reader.Value);

                reader.ReadToFollowing("emissive");
                reader.Read();
                dataStructList[i].m_emissive = CToolbox.StringToVector4(reader.Value);


                reader.ReadToFollowing("diffuseTex");


                if (!reader.IsEmptyElement)
                {
                    reader.Read();
                    string diffuse_tex_path = (reader.Value);

                    if (diffuse_tex_path.Equals("-1 default") == true)
                    {
                        Effect effect = Model.GetMaterialAtIndex(i);
                        if (effect is BasicEffect)
                        {
                            dataStructList[i].m_textureID = CVisual.Instance.LoadLevelTexture(Model.GetModelName() + i, Content, ((BasicEffect)effect).Texture);
                        }
                        else if (effect is XNAnimation.Effects.SkinnedModelBasicEffect)
                        {
                            dataStructList[i].m_textureID = CVisual.Instance.LoadLevelTexture(Model.GetModelName() + i, Content,
                                ((XNAnimation.Effects.SkinnedModelBasicEffect)effect).DiffuseMap);
                        }
                    }
                    else
                    {
                        dataStructList[i].m_textureID = CVisual.Instance.LoadLevelTexture(diffuse_tex_path, Content);
                    }
                }

                reader.ReadToFollowing("normalTex");

                if (!reader.IsEmptyElement)
                {
                    reader.Read();
                    string normal_tex_path = (reader.Value);
                    dataStructList[i].m_normalID = CVisual.Instance.LoadLevelTexture(normal_tex_path, Content);
                }

                reader.ReadToFollowing("specularTex");

                if (!reader.IsEmptyElement)
                {
                    reader.Read();
                    string specular_tex_path = (reader.Value);
                    dataStructList[i].m_specularID = CVisual.Instance.LoadLevelTexture(specular_tex_path, Content);
                }


                reader.ReadToFollowing("specularColour");
                reader.Read();
                dataStructList[i].m_specularColour = CToolbox.StringToVector4((reader.Value));

                reader.ReadToFollowing("specularPower");
                reader.Read();
                dataStructList[i].m_specularPower = float.Parse(reader.Value);

                reader.ReadToFollowing("Material");

            }

            return true;
        }

    }
    
}

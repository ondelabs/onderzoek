﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Onderzoek.Entity;
using Onderzoek.ModelData;
using Onderzoek.Visual;
using Onderzoek.Screens;
using Onderzoek.SceneGraph;
using Onderzoek.SpatialTree;

namespace Onderzoek.Physics
{
    public class CCollision
    {
        public struct STriData
        {
            public Vector3 s_firstPos;
            public Vector3 s_secondPos;
            public Vector3 s_thirdPos;
        }

        private struct SUpdateTicketData
        {
            public SUpdateTicketData(Matrix ParWorldMatrix, Quaternion ParRotation, bool Child)
            {
                s_parWorldMatrix = ParWorldMatrix;
                s_parRotMatrix = ParRotation;
                s_child = Child;
                s_parent = null;
            }

            public SUpdateTicketData(Matrix ParWorldMatrix, Quaternion ParRotation, bool Child, CEntity Parent)
            {
                s_parWorldMatrix = ParWorldMatrix;
                s_parRotMatrix = ParRotation;
                s_child = Child;
                s_parent = Parent;
            }

            public bool s_child;
            public Matrix s_parWorldMatrix;
            public Quaternion s_parRotMatrix;
            public CEntity s_parent;
        }

        public struct SCollisionData
        {
            public Vector3 s_prvPosition;
            public Vector3 s_nxtPosition;
            public Ray s_ray;
            private Ray s_oldRay;
            private bool s_checkWithPrv;
            public List<Vector3> s_prvCloseCollisionDir;
            public List<float> s_prvCloseCollisionDist;
            public List<CEntity> s_otherEntity;
            public List<int> s_otherEntityID;
            public List<CEntityOBB> s_otherEntityOBB;

            public SCollisionData(Vector3 NextPosition)
            {
                s_prvCloseCollisionDist = new List<float>();
                s_prvCloseCollisionDir = new List<Vector3>();
                s_otherEntity = new List<CEntity>();
                s_otherEntityOBB = new List<CEntityOBB>();
                s_otherEntityID = new List<int>();

                s_nxtPosition = NextPosition;
                s_prvPosition = NextPosition;

                s_ray = new Ray();
                s_oldRay = new Ray();

                s_checkWithPrv = false;
            }

            public bool CheckWithPrvData
            {
                get { return s_checkWithPrv; }
                set { s_checkWithPrv = value; }
            }

            public Ray PrvRay
            {
                get { return s_oldRay; }
            }

            public void Update(Vector3 PrvPosition, Vector3 NxtPosition)
            {
                s_prvPosition = PrvPosition;
                s_nxtPosition = NxtPosition;

                if (Vector3.DistanceSquared(s_nxtPosition, s_prvPosition) == 0)
                {
                    s_ray = new Ray(s_nxtPosition, Vector3.Zero);
                }
                else
                {
                    s_ray = new Ray(s_nxtPosition, CToolbox.UnitVector(s_nxtPosition - s_prvPosition));
                }

                if (Vector3.DistanceSquared(s_ray.Direction, s_oldRay.Direction) > 0.00001f)
                {
                    s_checkWithPrv = false;
                    Reset();
                }

                s_oldRay = s_ray;
            }

            public void Reset()
            {
                s_prvCloseCollisionDist.Clear();
                s_prvCloseCollisionDir.Clear();
                s_otherEntity.Clear();
                s_otherEntityOBB.Clear();
                s_otherEntityID.Clear();
            }

            public bool StoreNewData(Vector3 RayDirection, float Distance, CEntity OtherEntity, CEntityOBB OtherEntityOBB)
            {
                if (Distance != 0)
                {
                    s_otherEntity.Add( OtherEntity );
                    s_prvCloseCollisionDir.Add( RayDirection );
                    s_prvCloseCollisionDist.Add( Distance );
                    s_otherEntityOBB.Add( OtherEntityOBB );

                    return true;
                }

                return false;
            }
        }

        static CCollision instance;

        // For terrain heightmap stuff
        Mutex m_terrainMutex = new Mutex();
        private List<CTerrainEntity> m_terrainEntities;
        private CGameScreenHelper m_curScreen;
        private CSceneGraph m_sg;
        Mutex m_ticketMutex = new Mutex();
        // PLEASE remember to use a mutes wait and release when using this member variable!
        private List<SUpdateTicketData> m_updateTicketList;

        public static CCollision Instance
        {
            get { return instance; }
        }

        public CCollision()
        {
            if (instance != null)
                throw new Exception("Cannot have multiple singletons");

            instance = this;
        }

        public void Initialise(CGameScreenHelper CurScreen, CSceneGraph SG)
        {
            m_sg = SG;
            m_curScreen = CurScreen;
            m_terrainEntities = new List<CTerrainEntity>();
            m_updateTicketList = new List<SUpdateTicketData>();
        }

        public void SetSceneGraph(CSceneGraph SG)
        {
            m_sg = SG;
        }

        public void Dispose()
        {
            m_curScreen = null;

            if (m_terrainEntities != null)
            {
                m_terrainEntities.Clear();
                m_terrainEntities = null;
            }

            m_sg = null;

            instance = null;
        }

        public void RegisterTerrainIDForCollision(CTerrainEntity TerrainEntity)
        {
            m_terrainEntities.Add(TerrainEntity);
        }

        public bool RemoveTerrain(CTerrainEntity TerrainEntity)
        {
            return m_terrainEntities.Remove(TerrainEntity);
        }

        Vector3 m_down = Vector3.Down;
        public float GetDistFromTerrain(Vector3 Position, ref CTerrainEntity.SHeightMapData HMData, Vector3 RayDir, ref CEntity OtherEntity)
        {
            float dist = 0;

            for (int i = 0; i < m_terrainEntities.Count; ++i)
            {
                if (m_terrainEntities[i].GetHeightOfCurrentPos(Position, ref HMData, RayDir, ref dist) == true)
                {
                    OtherEntity = m_terrainEntities[i];
                    HMData.s_curCollisionTerrainID = i;
                    return dist;
                }
            }

            // Default value when there's no collision against any of the terrains
            return 1000.0f;
        }

        public float RayVsTerrain(Vector3 RayOrigin, Vector3 RayDir, ref CTerrainEntity.SHeightMapData HMData)
        {
                float dist = 0;

                for (int i = 0; i < m_terrainEntities.Count; ++i)
                {
                    if (m_terrainEntities[i].RayVsTerrain(RayOrigin, ref HMData, RayDir, ref dist) == true)
                    {
                        HMData.s_curCollisionTerrainID = i;
                        return dist;
                    }
                }

                // Default value when there's no collision against any of the terrains
                return 1000.0f;
        }

        public bool RayVsEntityBB(Ray ShotRay, ref float Dist, int EntityID)
        {
            return m_curScreen.GetEntity(EntityID).CheckAgainstRay(ShotRay, ref Dist);
        }

        public bool RayVsEntityChildBB(Ray ShotRay, ref float Dist, int EntityID)
        {
            return m_curScreen.GetEntity(EntityID).CheckChildBBAgainstRay(ShotRay, ref Dist);
        }

        public bool RayVsEntitySphere(Ray ShotRay, int EntityID)
        {
            return m_curScreen.GetEntity(EntityID).CheckRayVsSphere(ShotRay);
        }

        public List<CEntity> GetNearDynamicEntities(CEntity CurEntity, CSpatialTree CurEntityLeafNode, BoundingSphere BSphere, int NumSearchLevels)
        {
            List<int> entity_list = GetNearDynamicEntityIDs(CurEntity, CurEntityLeafNode, NumSearchLevels, BSphere);

            List<CEntity> actual_entities = new List<CEntity>();
            for (int i = 0; i < entity_list.Count; ++i)
            {
                CEntity other = m_curScreen.GetEntity(entity_list[i]);

                // Ignore static entities. // Ignore entities which are set not to collide. // Ignore itself.
                if (other.IsDynamicEntity() == false || other.IsCollidable() == false || other.GetSGID() == CurEntity.GetSGID()
                    || (CurEntity.DissociationID & other.AssociationID) != 0)
                {
                    entity_list.RemoveAt(i);
                    --i;
                    continue;
                }

                //if (BSphere.Intersects(other.GetBB()) == false)
                //{
                //    entity_list.RemoveAt(i);
                //    --i;
                //    continue;
                //}

                actual_entities.Add(other);
            }

            return actual_entities;
        }

        public List<int> GetNearDynamicEntityIDs(CEntity CurEntity, CSpatialTree CurEntityLeafNode, BoundingSphere BSphere, int NumSearchLevels)
        {
            List<int> entity_list = GetNearDynamicEntityIDs(CurEntity, CurEntityLeafNode, NumSearchLevels, BSphere);

            for (int i = 0; i < entity_list.Count; ++i)
            {
                CEntity other = m_curScreen.GetEntity(entity_list[i]);

                // Ignore static entities. // Ignore entities which are set not to collide. // Ignore itself.
                if (other.IsDynamicEntity() == false || other.IsCollidable() == false || other.GetSGID() == CurEntity.GetSGID()
                    || (CurEntity.DissociationID & other.AssociationID) != 0)
                {
                    entity_list.RemoveAt(i);
                    --i;
                    continue;
                }

                //if (BSphere.Intersects(other.GetBB()) == false)
                //{
                //    entity_list.RemoveAt(i);
                //    --i;
                //    continue;
                //}
            }

            return entity_list;
        }

        public List<int> GetNearEntityIDs(CEntity CurEntity, CSpatialTree CurEntityLeafNode, BoundingSphere BSphere, int NumSearchLevels)
        {
            List<int> entity_list = GetNearDynamicEntityIDs(CurEntity, CurEntityLeafNode, NumSearchLevels, BSphere);

            for (int i = 0; i < entity_list.Count; ++i)
            {
                CEntity other = m_curScreen.GetEntity(entity_list[i]);

                // Ignore entities which are set not to collide. // Ignore itself.
                if (other.IsCollidable() == false || other.GetSGID() == CurEntity.GetSGID() || (CurEntity.DissociationID & other.AssociationID) != 0)
                {
                    entity_list.RemoveAt(i);
                    --i;
                    continue;
                }

                ////if (BSphere.Intersects(other.GetBB()) == false)
                //if (BSphere.Intersects(other.GetBoundingSphere()) == false)
                //{
                //    entity_list.RemoveAt(i);
                //    --i;
                //    continue;
                //}
            }

            return entity_list;
        }

        public List<CEntity> GetNearDynamicEntities(CEntity CurEntity, CSpatialTree CurEntityLeafNode, CEntityOBB OOBB, int NumSearchLevels)
        {
            List<int> entity_list = GetNearDynamicEntityIDs(CurEntity, CurEntityLeafNode, NumSearchLevels, CurEntity.GetBoundingSphere());

            List<CEntity> actual_entities = new List<CEntity>();
            for (int i = 0; i < entity_list.Count; ++i)
            {
                CEntity other = m_curScreen.GetEntity(entity_list[i]);

                // Ignore static entities. // Ignore entities which are set not to collide. // Ignore itself.
                if (other.IsDynamicEntity() == false || other.IsCollidable() == false || other.GetSGID() == CurEntity.GetSGID()
                || (CurEntity.DissociationID & other.AssociationID) != 0)
                {
                    entity_list.RemoveAt(i);
                    --i;
                    continue;
                }

                Matrix rot = CurEntity.GetWorldMatrix();
                rot.Translation = Vector3.Zero;
                float dist = 0;

                // OOBB collision check here.
                if (RayVsOOBB(CToolbox.UnitVector(CurEntity.GetWorldPosition() - other.GetWorldPosition()), other.GetWorldPosition(), OOBB,
                    CurEntity.GetWorldMatrix(), rot, ref dist) == false)
                {
                    entity_list.RemoveAt(i);
                    --i;
                    continue;
                }

                if (dist > other.GetCollisionDistance)
                {
                    entity_list.RemoveAt(i);
                    --i;
                    continue;
                }

                actual_entities.Add(other);
            }

            return actual_entities;
        }

        public List<int> GetNearDynamicEntityIDs(CEntity CurEntity, CSpatialTree CurEntityLeafNode, CEntityOBB OOBB, int NumSearchLevels)
        {
            List<int> entity_list = GetNearDynamicEntityIDs(CurEntity, CurEntityLeafNode, NumSearchLevels, CurEntity.GetBoundingSphere());

            for (int i = 0; i < entity_list.Count; ++i)
            {
                CEntity other = m_curScreen.GetEntity(entity_list[i]);

                // Ignore static entities. // Ignore entities which are set not to collide. // Ignore itself.
                if (other.IsDynamicEntity() == false || other.IsCollidable() == false || other.GetSGID() == CurEntity.GetSGID()
                || (CurEntity.DissociationID & other.AssociationID) != 0)
                {
                    entity_list.RemoveAt(i);
                    --i;
                    continue;
                }

                Matrix rot = CurEntity.GetWorldMatrix();
                rot.Translation = Vector3.Zero;
                float dist = 0;

                // OOBB collision check here.
                if (RayVsOOBB(CToolbox.UnitVector(CurEntity.GetWorldPosition() - other.GetWorldPosition()), other.GetWorldPosition(), OOBB,
                    CurEntity.GetWorldMatrix(), rot, ref dist) == false)
                {
                    entity_list.RemoveAt(i);
                    --i;
                    continue;
                }

                if (dist > other.GetCollisionDistance)
                {
                    entity_list.RemoveAt(i);
                    --i;
                    continue;
                }
            }

            return entity_list;
        }

        public List<int> GetNearDynamicEntityIDs(CEntity CurEntity, CSpatialTree CurEntityLeafNode, BoundingBox CurBB, int NumSearchLevels)
        {
            List<int> entity_list = GetNearDynamicEntityIDs(CurEntity, CurEntityLeafNode, NumSearchLevels, CurEntity.GetBoundingSphere());

            for (int i = 0; i < entity_list.Count; ++i)
            {
                CEntity other = m_curScreen.GetEntity(entity_list[i]);

                // Ignore static entities. // Ignore entities which are set not to collide. // Ignore itself.
                if (other.IsDynamicEntity() == false || other.IsCollidable() == false || other.GetSGID() == CurEntity.GetSGID()
                || (CurEntity.DissociationID & other.AssociationID) != 0)
                {
                    entity_list.RemoveAt(i);
                    --i;
                    continue;
                }

                Matrix rot = CurEntity.GetWorldMatrix();
                rot.Translation = Vector3.Zero;

                // AABB collision check here.
                if (AABBvsAABB(other.GetBB(), CurBB) == false)
                {
                    entity_list.RemoveAt(i);
                    --i;
                    continue;
                }
            }

            return entity_list;
        }

        private List<int> GetNearDynamicEntityIDs(CEntity CurEntity, CSpatialTree CurEntityLeafNode, int NumSearchLevels, BoundingSphere BSphere)
        {
            return CurEntityLeafNode.CheckSphereVsSphereInTree(BSphere, NumSearchLevels);
        }

        //// FOR MULTITHREADING
        //// Also so need semaphore or mutex, or a autoreset thingy with set and waitone functions.
        //// It is very ineficient to create new hreads every frame, so it is best to use the above mentioned techniques and a while loop
        //// which will determine if the thread needs to exit (during game shutdown) or to just keep going (during runtime).
        //private void ThreadedGetNearDynamicEntities()
        //{
        //    m_entityList = GetNearDynamicEntities(m_CurEntity, m_CurEntityLeafNode, m_CurEntity.GetBoundingSphere(), 2);
        //}
        //List<int> m_entityList;
        //CEntity m_CurEntity;
        //CSpatialTree m_CurEntityLeafNode;

        // Check for only simple collisions
        public bool CheckForSimpleCollisions(CEntity CurEntity, CSpatialTree CurEntityLeafNode, Vector3 PrvPosition, Vector3 NxtPosition, ref SCollisionData CollisionData)
        {
            CollisionData.Update(PrvPosition, NxtPosition);

            List<int> entity_list = new List<int>();
            if (CurEntity.SimpleCollision() == false)
            {
                entity_list = CurEntityLeafNode.CheckRayVsSphereInTree(CollisionData.s_ray);
            }

            List<int> close_entities = GetNearEntityIDs(CurEntity, CurEntityLeafNode, CurEntity.GetBoundingSphere(), 1);
            for (int i = 0; i < close_entities.Count; ++i)
            {
                if (entity_list.Contains(close_entities[i]) == false)
                {
                    entity_list.Add(close_entities[i]);
                }
            }

            CollisionData.Reset();

            bool collision = false;
            for (int i = 0; i < entity_list.Count; ++i)
            {
                CEntity other = m_curScreen.GetEntity(entity_list[i]);

                // Ignore itself
                if (other.GetSGID() == CurEntity.GetSGID() || other.IsCollidable() == false || (CurEntity.DissociationID & other.AssociationID) != 0
                    || other.IsHidden() == true)
                {
                    continue;
                }

                // Check to see if there is a collision with the main collision box
                if (AABBvsAABB(CurEntity.GetBB(), other.GetBB()) == true)
                //if (CurEntity.GetBoundingSphere().Intersects(other.GetBoundingSphere()) == true)
                {
                    // Check for collisions
                    CEntityOBB[] obb_wrld = other.GetWorldOBB();

                    if (other.SimpleCollision() == false || CurEntity.SimpleCollision() == false)
                    {
                        if (IsCurEntityCollidingWithOtherEntity(obb_wrld, CurEntity, other) == true)
                        {
                            collision = true;
                        }
                    }
                }
            }

            return collision;
        }

        // Check for all collisions
        // Try passing the unaltered world position.
        public bool CheckForCollisions(CEntity CurEntity, CSpatialTree CurEntityLeafNode, Vector3 PrvPosition, Vector3 NxtPosition, ref SCollisionData CollisionData)
        {
            CollisionData.Update(PrvPosition, NxtPosition);

            //// FOR MULTITHREADING
            //m_CurEntity = CurEntity;
            //m_CurEntityLeafNode = CurEntityLeafNode;
            //Thread temp = new Thread(ThreadedGetNearDynamicEntities);
            //temp.Start();

            List<int> entity_list = new List<int>();
            if (CurEntity.SimpleCollision() == false)
            {
                entity_list = CurEntityLeafNode.CheckRayVsSphereInTree(CollisionData.s_ray);
            }

            //// FOR MULTITHREADING
            //while (temp.ThreadState == ThreadState.Running)
            //{
            //    int stop = 1;
            //}
            //if (m_entityList.Count > 0)
            //{
            //    entity_list.AddRange(m_entityList);
            //}

            // Swept test
            if (CollisionData.CheckWithPrvData == true && CollisionData.s_otherEntity != null)
            {
                // Now check to see if the new collision data is still colliding with the other entity.
                GetDistToBBs(CurEntity, ref CollisionData);
            }

            List<int> close_entities = GetNearEntityIDs(CurEntity, CurEntityLeafNode, CurEntity.GetBoundingSphere(), 1);
            for (int i = 0; i < close_entities.Count; ++i)
            {
                if (entity_list.Contains(close_entities[i]) == false)
                {
                    entity_list.Add(close_entities[i]);
                }
            }

            //for (int i = 0; i < CollisionData.s_otherEntityID.Count; ++i)
            //{
            //    if (entity_list.Contains(CollisionData.s_otherEntityID[i]) == true)
            //    {
            //        entity_list.Remove(CollisionData.s_otherEntityID[i]);
            //    }
            //}

            CollisionData.Reset();

            bool found = false;
            bool is_child = false;
            bool collision = false;
            for (int i = 0; i < entity_list.Count; ++i)
            {
                CEntity other = m_curScreen.GetEntity(entity_list[i]);

                // Ignore itself
                if (other.GetSGID() == CurEntity.GetSGID() || other.IsCollidable() == false || (CurEntity.DissociationID & other.AssociationID) != 0
                    || other.IsHidden() == true)
                {
                    continue;
                }

                // Check to see if there is a collision with the main collision box
                if (AABBvsAABB(CurEntity.GetBB(), other.GetBB()) == true)
                //if (CurEntity.GetBoundingSphere().Intersects(other.GetBoundingSphere()) == true)
                {
                    // Check for collisions
                    CEntityOBB[] obb_wrld = other.GetWorldOBB();

                    Vector3 new_ray_pos;
                    GetRayPosition(CurEntity.GetWorldOBB(), NxtPosition, obb_wrld, other.GetWorldPosition(), out new_ray_pos);
                    CollisionData.s_ray.Position = new_ray_pos;

                    bool is_stored = false;
                    if (other.SimpleCollision() == false && CurEntity.SimpleCollision() == false)
                    {
                        if (GetDistToBBs(CollisionData.s_ray, obb_wrld, CurEntity, other, ref CollisionData, ref is_stored) == true)
                        {
                            collision = true;
                        }

                        if (is_stored == true)
                        {
                            CollisionData.s_otherEntityID.Add(entity_list[i]);
                            found = true;
                        }
                    }
                    else
                    {
                        if (IsCurEntityCollidingWithOtherEntity(obb_wrld, CurEntity, other) == true)
                        {
                            collision = true;
                        }
                    }
#if EDITOR
            }
#else
                    // Check to see if there is a collision with the main collision box
                    //if (CurEntity.GetBB().Intersects(other.GetBB()) == true) <- not needed since we're testing above.
                    {
                        // Now check for parent child relationships.
                        is_child = CheckForChildParentRelationship(CurEntity, other, CollisionData.s_nxtPosition);
                    }
                }
#endif
            }

            // One more check needs to be done regarding the parent child realtionship
            // This checks to make sure that the child is still a child of the parent.
            //if (is_child == false)
            {
                CheckIfStillChild(CurEntity, CollisionData.s_nxtPosition);
            }

            if (found == true)
            {
                CollisionData.CheckWithPrvData = true;
                //CollisionData.s_otherEntityID = entity_list[i];
            }
            else
            {
                CollisionData.CheckWithPrvData = false;
            }

            return collision;
        }

        /// <summary>
        /// This is used to check for simple AABB vs AABB collisions.
        /// When a AABB vs AABB has been detected it will call has hit.
        /// It will pass zero for all values since it isn't possible to work out the direction or distance of the collision with a AABB vs AABB test.
        /// </summary>
        /// <param name="CurEntity"></param>
        /// <param name="CurEntityLeafNode"></param>
        /// <param name="PrvPosition"></param>
        /// <param name="NxtPosition"></param>
        /// <param name="CollisionData"></param>
        /// <returns>Returns true if a AABB vs AABB collision has been detected.</returns>
        public bool CheckForSphereCollisions(CEntity CurEntity, CSpatialTree CurEntityLeafNode, Vector3 PrvPosition, Vector3 NxtPosition, ref SCollisionData CollisionData)
        {
            CollisionData.Update(PrvPosition, NxtPosition);

            // Swept test
            if (CollisionData.CheckWithPrvData == true && CollisionData.s_otherEntity != null)
            {
                // Now check to see if the new collision data is still colliding with the other entity.
                GetDistToBBs(CurEntity, ref CollisionData);
            }

            List<int> entity_list = GetNearEntityIDs(CurEntity, CurEntityLeafNode, CurEntity.GetBoundingSphere(), 4);

            CollisionData.Reset();

            bool found = false;
            for (int i = 0; i < entity_list.Count; ++i)
            {
                CEntity other = m_curScreen.GetEntity(entity_list[i]);

                // Ignore itself
                if (other.GetSGID() == CurEntity.GetSGID() || other.IsCollidable() == false || (CurEntity.DissociationID & other.AssociationID) != 0)
                {
                    continue;
                }

                // Check to see if there is a collision with the main collision box
                //if (AABBvsAABB(CurEntity.GetBB(), other.GetBB()) == true)
                if (CurEntity.GetBoundingSphere().Intersects(other.GetBoundingSphere()) == true)
                {
                    found = true;

                    CurEntity.HasHit(other, 0, Vector3.Zero);
                }
            }

            return found;
        }

        private void GetRayPosition(CEntityOBB[] CurEntityOBBArray, Vector3 PlayerPos, CEntityOBB[] OtherEntityBB, Vector3 OtherEntityPosition,
            out Vector3 NewRayPos)
        {
            NewRayPos = PlayerPos;

            float dist = float.MaxValue;
            int index0 = -1;
            int index1 = -1;

            for (int i = 0; i < CurEntityOBBArray.Length; ++i)
            {
                // Does not have any children
                if (CurEntityOBBArray[i].m_child.Length == 0)
                {
                    float new_dist = Vector3.DistanceSquared(CurEntityOBBArray[i].m_position, OtherEntityPosition);
                    if( new_dist < dist )
                    {
                        index0 = i;
                        dist = new_dist;
                    }
                }
                else // has children
                {
                    for (int j = 0; j < CurEntityOBBArray[i].m_child.Length; ++j)
                    {
                        float new_dist = Vector3.DistanceSquared(CurEntityOBBArray[i].m_child[j].m_position, OtherEntityPosition);
                        if (new_dist < dist)
                        {
                            index0 = i;
                            index1 = j;
                            dist = new_dist;
                        }
                    }
                }
            }

            dist = float.MaxValue;
            int index2 = -1;
            int index3 = -1;

            for (int i = 0; i < OtherEntityBB.Length; ++i)
            {
                // Does not have any children
                if (OtherEntityBB[i].m_child.Length == 0)
                {
                    float new_dist = Vector3.DistanceSquared(OtherEntityBB[i].m_position, PlayerPos);
                    if (new_dist < dist)
                    {
                        index2 = i;
                        dist = new_dist;
                    }
                }
                else // has children
                {
                    for (int j = 0; j < OtherEntityBB[i].m_child.Length; ++j)
                    {
                        float new_dist = Vector3.DistanceSquared(OtherEntityBB[i].m_child[j].m_position, PlayerPos);
                        if (new_dist < dist)
                        {
                            index2 = i;
                            index3 = j;
                            dist = new_dist;
                        }
                    }
                }
            }

            // Use the child bb
            if (index1 != -1 && index3 != -1)
            {
                GetNewRayPosition(CurEntityOBBArray[index0].m_child[index1].m_OBB, PlayerPos, OtherEntityBB[index2].m_child[index3].m_OBB, OtherEntityPosition, ref NewRayPos);
            }
            else if (index1 != -1 && index3 == -1)
            {
                GetNewRayPosition(CurEntityOBBArray[index0].m_child[index1].m_OBB, PlayerPos, OtherEntityBB[index2].m_OBB, OtherEntityPosition, ref NewRayPos);
            }
            else if (index1 == -1 && index3 != -1)
            {
                GetNewRayPosition(CurEntityOBBArray[index0].m_OBB, PlayerPos, OtherEntityBB[index2].m_child[index3].m_OBB, OtherEntityPosition, ref NewRayPos);
            }
            else // use the parent bb
            {
                GetNewRayPosition(CurEntityOBBArray[index0].m_OBB, PlayerPos, OtherEntityBB[index2].m_OBB, OtherEntityPosition, ref NewRayPos);
            }
        }

        private void GetNewRayPosition(BoundingBox CurEntityWrldOBB, Vector3 PlayerPos, BoundingBox OtherEntityMainBB, Vector3 OtherEntityPosition,
            ref Vector3 NewRayPos)
        {
            float min_y = CurEntityWrldOBB.Min.Y, max_y = CurEntityWrldOBB.Max.Y;

            if (OtherEntityMainBB.Min.Y > PlayerPos.Y)
            {
                if (max_y > OtherEntityMainBB.Min.Y)
                {
                    if( max_y < OtherEntityMainBB.Max.Y )
                    {
                        NewRayPos.Y = max_y;
                    }
                    else
                    {
                        NewRayPos.Y = OtherEntityMainBB.Max.Y;
                    }
                }
            }
            else if (OtherEntityMainBB.Max.Y < PlayerPos.Y)
            {
                if (min_y < OtherEntityMainBB.Max.Y)
                {
                    if (min_y > OtherEntityMainBB.Min.Y)
                    {
                        NewRayPos.Y = min_y;
                    }
                    else
                    {
                        NewRayPos.Y = OtherEntityMainBB.Min.Y;
                    }
                }
            }

            #region NA
            //if (OtherEntityMainBB.Min.X > PlayerPos.X)
            //{
            //    for (int i = 0; i < CurEntityWrldOBB.Length; ++i)
            //    {
            //        if (CurEntityWrldOBB[i].X > OtherEntityMainBB.Min.X)
            //        {
            //            if (CurEntityWrldOBB[i].X < OtherEntityMainBB.Max.X)
            //            {
            //                NewRayPos.X = OtherEntityMainBB.Min.X;
            //                break;
            //            }
            //        }
            //    }
            //}
            //else if (OtherEntityMainBB.Max.X < PlayerPos.X)
            //{
            //    for (int i = 0; i < CurEntityWrldOBB.Length; ++i)
            //    {
            //        if (CurEntityWrldOBB[i].X < OtherEntityMainBB.Max.X)
            //        {
            //            if (CurEntityWrldOBB[i].X > OtherEntityMainBB.Min.X)
            //            {
            //                NewRayPos.X = OtherEntityMainBB.Max.X;
            //                break;
            //            }
            //        }
            //    }
            //}

            //if (OtherEntityMainBB.Min.Z > PlayerPos.Z)
            //{
            //    for (int i = 0; i < CurEntityWrldOBB.Length; ++i)
            //    {
            //        if (CurEntityWrldOBB[i].Z > OtherEntityMainBB.Min.Z)
            //        {
            //            if (CurEntityWrldOBB[i].Z < OtherEntityMainBB.Max.Z)
            //            {
            //                NewRayPos.Z = OtherEntityMainBB.Min.Z;
            //                break;
            //            }
            //        }
            //    }
            //}
            //else if (OtherEntityMainBB.Max.Z < PlayerPos.Z)
            //{
            //    for (int i = 0; i < CurEntityWrldOBB.Length; ++i)
            //    {
            //        if (CurEntityWrldOBB[i].Z < OtherEntityMainBB.Max.Z)
            //        {
            //            if (CurEntityWrldOBB[i].Z > OtherEntityMainBB.Min.Z)
            //            {
            //                NewRayPos.Z = OtherEntityMainBB.Max.Z;
            //                break;
            //            }
            //        }
            //    }
            //}
            #endregion
        }

        /// <summary>
        /// This will get the height of the current world position of the current entity.
        /// This should be used when there are height map(s) in your game.
        /// </summary>
        /// <param name="CurEntity">Current Entity (this)</param>
        /// <param name="CurWorldPos">The current entity's world position.</param>
        /// <param name="RayDir">Direction of the ray.</param>
        /// <param name="CurEntityLeafNode">m_entityLeafNode</param>
        /// <param name="HMData">Height Map data.</param>
        /// <returns>True if a valid height was found.</returns>
        public bool GetHeight(CEntity CurEntity, Vector3 CurWorldPos, Vector3 RayDir, CSpatialTree CurEntityLeafNode, ref CTerrainEntity.SHeightMapData HMData)
        {
            // First check for the height
            Ray height_dir_ray = new Ray(CurWorldPos, RayDir);
            CEntity other_entity = null;

            float dist = 1000.0f;
            GetNewHeight(CurEntity, CurWorldPos, CurEntityLeafNode, height_dir_ray, ref dist, ref other_entity);

            // Now check the terrain to see if there are any valid collisions
            float ter_dist = 1000.0f;
            CEntity ter_entity = null;
            ter_dist = GetDistFromTerrain(height_dir_ray.Position, ref HMData, RayDir, ref ter_entity);
            if (ter_dist < dist)
            {
                other_entity = ter_entity;
                dist = ter_dist;
            }

            if (dist != 1000.0f)
            {
                CurEntity.SetNewHeight(other_entity, (height_dir_ray.Position + (height_dir_ray.Direction * dist)));

                return true;
            }

            return false;
        }

        /// <summary>
        /// This will get the height of the current world position of the current entity.
        /// This should be used when there are height map(s) in your game.
        /// </summary>
        /// <param name="CurEntity">Current Entity (this)</param>
        /// <param name="CurWorldPos">The current entity's world position.</param>
        /// <param name="CurEntityLeafNode">m_entityLeafNode</param>
        /// <param name="HMData">Height Map data.</param>
        /// <returns>True if a valid height was found.</returns>
        public bool GetHeight(CEntity CurEntity, Vector3 CurWorldPos, CSpatialTree CurEntityLeafNode, ref CTerrainEntity.SHeightMapData HMData)
        {
            return GetHeight(CurEntity, CurWorldPos, m_down, CurEntityLeafNode, ref HMData);
        }

        /// <summary>
        /// This will get he height a the current world position of the current entity.
        /// This should be called when you DO NOT have any height maps in your game.
        /// </summary>
        /// <param name="CurEntity">Current Entity (this)</param>
        /// <param name="CurWorldPos">The current entity's world position.</param>
        /// <param name="CurEntityLeafNode">m_entityLeafNode</param>
        /// <returns>True if a valid height was found.</returns>
        public bool GetHeight(CEntity CurEntity, Vector3 CurWorldPos, CSpatialTree CurEntityLeafNode)
        {
            // First check for the height
            Ray height_dir_ray = new Ray(CurWorldPos, Vector3.Down);
            CEntity other_entity = null;

            float dist = 1000.0f;
            GetNewHeight(CurEntity, CurWorldPos, CurEntityLeafNode, height_dir_ray, ref dist, ref other_entity);

            if (dist != 1000.0f)
            {
                CurEntity.SetNewHeight(other_entity, (height_dir_ray.Position + (height_dir_ray.Direction * dist)));

                return true;
            }

            return false;
        }

        private void GetNewHeight(CEntity CurEntity, Vector3 CurWorldPos, CSpatialTree CurEntityLeafNode, Ray HeightRay, ref float Height, ref CEntity Other)
        {
            // First check for the height
            List<int> entity_list_h = CurEntityLeafNode.CheckRayVsSphereInTree(HeightRay);

            float dist = 1000.0f;
            for (int i = 0; i < entity_list_h.Count; ++i)
            {
                CEntity other = m_curScreen.GetEntity(entity_list_h[i]);

                // Ignore itself
                if (other.GetSGID() == CurEntity.GetSGID() || other.IsCollidable() == false || other.CanBeStoodOn() == false ||
                    (CurEntity.DissociationID & other.AssociationID) != 0 || other.IsHidden() == true)
                {
                    continue;
                }

                // Check to see if there is a collision with the main collision box
                // USE AABB VS AABB FIRST TO CHECK IF THEY'RE WITHIN RANGE OF A HEIGHT COLLISION.
                // SOMETHING SIMILAR TO PARENT CHILD RELATIONSHIP TEST.
                float? ray_inter_dist = other.GetBB().Intersects(HeightRay);
                if (ray_inter_dist != null)
                {
                    CEntityOBB[] obb_list = other.GetWorldOBB();

                    if (GetDistToBBs(HeightRay, obb_list, other, ref dist) == true)
                    {
                        Other = other;
                    }
                }
            }

            Height = dist;
        }

        // Simple AABB check
        private bool IsCurEntityCollidingWithOtherEntity(CEntityOBB[] OBBWorld, CEntity CurEntity, CEntity Other)
        {
            for (int i = 0; i < OBBWorld.Length; ++i)
            {
                if (OBBWorld[i].m_child.Length > 0)
                {
                    if (IsCurEntityCollidingWithOtherEntity(OBBWorld[i].m_child, CurEntity, Other) == true)
                    {
                        return true;
                    }
                }
                else
                {
                    BoundingBox temp_bb = BoundingBox.CreateFromPoints(OBBWorld[i].m_wrldOBB);
                    CEntityOBB[] cur_entity_bbs = CurEntity.GetWorldOBB();
                    if (IsCurEntityCollidingWithOtherEntity(cur_entity_bbs, temp_bb) == true)
                    {
                        Other.IsHit(CurEntity, 0, Vector3.Zero);
                        CurEntity.HasHit(Other, 0, Vector3.Zero);

                        return true;
                    }
                }
            }

            return false;
        }

        private bool IsCurEntityCollidingWithOtherEntity(CEntityOBB[] OBBWorld, BoundingBox OtherEntityBB)
        {
            for (int i = 0; i < OBBWorld.Length; ++i)
            {
                if (OBBWorld[i].m_child.Length > 0)
                {
                    if (IsCurEntityCollidingWithOtherEntity(OBBWorld[i].m_child, OtherEntityBB) == true)
                    {
                        return true;
                    }
                }
                else
                {
                    BoundingBox cur_entity_bb = BoundingBox.CreateFromPoints(OBBWorld[i].m_wrldOBB);
                    if (cur_entity_bb.Intersects(OtherEntityBB) == true)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        // Only used when the OBB have children
        // This is to check if the new position is still colliding with a particular part of the particular entity.
        private void GetDistToBBs(CEntity CurEntity, ref SCollisionData CollisionData)
        {
            int count = 0;

            foreach (CEntity other in CollisionData.s_otherEntity)
            {
                // First check to make sure that the prv data is still colliding with the other entity.
                if (CollisionData.PrvRay.Intersects(other.GetBB()) != null)
                {
                    float n_dist = CurEntity.GetCollisionDistance;
                    float hit_dist = n_dist;

                    Matrix rot = other.GetWorldMatrix();
                    Matrix other_world = rot;
                    rot.Translation = Vector3.Zero;

                    if (RayVsOOBB(CollisionData.s_prvCloseCollisionDir[count], CollisionData.s_ray.Position,
                        CollisionData.s_otherEntityOBB[count], other_world, rot, ref n_dist) == true)
                    {
                        // This also means it hasn't collided.
                        if (n_dist == 0)
                        {
                            other.IsHit(CurEntity, n_dist, CollisionData.s_prvCloseCollisionDir[count]);
                            CurEntity.HasHit(other, n_dist, CollisionData.s_prvCloseCollisionDir[count]);
                        }
                        else
                        {
                            if (n_dist < hit_dist)
                            {
                                hit_dist = n_dist;
                                other.IsHit(CurEntity, n_dist, CollisionData.s_prvCloseCollisionDir[count]);
                                CurEntity.HasHit(other, n_dist, CollisionData.s_prvCloseCollisionDir[count]);
                            }
                        }
                    }
                    else
                    {
                        // That means the current entity has gone through the other entity.
                        float dist = Vector3.DistanceSquared(CollisionData.s_prvPosition, CollisionData.s_nxtPosition);
                        if ((CollisionData.s_prvCloseCollisionDist[count] - dist) <= 0)
                        {
                            other.IsHit(CurEntity, dist, CollisionData.s_prvCloseCollisionDir[count]);
                            CurEntity.HasHit(other, dist, CollisionData.s_prvCloseCollisionDir[count]);
                        }
                    }
                }

                ++count;
            }
        }

        // Only used when the OBB have children
        private bool GetDistToBBs(Ray HRay, CEntityOBB[] OBBWorld, CEntity CurEntity, CEntity Other, ref SCollisionData CollisionData, ref bool IsStored)
        {
            float n_dist = CurEntity.GetCollisionDistance;
            float hit_dist = n_dist;
            bool valid_hit = false;
            Vector3 hit_normal = Vector3.Zero;

            for (int i = 0; i < OBBWorld.Length; ++i)
            {
                Matrix rot = Other.GetWorldMatrix();
                rot.Translation = Vector3.Zero;
                bool HRay_valid_hit = false;

                // Check against the 5 sides of the BB and use the CurEntity's ray if needed.
                for (int j = 0; j < 6; ++j)
                {
                    Vector3 ray_dir = OBBWorld[i].m_planes[j].Normal;
                    // This helps when testing against the bounding box corners.
                    if (j == 5)
                    {
                        // This test is only needed if the other ray have failed.
                        if (HRay_valid_hit == false && HRay.Direction.Y > -0.99999999f)
                        {
                            ray_dir = HRay.Direction;
                        }
                        else
                        {
                            // Otherwise We don't need to test.
                            break;
                        }
                    }

                    //float dist = n_dist;
                    if (RayVsOOBB(ray_dir, HRay.Position, OBBWorld[i], Other.GetWorldMatrix(), rot, ref n_dist) == true)
                    {
                        if (OBBWorld[i].m_child.Length > 0)
                        {
                            Ray n_ray = new Ray(HRay.Position, ray_dir);
                            if (GetDistToBBsChild(n_ray, OBBWorld[i].m_child, CurEntity, Other, ref CollisionData, ref IsStored) == true)
                            {
                                valid_hit = true;
                                HRay_valid_hit = true;
                            }
                        }
                        else
                        {
                            if (n_dist < hit_dist)
                            {
                                hit_normal = ray_dir;
                                hit_dist = n_dist;
                                valid_hit = true;
                                HRay_valid_hit = true;
                            }
                            else
                            {
                                //if (CollisionData.CheckWithPrvData == true)
                                {
                                    IsStored = true;
                                    CollisionData.StoreNewData(ray_dir, n_dist, Other, OBBWorld[i]);
                                }
                            }
                        }
                    }
                }
            }

            if (valid_hit == true )// && n_dist > 0)
            {
                // Not sure which normal to pass in here.
                Other.IsHit(CurEntity, n_dist, hit_normal);
                CurEntity.HasHit(Other, n_dist, hit_normal);
            }

            return valid_hit;
        }

        // Only used when the OBB have children
        private bool GetDistToBBsChild(Ray HRay, CEntityOBB[] OBBWorld, CEntity CurEntity, CEntity Other, ref SCollisionData CollisionData, ref bool IsStored)
        {
            float n_dist = CurEntity.GetCollisionDistance;
            bool valid_hit = false;
            Vector3 hit_normal = Vector3.Zero;
            float hit_dist = n_dist;

            for (int i = 0; i < OBBWorld.Length; ++i)
            {
                Matrix rot = Other.GetWorldMatrix();
                rot.Translation = Vector3.Zero;

                // The ray position needs to be moved to a place where it is likly to cause a collision,
                // otherwise even though the bbs are colliding the ray might miss the bb since the bb might
                // be above or below the ray.
                BoundingBox other_bb = BoundingBox.CreateFromPoints(OBBWorld[i].m_wrldOBB);
                Vector3 ray_pos = HRay.Position;

                if (RayVsOOBB(HRay.Direction, ray_pos, OBBWorld[i], Other.GetWorldMatrix(), rot, ref n_dist) == true)
                {
                    if (OBBWorld[i].m_child.Length > 0)
                    {
                        valid_hit = GetDistToBBsChild(HRay, OBBWorld[i].m_child, CurEntity, Other, ref CollisionData, ref IsStored);
                    }
                    else
                    {
                        if (hit_dist > n_dist && n_dist != 0)
                        {
                            hit_dist = n_dist;
                            valid_hit = true;
                            hit_normal = HRay.Direction;
                        }
                        else
                        {
                            IsStored = true;
                            CollisionData.StoreNewData(HRay.Direction, n_dist, Other, OBBWorld[i]);
                        }
                    }
                }
            }

            if (valid_hit == true )// && n_dist > 0)
            {
                // Not sure which normal to pass in here.
                Other.IsHit(CurEntity, hit_dist, hit_normal);
                CurEntity.HasHit(Other, hit_dist, hit_normal);
            }

            return valid_hit;
        }

        private void ChangePosition( ref Vector3 NewPosition, BoundingBox EntA, BoundingBox EntB )
        {
            if (EntA.Max.Y < EntB.Min.Y || EntA.Min.Y > EntB.Max.Y)
            {
                return;
            }

            // Now check to see if the position of the ray is too low or too high and adjust accordingly.
            if (NewPosition.Y > EntB.Max.Y)
            {
                float new_height = (EntB.Min.Y + EntB.Max.Y) / 2.0f;
                if (new_height < EntA.Max.Y && new_height > EntA.Min.Y)
                {
                    NewPosition.Y = new_height;
                }
            }
            else if (NewPosition.Y < EntB.Min.Y)
            {
                float new_height = NewPosition.Y = (EntB.Min.Y + EntB.Max.Y) / 2.0f;
                if (new_height < EntA.Max.Y && new_height > EntA.Min.Y)
                {
                    NewPosition.Y = new_height;
                }
            }
        }

        private bool GetDistToBBs(Ray HRay, CEntityOBB[] OBBList, CEntity Other, ref float Dist)
        {
            float n_dist = Dist;
            Vector3 hit_space_pos = Vector3.Zero;
            bool valid_hit = false;

            for (int i = 0; i < OBBList.Length; ++i)
            {
                Matrix rot = Other.GetWorldMatrix();
                rot.Translation = Vector3.Zero;

                if (RayVsOOBB(HRay.Direction, HRay.Position, OBBList[i], Other.GetWorldMatrix(), rot, ref n_dist) == true)
                {
                    if (OBBList[i].m_child.Length > 0)
                    {
                        valid_hit = GetDistToBBs(HRay, OBBList[i].m_child, Other, ref Dist);
                    }
                    else
                    {
                        if (n_dist < Dist && n_dist != 0)
                        {
                            Dist = n_dist;
                            valid_hit = true;
                        }
                    }
                }
            }

            return valid_hit;
        }

        // This needs to be called before the new world matrix is created.
        // It will set the right things if the entity has just become a child of a parent.
        public bool PostCollisionCheck(int UpdateTicket, CEntity Entity)
        {
            if (UpdateTicket < m_updateTicketList.Count && UpdateTicket > -1)
            {
                if (m_updateTicketList[UpdateTicket].s_child == true)
                {
                    Entity.SetAsChild(m_updateTicketList[UpdateTicket].s_parRotMatrix, m_updateTicketList[UpdateTicket].s_parWorldMatrix,
                        m_updateTicketList[UpdateTicket].s_parent);
                    Entity.SetUpdateTicket(-1);
                }
                else
                {
                    Entity.SetAsParent(m_updateTicketList[UpdateTicket].s_parRotMatrix, m_updateTicketList[UpdateTicket].s_parWorldMatrix);
                    Entity.SetUpdateTicket(-1);
                }

                m_updateTicketList.RemoveAt(UpdateTicket);

                return true;
            }

            return false;
        }

        public bool CreateParentChildRelation(string Child, string Parent)
        {
            lock (this)
            {
                CEntity child = m_curScreen.GetEntity(Child);
                CEntity parent = m_curScreen.GetEntity(Parent);

                if (child != null && parent != null)
                {
                    return CreateParentChildRelation(child, parent);
                }

                return false;
            }
        }

        public bool CreateParentChildRelation(CEntity Child, CEntity Parent)
        {
            lock (this)
            {
                if (Child.GetSGID().HasValue == true && Parent.GetSGID().HasValue == true)
                {
                    if (m_sg.CreateChildToParentRelation(Child.GetSGID().Value, Parent.GetSGID().Value) == true)
                    {
                        Matrix par_wrld_mat = Matrix.Identity;
                        Quaternion par_rot = Quaternion.Identity;

                        Parent.GetParentDataForChild(ref par_wrld_mat, ref par_rot);

                        Child.SetAsChild(par_rot, par_wrld_mat, Parent);

                        return true;
                    }
                }

                return false;
            }
        }

        public bool BreakParentChildRelation(string Child, string Parent)
        {
            lock (this)
            {
                CEntity child = m_curScreen.GetEntity(Child);
                CEntity parent = m_curScreen.GetEntity(Parent);

                if (child != null && parent != null)
                {
                    return BreakParentChildRelation(child, parent);
                }

                return false;
            }
        }

        public bool BreakParentChildRelation(CEntity Child, CEntity Parent)
        {
            lock (this)
            {
                if (Child.GetSGID().HasValue == true && Parent.GetSGID().HasValue == true)
                {
                    if (m_sg.BreakChildToParentRelation(Child.GetSGID().Value, Parent.GetSGID().Value) == true)
                    {
                        Matrix par_wrld_mat = Matrix.Identity;
                        Quaternion par_rot = Quaternion.Identity;

                        Parent.GetParentDataForChild(ref par_wrld_mat, ref par_rot);

                        Child.SetAsParent(par_rot, par_wrld_mat);

                        return true;
                    }
                }

                return false;
            }
        }

        // Only check to see if there is a parent child relationship.
        public void CheckForParentChildRelationship(CSpatialTree CurEntityLeafNode, CEntity CurEntity, Ray MovementDir)
        {
            List<int> entity_list = CurEntityLeafNode.CheckRayVsSphereInTree(MovementDir);

            for (int i = 0; i < entity_list.Count; ++i)
            {
                CEntity other = m_curScreen.GetEntity(entity_list[i]);

                if (other.GetSGID() == CurEntity.GetSGID())
                {
                    continue;
                }

                bool is_child = false;
                // Check to see if there is a collision with the main collision box
                if (CurEntity.GetBB().Intersects(other.GetBB()) == true)
                {
                    is_child = CheckForChildParentRelationship(CurEntity, other, MovementDir.Position);
                }

                // One more check needs to be done regarding the parent child relationship
                // This checks to make sure that the child is still a child of the parent.
                if (is_child == false)
                {
                    CheckIfStillChild(CurEntity, MovementDir.Position);
                }
            }
        }

        private bool CheckForChildParentRelationship(CEntity Child, CEntity Parent, Vector3 ChildWorldPosition)
        {
            // ...Check to see if the current entity can be a parent...
            if (true == Parent.CanBeParent && false == Child.ManualRelationship && Child.IsChild() == false
                && false == Parent.IsHidden())
            {
                // If the child entitiy is dynamic...
                if (Child.IsDynamicEntity() == true)
                {
                    // If the child entity's y value is bigger than the parent entity's then that means it is on top
                    // and so it is in the right position to be a child.
                    if (ChildWorldPosition.Y > Parent.GetBB().Min.Y)
                    {
                        //if( CToolbox.IsPointInXZMinMaxOfBoundingBox(Parent.GetBB().Min, Parent.GetBB().Max, ChildWorldPosition) == true )
                        if( IsPointInBoundingBox(Parent, ChildWorldPosition) == true )
                        {
                            Matrix par_wrld_mat = Matrix.Identity;
                            Quaternion par_rot = Quaternion.Identity;

                            Parent.GetParentDataForChild(ref par_wrld_mat, ref par_rot);

                            // ...Now set the child as parent
                            if (m_sg.SetChildToParent((int)Child.GetSGID(), (int)Parent.GetSGID()) == true)
                            {
                                m_updateTicketList.Add(new SUpdateTicketData(par_wrld_mat, par_rot, true, Parent));
                                Child.SetUpdateTicket(m_updateTicketList.Count - 1);
                                
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }

        private bool IsPointInBoundingBox(CEntity Parent, Vector3 Position)
        {
            Matrix inv_world = Matrix.Invert(Parent.GetWorldMatrix());

            Position = Vector3.TransformNormal(Vector3.Transform(Position, inv_world), Parent.GetWorldOBB()[0].m_invRotMatrix);

            Plane[] planes = Parent.GetWorldOBB()[0].m_localPlanesNoRot;

            if ((CToolbox.DotProduct(planes[0].Normal, Position) + planes[0].D) <= 0)
            {
                if ((CToolbox.DotProduct(planes[1].Normal, Position) + planes[1].D) <= 0)
                {
                    if ((CToolbox.DotProduct(planes[2].Normal, Position) + planes[2].D) <= 0)
                    {
                        if ((CToolbox.DotProduct(planes[3].Normal, Position) + planes[3].D) <= 0)
                        {
                            //if ((CToolbox.DotProduct(planes[4].Normal, Position) + planes[4].D) <= 0)
                            {
                                //if ((CToolbox.DotProduct(planes[5].Normal, Position) + planes[5].D) <= 0)
                                {
                                    //System.Diagnostics.Trace.WriteLine("yeah!");
                                    return true;
                                }
                            }
                        }
                    }
                }
            }

            //System.Diagnostics.Trace.WriteLine("no!");
            return false;
        }

        private void CheckIfStillChild(CEntity Child, Vector3 ChildWorldPosition)
        {
            // If the child entitiy is dynamic...
            if (Child.IsDynamicEntity() == true)
            {
                CEntity parent = m_sg.GetParentOfEntity((int)Child.GetSGID());

                if (parent != null)
                {
                    BoundingBox par_bb = parent.GetBB();

                    if (IsAParentOfB(par_bb, Child.GetBB(), ChildWorldPosition, parent) == false || parent.IsHidden() == true)
                    {
                        Matrix par_wrld_mat = Matrix.Identity;
                        Quaternion par_rot = Quaternion.Identity;

                        parent.GetParentDataForChild(ref par_wrld_mat, ref par_rot);

                        m_sg.NoLongerChildOfParent((int)Child.GetSGID(), (int)parent.GetSGID());

                        m_updateTicketList.Add(new SUpdateTicketData(par_wrld_mat, par_rot, false));
                        Child.SetUpdateTicket(m_updateTicketList.Count - 1);
                    }
                }
            }
        }

        // This is the tradional check to see if a child has a parent.
        //public bool IsAParentOfB(BoundingBox BBA, BoundingBox BBB, Vector3 BPos)
        public bool IsAParentOfB(BoundingBox BBA, BoundingBox BBB, Vector3 ChildWorldPosition, CEntity Parent)
        {
            if (ChildWorldPosition.Y >= BBA.Min.Y)
            {
                if (CToolbox.IsPointInXZMinMaxOfBoundingBox(BBA.Min, BBA.Max, ChildWorldPosition) == true)
                {
                    //if (BBA.Intersects(BBB) == true)
                    //if( AABBvsAABB(BBA, BBB) == true )
                    if (IsPointInBoundingBox(Parent, ChildWorldPosition) == true)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private bool CreateTrianglesFromBB(Vector3[] BB, Vector3 RayDir, Vector3 RayOrigin, ref float Dist)
        {
            //CVisual.Instance.AddToLineList(RayOrigin, RayOrigin + (RayDir * 2.0f), Microsoft.Xna.Framework.Graphics.Color.Red); // FOR DEBUG PURPOSES ONLY!

            //int[] index = {
            //    0, 4, 6,
            //    6, 4, 5,

            //    6, 5, 2,
            //    2, 5, 1,

            //    2, 1, 3,
            //    3, 1, 7,

            //    3, 7, 0,
            //    6, 7, 4,

            //    3, 0, 2,
            //    2, 0, 6,

            //    4, 7, 5,
            //    5, 7, 1
            //    };
            int[] index = {
                0, 4, 5,
                5, 1, 0,

                7, 3, 2,
                2, 6, 3,

                3, 7, 4,
                4, 0, 3,

                1, 5, 6,
                6, 2, 1,

                3, 0, 1,
                1, 2, 3,

                4, 7, 6,
                6, 5, 4
                };

            float t = 0;
            float min = 1000.0f;

            for (int i = 0; i < index.Length; i += 3)
            {
                STriData triangle;
                triangle.s_firstPos = BB[index[i]];
                triangle.s_secondPos = BB[index[i + 1]];
                triangle.s_thirdPos = BB[index[i + 2]];

                if (RayVsTriangle(RayDir, RayOrigin, triangle, out t) == true)
                {
                    if (min > t && t >= 0)
                    {
                        min = t;
                    }
                }
            }

            if (min != 1000.0f)
            {
                Dist = min;
                return true;
            }

            return false;
        }

        // ray vs cylinder
        // aabb vs oobb
        // oobb vs oobb
        // sphere vs sphere
        // sphere vs aabb
        // sphere vs oobb
        // cylinder vs cylinder
        // cylinder vs aabb
        // cylinder vs oobb
        // cylinder vs sphere

        public bool AABBvsAABB(BoundingBox EntA, BoundingBox EntB)
        {
            bool collision = true;

            if (EntA.Max.X < EntB.Min.X || EntA.Min.X > EntB.Max.X)
            {
                collision = false;
            }

            if (EntA.Max.Y < EntB.Min.Y || EntA.Min.Y > EntB.Max.Y)
            {
                collision = false;
            }

            if (EntA.Max.Z < EntB.Min.Z || EntA.Min.Z > EntB.Max.Z)
            {
                collision = false;
            }

            return collision;
        }

        public static bool RectVsRect(Rectangle EntA, Rectangle EntB)
        {
            if ((EntA.Right > EntB.Left && EntA.Left < EntB.Right)
                && (EntA.Bottom > EntB.Top && EntA.Top < EntB.Bottom))
            {
                return true;
            }

            else
            {
                return false;
            }
        }


        bool AABBvsAABB(BoundingBox EntA, BoundingBox EntB, ref float Dist)
        {
            Vector3 EntAPos = (EntA.Max + EntA.Min) * 0.5f;
            Vector3 EntBPos = (EntB.Max + EntB.Min) * 0.5f;

            Vector3 dif_in_1 = new Vector3(EntA.Max.X - EntAPos.X, EntA.Max.Y - EntAPos.Y, EntA.Max.Z - EntAPos.Z);
            Vector3 dif_in_2 = new Vector3(EntB.Max.X - EntBPos.X, EntB.Max.Y - EntBPos.Y, EntB.Max.Z - EntBPos.Z);

            float difincenterx = EntAPos.X - EntBPos.X;
            if (difincenterx < 0)
            {
                difincenterx *= -1;
            }
            float difincentery = EntAPos.Y - EntBPos.Y;
            if (difincentery < 0)
            {
                difincentery *= -1;
            }
            float difincenterz = EntAPos.Z - EntBPos.Z;
            if (difincenterz < 0)
            {
                difincenterz *= -1;
            }

            float difx_both = (dif_in_1.X + dif_in_2.X);
            float dify_both = (dif_in_1.Y + dif_in_2.Y);
            float difz_both = (dif_in_1.Z + dif_in_2.Z);

            if (difincenterx > difx_both)
            {
                return false;
            }
            else if (difincentery > dify_both)
            {
                return false;
            }
            else if (difincenterz > difz_both)
            {
                return false;
            }
            else
            {
                Dist = MathHelper.Min(difx_both - difincenterx, MathHelper.Min(dify_both - difincentery, difz_both - difincenterz));
                //System.Diagnostics.Trace.WriteLine(Dist);

                return true;
            }
        }

        private static void RaySphereDiscriminant(Vector3 RayDir, Vector3 RayOrigin, BoundingSphere Sphere, ref Vector4 Discrmnt)
        {
	        Discrmnt.X = CToolbox.DotProduct(RayDir, RayDir);
            Discrmnt.Y = CToolbox.DotProduct((RayDir * 2.0f), (RayOrigin - Sphere.Center));
            Discrmnt.Z = CToolbox.DotProduct((RayOrigin - Sphere.Center), (RayOrigin - Sphere.Center)) - (Sphere.Radius * Sphere.Radius);

	        Discrmnt.W = ( Discrmnt.Y * Discrmnt.Y ) - 4.0f * Discrmnt.X * Discrmnt.Z;
        }

        // NOT TESTED, BUT IT SHOULD WORK.
        // LengthOfInterRay should initally equal 1000.0f.
        public static bool RayVsSphere(Vector3 RayDir, Vector3 RayOrigin, BoundingSphere Sphere, ref float LengthOfInterRay)
        {
            Vector4 discrmnt = Vector4.Zero;
            RaySphereDiscriminant(RayDir, RayOrigin, Sphere, ref discrmnt);
            if (discrmnt.W < 0.0f)
            {
                return false;
            }
            else
            {
                if (discrmnt.W == 0.0f)
                {
                    LengthOfInterRay = ((-1.0f * discrmnt.Y) - (float)Math.Sqrt(discrmnt.W)) / (2.0f * discrmnt.X);
                    return true;
                }
                else
                {
                    float first_Inter = ((-1.0f * discrmnt.Y) - (float)Math.Sqrt(discrmnt.W)) / (2.0f * discrmnt.X);
                    float second_Inter = ((-1.0f * discrmnt.Y) + (float)Math.Sqrt(discrmnt.W)) / (2.0f * discrmnt.X);

                    if (first_Inter > second_Inter)
                    {
                        float temp = first_Inter;
                        first_Inter = second_Inter;
                        second_Inter = temp;
                    }

                    // See if the current t value from the t buffer is smaller, if so then store the new t value in the t buffer
                    if (LengthOfInterRay > first_Inter)
                    {
                        LengthOfInterRay = first_Inter;
                    }
                    else
                    {
                        return false;
                    }

                    if (first_Inter < 0)
                    {
                        LengthOfInterRay = second_Inter;
                        //InterData.m_position2 = RayOrigin + RayDir * first_Inter;
                    }
                    else
                    {
                        LengthOfInterRay = first_Inter;
                        //InterData.m_position2 = RayOrigin + RayDir * second_Inter;
                    }
                }

                return true;
            }
        }

        public static bool RayVsSphere(Vector3 RayDir, Vector3 RayOrigin, BoundingSphere Sphere, ref float LengthOfInterRay1, ref float LengthOfInterRay2)
        {
            Vector4 discrmnt = Vector4.Zero;
            RaySphereDiscriminant(RayDir, RayOrigin, Sphere, ref discrmnt);
            if (discrmnt.W < 0.0f)
            {
                return false;
            }
            else
            {
                if (discrmnt.W == 0.0f)
                {
                    LengthOfInterRay1 = ((-1.0f * discrmnt.Y) - (float)Math.Sqrt(discrmnt.W)) / (2.0f * discrmnt.X);
                    return true;
                }
                else
                {
                    float first_Inter = ((-1.0f * discrmnt.Y) - (float)Math.Sqrt(discrmnt.W)) / (2.0f * discrmnt.X);
                    float second_Inter = ((-1.0f * discrmnt.Y) + (float)Math.Sqrt(discrmnt.W)) / (2.0f * discrmnt.X);

                    if (first_Inter > second_Inter)
                    {
                        float temp = first_Inter;
                        first_Inter = second_Inter;
                        second_Inter = temp;
                    }

                    // See if the current t value from the t buffer is smaller, if so then store the new t value in the t buffer
                    if (LengthOfInterRay1 > first_Inter)
                    {
                        LengthOfInterRay1 = first_Inter;
                        LengthOfInterRay2 = second_Inter;
                    }
                    else
                    {
                        return false;
                    }

                    if (first_Inter < 0)
                    {
                        LengthOfInterRay1 = second_Inter;
                        LengthOfInterRay2 = first_Inter;
                    }
                    else
                    {
                        LengthOfInterRay1 = first_Inter;
                        LengthOfInterRay2 = second_Inter;
                    }
                }

                return true;
            }
        }

        public static bool RayVsSphere(Vector3 RayDir, Vector3 RayOrigin, BoundingSphere Sphere)
        {
            Vector4 discrmnt = Vector4.Zero;
            RaySphereDiscriminant(RayDir, RayOrigin, Sphere, ref discrmnt);
            if (discrmnt.W < 0.0f)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static bool SphereVsAABB(BoundingSphere Sphere, BoundingBox Box)
        {
            return Sphere.Intersects(Box);
        }

        public static bool SphereVsSphere(BoundingSphere Sphere1, BoundingSphere Sphere2)
        {
            return Sphere1.Intersects(Sphere2);
        }

        public bool SphereVsSphere(BoundingSphere Sphere1, int EntityID)
        {
            return Sphere1.Intersects(m_curScreen.GetEntity(EntityID).GetBoundingSphere());
        }

        public static bool RayVsOOBB(Vector3 WrldRayDir, Vector3 WrldRayOrigin, CEntityOBB LocalSpaceBB,
            Matrix EntWorldMatrix, Matrix EntRotMatrix, ref float Dist)
        {
            if (LocalSpaceBB.m_isAABB == true)
            {
                return RayVsAABB(WrldRayDir, WrldRayOrigin, BoundingBox.CreateFromPoints(LocalSpaceBB.m_wrldOBB), ref Dist);
            }

            float? dist = 0;

            Ray new_ray = new Ray();
            // The * -1.0f is not needed since the planes wor in pairs. One pair will have the inverse of each others
            // normals.
            Matrix inv_world = Matrix.Invert(EntWorldMatrix);
            Matrix inv_rot = Matrix.Invert(EntRotMatrix);

            new_ray.Direction = Vector3.TransformNormal(Vector3.TransformNormal(WrldRayDir, inv_rot), LocalSpaceBB.m_invRotMatrix);// *-1.0f;
            new_ray.Position = Vector3.TransformNormal(Vector3.Transform(WrldRayOrigin, inv_world), LocalSpaceBB.m_invRotMatrix);

            dist = new_ray.Intersects(LocalSpaceBB.m_localOBBNoRot); // Normal ray vs AABB
            if (dist != null)
            {
                //if (dist < Dist)
                {
                    //Position = RayOrigin - (RayDir * (1.0f - (float)dist));
                    //System.Diagnostics.Trace.WriteLine(dist);
                    Dist = (float)dist;

                    return true;
                }
            }

            return false;
        }

        public static bool RayVsAABB(Vector3 RayDir, Vector3 RayOrigin, BoundingBox BB)
        {
            // TEMPORARY FIX: Use your own Ray vs Bounding Box algorithm, NOOB!
            Ray temp2;
            temp2.Direction = RayDir;
            temp2.Position = RayOrigin;
            if (BB.Intersects(temp2) != null)
            {
                return true;
            }

            return false;

            #region commented
            //float t_x_min = 0, t_x_max = 0, t_y_min = 0, t_y_max = 0, t_z_min = 0, t_z_max = 0;

            //float a = 1.0f / RayDir.X;
            //if( a >= 0 )
            //{
            //    t_x_min = (BB.Min.X - RayOrigin.X) * a;
            //    t_x_max = (BB.Max.X - RayOrigin.X) * a;
            //}
            //else
            //{
            //    t_x_min = (BB.Max.X - RayOrigin.X) * a;
            //    t_x_max = (BB.Min.X - RayOrigin.X) * a;
            //}

            //a = 1.0f / RayDir.Y;
            //if( a >= 0 )
            //{
            //    t_y_min = (BB.Min.Y - RayOrigin.Y) * a;
            //    t_y_max = (BB.Max.Y - RayOrigin.Y) * a;
            //}
            //else
            //{
            //    t_y_min = (BB.Max.Y - RayOrigin.Y) * a;
            //    t_y_max = (BB.Min.Y - RayOrigin.Y) * a;
            //}

            //if( t_x_min > t_y_max || t_y_min > t_x_max )
            //{
            //    return false;
            //}

            //a = 1.0f / RayDir.Z;
            //if( a >= 0 )
            //{
            //    t_z_min = (BB.Min.Z - RayOrigin.Z) * a;
            //    t_z_max = (BB.Max.Z - RayOrigin.Z) * a;
            //}
            //else
            //{
            //    t_z_min = (BB.Max.Z - RayOrigin.Z) * a;
            //    t_z_max = (BB.Min.Z - RayOrigin.Z) * a;
            //}

            //if( t_x_min > t_z_max || t_z_min > t_x_max )
            //{
            //    return false;
            //}
            //else
            //{
            //    return true;
            //}
            #endregion
        }

        public static bool RayVsAABB(Vector3 RayDir, Vector3 RayOrigin, Vector3 Min, Vector3 Max)
        {
            // TEMPORARY FIX: Use your own Ray vs Bounding Box algorithm, NOOB!
            BoundingBox temp;
            temp.Min = Min;
            temp.Max = Max;

            Ray temp2;
            temp2.Direction = RayDir;
            temp2.Position = RayOrigin;
            if (temp.Intersects(temp2) != null)
            {
                return true;
            }

            return false;

            #region commented
            //float t_x_min = 0, t_x_max = 0, t_y_min = 0, t_y_max = 0, t_z_min = 0, t_z_max = 0;

            //float a = 1.0f / RayDir.X;
            //if (a >= 0)
            //{
            //    t_x_min = (Min.X - RayOrigin.X) * a;
            //    t_x_max = (Max.X - RayOrigin.X) * a;
            //}
            //else
            //{
            //    t_x_min = (Max.X - RayOrigin.X) * a;
            //    t_x_max = (Min.X - RayOrigin.X) * a;
            //}

            //a = 1.0f / RayDir.Y;
            //if (a >= 0)
            //{
            //    t_y_min = (Min.Y - RayOrigin.Y) * a;
            //    t_y_max = (Max.Y - RayOrigin.Y) * a;
            //}
            //else
            //{
            //    t_y_min = (Max.Y - RayOrigin.Y) * a;
            //    t_y_max = (Min.Y - RayOrigin.Y) * a;
            //}

            //if (t_x_min > t_y_max || t_y_min > t_x_max)
            //{
            //    return false;
            //}

            //a = 1.0f / RayDir.Z;
            //if (a >= 0)
            //{
            //    t_z_min = (Min.Z - RayOrigin.Z) * a;
            //    t_z_max = (Max.Z - RayOrigin.Z) * a;
            //}
            //else
            //{
            //    t_z_min = (Max.Z - RayOrigin.Z) * a;
            //    t_z_max = (Min.Z - RayOrigin.Z) * a;
            //}

            //if (t_x_min > t_z_max || t_z_min > t_x_max)
            //{
            //    return false;
            //}
            //else
            //{
            //    return true;
            //}
            #endregion
        }

        public static bool RayVsAABB(Ray ShotRay, Vector3 Min, Vector3 Max, ref float Dist)
        {
            // TEMPORARY FIX: Use your own Ray vs Bounding Box algorithm, NOOB!
            BoundingBox temp;
            temp.Min = Min;
            temp.Max = Max;

            float? t = temp.Intersects(ShotRay);
            if (t != null)
            {
                Dist = t.Value;
                return true;
            }

            return false;
        }

        public static bool RayVsAABB(Vector3 RayDir, Vector3 RayOrigin, BoundingBox BB, ref float Dist)
        {
            // TEMPORARY FIX: Use your own Ray vs Bounding Box algorithm, NOOB!
            Ray temp;
            temp.Direction = RayDir;
            temp.Position = RayOrigin;

            float? t = BB.Intersects(temp);
            if (t != null)
            {
                Dist = t.Value;
                return true;
            }

            return false;
        }

        public static bool RayVsAABB(Ray InterRay, BoundingBox BB, ref float Dist)
        {
            // TEMPORARY FIX: Use your own Ray vs Bounding Box algorithm, NOOB!
            float? t = BB.Intersects(InterRay);
            if (t.HasValue != false)
            {
                Dist = t.Value;
                return true;
            }

            return false;
        }

        public static bool RayVsModel(Vector3 WrldRayDir, Vector3 WrldRayOrigin, Matrix ModelWorldMatrix, Quaternion ModelWorldRotation, CDotXModel Model)
        {
            Ray new_ray = new Ray();

            Matrix inv_world = Matrix.Invert(ModelWorldMatrix);
            Matrix inv_rot = Matrix.Invert(Matrix.CreateFromQuaternion(ModelWorldRotation));

            new_ray.Direction = Vector3.TransformNormal(Vector3.TransformNormal(WrldRayDir, inv_rot), inv_rot);
            new_ray.Position = Vector3.TransformNormal(Vector3.Transform(WrldRayOrigin, inv_world), inv_world);

            Matrix[] transforms = new Matrix[Model.m_model.Bones.Count];
            Model.m_model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in Model.m_model.Meshes)
            {
                int num_index = mesh.IndexBuffer.SizeInBytes;

                switch (mesh.IndexBuffer.IndexElementSize)
                {
                    case IndexElementSize.SixteenBits:
                        {
                            num_index /= 16;
                            break;
                        }
                    case IndexElementSize.ThirtyTwoBits:
                        {
                            num_index /= 32;
                            break;
                        }
                }

                byte[] index_buffer = new byte[num_index];
                mesh.IndexBuffer.GetData(index_buffer);

                int temp = mesh.VertexBuffer.SizeInBytes;

                for (int i = 0; i < num_index; ++i)
                {
                    //mesh.VertexBuffer.
                }
            }

            return false;
        }

        public static bool RayVsTriangle(Vector3 RayDir, Vector3 RayOrigin, STriData Triangle, out float T)
        {
            Vector3 barycentric_coords = new Vector3(0, 0, 0);
            if (WorkOutTForRayTri(RayDir, RayOrigin, Triangle, out T, ref barycentric_coords) == false)
            {
                return false;
            }

            return true;
        }

        public static bool RayVsTriangle(Vector3 RayDir, Vector3 RayOrigin, STriData Triangle, ref Vector3 InterPoint)
        {
            float t = 0;

            Vector3 barycentric_coords = new Vector3(0, 0, 0);
            if (WorkOutTForRayTri(RayDir, RayOrigin, Triangle, out t, ref barycentric_coords) == false)
            {
                return false;
            }

            InterPoint = RayOrigin + RayDir * t;
            return true;
        }

        private static bool WorkOutTForRayTri(Vector3 RayDir, Vector3 RayOrigin, STriData Triangle, out float T, ref Vector3 BarycentricCoords)
        {
            float a = Triangle.s_firstPos.X - Triangle.s_secondPos.X;
            float b = Triangle.s_firstPos.Y - Triangle.s_secondPos.Y;
            float c = Triangle.s_firstPos.Z - Triangle.s_secondPos.Z;
            float d = Triangle.s_firstPos.X - Triangle.s_thirdPos.X;
            float e = Triangle.s_firstPos.Y - Triangle.s_thirdPos.Y;
            float f = Triangle.s_firstPos.Z - Triangle.s_thirdPos.Z;
            float g = RayDir.X;
            float h = RayDir.Y;
            float i = RayDir.Z;

            float j = Triangle.s_firstPos.X - RayOrigin.X;
            float k = Triangle.s_firstPos.Y - RayOrigin.Y;
            float l = Triangle.s_firstPos.Z - RayOrigin.Z;

            float eihf = (e * i) - (h * f);
            float gfdi = (g * f) - (d * i);
            float dheg = (d * h) - (e * g);

            float m = a * eihf + b * gfdi + c * dheg;

            if (m == 0)
            {
                T = 0;
                return false;
            }

            float beta = (j * eihf + k * gfdi + l * dheg) / m;

            if (beta < 0 || beta > 1.0f)
            {
                T = 0;
                return false;
            }

            float akjb = (a * k) - (j * b);
            float jcal = (j * c) - (a * l);
            float blkc = (b * l) - (k * c);

            float gamma = (i * akjb + h * jcal + g * blkc) / m;

            if (gamma < 0 || (gamma + beta) > 1.0f)
            {
                T = 0;
                return false;
            }

            T = -(f * akjb + e * jcal + d * blkc) / m;

            if (T < 0)
            {
                T = 0;
                return false;
            }

            float alpha = 1.0f - beta - gamma;
            BarycentricCoords.X = alpha;
            BarycentricCoords.Y = beta;
            BarycentricCoords.Z = gamma;

            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using Onderzoek.ModelData;

namespace Onderzoek.Physics
{
    public class CPhysics
    {
        public struct SProjectileData
        {
            public Vector3 s_curVel;
            public Vector3 s_nxtPos;
            public Vector3 s_acceleration; // setting Y (gravity) to -98.1f seems to work
        }

        public static void UpdateProjectilePhysics(ref SProjectileData ProjData, float DT)
        {
            ProjData.s_nxtPos += (ProjData.s_curVel * DT);
            ProjData.s_curVel += (DT * ProjData.s_acceleration);
        }

        // To create a bounding box for CParticleEntity.
        public static List<CModelOBB> CreateBoundingBoxFromPhysicsData(Vector3 MaxEmitterVelocity, Vector3 InitialPosition, Vector3 MaxInitialVelocity,
                                                            Vector3 MinInitialVelocity, Vector3 MaxAcceleration, Vector3 MinAcceleration, float MaxLifeTime,
                                                            float MaxSizeOfObject)
        {
            Vector3 max_velocity = MaxEmitterVelocity + MaxInitialVelocity;
            Vector3 min_velocity = MinInitialVelocity + (-MaxEmitterVelocity);

            Vector3 time = Vector3.Zero;
            if (MaxAcceleration.X != 0)
            {
                time.X = max_velocity.X / Math.Abs(MaxAcceleration.X);
            }
            else
            {
                time.X = MaxLifeTime;
            }
            if (MaxAcceleration.Y != 0)
            {
                time.Y = max_velocity.Y / Math.Abs(MaxAcceleration.Y);
            }
            else
            {
                time.Y = MaxLifeTime;
            }
            if (MaxAcceleration.Z != 0)
            {
                time.Z = max_velocity.Z / Math.Abs(MaxAcceleration.Z);
            }
            else
            {
                time.Z = MaxLifeTime;
            }

            Vector3 time_squared = time * time;
            Vector3 max_distance = max_velocity * time + 0.5f * MaxAcceleration * time_squared;

            Vector3 min_distance = min_velocity * MaxLifeTime + 0.5f * MinAcceleration * ( MaxLifeTime * MaxLifeTime );
            if (min_distance.X > InitialPosition.X)
            {
                min_distance.X = InitialPosition.X;
            }
            if (min_distance.Y > InitialPosition.Y)
            {
                min_distance.Y = InitialPosition.Y;
            }
            if (min_distance.Z > InitialPosition.Z)
            {
                min_distance.Z = InitialPosition.Z;
            }

            MaxSizeOfObject *= 1.0f;
            Vector3 max_size_of_object = new Vector3(MaxSizeOfObject, MaxSizeOfObject, MaxSizeOfObject);
            max_distance += max_size_of_object;
            min_distance -= max_size_of_object;

            List<CModelOBB> rtn_val = new List<CModelOBB>();
            CModelOBB temp = new CModelOBB();
            temp.m_encompass = false;
            temp.m_rotation = Vector3.Zero;
            temp.m_name = "BB_Frm_physics";
            temp.m_child = new List<CModelOBB>();
            temp.m_position = min_distance + ( (max_distance - min_distance) / 2.0f);
            temp.m_OBB = new BoundingBox(min_distance, max_distance).GetCorners();
            temp.m_scale = max_distance - min_distance;

            rtn_val.Add(temp);

            return rtn_val;
        }
    }
}

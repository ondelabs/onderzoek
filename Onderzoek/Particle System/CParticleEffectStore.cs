﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Xml;
using Onderzoek.ModelData;

namespace Onderzoek.Particle_System
{
    /// <summary>
    /// This class stores a particle effect. It is not a particle effect itself merely all the data about an effect.
    /// </summary>
    /// 
    public class CParticleEffectStore
    {
        public bool m_worldSpaceEmission;
        
        public List<CParticleSystemStore> m_particleSystems;
        public string m_effectName;

        public CParticleEffectStore()
        {
            m_particleSystems = new List<CParticleSystemStore>();
            m_particleSystems.Add(new CParticleSystemStore());
        }

        public void saveParticleEffect(XmlTextWriter writer)
        {
            writer.WriteStartElement("ParticleEffect");
            writer.WriteElementString("EffectName", m_effectName);

            writer.WriteStartElement("SystemList");
            for (int i = 0; i < m_particleSystems.Count; i++)
            {
                writer.WriteStartElement("System");
                m_particleSystems[i].writeToXML(writer);
                writer.WriteEndElement();

            }

            
            writer.WriteEndElement();
            writer.WriteEndElement();

        }

#if EDITOR
        public void loadParticleEffect(XmlTextReader reader)
#else
        public void loadParticleEffect(XmlTextReader reader, Microsoft.Xna.Framework.Content.ContentManager GameContentManager,
            Dictionary<string, CModel.SModelData> ModelList)
#endif
        {

            reader.ReadToFollowing("ParticleEffect");
            if (reader.EOF)
            {
                CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_error, "CParticleEffectSystem", "This is not a particle effect XML");
                throw new System.Exception("This is not a particle effect XML");
            }

            

            reader.ReadToFollowing("ParticleSystemList");
            m_particleSystems.Clear();

            if (!reader.IsEmptyElement)
            {
                while (true)
                {
                    reader.Read();
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "System" )
                    {
                        m_particleSystems.Add(new CParticleSystemStore());
#if EDITOR
                        m_particleSystems[m_particleSystems.Count - 1].readFromXML(reader);
#else
                        m_particleSystems[m_particleSystems.Count - 1].readFromXML(reader, GameContentManager, ModelList);
#endif
                    }

                    if (reader.NodeType == XmlNodeType.EndElement && reader.Name == "SystemList" || reader.EOF)
                        return;
                }

            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Onderzoek.Visual;
using Onderzoek.Camera;
using Onderzoek.ModelData;

namespace Onderzoek.Particle_System
{
    public class CParticleSystem
    {
        public CParticleSystemStore m_systemStore;

        public CParticleEmitter m_emitter;

        public float m_particlePoolFullness;

        public List<CParticle> m_emptyParticleList;
        public List<CParticle> m_forwardParticleList;
        public List<CParticle> m_reverseParticleList;


        public int m_particleIndex; //the index at which we need to create any new particles, it rolls back at the end.

        

        public CParticleSystem(CParticleSystemStore store)
        {
            m_systemStore = store;
            //m_particleList = new CParticle[100];
            m_emptyParticleList = new List<CParticle>();
            m_forwardParticleList = new List<CParticle>();
            m_reverseParticleList = new List<CParticle>();

            for (int i = 0; i < 100; i++)
            {
                m_emptyParticleList.Add(new CParticle());
            }

            m_emitter = new CParticleEmitter(store.m_emitterStore);
            

        }

        public CParticle getNextParticle()
        {
            if (m_emptyParticleList.Count == 0)
            {
                //fresh out of particles increasing pool size.

                m_emptyParticleList.Add(new CParticle());
            }
            CParticle p = m_emptyParticleList[0];
            m_emptyParticleList.RemoveAt(0);

            return p;

        }


        public void Render(Matrix effectWorldMatrix, bool emitToObjectSpace, Camera.CCamera camera)
        {
            if(m_systemStore.m_displayBackfaces)
                CVisual.Instance.RenderBothFaces();

            if (!m_systemStore.m_emitterStore.m_isParticle3dModel)
            {
                CVisual.Instance.BeginQuadParticleRender();

                // This iS to render with a texture
                CVisual.Instance.SetUpQuadParticleEffect(camera, 1.0f/m_systemStore.m_noOfFrames);
            }
            else
            {
                
                CVisual.Instance.BeginDotXParticleRender(m_systemStore.m_emitterStore.m_3dModelParticle.getBaseModel());

                // This iS to render with a texture
                CVisual.Instance.SetUpDotXParticleEffect(1, camera);
            }



            int textureIndex = m_systemStore.m_emitterStore.m_particleTextureId;

            Blend oldDestBlend = Blend.InverseSourceAlpha;
            

            if (m_systemStore.m_isAdditiveBlending)
            {
                oldDestBlend = CVisual.Instance.GetGraphicsDevice().RenderState.DestinationBlend;
                CVisual.Instance.GetGraphicsDevice().RenderState.DestinationBlend = Blend.One;
            }

            //int noOfParticlesToRender;

            Matrix[] matrixArray = new Matrix[m_forwardParticleList.Count + m_reverseParticleList.Count];

            Vector4[] tintArray = new Vector4[m_forwardParticleList.Count + m_reverseParticleList.Count];
            int[] frameArray = new int[m_forwardParticleList.Count + m_reverseParticleList.Count];


            for (int i = 0; i < m_reverseParticleList.Count; i++)
            {
                if (emitToObjectSpace)
                {
                    matrixArray[i] = m_reverseParticleList[i].m_particleWorldMatrix * effectWorldMatrix;
                }
                else
                {
                    matrixArray[i] = m_reverseParticleList[i].m_particleWorldMatrix;
                }
                tintArray[i] = m_reverseParticleList[i].m_tint;
                frameArray[i] = m_reverseParticleList[i].m_currentFrame;
            }

            for (int i = m_forwardParticleList.Count - 1; i >= 0; i--)
            {
                if (emitToObjectSpace)
                {
                    matrixArray[i + m_reverseParticleList.Count] = m_forwardParticleList[i].m_particleWorldMatrix * effectWorldMatrix;
                }
                else
                {
                    matrixArray[i + m_reverseParticleList.Count] = m_forwardParticleList[i].m_particleWorldMatrix;
                }
                tintArray[i + m_reverseParticleList.Count] = m_forwardParticleList[i].m_tint;
                frameArray[i + m_reverseParticleList.Count] = m_forwardParticleList[i].m_currentFrame;

            }

            m_particlePoolFullness = ((m_forwardParticleList.Count + m_reverseParticleList.Count) / (m_forwardParticleList.Count + m_reverseParticleList.Count + m_emptyParticleList.Count));



            if (!m_systemStore.m_emitterStore.m_isParticle3dModel)
            {
                //CVisual.Instance.RenderQuadParticles(matrixArray, tintArray, frameArray,textureIndex, m_forwardParticleList.Count + m_reverseParticleList.Count);
                if(m_systemStore.m_noOfFrames == 1)
                    CVisual.Instance.RenderQuadParticles(matrixArray, tintArray, textureIndex, m_forwardParticleList.Count + m_reverseParticleList.Count);
                else
                    CVisual.Instance.RenderQuadParticles(matrixArray, tintArray, frameArray, textureIndex, m_forwardParticleList.Count + m_reverseParticleList.Count);
            }
            else
            {
                CVisual.Instance.RenderDotXParticles(matrixArray, tintArray, textureIndex, m_forwardParticleList.Count + m_reverseParticleList.Count);
            }


            if(m_systemStore.m_emitterStore.m_isParticle3dModel)
                CVisual.Instance.EndDotXParticleRender();
            else
                CVisual.Instance.EndQuadParticleRender();



            if (m_systemStore.m_isAdditiveBlending)
            {

                CVisual.Instance.GetGraphicsDevice().RenderState.DestinationBlend = oldDestBlend;
            }

            CVisual.Instance.RenderOneFace();
            //CVisual.Instance.RenderParticles(m_wm, m_col, );
        }





        public void Update(Matrix effectWorldMatrix, bool emitToObjectSpace, Camera.CCamera camera)
        {
            m_emitter.update(this, effectWorldMatrix, emitToObjectSpace, camera);
            List<int> deadParticles = new List<int>();

            for (int i = 0; i < m_forwardParticleList.Count; i++)
            {
                if (m_forwardParticleList[i].Update(m_systemStore, effectWorldMatrix, camera)) // if returns true then thsi particle has died.
                {
                    deadParticles.Add(i);
                }
            }


            for (int i = deadParticles.Count - 1; i >= 0; i--)
            {
                CParticle p = m_forwardParticleList[deadParticles[i]];
                m_forwardParticleList.RemoveAt(deadParticles[i]);
                m_emptyParticleList.Add(p);

            }

            deadParticles.Clear();

            for (int i = 0; i < m_reverseParticleList.Count; i++)
            {
                if (m_reverseParticleList[i].Update(m_systemStore, effectWorldMatrix, camera))
                {
                    deadParticles.Add(i);

                }
            }

            for (int i = deadParticles.Count - 1; i >= 0; i--)
            {
                CParticle p = m_reverseParticleList[deadParticles[i]];
                p.m_particleWorldMatrix = Matrix.Identity;
                p.m_velocity = Vector3.Zero;

                m_reverseParticleList.RemoveAt(deadParticles[i]);
                m_emptyParticleList.Add(p);
            }


        }

        public void Reset()
        {
            m_emitter.Reset();


            for (int i = m_reverseParticleList.Count-1; i >=0 ; i--)
            {
                m_emptyParticleList.Add(m_reverseParticleList[i]);
                m_reverseParticleList[i].m_particleWorldMatrix = Matrix.Identity;
                m_reverseParticleList[i].m_velocity = Vector3.Zero;

                m_reverseParticleList.RemoveAt(i);

                
            }

            for (int i = m_forwardParticleList.Count-1; i >= 0; i--)
            {
                m_emptyParticleList.Add(m_forwardParticleList[i]);
                m_forwardParticleList[i].m_particleWorldMatrix = Matrix.Identity;
                m_forwardParticleList[i].m_velocity = Vector3.Zero;
                m_forwardParticleList.RemoveAt(i);
            }


        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

namespace Onderzoek.Particle_System
{
    public class CParticleEmitter
    {
        CParticleEmitterStore m_emitterStore;
        

        public float m_lifetime;
        public float m_maxLifetime;
        public int m_particleEmissionAreaIndex;
        Random m_randomNoGen;
        

        float m_emissionBacklog; // the fractional particle backlog, when this adds up to 1 we can release anotehr particle.
        float m_emissionRate;
        bool m_isEmissionRateStable;
        float m_startEmissionRate;
        float m_startEmissionRateLifetime;
        float m_endEmissionRate;
        float m_endEmissionRateLifetime;

        int m_maxParticleLifetime;

        bool m_isParticleLifetimeStable;
        float m_startParticleLifetime;
        float m_startParticleLifetime_Lifetime;
        float m_endParticleLifetime;
        float m_endParticleLifetime_Lifetime;



        Vector3 m_direction;
        float m_directionSpread; //how much the particles should spread on emission.
        Vector3 m_particleSpin; // at wat rotation the particle will be emitted


        public CParticleEmitter(CParticleEmitterStore store)
        {
            m_emitterStore = store;
            m_maxParticleLifetime = 20; // default. needs a list
            m_maxLifetime = 100;
            m_randomNoGen = new Random();

        }

        public void update(CParticleSystem particleSystem, Matrix effectWorldMatrix, bool emitToObjectSpace, Camera.CCamera camera)
        {
            if (m_lifetime > m_maxLifetime)
            {
                if (m_emitterStore.m_loopEmitter)
                    Reset();
                else
                    return;
            }

            m_lifetime++;

            float lifetimePercent = m_lifetime/m_maxLifetime;

            if (lifetimePercent > m_endParticleLifetime_Lifetime)
                recheckParticleLifetime(m_emitterStore);

            if (!m_isParticleLifetimeStable)
            {
                float localParticleLifetime_lifetime = (lifetimePercent - m_startParticleLifetime_Lifetime) / (m_startParticleLifetime_Lifetime - m_endParticleLifetime_Lifetime);
                m_maxParticleLifetime = (int)(m_startParticleLifetime * (1 - localParticleLifetime_lifetime) + m_endParticleLifetime_Lifetime * localParticleLifetime_lifetime);
            }

            //check emission rate.
            if (lifetimePercent > m_endEmissionRateLifetime) //we are out of the current segment. recheck weights.
            {
                recheckEmissionRate(m_emitterStore);
            }

            if(!m_isEmissionRateStable)
            {
                float localEmissionRateLifetime = (lifetimePercent - m_startEmissionRateLifetime) / (m_endEmissionRateLifetime - m_startEmissionRateLifetime);
                m_emissionRate = m_startEmissionRate * (1 - localEmissionRateLifetime) + m_endEmissionRate * localEmissionRateLifetime;
            }

            emitParticles((int)m_emissionRate, particleSystem, effectWorldMatrix, emitToObjectSpace, camera);

            m_emissionBacklog += m_emissionRate - (int)m_emissionRate;

            if (m_emissionBacklog >= 1)
            {
                m_emissionBacklog -= 1;
                emitParticles(1, particleSystem, effectWorldMatrix, emitToObjectSpace, camera);
            }

        }


        public void emitParticles(int noOfParticles, CParticleSystem particleSystem, Matrix effectWorldMatrix, bool emitToObjectSpace, Camera.CCamera camera)
        {
            

            for (int i = 0; i < noOfParticles; i++)
            {

                CParticle p = particleSystem.getNextParticle();
                
                initializeParticle(p, particleSystem.m_systemStore, effectWorldMatrix, emitToObjectSpace, camera);

                if (m_emitterStore.m_nextPointOnNextParticle)
                {
                    //if(m_emitterStore.m_usingCustomPoints)
                    if(m_emitterStore.m_orderedEmission)
                        m_particleEmissionAreaIndex = (m_particleEmissionAreaIndex + 1) % m_emitterStore.m_numberOfEmissionPoints;
                    else
                        m_particleEmissionAreaIndex = m_randomNoGen.Next(m_emitterStore.m_numberOfEmissionPoints);

                }

                if (p.m_sortDistanceMultiplier < 0)
                {
                    particleSystem.m_forwardParticleList.Insert(0,p);

                }
                else
                    particleSystem.m_reverseParticleList.Add(p);
            }

            if (!m_emitterStore.m_nextPointOnNextParticle)
            {
                //if(m_emitterStore.m_usingCustomPoints)
                if (m_emitterStore.m_orderedEmission)
                    m_particleEmissionAreaIndex = (m_particleEmissionAreaIndex + 1) % m_emitterStore.m_numberOfEmissionPoints;
                else
                    m_particleEmissionAreaIndex = m_randomNoGen.Next(m_emitterStore.m_numberOfEmissionPoints);
                //else
                //    m_particleEmissionAreaIndex = (m_particleEmissionAreaIndex + 1) % (;
            }

        }

        

        public void initializeParticle(CParticle particle, CParticleSystemStore particleSystem, Matrix effectWorldMatrix, bool emitToObjectSpace, Onderzoek.Camera.CCamera camera)
        {
            particle.m_lifeTime = 0;

            particle.m_noOfFrames = particleSystem.m_noOfFrames;
            particle.m_animationSpeed = particleSystem.m_animationSpeed;

            if (particleSystem.m_randomStartFrame)
                particle.m_currentFrame = m_randomNoGen.Next(particle.m_noOfFrames);
            else
                particle.m_currentFrame = 0;


            if (emitToObjectSpace)
            {
                particle.m_particleWorldMatrix = Matrix.Identity;
                if(m_emitterStore.m_usingCustomPoints)
                    particle.m_particleWorldMatrix.Translation = m_emitterStore.m_emitterOffset + m_emitterStore.m_customPointList[m_particleEmissionAreaIndex];
                else
                {
                    float xVal = - m_emitterStore.m_emitterAreaSize.X/2 + m_emitterStore.m_emitterAreaSize.X * ( (float)(m_particleEmissionAreaIndex % m_emitterStore.m_emitterAreaIncrements.X) / m_emitterStore.m_emitterAreaIncrements.X);
                    float yVal = - m_emitterStore.m_emitterAreaSize.Y/2 + m_emitterStore.m_emitterAreaSize.Y * ( (float)((int)((float)m_particleEmissionAreaIndex/m_emitterStore.m_emitterAreaIncrements.X) % m_emitterStore.m_emitterAreaIncrements.Y) / m_emitterStore.m_emitterAreaIncrements.Y);
                    float zVal = - m_emitterStore.m_emitterAreaSize.Z/2 + m_emitterStore.m_emitterAreaSize.Z * ( (float)((int)((float)(m_particleEmissionAreaIndex/m_emitterStore.m_emitterAreaIncrements.X)/m_emitterStore.m_emitterAreaIncrements.Y) % m_emitterStore.m_emitterAreaIncrements.Z) / m_emitterStore.m_emitterAreaIncrements.Z);
                    particle.m_particleWorldMatrix.Translation = m_emitterStore.m_emitterOffset + new Vector3(xVal, yVal, zVal);
                }

            }
            else
            {
                particle.m_particleWorldMatrix = Matrix.Identity;
                if (m_emitterStore.m_usingCustomPoints)
                    particle.m_particleWorldMatrix.Translation = m_emitterStore.m_emitterOffset + m_emitterStore.m_customPointList[m_particleEmissionAreaIndex];
                else
                {
                    float xVal = -m_emitterStore.m_emitterAreaSize.X / 2 + m_emitterStore.m_emitterAreaSize.X * ((float)(m_particleEmissionAreaIndex % m_emitterStore.m_emitterAreaIncrements.X) / m_emitterStore.m_emitterAreaIncrements.X);
                    float yVal = -m_emitterStore.m_emitterAreaSize.Y / 2 + m_emitterStore.m_emitterAreaSize.Y * ((float)((int)((float)m_particleEmissionAreaIndex / m_emitterStore.m_emitterAreaIncrements.X) % m_emitterStore.m_emitterAreaIncrements.Y) / m_emitterStore.m_emitterAreaIncrements.Y);
                    float zVal = -m_emitterStore.m_emitterAreaSize.Z / 2 + m_emitterStore.m_emitterAreaSize.Z * ((float)((int)((float)(m_particleEmissionAreaIndex / m_emitterStore.m_emitterAreaIncrements.X) / m_emitterStore.m_emitterAreaIncrements.Y) % m_emitterStore.m_emitterAreaIncrements.Z) / m_emitterStore.m_emitterAreaIncrements.Z);
                    particle.m_particleWorldMatrix.Translation = m_emitterStore.m_emitterOffset + new Vector3(xVal, yVal, zVal);
                }
                //particle.m_particleWorldMatrix.Translation = m_emitterStore.m_emitterOffset;
                particle.m_particleWorldMatrix = effectWorldMatrix * particle.m_particleWorldMatrix;

            }


            if (m_emitterStore.m_rotateRandomly)
            {
                Vector3 tran = particle.m_particleWorldMatrix.Translation;
                Vector3 dir = new Vector3((0.5f - (float)m_randomNoGen.NextDouble()) * MathHelper.TwoPi, (0.5f - (float)m_randomNoGen.NextDouble()) * MathHelper.TwoPi, (0.5f - (float)m_randomNoGen.NextDouble()) * MathHelper.TwoPi);
                dir.Normalize();

                particle.m_particleWorldMatrix = CToolbox.ConvertDirIntoRotation(dir);
                particle.m_particleWorldMatrix.Translation = tran;


            }



            //particle.m_velocity = (Vector3.Forward * Matrix.CreateRotationX(m_emitterStore.m_emissionDirection.X))* Matrix.CreateRotationY(;
            particle.m_velocity =Vector3.Transform( Vector3.Transform(Vector3.Forward, Matrix.CreateRotationX(m_emitterStore.m_emissionDirection.X)), Matrix.CreateRotationY(m_emitterStore.m_emissionDirection.Y));

            Matrix m = new Matrix(effectWorldMatrix.M11, effectWorldMatrix.M12, effectWorldMatrix.M13, effectWorldMatrix.M14,
                effectWorldMatrix.M21, effectWorldMatrix.M22, effectWorldMatrix.M23, effectWorldMatrix.M24,
                effectWorldMatrix.M31, effectWorldMatrix.M32, effectWorldMatrix.M33, effectWorldMatrix.M34,
                effectWorldMatrix.M41, effectWorldMatrix.M42, effectWorldMatrix.M43, effectWorldMatrix.M44);
            
            m.Translation = Vector3.Zero;
            particle.m_velocity = Vector3.Transform(particle.m_velocity, m);


            particle.m_velocity.X += (float)(1 - 2 * m_randomNoGen.NextDouble()) * (m_emitterStore.m_emissionSpread ) / 40;
            particle.m_velocity.Y += (float)(1 - 2 * m_randomNoGen.NextDouble()) * (m_emitterStore.m_emissionSpread) / 40;
            particle.m_velocity.Z += (float)(1 - 2 * m_randomNoGen.NextDouble()) * (m_emitterStore.m_emissionSpread) / 40;

            particle.m_velocity *= m_emitterStore.m_emissionMagnitude;

            particle.m_maxLifetime = m_maxParticleLifetime;

            particle.recheckTint(this.m_emitterStore);
            particle.recheckVelocity(particleSystem);
            particle.recheckWeight(m_emitterStore);
            particle.recheckSize(m_emitterStore);

            Vector3 v = new Vector3(particle.m_velocity.X, particle.m_velocity.Y, particle.m_velocity.Z);
            v.Normalize();

            particle.m_sortDistanceMultiplier = (float)(Vector3.Dot(v, camera.GetDir()));
            particle.m_previousSize = 1;
            particle.m_size = 1;




        }

        public void Reset()
        {
            m_lifetime = 0;

            m_maxLifetime = m_emitterStore.m_maxLifetime;
#if ENGINE
            m_emitterStore.m_loopEmitter = m_emitterStore.m_origLoopEmitter;
#endif
            m_maxParticleLifetime = 0;
            m_endEmissionRate = 0;
            m_endEmissionRateLifetime = 0;
            m_endParticleLifetime_Lifetime = 0;
            m_emissionBacklog = 0;

            m_endParticleLifetime_Lifetime = 0;
            m_endEmissionRateLifetime = 0;



        }

        public void recheckParticleLifetime(CParticleEmitterStore emitterStore)
        {
            float lifetimePercent = m_lifetime / m_maxLifetime;

            for (int i = 0; i < emitterStore.m_particleLifetimeKeyList.Count; i++)
            {
                if (m_lifetime <= emitterStore.m_particleLifetimeKeyList[i].m_lifetime)
                {
                    m_startParticleLifetime = emitterStore.m_particleLifetimeKeyList[i - 1].m_value ;
                    m_startParticleLifetime_Lifetime = emitterStore.m_particleLifetimeKeyList[i - 1].m_lifetime / 100;
                    m_endParticleLifetime = emitterStore.m_particleLifetimeKeyList[i].m_value;
                    m_endParticleLifetime_Lifetime = emitterStore.m_particleLifetimeKeyList[i].m_lifetime / 100;

                    if (m_startParticleLifetime == m_endParticleLifetime)
                    {
                        m_isParticleLifetimeStable = true;
                        m_maxParticleLifetime = (int)m_startParticleLifetime;
                    }
                    else
                        m_isParticleLifetimeStable = false;
                    return;

                }
            }
        }


        public void recheckEmissionRate(CParticleEmitterStore emitterStore)
        {
            float lifetimePercent = m_lifetime / m_maxLifetime;

            for (int i = 0; i < emitterStore.m_emissionRateKeyList.Count; i++)
            {
                if (lifetimePercent <= emitterStore.m_emissionRateKeyList[i].m_lifetime/100)
                {
                    m_startEmissionRate = emitterStore.m_emissionRateKeyList[i-1].m_value/20;
                    m_startEmissionRateLifetime = emitterStore.m_emissionRateKeyList[i-1].m_lifetime/100;
                    m_endEmissionRate = emitterStore.m_emissionRateKeyList[i ].m_value/20;
                    m_endEmissionRateLifetime = emitterStore.m_emissionRateKeyList[i].m_lifetime/100;
                    return;

                }
            }
        }

    }

}

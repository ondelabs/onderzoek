﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Onderzoek.Particle_System
{
    public class CParticleEffect
    {
        public bool m_emitToObjectSpace;

        public CParticleEffectStore m_effectStore; // this is teh link to the particle effect that this effect follows.
        public List<CParticleSystem> m_particleSystemList;

        public bool isPaused;


        public CParticleEffect()
        {
            m_particleSystemList = new List<CParticleSystem>();
        }


        public void Render(Matrix worldMatrix, Onderzoek.Camera.CCamera camera)
        {
            for (int i = 0; i < m_particleSystemList.Count; i++)
            {
                m_particleSystemList[i].Render(worldMatrix, m_emitToObjectSpace, camera);
            }
        }

        public void Update(Matrix worldMatrix, Onderzoek.Camera.CCamera camera)
        {
            if (isPaused)
                return;

            for (int i = 0; i < m_particleSystemList.Count; i++)
            {
                m_particleSystemList[i].Update(worldMatrix, m_emitToObjectSpace, camera);

            }
        }

        public void Step(Matrix worldMatrix, Onderzoek.Camera.CCamera camera)
        {
            
            for (int i = 0; i < m_particleSystemList.Count; i++)
            {
                m_particleSystemList[i].Update(worldMatrix, m_emitToObjectSpace, camera);

            }
        }

        public bool HasParticleEffectFinished()
        {
            uint j = 0;
            for (int i = 0; i < m_particleSystemList.Count; i++)
            {
                if (m_particleSystemList[i].m_systemStore.m_emitterStore.m_loopEmitter == false)
                {
                    if (m_particleSystemList[i].m_emitter.m_lifetime > m_particleSystemList[i].m_emitter.m_maxLifetime)
                    {
                        if ((m_particleSystemList[i].m_reverseParticleList.Count + m_particleSystemList[i].m_forwardParticleList.Count) == 0)
                        {
                            ++j;
                        }
                    }
                }
            }

            if (j == m_particleSystemList.Count)
            {
                return true;
            }

            return false;
        }

        public void StopInfiniteLoop()
        {
            for (int i = 0; i < m_particleSystemList.Count; i++)
            {
                m_particleSystemList[i].m_systemStore.m_emitterStore.m_loopEmitter = false;
            }
        }

        public void StopInfiniteLoopAndEmitter()
        {
            for (int i = 0; i < m_particleSystemList.Count; i++)
            {
                m_particleSystemList[i].m_systemStore.m_emitterStore.m_loopEmitter = false;
                m_particleSystemList[i].m_emitter.m_lifetime = m_particleSystemList[i].m_emitter.m_maxLifetime;
            }
        }

        /// <summary>
        /// This function will clear the particle effect and reset the particle system lists to conform to a new effect. Call it after u chagne the effectstore.
        /// </summary>
        public void resetFromStore()
        {
            m_particleSystemList.Clear();
            for (int i = 0; i < m_effectStore.m_particleSystems.Count; i++)
            {
                m_particleSystemList.Add(new CParticleSystem(m_effectStore.m_particleSystems[i]));

            }

        }

        public void Reset()
        {

            for (int i = 0; i < m_particleSystemList.Count; i++)
            {
                m_particleSystemList[i].Reset();
            }
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using Microsoft.Xna.Framework;
using System.Collections;

using Onderzoek.ModelData;

namespace Onderzoek.Particle_System
{
    public class CLifetimeValueKey
    {
        public float m_lifetime;
        public float m_value;

        public CLifetimeValueKey(float lifetime, float val)
        {
            m_lifetime = lifetime;
            m_value = val;
        }

    }

    /// <summary>
    /// a pair of lifetime and v3. this can be used for colour as well as 
    /// </summary>
    public class CLifetimeV3Key
    {
        public float m_lifetime;
        public Vector3 m_direction;

        public CLifetimeV3Key(float lifetime, Vector3 v1)
        {
            m_lifetime = lifetime;
            m_direction = v1;
        }

    }

    public class CLifetimeParticleVelocityKey
    {
        public float m_lifetime;
        public Vector3 m_direction;
        public Vector2 m_controlDirection; //the values for the control. Make sure both are up to date.
        public float m_magnitude, m_randomness;

        public CLifetimeParticleVelocityKey(float lifetime, Vector3 v1, Vector2 controlDir, float magnitude, float randomness)
        {
            m_lifetime = lifetime;
            m_direction = v1;
            m_magnitude = magnitude;
            m_randomness = randomness;
        }

    }


    public class CLifetimeV4Key
    {
        public float m_lifetime;
        public Vector4 m_v4Value;

        public CLifetimeV4Key(float lifetime, Vector4 val)
        {
            m_lifetime = lifetime;
            m_v4Value = val;
        }
    }


    public class CParticleEmitterStore
    {
        
        public Vector3 m_emitterAreaSize;
        public Vector3 m_emitterAreaIncrements;
        public int m_numberOfEmissionPoints;
        //public Vector3 m_emitterEmissionOffset;

        public List<Vector3> m_emissionPointLists;

        public List<CLifetimeValueKey> m_emissionRateKeyList;
        public List<CLifetimeValueKey> m_particleLifetimeKeyList;

        //public List<CLifetimeV3Key> m_emissionDirectionKeyList;
        public Vector3 m_emissionDirection;
        public float m_emissionMagnitude;
        public float m_emissionSpread;
        public Vector3 m_emitterOffset;

        Vector3 m_emitterSize;
        Vector3 m_emitterSizeIncrements; //for a point emitter you would need 1,1,1 here etc.

        public bool m_usingCustomPoints;
        public bool m_orderedEmission;
        public bool m_nextPointOnNextParticle;

        public List<Vector3> m_customPointList;

        public bool m_rotateRandomly;
        
        public List<CLifetimeV3Key> m_emissionRotationKeyList;

        //note that these are dependent on the particles, not the emitters lifetime.
        public List<CLifetimeV4Key> m_particleColourKeyList;
        public List<CLifetimeV3Key> m_particleVelocityKeyList; //the velocities fo teh particle
        
        
        public bool m_loopEmitter;
#if ENGINE
        public bool m_origLoopEmitter;
#endif

        public int m_maxLifetime;

        
        public List<CLifetimeValueKey> m_particleSizeKeyList;
        public List<CLifetimeValueKey> m_emissionWeightKeyList;

        public bool m_isParticle3dModel; //is true if the particle is a 3d model.
        public string m_textureName; // the texture or model name
        public string m_modelName;

        public int m_particleTextureId;
        public CDotXModel m_3dModelParticle;

        public float m_particleFriction;



        public CParticleEmitterStore()
        {
            m_emitterAreaIncrements = Vector3.One;

            m_emissionRateKeyList = new List<CLifetimeValueKey>();
            m_emissionRateKeyList.Add(new CLifetimeValueKey(0, 20));
            m_emissionRateKeyList.Add(new CLifetimeValueKey(100, 20));

            m_particleLifetimeKeyList = new List<CLifetimeValueKey>();
            m_particleLifetimeKeyList.Add(new CLifetimeValueKey(0, 50));
            m_particleLifetimeKeyList.Add(new CLifetimeValueKey(100, 50));

            m_particleSizeKeyList = new List<CLifetimeValueKey>();
            m_particleSizeKeyList.Add(new CLifetimeValueKey(0, 100));
            m_particleSizeKeyList.Add(new CLifetimeValueKey(0, 100));

            m_emissionDirection = Vector3.Forward;
            m_emissionMagnitude = 0;

            m_customPointList = new List<Vector3>();

            //m_emissionDirectionKeyList = new List<CLifetimeV3Key>();
            //m_emissionDirectionKeyList.Add(new CLifetimeV3Key(0, Vector3.Forward));
            //m_emissionDirectionKeyList.Add(new CLifetimeV3Key(100, Vector3.Forward));

            //m_emissionDirectionSpreadKeyList  = new List<CLifetimeValueKey>();
            m_emissionRotationKeyList = new List<CLifetimeV3Key>();
            m_particleColourKeyList = new List<CLifetimeV4Key>();
            m_particleColourKeyList.Add(new CLifetimeV4Key(0, Vector4.One));
            m_particleColourKeyList.Add(new CLifetimeV4Key(100, Vector4.One));


            m_particleVelocityKeyList = new List<CLifetimeV3Key>();
            m_emissionWeightKeyList = new List<CLifetimeValueKey>();
            m_emissionWeightKeyList.Add(new CLifetimeValueKey(0, 0));
            m_emissionWeightKeyList.Add(new CLifetimeValueKey(100, 0));

            
            m_particleSizeKeyList = new List<CLifetimeValueKey>();
            m_particleSizeKeyList.Add(new CLifetimeValueKey(0, 100));
            m_particleSizeKeyList.Add(new CLifetimeValueKey(100, 100));

            m_particleFriction = 0;
            m_loopEmitter = true;
            m_maxLifetime = 20;
            m_textureName = "default";
            m_modelName = "Quad";
        }

        public void setTextureName(string name)
        {
            m_textureName = name;

            //find the texture id from the level textures.
        }

        public string getTextureName()
        {
            return m_textureName;

            //find the texture id from the level textures.
        }

        public void set3dModel(string name, Onderzoek.ModelData.CDotXModel model)
        {
            //if (!m_isParticle3dModel)
            //    throw new System.Exception("trying to set a 3d model when particle is a texture");

            m_modelName = name;
            m_3dModelParticle = model;

        }
        public void setTextureNameID(string name, int id)
        {
                m_textureName = name;
                m_particleTextureId = id;
        }

#if EDITOR
        public void readFromXML(XmlTextReader reader)
#else
        public void readFromXML(XmlTextReader reader, Microsoft.Xna.Framework.Content.ContentManager GameContentManager,
            Dictionary<string, CModel.SModelData> ModelList)
#endif
        {
            reader.ReadToFollowing("ParticleModel");
            reader.Read();
            m_modelName = reader.Value;

            if (m_modelName.Equals("Quad"))
                m_isParticle3dModel = false;
            else
            {
#if ENGINE
                m_3dModelParticle = new CDotXModel();
                m_3dModelParticle = (CDotXModel)m_3dModelParticle.Initialise(GameContentManager, "Model\\" + m_modelName, ModelList);
#endif
                m_isParticle3dModel = true;
            }

            reader.ReadToFollowing("ParticleTexture");
            reader.Read();
            m_textureName = reader.Value;
#if ENGINE
            m_particleTextureId = Onderzoek.Visual.CVisual.Instance.LoadLevelTexture("Texture/Particles/" + m_textureName, GameContentManager);
#endif

            reader.ReadToFollowing("RandomRotation");
            reader.Read();
            m_rotateRandomly = bool.Parse(reader.Value);

            //pick up particle type if u want it.

            //reader.ReadToFollowing("ParticleModelName");
            //reader.Read();
            //m_textureName = reader.Value;

            reader.ReadToFollowing("EmissionDirection");
            reader.Read();
            CToolbox.StringToVector3(reader.Value, ref m_emissionDirection);

            reader.ReadToFollowing("EmissionMagnitude");
            reader.Read();
            m_emissionMagnitude = float.Parse(reader.Value);


            reader.ReadToFollowing("EmissionSpread");
            reader.Read();
            m_emissionSpread = float.Parse(reader.Value);

            reader.ReadToFollowing("EmitterOffset");
            reader.Read();
            CToolbox.StringToVector3(reader.Value, ref m_emitterOffset);

            reader.ReadToFollowing("EmitterSize");
            reader.Read();
            CToolbox.StringToVector3(reader.Value, ref m_emitterSize);

            reader.ReadToFollowing("EmitterSizeIncrements");
            reader.Read();
            CToolbox.StringToVector3(reader.Value, ref m_emitterSizeIncrements);

            reader.ReadToFollowing("UseCustomPoints");

            if (reader.EOF)
                throw new Exception("Custom points segment of particle effect xml missing");
            reader.Read();
            m_usingCustomPoints = bool.Parse(reader.Value);

            reader.ReadToFollowing("CustomPointList");

            if (reader.IsEmptyElement == false)
            {
                while (true)
                {
                    reader.Read();
                    if ((reader.NodeType == XmlNodeType.EndElement && reader.Name.Equals("CustomPointList") || reader.EOF))
                        break;

                    if (reader.NodeType == XmlNodeType.Element && reader.Name.Equals("Point"))
                    {
                        reader.Read();
                        Vector3 v = new Vector3();
                        if (!CToolbox.StringToVector3(reader.Value, ref v))
                            throw new Exception("bad vector3 in points list");
                        m_customPointList.Add(v);

                    }
                }
            }

            if (m_usingCustomPoints)
                m_numberOfEmissionPoints = m_customPointList.Count;
                

            reader.ReadToFollowing("Friction");
            reader.Read();
            m_particleFriction = float.Parse(reader.Value);

            reader.ReadToFollowing("LoopEmitter");
            reader.Read();
            m_loopEmitter = bool.Parse(reader.Value);
#if ENGINE
            m_origLoopEmitter = m_loopEmitter;
#endif


            reader.ReadToFollowing("EmitterLifetime");
            reader.Read();
            m_maxLifetime = int.Parse(reader.Value);



            reader.ReadToFollowing("EmissionRateList");
            readListFromXML(reader, m_emissionRateKeyList);

            //reader.ReadToFollowing("EmissionDirectionList");
            //readListFromXML(reader, m_emissionDirectionKeyList);

            //reader.ReadToFollowing("EmissionSpreadList");
            //readListFromXML(reader, m_emissionDirectionSpreadKeyList);

            reader.ReadToFollowing("EmissionRotationList");
            if(!reader.IsEmptyElement)
            readListFromXML(reader, m_emissionRotationKeyList);

            reader.ReadToFollowing("EmissionColourList");
            readListFromXML(reader, m_particleColourKeyList);


            //reader.ReadToFollowing("EmissionVelocityList");
            //if (!reader.IsEmptyElement)
            //readListFromXML(reader, m_particleVelocityKeyList);

            reader.ReadToFollowing("EmissionSizeList");
            readListFromXML(reader, m_particleSizeKeyList);

            reader.ReadToFollowing("EmissionWeightList");
            readListFromXML(reader, m_emissionWeightKeyList);


            reader.ReadToFollowing("ParticleLifetimeLists");
            readListFromXML(reader, m_particleLifetimeKeyList);

        }



        public void writeToXML(XmlTextWriter writer)
        {

            if(m_isParticle3dModel)
                writer.WriteElementString("ParticleModel", m_modelName);
            else
                writer.WriteElementString("ParticleModel", "Quad");

            writer.WriteElementString("ParticleTexture", m_textureName);

            writer.WriteStartElement("RandomRotation");
            writer.WriteValue(m_rotateRandomly.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("EmissionDirection");
            writer.WriteValue(m_emissionDirection.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("EmissionMagnitude");
            writer.WriteValue(m_emissionMagnitude.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("EmissionSpread");
            writer.WriteValue(m_emissionSpread.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("EmitterOffset");
            //writer.WriteValue(m_emitterOffset.X.ToString() + " " + m_emitterOffset.Y.ToString() + " " + m_emitterOffset.Z.ToString());
            writer.WriteValue(m_emitterOffset.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("EmitterSize");
            writer.WriteValue(m_emitterSize.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("EmitterSizeIncrements");
            writer.WriteValue(m_emitterSizeIncrements.ToString());
            writer.WriteEndElement();

            writer.WriteElementString("UseCustomPoints", m_usingCustomPoints.ToString());

            writer.WriteStartElement("CustomPointList");
            for (int i = 0; i < m_customPointList.Count; i++)
            {
                writer.WriteElementString("Point", m_customPointList[i].ToString());
                
            }
            writer.WriteEndElement();


            writer.WriteStartElement("Friction");
            writer.WriteValue(m_particleFriction.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("LoopEmitter");
            writer.WriteValue(m_loopEmitter.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("EmitterLifetime");
            writer.WriteValue(m_maxLifetime.ToString());
            writer.WriteEndElement();

            


            writer.WriteStartElement("EmissionRateList");

            //for(int i=0; i<m_emissionRateKeyList.Count; i++)
            {
                writeListToXML(writer, m_emissionRateKeyList);
            }
            writer.WriteEndElement();

           

            writer.WriteStartElement("EmissionRotationList");
            //for (int i = 0; i < m_emissionRotationKeyList.Count; i++)
            {
                writeListToXML(writer, m_emissionRotationKeyList);
            }
            writer.WriteEndElement();

            writer.WriteStartElement("EmissionColourList");
            //for (int i = 0; i < m_particleColourKeyList.Count; i++)
            {
                writeListToXML(writer, m_particleColourKeyList);
            }
            writer.WriteEndElement();

            writer.WriteStartElement("EmissionVelocityList");
            //for (int i = 0; i < m_particleVelocityKeyList.Count; i++)
            {
                writeListToXML(writer, m_particleVelocityKeyList);
            }
            writer.WriteEndElement();


            writer.WriteStartElement("EmissionSizeList");
            //for (int i = 0; i < m_particleSizeKeyList.Count; i++)
            {
                writeListToXML(writer, m_particleSizeKeyList);
            }
            writer.WriteEndElement();


            writer.WriteStartElement("EmissionWeightList");
            //for (int i = 0; i < m_emissionWeightKeyList.Count; i++)
            {
                writeListToXML(writer, m_emissionWeightKeyList);
            }
            writer.WriteEndElement();

            writer.WriteStartElement("ParticleLifetimeLists");
            //for (int i = 0; i < m_particleLifetimeKeyList.Count; i++)
            {
                writeListToXML(writer, m_particleLifetimeKeyList);
            }
            writer.WriteEndElement();

        }

        public static void readListFromXML(XmlTextReader reader, List<CLifetimeValueKey> list)
        {
            list.Clear();
            while (!reader.EOF)
            {
                reader.Read();
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name != "Pair")
                {
                    return;
                }
                if (reader.NodeType == XmlNodeType.Element && reader.Name == "Pair")
                {
                    reader.Read();
                    CLifetimeValueKey p = new CLifetimeValueKey(int.Parse(reader.Value.Split(' ')[0]), float.Parse(reader.Value.Split(' ')[1]));
                    list.Add(p);

                }
            }

        }

        public static void readListFromXML(XmlTextReader reader, List<CLifetimeV3Key> list)
        {
            list.Clear();
            while (!reader.EOF)
            {
                reader.Read();
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name != "Pair")
                {
                    return;
                }
                if (reader.NodeType == XmlNodeType.Element && reader.Name == "Pair")
                {
                    reader.Read();
                    CLifetimeV3Key p = new CLifetimeV3Key(int.Parse(reader.Value.Split(' ')[0]), new Vector3(float.Parse(reader.Value.Split(' ')[1]), float.Parse(reader.Value.Split(' ')[2]), float.Parse(reader.Value.Split(' ')[3])));
                    list.Add(p);
                }
            }

        }

        public static void readListFromXML(XmlTextReader reader, List<CLifetimeV4Key> list)
        {
            list.Clear();
            while (!reader.EOF)
            {
                reader.Read();
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name != "Pair")
                {
                    return;
                }
                if (reader.NodeType == XmlNodeType.Element && reader.Name == "Pair")
                {
                    reader.Read();
                    CLifetimeV4Key p = new CLifetimeV4Key(int.Parse(reader.Value.Split(' ')[0]), new Vector4(float.Parse(reader.Value.Split(' ')[1]), float.Parse(reader.Value.Split(' ')[2]), float.Parse(reader.Value.Split(' ')[3]), float.Parse(reader.Value.Split(' ')[4])));
                    list.Add(p);
                }
            }

        }



        public static void writeListToXML(XmlTextWriter writer, List<CLifetimeValueKey> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                writer.WriteStartElement("Pair");

                writer.WriteValue(list[i].m_lifetime + " " + list[i].m_value);
                writer.WriteEndElement();

            }

        }

        public static void writeListToXML(XmlTextWriter writer, List<CLifetimeV3Key> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                writer.WriteStartElement("Pair");

                writer.WriteValue(list[i].m_lifetime + " " + list[i].m_direction.X + " " + list[i].m_direction.Y + " " + list[i].m_direction.Z );

                writer.WriteEndElement();

            }

        }

        public static void writeListToXML(XmlTextWriter writer, List<CLifetimeV4Key> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                writer.WriteStartElement("Pair");

                writer.WriteValue(list[i].m_lifetime + " " + list[i].m_v4Value.X + " " + list[i].m_v4Value.Y + " " + list[i].m_v4Value.Z + " " + list[i].m_v4Value.W);

                writer.WriteEndElement();

            }

        }



    }
}

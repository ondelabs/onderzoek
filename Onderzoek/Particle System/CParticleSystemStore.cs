﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using Microsoft.Xna.Framework;

using Onderzoek.ModelData;

namespace Onderzoek.Particle_System
{
    /// <summary>
    /// stores data about a particle system. is not a system in itself.
    /// </summary>
    public class CParticleSystemStore
    {
        //force list, blocker list.

        public CParticleEmitterStore m_emitterStore; //the emitter that is in this system.

        public string m_systemName;
        public bool m_emitToWorldSpace; //emit these particles to world space
        public bool m_isBillboardBehaviour;
        public bool m_rotateToVelocity;

        public int m_noOfFrames;
        public bool m_randomStartFrame;
        public int m_animationSpeed;

        public bool m_displayBackfaces;

        public List<CLifetimeParticleVelocityKey> m_particleVelocityKeyList;
        public bool m_particleApplyVelocityDirectly; //indicates if m_particleVelocity should be used as velocity or force.
        



        /*
         * 
         * m_graphics.RenderState.AlphaBlendOperation = BlendFunction.Add; (there are a few choices)

            m_graphics.RenderState.SourceBlend = Blend.SourceAlpha; there are serveral other choices)
            m_graphics.RenderState.DestinationBlend = Blend.InverseSourceAlpha; (there are serveral other choices)

            Once the rendering of all the aprticle effects for a particular blend states is over, you have to set it back to what it was (which is the top three). Normally for particle effects, the destBlend is 1, so all you would need to do is change:

            m_graphics.RenderState.DestinationBlend = Blend.InverseSourceAlpha to m_graphics.RenderState.DestinationBlend = Blend.One and then change it back again to m_graphics.RenderState.DestinationBlend = Blend.InverseSourceAlpha once the rendering is done.
         * 
         * */


        public bool m_isAdditiveBlending;

        public CParticleSystemStore()
        {
            m_systemName = "sys";
            m_emitterStore = new CParticleEmitterStore();
            m_noOfFrames = 1;
            m_particleVelocityKeyList = new List<CLifetimeParticleVelocityKey>();

            m_particleVelocityKeyList.Add(new CLifetimeParticleVelocityKey(0, Vector3.Zero, Vector2.Zero, 0, 0));
            m_particleVelocityKeyList.Add(new CLifetimeParticleVelocityKey(100, Vector3.Zero, Vector2.Zero, 0, 0));

        }

        public static void writeListToXML(XmlTextWriter writer, List<CLifetimeParticleVelocityKey> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                writer.WriteStartElement("Pair");

                writer.WriteValue(list[i].m_lifetime + " "
                    + list[i].m_magnitude + " "
                    + list[i].m_direction.X + " " 
                    + list[i].m_direction.Y + " "
                    + list[i].m_direction.Z + " "
                    + list[i].m_controlDirection.X + " "
                    + list[i].m_controlDirection.Y + " "
                    + list[i].m_randomness + " "
                    );

                writer.WriteEndElement();

            }

        }

        public static void readListFromXML(XmlTextReader reader, List<CLifetimeParticleVelocityKey> list)
        {
            list.Clear();
            while (!reader.EOF)
            {
                reader.Read();
                if (reader.NodeType == XmlNodeType.EndElement && reader.Name != "Pair")
                {
                    return;
                }
                if (reader.NodeType == XmlNodeType.Element && reader.Name == "Pair")
                {
                    reader.Read();
                    CLifetimeParticleVelocityKey p = new CLifetimeParticleVelocityKey(int.Parse(reader.Value.Split(' ')[0]), 
                        new Vector3(float.Parse(reader.Value.Split(' ')[2]), float.Parse(reader.Value.Split(' ')[3]), float.Parse(reader.Value.Split(' ')[4])),
                        new Vector2(float.Parse(reader.Value.Split(' ')[5]), float.Parse(reader.Value.Split(' ')[6])),
                        float.Parse(reader.Value.Split(' ')[1]),
                        float.Parse(reader.Value.Split(' ')[7]
                        ));
                    list.Add(p);
                }
            }

        }


        public void writeToXML(XmlTextWriter writer)
        {
            writer.WriteStartElement("System");

            writer.WriteElementString("SystemName", m_systemName);
            writer.WriteElementString("IsAdditive", m_isAdditiveBlending.ToString());
            writer.WriteElementString("IsBillboarded", m_isBillboardBehaviour.ToString());
            writer.WriteElementString("RotateToVelocity", m_rotateToVelocity.ToString());
            writer.WriteStartElement("ApplyVelocityDirectly");
            writer.WriteValue(m_particleApplyVelocityDirectly.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("EmissionVelocityList");
            writeListToXML(writer, m_particleVelocityKeyList);
            writer.WriteEndElement();

            writer.WriteStartElement("EmitterData");

            m_emitterStore.writeToXML(writer);
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

#if EDITOR
        public void readFromXML(XmlTextReader reader)
#else
        public void readFromXML(XmlTextReader reader, Microsoft.Xna.Framework.Content.ContentManager GameContentManager,
            Dictionary<string, CModel.SModelData> ModelList)
#endif
        {
            reader.ReadToFollowing("SystemName");
            reader.Read();
            m_systemName = reader.Value;

            reader.ReadToFollowing("IsAdditive");
            reader.Read();
            m_isAdditiveBlending = bool.Parse(reader.Value);

            reader.ReadToFollowing("IsBillboarded");
            reader.Read();
            m_isBillboardBehaviour = bool.Parse(reader.Value);

            reader.ReadToFollowing("RotateToVelocity");
            reader.Read();
            m_rotateToVelocity = bool.Parse(reader.Value);


            reader.ReadToFollowing("ApplyVelocityDirectly");
            reader.Read();
            m_particleApplyVelocityDirectly = bool.Parse(reader.Value);

            reader.ReadToFollowing("EmissionVelocityList");
            readListFromXML(reader, m_particleVelocityKeyList);


            
            reader.ReadToFollowing("EmitterData");
            //reader.Read();
#if EDITOR
            m_emitterStore.readFromXML(reader);
#else
            m_emitterStore.readFromXML(reader, GameContentManager, ModelList);
#endif




        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Onderzoek
{
    public struct VertexPosition
    {
        private Vector3 m_position;

        public Vector3 GetPosition()
        {
            return m_position;
        }

        public VertexPosition(Vector3 Position)
        {
            this.m_position = Position;
        }

        public static VertexElement[] VertexElements =
        {
            new VertexElement(0, 0, VertexElementFormat.Vector3, VertexElementMethod.Default, VertexElementUsage.Position, 0),
        };

        public static int SizeInBytes = (sizeof(float) * 3);
    }
}

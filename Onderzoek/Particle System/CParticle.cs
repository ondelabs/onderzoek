﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;


namespace Onderzoek.Particle_System
{
    /// <summary>
    /// This is an actual particle, representing a particle in the game.
    /// </summary>
    public class CParticle
    {
        public float m_lifeTime;

        public float m_sortDistanceMultiplier;

        public float m_maxLifetime;

        public float m_nextLifetimeCheck; //Stores when the next complete check should be made for this particle. 

        public int m_currentFrame;
        public int m_noOfFrames;
        public int m_animationStep;
        public int m_animationSpeed;
        

        public float m_friction;

        public Matrix m_particleWorldMatrix; // this matrix is local if the effect is local and global if the effect is global.

        public float m_size;
        public float m_previousSize;
        public bool m_isSizeStable;
        public bool m_isVelocityStable;

        float m_startSize;
        float m_startSizeLifetime;
        float m_endSize;
        float m_endSizeLifetime;

        public float m_weight;
        bool m_isWeightStable; // tells us if the weight is stable for this interval only. a stable weight means start and end values are the same.
        float m_startWeight;
        float m_endWeight;
        float m_startWeightLifetime;
        float m_endWeightLifetime;


        bool m_isTintStable; //indicates if its necessary to interpolate between the two tints. if this is not set we will not try to interpolate between tints.
        bool m_alphaChangeOnly;// not used yet. //if tint change required is set then this value is ignored. if tint change is not set and this is set tehn only the alpha value is interpolated.

        public Vector4 m_tint;
        Vector4 m_startTint;
        float m_startTintLifetime;
        Vector4 m_endTint;
        float m_endTintLifetime;

        public Vector3 m_currentParticleDirection;
        public float m_currentRandomness;
        Vector3 m_startParticleDirection;
        public float m_startRandomness;
        float m_startParticleDirectionLifetime;
        Vector3 m_endParticleDirection;
        public float m_endRandomness;
        float m_endParticleDirectionLifetime;
        public bool m_isDirectionStable;

        public Vector3 m_velocity;


        public CParticle()
        {
            m_tint = Vector4.One;
            

            m_particleWorldMatrix = Matrix.Identity;
            m_maxLifetime = 100;
            m_lifeTime = int.MaxValue;
            m_previousSize = 1;

        }

        //float angle = 0;

        /// <summary>
        /// updates the particle and returns true if the particle has died.
        /// </summary>
        /// <param name="systemStore"></param>
        /// <param name="effectWorldMatrix"></param>
        /// <returns></returns>
        public bool Update(CParticleSystemStore systemStore, Matrix effectWorldMatrix, Onderzoek.Camera.CCamera camera)
        {
            m_previousSize = m_size;

            m_lifeTime++;

            if (m_lifeTime > m_maxLifetime)
                return true;

            if (m_animationSpeed != 0)
            {
                m_animationStep++;

                if (m_animationStep >= m_animationSpeed)
                {

                    m_animationStep = 0;
                    m_currentFrame = (m_currentFrame + 1) % m_noOfFrames;
                }
            }

             

            float lifetimePercent = m_lifeTime / m_maxLifetime;
            //weight check.
            if (lifetimePercent > m_endWeightLifetime) //we are out of the current segment. recheck weights.
            {
                recheckWeight(systemStore.m_emitterStore);
            }
            if (m_isWeightStable)
            {
                m_weight = m_startWeight;
            }
            else
            {
                float localWeightLifetime = (lifetimePercent - m_startWeightLifetime) / (m_endWeightLifetime - m_startWeightLifetime);
                m_weight = m_startWeight * (1 - localWeightLifetime) + m_endWeight * localWeightLifetime;
            }


            if (lifetimePercent > m_endSizeLifetime) //we are out of the current segment. recheck weights.
            {
                recheckSize(systemStore.m_emitterStore);
            }

            if (m_isSizeStable)
            {
                m_size = m_startSize;

            }
            else
            {
                float localSizeLifetime = (lifetimePercent - m_startSizeLifetime) / (m_endSizeLifetime - m_startSizeLifetime);
                
                m_size = m_startSize * (1 - localSizeLifetime) + m_endSize * localSizeLifetime;
            }


            if (lifetimePercent > m_endParticleDirectionLifetime)
            {
                recheckVelocity(systemStore);
            }

            float localVelocityLifetime = (lifetimePercent - m_startParticleDirectionLifetime) / (m_endParticleDirectionLifetime - m_startParticleDirectionLifetime);

            if (m_isVelocityStable)
            {
                m_currentParticleDirection = m_startParticleDirection;
            }
            else
            {
                m_currentParticleDirection.X = m_startParticleDirection.X * (1 - localVelocityLifetime) + m_endParticleDirection.X * localVelocityLifetime;
                m_currentParticleDirection.Y = m_startParticleDirection.Y * (1 - localVelocityLifetime) + m_endParticleDirection.Y * localVelocityLifetime;
                m_currentParticleDirection.Z = m_startParticleDirection.Z * (1 - localVelocityLifetime) + m_endParticleDirection.Z * localVelocityLifetime;


            }

            
            m_currentRandomness = m_startRandomness * (1 - localVelocityLifetime) + m_endRandomness * localVelocityLifetime;
            
            //colour check
            if (lifetimePercent > m_endTintLifetime)
            {
                recheckTint(systemStore.m_emitterStore);
            }

            if (m_isTintStable)
            {
                
                m_tint = m_startTint;

            }
            else
            {
                float localTintLifetime = (lifetimePercent - m_startTintLifetime) / (m_endTintLifetime - m_startTintLifetime);
                m_tint = m_startTint * (1 - localTintLifetime) + m_endTint * localTintLifetime;
            }

            if(systemStore.m_particleApplyVelocityDirectly)
                m_velocity = m_currentParticleDirection;
            else
                m_velocity += m_currentParticleDirection/10;

            m_velocity.Y -= m_weight;

            

            if (systemStore.m_isBillboardBehaviour)
            {
                Matrix inv_view = camera.GetInvView();
                m_particleWorldMatrix.M11 = inv_view.M11;
                m_particleWorldMatrix.M12 = inv_view.M12;
                m_particleWorldMatrix.M13 = inv_view.M13;
                m_particleWorldMatrix.M21 = inv_view.M21;
                m_particleWorldMatrix.M22 = inv_view.M22;
                m_particleWorldMatrix.M23 = inv_view.M23;
                m_particleWorldMatrix.M31 = inv_view.M31;
                m_particleWorldMatrix.M32 = inv_view.M32;
                m_particleWorldMatrix.M33 = inv_view.M33;
            }


            if (systemStore.m_rotateToVelocity)
            {
                Vector3 tran = m_particleWorldMatrix.Translation;
                Vector3 dir = new Vector3(m_velocity.X, m_velocity.Y, m_velocity.Z);
                dir.Normalize();

                m_particleWorldMatrix = CToolbox.ConvertDirIntoRotation(dir);
                m_particleWorldMatrix.Translation = tran;
                
                //m_particleWorldMatrix =Matrix.(m_velocity.X, m_velocity.Y, m_velocity.Z);
                //m_particleWorldMatrix.Translation = tran;
            }

            


            m_velocity -= systemStore.m_emitterStore.m_particleFriction * m_velocity;



            // Uncomment this, the angle memeber variable above this function and "* rot" below.
            //ROTATION TEST///////////////////////////////////////////////////////////////////////////
            //Matrix rot = Matrix.CreateFromAxisAngle(Vector3.UnitZ, angle+=0.1f);
            //ROTATION TEST///////////////////////////////////////////////////////////////////////////

            m_particleWorldMatrix *= Matrix.CreateTranslation(m_velocity / 100);

            if(systemStore.m_rotateToVelocity || systemStore.m_isBillboardBehaviour)
                m_particleWorldMatrix = Matrix.CreateScale(m_size ) * m_particleWorldMatrix;// * rot;
            else
                m_particleWorldMatrix = Matrix.CreateScale(m_size/m_previousSize ) * m_particleWorldMatrix;
            m_previousSize = m_size;

            

            //m_wm[i] = Matrix.CreateTranslation(m_particleArray[i].s_projData.s_nxtPos);
            



            //m_particleWorldMatrix *= Matrix.CreateTranslation(new Vector3(0, 
            return false;
        }


        public void recheckSize(CParticleEmitterStore emitterStore)
        {
            float lifetimePercent = m_lifeTime / m_maxLifetime;

            for (int i = 0; i < emitterStore.m_particleSizeKeyList.Count; i++)
            {
                if (lifetimePercent >= emitterStore.m_particleSizeKeyList[i].m_lifetime)
                {
                    m_startSize = emitterStore.m_particleSizeKeyList[i].m_value / 100;
                    m_startSizeLifetime = emitterStore.m_particleSizeKeyList[i].m_lifetime / 100;
                    m_endSize = emitterStore.m_particleSizeKeyList[i + 1].m_value / 100;
                    m_endSizeLifetime = emitterStore.m_particleSizeKeyList[i + 1].m_lifetime / 100;

                    if (m_startSize == m_endSize)
                        m_isSizeStable = true;
                    else
                        m_isSizeStable = false;

                    return;

                }
            }
        }




        public void recheckWeight(CParticleEmitterStore emitterStore)
        {
            float lifetimePercent = m_lifeTime/m_maxLifetime;

            for (int i = 0; i < emitterStore.m_emissionWeightKeyList.Count; i++)
            {
                if (lifetimePercent >= emitterStore.m_emissionWeightKeyList[i].m_lifetime)
                {
                    m_startWeight = emitterStore.m_emissionWeightKeyList[i].m_value/50;
                    m_startWeightLifetime = emitterStore.m_emissionWeightKeyList[i].m_lifetime / 100;
                    m_endWeight = emitterStore.m_emissionWeightKeyList[i + 1].m_value/50;
                    m_endWeightLifetime = emitterStore.m_emissionWeightKeyList[i + 1].m_lifetime / 100;

                    if (m_startWeight == m_endWeight)
                        m_isWeightStable = true;
                    else
                        m_isWeightStable = false;

                    return;

                }
            }
        }


        public void recheckTint(CParticleEmitterStore emitterStore)
        {
            float lifetimePercent = m_lifeTime / m_maxLifetime;

            for (int i = 1; i < emitterStore.m_particleColourKeyList.Count; i++)
            {
                if (lifetimePercent <= emitterStore.m_particleColourKeyList[i].m_lifetime / 100)
                {
                    m_startTint = emitterStore.m_particleColourKeyList[i-1].m_v4Value;
                    m_startTintLifetime = emitterStore.m_particleColourKeyList[i-1].m_lifetime / 100 ;
                    m_endTint = emitterStore.m_particleColourKeyList[i ].m_v4Value;
                    m_endTintLifetime = emitterStore.m_particleColourKeyList[i ].m_lifetime / 100 ;

                    if (m_startTint == m_endTint)
                        m_isTintStable = true;
                    else
                        m_isTintStable = false;

                    return;

                }
            }
        }

        public void recheckVelocity(CParticleSystemStore systemStore)
        {
            float lifetimePercent = m_lifeTime / m_maxLifetime;

            for (int i = 1; i < systemStore.m_particleVelocityKeyList.Count; i++)
            {
                if (lifetimePercent <= systemStore.m_particleVelocityKeyList[i].m_lifetime / 100)
                {
                    m_startParticleDirection = systemStore.m_particleVelocityKeyList[i - 1].m_direction;
                    m_startParticleDirection = Vector3.Transform(Vector3.Transform(m_startParticleDirection, Matrix.CreateRotationX(MathHelper.TwoPi * systemStore.m_particleVelocityKeyList[i - 1].m_randomness)),Matrix.CreateRotationY(MathHelper.TwoPi * systemStore.m_particleVelocityKeyList[i - 1].m_randomness));


                    m_startRandomness = systemStore.m_particleVelocityKeyList[i - 1].m_randomness;
                    //m_startVelocityMagnitude = systemStore.m_particleVelocityKeyList[i - 1].m_magnitude;
                    m_startParticleDirectionLifetime = systemStore.m_particleVelocityKeyList[i - 1].m_lifetime / 100;
                    m_endParticleDirection = systemStore.m_particleVelocityKeyList[i].m_direction;
                    m_endParticleDirection = Vector3.Transform(Vector3.Transform(m_endParticleDirection, Matrix.CreateRotationX(MathHelper.TwoPi * systemStore.m_particleVelocityKeyList[i].m_randomness)), Matrix.CreateRotationY(MathHelper.TwoPi * systemStore.m_particleVelocityKeyList[i].m_randomness));

                    m_endRandomness = systemStore.m_particleVelocityKeyList[i].m_randomness;
                    //m_endVelocityMagnitude = systemStore.m_particleVelocityKeyList[i].m_magnitude;
                    m_endParticleDirectionLifetime = systemStore.m_particleVelocityKeyList[i].m_lifetime / 100;



                    if (m_startParticleDirection == m_endParticleDirection)
                        m_isDirectionStable = true;
                    else
                        m_isDirectionStable = false;

                    return;

                }
            }
        }




    }
}

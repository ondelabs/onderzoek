using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
#if XBOX360
using Microsoft.Xna.Framework.GamerServices;
#endif
using Microsoft.Xna.Framework.Net;

using Onderzoek;
using Onderzoek.Visual;
using Onderzoek.Input;
using Onderzoek.GUI;
using Onderzoek.Screens;

#if EDITOR
#else
using Example.Screens;
#endif



namespace Example
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Onderzoek.Game1
    {
        public Game1(bool ByPassVisualCreation): base(ByPassVisualCreation)
        {
        }

        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            base.LoadContent();

            m_cueManager.ScreenHasChanged(Onderzoek.Audio.CCueManager.ECurScreen.Game);

            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "Game1", "Set cue manager to Game.");

#if EDITOR
#else
            AddScreen(new CMainGameScreen(this.Services, this));
#endif

            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "Game1", "Added new screen");

            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "Example.Game1", "Game1 content loaded.");
        }

        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
    }
}

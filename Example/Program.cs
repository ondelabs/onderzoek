using System;

namespace Example
{
    static class Program
    {
#if WINDOWS
        [STAThread]
#endif
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (Game1 game = new Game1(false))
            {
                game.Run();
            }
        }
    }
}


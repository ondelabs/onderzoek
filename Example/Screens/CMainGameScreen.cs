﻿#if ENGINE
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Onderzoek;
using Onderzoek.ModelData;
using Onderzoek.Entity;
using Onderzoek.SpatialTree;
using Onderzoek.Visual;
using Onderzoek.Input;
using Onderzoek.Physics;
using Onderzoek.GUI;
using Onderzoek.Camera;
using Onderzoek.Audio;
using Onderzoek.Event;
using XMLManip;

using Example.Entity;

namespace Example.Screens
{
    public class CMainGameScreen : Onderzoek.Screens.CGameScreenHelper
    {
        CFPCamera m_camera;
        CEntity m_player;
        CEntity m_forwardRenderTest;
        CTESTParticleEntity m_testParticle;
        CTerrainEntity.SHeightMapData m_terrainHMData;

        public CMainGameScreen(GameServiceContainer Services, Game1 CurScreenManager)
        {
            base.Initialise(Services);

            ScreenManager = CurScreenManager;

            Initialise();
        }

        ~CMainGameScreen()
        {
            m_camera = null;

            this.DeInitialise();
        }

        public override void DeInitialise()
        {
            if (m_player != null)
            {
                m_player.Dispose();
            }
            if (m_forwardRenderTest != null)
            {
                m_forwardRenderTest.Dispose();
            }
            if (m_testParticle != null)
            {
                m_testParticle.Dispose();
            }

            base.DeInitialise();
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        public override void Initialise()
        {
            base.Initialise();

            m_camera = new CFPCamera(CVisual.Instance.GetAspectRatio(), 90.0f, 1.0f, 1000.0f, new Vector3(22.0f, 17.0f, -22.0f), Vector3.Zero, ScreenManager.m_inputData);

            m_terrainHMData = new CTerrainEntity.SHeightMapData();

            LoadContent();

            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CMainGameSceen", "CMainGameScreen has been initialised.");
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        public override bool LoadContent()
        {
            // Initialise and load the skybox data
            m_skyBox = new CSkyBox("fx/SkyBox", "Texture/SkyBoxDark", m_content);

            // Load and create the terrain data
            Texture2D heightmap_tex = m_content.Load<Texture2D>("Texture/heightmap");

            Vector2 split = new Vector2(10.0f, 10.0f);
            Vector2 length = new Vector2(256.0f, 256.0f);
            Vector2 num_vert = new Vector2(256.0f, 256.0f);

            CTerrainModel temp_terrain = new CTerrainModel();
            temp_terrain.LoadAndCreateTerrain(heightmap_tex, CVisual.Instance.GetGraphicsDevice(), length, num_vert, 20, split, m_modelList, "Terrain0");

            // Create a terrain entity - Make a get material for terrain models too!
            CTerrainEntity temp_terrain_ent = new CTerrainEntity(new Vector3(0, -5.0f, 0), Vector3.Zero, "Terrain1", temp_terrain,
                temp_terrain.GetMaterials(m_content, "NA"));
            m_entityList.Add(temp_terrain_ent);
            m_terrainHMData = new CTerrainEntity.SHeightMapData();
            m_terrainHMData.Initialise();


            // Load and create the second terrain data
            Texture2D heightmap_tex2 = m_content.Load<Texture2D>("Texture/heightmap2");

            split = new Vector2(10.0f, 10.0f);
            length = new Vector2(256.0f, 256.0f);
            num_vert = new Vector2(256.0f, 256.0f);

            CTerrainModel temp_terrain2 = new CTerrainModel();
            temp_terrain2.LoadAndCreateTerrain(heightmap_tex2, CVisual.Instance.GetGraphicsDevice(), length, num_vert, 10, split, m_modelList, "Terrain1");

            // Create a terrain entity
            CTerrainEntity temp_terrain2_ent = new CTerrainEntity(new Vector3(256.0f, -5.0f, 0), Vector3.Zero, "Terrain2", temp_terrain2,
                temp_terrain2.GetMaterials(m_content, "NA"));
            m_entityList.Add(temp_terrain2_ent);


            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Load the editor_Arrow model - This entity will be used to test the forward rendering.
            CDotXModel temp_modelfr = new CDotXModel();
            temp_modelfr = (CDotXModel)temp_modelfr.Initialise(m_content, "Model/editor_Arrow", m_modelList);

            m_forwardRenderTest = new CDotXEntity(new Vector3(0, 6.0f, 15.0f), Vector3.Zero, "EditorArrow", temp_modelfr,
                temp_modelfr.GetMaterials(m_content, "Model/editor_Arrow"), false, false);
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////


            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Create 2000 box entities
            Random ran_num = new Random();

            for (uint i = 0; i < 15; ++i)
            {
                // Load the box model
                CDotXModel temp_modelr = new CDotXModel();
                temp_modelr = (CDotXModel)temp_modelr.Initialise(m_content, "Model/red", m_modelList);

                Vector3 pos = new Vector3(ran_num.Next(80, 90), 8, ran_num.Next(-35, -25));//Vector3(-30.0f, 2.7f, 32.0f)

                //CTerrainEntity.SHeightMapData hm_data = new CTerrainEntity.SHeightMapData();
                //hm_data.Initialise();
                //float height = m_collision.GetHeightOfPosOnTerrain(pos, hm_data);
                //pos.Y = height;

                List<CMaterial> temp_mat_red = temp_modelr.GetMaterials(m_content, "Model/red");
                for (int j = 0; j < temp_mat_red.Count; ++j)
                {
                    temp_mat_red[j].m_diffuse.W = 0.75f;
                }

                CDotXEntity temp_entityr = new CDotXEntity(pos, Vector3.Zero, "Red" + i, temp_modelr,
                    temp_mat_red, false, false);

                m_entityList.Add(temp_entityr);
            }
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Create 2000 box entities
            //Random ran_num = new Random();

            for (uint i = 0; i < 15; ++i)
            {
                // Load the box model
                CDotXModel temp_model = new CDotXModel();
                temp_model = (CDotXModel)temp_model.Initialise(m_content, "Model/blue", m_modelList);

                Vector3 pos = new Vector3(ran_num.Next(70, 80), 8, ran_num.Next(-45, -35));//Vector3(-30.0f, 2.7f, 32.0f)

                //CTerrainEntity.SHeightMapData hm_data = new CTerrainEntity.SHeightMapData();
                //hm_data.Initialise();
                //float height = m_collision.GetHeightOfPosOnTerrain(pos, hm_data);
                //pos.Y = height;

                CDotXEntity temp_entity = new CDotXEntity(pos, Vector3.Zero, "Blue" + i, temp_model,
                    temp_model.GetMaterials(m_content, "Model/blue"), false, false);

                m_entityList.Add(temp_entity);
            }
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Load the box model
            CDotXModel temp_modelt = new CDotXModel();
            temp_modelt = (CDotXModel)temp_modelt.Initialise(m_content, "Model/transparent", m_modelList);

            Vector3 post = new Vector3(0, 4.0f, 15.0f);

            CDotXEntity temp_entityd = new CDotXEntity(post, Vector3.Zero, "Transparent", temp_modelt,
                temp_modelt.GetMaterials(m_content, "Model/transparent"), true, true);
            m_entityList.Add(temp_entityd);
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Load the box model
            CDotXModel temp_modelcu = new CDotXModel();
            temp_modelcu = (CDotXModel)temp_modelcu.Initialise(m_content, "Model/cube", m_modelList);

            CDotXEntity temp_entitycu = new CDotXEntity(new Vector3(-60, 5.0f, 0.0f), Vector3.Zero, "Cube", temp_modelcu,
                temp_modelcu.GetMaterials(m_content, "Model/cube"), true, true);
            m_entityList.Add(temp_entitycu);
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Load the box model
            CDotXModel temp_models = new CDotXModel();
            temp_models = (CDotXModel)temp_models.Initialise(m_content, "Model/shapes", m_modelList);

            CDotXEntity temp_entitys = new CDotXEntity(new Vector3(-60.0f, 5.0f, 10.0f), Vector3.Zero, "Shapes", temp_models,
                temp_models.GetMaterials(m_content, "Model/shapes"), true, true);
            m_entityList.Add(temp_entitys);
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Load the box model
            CDotXModel temp_modelc = new CDotXModel();
            temp_modelc = (CDotXModel)temp_modelc.Initialise(m_content, "Model/grid", m_modelList);

            Vector3 posc = new Vector3(80.0f, 7.0f, -35.0f);//Vector3(-30.0f, 2.7f, 32.0f)

            CDotXEntity temp_entityc = new CDotXEntity(posc, Vector3.Zero, "Grid", temp_modelc,
                temp_modelc.GetMaterials(m_content, "Model/grid"), true, true);
            m_entityList.Add(temp_entityc);
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////


            // Load the Portal Room model
            CDotXModel temp_model2 = new CDotXModel();
            temp_model2 = (CDotXModel)temp_model2.Initialise(m_content, "Model/portal_room", m_modelList);

            // Add the Portal Room entity
            List<CMaterial> temp = temp_model2.GetMaterials(m_content, "Model/portal_room");
            //foreach (CMaterial mat in temp)
            //{
            //    mat.m_diffuse.W = 0.5f;
            //}
            Vector3 pos2 = new Vector3(25, 6, 25);
            CRoomEntity temp_entity2 = new CRoomEntity(pos2, new Vector3(0, 0, 0), "PortalRoom", temp_model2,
                temp, false);

            m_entityList.Add(temp_entity2);


            // Load the box model
            CDotXModel temp_model_box = new CDotXModel();
            temp_model_box = (CDotXModel)temp_model_box.Initialise(m_content, "Model/box", m_modelList);

            // place these entities so that they're within the room.
            for (uint i = 0; i < 5; ++i)
            {
                Vector3 pos3 = new Vector3(23, 6, 20 + (i * 2.5f));
                CDotXEntity temp_entity3 = new CDotXEntity(pos3, Vector3.Zero, "Box" + i+1, temp_model_box,
                    temp_model_box.GetMaterials(m_content, "Model/box"), false, false);
                m_entityList.Add(temp_entity3);
            }

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Create a portal model to visualise the portals
            CWireFrameModel temp_model3 = new CWireFrameModel();
            temp_model3 = (CWireFrameModel)temp_model3.Initialise(CModel.CreateAPortal(), m_modelList, "PortalModel0");

            // Add the portal entity
            Vector3 pos4 = new Vector3(29, 5.55f, 21.68855f);
            CPortalEntity temp_entity4 = new CPortalEntity(Vector3.One, pos4, new Vector3(0, 0, -1.5708f), "Portal0", temp_model3);
            m_entityList.Add(temp_entity4);
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Create a portal model to visualise the portals
            temp_model3 = (CWireFrameModel)temp_model3.Initialise(CModel.CreateAPortal(), m_modelList, "PortalModel0");

            // Add the portal entity
            Vector3 pos5 = new Vector3(29, 5.5f, 27.5f);
            CPortalEntity temp_entity5 = new CPortalEntity(Vector3.One, pos5, new Vector3(0, 0, -1.5708f), "Portal1", temp_model3);
            m_entityList.Add(temp_entity5);
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Create a portal model to visualise the portals
            temp_model3 = (CWireFrameModel)temp_model3.Initialise(CModel.CreateAPortal(), m_modelList, "PortalModel0");

            // Add the portal entity
            Vector3 pos6 = new Vector3(21.2f, 6, 22.1f);
            CPortalEntity temp_entity6 = new CPortalEntity(Vector3.One, pos6, new Vector3(0, 0, 1.5708f), "Portal2", temp_model3);
            m_entityList.Add(temp_entity6);
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////


            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // dynamic collision object
            CDotXModel temp_model23 = new CDotXModel();
            temp_model23 = (CDotXModel)temp_model23.Initialise(m_content, "Model/default", m_modelList);

            List<CMaterial> temp_mat_default = temp_model23.GetMaterials(m_content, "Model/default");
            for (int j = 0; j < temp_mat_default.Count; ++j)
            {
                temp_mat_default[j].m_diffuse.W = 0.4f;
            }

            // Add the // Dynamic collision object entity
            Vector3 pos23 = new Vector3(-25, 8, -50);
            CDynDotXEntity temp_entity23 = new CDynDotXEntity(pos23, new Vector3(0, 0, 0), "Default", temp_model23,
                temp_mat_default, true);
            m_entityList.Add(temp_entity23);
            m_dynamicEntityList.Add(temp_entity23);
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // dynamic Platform
            CDotXModel temp_model25 = new CDotXModel();
            temp_model25 = (CDotXModel)temp_model25.Initialise(m_content, "Model/platform", m_modelList);

            // Add the // dynamic Platform object entity
            Vector3 pos26 = new Vector3(0.0f, 10.0f, 0);

            CKeyFrame.SKFData key_frame_data = new CKeyFrame.SKFData();
            key_frame_data.s_loop = true;
            key_frame_data.s_active = true;
            key_frame_data.s_numKeyFrames = 6;
            key_frame_data.s_keyFrameAry = new CKeyFrame.SKeyFrame[key_frame_data.s_numKeyFrames];
            key_frame_data.s_keyFrameAry[0] = new CKeyFrame.SKeyFrame(0, pos26, new Vector3(0, 0.74f, 0));
            key_frame_data.s_keyFrameAry[1] = new CKeyFrame.SKeyFrame(5, pos26, new Vector3(0, 0.74f, 0));
            key_frame_data.s_keyFrameAry[2] = new CKeyFrame.SKeyFrame(8, new Vector3(10.0f, 10.0f, 50.0f), new Vector3(0, 1.57f, 0));
            key_frame_data.s_keyFrameAry[3] = new CKeyFrame.SKeyFrame(16, new Vector3(40.0f, 20.0f, 50.0f), new Vector3(0, 3.14f, 0));
            key_frame_data.s_keyFrameAry[4] = new CKeyFrame.SKeyFrame(24, new Vector3(40.0f, 10.0f, 0.0f), new Vector3(0, 4.71f, 0));
            key_frame_data.s_keyFrameAry[5] = new CKeyFrame.SKeyFrame(32, pos26, new Vector3(0, 0.74f, 0));

            CKeyFramedDotXEntity temp_entity25 = new CKeyFramedDotXEntity(pos26, new Vector3(0, 0.74f, 0), "KeyFramed", temp_model25,
                temp_model25.GetMaterials(m_content, "Model/platform"), true, key_frame_data);
            m_entityList.Add(temp_entity25);
            m_dynamicEntityList.Add(temp_entity25);
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////

            Vector3 pos24 = new Vector3(-22.0f, 1.7f + 4.064f, 32.0f);
            //LoadBuilding("Model/WholeBuilding", ref for_spt, pos24, true);

            //LoadBuilding("Model/piller", ref for_spt, pos24 + new Vector3(10.0f, 18.0007f, 10.0f), true);
            // LoadBuilding("Model/piller", ref for_spt, pos24 + new Vector3(-10.0f, 18.0007f, -10.0f), true);
            //LoadBuilding("Model/piller", ref for_spt, pos24 + new Vector3(-14.0f, 18.0007f, 14.0f), true);
            //LoadBuilding("Model/piller", ref for_spt, pos24 + new Vector3(14.0f, 18.0007f, -14.0f), true);
            //LoadBuilding("Model/Atrium/sponza", pos24 + new Vector3(0, 0.0f, 0), true);
            //LoadBuilding("Model/flat", pos24 + new Vector3(0, 0.0f, 0), false);
            //LoadBuilding("Model/Atrium/sponza", new Vector3(-12, 4.064f, -23), true, true);
            LoadBuilding("Model/Atrium/sponza", new Vector3(-12, 4.064f, -23), Vector3.Zero, true, false, false);
            LoadBuilding("Model/Flat/flat", pos24, Vector3.Zero, true, false, true);
            LoadBuilding("Model/Radio/Radio", new Vector3(-28, 6.0f, 25), new Vector3(0, 3.14159f, 0), false, false, false);


            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Create a portal model to visualise the portals
            temp_model3 = (CWireFrameModel)temp_model3.Initialise(CModel.CreateAPortal(), m_modelList, "PortalModel0");

            // Add the portal entity
            Vector3 portal_entity_flat_pos = new Vector3(-28.5f, 3.66f, 46.0f);
            CPortalEntity portal_entity_flat = new CPortalEntity(Vector3.One * 2.0f, portal_entity_flat_pos, new Vector3(1.5708f, 0, 1.57f), "PortalEntityFlat", temp_model3);
            m_entityList.Add(portal_entity_flat);
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////


            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // TEST - Particle Entity
            CDotXModel test_model = new CDotXModel();
            test_model = (CDotXModel)test_model.Initialise(m_content, "Model/particle", m_modelList);

            List<CMaterial> test_part_default = test_model.GetMaterials(m_content, "Model/particle");
            for (int j = test_part_default.Count / 5; j < test_part_default.Count; ++j)
            {
                test_part_default[j].m_diffuse.W = 0.99f;
            }

            // Add the // Static collision object entity
            CTESTParticleEntity test_entity = new CTESTParticleEntity(new Vector3(-19, 2, 18), Vector3.Zero, "ParticleEmitter", test_part_default, false);
            //CParticleEffectEntity test_entity = new CParticleEffectEntity(new Vector3(-19, 5, 18), Vector3.Zero, test_model, "ParticleEmitter", false);
            m_entityList.Add(test_entity);
            m_testParticle = test_entity;
            m_dynamicEntityList.Add(test_entity);
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Hit decal
            CDotXModel decal_model = new CDotXModel();
            //decal_model.Initialise(m_content, "Model/hit", false);
            decal_model = (CDotXModel)decal_model.Initialise(m_content, "Model/soldier", m_modelList);

            // Add the // dynamic collision object entity
            Vector3 decal_pos = new Vector3(-60.0f, 38.5f, 40.0f);
            CDecalEntity decal_ent = new CDecalEntity(decal_pos, new Vector3(0, 0, 0), "Decal", decal_model,
                false, decal_model.GetMaterials(m_content, "Model/soldier"));
            m_entityList.Add(decal_ent);
            m_dynamicEntityList.Add(decal_ent);
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Player
            CDotXModel temp_model24 = new CDotXModel();
            temp_model24 = (CDotXModel)temp_model24.Initialise(m_content, "Model/SphereHighPoly", m_modelList);

            // Add the // dynamic collision object entity
            Vector3 pos25 = new Vector3(30, 10, 1.0f);

            CPlayer temp_entity24 = new CPlayer(decal_ent, pos25, Vector3.Zero, "Player", temp_model24,
                temp_model24.GetMaterials(m_content, "Model/SphereHighPoly"), true, ScreenManager.m_inputData);
            m_entityList.Add(temp_entity24);
            m_dynamicEntityList.Add(temp_entity24);
            m_player = temp_entity24;
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////

            // EVENTS
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
            List<string> trig_by0 = new List<string>();
            trig_by0.Add("Player");
            List<string> trig0 = new List<string>();
            trig0.Add("Test0");
            CSphereTriggerEntity trig_ent0 = new CSphereTriggerEntity(new Vector3(73, 4, 4), new Vector3(0.75f, 0, 0), "Trigger0", new Vector3(2.5f),
                trig_by0, 10, trig0, new CEventDelete(this), false);
            m_entityList.Add(trig_ent0);
            m_dynamicEntityList.Add(trig_ent0);
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
            List<string> trig1 = new List<string>();
            trig1.Add("Model/Flat/flat");
            CSphereTriggerEntity trig_ent1 = new CSphereTriggerEntity(new Vector3(28, 13.0f, -8), new Vector3(0.75f, 0, 0), "Trigger1", new Vector3(2.5f),
                trig_by0, 0, trig1, new CEventHide(this), false);
            m_entityList.Add(trig_ent1);
            m_dynamicEntityList.Add(trig_ent1);
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
            List<string> trig2 = new List<string>();
            trig2.Add("Model/Flat/flat");
            CSphereTriggerEntity trig_ent2 = new CSphereTriggerEntity(new Vector3(28, 9, -40), new Vector3(0, 0.9f, 0), "Trigger2", new Vector3(2.5f),
                null, 0, trig2, new CEventReInit(this), false);
            m_entityList.Add(trig_ent2);
            m_dynamicEntityList.Add(trig_ent2);
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////

            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
            List<string> trig3 = new List<string>();
            trig3.Add("Test0");
            CBoxTriggerEntity trig_ent3 = new CBoxTriggerEntity(new Vector3(73, 4, 34), new Vector3(0, 0.9f, 0), "Trigger3", trig_by0, 0, new Vector3(2.5f,2.5f,2.5f), 
                trig3, new CEventCreate(this), false);
            m_entityList.Add(trig_ent3);
            m_dynamicEntityList.Add(trig_ent3);
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////


#if WINDOWS
            // ANIMATED MODEL
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
            //CAnimatedModel twiddle = new CAnimatedModel();
            //twiddle.Initialise(m_content.Load<Model>("Model/dude"), "Model/dude", false);
            //m_modelList.Add(twiddle);

            CAnimatedModel anim_model = new CAnimatedModel();
            anim_model = (CAnimatedModel)anim_model.Initialise(m_content, "Model/head_fbx", m_modelList);

            // Add the // dynamic collision object entity
            Vector3 twiddle_pos = new Vector3(22.0f, 16.0f, 78.0f);
            CAnimatedEntity twiddle_ent = new CAnimatedEntity(twiddle_pos, new Vector3(0, 0, 0), "AnimatedEntity", anim_model,
                anim_model.GetMaterials(m_content, "Model/head_fbx"),
                true);
            m_entityList.Add(twiddle_ent);
            m_dynamicEntityList.Add(twiddle_ent);
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
#endif


            CCueEntity cue_entity = new CCueEntity(new Vector3(-28, 6.0f, 25), Vector3.Zero, m_camera.GetInvView(), "RadioCue", CModel.CreateOBBofUnitSize(40.0f), "3D.TF2", true);
            m_entityList.Add(cue_entity);
            m_dynamicEntityList.Add(cue_entity);


            //// Loading the lights //////////////////////////////////////////////////////////////////////////////////////
            // Add the dir light
            Vector3 light_dir_pos = new Vector3(0, 0, 0);
            CDirLightEntity light_dir_ent = new CDirLightEntity(new Vector3(1.0f, 1.0f, 1.0f), light_dir_pos, "DirLight", true, 4,
                new Vector3(0.2f, 0.3f, 0.4f), 1.0f, CModel.CreateOBBofUnitSize(256), true, 0.65f);
            m_entityList.Add(light_dir_ent);
            m_scDirLightList.Add(light_dir_ent);


            // Add the point light
            Vector3 light_pt_pos4 = new Vector3(-24.0f, 8.0f, 25.0f);
            CPointLightEntity light_pt_ent4 = new CPointLightEntity(light_pt_pos4, new Vector3(0, 0, 0), "PtLight0", 20.0f,
                new Vector3(1.0f, 0.8f, 0.2f), 1.0f, CModel.CreateOBBofUnitSize(20.0f), false);
            m_entityList.Add(light_pt_ent4);


            // Add the point light
            Vector3 light_pt_pos3 = new Vector3(1.0f, 8.0f, 42.0f);
            CPointLightEntity light_pt_ent3 = new CPointLightEntity(light_pt_pos3, new Vector3(0, 0, 0), "PtLight1", 20.0f,
                new Vector3(0, 0.4f, 0.3f), 1.0f, CModel.CreateOBBofUnitSize(20.0f), false);
            m_entityList.Add(light_pt_ent3);


            //// Add the point light
            //Vector3 light_pt_pos2 = new Vector3(24.0f, 12.0f, 24.0f);
            //CPointLightEntity light_pt_ent2 = new CPointLightEntity(light_pt_pos2, new Vector3(0, 0, 0), 20.0f,
            //    new Vector3(1.0f, 0.8f, 0.2f), 1.0f, CModel.CreateOBBofUnitSize(20.0f), false);
            //m_entityList.Add(light_pt_ent2);


            //for (int i = 0; i < 50; ++i)
            //{
            //    // Add the point light
            //    Vector3 light_pt_pos = new Vector3(ran_num.Next(-127, 127), ran_num.Next(5, 20), ran_num.Next(-127, 0));
            //    CPointLightEntity light_pt_ent = new CPointLightEntity(light_pt_pos, new Vector3(0, 0, 0), "PtLight" + i+2, 20.0f,
            //        new Vector3(1.0f, 0.8f, 0.2f), 1.0f, CModel.CreateOBBofUnitSize(20.0f), false);
            //    m_entityList.Add(light_pt_ent);
            //}


            //// Add the spot light to spatial partioning tree.
            //m_entityList[m_entityList.Count - 1].GetMinMax(out min, out max);
            //for_spt.Add(new CSpatialTree.STreeInput(light_spot_pos, m_entityList.Count - 1, false, false, false, new CSpatialTree.SPortalInfo(), 0, min, max));


            //// Add the spot light
            //Vector3 light_spot_pos2 = new Vector3(-34.0f, 9.0f, 43.0f);
            //CSpotLightEntity light_spot_ent2 = new CSpotLightEntity(light_spot_pos2, new Vector3(0.0f, 3.0f, 36.0f), new Vector3(0, 0, 0), 40.0f,
            //    25.0f, true, new Vector3(0, 0.8f, 0.7f), 2.0f, CModel.CreateOBBofUnitSize(40.0f), false);
            //m_entityList.Add(light_spot_ent2);
            //m_scSpotLightList.Add(light_spot_ent2);
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////

            CSharedLoggerMessenger.Instance.WriteMessage(ESharedLoggerType.e_info, "CMainGameSceen", "Content has been loaded.");

            // Call base CGameScreenHelper
            return base.LoadContent();
        }

        private void LoadBuilding(string ModelName, Vector3 Pos, Vector3 Rot, bool Collision, bool Translucent, bool Room)
        {
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
            // Static collision object
            CDotXModel temp_model24 = new CDotXModel();
            temp_model24 = (CDotXModel)temp_model24.Initialise(m_content, ModelName, m_modelList);

            List<CMaterial> temp_mat_default = temp_model24.GetMaterials(m_content, ModelName);
            if (Translucent == true)
            {
                for (int j = temp_mat_default.Count / 5; j < temp_mat_default.Count; ++j)
                {
                    temp_mat_default[j].m_diffuse.W = 0.4f;
                }
            }

            if (Room == false)
            {
                // Add the // Static collision object entity
                CDotXEntity temp_entity24 = new CDotXEntity(Pos, Rot, ModelName, temp_model24,
                    temp_mat_default, Collision, Collision);
                m_entityList.Add(temp_entity24);
            }
            else
            {
                // Add the // Static collision object entity
                CRoomEntity temp_entity24 = new CRoomEntity(Pos, Rot, ModelName, temp_model24,
                    temp_mat_default, Collision);
                m_entityList.Add(temp_entity24);
            }
            //////////////////////////////////////////////////////////////////////////////////////////////////////////////
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            if (ScreenManager.m_inputData.m_back == true)
            {
                ScreenManager.Exit();
            }

            base.BeginUpdate(gameTime);

            m_testParticle.Update(m_camera, (float)gameTime.ElapsedGameTime.Milliseconds / 1000.0f);

            m_camera.Update(m_player.GetWorldPosition(), m_player.GetWorldMatrix().Forward, Matrix.CreateFromQuaternion(m_player.GetWorldRotation()), m_player.GetWorldMatrix());

            base.EndUpdate(gameTime, m_camera, m_player.GetWorldPosition());
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Draw(GameTime gameTime)
        {
            // As usual, first call BeginDraw.
            base.BeginDraw(m_camera, gameTime);

            // You can add you're own render code here if you wish. The deferred renderer will be used if you render anything here.
            // All the lights in the scene will be used to light any entities you palce here to render.

            // Now you also need to call MainDraw. This is so taht the engine can render the entities in the m_entityList.
            base.MainDraw(gameTime, m_camera/*, false*/);

            // Post process can be performed here for non translucent objects.

            // This needs to be called if you want to have a light, currently only ONE directional light is supported.
            // If you do not wish to add any lights to the objects you are about to render then only pass in the camera.
            // This needs to be called even when rendering Translucent objects.
            CVisual.Instance.BeginForwardRender(m_scDirLightList[0], m_camera);

            // You can add you're own render code here if you wish. The forward renderer will be used here.
            // This is where you call the entities you wish to render.

            if (m_forwardRenderTest != null)
            {
                // NOTE: It's ForwardRender(...) not just Render(...).
                m_forwardRenderTest.ForwardRender(m_camera);
            }

            base.TranslucentDraw(m_camera);

            CVisual.Instance.EndForwardRender(gameTime);

            // Post process can be performed here for all objects.

            // Now you must call EndDraw.
            base.EndDraw(m_camera);
        }
    }
}
#endif
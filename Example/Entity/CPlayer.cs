﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using Onderzoek;
using Onderzoek.Visual;
using Onderzoek.ModelData;
using Onderzoek.Physics;
using Onderzoek.Camera;
using Onderzoek.Audio;
using Onderzoek.Input;

namespace Example.Entity
{
    public class CPlayer : Onderzoek.Entity.CDynamicEntity
    {
        CPhysics.SProjectileData m_projData;
        CCollision.SCollisionData m_collisionData;
        bool m_freeFall = false;
        bool m_atStartOfFrame = false;
        protected Vector3 m_prvPosition;
        float m_pitch = 0;
        float m_yaw = 0;
        float m_height = 0;
        CInputData m_inputData;
        Vector3 m_prvPosForCollision;

        CDecalEntity m_hitEntity;

        public override void Dispose()
        {
            base.Dispose();

            m_hitEntity.Dispose();
        }

        public CPlayer(CDecalEntity HitEntity, Vector3 Position, Vector3 Rotation, string EntityName, CModel Model, List<CMaterial> Material,
            bool IsCollidable, CInputData InputData)
        {
            Initialise(HitEntity, Position, Quaternion.CreateFromYawPitchRoll(Rotation.Y, Rotation.X, Rotation.Z), EntityName, Model, Material,
                IsCollidable, InputData);
        }

        public void Initialise(CDecalEntity HitEntity, Vector3 Position, Quaternion Rotation, string EntityName, CModel Model,
            List<CMaterial> Material, bool IsCollidable, CInputData InputData)
        {
            base.Initalise();

            m_inputData = InputData;
            m_portalInfo = new SPortalInfo();
            m_material = Material;

            m_collisionData = new CCollision.SCollisionData(Vector3.Zero);
            m_projData = new CPhysics.SProjectileData();
            m_projData.s_acceleration = new Vector3(0, -98.1f, 0);
            m_projData.s_curVel = new Vector3();

            m_entityName = EntityName;
            m_hitEntity = HitEntity;
            m_entityHMData = new Onderzoek.Entity.CTerrainEntity.SHeightMapData();
            m_entityHMData.Initialise();
            m_isDynamic = true;
            m_position = Position;
            m_modelData = Model;
            m_isCollidable = IsCollidable;

            m_worldMatrix = Matrix.CreateFromYawPitchRoll(Rotation.Y, Rotation.X, Rotation.Z) * Matrix.CreateTranslation(m_position);

            // Transform local space OBB corners to world space
            InitialiseEntityOBB(m_modelData.GetOBB());

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            IsTranslucent(((CDotXModel)m_modelData).m_model);
        }

        private void FindNewVelocity()
        {
            m_projData.s_curVel = (m_prvPosition - m_position) * -50.0f;
        }

        public override int GetEntityID()
        {
            return 21;
        }

#if EDITOR
        public override void Update()
        {
            base.Update();
        }
#else

        public override void Update(float DT)
        {
            m_prvPosition = m_position;

            /////////////////////////////Look at maths//////////////////////////////
            m_pitch -= m_inputData.m_rightStick.Y * 0.1f;

            //constrain pitch to vertical to avoid confusion
            m_pitch = MathHelper.Clamp(m_pitch, -(MathHelper.PiOver2) + .0001f,
                (MathHelper.PiOver2) - .0001f);

            m_yaw -= m_inputData.m_rightStick.X * 0.1f;

            //float mod yaw to avoid eventual precision errors
            //as we move away from 0
            m_yaw = m_yaw % MathHelper.TwoPi;

            //create a new aspect based on pitch and yaw
            m_rotation = Quaternion.CreateFromAxisAngle(Vector3.Up, -m_yaw) *
                Quaternion.CreateFromAxisAngle(Vector3.Right, m_pitch);

            //normalize to reduce errors
            Quaternion.Normalize(ref m_rotation, out m_rotation);
            ////////////////////////////////////////////////////////////////////////

            m_prvPosForCollision = Vector3.Transform(m_prvPosition, m_parWrldMat);
            if (m_freeFall == false)
            {
                Vector3 v = new Vector3(m_inputData.m_leftStick.X * 0.4f, 0, -m_inputData.m_leftStick.Y * 0.4f);
                m_position += Vector3.Transform(v, m_rotation);

                if (Vector3.DistanceSquared(m_prvPosForCollision, m_position) != 0)
                {
                    CCueManager.Instance.PlayCue("PlayerWalk");
                }
            }

            //if (m_inputData.m_useHeight == true) // DEBUG purpose only
            {
                // If the player has just started to jump then do this
                if (m_inputData.m_jump == true)
                {
                    CCueManager.Instance.RestartCue("Game.PlayerJump");
                    m_inputData.m_jump = false;
                    m_projData.s_curVel += new Vector3(0, 40.0f, 0);

                    m_projData.s_nxtPos = m_position;
                    CPhysics.UpdateProjectilePhysics(ref m_projData, DT);
                    m_position = m_projData.s_nxtPos;
                    m_freeFall = true;
                }
                else // Do normal
                {
                    if (CCollision.Instance.GetHeight(this, m_worldMatrix.Translation, m_entityLeafNode, ref m_entityHMData) == false)
                    {
                        // SO that it doesn't keep falling or keep rising when it falls out of the scene.
                        m_height = m_worldMatrix.Translation.Y;
                    }

                    // Now get the actual height of the player
                    float dif = m_worldMatrix.Translation.Y - m_height;

                    // Use physics if the difference between the heights are bigger than 0.5f
                    if (dif > 0.5f)
                    {
                        m_projData.s_nxtPos = m_position;
                        CPhysics.UpdateProjectilePhysics(ref m_projData, DT);
                        m_position = m_projData.s_nxtPos;
                        m_freeFall = true;
                    }
                    //// If the incline is too steep then player can't move up that incline.
                    //else if (dif < -0.5f)
                    //{
                    //    m_position = m_prvPosition;
                    //}
                    else
                    {
                        Vector3 temp = Vector3.Transform(new Vector3(0, m_height, 0), Matrix.Invert(m_parWrldMat));
                        m_position.Y = temp.Y;

                        // Store the velocity of the player
                        FindNewVelocity();
                        m_freeFall = false;
                    }
                }
            }

            //TEMP - OLD
            //List<int> other_entity_list = m_entityLeafNode.GetOtherEntityIDsInSameSpace();
            //TEMP - OLD

            // Is this entity intersecting with any other entity?
            m_position = Vector3.Transform(m_position, m_parWrldMat);

            //TEMP - OLD
            //CCollision.Instance.NewPosIfIntersectingAABBvsAABB(other_entity_list, m_wrldOBB, ref m_position);
            //TEMP - OLD


            // Get the entities which this entity could collide into.
            if (Vector3.DistanceSquared(m_prvPosForCollision, m_position) > 0.001f)
            {
                CCollision.Instance.CheckForCollisions(this, m_entityLeafNode, m_prvPosForCollision,
                    m_position, ref m_collisionData);
            }

            m_position = Vector3.Transform(m_position, Matrix.Invert(m_parWrldMat));

            // This will set the parent child relationship data.
            CCollision.Instance.PostCollisionCheck(m_updateTicket, this);


            Matrix rot = Matrix.CreateFromQuaternion(m_rotation);
            m_worldMatrix = rot * Matrix.CreateTranslation(m_position);
            // This will apply the parent matrix on to the entity if it's has a parent.
            // This MUST be called straight after creating the new world matrix!
            UpdateIfChild();

            // Update the obbs
            List<CModelOBB> OBB = m_modelData.GetOBB();
            UpdateEntityOBB(OBB);

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            // The following line will only needed to be updated if the bounding box size changes or if the dynamic object moves(translation) in the scene.
            // This needs to be updated for the calcualtion that will take place in the below base update.
            UpdateCollisionSphere();
            m_collisionDist = 1.0f;

            if (m_inputData.m_weaponFire == true && prv == false)
            {
                Ray shot_ray;
                shot_ray.Direction = m_worldMatrix.Forward;
                shot_ray.Position = m_worldMatrix.Translation + shot_ray.Direction;

                float dist = 1000.0f;

                m_entityLeafNode.CheckRayVsBBInTree(shot_ray, ref dist);

                if (dist != 1000.0f)
                {
                    m_hitEntity.SetPositionOfDecal(shot_ray.Position + (m_worldMatrix.Forward * (dist - 0.5f)), shot_ray.Direction);
                    //m_hitEntity.SetRotation();
                }
            }
            prv = m_inputData.m_weaponFire;
            m_inputData.m_weaponFire = false;
        }

        public override void STUpdateEntityNodeInTree()
        {
            // this needs to be called so that the spatial partioning tree is updated and it won't just disappear from the scene.
            m_entityLeafNode.MoveNodeAroundTree(m_worldMatrix.Translation, m_maxLengthOfEntity);
        }

        bool prv = false;
#endif

        // Needs to be called as soon as the entity becomes a child of another entity.
        public override void SetAsChild(Quaternion ParRot, Matrix PWrldMat, Onderzoek.Entity.CEntity Parent)
        {
            Matrix inv_par_wrld = Matrix.Invert(PWrldMat);
            m_parWrldMat = PWrldMat;
            m_parRotation = ParRot;
            m_position = Vector3.Transform(m_position, inv_par_wrld);
            m_prvPosition = Vector3.Transform(m_prvPosition, inv_par_wrld);
            FindNewVelocity();

            m_rotation = Quaternion.Concatenate(m_rotation, Quaternion.Inverse(ParRot));

            Vector3 temp = CToolbox.QuaternionToEuler(m_rotation);
            m_yaw = -temp.X;
            m_pitch = temp.Y;

            m_isChild = true;
        }

        // Needs to be called when the entity stops becoming a child of an entity.
        public override void SetAsParent(Quaternion ParRot, Matrix PWrldMat)
        {
            m_parWrldMat = Matrix.Identity;
            m_parRotation = Quaternion.Identity;
            m_prvPosition = Vector3.Transform(m_prvPosition, PWrldMat);
            m_position = Vector3.Transform(m_position, PWrldMat);
            FindNewVelocity();
            m_rotation = Quaternion.Concatenate(m_rotation, ParRot);

            Vector3 temp = CToolbox.QuaternionToEuler(m_rotation);
            m_yaw = -temp.X;
            m_pitch = temp.Y;

            m_isChild = false;
        }

        public override Quaternion GetWorldRotation()
        {
            if (m_isChild == true)
            {
                Matrix rot = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateFromQuaternion(m_parRotation);
                return Quaternion.CreateFromRotationMatrix(rot);
            }
            else
            {
                return m_rotation;
            }
        }

        public override bool CheckAgainstClipPlanes(Plane[] ClipPlanes)
        {
            if (m_atStartOfFrame == true)
            {
                return base.CheckAgainstClipPlanes(ClipPlanes);
            }

            return false;
        }

        public override bool CheckAgainstClipPlanes(List<Plane[]> ClipPlanes)
        {
            if (m_atStartOfFrame == true)
            {
                return base.CheckAgainstClipPlanes(ClipPlanes);
            }

            return false;
        }

        // This is used by the collision engine to set the new height of the entity.
        public override void SetNewHeight(Onderzoek.Entity.CEntity HitEntity, Vector3 Height)
        {
            m_height = Height.Y + 2.0f;
        }

        public override void IsHit(Onderzoek.Entity.CEntity Collider, float DistFromCollider, Vector3 NormalOfCollision)
        {
        }

        public override void HasHit(Onderzoek.Entity.CEntity HitEntity, float DistToHitObject, Vector3 NormalOfCollision)
        {
            float dif = m_collisionDist - DistToHitObject;
            Vector3 move_back_by = NormalOfCollision * -dif;

            m_position = m_position + move_back_by;
            m_prvPosition = m_prvPosition + move_back_by;

            if (m_freeFall == true)
            {
                if (NormalOfCollision.Y != 0)
                {
                    m_projData.s_curVel.Y = 0;
                }
                else if (NormalOfCollision.X != 0)
                {
                    m_projData.s_curVel.X = 0;// -m_projData.s_curVel.X;
                    //m_position = m_prvPosition;
                }
                else
                {
                    m_projData.s_curVel.Z = 0;// -m_projData.s_curVel.Z;
                    //m_position = m_prvPosition;
                }
            }
        }

        public override void SetRender(bool Render, bool AtStartOfFrame)
        {
            m_atStartOfFrame = AtStartOfFrame;
            m_render = Render;
        }

        public override void SetMaterialList(List<CMaterial> MaterialList)
        {
            m_material = MaterialList;
            IsTranslucent(((CDotXModel)m_modelData).m_model);
        }

        public override void Render(CCamera Camera)
        {
            CVisual.Instance.Render(((CDotXModel)m_modelData).m_model, Camera, m_material, m_worldMatrix, m_opaqueParts);
        }

        public override void ForwardRender(CCamera Camera)
        {
            CVisual.Instance.ForwardRender(((CDotXModel)m_modelData).m_model, m_material, m_worldMatrix, m_opaqueParts);
        }

        public override void ForwardTranslucentRender(CCamera Camera)
        {
            CVisual.Instance.ForwardRender(((CDotXModel)m_modelData).m_model, m_material, m_worldMatrix, m_translucentParts);
        }

        public override void RenderForShadow(CCamera Camera)
        {
            CVisual.Instance.RenderForShadow(((CDotXModel)m_modelData).m_model, m_worldMatrix);
        }
    }
}

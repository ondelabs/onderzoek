﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Microsoft.Xna.Framework;

using Onderzoek;
using Onderzoek.Visual;
using Onderzoek.ModelData;
using Onderzoek.Physics;
using Onderzoek.Camera;

namespace Example.Entity
{
    public class CDecalEntity : Onderzoek.Entity.CDynamicEntity
    {
        public override void Dispose()
        {
            base.Dispose();
        }

        public CDecalEntity(Vector3 Position, Vector3 Rotation, string EntityName, CModel Model, bool IsCollidable, List<CMaterial> Material)
        {
            Initialise(Position, Quaternion.CreateFromYawPitchRoll(Rotation.Y, Rotation.X, Rotation.Z), EntityName, Model, IsCollidable, Material);
        }

        public void Initialise(Vector3 Position, Quaternion Rotation, string EntityName, CModel Model, bool IsCollidable, List<CMaterial> Material)
        {
            base.Initalise();

            m_portalInfo = new SPortalInfo();
            m_material = Material;
            m_material[0].m_specularPower = 128.0f;
            m_material[0].m_specularColour = new Vector4(1.0f, 1.0f, 1.0f, 1.0f);

            m_entityName = EntityName;
            m_isDynamic = true;
            m_position = Position;
            m_modelData = Model;
            m_isCollidable = IsCollidable;
            m_rotation = Rotation;

            m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);

            // Transform local space OBB corners to world space
            InitialiseEntityOBB(m_modelData.GetOBB());

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            IsTranslucent(((CDotXModel)m_modelData).m_model);
        }

#if EDITOR
        public override void Update()
        {
            base.Update();
        }
#else

        public override void Update(float DT)
        {
            m_worldMatrix = Matrix.CreateFromQuaternion(m_rotation) * Matrix.CreateTranslation(m_position);
            // This will apply the parent matrix on to the entity if it's has a parent.
            // This MUST be called straight after creating the new world matrix!
            UpdateIfChild();

            // Update the obbs
            List<CModelOBB> OBB = m_modelData.GetOBB();
            UpdateEntityOBB(OBB);

            // Get the min and max world positions
            CToolbox.GetMinMax(m_wrldOBB[0].m_wrldOBB, out m_bb);

            // The following two lines will only needed to be updated if the bounding box size changes or if the dynamic object moves(translation) in the scene.

            // This needs to be updated for the calcualtion that will take place in the below base update.
            UpdateCollisionSphere();
        }

        public void SetPositionOfDecal(Vector3 Position, Vector3 Dir)
        {
            if (m_isChild == true)
            {
                SetAsParent(m_parRotation, m_parWrldMat);
            }

            Update(0);

            m_position = Position;

            Ray temp = new Ray(Position, Dir);
            CCollision.Instance.CheckForParentChildRelationship(m_entityLeafNode, this, temp);

            CCollision.Instance.PostCollisionCheck(m_updateTicket, this);

            Update(0);
        }
#endif

        public override void SetMaterialList(List<CMaterial> MaterialList)
        {
            m_material = MaterialList;
            IsTranslucent(((CDotXModel)m_modelData).m_model);
        }

        public override void Render(CCamera Camera)
        {
            CVisual.Instance.Render(((CDotXModel)m_modelData).m_model, Camera, m_material, m_worldMatrix, m_opaqueParts);
        }

        public override void ForwardRender(CCamera Camera)
        {
            CVisual.Instance.ForwardRender(((CDotXModel)m_modelData).m_model, m_material, m_worldMatrix, m_opaqueParts);
        }

        public override void ForwardTranslucentRender(CCamera Camera)
        {
            CVisual.Instance.ForwardRender(((CDotXModel)m_modelData).m_model, m_material, m_worldMatrix, m_translucentParts);
        }

        //public override void RenderForShadow(CCamera Camera)
        //{
        //    CVisual.Instance.RenderForShadow(((CDotXModel)m_modelData).m_model, Camera, m_worldMatrix);
        //}
    }
}
